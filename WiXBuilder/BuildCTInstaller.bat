
IF [%1]==[] GOTO DEFAULTPATH
  SET ProjectBinariesPath=%1
  SET ProjectBinariesPath=%ProjectBinariesPath:"=%
GOTO ENDPATH
:DEFAULTPATH
  SET ProjectBinariesPath=.\Debug
:ENDPATH

IF [%2]==[] GOTO DEFAULTVERSIONNUMBER
  SET ProjectBinariesVersion=%2
  SET ProjectBinariesVersion=%ProjectBinariesVersion:"=%
GOTO ENDVERSIONNUMBER
:DEFAULTVERSIONNUMBER
FOR /F "tokens=2 delims==" %%I IN ('wmic datafile where "name='%ProjectBinariesPath:\=\\%GRINGlobal-CuratorTool.exe'" get version /value') DO SET ProjectBinariesVersion=%%I
SET ProjectBinariesVersion=%ProjectBinariesVersion%
:ENDVERSIONNUMBER

SET ProjectProgramDataPath=%ProjectBinariesPath:GRINGlobal.Client.CuratorTool\GRINGlobal.Client.CuratorTool\bin\Release\=DefaultProgramDataFiles\GRIN-Global%
SET ProjectFormsPath=%ProjectBinariesPath%Forms
SET ProjectImagesPath=%ProjectBinariesPath%Images
SET ProjectWizardsPath=%ProjectBinariesPath%Wizards
SET ProjectMicrosoftReportViewer15Path=%ProjectBinariesPath%MicrosoftReportViewer15
SET ProjectZXingPath=%ProjectBinariesPath:GRINGlobal.Client.CuratorTool\GRINGlobal.Client.CuratorTool\bin\Release\=ZXing%

"%WIX%bin\heat.exe" dir "%ProjectFormsPath%" -var env.ProjectFormsPath -cg FormsGroup -dr INSTALLDIR -gg -sfrag -template fragment -out ProjectForms.wxs
"%WIX%bin\candle.exe" -arch x64 ProjectForms.wxs

"%WIX%bin\heat.exe" dir "%ProjectImagesPath%" -var env.ProjectImagesPath -cg ImagesGroup -dr INSTALLDIR -gg -sfrag -template fragment -out ProjectImages.wxs
"%WIX%bin\candle.exe" -arch x64 ProjectImages.wxs

"%WIX%bin\heat.exe" dir "%ProjectProgramDataPath%" -var env.ProjectProgramDataPath -cg ProgramDataGroup -dr CommonAppDataFolder -gg -sfrag -template fragment -out ProjectProgramData.wxs
"%WIX%bin\candle.exe" -arch x64 ProjectProgramData.wxs

"%WIX%bin\heat.exe" dir "%ProjectWizardsPath%" -var env.ProjectWizardsPath -cg WizardsGroup -dr INSTALLDIR -gg -sfrag -template fragment -out ProjectWizards.wxs
"%WIX%bin\candle.exe" -arch x64 ProjectWizards.wxs

"%WIX%bin\heat.exe" dir "%ProjectMicrosoftReportViewer15Path%" -var env.ProjectMicrosoftReportViewer15Path -cg MicrosoftReportViewer15Group -dr INSTALLDIR -gg -sfrag -template fragment -out ProjectMicrosoftReportViewer15.wxs
"%WIX%bin\candle.exe" -arch x64 ProjectMicrosoftReportViewer15.wxs

"%WIX%bin\heat.exe" dir "%ProjectZXingPath%" -var env.ProjectZXingPath -cg ZXingGroup -dr INSTALLDIR -gg -sfrag -sreg -template fragment -out ProjectZXing.wxs
"%WIX%bin\candle.exe" -arch x64 ProjectZXing.wxs

"%WIX%bin\candle.exe" -arch x64 CuratorToolProduct.wxs
"%WIX%bin\light.exe" CuratorToolProduct.wixobj ProjectForms.wixobj ProjectImages.wixobj ProjectProgramData.wixobj ProjectWizards.wixobj ProjectMicrosoftReportViewer15.wixobj ProjectZXing.wixobj -out GrinGlobal_Client_Installer.msi

"%WIX%bin\candle.exe" -arch x64 -ext WiXBalExtension -ext WixUtilExtension -ext WiXNetFxExtension CuratorToolBundle.wxs
"%WIX%bin\light.exe" CuratorToolBundle.wixobj -ext WiXBalExtension -ext WixUtilExtension -ext WiXNetFxExtension -out GrinGlobal_Client_Installer.exe