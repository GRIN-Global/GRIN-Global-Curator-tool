﻿using System;
using System.Windows.Forms;

namespace AttachmentWizard
{
    partial class AttachmentWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_datagridviewAttachments = new System.Windows.Forms.DataGridView();
            this.ux_buttonBatchFiles = new System.Windows.Forms.Button();
            this.ux_splitcontainerMain = new System.Windows.Forms.SplitContainer();
            this.ux_checkboxViewExistingAttachments = new System.Windows.Forms.CheckBox();
            this.ux_treeviewList = new System.Windows.Forms.TreeView();
            this.ux_splitcontainerAttachmentDataDisplay = new System.Windows.Forms.SplitContainer();
            this.ux_groupboxView = new System.Windows.Forms.GroupBox();
            this.ux_radiobuttonViewDetails = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonViewTile = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonViewList = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonViewSmallIcon = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonViewLargeIcon = new System.Windows.Forms.RadioButton();
            this.ux_listviewAttachments = new System.Windows.Forms.ListView();
            this.ux_tabcontrolMain = new System.Windows.Forms.TabControl();
            this.ux_tabpageFormView = new System.Windows.Forms.TabPage();
            this.ux_panelFormView = new System.Windows.Forms.Panel();
            this.ux_labelDescriptionCode = new System.Windows.Forms.Label();
            this.ux_comboboxDescriptionCode = new System.Windows.Forms.ComboBox();
            this.ux_textboxDescription = new System.Windows.Forms.TextBox();
            this.ux_textboxCopyrightInformation = new System.Windows.Forms.TextBox();
            this.ux_labelDescription = new System.Windows.Forms.Label();
            this.ux_textboxTitle = new System.Windows.Forms.TextBox();
            this.ux_labelTitle = new System.Windows.Forms.Label();
            this.ux_labelSortOrder = new System.Windows.Forms.Label();
            this.ux_labelAttachCooperator = new System.Windows.Forms.Label();
            this.ux_textboxSortOrder = new System.Windows.Forms.TextBox();
            this.ux_textboxAttachCooperator = new System.Windows.Forms.TextBox();
            this.ux_labelNote = new System.Windows.Forms.Label();
            this.ux_comboboxAttachDateCode = new System.Windows.Forms.ComboBox();
            this.ux_textboxNote = new System.Windows.Forms.TextBox();
            this.ux_labelAttachDateCode = new System.Windows.Forms.Label();
            this.ux_labelCopyrightInformation = new System.Windows.Forms.Label();
            this.ux_textboxAttachDate = new System.Windows.Forms.TextBox();
            this.ux_checkboxIsWebVisible = new System.Windows.Forms.CheckBox();
            this.ux_labelAttachDate = new System.Windows.Forms.Label();
            this.ux_labelCategoryCode = new System.Windows.Forms.Label();
            this.ux_textboxContentType = new System.Windows.Forms.TextBox();
            this.ux_comboboxCategoryCode = new System.Windows.Forms.ComboBox();
            this.ux_labelContentType = new System.Windows.Forms.Label();
            this.ux_tabpageGridView = new System.Windows.Forms.TabPage();
            this.ux_openfiledialog = new System.Windows.Forms.OpenFileDialog();
            this.ux_buttonSave = new System.Windows.Forms.Button();
            this.ux_buttonSaveAndExit = new System.Windows.Forms.Button();
            this.ux_menustripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ux_statusstripMain = new System.Windows.Forms.StatusStrip();
            this.ux_statusProgressDescription = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_statusProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.ux_statusProgressValue = new System.Windows.Forms.ToolStripStatusLabel();
            this.ux_groupboxAttachmentType = new System.Windows.Forms.GroupBox();
            this.ux_radiobuttonInventory = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonTaxonomySpecies = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonTaxonomyGenus = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonTaxonomyFamily = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonOrderRequest = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonMethod = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonCropTraitCode = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonCropTrait = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonCrop = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonAccessionInventoryGroup = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonAccession = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewAttachments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerMain)).BeginInit();
            this.ux_splitcontainerMain.Panel1.SuspendLayout();
            this.ux_splitcontainerMain.Panel2.SuspendLayout();
            this.ux_splitcontainerMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerAttachmentDataDisplay)).BeginInit();
            this.ux_splitcontainerAttachmentDataDisplay.Panel1.SuspendLayout();
            this.ux_splitcontainerAttachmentDataDisplay.Panel2.SuspendLayout();
            this.ux_splitcontainerAttachmentDataDisplay.SuspendLayout();
            this.ux_groupboxView.SuspendLayout();
            this.ux_tabcontrolMain.SuspendLayout();
            this.ux_tabpageFormView.SuspendLayout();
            this.ux_panelFormView.SuspendLayout();
            this.ux_tabpageGridView.SuspendLayout();
            this.ux_menustripMain.SuspendLayout();
            this.ux_statusstripMain.SuspendLayout();
            this.ux_groupboxAttachmentType.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_datagridviewAttachments
            // 
            this.ux_datagridviewAttachments.AllowUserToAddRows = false;
            this.ux_datagridviewAttachments.AllowUserToOrderColumns = true;
            this.ux_datagridviewAttachments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewAttachments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewAttachments.Location = new System.Drawing.Point(3, 3);
            this.ux_datagridviewAttachments.Name = "ux_datagridviewAttachments";
            this.ux_datagridviewAttachments.Size = new System.Drawing.Size(676, 247);
            this.ux_datagridviewAttachments.TabIndex = 0;
            this.ux_datagridviewAttachments.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.ux_datagridviewAttachments_CellEnter);
            this.ux_datagridviewAttachments.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.ux_datagridview_RowEnter);
            // 
            // ux_buttonBatchFiles
            // 
            this.ux_buttonBatchFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonBatchFiles.Location = new System.Drawing.Point(175, 3);
            this.ux_buttonBatchFiles.Name = "ux_buttonBatchFiles";
            this.ux_buttonBatchFiles.Size = new System.Drawing.Size(91, 23);
            this.ux_buttonBatchFiles.TabIndex = 1;
            this.ux_buttonBatchFiles.Text = "Batch Files...";
            this.ux_buttonBatchFiles.UseVisualStyleBackColor = true;
            this.ux_buttonBatchFiles.Click += new System.EventHandler(this.ux_buttonBatchFiles_Click);
            // 
            // ux_splitcontainerMain
            // 
            this.ux_splitcontainerMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_splitcontainerMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_splitcontainerMain.Location = new System.Drawing.Point(4, 87);
            this.ux_splitcontainerMain.Name = "ux_splitcontainerMain";
            // 
            // ux_splitcontainerMain.Panel1
            // 
            this.ux_splitcontainerMain.Panel1.Controls.Add(this.ux_checkboxViewExistingAttachments);
            this.ux_splitcontainerMain.Panel1.Controls.Add(this.ux_treeviewList);
            this.ux_splitcontainerMain.Panel1.Controls.Add(this.ux_buttonBatchFiles);
            // 
            // ux_splitcontainerMain.Panel2
            // 
            this.ux_splitcontainerMain.Panel2.Controls.Add(this.ux_splitcontainerAttachmentDataDisplay);
            this.ux_splitcontainerMain.Size = new System.Drawing.Size(977, 649);
            this.ux_splitcontainerMain.SplitterDistance = 273;
            this.ux_splitcontainerMain.TabIndex = 2;
            // 
            // ux_checkboxViewExistingAttachments
            // 
            this.ux_checkboxViewExistingAttachments.AutoSize = true;
            this.ux_checkboxViewExistingAttachments.Location = new System.Drawing.Point(6, 9);
            this.ux_checkboxViewExistingAttachments.Name = "ux_checkboxViewExistingAttachments";
            this.ux_checkboxViewExistingAttachments.Size = new System.Drawing.Size(150, 17);
            this.ux_checkboxViewExistingAttachments.TabIndex = 2;
            this.ux_checkboxViewExistingAttachments.Text = "View Existing Attachments";
            this.ux_checkboxViewExistingAttachments.UseVisualStyleBackColor = true;
            this.ux_checkboxViewExistingAttachments.CheckedChanged += new System.EventHandler(this.ux_checkboxViewExistingAttachments_CheckedChanged);
            // 
            // ux_treeviewList
            // 
            this.ux_treeviewList.AllowDrop = true;
            this.ux_treeviewList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_treeviewList.Location = new System.Drawing.Point(3, 32);
            this.ux_treeviewList.Name = "ux_treeviewList";
            this.ux_treeviewList.Size = new System.Drawing.Size(263, 610);
            this.ux_treeviewList.TabIndex = 0;
            this.ux_treeviewList.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.ux_treeviewList_AfterSelect);
            this.ux_treeviewList.DragDrop += new System.Windows.Forms.DragEventHandler(this.ux_treeviewList_DragDrop);
            this.ux_treeviewList.DragEnter += new System.Windows.Forms.DragEventHandler(this.ux_treeviewList_DragEnter);
            // 
            // ux_splitcontainerAttachmentDataDisplay
            // 
            this.ux_splitcontainerAttachmentDataDisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ux_splitcontainerAttachmentDataDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ux_splitcontainerAttachmentDataDisplay.Location = new System.Drawing.Point(0, 0);
            this.ux_splitcontainerAttachmentDataDisplay.Name = "ux_splitcontainerAttachmentDataDisplay";
            this.ux_splitcontainerAttachmentDataDisplay.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ux_splitcontainerAttachmentDataDisplay.Panel1
            // 
            this.ux_splitcontainerAttachmentDataDisplay.Panel1.Controls.Add(this.ux_groupboxView);
            this.ux_splitcontainerAttachmentDataDisplay.Panel1.Controls.Add(this.ux_listviewAttachments);
            this.ux_splitcontainerAttachmentDataDisplay.Panel1.Tag = "inventory_id";
            // 
            // ux_splitcontainerAttachmentDataDisplay.Panel2
            // 
            this.ux_splitcontainerAttachmentDataDisplay.Panel2.Controls.Add(this.ux_tabcontrolMain);
            this.ux_splitcontainerAttachmentDataDisplay.Size = new System.Drawing.Size(700, 649);
            this.ux_splitcontainerAttachmentDataDisplay.SplitterDistance = 362;
            this.ux_splitcontainerAttachmentDataDisplay.TabIndex = 0;
            // 
            // ux_groupboxView
            // 
            this.ux_groupboxView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxView.Controls.Add(this.ux_radiobuttonViewDetails);
            this.ux_groupboxView.Controls.Add(this.ux_radiobuttonViewTile);
            this.ux_groupboxView.Controls.Add(this.ux_radiobuttonViewList);
            this.ux_groupboxView.Controls.Add(this.ux_radiobuttonViewSmallIcon);
            this.ux_groupboxView.Controls.Add(this.ux_radiobuttonViewLargeIcon);
            this.ux_groupboxView.Location = new System.Drawing.Point(414, 1);
            this.ux_groupboxView.Name = "ux_groupboxView";
            this.ux_groupboxView.Size = new System.Drawing.Size(279, 32);
            this.ux_groupboxView.TabIndex = 10;
            this.ux_groupboxView.TabStop = false;
            this.ux_groupboxView.Text = "View";
            // 
            // ux_radiobuttonViewDetails
            // 
            this.ux_radiobuttonViewDetails.AutoSize = true;
            this.ux_radiobuttonViewDetails.Location = new System.Drawing.Point(220, 11);
            this.ux_radiobuttonViewDetails.Name = "ux_radiobuttonViewDetails";
            this.ux_radiobuttonViewDetails.Size = new System.Drawing.Size(57, 17);
            this.ux_radiobuttonViewDetails.TabIndex = 4;
            this.ux_radiobuttonViewDetails.Text = "Details";
            this.ux_radiobuttonViewDetails.UseVisualStyleBackColor = true;
            this.ux_radiobuttonViewDetails.CheckedChanged += new System.EventHandler(this.ux_radiobuttonView_CheckedChanged);
            // 
            // ux_radiobuttonViewTile
            // 
            this.ux_radiobuttonViewTile.AutoSize = true;
            this.ux_radiobuttonViewTile.Location = new System.Drawing.Point(173, 11);
            this.ux_radiobuttonViewTile.Name = "ux_radiobuttonViewTile";
            this.ux_radiobuttonViewTile.Size = new System.Drawing.Size(42, 17);
            this.ux_radiobuttonViewTile.TabIndex = 3;
            this.ux_radiobuttonViewTile.Text = "Tile";
            this.ux_radiobuttonViewTile.UseVisualStyleBackColor = true;
            this.ux_radiobuttonViewTile.CheckedChanged += new System.EventHandler(this.ux_radiobuttonView_CheckedChanged);
            // 
            // ux_radiobuttonViewList
            // 
            this.ux_radiobuttonViewList.AutoSize = true;
            this.ux_radiobuttonViewList.Location = new System.Drawing.Point(123, 11);
            this.ux_radiobuttonViewList.Name = "ux_radiobuttonViewList";
            this.ux_radiobuttonViewList.Size = new System.Drawing.Size(41, 17);
            this.ux_radiobuttonViewList.TabIndex = 2;
            this.ux_radiobuttonViewList.Text = "List";
            this.ux_radiobuttonViewList.UseVisualStyleBackColor = true;
            this.ux_radiobuttonViewList.CheckedChanged += new System.EventHandler(this.ux_radiobuttonView_CheckedChanged);
            // 
            // ux_radiobuttonViewSmallIcon
            // 
            this.ux_radiobuttonViewSmallIcon.AutoSize = true;
            this.ux_radiobuttonViewSmallIcon.Location = new System.Drawing.Point(67, 11);
            this.ux_radiobuttonViewSmallIcon.Name = "ux_radiobuttonViewSmallIcon";
            this.ux_radiobuttonViewSmallIcon.Size = new System.Drawing.Size(50, 17);
            this.ux_radiobuttonViewSmallIcon.TabIndex = 1;
            this.ux_radiobuttonViewSmallIcon.Text = "Small";
            this.ux_radiobuttonViewSmallIcon.UseVisualStyleBackColor = true;
            this.ux_radiobuttonViewSmallIcon.CheckedChanged += new System.EventHandler(this.ux_radiobuttonView_CheckedChanged);
            // 
            // ux_radiobuttonViewLargeIcon
            // 
            this.ux_radiobuttonViewLargeIcon.AutoSize = true;
            this.ux_radiobuttonViewLargeIcon.Checked = true;
            this.ux_radiobuttonViewLargeIcon.Location = new System.Drawing.Point(9, 11);
            this.ux_radiobuttonViewLargeIcon.Name = "ux_radiobuttonViewLargeIcon";
            this.ux_radiobuttonViewLargeIcon.Size = new System.Drawing.Size(52, 17);
            this.ux_radiobuttonViewLargeIcon.TabIndex = 0;
            this.ux_radiobuttonViewLargeIcon.TabStop = true;
            this.ux_radiobuttonViewLargeIcon.Tag = "";
            this.ux_radiobuttonViewLargeIcon.Text = "Large";
            this.ux_radiobuttonViewLargeIcon.UseVisualStyleBackColor = true;
            this.ux_radiobuttonViewLargeIcon.CheckedChanged += new System.EventHandler(this.ux_radiobuttonView_CheckedChanged);
            // 
            // ux_listviewAttachments
            // 
            this.ux_listviewAttachments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_listviewAttachments.HideSelection = false;
            this.ux_listviewAttachments.Location = new System.Drawing.Point(4, 35);
            this.ux_listviewAttachments.MultiSelect = false;
            this.ux_listviewAttachments.Name = "ux_listviewAttachments";
            this.ux_listviewAttachments.Size = new System.Drawing.Size(689, 320);
            this.ux_listviewAttachments.TabIndex = 0;
            this.ux_listviewAttachments.UseCompatibleStateImageBehavior = false;
            this.ux_listviewAttachments.ItemActivate += new System.EventHandler(this.ux_listviewAttachments_ItemActivate);
            this.ux_listviewAttachments.SelectedIndexChanged += new System.EventHandler(this.ux_listviewAttachments_SelectedIndexChanged);
            // 
            // ux_tabcontrolMain
            // 
            this.ux_tabcontrolMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_tabcontrolMain.Controls.Add(this.ux_tabpageFormView);
            this.ux_tabcontrolMain.Controls.Add(this.ux_tabpageGridView);
            this.ux_tabcontrolMain.Location = new System.Drawing.Point(3, 3);
            this.ux_tabcontrolMain.Name = "ux_tabcontrolMain";
            this.ux_tabcontrolMain.SelectedIndex = 0;
            this.ux_tabcontrolMain.Size = new System.Drawing.Size(690, 278);
            this.ux_tabcontrolMain.TabIndex = 0;
            // 
            // ux_tabpageFormView
            // 
            this.ux_tabpageFormView.Controls.Add(this.ux_panelFormView);
            this.ux_tabpageFormView.Location = new System.Drawing.Point(4, 22);
            this.ux_tabpageFormView.Name = "ux_tabpageFormView";
            this.ux_tabpageFormView.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tabpageFormView.Size = new System.Drawing.Size(682, 252);
            this.ux_tabpageFormView.TabIndex = 0;
            this.ux_tabpageFormView.Text = "Form View";
            this.ux_tabpageFormView.UseVisualStyleBackColor = true;
            // 
            // ux_panelFormView
            // 
            this.ux_panelFormView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_panelFormView.Controls.Add(this.ux_labelDescriptionCode);
            this.ux_panelFormView.Controls.Add(this.ux_comboboxDescriptionCode);
            this.ux_panelFormView.Controls.Add(this.ux_textboxDescription);
            this.ux_panelFormView.Controls.Add(this.ux_textboxCopyrightInformation);
            this.ux_panelFormView.Controls.Add(this.ux_labelDescription);
            this.ux_panelFormView.Controls.Add(this.ux_textboxTitle);
            this.ux_panelFormView.Controls.Add(this.ux_labelTitle);
            this.ux_panelFormView.Controls.Add(this.ux_labelSortOrder);
            this.ux_panelFormView.Controls.Add(this.ux_labelAttachCooperator);
            this.ux_panelFormView.Controls.Add(this.ux_textboxSortOrder);
            this.ux_panelFormView.Controls.Add(this.ux_textboxAttachCooperator);
            this.ux_panelFormView.Controls.Add(this.ux_labelNote);
            this.ux_panelFormView.Controls.Add(this.ux_comboboxAttachDateCode);
            this.ux_panelFormView.Controls.Add(this.ux_textboxNote);
            this.ux_panelFormView.Controls.Add(this.ux_labelAttachDateCode);
            this.ux_panelFormView.Controls.Add(this.ux_labelCopyrightInformation);
            this.ux_panelFormView.Controls.Add(this.ux_textboxAttachDate);
            this.ux_panelFormView.Controls.Add(this.ux_checkboxIsWebVisible);
            this.ux_panelFormView.Controls.Add(this.ux_labelAttachDate);
            this.ux_panelFormView.Controls.Add(this.ux_labelCategoryCode);
            this.ux_panelFormView.Controls.Add(this.ux_textboxContentType);
            this.ux_panelFormView.Controls.Add(this.ux_comboboxCategoryCode);
            this.ux_panelFormView.Controls.Add(this.ux_labelContentType);
            this.ux_panelFormView.Location = new System.Drawing.Point(0, 0);
            this.ux_panelFormView.Name = "ux_panelFormView";
            this.ux_panelFormView.Size = new System.Drawing.Size(695, 253);
            this.ux_panelFormView.TabIndex = 1;
            // 
            // ux_labelDescriptionCode
            // 
            this.ux_labelDescriptionCode.AutoSize = true;
            this.ux_labelDescriptionCode.Location = new System.Drawing.Point(3, 68);
            this.ux_labelDescriptionCode.Name = "ux_labelDescriptionCode";
            this.ux_labelDescriptionCode.Size = new System.Drawing.Size(88, 13);
            this.ux_labelDescriptionCode.TabIndex = 115;
            this.ux_labelDescriptionCode.Tag = "description_code";
            this.ux_labelDescriptionCode.Text = "Description Code";
            // 
            // ux_comboboxDescriptionCode
            // 
            this.ux_comboboxDescriptionCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboboxDescriptionCode.FormattingEnabled = true;
            this.ux_comboboxDescriptionCode.Location = new System.Drawing.Point(6, 84);
            this.ux_comboboxDescriptionCode.Name = "ux_comboboxDescriptionCode";
            this.ux_comboboxDescriptionCode.Size = new System.Drawing.Size(158, 21);
            this.ux_comboboxDescriptionCode.TabIndex = 114;
            this.ux_comboboxDescriptionCode.Tag = "description_code";
            // 
            // ux_textboxDescription
            // 
            this.ux_textboxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxDescription.Location = new System.Drawing.Point(233, 22);
            this.ux_textboxDescription.Multiline = true;
            this.ux_textboxDescription.Name = "ux_textboxDescription";
            this.ux_textboxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxDescription.Size = new System.Drawing.Size(351, 43);
            this.ux_textboxDescription.TabIndex = 112;
            this.ux_textboxDescription.Tag = "description";
            // 
            // ux_textboxCopyrightInformation
            // 
            this.ux_textboxCopyrightInformation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_textboxCopyrightInformation.Location = new System.Drawing.Point(6, 125);
            this.ux_textboxCopyrightInformation.Multiline = true;
            this.ux_textboxCopyrightInformation.Name = "ux_textboxCopyrightInformation";
            this.ux_textboxCopyrightInformation.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxCopyrightInformation.Size = new System.Drawing.Size(325, 80);
            this.ux_textboxCopyrightInformation.TabIndex = 31;
            this.ux_textboxCopyrightInformation.Tag = "copyright_information";
            // 
            // ux_labelDescription
            // 
            this.ux_labelDescription.AutoSize = true;
            this.ux_labelDescription.Location = new System.Drawing.Point(230, 6);
            this.ux_labelDescription.Name = "ux_labelDescription";
            this.ux_labelDescription.Size = new System.Drawing.Size(60, 13);
            this.ux_labelDescription.TabIndex = 113;
            this.ux_labelDescription.Tag = "description";
            this.ux_labelDescription.Text = "Description";
            // 
            // ux_textboxTitle
            // 
            this.ux_textboxTitle.Location = new System.Drawing.Point(6, 22);
            this.ux_textboxTitle.Multiline = true;
            this.ux_textboxTitle.Name = "ux_textboxTitle";
            this.ux_textboxTitle.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxTitle.Size = new System.Drawing.Size(219, 43);
            this.ux_textboxTitle.TabIndex = 110;
            this.ux_textboxTitle.Tag = "title";
            // 
            // ux_labelTitle
            // 
            this.ux_labelTitle.AutoSize = true;
            this.ux_labelTitle.Location = new System.Drawing.Point(3, 6);
            this.ux_labelTitle.Name = "ux_labelTitle";
            this.ux_labelTitle.Size = new System.Drawing.Size(27, 13);
            this.ux_labelTitle.TabIndex = 111;
            this.ux_labelTitle.Tag = "title";
            this.ux_labelTitle.Text = "Title";
            // 
            // ux_labelSortOrder
            // 
            this.ux_labelSortOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_labelSortOrder.AutoSize = true;
            this.ux_labelSortOrder.Location = new System.Drawing.Point(590, 6);
            this.ux_labelSortOrder.Name = "ux_labelSortOrder";
            this.ux_labelSortOrder.Size = new System.Drawing.Size(55, 13);
            this.ux_labelSortOrder.TabIndex = 28;
            this.ux_labelSortOrder.Tag = "sort_order";
            this.ux_labelSortOrder.Text = "Sort Order";
            // 
            // ux_labelAttachCooperator
            // 
            this.ux_labelAttachCooperator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_labelAttachCooperator.AutoSize = true;
            this.ux_labelAttachCooperator.Location = new System.Drawing.Point(337, 208);
            this.ux_labelAttachCooperator.Name = "ux_labelAttachCooperator";
            this.ux_labelAttachCooperator.Size = new System.Drawing.Size(93, 13);
            this.ux_labelAttachCooperator.TabIndex = 109;
            this.ux_labelAttachCooperator.Tag = "attach_cooperator_id";
            this.ux_labelAttachCooperator.Text = "Attach Cooperator";
            // 
            // ux_textboxSortOrder
            // 
            this.ux_textboxSortOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxSortOrder.Location = new System.Drawing.Point(593, 22);
            this.ux_textboxSortOrder.Name = "ux_textboxSortOrder";
            this.ux_textboxSortOrder.Size = new System.Drawing.Size(92, 20);
            this.ux_textboxSortOrder.TabIndex = 27;
            this.ux_textboxSortOrder.Tag = "sort_order";
            // 
            // ux_textboxAttachCooperator
            // 
            this.ux_textboxAttachCooperator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxAttachCooperator.Location = new System.Drawing.Point(340, 223);
            this.ux_textboxAttachCooperator.Name = "ux_textboxAttachCooperator";
            this.ux_textboxAttachCooperator.Size = new System.Drawing.Size(345, 20);
            this.ux_textboxAttachCooperator.TabIndex = 108;
            this.ux_textboxAttachCooperator.Tag = "attach_cooperator_id";
            // 
            // ux_labelNote
            // 
            this.ux_labelNote.AutoSize = true;
            this.ux_labelNote.Location = new System.Drawing.Point(337, 109);
            this.ux_labelNote.Name = "ux_labelNote";
            this.ux_labelNote.Size = new System.Drawing.Size(35, 13);
            this.ux_labelNote.TabIndex = 30;
            this.ux_labelNote.Tag = "note";
            this.ux_labelNote.Text = "Notes";
            // 
            // ux_comboboxAttachDateCode
            // 
            this.ux_comboboxAttachDateCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_comboboxAttachDateCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboboxAttachDateCode.FormattingEnabled = true;
            this.ux_comboboxAttachDateCode.Location = new System.Drawing.Point(157, 223);
            this.ux_comboboxAttachDateCode.Name = "ux_comboboxAttachDateCode";
            this.ux_comboboxAttachDateCode.Size = new System.Drawing.Size(174, 21);
            this.ux_comboboxAttachDateCode.TabIndex = 106;
            this.ux_comboboxAttachDateCode.Tag = "attach_date_code";
            // 
            // ux_textboxNote
            // 
            this.ux_textboxNote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxNote.Location = new System.Drawing.Point(340, 125);
            this.ux_textboxNote.Multiline = true;
            this.ux_textboxNote.Name = "ux_textboxNote";
            this.ux_textboxNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ux_textboxNote.Size = new System.Drawing.Size(345, 80);
            this.ux_textboxNote.TabIndex = 29;
            this.ux_textboxNote.Tag = "note";
            // 
            // ux_labelAttachDateCode
            // 
            this.ux_labelAttachDateCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_labelAttachDateCode.AutoSize = true;
            this.ux_labelAttachDateCode.Location = new System.Drawing.Point(154, 207);
            this.ux_labelAttachDateCode.Name = "ux_labelAttachDateCode";
            this.ux_labelAttachDateCode.Size = new System.Drawing.Size(92, 13);
            this.ux_labelAttachDateCode.TabIndex = 107;
            this.ux_labelAttachDateCode.Tag = "attach_date_code";
            this.ux_labelAttachDateCode.Text = "Attach Date Code";
            // 
            // ux_labelCopyrightInformation
            // 
            this.ux_labelCopyrightInformation.AutoSize = true;
            this.ux_labelCopyrightInformation.Location = new System.Drawing.Point(3, 109);
            this.ux_labelCopyrightInformation.Name = "ux_labelCopyrightInformation";
            this.ux_labelCopyrightInformation.Size = new System.Drawing.Size(106, 13);
            this.ux_labelCopyrightInformation.TabIndex = 32;
            this.ux_labelCopyrightInformation.Tag = "copyright_information";
            this.ux_labelCopyrightInformation.Text = "Copyright Information";
            // 
            // ux_textboxAttachDate
            // 
            this.ux_textboxAttachDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_textboxAttachDate.Location = new System.Drawing.Point(6, 223);
            this.ux_textboxAttachDate.Name = "ux_textboxAttachDate";
            this.ux_textboxAttachDate.Size = new System.Drawing.Size(145, 20);
            this.ux_textboxAttachDate.TabIndex = 38;
            this.ux_textboxAttachDate.Tag = "attach_date";
            // 
            // ux_checkboxIsWebVisible
            // 
            this.ux_checkboxIsWebVisible.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_checkboxIsWebVisible.AutoSize = true;
            this.ux_checkboxIsWebVisible.Location = new System.Drawing.Point(593, 48);
            this.ux_checkboxIsWebVisible.Name = "ux_checkboxIsWebVisible";
            this.ux_checkboxIsWebVisible.Size = new System.Drawing.Size(99, 17);
            this.ux_checkboxIsWebVisible.TabIndex = 33;
            this.ux_checkboxIsWebVisible.TabStop = false;
            this.ux_checkboxIsWebVisible.Tag = "is_web_visible";
            this.ux_checkboxIsWebVisible.Text = "Is Web Visible?";
            this.ux_checkboxIsWebVisible.UseVisualStyleBackColor = true;
            // 
            // ux_labelAttachDate
            // 
            this.ux_labelAttachDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_labelAttachDate.AutoSize = true;
            this.ux_labelAttachDate.Location = new System.Drawing.Point(6, 207);
            this.ux_labelAttachDate.Name = "ux_labelAttachDate";
            this.ux_labelAttachDate.Size = new System.Drawing.Size(64, 13);
            this.ux_labelAttachDate.TabIndex = 39;
            this.ux_labelAttachDate.Tag = "attach_date";
            this.ux_labelAttachDate.Text = "Attach Date";
            // 
            // ux_labelCategoryCode
            // 
            this.ux_labelCategoryCode.AutoSize = true;
            this.ux_labelCategoryCode.Location = new System.Drawing.Point(170, 69);
            this.ux_labelCategoryCode.Name = "ux_labelCategoryCode";
            this.ux_labelCategoryCode.Size = new System.Drawing.Size(77, 13);
            this.ux_labelCategoryCode.TabIndex = 35;
            this.ux_labelCategoryCode.Tag = "category_code";
            this.ux_labelCategoryCode.Text = "Category Code";
            // 
            // ux_textboxContentType
            // 
            this.ux_textboxContentType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxContentType.Location = new System.Drawing.Point(340, 85);
            this.ux_textboxContentType.Name = "ux_textboxContentType";
            this.ux_textboxContentType.Size = new System.Drawing.Size(345, 20);
            this.ux_textboxContentType.TabIndex = 36;
            this.ux_textboxContentType.Tag = "content_type";
            // 
            // ux_comboboxCategoryCode
            // 
            this.ux_comboboxCategoryCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboboxCategoryCode.FormattingEnabled = true;
            this.ux_comboboxCategoryCode.Location = new System.Drawing.Point(173, 84);
            this.ux_comboboxCategoryCode.Name = "ux_comboboxCategoryCode";
            this.ux_comboboxCategoryCode.Size = new System.Drawing.Size(158, 21);
            this.ux_comboboxCategoryCode.TabIndex = 34;
            this.ux_comboboxCategoryCode.Tag = "category_code";
            // 
            // ux_labelContentType
            // 
            this.ux_labelContentType.AutoSize = true;
            this.ux_labelContentType.Location = new System.Drawing.Point(337, 70);
            this.ux_labelContentType.Name = "ux_labelContentType";
            this.ux_labelContentType.Size = new System.Drawing.Size(71, 13);
            this.ux_labelContentType.TabIndex = 37;
            this.ux_labelContentType.Tag = "content_type";
            this.ux_labelContentType.Text = "Content Type";
            // 
            // ux_tabpageGridView
            // 
            this.ux_tabpageGridView.Controls.Add(this.ux_datagridviewAttachments);
            this.ux_tabpageGridView.Location = new System.Drawing.Point(4, 22);
            this.ux_tabpageGridView.Name = "ux_tabpageGridView";
            this.ux_tabpageGridView.Padding = new System.Windows.Forms.Padding(3);
            this.ux_tabpageGridView.Size = new System.Drawing.Size(678, 251);
            this.ux_tabpageGridView.TabIndex = 1;
            this.ux_tabpageGridView.Text = "Grid View";
            this.ux_tabpageGridView.UseVisualStyleBackColor = true;
            // 
            // ux_openfiledialog
            // 
            this.ux_openfiledialog.Filter = "All files (*.*)|*.*";
            // 
            // ux_buttonSave
            // 
            this.ux_buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSave.Location = new System.Drawing.Point(786, 29);
            this.ux_buttonSave.Name = "ux_buttonSave";
            this.ux_buttonSave.Size = new System.Drawing.Size(90, 23);
            this.ux_buttonSave.TabIndex = 6;
            this.ux_buttonSave.Text = "Save";
            this.ux_buttonSave.UseVisualStyleBackColor = true;
            this.ux_buttonSave.Click += new System.EventHandler(this.ux_buttonSave_Click);
            // 
            // ux_buttonSaveAndExit
            // 
            this.ux_buttonSaveAndExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSaveAndExit.Location = new System.Drawing.Point(882, 29);
            this.ux_buttonSaveAndExit.Name = "ux_buttonSaveAndExit";
            this.ux_buttonSaveAndExit.Size = new System.Drawing.Size(90, 23);
            this.ux_buttonSaveAndExit.TabIndex = 7;
            this.ux_buttonSaveAndExit.Text = "Save and Exit";
            this.ux_buttonSaveAndExit.UseVisualStyleBackColor = true;
            this.ux_buttonSaveAndExit.Click += new System.EventHandler(this.ux_buttonSaveAndExit_Click);
            // 
            // ux_menustripMain
            // 
            this.ux_menustripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.ux_menustripMain.Location = new System.Drawing.Point(0, 0);
            this.ux_menustripMain.Name = "ux_menustripMain";
            this.ux_menustripMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.ux_menustripMain.Size = new System.Drawing.Size(984, 24);
            this.ux_menustripMain.TabIndex = 8;
            this.ux_menustripMain.Text = "View";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.optionsToolStripMenuItem.Text = "Options...";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // ux_statusstripMain
            // 
            this.ux_statusstripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ux_statusProgressDescription,
            this.ux_statusProgressBar,
            this.ux_statusProgressValue});
            this.ux_statusstripMain.Location = new System.Drawing.Point(0, 739);
            this.ux_statusstripMain.Name = "ux_statusstripMain";
            this.ux_statusstripMain.Size = new System.Drawing.Size(984, 22);
            this.ux_statusstripMain.TabIndex = 9;
            this.ux_statusstripMain.Text = "statusStrip1";
            // 
            // ux_statusProgressDescription
            // 
            this.ux_statusProgressDescription.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.ux_statusProgressDescription.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
            this.ux_statusProgressDescription.Name = "ux_statusProgressDescription";
            this.ux_statusProgressDescription.Size = new System.Drawing.Size(745, 19);
            this.ux_statusProgressDescription.Spring = true;
            this.ux_statusProgressDescription.Text = "toolStripStatusLabel1";
            this.ux_statusProgressDescription.Visible = false;
            // 
            // ux_statusProgressBar
            // 
            this.ux_statusProgressBar.Name = "ux_statusProgressBar";
            this.ux_statusProgressBar.Size = new System.Drawing.Size(100, 18);
            this.ux_statusProgressBar.Visible = false;
            // 
            // ux_statusProgressValue
            // 
            this.ux_statusProgressValue.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.ux_statusProgressValue.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
            this.ux_statusProgressValue.Name = "ux_statusProgressValue";
            this.ux_statusProgressValue.Size = new System.Drawing.Size(122, 19);
            this.ux_statusProgressValue.Text = "toolStripStatusLabel2";
            this.ux_statusProgressValue.Visible = false;
            // 
            // ux_groupboxAttachmentType
            // 
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonInventory);
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonTaxonomySpecies);
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonTaxonomyGenus);
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonTaxonomyFamily);
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonOrderRequest);
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonMethod);
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonCropTraitCode);
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonCropTrait);
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonCrop);
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonAccessionInventoryGroup);
            this.ux_groupboxAttachmentType.Controls.Add(this.ux_radiobuttonAccession);
            this.ux_groupboxAttachmentType.Location = new System.Drawing.Point(9, 23);
            this.ux_groupboxAttachmentType.Name = "ux_groupboxAttachmentType";
            this.ux_groupboxAttachmentType.Size = new System.Drawing.Size(669, 58);
            this.ux_groupboxAttachmentType.TabIndex = 10;
            this.ux_groupboxAttachmentType.TabStop = false;
            this.ux_groupboxAttachmentType.Text = "Attachment Type";
            // 
            // ux_radiobuttonInventory
            // 
            this.ux_radiobuttonInventory.AutoSize = true;
            this.ux_radiobuttonInventory.Location = new System.Drawing.Point(129, 12);
            this.ux_radiobuttonInventory.Name = "ux_radiobuttonInventory";
            this.ux_radiobuttonInventory.Size = new System.Drawing.Size(69, 17);
            this.ux_radiobuttonInventory.TabIndex = 10;
            this.ux_radiobuttonInventory.Tag = ":inventoryid  ; get_accession_inv_attach ; inventory_id";
            this.ux_radiobuttonInventory.Text = "Inventory";
            this.ux_radiobuttonInventory.UseVisualStyleBackColor = true;
            this.ux_radiobuttonInventory.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // ux_radiobuttonTaxonomySpecies
            // 
            this.ux_radiobuttonTaxonomySpecies.AutoSize = true;
            this.ux_radiobuttonTaxonomySpecies.Location = new System.Drawing.Point(538, 35);
            this.ux_radiobuttonTaxonomySpecies.Name = "ux_radiobuttonTaxonomySpecies";
            this.ux_radiobuttonTaxonomySpecies.Size = new System.Drawing.Size(115, 17);
            this.ux_radiobuttonTaxonomySpecies.TabIndex = 9;
            this.ux_radiobuttonTaxonomySpecies.Tag = ":taxonomyspeciesid ; get_taxonomy_attach ; taxonomy_species_id";
            this.ux_radiobuttonTaxonomySpecies.Text = "Taxonomy Species";
            this.ux_radiobuttonTaxonomySpecies.UseVisualStyleBackColor = true;
            this.ux_radiobuttonTaxonomySpecies.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // ux_radiobuttonTaxonomyGenus
            // 
            this.ux_radiobuttonTaxonomyGenus.AutoSize = true;
            this.ux_radiobuttonTaxonomyGenus.Location = new System.Drawing.Point(410, 35);
            this.ux_radiobuttonTaxonomyGenus.Name = "ux_radiobuttonTaxonomyGenus";
            this.ux_radiobuttonTaxonomyGenus.Size = new System.Drawing.Size(108, 17);
            this.ux_radiobuttonTaxonomyGenus.TabIndex = 8;
            this.ux_radiobuttonTaxonomyGenus.Tag = ":taxonomygenusid ; get_taxonomy_attach ; taxonomy_genus_id";
            this.ux_radiobuttonTaxonomyGenus.Text = "Taxonomy Genus";
            this.ux_radiobuttonTaxonomyGenus.UseVisualStyleBackColor = true;
            this.ux_radiobuttonTaxonomyGenus.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // ux_radiobuttonTaxonomyFamily
            // 
            this.ux_radiobuttonTaxonomyFamily.AutoSize = true;
            this.ux_radiobuttonTaxonomyFamily.Location = new System.Drawing.Point(284, 35);
            this.ux_radiobuttonTaxonomyFamily.Name = "ux_radiobuttonTaxonomyFamily";
            this.ux_radiobuttonTaxonomyFamily.Size = new System.Drawing.Size(106, 17);
            this.ux_radiobuttonTaxonomyFamily.TabIndex = 7;
            this.ux_radiobuttonTaxonomyFamily.Tag = ":taxonomyfamilyid ; get_taxonomy_attach ; taxonomy_family_id";
            this.ux_radiobuttonTaxonomyFamily.Text = "Taxonomy Family";
            this.ux_radiobuttonTaxonomyFamily.UseVisualStyleBackColor = true;
            this.ux_radiobuttonTaxonomyFamily.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // ux_radiobuttonOrderRequest
            // 
            this.ux_radiobuttonOrderRequest.AutoSize = true;
            this.ux_radiobuttonOrderRequest.Location = new System.Drawing.Point(449, 12);
            this.ux_radiobuttonOrderRequest.Name = "ux_radiobuttonOrderRequest";
            this.ux_radiobuttonOrderRequest.Size = new System.Drawing.Size(94, 17);
            this.ux_radiobuttonOrderRequest.TabIndex = 6;
            this.ux_radiobuttonOrderRequest.Tag = ":orderrequestid ; get_order_request_attach ; order_request_id";
            this.ux_radiobuttonOrderRequest.Text = "Order Request";
            this.ux_radiobuttonOrderRequest.UseVisualStyleBackColor = true;
            this.ux_radiobuttonOrderRequest.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // ux_radiobuttonMethod
            // 
            this.ux_radiobuttonMethod.AutoSize = true;
            this.ux_radiobuttonMethod.Location = new System.Drawing.Point(591, 12);
            this.ux_radiobuttonMethod.Name = "ux_radiobuttonMethod";
            this.ux_radiobuttonMethod.Size = new System.Drawing.Size(61, 17);
            this.ux_radiobuttonMethod.TabIndex = 5;
            this.ux_radiobuttonMethod.Tag = ":methodid ; get_method_attach ; method_id";
            this.ux_radiobuttonMethod.Text = "Method";
            this.ux_radiobuttonMethod.UseVisualStyleBackColor = true;
            this.ux_radiobuttonMethod.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // ux_radiobuttonCropTraitCode
            // 
            this.ux_radiobuttonCropTraitCode.AutoSize = true;
            this.ux_radiobuttonCropTraitCode.Location = new System.Drawing.Point(165, 35);
            this.ux_radiobuttonCropTraitCode.Name = "ux_radiobuttonCropTraitCode";
            this.ux_radiobuttonCropTraitCode.Size = new System.Drawing.Size(99, 17);
            this.ux_radiobuttonCropTraitCode.TabIndex = 4;
            this.ux_radiobuttonCropTraitCode.Tag = ":croptraitcodeid ; get_crop_trait_code_attach ; crop_trait_code_id";
            this.ux_radiobuttonCropTraitCode.Text = "Crop Trait Code";
            this.ux_radiobuttonCropTraitCode.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCropTraitCode.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // ux_radiobuttonCropTrait
            // 
            this.ux_radiobuttonCropTrait.AutoSize = true;
            this.ux_radiobuttonCropTrait.Location = new System.Drawing.Point(74, 35);
            this.ux_radiobuttonCropTrait.Name = "ux_radiobuttonCropTrait";
            this.ux_radiobuttonCropTrait.Size = new System.Drawing.Size(71, 17);
            this.ux_radiobuttonCropTrait.TabIndex = 3;
            this.ux_radiobuttonCropTrait.Tag = ":croptraitid ; get_crop_trait_attach ; crop_trait_id";
            this.ux_radiobuttonCropTrait.Text = "Crop Trait";
            this.ux_radiobuttonCropTrait.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCropTrait.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // ux_radiobuttonCrop
            // 
            this.ux_radiobuttonCrop.AutoSize = true;
            this.ux_radiobuttonCrop.Location = new System.Drawing.Point(7, 35);
            this.ux_radiobuttonCrop.Name = "ux_radiobuttonCrop";
            this.ux_radiobuttonCrop.Size = new System.Drawing.Size(47, 17);
            this.ux_radiobuttonCrop.TabIndex = 2;
            this.ux_radiobuttonCrop.Tag = ":cropid ; get_crop_attach ; crop_id";
            this.ux_radiobuttonCrop.Text = "Crop";
            this.ux_radiobuttonCrop.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCrop.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // ux_radiobuttonAccessionInventoryGroup
            // 
            this.ux_radiobuttonAccessionInventoryGroup.AutoSize = true;
            this.ux_radiobuttonAccessionInventoryGroup.Location = new System.Drawing.Point(246, 12);
            this.ux_radiobuttonAccessionInventoryGroup.Name = "ux_radiobuttonAccessionInventoryGroup";
            this.ux_radiobuttonAccessionInventoryGroup.Size = new System.Drawing.Size(155, 17);
            this.ux_radiobuttonAccessionInventoryGroup.TabIndex = 1;
            this.ux_radiobuttonAccessionInventoryGroup.Tag = ":accessioninvgroupid ; get_accession_inv_group_attach ; accession_inv_group_id";
            this.ux_radiobuttonAccessionInventoryGroup.Text = "Accession/Inventory Group";
            this.ux_radiobuttonAccessionInventoryGroup.UseVisualStyleBackColor = true;
            this.ux_radiobuttonAccessionInventoryGroup.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // ux_radiobuttonAccession
            // 
            this.ux_radiobuttonAccession.AutoSize = true;
            this.ux_radiobuttonAccession.Location = new System.Drawing.Point(7, 12);
            this.ux_radiobuttonAccession.Name = "ux_radiobuttonAccession";
            this.ux_radiobuttonAccession.Size = new System.Drawing.Size(74, 17);
            this.ux_radiobuttonAccession.TabIndex = 0;
            this.ux_radiobuttonAccession.Tag = ":accessionid  ; get_accession_inv_attach ; inventory_id";
            this.ux_radiobuttonAccession.Text = "Accession";
            this.ux_radiobuttonAccession.UseVisualStyleBackColor = true;
            this.ux_radiobuttonAccession.CheckedChanged += new System.EventHandler(this.ux_radiobuttonAttachmentType_CheckedChanged);
            // 
            // AttachmentWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 761);
            this.Controls.Add(this.ux_groupboxAttachmentType);
            this.Controls.Add(this.ux_statusstripMain);
            this.Controls.Add(this.ux_buttonSaveAndExit);
            this.Controls.Add(this.ux_buttonSave);
            this.Controls.Add(this.ux_splitcontainerMain);
            this.Controls.Add(this.ux_menustripMain);
            this.MainMenuStrip = this.ux_menustripMain;
            this.Name = "AttachmentWizard";
            this.Text = "Attachment Wizard";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AttachmentWizard_FormClosing);
            this.Load += new System.EventHandler(this.AttachmentWizard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewAttachments)).EndInit();
            this.ux_splitcontainerMain.Panel1.ResumeLayout(false);
            this.ux_splitcontainerMain.Panel1.PerformLayout();
            this.ux_splitcontainerMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerMain)).EndInit();
            this.ux_splitcontainerMain.ResumeLayout(false);
            this.ux_splitcontainerAttachmentDataDisplay.Panel1.ResumeLayout(false);
            this.ux_splitcontainerAttachmentDataDisplay.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ux_splitcontainerAttachmentDataDisplay)).EndInit();
            this.ux_splitcontainerAttachmentDataDisplay.ResumeLayout(false);
            this.ux_groupboxView.ResumeLayout(false);
            this.ux_groupboxView.PerformLayout();
            this.ux_tabcontrolMain.ResumeLayout(false);
            this.ux_tabpageFormView.ResumeLayout(false);
            this.ux_panelFormView.ResumeLayout(false);
            this.ux_panelFormView.PerformLayout();
            this.ux_tabpageGridView.ResumeLayout(false);
            this.ux_menustripMain.ResumeLayout(false);
            this.ux_menustripMain.PerformLayout();
            this.ux_statusstripMain.ResumeLayout(false);
            this.ux_statusstripMain.PerformLayout();
            this.ux_groupboxAttachmentType.ResumeLayout(false);
            this.ux_groupboxAttachmentType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView ux_datagridviewAttachments;
        private System.Windows.Forms.Button ux_buttonBatchFiles;
        private System.Windows.Forms.SplitContainer ux_splitcontainerMain;
        private System.Windows.Forms.SplitContainer ux_splitcontainerAttachmentDataDisplay;
        private System.Windows.Forms.OpenFileDialog ux_openfiledialog;
        private System.Windows.Forms.TabControl ux_tabcontrolMain;
        private System.Windows.Forms.TabPage ux_tabpageFormView;
        private System.Windows.Forms.TabPage ux_tabpageGridView;
        private System.Windows.Forms.TreeView ux_treeviewList;
        private System.Windows.Forms.ListView ux_listviewAttachments;
        private System.Windows.Forms.TextBox ux_textboxSortOrder;
        private System.Windows.Forms.Label ux_labelSortOrder;
        private System.Windows.Forms.TextBox ux_textboxCopyrightInformation;
        private System.Windows.Forms.Label ux_labelCopyrightInformation;
        private System.Windows.Forms.TextBox ux_textboxNote;
        private System.Windows.Forms.Label ux_labelNote;
        private System.Windows.Forms.CheckBox ux_checkboxIsWebVisible;
        private System.Windows.Forms.ComboBox ux_comboboxCategoryCode;
        private System.Windows.Forms.Label ux_labelCategoryCode;
        private System.Windows.Forms.TextBox ux_textboxContentType;
        private System.Windows.Forms.Label ux_labelContentType;
        private System.Windows.Forms.TextBox ux_textboxAttachDate;
        private System.Windows.Forms.Label ux_labelAttachDate;
        private System.Windows.Forms.ComboBox ux_comboboxAttachDateCode;
        private System.Windows.Forms.Label ux_labelAttachDateCode;
        private System.Windows.Forms.TextBox ux_textboxDescription;
        private System.Windows.Forms.Label ux_labelDescription;
        private System.Windows.Forms.TextBox ux_textboxTitle;
        private System.Windows.Forms.Label ux_labelTitle;
        private System.Windows.Forms.Label ux_labelAttachCooperator;
        private System.Windows.Forms.TextBox ux_textboxAttachCooperator;
        private System.Windows.Forms.Panel ux_panelFormView;
        private System.Windows.Forms.Button ux_buttonSave;
        private System.Windows.Forms.Button ux_buttonSaveAndExit;
        private System.Windows.Forms.MenuStrip ux_menustripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.CheckBox ux_checkboxViewExistingAttachments;
        private System.Windows.Forms.Label ux_labelDescriptionCode;
        private System.Windows.Forms.ComboBox ux_comboboxDescriptionCode;
        private StatusStrip ux_statusstripMain;
        private ToolStripStatusLabel ux_statusProgressDescription;
        private ToolStripProgressBar ux_statusProgressBar;
        private ToolStripStatusLabel ux_statusProgressValue;
        private GroupBox ux_groupboxView;
        private RadioButton ux_radiobuttonViewDetails;
        private RadioButton ux_radiobuttonViewTile;
        private RadioButton ux_radiobuttonViewList;
        private RadioButton ux_radiobuttonViewSmallIcon;
        private RadioButton ux_radiobuttonViewLargeIcon;
        private GroupBox ux_groupboxAttachmentType;
        private RadioButton ux_radiobuttonTaxonomyFamily;
        private RadioButton ux_radiobuttonOrderRequest;
        private RadioButton ux_radiobuttonMethod;
        private RadioButton ux_radiobuttonCropTraitCode;
        private RadioButton ux_radiobuttonCropTrait;
        private RadioButton ux_radiobuttonCrop;
        private RadioButton ux_radiobuttonAccessionInventoryGroup;
        private RadioButton ux_radiobuttonAccession;
        private RadioButton ux_radiobuttonTaxonomySpecies;
        private RadioButton ux_radiobuttonTaxonomyGenus;
        private RadioButton ux_radiobuttonInventory;
    }
}

