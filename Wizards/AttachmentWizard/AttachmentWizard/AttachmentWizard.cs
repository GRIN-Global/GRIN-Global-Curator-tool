﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GRINGlobal.Client.Common;

namespace AttachmentWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class AttachmentWizard : Form, IGRINGlobalDataWizard
    {
        char _lastDGVCharPressed;
        string _originalPKeys = "";
        string _previousPKeys = "";
        string _currentPKeys = "";
        string _previousFolderPathOrFileNames = "";
        string _currentFolderPathOrFileNames = "";
        string _attachParameterName = ":inventoryid";
        string _attachParentPKeyName = "inventory_id";
        string _attachDataviewName = "get_accession_inv_attach";
        SharedUtils _sharedUtils;
        bool _enableHotSync = true;

        // Filename Parsing Options...
        string _includeFileFilter = "*.jpg; *.jpeg; *.png; *.gif; *.xls; *.xlsx; *.doc; *.docx; *.ppt; *.pptx; *.pdf; *.txt; *.rtf; *.zip";
        string _excludedFileFilter = "*.tif; *.tiff; *_t.jpg; *_t.jpeg; *.exe; *.dll";
        string _parseMethod = "SmartParse";
        bool _useCaseSensitiveFileNames = false;
        bool _ignoreFolderNames = false;
        char[] _delimiters = new char[2] { '_', ' ' };
        int[] _fixedFieldTokenLengths = new int[12];

        // SmartParse default parameters...
        string _allowedFormTypeCode = " ** BA BD BL CA CL CM CT DN EA ER FI FR GS HE HS IO IV LA LV MF MI MS PA PD PF PL PO PR RH RN RT SC SD SG SP ST TC TU VI ";
        string _allowedDescriptionCode = " Achenes Bark Branch Bud Bulb Cladode Collection_Site Ear Field Flower Fruit Greenhouse Head Kernel Leaves Miscellaneous Nut Panicle Plant Pod Root Seed Seedling Shoot Silique Spike Spine Stalk Stem Stipule Tendril Umbel Vegetative ";
        string _allowedInventoryPrefix = " Ames PI ";
        string _supportedImageExtensions = "";

        // Attachment info...
        Dictionary<string, int> _attachmentFilesDictionary = new Dictionary<string, int>();
        Dictionary<string, int> _dragAndDropAttachmentFilesDictionary = new Dictionary<string, int>();
        ImageList _treeviewImageList;
        DataSet _dsAttach = null;
        DataTable _attachTable = null;
        BindingSource _attachBindingSource = null;
        ImageList _largeImageList = null;
        ImageList _smallImageList32 = null;
        ImageList _smallImageList64 = null;

        public AttachmentWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            // Copy the parameters into private class variables...
            _originalPKeys = pKeys;  // ex: ":accessionid=1453504,1445409; :inventoryid=; :orderrequestid=; :cooperatorid=; :geographyid=; :taxonomygenusid=; :cropid="
            _previousPKeys = "";
            _previousFolderPathOrFileNames = "";
            _sharedUtils = sharedUtils;

            _currentPKeys = LoadPKeyList(_originalPKeys);
            _currentFolderPathOrFileNames = LoadFilePathsParameterList(_originalPKeys);
        }
        public string FormName
        {
            get
            {
                return "Attachment Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();

                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "";
            }
        }

        private void AttachmentWizard_Load(object sender, EventArgs e)
        {
            int PKey = -1;

            _attachBindingSource = new BindingSource();
            //_attachBindingSource.ListChanged += new ListChangedEventHandler(_attachBindingSource_ListChanged);
            //_attachBindingSource.CurrentChanged += new EventHandler(_attachBindingSource_CurrentChanged);

            // Load the form_type_codes from the local LUT database...
            string localDBInventoryFormTypeCodes = LoadFormTypeCodes();
            if (!string.IsNullOrEmpty(localDBInventoryFormTypeCodes.Trim())) _allowedFormTypeCode = localDBInventoryFormTypeCodes;

            // Load the inventory prefixes from the remote database...
            string remoteDBInventoryPrefix = LoadInventoryPrefix();
            if (!string.IsNullOrEmpty(remoteDBInventoryPrefix.Trim())) _allowedInventoryPrefix = remoteDBInventoryPrefix;

            // Load the attach_description_codes from the remote database...
            string remoteDBDescriptionCodes = LoadDescriptionCodes();
            if (!string.IsNullOrEmpty(remoteDBDescriptionCodes.Trim())) _allowedDescriptionCode = remoteDBDescriptionCodes;

            // Load .NET Image object supported file extensions...
            string supportedImageExtensions = "";
            foreach (System.Drawing.Imaging.ImageCodecInfo imageCodec in System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders())
            {
                supportedImageExtensions += imageCodec.FilenameExtension.ToLower() + ";";
            }
            if (!string.IsNullOrEmpty(supportedImageExtensions.Trim())) _supportedImageExtensions += supportedImageExtensions;

            // Load Attachment Wizard user setting...
            _includeFileFilter = _sharedUtils.GetUserSetting(this.Name, "_includeFileFilter", "Value", _includeFileFilter);
            _excludedFileFilter = _sharedUtils.GetUserSetting(this.Name, "_excludedFileFilter", "Value", _excludedFileFilter);
            _parseMethod = _sharedUtils.GetUserSetting(this.Name, "_parseMethod", "Value", _parseMethod);
            bool caseSensitiveFileNames = false;
            if (bool.TryParse(_sharedUtils.GetUserSetting(this.Name, "_useCaseSensitiveFileNames", "Value", _useCaseSensitiveFileNames.ToString()), out caseSensitiveFileNames))
            {
                _useCaseSensitiveFileNames = caseSensitiveFileNames;
            }
            bool ignoreFolderNames = false;
            if (bool.TryParse(_sharedUtils.GetUserSetting(this.Name, "_ignoreFolderNames", "Value", _ignoreFolderNames.ToString()), out ignoreFolderNames))
            {
                _ignoreFolderNames = ignoreFolderNames;
            }
            _delimiters = _sharedUtils.GetUserSetting(this.Name, "_delimiters", "Value", new string(_delimiters)).ToString().ToCharArray();
            string[] fixedFieldTokenLengths = _sharedUtils.GetUserSetting(this.Name, "_fixedFieldTokenLengths", "Value", "").Split(' ');
            for (int i = 0; i < 12; i++)
            {
                int tokenLength = 0;
                if (i < fixedFieldTokenLengths.Length &&
                    int.TryParse(fixedFieldTokenLengths[i], out tokenLength))
                {
                    _fixedFieldTokenLengths[i] = tokenLength;
                }
                else
                {
                    _fixedFieldTokenLengths[i] = 0;
                }
            }

            // Determine the _pKeyType, _parameterType, and _dataviewName from the incoming parameters passed into this class (if one was passed)
            // using the radio buttons in the attachment wizard main screen and ensure the matching radio button is checked...
            if (System.Text.RegularExpressions.Regex.IsMatch(_currentPKeys.Trim(), @"^:.*id="))
            {
                string currentParameterType = _currentPKeys.Split('=')[0].Trim().ToLower();
                string currentPKeys = _currentPKeys.Split('=')[1].Trim();
                foreach (Control ctrl in ux_groupboxAttachmentType.Controls)
                {
                    // Find the radio button that matches the incoming parameter list and check it...
                    if (ctrl.GetType() == typeof(RadioButton) &&
                        ctrl.Tag != null &&
                        ctrl.Tag.GetType() == typeof(string))
                    {
                        // Reset the radio button to unchecked...
                        ((RadioButton)ctrl).Checked = false;
                        string[] controlTokens = ((string)ctrl.Tag).Split(';');
                        if (controlTokens.Length == 3 &&
                            controlTokens[0].Trim().ToLower().Contains(currentParameterType))
                        {
                            // We found a match between the incoming PKeys type and radio button - so check it now...
                            // (NOTE: The radio button check-changed event handler will set the _attachPKeyType, _attachParameterType,
                            // and _attachDataviewName global variables)
                            ((RadioButton)ctrl).Checked = true;
                            break;
                        }
                        else if(controlTokens.Length == 3 &&
                            controlTokens[1].Trim().ToLower().Replace("get_", "").Replace("_", "") == currentParameterType.Trim().Replace(":", "").Replace("id", ""))
                        {
                            // We found a match between the incoming PKeys type and radio button - so check it now...
                            // (NOTE: The radio button check-changed event handler will set the _attachPKeyType, _attachParameterType,
                            // and _attachDataviewName global variables)
                            ((RadioButton)ctrl).Checked = true;
                            ux_checkboxViewExistingAttachments.Checked = true;
                            break;
                        }
                    }
                }
                // By now a radio button should be checked but if the global parameters are still empty a radio button
                // was not checked - so check the accession/inv radio button as the default fail-safe choice...
                if (string.IsNullOrEmpty(_attachParentPKeyName) ||
                    string.IsNullOrEmpty(_attachParameterName) ||
                    string.IsNullOrEmpty(_attachDataviewName))
                {
                    ux_radiobuttonInventory.Checked = true;
                }

                // NOTE: if this wizard was launched by a drag/drop operation in the CT treeview there should only be one PKey in the incoming parameter list
                if (_originalPKeys.Trim().ToLower().Contains(":filepaths=") &&
                    !string.IsNullOrEmpty(_currentPKeys) &&
                    _currentPKeys.Split('=').Length == 2 &&
                    !string.IsNullOrEmpty(_currentFolderPathOrFileNames))
                {
                    // Extract the parent PKEY from _currentPKeys global variable that was populated during the
                    // Attachment Wizard creation...
                    if (int.TryParse(_currentPKeys.Split('=')[1].Trim(), out PKey))
                    {
                        foreach (string fullPath in _currentFolderPathOrFileNames.Split(','))
                        {
                            if(!_dragAndDropAttachmentFilesDictionary.ContainsKey(fullPath.Trim())) _dragAndDropAttachmentFilesDictionary.Add(fullPath.Trim(), PKey);
                        }
                    }
                }
           }

            RefreshData();

            // Bind and format the controls on the panel...
            bindControls(ux_panelFormView.Controls, _attachBindingSource);
            formatControls(ux_panelFormView.Controls, _attachBindingSource);

            // Populate the View enums for the ListView control's View property...
            ux_radiobuttonViewLargeIcon.Tag = View.LargeIcon;
            ux_radiobuttonViewDetails.Tag = View.Details;
            ux_radiobuttonViewSmallIcon.Tag = View.SmallIcon;
            ux_radiobuttonViewList.Tag = View.List;
            ux_radiobuttonViewTile.Tag = View.Tile;

            // Wireup the event handlers for Foreign Key columns...
            ux_datagridviewAttachments.CellFormatting += new DataGridViewCellFormattingEventHandler(ux_datagridview_CellFormatting);
            ux_datagridviewAttachments.CellEndEdit += new DataGridViewCellEventHandler(ux_datagridview_CellEndEdit);
            ux_datagridviewAttachments.DataError += new DataGridViewDataErrorEventHandler(ux_datagridview_DataError);
            ux_datagridviewAttachments.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(ux_datagridview_EditingControlShowing);
            ux_datagridviewAttachments.PreviewKeyDown += new PreviewKeyDownEventHandler(ux_datagridview_PreviewKeyDown);
            ux_datagridviewAttachments.KeyDown += new KeyEventHandler(ux_datagridview_KeyDown);

            // Get language translations for the components and controls in this applicataion...
            if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
        }

        private void AttachmentWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Save local user setttings...
            //Properties.Settings.Default.Save();

            // Check to see if there are unsaved attachment changes and if so
            // ask the user if they would like to save them before exiting the wizard...
            if (_attachTable.GetChanges() != null)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to abandon your edits and exit?", "Cancel Edits", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "AttachmentWizard_FormClosingMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = _attachTable.GetChanges().Rows.Count.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                if (DialogResult.No == ggMessageBox.ShowDialog())
                {
                    e.Cancel = true;
                }
            }
        }

        private int SaveAttachmentData()
        {
            DataSet attachChanges = new DataSet();
            DataSet attachResults = new DataSet();
            int errorCount = 0;
            string remotePath = "";

            // Make sure that any edits pending have been committed to the datatable
            // prior to getting changes...
            _attachBindingSource.EndEdit();

            if (_attachTable.GetChanges() != null)
            {
                // Make sure all edits to the _inventoryAttach table have been commited...
                _attachBindingSource.EndEdit();

                // Show the user that something is being done...
                UpdateProgressBar(true, 0, 4, 1, "Processing database records...");
                attachChanges.Tables.Add(_attachTable.GetChanges());
                ScrubData(attachChanges);
                // Save the changes to the remote server...
                UpdateProgressBar(true, 0, 4, 2, "Processing database records...");
                attachResults = _sharedUtils.SaveWebServiceData(attachChanges);
                UpdateProgressBar(true, 0, 4, 3, "Processing database records...");
                if (attachResults.Tables.Contains(_attachTable.TableName))
                {
                    errorCount += SyncSavedResults(_attachTable, attachResults.Tables[_attachTable.TableName]);
                }
                UpdateProgressBar(false, 0, 4, 0, "Done processing database records...");

                Dictionary<string, string> imageLoadDictionary = new Dictionary<string, string>();
                foreach (DataRow dr in attachResults.Tables[_attachTable.TableName].Rows)
                {
                    if (dr["SavedAction"].ToString().ToUpper() != "DELETE" &&
                        dr["SavedStatus"].ToString().ToUpper() == "SUCCESS" &&
                        !string.IsNullOrEmpty(dr["virtual_path"].ToString()))
                    {
                        // Get local file name and full path...
                        string localFilePath = "";
                        string remoteFilePath = dr["virtual_path"].ToString();
                        string[] tokens = dr["virtual_path"].ToString().Split('/', '\\', ':');
                        string remoteFileName = tokens[tokens.Length - 1];
                        foreach (string key in _attachmentFilesDictionary.Keys)
                        {
                            if (key.Contains(remoteFileName) &&
                                _attachmentFilesDictionary[key] == (int)dr[_attachParentPKeyName])
                            {
                                // Found the dictionary entry that matches the filename and the inventory_id...
                                localFilePath = key;
                                break;
                            }
                        }

                        // If the local filename and path was found in the dictionary 
                        // and the record was either inserted or the virtual path was changed then
                        // the image needs to be loaded to the server - so add it to the dictionary list...
                        if (!string.IsNullOrEmpty(localFilePath))
                        {
                            imageLoadDictionary.Add(localFilePath, dr["virtual_path"].ToString());
                            //imageLoadDictionary.Add(localFilePath, "AIA/Zea/9/1445409/FakeAttachmentForPI_550473.pdf");
                        }
                    }
                }

                int i = 0;
                UpdateProgressBar(true, 0, imageLoadDictionary.Count, i, "Uploading image files...");
                foreach (string localFilePath in imageLoadDictionary.Keys)
                {
                    i++;
                    UpdateProgressBar(true, 0, imageLoadDictionary.Count, i, "Uploading image files...");
                    try
                    {
                        byte[] imageBytes = System.IO.File.ReadAllBytes(localFilePath);
                        // Attempt to upload the image to the remote server...
                        if (imageBytes != null)
                        {
                            System.IO.FileInfo fi = new System.IO.FileInfo(localFilePath);
                            if (_supportedImageExtensions.ToLower().Contains(fi.Extension.ToLower()))
                            {
                                remotePath = _sharedUtils.SaveAttachment(imageLoadDictionary[localFilePath], imageBytes, true, true);
                            }
                            else
                            {
                                remotePath = _sharedUtils.SaveAttachment(imageLoadDictionary[localFilePath], imageBytes, false, true);
                            }
                            // Check the return string to make sure that the file name and/or path for the attachment file on the server
                            // has been returned (and if not a server error has occured and should be reported to the enduser)...
                            if (!remotePath.Contains(fi.Name))
                            {
                                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The attempt to upload the attachment file has returned a warning message from the server that should be reviewed.\n\nError Message:\n {0}", "Attachment Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                ggMessageBox.Name = "AttachmentWizard_SaveInventoryAttachmentData";
                                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                                string[] argsArray = new string[100];
                                argsArray[0] = remotePath;
                                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                                ggMessageBox.ShowDialog();
                            }
                        }
                    }
                    catch (Exception)
                    {
                        // This attachment must be a web browser URL so do nothing...
                    }
                }
                UpdateProgressBar(false, 0, imageLoadDictionary.Count, 0, "Done uploading image files...");
            }
            return errorCount;
        }

        private void ScrubData(DataSet ds)
        {
            // Make sure all non-nullable fields do not contain a null value - if they do, replace it with the default value...
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr.RowState != DataRowState.Deleted)
                    {
                        foreach (DataColumn dc in dt.Columns)
                        {
                            if (dc.ExtendedProperties.Contains("is_nullable") &&
                                dc.ExtendedProperties["is_nullable"].ToString().Trim().ToUpper() == "N" &&
                                dr[dc] == DBNull.Value)
                            {
                                if (dc.ExtendedProperties.Contains("default_value") &&
                                    !string.IsNullOrEmpty(dc.ExtendedProperties["default_value"].ToString()) &&
                                    dc.ExtendedProperties["default_value"].ToString().Trim().ToUpper() != "{DBNULL.VALUE}")
                                {
                                    dr[dc] = dc.ExtendedProperties["default_value"].ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        private int SyncSavedResults(DataTable originalTable, DataTable savedResults)
        {
            int errorCount = 0;

            // The middle tier will ignore 'changed records' that have no 'real' changes by not putting them in the saved results datatable
            // so we will 'pretend' that the MT returned 'success' for saving an unchanged record...
            foreach (DataRow dr in originalTable.GetChanges().Rows)
            {
                if (dr.RowState == DataRowState.Modified &&
                    !savedResults.Rows.Contains(dr[originalTable.PrimaryKey[0].ColumnName]))
                {
                    DataRow newDR = savedResults.NewRow();
                    newDR.ItemArray = dr.ItemArray;
                    newDR["OriginalPrimaryKeyID"] = dr[originalTable.PrimaryKey[0].ColumnName];
                    newDR["NewPrimaryKeyID"] = dr[originalTable.PrimaryKey[0].ColumnName];
                    newDR["SavedAction"] = "Update";
                    newDR["SavedStatus"] = "Success";
                    newDR["ExceptionMessage"] = "";
                    savedResults.Rows.Add(newDR);
                }
            }
            savedResults.AcceptChanges();

            if (savedResults != null && savedResults.PrimaryKey.Length == 1)
            {
                string pKeyCol = savedResults.PrimaryKey[0].ColumnName.Trim().ToUpper();
                savedResults.Columns[pKeyCol].ReadOnly = false;
                foreach (DataRow dr in savedResults.Rows)
                {
                    DataRow originalRow = originalTable.Rows.Find(dr["OriginalPrimaryKeyID"]);

                    switch (dr["SavedAction"].ToString())
                    {
                        case "Insert":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the originalTable row's status for this new row to committed (and update the pkey with the int returned from the server DB)...
                                if (originalRow != null)
                                {
                                    bool origColumnReadOnlyValue = originalRow.Table.Columns[pKeyCol].ReadOnly;
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = false;
                                    originalRow[pKeyCol] = dr["NewPrimaryKeyID"];
                                    originalRow.AcceptChanges();
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = origColumnReadOnlyValue;
                                    originalRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Update":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                originalRow.AcceptChanges();
                                originalRow.ClearErrors();
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Delete":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                string deleteAttachmentVirtualPath = "";
                                string deleteAttachmentThumbnailVirtualPath = "";
                                // Set the row's status for this deleted row to committed...
                                if (originalRow != null)
                                {
                                    if (originalRow.Table.Columns.Contains("virtual_path")) deleteAttachmentVirtualPath = originalRow["virtual_path"].ToString().Trim();
                                    if (originalRow.Table.Columns.Contains("thumbnail_virtual_path")) deleteAttachmentThumbnailVirtualPath = originalRow["thumbnail_virtual_path"].ToString().Trim();
                                    originalRow.AcceptChanges();
                                    originalRow.ClearErrors();
                                }
                                else
                                {
                                    DataRow deletedRow = null;
                                    foreach (DataRow deleteddr in originalTable.Rows)
                                    {
                                        if (deleteddr.RowState == DataRowState.Deleted && deleteddr[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                        {
                                            if (deleteddr.Table.Columns.Contains("virtual_path")) deleteAttachmentVirtualPath = deleteddr["virtual_path", DataRowVersion.Original].ToString().Trim();
                                            if (deleteddr.Table.Columns.Contains("thumbnail_virtual_path")) deleteAttachmentThumbnailVirtualPath = deleteddr["thumbnail_virtual_path", DataRowVersion.Original].ToString().Trim();
                                            deletedRow = deleteddr;
                                        }
                                    }
                                    deletedRow.AcceptChanges();
                                    deletedRow.ClearErrors();
                                }
                                // Now that the record has been deleted the attachment should be deleted too...
                                if (!string.IsNullOrEmpty(deleteAttachmentVirtualPath)) _sharedUtils.DeleteAttachment(deleteAttachmentVirtualPath);
                                if (!string.IsNullOrEmpty(deleteAttachmentThumbnailVirtualPath)) _sharedUtils.DeleteAttachment(deleteAttachmentThumbnailVirtualPath);
                            }
                            else
                            {
                                errorCount++;
                                // Find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in originalTable.Rows)
                                {
                                    if (deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        deletedRow.RejectChanges();
                                        deletedRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            return errorCount;
        }

        private void UpdateProgressBar(bool visible, int min, int max, int progressVal, string description)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            ux_statusstripMain.Visible = visible;
            ux_statusProgressBar.Visible = visible;
            ux_statusProgressValue.Visible = visible;
            ux_statusProgressDescription.Visible = visible;
            ux_statusProgressBar.Minimum = min;
            ux_statusProgressBar.Maximum = max;
            ux_statusProgressBar.Value = progressVal;
            ux_statusProgressValue.Text = (progressVal * 100 / (Math.Max(max - min, 1))).ToString() + " %";
            //ux_statusProgressValue.Update();
            ux_statusProgressDescription.Text = description;
            //ux_statusProgressDescription.Update();
            ux_statusstripMain.Update();

            Application.DoEvents();

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void ux_buttonSave_Click(object sender, EventArgs e)
        {
            int errorCount = SaveAttachmentData();

            if (errorCount == 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Attachment Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "AttachmentWizard_ux_buttonSave1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();

                // Get a fresh copy of the data and rebuild the interface (in case some attachments were deleted)...
                _currentFolderPathOrFileNames = "";
                _previousPKeys = "";
                _previousFolderPathOrFileNames = "";
                RefreshData();
            }
            else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed in the Grid View tab.\n\n  Error Count: {0}", "Attachment Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "AttachmentWizard_ux_buttonSave2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }
        }

        private void ux_buttonSaveAndExit_Click(object sender, EventArgs e)
        {
            int errorCount = SaveAttachmentData();

            if (errorCount == 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Attachment Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "AttachmentWizard_ux_buttonSave1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();

                // Close the wizard now...
                this.Close();
            }
            else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\nWould you like to review them now?\n\nClick Yes to review the errors now.\n(Click No to abandon the errors and exit the Order Wizard).\n\n  Error Count: {0}", "Attachment Wizard Data Save Results", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "AttachmentWizard_ux_buttonSave2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();

                // There were errors in saving the data so let the user decide if the wizard should be closed...
                if (DialogResult.No == ggMessageBox.ShowDialog())
                {
                    this.Close();
                }
                else
                {
                    // Do nothing (stay in the wizard)...
                }
            }
        }

        private void ux_checkboxViewExistingAttachments_CheckedChanged(object sender, EventArgs e)
        {
            _previousPKeys = "";
            _previousFolderPathOrFileNames = "";
            RefreshData();
        }

        private void ux_buttonBatchFiles_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            //ofd.DefaultExt = "*.*";
            ofd.Multiselect = true;
            ofd.RestoreDirectory = false;
            //ofd.Title = "Batch File";
            ofd.FileName = "Folder Selection";
            ofd.CheckPathExists = true;
            ofd.ShowReadOnly = false;
            ofd.ReadOnlyChecked = true;
            ofd.CheckFileExists = false;
            ofd.ValidateNames = false;
            if (DialogResult.OK == ofd.ShowDialog())
            {
                // Check to see if the user selected a folder or 
                // a group of files...
                if (ofd.FileName.Contains("Folder Selection"))
                {
                    _currentFolderPathOrFileNames = ofd.FileName.Replace("Folder Selection", "");
                }
                else
                {
                    string selectedFiles = "";
                    foreach (string fileName in ofd.FileNames)
                    {
                        selectedFiles += fileName + ",";
                    }
                    _currentFolderPathOrFileNames = selectedFiles.Trim(',');
                }
                _currentPKeys = _attachParameterName + "=";
                // Hotsyncing the listview with the dgv is not a good idea so suspend it by
                // resetting the currentcell in the dgv...
                ux_datagridviewAttachments.CurrentCell = null;
                RefreshData();
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Options optionsDialog = new Options();
            optionsDialog.IncludeFilter = _includeFileFilter;
            optionsDialog.ExcludeFilter = _excludedFileFilter;
            optionsDialog.ParseMethod = _parseMethod;
            optionsDialog.CaseSensitive =_useCaseSensitiveFileNames;
            optionsDialog.IgnoreFolderNames =_ignoreFolderNames;
            optionsDialog.Delimiters =_delimiters;
            optionsDialog.FixedFieldTokenLengths = _fixedFieldTokenLengths;
            if (DialogResult.OK == optionsDialog.ShowDialog())
            {
                _includeFileFilter = optionsDialog.IncludeFilter;
                _excludedFileFilter = optionsDialog.ExcludeFilter;
                _parseMethod = optionsDialog.ParseMethod;
                _useCaseSensitiveFileNames = optionsDialog.CaseSensitive;
                _ignoreFolderNames = optionsDialog.IgnoreFolderNames;
                _delimiters = optionsDialog.Delimiters;
                _fixedFieldTokenLengths = optionsDialog.FixedFieldTokenLengths;

                // Save these setting to the server...
                // _sharedUtils.SaveUserSetting("", ux_checkboxIgnoreCase.Name, "Checked", ux_checkboxIgnoreCase.Checked.ToString());
                _sharedUtils.SaveUserSetting(this.Name, "_includeFileFilter", "Value", _includeFileFilter);
                _sharedUtils.SaveUserSetting(this.Name, "_excludedFileFilter", "Value", _excludedFileFilter);
                _sharedUtils.SaveUserSetting(this.Name, "_parseMethod", "Value", _parseMethod);
                _sharedUtils.SaveUserSetting(this.Name, "_useCaseSensitiveFileNames", "Value", _useCaseSensitiveFileNames.ToString());
                _sharedUtils.SaveUserSetting(this.Name, "_ignoreFolderNames", "Value", _ignoreFolderNames.ToString());
                _sharedUtils.SaveUserSetting(this.Name, "_delimiters", "Value", new string(_delimiters).Trim());
                string fixedFieldTokens = "";
                for(int i = 0; i<12; i++)
                {
                    fixedFieldTokens += _fixedFieldTokenLengths[i] + " ";
                }
                _sharedUtils.SaveUserSetting(this.Name, "_fixedFieldTokenLengths", "Value", fixedFieldTokens);
            }
        }

        #region ListView control logic...

        private void ux_listviewAttachments_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;

            // More than one attachment image can be selected in the listview
            // so update the current attachment record to coincide with the first
            // attachment selected...
            if (_enableHotSync && 
                lv.SelectedIndices != null &&
                lv.SelectedIndices.Count > 0)
            {
                _attachBindingSource.Position = lv.SelectedIndices[0];
                if (ux_datagridviewAttachments.CurrentCell != null &&
                    ux_datagridviewAttachments.CurrentCell.RowIndex != _attachBindingSource.Position)
                {
                    ux_datagridviewAttachments.CurrentCell = ux_datagridviewAttachments[ux_datagridviewAttachments.CurrentCell.ColumnIndex, _attachBindingSource.Position];
                }
            }
        }

        private void ux_listviewAttachments_ItemActivate(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;
            foreach (int i in lv.SelectedIndices)
            {
                string attachmentRemotePath = _attachTable.DefaultView[i]["virtual_path"].ToString();
                if (_attachTable.DefaultView[i]["category_code"].ToString().ToUpper() == "LINK")
                {
                    // This attachment is a link to data on another system so start the default
                    // browser with the URL virtual path...
                    try
                    {
                        System.Diagnostics.Process.Start(attachmentRemotePath);
                    }
                    catch (Exception)
                    {
                    }
                }
                else
                {
                    // This attachment is a file stored on a server that needs to be downloaded so
                    // download it now and display it in the application with this file extension support...
                    if (!Uri.IsWellFormedUriString(attachmentRemotePath, UriKind.Absolute)) attachmentRemotePath = "~/uploads/images/" + attachmentRemotePath;
                    //System.IO.FileInfo fi = new System.IO.FileInfo(attachmentRemotePath);
                    byte[] attachBytes = _sharedUtils.GetAttachment(attachmentRemotePath);
                    if (attachBytes != null && attachBytes.Length > 0)
                    {
                        string attachmentTempPath = System.IO.Path.GetTempFileName();  //System.Environment.GetFolderPath(Environment..SpecialFolder.InternetCache) + @"\GRIN-Global\Curator Tool";
                        System.IO.File.WriteAllBytes(attachmentTempPath, attachBytes);

                        if (System.IO.File.Exists(attachmentTempPath) &&
                            !string.IsNullOrEmpty(System.IO.Path.GetExtension(attachmentRemotePath)))
                        {
                            System.IO.File.Move(attachmentTempPath, attachmentTempPath.Replace(".tmp", System.IO.Path.GetExtension(attachmentRemotePath)));
                        }

                        System.Diagnostics.Process.Start(attachmentTempPath.Replace(".tmp", System.IO.Path.GetExtension(attachmentRemotePath)));
                    }
                }
            }

        }
        #endregion

        #region TreeView control logic...

        private void ux_treeviewList_DragEnter(object sender, DragEventArgs e)
        {
            // Okay we are in the middle of a Drag and Drop operation and the mouse is in 
            // the treeview control so lets handle this event...

            // Convert the mouse coordinates from screen to client...
            Point ptClientCoord = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));
            // Get the node closest to the mouse cursor (to make sure it is a folder)...
            TreeNode tnClosestToMouse = ((TreeView)sender).GetNodeAt(ptClientCoord);

            // Is this a collection of dataset rows being dragged to a node...
            if (e.Data.GetDataPresent("System.Data.DataSet"))
            {
                e.Effect = DragDropEffects.Copy;
            }
            // Is this an attachment being dragged in to the Attachment Wizard treeview...
            else if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else if(e.Data.GetDataPresent(DataFormats.Text))
            {
                string dndText = (string)e.Data.GetData(DataFormats.Text);
                TreeNode destinationNode = ((TreeView)sender).GetNodeAt(((TreeView)sender).PointToClient(new Point(e.X, e.Y)));
                if (Uri.IsWellFormedUriString(dndText, UriKind.Absolute))
                {
                    e.Effect = DragDropEffects.Copy;
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ux_treeviewList_DragDrop(object sender, DragEventArgs e)
        {
            // The drag-drop event is coming to a close - process this event to handle the dropping of
            // data into the treeview...

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Convert the mouse coordinates from screen to client...
            Point ptClientCoord = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));
            string[] formats = e.Data.GetFormats();

            // Is this a collection of dataset rows being dragged to a node...
            if (e.Data.GetDataPresent("System.Data.DataSet") && e.Effect != DragDropEffects.None)
            {
                DataSet dndData = (DataSet)e.Data.GetData("System.Data.DataSet");

                if (dndData != null &&
                    dndData.Tables.Count > 0)
                {
                    DataTable dt = dndData.Tables[0];
                    string parentPKeyName = "";
                    string parameterPKeys = "";
                    DataSet ds;
                    string seQuery = "";
                    if (dt.PrimaryKey.Length > 0) parentPKeyName = dt.PrimaryKey[0].ColumnName.ToLower();
                    switch (parentPKeyName)
                    {
                        case "accession_inv_group_id":
                        case "crop_id":
                        case "crop_trait_id":
                        case "crop_trait_code_id":
                        case "method_id":
                        case "order_request_id":
                        case "taxonomy_family_id":
                        case "taxonomy_genus_id":
                        case "taxonomy_species_id":
                            // Build the PKey parameter string to use in the RefreshData method...
                            parameterPKeys = ":" + parentPKeyName.ToLower().Replace("_", "").Trim() + "=";
                            foreach (DataRow dr in dt.Rows)
                            {
                                parameterPKeys += dr[parentPKeyName].ToString() + ",";
                            }
                            parameterPKeys = parameterPKeys.TrimEnd(',');
                            break;
                        case "inventory_id":
                            // The inventory PKeys will need to be converted to the '**' system inventory
                            // if the Accession radio button is checked...
                            if (ux_radiobuttonAccession.Checked)
                            {
                                // First find the collection of accessions that are the parents of the inventory records...
                                seQuery = "@inventory.inventory_id IN (";
                                foreach (DataRow dr in dt.Rows)
                                {
                                    seQuery += dr["inventory_id"].ToString() + ",";
                                }
                                seQuery = seQuery.Trim(',') + ")";
                                ds = _sharedUtils.SearchWebService(seQuery, true, true, null, "accession", 0, 0);
                                // Next find the collection of '**' inventory records matching the accession found above...
                                seQuery = "@inventory.accession_id IN (";
                                if (ds.Tables.Contains("SearchResult"))
                                {
                                    // Build a list of order_request_ids to use for gathering the order_request_items...
                                    foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                                    {
                                        seQuery += dr["ID"].ToString() + ",";
                                    }
                                    seQuery = seQuery.TrimEnd(',') + ") AND @inventory.form_type_code='**'";
                                }
                                ds = _sharedUtils.SearchWebService(seQuery, true, true, null, "inventory", 0, 0);
                                parameterPKeys = ":inventoryid=";
                                if (ds.Tables.Contains("SearchResult"))
                                {
                                    // Build a list of inventory_ids to use for refreshing the data later on...
                                    foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                                    {
                                        parameterPKeys += dr["ID"].ToString() + ",";
                                    }
                                    parameterPKeys = parameterPKeys.TrimEnd(',');
                                }
                            }
                            else
                            {
                                parameterPKeys = ":inventoryid=";
                                foreach (DataRow dr in dt.Rows)
                                {
                                    parameterPKeys += dr["inventory_id"].ToString() + ",";
                                }
                                parameterPKeys = parameterPKeys.TrimEnd(',');
                            }
                            break;
                        case "accession_id":
                            // The accession PKeys need to be converted to inventory PKeys so
                            // build the Search Engine query to find all inventory PKeys that match
                            // the list of accession PKeys...
                            seQuery = "@inventory.accession_id IN (";
                            foreach (DataRow dr in dt.Rows)
                            {
                                seQuery += dr[parentPKeyName].ToString() + ",";
                            }
                            seQuery = seQuery.Trim(',') + ")";
                            if (ux_radiobuttonAccession.Checked) seQuery += " AND @inventory.form_type_code='**'";
                            ds = _sharedUtils.SearchWebService(seQuery, true, true, null, "inventory", 0, 0);
                            // Build the PKey (inventory_id) parameter string to use in the RefreshData method...
                            parameterPKeys = ":inventoryid=";
                            if (ds.Tables.Contains("SearchResult"))
                            {
                                // Build a list of order_request_ids to use for gathering the order_request_items...
                                foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                                {
                                    parameterPKeys += dr["ID"].ToString() + ",";
                                }
                                parameterPKeys = parameterPKeys.TrimEnd(',');
                            }
                            break;
                        default:
                            break;
                    }

                    // Merge the new drag and drop nodes with the existing ones...
                    string currentPKeyParameterName = _currentPKeys.Trim().Split('=')[0];
                    string parameterPKeysName = parameterPKeys.Trim().Split('=')[0];
                    if (currentPKeyParameterName == parameterPKeysName)
                    {
                        _currentPKeys += parameterPKeys.Replace(parameterPKeysName + "=", ",").TrimEnd(',');
                    }
                    else
                    {
                        _currentPKeys = parameterPKeys.TrimEnd(',');
                    }
                    _previousPKeys = "";
                    _previousFolderPathOrFileNames = "";
                    RefreshData();
                }
            }
            else if (e.Data.GetDataPresent("FileDrop") && e.Effect != DragDropEffects.None)
            {
                string[] fullPaths = (string[])e.Data.GetData(DataFormats.FileDrop);
                TreeNode destinationNode = ((TreeView)sender).GetNodeAt(((TreeView)sender).PointToClient(new Point(e.X, e.Y)));

                if (destinationNode != null)
                {
                    // Get the PKey for the node...
                    int PKey = -1;
                    if (destinationNode.Tag.ToString().ToUpper().StartsWith("FOLDER"))
                    {
                        PKey = int.Parse(destinationNode.Name);
                    }
                    else if (destinationNode.Tag.ToString().ToUpper().StartsWith("ATTACHMENT") &&
                        destinationNode.Parent != null &&
                        destinationNode.Parent.Tag.ToString().ToUpper().StartsWith("FOLDER"))
                    {
                        PKey = int.Parse(destinationNode.Parent.Name);
                    }

                    // Process each filepath in the collection...
                    foreach (string fullPath in fullPaths)
                    {
                        // Remember the association between file and PKey specified by user...
                        if (!_dragAndDropAttachmentFilesDictionary.ContainsKey(fullPath))
                        {
                            _dragAndDropAttachmentFilesDictionary.Add(fullPath, PKey);
                        }
                        // Concatenate each fullpath into a single string used when processing RefreshData()...
                        if (System.IO.File.Exists(fullPath) ||
                            System.IO.Directory.Exists(fullPath))
                        {
                            if (!_currentFolderPathOrFileNames.Contains(fullPath)) _currentFolderPathOrFileNames += "; " + fullPath;
                        }
                    }

                    // Reload image, dictionary, and attachment data based on updated current IDs and updated current Paths global variables...
                    _previousPKeys = "";
                    _previousFolderPathOrFileNames = "";
                    RefreshData();
                }
            }
            else if (e.Data.GetDataPresent(DataFormats.Text))
            {
                string dndText = (string)e.Data.GetData(DataFormats.Text);
                TreeNode destinationNode = ((TreeView)sender).GetNodeAt(((TreeView)sender).PointToClient(new Point(e.X, e.Y)));

                if (Uri.IsWellFormedUriString(dndText, UriKind.Absolute) &&
                    destinationNode != null)
                {
                    // Get the PKey for the node...
                    int PKey = -1;
                    if (destinationNode.Tag.ToString().ToUpper().StartsWith("FOLDER"))
                    {
                        PKey = int.Parse(destinationNode.Name);
                    }
                    else if (destinationNode.Tag.ToString().ToUpper().StartsWith("ATTACHMENT") &&
                        destinationNode.Parent != null &&
                        destinationNode.Parent.Tag.ToString().ToUpper().StartsWith("FOLDER"))
                    {
                        PKey = int.Parse(destinationNode.Parent.Name);
                    }

                    // Remember the association between the URL and PKey specified by user...
                    if (!_dragAndDropAttachmentFilesDictionary.ContainsKey(dndText))
                    {
                        _dragAndDropAttachmentFilesDictionary.Add(dndText, PKey);
                    }

                    // Concatenate the URL into the string used when processing RefreshData()...
                    if (!_currentFolderPathOrFileNames.Contains(dndText))
                    {
                        _currentFolderPathOrFileNames += "; " + dndText;
                    }

                    // Reload image, dictionary, and attachment data based on updated current IDs and updated current Paths global variables...
                    _previousPKeys = "";
                    _previousFolderPathOrFileNames = "";
                    RefreshData();
                }
            }


            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void ux_treeviewList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeView tv = (TreeView)sender;
            TreeNode tn = tv.SelectedNode;
            if (tn.Tag.ToString().ToUpper() == "ROOTFOLDER")
            {
                _attachTable.DefaultView.RowFilter = "";
            }
            else if (tn.Tag.ToString().ToUpper() == "FOLDER")
            {
                if (_attachTable.Columns.Contains(_attachParentPKeyName)) _attachTable.DefaultView.RowFilter = _attachParentPKeyName + " = " + tn.Name;
            }
            else if (tn.Tag.ToString().ToUpper() == "ATTACHMENT")
            {
                _attachTable.DefaultView.RowFilter = _attachTable.PrimaryKey[0].ColumnName + " = " + tn.Name;
            }

            if (!ux_checkboxViewExistingAttachments.Checked)
            {
                if (string.IsNullOrEmpty(_attachTable.DefaultView.RowFilter))
                {
                    _attachTable.DefaultView.RowFilter = _attachTable.PrimaryKey[0].ColumnName + " < 0";
                }
                else if (!_attachTable.DefaultView.RowFilter.Contains(_attachTable.PrimaryKey[0].ColumnName + " < 0"))
                {
                    _attachTable.DefaultView.RowFilter = _attachTable.DefaultView.RowFilter + " AND " + _attachTable.PrimaryKey[0].ColumnName + " < 0";
                }
            }

            RefreshData();
        }
        #endregion

        #region DGV control logic...
        private void ux_datagridview_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            // Get the defaultview of the datatable used by the dgv...
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }

            // Begin processing cell formatting based on the datatype of the field...
            if (dv != null &&
                e.ColumnIndex > -1 &&
                e.RowIndex > -1 &&
                e.RowIndex < dv.Count)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                // Format FKey fields...
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    e.RowIndex < dv.Count &&
                    dv[e.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (dv[e.RowIndex][e.ColumnIndex] != DBNull.Value)
                    {
                        e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim(), dv[e.RowIndex][e.ColumnIndex].ToString().Trim(), "", dv[e.RowIndex][e.ColumnIndex].ToString().Trim());
                    }
                    dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    e.FormattingApplied = true;
                }
                // Format date/time fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        e.Value = ((DateTime)dv[e.RowIndex][e.ColumnIndex]).ToString(dateFormat);
                        e.FormattingApplied = true;
                    }
                }
                // Format integer fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    (dc.DataType == typeof(int) ||
                    dc.DataType == typeof(Int16) ||
                    dc.DataType == typeof(Int32) ||
                    dc.DataType == typeof(Int64)))
                {
                    int junk;
                    if (!int.TryParse(e.Value.ToString(), out junk))
                    {
                        dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    }
                }

                if (dc.ReadOnly)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }

                if (dc.ExtendedProperties.Contains("is_nullable") &&
                    dc.ExtendedProperties["is_nullable"].ToString() == "N" &&
                    string.IsNullOrEmpty(dv[e.RowIndex][e.ColumnIndex].ToString()))
                {
                    e.CellStyle.BackColor = Color.Plum;
                }
            }
        }

        private void ux_datagridview_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }
            if (dv != null && e.ColumnIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        DateTime formattedDate;
                        if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out formattedDate))
                        {
                            e.Value = formattedDate;
                            e.ParsingApplied = true;
                        }
                    }
                }
            }
        }

        private void ux_datagridview_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataRow dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
            if (!dgv.CurrentRow.IsNewRow)
            {
                if (dr.GetColumnsInError().Contains(dr.Table.Columns[e.ColumnIndex]) && dr[e.ColumnIndex] != DBNull.Value) dr.SetColumnError(e.ColumnIndex, null);
            }
            dgv.EndEdit();
            dr.EndEdit();
            if (dr.Table.DefaultView.Sort.Contains(dr.Table.Columns[e.ColumnIndex].ColumnName))
            {
                // This column is used for sorting - so changes here will shuffle the order 
                // of the rows in the DGV - so refresh every cell in the DGV...
                RefreshDGVFormatting(dgv);
                // Also rebuild the ListView to display the new ordering of the attachments...
                BuildListView();
            }
            else
            {
                // Refresh just the row that is changing...
                RefreshDGVRowFormatting(dgv.CurrentRow);
            }
        }

        private void ux_datagridview_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string errorMessage = e.Exception.Message;
            int columnWithError = -1;

            // Find the cell the error belongs to (don't use e.ColumnIndex because it points to the current cell *NOT* the offending cell)...
            foreach (DataGridViewColumn col in dgv.Columns)
            {
                if (errorMessage.Contains(col.Name))
                {
                    dgv[col.Name, e.RowIndex].ErrorText = errorMessage;
                    columnWithError = col.Index;
                }
            }
        }

        private void ux_datagridview_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            // Get the defaultview of the datatable used by the dgv...
            DataTable dt = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dt = ((DataTable)((BindingSource)dgv.DataSource).DataSource);
            }
            else
            {
                dt = ((DataTable)dgv.DataSource);
            }
            string columnName = dgv.CurrentCell.OwningColumn.Name;
            DataColumn dc = dt.Columns[columnName];
            DataRow dr;

            if (_sharedUtils.LookupTablesIsValidFKField(dc))
            {
                //string luTableName = dc.ExtendedProperties["foreign_key_resultset_name"].ToString().Trim();
                string luTableName = dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim();
                dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
                string suggestedFilter = dgv.CurrentCell.EditedFormattedValue.ToString();
                if (_lastDGVCharPressed > 0) suggestedFilter = _lastDGVCharPressed.ToString();
                GRINGlobal.Client.Common.LookupTablePicker ltp = new GRINGlobal.Client.Common.LookupTablePicker(_sharedUtils, columnName, dr, suggestedFilter);
                //_lastDGVCharPressed = (char)0;
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    if (dr != null)
                    {
                        if (ltp.NewKey != null && dr[dgv.CurrentCell.ColumnIndex].ToString().Trim() != ltp.NewKey.Trim())
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = ltp.NewKey.Trim();
                            dgv.CurrentCell.Value = ltp.NewValue.Trim();
                        }
                        else if (ltp.NewKey == null)
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = DBNull.Value;
                            dgv.CurrentCell.Value = "";
                        }
                        dr.SetColumnError(dgv.CurrentCell.ColumnIndex, null);
                    }
                }
                dgv.EndEdit();
            }
        }


        private void ux_datagridviewAttachments_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (_enableHotSync &&
                ux_listviewAttachments.Items.Count > e.RowIndex &&
                !ux_listviewAttachments.Items[e.RowIndex].Selected)
            {
                ux_listviewAttachments.Items[e.RowIndex].Selected = true;
            }
        }

        private void ux_datagridview_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void ux_datagridview_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else if (dgv.DataSource.GetType() == typeof(DataTable))
            {
                dv = ((DataTable)(dgv.DataSource)).DefaultView;
            }

            // Reset the last char pressed global variable...
            _lastDGVCharPressed = (char)0;

            // If we are: 
            //  1) in edit mode, 
            //  2) the current cell is parked on a cell that is a FK lookup 
            //  3) and the Alt and Ctrl keys are not down
            // Then remember the keypress so that it can be passed into the Lookup Picker dialog...
            if (dv != null &&
                dgv.CurrentCell != null &&
                dgv.CurrentCell.ColumnIndex > -1 &&
                dgv.CurrentCell.RowIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[dgv.CurrentCell.ColumnIndex];
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    dgv.CurrentCell.RowIndex < dv.Count &&
                    dv[dgv.CurrentCell.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (!e.Alt && !e.Control)
                    {
                        KeysConverter kc = new KeysConverter();
                        string lastChar = kc.ConvertToString(e.KeyValue).Replace("NumPad", "");
                        if (lastChar.Length == 1)
                        {
                            if (e.Shift)
                            {
                                _lastDGVCharPressed = lastChar.ToUpper()[0];
                            }
                            else
                            {
                                _lastDGVCharPressed = lastChar.ToLower()[0];
                            }
                        }
                    }
                }
            }
        }

        private void ux_datagridview_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (_sharedUtils.ProcessDGVEditShortcutKeys(dgv, e, _sharedUtils.UserCooperatorID))
            {
                BuildTreeView();
                BuildListView();
                RefreshDGVFormatting(dgv);
            }
        }

        private void RefreshDGVFormatting(DataGridView dgv)
        {
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                RefreshDGVRowFormatting(dgvr);
            }

            // Show SortGlyphs for the column headers (this takes two steps)...
            // First reset them all to No Sort...
            foreach (DataGridViewColumn dgvc in dgv.Columns)
            {
                dgvc.HeaderCell.SortGlyphDirection = SortOrder.None;
            }
            // Now inspect the sort string from the datatable in use to set the SortGlyphs...
            string strOrder = "";
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                strOrder = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView.Sort;
            }
            else
            {
                strOrder = ((DataTable)dgv.DataSource).DefaultView.Sort;
            }
            char[] chararrDelimiters = { ',' };
            string[] strarrSortCols = strOrder.Split(chararrDelimiters);
            foreach (string strSortCol in strarrSortCols)
            {
                if (strSortCol.Contains("ASC"))
                {
                    if (dgv.Columns.Contains(strSortCol.Replace(" ASC", "").Replace("_sortable", "").Trim())) dgv.Columns[strSortCol.Replace(" ASC", "").Replace("_sortable", "").Trim()].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                }
                if (strSortCol.Contains("DESC"))
                {
                    if (dgv.Columns.Contains(strSortCol.Replace(" DESC", "").Replace("_sortable", "").Trim())) dgv.Columns[strSortCol.Replace(" DESC", "").Replace("_sortable", "").Trim()].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                }
            }
        }

        private void RefreshDGVRowFormatting(DataGridViewRow dgvr)
        {
            foreach (DataGridViewCell dgvc in dgvr.Cells)
            {
                // Reset the background and foreground color...
                dgvc.Style.BackColor = Color.Empty;
                dgvc.Style.ForeColor = Color.Empty;
                dgvc.Style.SelectionBackColor = Color.Empty;
                dgvc.Style.SelectionForeColor = Color.Empty;
            }

            // Reset the row's default background color...
            dgvr.DefaultCellStyle.BackColor = Color.Empty;

            // If the row has changes make each changed cell yellow...
            DataRow dr = ((DataRowView)dgvr.DataBoundItem).Row;
            if (dr.RowState == DataRowState.Modified)
            {
                foreach (DataGridViewCell dgvc in dgvr.Cells)
                {
                    string dcName = dgvc.OwningColumn.Name;
                    // If the cell has been changed make it yellow...
                    if (dr[dcName, DataRowVersion.Original].ToString().Trim() != dr[dcName, DataRowVersion.Current].ToString().Trim())
                    {
                        dgvc.Style.BackColor = Color.Yellow;
                        dr.SetColumnError(dcName, null);
                    }
                }
            }
        }

        #endregion

        #region Dynamic Controls logic...
        private void bindControls(Control.ControlCollection controlCollection, BindingSource bindingSource)
        {
            foreach (Control ctrl in controlCollection)
            {
                //if (ctrl != ux_bindingnavigatorForm)  // Leave the bindingnavigator alone
                if (!(ctrl is BindingNavigator))  // Leave the bindingnavigators alone
                {
                    // If the ctrl has children - bind them too...
                    if (ctrl.Controls.Count > 0)
                    {
                        bindControls(ctrl.Controls, bindingSource);
                    }
                    // Bind the control (by type)...
                    if (ctrl is ComboBox) bindComboBox((ComboBox)ctrl, bindingSource);
                    if (ctrl is TextBox) bindTextBox((TextBox)ctrl, bindingSource);
                    if (ctrl is CheckBox) bindCheckBox((CheckBox)ctrl, bindingSource);
                    if (ctrl is DateTimePicker) bindDateTimePicker((DateTimePicker)ctrl, bindingSource);
                    if (ctrl is Label) bindLabel((Label)ctrl, bindingSource);
                }
            }
        }

        private void formatControls(Control.ControlCollection controlCollection, BindingSource bindingSource)
        {
            foreach (Control ctrl in controlCollection)
            {
                if (ctrl != ux_panelFormView)  // Leave the panel control alone
                {
                    // If the ctrl has children - set their edit mode too...
                    if (ctrl.Controls.Count > 0)
                    {
                        formatControls(ctrl.Controls, bindingSource);
                    }
                    // Set the edit mode for the control...
                    if (ctrl != null &&
                        ctrl.Tag != null &&
                        ctrl.Tag is string &&
                        bindingSource != null &&
                        bindingSource.DataSource is DataTable &&
                        ((DataTable)bindingSource.DataSource).Columns.Contains(ctrl.Tag.ToString().Trim().ToLower()))
                    {
                        // If the field is a ReadOnly field do not allow the tab key to be used to navigate to it...
                        ctrl.TabStop = !((DataTable)bindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        // Set the control's edit properties based on what type of control it is, what edit mode is current, and if the field is readonly...
                        if (ctrl is TextBox)
                        {
                            // TextBoxes have a ReadOnly property in addition to an Enabled property so we handle this one separate...
                            ((TextBox)ctrl).ReadOnly = ((DataTable)bindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        }
                        else if (ctrl is Label)
                        {
                            // Do nothing to the Label
                        }
                        else
                        {
                            // All other control types (ComboBox, CheckBox, DateTimePicker) except Labels...
                            ctrl.Enabled = !((DataTable)bindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        }
                    }
                }
            }
        }

        private void bindComboBox(ComboBox comboBox, BindingSource bindingSource)
        {
            comboBox.DataBindings.Clear();
            comboBox.Enabled = false;
            if (comboBox != null &&
                comboBox.Tag != null &&
                comboBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(comboBox.Tag.ToString().Trim().ToLower()))
            {
                if (_sharedUtils != null)
                {
                    DataColumn dc = ((DataTable)bindingSource.DataSource).Columns[comboBox.Tag.ToString().Trim().ToLower()];
                    _sharedUtils.BindComboboxToCodeValue(comboBox, dc);
                    if (comboBox.DataSource.GetType() == typeof(DataTable))
                    {
                        // Calculate the maximum width needed for displaying the dropdown items and set the combobox property...
                        int maxWidth = comboBox.DropDownWidth;
                        foreach (DataRow dr in ((DataTable)comboBox.DataSource).Rows)
                        {
                            if (TextRenderer.MeasureText(dr["display_member"].ToString().Trim(), comboBox.Font).Width > maxWidth)
                            {
                                maxWidth = TextRenderer.MeasureText(dr["display_member"].ToString().Trim(), comboBox.Font).Width;
                            }
                        }
                        comboBox.DropDownWidth = maxWidth;
                    }

                    // Bind the SelectedValue property to the binding source...
                    comboBox.DataBindings.Add("SelectedValue", bindingSource, comboBox.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);

                    // Wire up to an event handler if this column is a date_code (format) field...
                    if (dc.ColumnName.Trim().ToLower().EndsWith("_code") &&
                        dc.Table.Columns.Contains(dc.ColumnName.Trim().ToLower().Substring(0, dc.ColumnName.Trim().ToLower().LastIndexOf("_code"))))
                    {
                        comboBox.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
                    }
                }
                else
                {
                    // Bind the Text property to the binding source...
                    comboBox.DataBindings.Add("Text", bindingSource, comboBox.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);
                }
            }
        }

        private void bindTextBox(TextBox textBox, BindingSource bindingSource)
        {
            textBox.DataBindings.Clear();
            textBox.ReadOnly = true;
            if (textBox != null &&
                textBox.Tag != null &&
                textBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(textBox.Tag.ToString().Trim().ToLower()))
            {
                DataTable dt = (DataTable)bindingSource.DataSource;
                DataColumn dc = dt.Columns[textBox.Tag.ToString().Trim().ToLower()];
                if (_sharedUtils.LookupTablesIsValidFKField(dc))
                {
                    // Create a new binding that handles display_member/value_member conversions...
                    Binding textBinding = new Binding("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                    textBinding.Format += new ConvertEventHandler(textLUBinding_Format);
                    textBinding.Parse += new ConvertEventHandler(textLUBinding_Parse);
                    textBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                    // Bind it to the textbox...
                    textBox.DataBindings.Add(textBinding);
                }
                else if (dc.DataType == typeof(DateTime))
                {
                    // Create a new binding that handles display_member/value_member conversions...
                    Binding textBinding = new Binding("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                    textBinding.Format += new ConvertEventHandler(textDateTimeBinding_Format);
                    textBinding.Parse += new ConvertEventHandler(textDateTimeBinding_Parse);
                    textBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                    // Bind it to the textbox...
                    textBox.DataBindings.Add(textBinding);
                }
                else
                {
                    // Bind to a plain-old text field in the database (no LU required)...
                    textBox.DataBindings.Add("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                }

                // Add an event handler for processing the first key press (to display the lookup picker dialog)...
                textBox.KeyDown += new KeyEventHandler(textBox_KeyDown);
                textBox.KeyPress += new KeyPressEventHandler(textBox_KeyPress);
            }
        }

        private void bindCheckBox(CheckBox checkBox, BindingSource bindingSource)
        {
            checkBox.DataBindings.Clear();
            checkBox.Enabled = false;
            if (checkBox != null &&
                checkBox.Tag != null &&
                checkBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(checkBox.Tag.ToString().Trim().ToLower()))
            {
                DataTable dt = (DataTable)bindingSource.DataSource;
                DataColumn dc = dt.Columns[checkBox.Tag.ToString().Trim().ToLower()];
                checkBox.Text = dc.Caption;
                Binding boolBinding = new Binding("Checked", bindingSource, checkBox.Tag.ToString().Trim().ToLower());
                boolBinding.Format += new ConvertEventHandler(boolBinding_Format);
                boolBinding.Parse += new ConvertEventHandler(boolBinding_Parse);
                boolBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                checkBox.DataBindings.Add(boolBinding);
            }
        }

        private void bindDateTimePicker(DateTimePicker dateTimePicker, BindingSource bindingSource)
        {
            dateTimePicker.DataBindings.Clear();
            dateTimePicker.Enabled = false;
            if (dateTimePicker != null &&
                dateTimePicker.Tag != null &&
                dateTimePicker.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(dateTimePicker.Tag.ToString().Trim().ToLower()))
            {
                // Now bind the control to the column in the bindingSource...
                dateTimePicker.DataBindings.Add("Text", bindingSource, dateTimePicker.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        private void bindLabel(Label label, BindingSource bindingSource)
        {
            if (label != null &&
                label.Tag != null &&
                label.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(label.Tag.ToString().Trim().ToLower()))
            {
                //label.DataBindings.Add("Text", bindingSource, label.Tag.ToString().Trim().ToLower());
                label.Text = ((DataTable)bindingSource.DataSource).Columns[label.Tag.ToString().Trim().ToLower()].Caption;
            }
        }

        void boolBinding_Format(object sender, ConvertEventArgs e)
        {
            switch (e.Value.ToString().ToUpper())
            {
                case "Y":
                    e.Value = true;
                    break;
                case "N":
                    e.Value = false;
                    break;
                default:
                    e.Value = false;
                    break;
            }
        }

        void boolBinding_Parse(object sender, ConvertEventArgs e)
        {
            if (e.Value != null)
            {
                switch ((bool)e.Value)
                {
                    case true:
                        e.Value = "Y";
                        break;
                    case false:
                        e.Value = "N";
                        break;
                    default:
                        e.Value = "N";
                        break;
                }
            }
            else
            {
                e.Value = "N";
            }
        }

        void textDateTimeBinding_Format(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (dc.DataType == typeof(DateTime) &&
                !string.IsNullOrEmpty(e.Value.ToString()) &&
                dt.Columns.Contains(dc.ColumnName.Trim().ToLower() + "_code"))
            {
                DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                string dateFormat = "MM/dd/yyyy";
                dateFormat = drv[dc.ColumnName + "_code"].ToString().Trim();
                e.Value = ((DateTime)e.Value).ToString(dateFormat);
            }
            else if (dc.ColumnName.ToLower().Trim() == "ordered_date" ||
                dc.ColumnName.ToLower().Trim() == "completed_date")
            {
                DateTime formattedDate;
                if (DateTime.TryParse(e.Value.ToString(), out formattedDate))
                {
                    e.Value = formattedDate.ToString("d");
                }
            }
        }

        void textDateTimeBinding_Parse(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (dc.DataType == typeof(DateTime) &&
                !string.IsNullOrEmpty(e.Value.ToString()) &&
                dt.Columns.Contains(dc.ColumnName.Trim().ToLower() + "_code"))
            {
                DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                string dateFormat = drv[dc.ColumnName + "_code"].ToString().Trim();
                DateTime parsedDateTime;
                if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out parsedDateTime))
                {
                    e.Value = parsedDateTime;
                }
            }
        }

        void textLUBinding_Format(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (!string.IsNullOrEmpty(e.Value.ToString()))
            {
                //e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_resultset_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
                e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
            }
        }

        void textLUBinding_Parse(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (!string.IsNullOrEmpty(e.Value.ToString()))
            {
                //e.Value = _sharedUtils.GetLookupValueMember(dc.ExtendedProperties["foreign_key_resultset_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());  ((DataRowView)((BindingSource)b.DataSource).Current).Row
                e.Value = _sharedUtils.GetLookupValueMember(((DataRowView)((BindingSource)b.DataSource).Current).Row, dc.ExtendedProperties["foreign_key_dataview_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
            }
        }

        void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            string dateColumnName = cb.Tag.ToString().Replace("_code", "");
            foreach (Binding b in cb.DataBindings)
            {
                foreach (Control ctrl in cb.Parent.Controls)
                {
                    if (ctrl.Tag.ToString() == dateColumnName &&
                        ctrl.GetType() == typeof(TextBox))
                    {
                        DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                        if (drv == null) continue;
                        string dateFormat = "MM/dd/yyyy";
                        dateFormat = drv[cb.Tag.ToString()].ToString().Trim();
                        DateTime dt;
                        if (DateTime.TryParseExact(drv[dateColumnName].ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                        {
                            ctrl.Text = ((DateTime)drv[dateColumnName]).ToString(dateFormat);
                        }
                    }
                }
            }
        }

        void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;


            if (!tb.ReadOnly)
            {
                foreach (Binding b in tb.DataBindings)
                {
                    if (b.BindingManagerBase != null &&
                        b.BindingManagerBase.Current != null &&
                        b.BindingManagerBase.Current is DataRowView &&
                        b.BindingMemberInfo.BindingField != null)
                    {
                        if (_sharedUtils.LookupTablesIsValidFKField(((DataRowView)b.BindingManagerBase.Current).Row.Table.Columns[b.BindingMemberInfo.BindingField]) &&
                            e.KeyChar != Convert.ToChar(Keys.Escape)) // Ignore the Escape key and process anything else...
                        {
                            string filterText = tb.Text;
                            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), "[a-zA-Z0-9]"))
                            {
                                filterText = e.KeyChar.ToString();
                            }
                            LookupTablePicker ltp = new LookupTablePicker(_sharedUtils, tb.Tag.ToString(), ((DataRowView)b.BindingManagerBase.Current).Row, filterText);
                            ltp.StartPosition = FormStartPosition.CenterParent;
                            if (DialogResult.OK == ltp.ShowDialog())
                            {
                                tb.Text = ltp.NewValue.Trim();
                            }
                            e.Handled = true;
                        }
                    }
                }
            }
        }

        void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) // Process the Delete key (since it is not passed on to the KeyPress event handler)...
            {
                TextBox tb = (TextBox)sender;

                if (!tb.ReadOnly)
                {
                    foreach (Binding b in tb.DataBindings)
                    {
                        if (b.BindingManagerBase != null &&
                            b.BindingManagerBase.Current != null &&
                            b.BindingManagerBase.Current is DataRowView &&
                            b.BindingMemberInfo.BindingField != null)
                        {
                            // Just in case the user selected only a part of the full text to delete - strip out the selected text and process normally...
                            string remainingText = tb.Text.Remove(tb.SelectionStart, tb.SelectionLength);
                            if (string.IsNullOrEmpty(remainingText))
                            {
                                // When a textbox is bound to a table - some datatypes will not revert to a DBNull via the bound control - so
                                // take control of the update and force the field back to a null (non-nullable fields should show up to the GUI with colored background)...
                                ((DataRowView)b.BindingManagerBase.Current).Row[b.BindingMemberInfo.BindingField] = DBNull.Value;
                                b.ReadValue();
                                e.Handled = true;
                            }
                            else
                            {
                                if (_sharedUtils.LookupTablesIsValidFKField(((DataRowView)b.BindingManagerBase.Current).Row.Table.Columns[b.BindingMemberInfo.BindingField]))
                                {
                                    LookupTablePicker ltp = new LookupTablePicker(_sharedUtils, tb.Tag.ToString(), ((DataRowView)b.BindingManagerBase.Current).Row, remainingText);
                                    ltp.StartPosition = FormStartPosition.CenterParent;
                                    if (DialogResult.OK == ltp.ShowDialog())
                                    {
                                        tb.Text = ltp.NewValue.Trim();
                                        b.WriteValue();
                                        e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        private void RefreshData()
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Disable HotSync between the listview and the datagridview...
            _enableHotSync = false;

            // Initialize validPKeys with the default accession_inv_attach table PKey list...
            string validPKeys = _attachParameterName + "=";
            string validParentPKeys = _attachParameterName + "=";

            // If the user has selected the 'Accession' radio button adjust the PKEY name for the validPKeys...
            if (_attachParameterName.Trim().ToLower().Contains("accession")) validParentPKeys = ":inventoryid=";

            // Don't reload the remote data unless something has changed in the list of attachments being worked on...
            if (_previousPKeys != _currentPKeys ||
                _previousFolderPathOrFileNames != _currentFolderPathOrFileNames)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(_currentPKeys.Trim(), @"^:.*id=")) // match pkeys like ':inventoryid=' or ':orderrequestid='
                {
                    // Current attachment filter is a list of PKeys so retrieve all attachments on the 
                    // server associated with this list of PKeys...
                    validPKeys = _currentPKeys.Trim();
                    if (validPKeys.Trim().Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries).Length == 2) validPKeys += ",";
                }

                if (_previousFolderPathOrFileNames != _currentFolderPathOrFileNames ||
                    validPKeys.Contains(_currentPKeys))
                {
                    // This is a list of files so process each fullpath file name...
                    // Create a dictionary by inspecting each file in the file collection or folder 
                    // and using the file name to retrieve the PKEY...
                    _attachmentFilesDictionary = LoadAttachmentDictionary(_currentFolderPathOrFileNames);

                    // Get the remote servers filepath for each attachment's ParentPKey to be saved....
                    // First build the PKey parameter list...
                    foreach (int pkey in _attachmentFilesDictionary.Values)
                    {
                        if (pkey > 0 &&
                            !validParentPKeys.Contains(pkey.ToString()))
                        {
                            // Update the parameter list of PKeys that will be used to retrieve exisiting attachment records...
                            validParentPKeys += pkey.ToString() + ",";
                        }
                    }
                }

                validPKeys = validPKeys.TrimEnd(',').Trim();
                validParentPKeys = validParentPKeys.TrimEnd(',').Trim();

                // Get the dataview names needed for processing validPKeys...
                string parentPKeyName = _attachParentPKeyName;
                string attachDataview = _attachDataviewName;
                string attachFilePathDataview = "attach_wizard_" + attachDataview + "_filepath";

                // Get any pre-existing attachment records for the PKeys in the dictionary list...
                _dsAttach = _sharedUtils.GetWebServiceData(attachDataview, validPKeys, 0, 0);

                // If data was retrieved build a new interface...
                if (_dsAttach != null &&
                    _dsAttach.Tables.Contains(attachDataview))
                {
                    _attachTable = _dsAttach.Tables[attachDataview];
                    // Clear the databindings (in case the new dataview does not have all of 
                    // the columns that were bound during the last databinding...
                    foreach (Control ctrl in ux_panelFormView.Controls)
                    {
                        ctrl.DataBindings.Clear();
                    }
                    _attachBindingSource.DataSource = _attachTable;
                    bindControls(ux_panelFormView.Controls, _attachBindingSource);
                    formatControls(ux_panelFormView.Controls, _attachBindingSource);

                    // Create the edit mode datagridview...
                    _sharedUtils.BuildEditDataGridView(ux_datagridviewAttachments, _attachTable);
                }

                // Call the dataview to calculate the filepath of the attachments in the dictionary list...
                DataTable attachFilePath = GetAttachmentFilePaths(validParentPKeys, attachFilePathDataview);

                // Process each of the entries in the dictionary list that have a valid PKey...
                if (_attachTable != null &&
                    _attachTable.Rows.Count >= 0 &&
                    attachFilePath != null &&
                    attachFilePath.Rows.Count >= 0)
                {
                    int i = 0;
                    // Add the new records to the table...
                    foreach (string localFilePath in _attachmentFilesDictionary.Keys)
                    {
                        i++;
                        UpdateProgressBar(true, 0, _attachmentFilesDictionary.Count, i, "Processing database records...");
                        // Get the parent PKey...
                        int PKey = _attachmentFilesDictionary[localFilePath];

                        // Check to see if this is a physical file or a web URL by creating a FileInfo object...
                        try
                        {
                            // Get the filepath for this parent PKey...
                            System.IO.FileInfo fi = new System.IO.FileInfo(localFilePath);
                            // If we made it here the file must be physical...
                            string fileName = fi.Name;
                            DateTime imageDate = FindDateFromFileNameOrPath(fi.FullName);
                            string attachmentDescriptionCode = FindDescriptionCodeFromFileNameOrPath(fi.FullName);
                            string attachmentContentType = FindContentTypeFromFileNameOrPath(fi.FullName);
                            string attachmentCategoryCode = FindCategoryCodeFromFileName(fi.FullName);
                            DataRow remoteFilePath = attachFilePath.Rows.Find(PKey);
                            DataRow[] drs = _attachTable.Select(parentPKeyName + " = " + PKey.ToString() + " AND virtual_path LIKE '%" + fileName + "%'");
                            if (drs.Length > 0)
                            {
                                // There is an existing accession_inv_attach record associated with this
                                // PKey linked to a file using this filename - so do not create a new
                                // attachment record but instead let the user update the existing record...
                                string fullFileName = remoteFilePath["attach_filepath"].ToString() + fileName;
                                drs[0]["virtual_path"] = fullFileName;
                                // Only add the thumbnail URL if the attachment is an image...
                                if (_supportedImageExtensions.ToLower().Contains(new System.IO.FileInfo(fullFileName).Extension.ToLower()) &&
                                    _attachTable.Columns.Contains("thumbnail_virtual_path"))
                                {
                                    drs[0]["thumbnail_virtual_path"] = fullFileName.Insert(fullFileName.LastIndexOf('.'), "_thumbnail");
                                }
                            }
                            else
                            {
                                string fullFileName = fi.FullName;
                                if (remoteFilePath != null) fullFileName = remoteFilePath["attach_filepath"].ToString() + fileName;
                                // Add a new attachment record for this valid PKey...
                                DataRow newAttachment = _attachTable.NewRow();
                                if (_attachTable.Columns.Contains(parentPKeyName)) newAttachment[parentPKeyName] = PKey;
                                if (_attachTable.Columns.Contains("virtual_path")) newAttachment["virtual_path"] = fullFileName;
                                // Only add the thumbnail URL if the attachment is an image...
                                if (_supportedImageExtensions.ToLower().Contains(new System.IO.FileInfo(fileName).Extension.ToLower()) &&
                                    _attachTable.Columns.Contains("thumbnail_virtual_path"))
                                {
                                    newAttachment["thumbnail_virtual_path"] = fullFileName.Insert(fullFileName.LastIndexOf('.'), "_thumbnail");
                                }
                                //if (_attachTable.Columns.Contains("sort_order")) newAttachment["sort_order"] = 0;
                                //if (_attachTable.Columns.Contains("title")) newAttachment["title"] = "";
                                //if (_attachTable.Columns.Contains("description")) newAttachment["description"] = "";
                                if (_attachTable.Columns.Contains("description_code")) newAttachment["description_code"] = attachmentDescriptionCode;
                                if (_attachTable.Columns.Contains("content_type")) newAttachment["content_type"] = attachmentContentType;
                                if (_attachTable.Columns.Contains("category_code")) newAttachment["category_code"] = attachmentCategoryCode;
                                //if(_attachTable.Columns.Contains("copyright_information")) newAttachment["copyright_information"] = "";
                                if (_attachTable.Columns.Contains("attach_cooperator_id")) newAttachment["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
                                if (_attachTable.Columns.Contains("is_web_visible")) newAttachment["is_web_visible"] = "Y";
                                if (_attachTable.Columns.Contains("attach_date")) newAttachment["attach_date"] = imageDate;
                                if (_attachTable.Columns.Contains("attach_date_code")) newAttachment["attach_date_code"] = "MM/dd/yyyy";
                                //if(_attachTable.Columns.Contains("note")) newAttachment["note"] = "";
                                _attachTable.Rows.Add(newAttachment);
                            }
                        }
                        catch (Exception)
                        {
                            // If we made it here the filename was not valid so treat it as a web URL...
                            // Add a new attachment record for this valid PKey...
                            DataRow newAttachment = _attachTable.NewRow();
                            if (_attachTable.Columns.Contains(parentPKeyName)) newAttachment[parentPKeyName] = PKey;
                            if (_attachTable.Columns.Contains("virtual_path")) newAttachment["virtual_path"] = localFilePath;
                            //if (_attachTable.Columns.Contains("thumbnail_virtual_path")) newAttachment["thumbnail_virtual_path"] = "";
                            //if (_attachTable.Columns.Contains("sort_order")) newAttachment["sort_order"] = 0;
                            //if (_attachTable.Columns.Contains("title")) newAttachment["title"] = "";
                            //if (_attachTable.Columns.Contains("description")) newAttachment["description"] = "";
                            //if (_attachTable.Columns.Contains("description_code")) newAttachment["description_code"] = "";
                            if (_attachTable.Columns.Contains("content_type")) newAttachment["content_type"] = "text/html";
                            if (_attachTable.Columns.Contains("category_code")) newAttachment["category_code"] = "LINK";
                            //if(_attachTable.Columns.Contains("copyright_information")) newAttachment["copyright_information"] = "";
                            if (_attachTable.Columns.Contains("attach_cooperator_id")) newAttachment["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
                            if (_attachTable.Columns.Contains("is_web_visible")) newAttachment["is_web_visible"] = "Y";
                            if (_attachTable.Columns.Contains("attach_date")) newAttachment["attach_date"] = DateTime.Now.Date;
                            if (_attachTable.Columns.Contains("attach_date_code")) newAttachment["attach_date_code"] = "MM/dd/yyyy";
                            //if(_attachTable.Columns.Contains("note")) newAttachment["note"] = "";
                            _attachTable.Rows.Add(newAttachment);
                        }
                    }
                }
                UpdateProgressBar(false, 0, _attachmentFilesDictionary.Count, 0, "Done processing database records...");

                // Create an imagelist to be used by the imagelistview control...
                string currentRowFilter = "";
                string attachTableRowFilter = "";
                if (_attachTable != null && string.IsNullOrEmpty(_attachTable.DefaultView.Sort) && _attachTable.Columns.Contains("sort_order"))
                {
                    _attachTable.DefaultView.Sort = "sort_order ASC";
                    currentRowFilter = _attachTable.DefaultView.RowFilter;
                    attachTableRowFilter = _attachTable.PrimaryKey[0].ColumnName + " < 0";
                    if (!ux_checkboxViewExistingAttachments.Checked)
                    {
                        if (string.IsNullOrEmpty(currentRowFilter))
                        {
                            _attachTable.DefaultView.RowFilter = attachTableRowFilter;
                        }
                        else if (!currentRowFilter.Contains(attachTableRowFilter))
                        {
                            _attachTable.DefaultView.RowFilter = currentRowFilter + " AND " + attachTableRowFilter;
                        }
                    }
                    _largeImageList = LoadImageList(_attachmentFilesDictionary, _attachTable.DefaultView.ToTable());
                    _smallImageList64 = ResizeImageList(_largeImageList, 64);
                    _smallImageList32 = ResizeImageList(_largeImageList, 32);

                UpdateProgressBar(false, 0, _attachmentFilesDictionary.Count, 0, "Done processing Attachment Files...");

                // Create a treeview of the files being attached to the PKey...
                BuildTreeView();

                // Remember the path of the currently loaded attachments...
                _previousFolderPathOrFileNames = _currentFolderPathOrFileNames;
                // Remember the list of PKeys being used...
                _previousPKeys = _currentPKeys;
                }
            }

            // Bind the attachTable datatable to the binding source...        
            _attachBindingSource.DataSource = _attachTable;

            // Update the listview....
            BuildListView();

            // Enable HotSync between the listview and the datagridview again...
            _enableHotSync = true;

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private Dictionary<string, int> LoadAttachmentDictionary(string fullFolderPathOrFileNameCollection)
        {
            Dictionary<string, int> attachmentDictionary = new Dictionary<string, int>();

            // This is a collection of filenames and/or folder names...
            string[] fileOrFolderTokens = fullFolderPathOrFileNameCollection.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
            // Iterate through the string tokens looking for file or folder names...
            foreach (string fileOrFolderToken in fileOrFolderTokens)
            {
                if (System.IO.File.Exists(fileOrFolderToken.Trim()))
                {
                    // This is an individual file...
                    System.IO.FileInfo fi = new System.IO.FileInfo(fileOrFolderToken.Trim());
                    if (IsFileIncluded(fi) &&
                        !IsFileExcluded(fi))
                    {
                        // If a PKey token(s) is found in the full file name/path use it instead of the PKey of the treeview node...
                        int PKey = -1;  
                        // The DragAndDrop performed by the user overrides the PKEY hints found in the filename and path...
                        if (_dragAndDropAttachmentFilesDictionary.ContainsKey(fi.FullName))
                        {
                            PKey = _dragAndDropAttachmentFilesDictionary[fi.FullName];
                        }
                        // If the attachment file was not part of a Drag/Drop process look at the filename and path 
                        // to resolve the parent of the attachment...
                        if(PKey < 0)
                        {
                            PKey = FindPKeyFromFileNameOrPath(fi.FullName);
                        }

                        // Add the entry to the dictionary if it is not already there...
                        if (!attachmentDictionary.ContainsKey(fi.FullName))
                        {
                            attachmentDictionary.Add(fi.FullName, PKey);
                        }
                    }
                }
                else if (System.IO.Directory.Exists(fileOrFolderToken.Trim()))
                {
                    // This is a folder which must be handled recursively because it can contain
                    // multiple files and subfolders...
                    System.IO.DirectoryInfo folderInfo = new System.IO.DirectoryInfo(fileOrFolderToken.Trim());
                    AddAttachmentCollection(attachmentDictionary, folderInfo);
                    // Now look for subfolders and gather up a collection of them also to recursively iterate through...
                    System.IO.DirectoryInfo[] subFolders = folderInfo.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);
                    if (subFolders != null &&
                        subFolders.Length > 0)
                    {
                        // Process each subfolder in the collection...
                        foreach (System.IO.DirectoryInfo di in subFolders)
                        {
                            // Add all files in the subfolder to the dictionary...
                            AddAttachmentCollection(attachmentDictionary, di);
                        }
                    }
                }
                else if(Uri.IsWellFormedUriString(fileOrFolderToken.Trim(), UriKind.Absolute))
                {
                    // This is a web browser URL (not a file or folder) so process it...
                    int PKey = -1;
                    // The DragAndDrop performed by the user overrides the PKEY hints found in the filename and path...
                    if (_dragAndDropAttachmentFilesDictionary.ContainsKey(fileOrFolderToken.Trim()))
                    {
                        PKey = _dragAndDropAttachmentFilesDictionary[fileOrFolderToken.Trim()];
                    }
                    // If the URL link was not part of a Drag/Drop process look at the URL text 
                    // to resolve the parent of the attachment...
                    if (PKey < 0)
                    {
                        PKey = FindPKeyFromFileNameOrPath(fileOrFolderToken.Trim());
                    }

                    // Add the entry to the dictionary if it is not already there...
                    if (!attachmentDictionary.ContainsKey(fileOrFolderToken.Trim()))
                    {
                        attachmentDictionary.Add(fileOrFolderToken.Trim(), PKey);
                    }
                }
            }

            return attachmentDictionary;
        }

        private void AddAttachmentCollection(Dictionary<string, int> attachmentDictionary, System.IO.DirectoryInfo folderInfo)
        {
            System.IO.FileInfo[] attachmentFiles = folderInfo.GetFiles("*.*", System.IO.SearchOption.TopDirectoryOnly);
            int i = 0;
            foreach (System.IO.FileInfo fi in attachmentFiles)
            {
                i++;
                if (IsFileIncluded(fi) &&
                    !IsFileExcluded(fi))
                {
                    int PKey = FindPKeyFromFileNameOrPath(fi.FullName);
                    if (!attachmentDictionary.ContainsKey(fi.FullName))
                    {
                        attachmentDictionary.Add(fi.FullName, PKey);
                    }
                    UpdateProgressBar(true, 0, attachmentFiles.Length, i, "Inspecting files in folder '" + folderInfo.Name + "'");
                }
            }
            UpdateProgressBar(false, 0, attachmentFiles.Length, i, "Done inspecting files in folder '" + folderInfo.Name + "'");
        }

        private void BuildListView()
        {
            if (_largeImageList != null &&
                _smallImageList64 != null &&
                _smallImageList32 != null &&
                _attachTable != null)
            {
                ux_listviewAttachments.LargeImageList = _largeImageList;
                if (ux_radiobuttonViewSmallIcon.Checked)
                {
                    ux_listviewAttachments.SmallImageList = _smallImageList64;
                }
                else
                {
                    ux_listviewAttachments.SmallImageList = _smallImageList32;
                }
                //ux_listviewAttachments.View = View.Tile;
                //ux_listviewAttachments.TileSize = new Size(ux_listviewAttachments.Size.Width, _largeImageList.ImageSize.Height + 4);                

                ux_listviewAttachments.Items.Clear();
                ux_listviewAttachments.Groups.Clear();
                ux_listviewAttachments.Columns.Clear();

                // Create the ListView columns...
                if (_attachTable.Columns.Contains("virtual_path"))
                {
                    ux_listviewAttachments.Columns.Add("File Name", "File Name");
                }
                if (_attachTable.Columns.Contains("description_code"))
                {
                    ux_listviewAttachments.Columns.Add("Description Code", "Description Code");
                }
                if (_attachTable.Columns.Contains("title"))
                {
                    ux_listviewAttachments.Columns.Add("Title", "Title");
                }
                if (_attachTable.Columns.Contains("description"))
                {
                    ux_listviewAttachments.Columns.Add("Description", "Description");
                }

                // Populate the ListView images and column data...
                foreach (DataRowView drv in _attachTable.DefaultView)
                {
                    string friendlyName = _sharedUtils.GetLookupDisplayMember(_attachParentPKeyName.Replace("_id", "_lookup"), drv[_attachParentPKeyName].ToString(), "", drv[_attachParentPKeyName].ToString());
                    if (ux_listviewAttachments.Groups[friendlyName] == null)
                    {
                        ux_listviewAttachments.Groups.Add(friendlyName, friendlyName);
                    }

                    string VirtualPath = "";
                    try
                    {
                        System.IO.FileInfo fi = new System.IO.FileInfo(drv["virtual_path"].ToString());
                        VirtualPath = fi.Name;
                    }
                    catch (Exception)
                    {
                        VirtualPath = drv["virtual_path"].ToString();
                    }
                    ListViewItem newLVI = new ListViewItem(VirtualPath, drv[_attachTable.PrimaryKey[0].ColumnName].ToString(), ux_listviewAttachments.Groups[friendlyName]);
                    if (_attachTable.Columns.Contains("description_code")) newLVI.SubItems.Add(drv["description_code"].ToString());
                    if (_attachTable.Columns.Contains("title")) newLVI.SubItems.Add(drv["title"].ToString());
                    if (_attachTable.Columns.Contains("description")) newLVI.SubItems.Add(drv["description"].ToString());
                    ux_listviewAttachments.Items.Add(newLVI);
                }

                // Calculate the details view column widths...
                CalculateListViewColumnWidth();
            }
        }

        private ImageList LoadImageList(Dictionary<string, int> attachmentDictionary, DataTable attachTable)
        {
            ImageList returnImageList = new ImageList();
            returnImageList.ImageSize = new Size(256, 256);
            returnImageList.ColorDepth = ColorDepth.Depth32Bit;
            string parentPKeyName = "";
            string attachTablePKeyName = _attachTable.PrimaryKey[0].ColumnName;

            // Get the parent PKey name for the attachment table for later processing...
            foreach (DataColumn dc in _attachTable.Columns)
            {
                if (dc.ColumnName.Replace("_", "") == _currentPKeys.Split('=')[0].Trim().TrimStart(':'))
                {
                    parentPKeyName = dc.ColumnName;
                    break;
                }
            }

            // Load the images into an imagelist cache to be used by the imagelistview control...
            int i = 0;
            //foreach (string localFilePath in attachmentDictionary.Keys)
            foreach (DataRow dr in attachTable.Rows)
            {
                string remoteServerFilePath = dr["virtual_path"].ToString();
                // Default image path should be the remote server's thumbnail image...
                string remoteServerThumbnailFilePath = dr["thumbnail_virtual_path"].ToString();
                // Thumbnail field is nullable so we need to make sure it has a value...
                if (string.IsNullOrEmpty(remoteServerThumbnailFilePath)) remoteServerThumbnailFilePath = remoteServerFilePath;
                // If the virtual_path is not an absolute URI the attachment record is new (and thus 
                // no remote image exists or a newer local copy of the image file is being used to
                // update the existing record - the only way to know is to check the attachmentDictionary for a matching
                // entry and if one is found use it instead of attempting to retrieve a remote image...
                System.IO.FileInfo fi;
                string escapedURI = Uri.EscapeUriString(remoteServerFilePath);
                Uri tempURI;
                if (Uri.IsWellFormedUriString(escapedURI, UriKind.Absolute))
                {
                    // Create a fileinfo object using the remote server's virtual_path...
                    tempURI = new Uri(escapedURI);
                    try
                    {
                        fi = new System.IO.FileInfo(tempURI.AbsolutePath);
                    }
                    catch (Exception)
                    {
                        fi = new System.IO.FileInfo("undefined.undefined");
                    }
                }
                else
                {
                    if (Uri.TryCreate(remoteServerFilePath, UriKind.Absolute, out tempURI))
                    {
                        tempURI = new Uri(remoteServerFilePath);
                        try
                        {
                            fi = new System.IO.FileInfo(tempURI.AbsolutePath);
                        }
                        catch (Exception)
                        {
                            fi = new System.IO.FileInfo("undefined.undefined");
                        }
                    }
                    else
                    {
                        try
                        {
                            fi = new System.IO.FileInfo(remoteServerFilePath);
                        }
                        catch (Exception)
                        {
                            fi = new System.IO.FileInfo("undefined.undefined");
                        }
                    }
                }
                // Get the filename from the server virtual_path...
                string fileName = fi.Name;
                // Get the PKey...
                string parentPKey = dr[parentPKeyName].ToString();

                // Now iterate through the dictionary looking for a key that matches both 
                // the filename and the parent foldername (aka the parentPKeyName) which 
                // indicates a local copy of the image is available - so we should use that instead...
                foreach (string key in attachmentDictionary.Keys)
                {
                    if (key.Contains(fileName) &&
                        attachmentDictionary[key].ToString().Trim() == parentPKey.Trim())
                    {
                        remoteServerFilePath = key;
                        break;
                    }
                }
                // Load either the remote thumbnail image of the attachment,
                // a local copy of the image,
                // or an icon image (based on the file extension)...
                if (remoteServerFilePath.Length > 0)
                {
                    if (System.IO.File.Exists(remoteServerFilePath) &&
                        !string.IsNullOrEmpty(fi.Extension) &&
                        _supportedImageExtensions.Contains(fi.Extension.ToLower()))
                    {
                        Image fullImage = Image.FromFile(remoteServerFilePath);
                        Image thumbnailImage = fullImage.GetThumbnailImage(128, 128, null, IntPtr.Zero);
                        fullImage.Dispose();
                        returnImageList.Images.Add(dr[attachTablePKeyName].ToString(), thumbnailImage);
                    }
                    else if (dr["category_code"].ToString().ToUpper() != "LINK" &&
                        !Uri.IsWellFormedUriString(remoteServerThumbnailFilePath, UriKind.Absolute) &&
                        !string.IsNullOrEmpty(fi.Extension) &&
                        _supportedImageExtensions.Contains(fi.Extension.ToLower()))
                    {
                        byte[] attachBytes = _sharedUtils.GetAttachment(remoteServerThumbnailFilePath);
                        if (attachBytes != null && attachBytes.Length > 0)
                        {
                            System.IO.MemoryStream ms = new System.IO.MemoryStream(attachBytes);
                            Image fullImage = Image.FromStream(ms, false, true);
                            Image thumbnailImage = fullImage.GetThumbnailImage(Math.Min(256, fullImage.Width), Math.Min(256, fullImage.Height), null, IntPtr.Zero);
                            fullImage.Dispose();
                            returnImageList.Images.Add(dr[attachTablePKeyName].ToString(), thumbnailImage);
                        }
                        else
                        {
                            // Thumbnail was not retrieved - use a file-type specific icon instead...
                            string[] matchingFileExtImages = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Images\AttachmentWizard\", "file_extension_" + fi.Extension.TrimStart('.') + ".*", System.IO.SearchOption.TopDirectoryOnly);
                            if (matchingFileExtImages.Length > 0)
                            {
                                Image fullImage = Image.FromFile(matchingFileExtImages[0]);
                                Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width, fullImage.Height, null, IntPtr.Zero);
                                fullImage.Dispose();
                                returnImageList.Images.Add(dr[attachTablePKeyName].ToString(), thumbnailImage);
                            }
                        }
                    }
                    else if (!string.IsNullOrEmpty(fi.Extension) &&
                        _supportedImageExtensions.Contains(fi.Extension.ToLower()))
                    {
                        byte[] attachBytes = _sharedUtils.GetAttachment(remoteServerFilePath.TrimStart('/'));
                        if (attachBytes != null && attachBytes.Length > 0)
                        {
                            // Thumbnail image retrieved - add it to the image list...
                            System.IO.MemoryStream ms = new System.IO.MemoryStream(attachBytes);
                            Image fullImage = Image.FromStream(ms, false, true);
                            Image thumbnailImage = fullImage.GetThumbnailImage(Math.Min(128, fullImage.Width), Math.Min(128, fullImage.Height), null, IntPtr.Zero);
                            fullImage.Dispose();
                            returnImageList.Images.Add(dr[attachTablePKeyName].ToString(), thumbnailImage);
                        }
                        else
                        {
                            // Thumbnail was not retrieved - use a file-type specific icon instead...
                            string[] matchingFileExtImages = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Images\AttachmentWizard\", "file_extension_" + fi.Extension.TrimStart('.') + ".*", System.IO.SearchOption.TopDirectoryOnly);
                            if (matchingFileExtImages.Length > 0)
                            {
                                Image fullImage = Image.FromFile(matchingFileExtImages[0]);
                                Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width, fullImage.Height, null, IntPtr.Zero);
                                fullImage.Dispose();
                                returnImageList.Images.Add(dr[attachTablePKeyName].ToString(), thumbnailImage);
                            }
                        }

                    }
                    else
                    {
                        // This attachment is not a supported image so use an icon image instead...
                        string[] matchingFileExtImages = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Images\AttachmentWizard\", "file_extension_" + fi.Extension.TrimStart('.') + ".*", System.IO.SearchOption.TopDirectoryOnly);
                        if (matchingFileExtImages.Length > 0)
                        {
                            Image fullImage = Image.FromFile(matchingFileExtImages[0]);
                            Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width, fullImage.Height, null, IntPtr.Zero);
                            fullImage.Dispose();
                            returnImageList.Images.Add(dr[attachTablePKeyName].ToString(), thumbnailImage);
                        }
                        else if (dr["category_code"].ToString().ToUpper() == "LINK" ||
                            dr["content_type"].ToString().ToUpper().Contains("HTML"))
                        {
                            matchingFileExtImages = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Images\AttachmentWizard\", "file_extension_html" + ".*", System.IO.SearchOption.TopDirectoryOnly);
                            if (matchingFileExtImages.Length > 0)
                            {
                                Image fullImage = Image.FromFile(matchingFileExtImages[0]);
                                Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width, fullImage.Height, null, IntPtr.Zero);
                                fullImage.Dispose();
                                returnImageList.Images.Add(dr[attachTablePKeyName].ToString(), thumbnailImage);
                            }
                        }
                        else
                        {
                            matchingFileExtImages = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + @"\Images\AttachmentWizard\", "file_extension_unknown" + ".*", System.IO.SearchOption.TopDirectoryOnly);
                            if (matchingFileExtImages.Length > 0)
                            {
                                Image fullImage = Image.FromFile(matchingFileExtImages[0]);
                                Image thumbnailImage = fullImage.GetThumbnailImage(fullImage.Width, fullImage.Height, null, IntPtr.Zero);
                                fullImage.Dispose();
                                returnImageList.Images.Add(dr[attachTablePKeyName].ToString(), thumbnailImage);
                            }
                        }
                    }
                }
                i++;
                UpdateProgressBar(true, 0, attachTable.Rows.Count, i, "Processing Attachment Files...");
            }

            return returnImageList;
        }

        private ImageList ResizeImageList(ImageList sourceImageList, int newSize)
        {
            ImageList returnImageList = new ImageList();
            returnImageList.ImageSize = new Size(newSize, newSize);
            returnImageList.ColorDepth = ColorDepth.Depth32Bit;

            foreach (string imagePKey in sourceImageList.Images.Keys)
            {
                Image sourceImage = sourceImageList.Images[imagePKey];
                Image thumbnailImage = sourceImage.GetThumbnailImage(Math.Min(newSize, sourceImage.Width), Math.Min(newSize, sourceImage.Height), null, IntPtr.Zero);
                returnImageList.Images.Add(imagePKey, thumbnailImage);
            }

            return returnImageList;
        }

        private void BuildTreeView()
        {
            ux_treeviewList.Nodes.Clear();
            ux_treeviewList.ShowNodeToolTips = true;
            ux_treeviewList.ImageList = GetTreeviewImageList();
            ux_treeviewList.ImageKey = "inactive_unknown";
            ux_treeviewList.SelectedImageKey = "active_unknown";
            ux_treeviewList.Sorted = false;

            TreeNode rootNode = new TreeNode();
            rootNode.Name = _currentFolderPathOrFileNames;
            rootNode.Text = "Attachments";
            rootNode.Tag = "ROOTFOLDER";
            rootNode.ImageKey = "inactive_folder";
            rootNode.SelectedImageKey = "active_folder";
            //rootNode.ToolTipText = "Hello World";

            // Process all of the attachment records that *should* have a parent folder...
            foreach (DataRowView drv in _attachTable.DefaultView)
            {
                TreeNode newAttachNode = new TreeNode();
                TreeNode currentAttachFolderNode = new TreeNode();
                string nodeText = _sharedUtils.GetLookupDisplayMember(_attachParentPKeyName.Replace("_id", "_lookup"), drv[_attachParentPKeyName].ToString(), "", drv[_attachParentPKeyName].ToString());
                if (!rootNode.Nodes.ContainsKey(drv[_attachParentPKeyName].ToString()))
                {
                    // This parent folder node doesn't exist yet - so create it now...
                    TreeNode newAttachFolderNode = new TreeNode();
                    newAttachFolderNode.Name = drv[_attachParentPKeyName].ToString();
                    newAttachFolderNode.Text = nodeText;
                    newAttachFolderNode.Tag = "FOLDER";
                    newAttachFolderNode.ImageKey = "inactive_folder";
                    newAttachFolderNode.SelectedImageKey = "active_folder";
                    //newAttachFolderNode.ToolTipText = "Hello World";
                    rootNode.Nodes.Add(newAttachFolderNode);
                    currentAttachFolderNode = newAttachFolderNode;
                }
                else
                {
                    currentAttachFolderNode = rootNode.Nodes[drv[_attachParentPKeyName].ToString()];
                }
                //string[] fileNameTokens = drv["virtual_path"].ToString().Split('\\', '/', ':');
                string[] fileNameTokens = drv["virtual_path"].ToString().Split('\\');
                newAttachNode.Name = drv[_attachTable.PrimaryKey[0].ColumnName].ToString();
                newAttachNode.Text = fileNameTokens[fileNameTokens.Length - 1];
                newAttachNode.Tag = "ATTACHMENT";
                newAttachNode.ImageKey = "inactive_" + _attachParentPKeyName.Replace("_id", "_attach_id");
                newAttachNode.SelectedImageKey = "active_" + _attachParentPKeyName.Replace("_id", "_attach_id");
                newAttachNode.ToolTipText = drv["virtual_path"].ToString();
                currentAttachFolderNode.Nodes.Add(newAttachNode);
            }

            // Create an empty folder for each PKey being processed currently...
            if (_currentPKeys.ToLower().Trim().StartsWith(_attachParameterName) ||
                _currentPKeys.ToLower().Trim().StartsWith(":" + _attachParentPKeyName.Replace("_", "")))
            {
                string pkeys = _currentPKeys.Split('=')[1];
                string[] PKeyIDs = pkeys.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                // Process all of the PKeys that may (or may not) have attachment records - create a parent folder
                // for each PKey in the list...
                foreach (string pkey in PKeyIDs)
                {
                    string friendlyName = _sharedUtils.GetLookupDisplayMember(_attachParentPKeyName.Replace("_id", "_lookup"), pkey, "", pkey);
                    if (!rootNode.Nodes.ContainsKey(pkey))
                    {
                        // This parent folder node doesn't exist yet - so create it now...
                        TreeNode newAttachFolderNode = new TreeNode();
                        newAttachFolderNode.Name = pkey;
                        newAttachFolderNode.Text = friendlyName;
                        newAttachFolderNode.Tag = "FOLDER";
                        newAttachFolderNode.ImageKey = "inactive_folder";
                        newAttachFolderNode.SelectedImageKey = "active_folder";
                        //newAttachFolderNode.ToolTipText = "Hello World";
                        rootNode.Nodes.Add(newAttachFolderNode);
                    }
                }
            }

            if (rootNode.Nodes.Count > 0)
            {
                ux_splitcontainerAttachmentDataDisplay.Enabled = true;
            }
            else
            {
                ux_splitcontainerAttachmentDataDisplay.Enabled = false;
            }

            ux_treeviewList.Nodes.Add(rootNode);
            ux_treeviewList.ExpandAll();
            //ux_treeviewList.Sort();
        }

        private ImageList GetTreeviewImageList()
        {
            if (_treeviewImageList == null)
            {
                ImageList treeviewImageList = new ImageList();

                // Load the images for the tree view(s)...
                treeviewImageList.ColorDepth = ColorDepth.Depth32Bit;

                // Load the icons from the image sub-directrory (this directory is found under the directory the Curator Tool was launched from)...
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory());
                System.IO.FileInfo[] iconFiles = di.GetFiles("Images\\TreeView\\*.ico", System.IO.SearchOption.TopDirectoryOnly);
                if (iconFiles != null && iconFiles.Length > 0)
                {
                    for (int i = 0; i < iconFiles.Length; i++)
                    {
                        treeviewImageList.Images.Add(iconFiles[i].Name.TrimEnd('.', 'i', 'c', 'o'), Icon.ExtractAssociatedIcon(iconFiles[i].FullName));
                    }
                }

                // Cache the list so you don't have to load it again...
                _treeviewImageList = treeviewImageList;
            }

            return _treeviewImageList;
        }

        private int FindPKeyFromFileNameOrPath(string fileName)
        {
            switch (_attachParentPKeyName.Trim().ToLower())
            {
                case "inventory_id":
                    int inventoryID = FindInventoryIDFromFileNameOrPath(fileName);
                    // If the 'Accession' radio button is selected make sure this is the system '**' inventory record...
                    if(_attachParameterName.Trim().ToLower().Contains("accession"))
                    {
                        if (!_sharedUtils.GetLookupDisplayMember("inventory_lookup", inventoryID.ToString(), "", inventoryID.ToString()).ToLower().Trim().Contains("**"))
                        {
                            // The user wants the attachments assigned to the accession, but this is not the '**' inventory - so get it now...
                            DataTable dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE value_member=" + inventoryID.ToString().Trim(), "");
                            if (dt != null &&
                                dt.Rows.Count == 1)
                            {
                                dt = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE '%**' AND accession_id=" + dt.Rows[0]["accession_id"].ToString().Trim(), "");
                                if (dt != null &&
                                    dt.Rows.Count == 1)
                                {
                                    inventoryID = (int)dt.Rows[0]["value_member"];
                                }
                            }
                        }
                    }
                    return inventoryID;
                case "accession_inv_group_id":
                case "crop_id":
                case "crop_trait_id":
                case "crop_trait_code_id":
                case "method_id":
                case "order_request_id":
                case "taxonomy_family_id":
                case "taxonomy_genus_id":
                case "taxonomy_species_id":
                    return FindPKeyIDFromFileNameOrPath(fileName, _attachParentPKeyName.Trim().ToLower());
                default:
                    return -1;
            }
        }

        private string[] BuildFileNameTokens(System.IO.FileInfo fi)
        {
            string[] fileNameTokens;
            // Create the filename tokens based on which parsing method the user has chosen...
            if (_parseMethod.ToLower() == "fixedfieldparse")
            {
                int i = 0;
                int start = 0;
                int length = 0;
                fileNameTokens = new string[_fixedFieldTokenLengths.Length];
                foreach (int tokenLength in _fixedFieldTokenLengths)
                {
                    length = Math.Min(tokenLength, fi.Name.IndexOf(fi.Extension) - start);
                    fileNameTokens[i] = fi.Name.Substring(start, length).Trim();
                    i++;
                    start += length;
                }
            }
            else if (_parseMethod.ToLower() == "delimitedparse")
            {
                fileNameTokens = fi.Name.TrimEnd(fi.Extension.ToCharArray()).Split(_delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                fileNameTokens = fi.Name.TrimEnd(fi.Extension.ToCharArray()).Split(new char[] { ' ', '_' }, StringSplitOptions.RemoveEmptyEntries);
            }

            return fileNameTokens;
        }

        private int FindInventoryIDFromFileNameOrPath(string fileName)
        {
            int returnID = -1;
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string[] fileNameTokens = new string[0];

            if (fi != null) fileNameTokens = BuildFileNameTokens(fi);

            // Look for a known prefix + number combination (requires 2 adjacent tokens somewhere in the filename)...
            for (int i = 0; ((i < fileNameTokens.Length - 1) && (returnID < 0)); i++)
            {
                if (IsPrefix(" " + fileNameTokens[i].Trim() + " "))
                {
                    // We found a token in the filename that matches an allowed inventory prefix - let the fun begin...
                    // so in theory the next token *must be* an integer...
                    if (IsInteger(" " + fileNameTokens[i + 1].Trim() + " "))
                    {
                        // Remove leading zeros and spaces...
                        int fileNameTokenInt = int.Parse(" " + fileNameTokens[i + 1].Trim() + " ");
                        fileNameTokens[i + 1] = fileNameTokenInt.ToString();
                        // Found an integer - so add that to the prefix and search the LUT for a match...
                        string inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + "%";
                        DataTable dtPart12 = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                        if (dtPart12.Rows.Count == 0)
                        {
                            // Nothing in the inventory LUT matches this filename token so move on to the next token and keep looking...
                            continue;
                        }
                        else if (dtPart12.Rows.Count == 1)
                        {
                            // The only match in the inventory LUT is prefix + number (most likely because it is an accessin number) so return that...
                            returnID = (int)dtPart12.Rows[0]["value_member"];
                            break;
                        }
                        else if (dtPart12.Rows.Count > 1)
                        {
                            // More than one matching inventory lotcode so see if we can add one
                            // or more tokens to find a single match...
                            if (i < fileNameTokens.Length - 2)
                            {
                                inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + " " + fileNameTokens[i + 2] + "%";
                                DataTable dtPart123 = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                if (dtPart123.Rows.Count == 0)
                                {
                                    // The follow-on token does not match any inventory lotcodes so this must be the accession number 
                                    // to be sure we will go find the '**' inventory_id and make sure we get one match...
                                    inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + " **%";
                                    dtPart123 = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                    if (dtPart123.Rows.Count == 1)
                                    {
                                        // The only match in the inventory LUT is prefix + number + '**' (aka the 'system inventory' for the accession) so return that...
                                        returnID = (int)dtPart123.Rows[0]["value_member"];
                                        break;
                                    }
                                    else
                                    {
                                        // Nothing in the inventory LUT matches this filename token so move on to the next token and keep looking...
                                        continue;
                                    }
                                }
                                else if (dtPart123.Rows.Count == 1)
                                {
                                    // The only match in the inventory LUT is prefix + number + (either suffix or form_type_code) so return that...
                                    returnID = (int)dtPart123.Rows[0]["value_member"];
                                    break;
                                }
                                else if (dtPart123.Rows.Count > 1)
                                {
                                    if (i < fileNameTokens.Length - 3)
                                    {
                                        // Still more than one matching inventory lotcode so see if we can add the form_type_code 
                                        // in order to narrow it down to a single match...
                                        inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + " " + fileNameTokens[i + 2] + " " + fileNameTokens[i + 3];
                                        DataTable dtPart123Form = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                        if (dtPart123Form.Rows.Count == 1)
                                        {
                                            // The only match in the inventory LUT is prefix + number + '**' (aka the 'system inventory' for the accession) so return that...
                                            returnID = (int)dtPart123Form.Rows[0]["value_member"];
                                            break;
                                        }
                                        else
                                        {
                                            // Nothing in the inventory LUT matches this filename token so move on to the next token and keep looking...
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        // We are out of tokens to try so this must be the accession number 
                                        // to be sure we will go find the '**' inventory_id and make sure we get one match...
                                        inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + " " + fileNameTokens[i + 2] + " **";
                                        DataTable dtPart123SystemForm = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                        if (dtPart123SystemForm.Rows.Count == 1)
                                        {
                                            // The only match in the inventory LUT is prefix + number + '**' (aka the 'system inventory' for the accession) so return that...
                                            returnID = (int)dtPart123SystemForm.Rows[0]["value_member"];
                                            break;
                                        }
                                        else
                                        {
                                            // Nothing in the inventory LUT matches this filename token so move on to the next token and keep looking...
                                            continue;
                                        }
                                    }
                                }
                            }
                            else if (i < fileNameTokens.Length - 1)
                            {
                                // The follow-on token does not match any inventory lotcodes so this must be the accession number 
                                // to be sure we will go find the '**' inventory_id and make sure we get one match...
                                inventoryNumberSearch = fileNameTokens[i] + " " + fileNameTokens[i + 1] + " **%";
                                DataTable dtPart123 = _sharedUtils.GetLocalData("SELECT * FROM inventory_lookup WHERE display_member LIKE @displaymember", "@displaymember=" + inventoryNumberSearch);
                                if (dtPart123.Rows.Count == 1)
                                {
                                    // The only match in the inventory LUT is prefix + number + '**' (aka the 'system inventory' for the accession) so return that...
                                    returnID = (int)dtPart123.Rows[0]["value_member"];
                                    break;
                                }
                                else
                                {
                                    // Nothing in the inventory LUT matches this filename token so move on to the next token and keep looking...
                                    continue;
                                }
                            }
                            else
                            {
                            }
                        }
                    }
                    else
                    {
                    }
                }
            }

            // If an inventory/accession number was not found in the filename itself - try to find a number in the path...
            if (returnID == -1 &&
                !string.IsNullOrEmpty(fi.DirectoryName) &&
                !_ignoreFolderNames)
            {
//// We need to change the file path delimeter ('\' OR '/') to the '_' so it can be processed like a filenamelook like a filename so replace the '/' and '\' with '_/
//string pathName = fi.DirectoryName.Replace('\\', '_').Replace('/', '_');
                returnID = FindPKeyFromFileNameOrPath(fi.DirectoryName);
            }

            return returnID;
        }

        private int FindPKeyIDFromFileNameOrPath(string fileName, string parentPKeyName)
        {
            int returnID = -1;
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string lutTableName = parentPKeyName.Trim().ToLower().Replace("_id", "_lookup");
            string[] fileNameTokens = new string[0];

            if (fi != null) fileNameTokens = BuildFileNameTokens(fi);

            // If a LUT based on the pkeyName doesn't exist - bail out now...
            if (!_sharedUtils.LocalDatabaseTableExists(lutTableName)) return returnID;

            // Look for any rows in the LUT that match a filename token...
            for (int i = 0; ((i < fileNameTokens.Length) && (returnID < 0)); i++)
            {
                string searchString = fileNameTokens[i] + "%";
                DataTable dtLUTMatch = _sharedUtils.GetLocalData("SELECT * FROM " + lutTableName + " WHERE display_member LIKE @displaymember", "@displaymember=" + searchString);
                int searchInt;
                if (dtLUTMatch != null &&
                    dtLUTMatch.Rows.Count < 1 &&
                    int.TryParse(fileNameTokens[i].Trim().ToLower(), out searchInt))
                {
                    dtLUTMatch = _sharedUtils.GetLocalData("SELECT * FROM " + lutTableName + " WHERE value_member = @valuemember", "@valuemember=" + searchInt);
                }
                foreach (DataRow dr in dtLUTMatch.Rows)
                {
                    if (fileName.Replace(" ", "").Replace("_", "").Trim().ToLower().Contains(dr["display_member"].ToString().Replace(" ", "").Replace("_", "").Trim().ToLower()) ||
                        fileName.Replace(" ", "").Replace("_", "").Trim().ToLower().Contains(dr["value_member"].ToString().Trim().ToLower()))
                    {
                        returnID = (int)dr["value_member"];
                        break;
                    }
                }
                if (returnID > 0) break;
            }

            // If a PKey was not found in the filename itself - try to find a match in the path...
            if (returnID == -1 &&
                !string.IsNullOrEmpty(fi.DirectoryName) &&
                !_ignoreFolderNames)
            {
                returnID = FindPKeyFromFileNameOrPath(fi.DirectoryName);
            }

            return returnID;
        }

        private DateTime FindDateFromFileNameOrPath(string fileName)
        {
            bool foundDate = false;
            DateTime returnDate = DateTime.Now.Date;
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string[] fileNameTokens = new string[0];

            if (fi != null) fileNameTokens = BuildFileNameTokens(fi);

            // First try to find the date in the filename itself...
            foreach (string token in fileNameTokens)
            {
                if (DateTime.TryParse(token, out returnDate))
                {
                    foundDate = true;
                    break;
                }
            }

            // If the filename doesn't contain a valid date look in the path of the file for a date...
            if (!foundDate &&
                !string.IsNullOrEmpty(fi.DirectoryName) &&
                !_ignoreFolderNames)
            {
                returnDate = FindDateFromFileNameOrPath(fi.DirectoryName);
            }

            return returnDate;
        }

        private string FindDescriptionCodeFromFileNameOrPath(string fileName)
        {
            bool foundContentType = false;
            string returnContentType = "";
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string[] fileNameTokens = new string[0];

            if (fi != null) fileNameTokens = BuildFileNameTokens(fi);

            // First try to find the description code in the filename itself...
            foreach (string token in fileNameTokens)
            {
                if (IsDescriptionCode(token))
                {
                    returnContentType = GetValidDescriptionCode(token);
                    foundContentType = true;
                    break;
                }
            }

            // If the filename doesn't contain a valid description code look in the path of the file for a date...
            if (!foundContentType &&
                !string.IsNullOrEmpty(fi.DirectoryName) &&
                !_ignoreFolderNames)
            {
                returnContentType = FindDescriptionCodeFromFileNameOrPath(fi.DirectoryName);
            }

            return returnContentType;
        }

        private string FindContentTypeFromFileNameOrPath(string fileName)
        {
            string returnContentType = System.Web.MimeMapping.GetMimeMapping(fileName);
            return returnContentType;
        }

        private string FindCategoryCodeFromFileName(string fileName)
        {
            string returnCategoryCode = "";
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string mimeType = System.Web.MimeMapping.GetMimeMapping(fileName);

            // First try to detect if this is an image from the .NET mime mapping...
            if (mimeType.ToUpper().Contains("IMAGE"))
            {
                returnCategoryCode = "IMAGE";
            }
            // Next try to detect if this is an image from the file extension...
            else if (_supportedImageExtensions.Contains(fi.Extension))
            {
                returnCategoryCode = "IMAGE";
            }
            else
            // Couldn't detect that the file was an image so fallback to a default of documewnt type...
            {
                returnCategoryCode = "DOCUMENT";
            }

            return returnCategoryCode;
        }

        private bool IsPrefix(string stringToken)
        {
            string token = stringToken;
            string allowedPrefix = _allowedInventoryPrefix;
            if (!_useCaseSensitiveFileNames)
            {
                token = stringToken.ToLower();
                allowedPrefix = _allowedInventoryPrefix.ToLower();
            }

            return allowedPrefix.Contains(" " + token.Trim() + " ");
        }

        private bool IsInteger(string stringToken)
        {
            int intResult = 0;

            return int.TryParse(stringToken.Trim(), out intResult);
        }

        private bool IsFormTypeCode(string stringToken)
        {
            string token = stringToken;
            string allowedFormTypeCode = _allowedFormTypeCode;
            if (!_useCaseSensitiveFileNames)
            {
                token = stringToken.ToLower();
                allowedFormTypeCode = _allowedFormTypeCode.ToLower();
            }

            return allowedFormTypeCode.Contains(" " + token.Trim() + " ");
        }

        private bool IsDescriptionCode(string stringToken)
        {
            string token = stringToken;
            string allowedDescriptionCode = _allowedDescriptionCode;
            if (!_useCaseSensitiveFileNames)
            {
                token = stringToken.ToLower();
                allowedDescriptionCode = _allowedDescriptionCode.ToLower();
            }

            return allowedDescriptionCode.Contains(" " + token.Trim() + " ");
        }

        private DataTable GetAttachmentFilePaths(string validPKeys, string attachFilePathDataview)
        {
            DataTable returnDT = new DataTable();
            string[] pkeyTokens = validPKeys.Split('=');
            if (pkeyTokens != null && 
                pkeyTokens.Length == 2 &&
                !string.IsNullOrEmpty(_attachParentPKeyName) &&
                !string.IsNullOrEmpty(_attachDataviewName) &&
                !string.IsNullOrEmpty(attachFilePathDataview))
            {
                // STEP 1 - generate the 'default' file paths for all valid PKeys...
                // Get all of the parent PKeys and their 'friendly names'...
                string pkeys = string.IsNullOrEmpty(pkeyTokens[1].Trim()) ? "null" : pkeyTokens[1].Trim();
                string sqlQuery = "SELECT * FROM " + _attachParentPKeyName.Trim().ToLower().Replace("_id", "_lookup") + " WHERE value_member IN (" + pkeys + ")";
                returnDT = _sharedUtils.GetLocalData(sqlQuery, "@valuemember=" + pkeyTokens[1].Trim());
                // Add the filepath column and rename the PKey column for the merge code below...
                returnDT.Columns.Add("attach_filepath", typeof(string));
                returnDT.Columns["value_member"].ColumnName = _attachParentPKeyName.Trim().ToLower();
                // Populate the attach_filepath column with data...
                switch (_attachParentPKeyName.Trim().ToLower())
                {
                    case "inventory_id":
                        foreach (DataRow dr in returnDT.Rows)
                        {
                            dr["attach_filepath"] = _attachDataviewName.Trim().ToLower().Replace("get_", "") + "/" + dr["display_member"].ToString().Replace("*", "").Trim().Replace(" ", "_") + "/";
                        }
                        break;
                    case "accession_inv_group_id":
                    case "crop_id":
                    case "crop_trait_id":
                    case "crop_trait_code_id":
                    case "method_id":
                    case "taxonomy_family_id":
                    case "taxonomy_genus_id":
                    case "taxonomy_species_id":
                        foreach (DataRow dr in returnDT.Rows)
                        {
                            dr["attach_filepath"] = _attachDataviewName.Trim().ToLower().Replace("get_", "") + "/" + dr["display_member"].ToString().Replace(" ", "_") + "/";
                        }
                        break;
                    case "order_request_id":
                        foreach (DataRow dr in returnDT.Rows)
                        {
                            dr["attach_filepath"] = _attachDataviewName.Trim().ToLower().Replace("get_", "") + "/" + dr[_attachParentPKeyName.Trim().ToLower()].ToString().Replace(" ", "_") + "/";
                        }
                        break;
                    default:
                        break;
                }

                // STEP 2 - see if there is a dataview on the server that overrides the default file paths generated above...
                DataSet dsFilePath = _sharedUtils.GetWebServiceData(attachFilePathDataview, validPKeys, 0, 0);

                if (dsFilePath != null &&
                    dsFilePath.Tables.Contains(attachFilePathDataview) &&
                    dsFilePath.Tables[attachFilePathDataview].Rows.Count > 0)
                {
                    //  A dataview returned filepath results so merge these results with the default filepaths...
                    returnDT.Merge(dsFilePath.Tables[attachFilePathDataview], false);
                }
            }

            return returnDT;
        }

        private string GetValidDescriptionCode(string token)
        {
            string returnValidCode = token;
            string[] validCodes = _allowedDescriptionCode.Split(' ');

            foreach (string validCode in validCodes)
            {
                if (validCode.ToLower() == token.ToLower())
                {
                    return validCode;
                }
            }

            return returnValidCode;
        }

        private bool IsFileIncluded(System.IO.FileInfo fi)
        {
            bool included = false;
            string[] filters = _includeFileFilter.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string filter in filters)
            {
                string pattern = "^" + filter.Trim().Replace(".", "\\.").Replace("*", ".*") + "$";
                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (r.IsMatch(fi.Name))
                {
                    included = true;
                    break;
                }
            }

            return included;
        }

        private bool IsFileExcluded(System.IO.FileInfo fi)
        {
            bool excluded = false;
            string[] filters = _excludedFileFilter.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string filter in filters)
            {
                string pattern = "^" + filter.Trim().Replace(".", "\\.").Replace("*", ".*") + "$";
                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (r.IsMatch(fi.Name))
                {
                    excluded = true;
                    break;
                }
            }

            return excluded;
        }

        private string LoadPKeyList(string pKeys)
        {
            string returnPKeyList = _attachParameterName + "=";  // Default to accession_inv_attachment records
            string parentPKeyType = _attachParameterName;
            string delimitedPKeys = "";

            // Check to see if more than one PKey type is passed in and
            // if so use only the first PKey type that contains PKey data...
            foreach (string pkeyToken in pKeys.Split(';'))
            {
                string[] pkeyKeyValue = pkeyToken.Split('=');
                if (pkeyKeyValue.Length == 2 &&
                    pkeyKeyValue[1].Length > 0)
                {
                    parentPKeyType = pkeyKeyValue[0].Trim().ToLower();
                    delimitedPKeys = pkeyKeyValue[1].Trim();
                    break;
                }
            }

            switch (parentPKeyType)
            {
                case ":accessioninvattachid":
                case ":accessioninvgroupattachid":
                case ":cropattachid":
                case ":croptraitattachid":
                case ":croptraitcodeattachid":
                case ":methodattachid":
                case ":orderrequestattachid":
                case ":taxonomyattachid":
                case ":inventoryid":
                case ":accessioninvgroupid":
                case ":cropid":
                case ":croptraitid":
                case ":croptraitcodeid":
                case ":methodid":
                case ":orderrequestid":
                case ":taxonomyfamilyid":
                case ":taxonomygenusid":
                case ":taxonomyspeciesid":
                    returnPKeyList = parentPKeyType + "=" + delimitedPKeys;
                    break;
                case ":accessionid":
                    string seQueryString = "@inventory.accession_id IN (" + delimitedPKeys + ")";
                    if (_sharedUtils != null)
                    {
                        // Use the Search Engine to find the inventory IDs...
                        DataSet ds = _sharedUtils.SearchWebService(seQueryString, true, true, null, "inventory", 0, 0);
                        // Build the inventory_id parameter string...
                        returnPKeyList = ":inventoryid=";
                        if (ds.Tables.Contains("SearchResult"))
                        {
                            foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                            {
                                returnPKeyList += dr["ID"].ToString() + ",";
                            }
                            returnPKeyList = returnPKeyList.TrimEnd(',');
                        }
                    }
                    break;
                default:
                    returnPKeyList = ":inventoryid=";
                    break;
            }

            return returnPKeyList;
        }

        private string LoadFilePathsParameterList(string pKeys)
        {
            string filepathList = "";
            if(pKeys.Contains(":filepaths="))
            {
                int startOfFilePath = pKeys.ToLower().IndexOf(":filepaths=");
                filepathList = pKeys.Substring(startOfFilePath + ":filepaths=".Length);
            }

            return filepathList;
        }

        private string LoadFormTypeCodes()
        {
            string returnFormTypeCodes = "";

            DataTable formTypeCodeTable = _sharedUtils.GetLocalData("SELECT * FROM code_value_lookup WHERE group_name = @groupname", "@groupname=germplasm_form");
            if (formTypeCodeTable != null &&
                formTypeCodeTable.Rows.Count > 0)
            {
                foreach (DataRow dr in formTypeCodeTable.Rows)
                {
                    returnFormTypeCodes += dr["display_member"].ToString() + " ";
                }
            }

            return " " + returnFormTypeCodes + " ";
        }

        private string LoadDescriptionCodes()
        {
            string returnDescriptionCodes = "";

            DataTable descriptionCodeTable = _sharedUtils.GetLocalData("SELECT * FROM code_value_lookup WHERE group_name = @groupname", "@groupname=attach_description_code");
            if (descriptionCodeTable != null &&
                descriptionCodeTable.Rows.Count > 0)
            {
                foreach (DataRow dr in descriptionCodeTable.Rows)
                {
                    returnDescriptionCodes += dr["value_member"].ToString() + " ";
                }
            }

            return " " + returnDescriptionCodes + " ";
        }

        private string LoadInventoryPrefix()
        {
            string returnInventoryPrefix = "";

            DataSet ds = _sharedUtils.GetWebServiceData("inventory_attach_wizard_get_distinct_prefix", "", 0, 0);
            if (ds != null &&
                ds.Tables.Contains("inventory_attach_wizard_get_distinct_prefix") &&
                ds.Tables["inventory_attach_wizard_get_distinct_prefix"].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables["inventory_attach_wizard_get_distinct_prefix"].Rows)
                {
                    returnInventoryPrefix += dr["prefix"].ToString() + " ";
                }
            }

            return " " + returnInventoryPrefix + " ";
        }

        private void ux_radiobuttonAttachmentType_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;

            // If the radio button is checked update the global variables...
            if (rb.Checked &&
                rb.Tag != null &&
                rb.Tag.GetType() == typeof(string))
            {
                string[] controlTokens = ((string)rb.Tag).Split(';');
                if (controlTokens.Length == 3)
                {
                    _attachParameterName = controlTokens[0].Trim().ToLower().Split(',')[0];
                    _attachDataviewName = controlTokens[1].Trim().ToLower();
                    _attachParentPKeyName = controlTokens[2].Trim().ToLower();
                    // Disable the 'Batch Files' button if the crop_trait or crop_trait_code 
                    // radio buttons have been selected...
                    ux_buttonBatchFiles.Enabled = true;
                    if (_attachParentPKeyName.Trim().ToLower().Contains("crop_trait")) ux_buttonBatchFiles.Enabled = false;
                }
            }

            // Check to see if there are unsaved attachment changes and if so
            // ask the user if they would like to save them before switching attach tables...
            else if (!rb.Checked &&
                rb.Tag != null &&
                rb.Tag.GetType() == typeof(string))
            {
                // Test to see if there are unsaved changes and alert the user in case they want 
                // to save the edits before clearing the data...
                if (_attachTable != null &&
                    _attachTable.GetChanges() != null)
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to abandon your edits??", "Cancel Edits", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "AttachmentWizard_AttachmentTypeCheckChanged1";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    argsArray[0] = _attachTable.GetChanges().Rows.Count.ToString();
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    if (DialogResult.Yes == ggMessageBox.ShowDialog())
                    {
                        // Clear everything and start out fresh...
                        _currentPKeys = _attachParameterName + "="; ;
                        _previousPKeys = "";
                        _currentFolderPathOrFileNames = "";
                        _previousFolderPathOrFileNames = "";
                        if (_attachTable != null) _attachTable.Clear();
                        if (ux_treeviewList != null) ux_treeviewList.Nodes.Clear();
                        // Rebuild the control groups with no parameters...
                        BuildTreeView();
                        BuildListView();
                        RefreshData();
                    }
                }
                else
                {
                    // Clear everything and start out fresh...
                    _currentPKeys = _attachParameterName + "="; ;
                    _previousPKeys = "";
                    _currentFolderPathOrFileNames = "";
                    _previousFolderPathOrFileNames = "";
                    if (_attachTable != null) _attachTable.Clear();
                    if (ux_treeviewList != null) ux_treeviewList.Nodes.Clear();
                    // Rebuild the control groups with no parameters...
                    BuildTreeView();
                    BuildListView();
                    RefreshData();
                }
            }
        }

        private void ux_radiobuttonView_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (rb.Checked)
            {
                ux_listviewAttachments.View = (View)rb.Tag;
                if (ux_listviewAttachments.View == View.SmallIcon)
                {
                    ux_listviewAttachments.SmallImageList = _smallImageList64;
                }
                else
                {
                    ux_listviewAttachments.SmallImageList = _smallImageList32;
                }
                CalculateListViewColumnWidth();
            }
        }

        private void CalculateListViewColumnWidth()
        {
            int maxListViewWidth = ux_listviewAttachments.Width - SystemInformation.VerticalScrollBarWidth;
            int minFileNameWidth = 0;
            if (ux_listviewAttachments.Columns.ContainsKey("File Name")) minFileNameWidth = TextRenderer.MeasureText(ux_listviewAttachments.Columns["File Name"].Text, ux_listviewAttachments.Font).Width + ux_listviewAttachments.Margin.Size.Width;
            int maxFileNameWidth = 60;
            int maxFileNameTextLengthIndex = 0;
            int minDescriptionCodeWidth = 0;
            if (ux_listviewAttachments.Columns.ContainsKey("Description Code")) minDescriptionCodeWidth = TextRenderer.MeasureText(ux_listviewAttachments.Columns["Description Code"].Text, ux_listviewAttachments.Font).Width + ux_listviewAttachments.Margin.Size.Width;
            int maxDescriptionCodeWidth = 60;
            int maxDescriptionCodeTextLengthIndex = 0;
            int minTitleWidth = 0;
            if (ux_listviewAttachments.Columns.ContainsKey("Title")) minTitleWidth = TextRenderer.MeasureText(ux_listviewAttachments.Columns["Title"].Text, ux_listviewAttachments.Font).Width + ux_listviewAttachments.Margin.Size.Width;
            int maxTitleWidth = 60;
            int maxTitleTextLengthIndex = 0;
            int minDescriptionWidth = 0;
            if (ux_listviewAttachments.Columns.ContainsKey("Description")) minDescriptionWidth = TextRenderer.MeasureText(ux_listviewAttachments.Columns["Description"].Text, ux_listviewAttachments.Font).Width + ux_listviewAttachments.Margin.Size.Width;
            int maxDescriptionWidth = 60;
            int maxDescriptionTextLengthIndex = 0;
            if (ux_listviewAttachments.View == View.Details ||
                ux_listviewAttachments.View == View.List ||
                ux_listviewAttachments.View == View.Tile)
            {
                // Calculate the max filename text length in pixels to set the File Name column width...
                if (ux_listviewAttachments.Items.Count > 0)
                {
                    if (ux_listviewAttachments.Columns.ContainsKey("File Name"))
                    {
                        int subItemIndex = ux_listviewAttachments.Columns["File Name"].Index;
                        // Default to column header text width...
                        maxFileNameWidth = minFileNameWidth;
                        foreach (ListViewItem lvi in ux_listviewAttachments.Items)
                        {
                            if (lvi.SubItems[subItemIndex].Text.Length > ux_listviewAttachments.Items[maxFileNameTextLengthIndex].SubItems[subItemIndex].Text.Length)
                            {
                                maxFileNameTextLengthIndex = lvi.Index;
                            }
                        }
                        maxFileNameWidth = Math.Max(maxFileNameWidth, TextRenderer.MeasureText(ux_listviewAttachments.Items[maxFileNameTextLengthIndex].SubItems[subItemIndex].Text.ToUpper(), ux_listviewAttachments.Font).Width + ux_listviewAttachments.SmallImageList.ImageSize.Width + ux_listviewAttachments.Margin.Size.Width);
                    }
                    // Set the description code column width to the width of the column header text...
                    if (ux_listviewAttachments.Columns.ContainsKey("Description Code"))
                    {
                        int subItemIndex = ux_listviewAttachments.Columns["Description Code"].Index;
                        // Default to column header text width...
                        maxDescriptionCodeWidth = minDescriptionCodeWidth;
                        foreach (ListViewItem lvi in ux_listviewAttachments.Items)
                        {
                            if (lvi.SubItems[subItemIndex].Text.Length > ux_listviewAttachments.Items[maxDescriptionCodeTextLengthIndex].SubItems[subItemIndex].Text.Length)
                            {
                                maxDescriptionCodeTextLengthIndex = lvi.Index;
                            }
                        }
                        maxDescriptionCodeWidth = Math.Max(maxDescriptionCodeWidth, TextRenderer.MeasureText(ux_listviewAttachments.Items[maxDescriptionCodeTextLengthIndex].SubItems[subItemIndex].Text, ux_listviewAttachments.Font).Width + ux_listviewAttachments.Margin.Size.Width);
                    }
                    if (ux_listviewAttachments.Columns.ContainsKey("Title"))
                    {
                        int subItemIndex = ux_listviewAttachments.Columns["Title"].Index;
                        // Default to column header text width...
                        maxTitleWidth = minTitleWidth;
                        foreach (ListViewItem lvi in ux_listviewAttachments.Items)
                        {
                            if (lvi.SubItems[subItemIndex].Text.Length > ux_listviewAttachments.Items[maxTitleTextLengthIndex].SubItems[subItemIndex].Text.Length)
                            {
                                maxTitleTextLengthIndex = lvi.Index;
                            }
                        }
                        maxTitleWidth = Math.Max(maxTitleWidth, TextRenderer.MeasureText(ux_listviewAttachments.Items[maxTitleTextLengthIndex].SubItems[subItemIndex].Text, ux_listviewAttachments.Font).Width + ux_listviewAttachments.Margin.Size.Width);
                    }
                    if (ux_listviewAttachments.Columns.ContainsKey("Description"))
                    {
                        int subItemIndex = ux_listviewAttachments.Columns["Description"].Index;
                        // Default to column header text width...
                        maxDescriptionWidth = minDescriptionWidth;
                        foreach (ListViewItem lvi in ux_listviewAttachments.Items)
                        {
                            if (lvi.SubItems[subItemIndex].Text.Length > ux_listviewAttachments.Items[maxDescriptionTextLengthIndex].SubItems[subItemIndex].Text.Length)
                            {
                                maxDescriptionTextLengthIndex = lvi.Index;
                            }
                        }
                        maxDescriptionWidth = Math.Max(maxDescriptionWidth, TextRenderer.MeasureText(ux_listviewAttachments.Items[maxDescriptionTextLengthIndex].SubItems[subItemIndex].Text, ux_listviewAttachments.Font).Width + ux_listviewAttachments.Margin.Size.Width);
                    }

                    // Make the column width calculations maximizing column width in this priority: File Name, Description Code, Title, Description...
                    if (ux_listviewAttachments.Columns.ContainsKey("File Name"))
                    {
                        ux_listviewAttachments.Columns["File Name"].Width = Math.Min(maxFileNameWidth, maxListViewWidth - minDescriptionCodeWidth - minTitleWidth - minDescriptionWidth);
                    }
                    if (ux_listviewAttachments.Columns.ContainsKey("Description Code"))
                    {
                        ux_listviewAttachments.Columns["Description Code"].Width =
                            Math.Min(maxDescriptionCodeWidth, maxListViewWidth - (ux_listviewAttachments.Columns.ContainsKey("File Name") ? ux_listviewAttachments.Columns["File Name"].Width : minFileNameWidth) -
                            minTitleWidth - minDescriptionWidth);
                    }
                    if (ux_listviewAttachments.Columns.ContainsKey("Title"))
                    {
                        ux_listviewAttachments.Columns["Title"].Width =
                            Math.Min(maxTitleWidth, maxListViewWidth - (ux_listviewAttachments.Columns.ContainsKey("File Name") ? ux_listviewAttachments.Columns["File Name"].Width : minFileNameWidth) -
                            (ux_listviewAttachments.Columns.ContainsKey("Description Code") ? ux_listviewAttachments.Columns["Description Code"].Width : minDescriptionCodeWidth) -
                            minDescriptionWidth);
                    }
                    if (ux_listviewAttachments.Columns.ContainsKey("Description"))
                    {
                        ux_listviewAttachments.Columns["Description"].Width =
                            Math.Min(maxDescriptionWidth, maxListViewWidth - (ux_listviewAttachments.Columns.ContainsKey("File Name") ? ux_listviewAttachments.Columns["File Name"].Width : minFileNameWidth) -
                            (ux_listviewAttachments.Columns.ContainsKey("Description Code") ? ux_listviewAttachments.Columns["Description Code"].Width : minDescriptionCodeWidth) -
                            (ux_listviewAttachments.Columns.ContainsKey("Title") ? ux_listviewAttachments.Columns["Title"].Width : minTitleWidth) -
                            ux_listviewAttachments.Margin.Size.Width);
                    }

                    // If this is the Tile view - make sure the full filename is displayed
                    // by making the TileSize fit the max filename length...
                    if (ux_listviewAttachments.View == View.Tile)
                    {
                        ux_listviewAttachments.TileSize = new Size(_largeImageList.ImageSize.Width + ux_listviewAttachments.Columns["File Name"].Width, _largeImageList.ImageSize.Height + 4);
                    }
                }
            }
        }
    }
}