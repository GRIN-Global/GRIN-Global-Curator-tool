﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AttachmentWizard
{
    public partial class Options : Form
    {
        string _includeFilter = "";
        string _excludeFilter = "";
        string _parseMethod = "SmartParse";
        bool _caseSensitive = false;
        bool _ignoreFolderNames = false;
        char[] _delimiters = new char[2] {'_', ' '};
        int[] _fixedFieldTokenLengths = new int[12];

        public Options()
        {
            InitializeComponent();
        }

        public string IncludeFilter
        {
            get
            {
                return _includeFilter;
            }
            set
            {
                _includeFilter = value;
                ux_textboxIncludeFilter.Text = _includeFilter;
            }
        }
 
        public string ExcludeFilter
        {
            get
            {
                return _excludeFilter;
            }
            set
            {
                _excludeFilter = value;
                ux_textboxExcludeFilter.Text = _excludeFilter;
            }
        }

        public string ParseMethod
        {
            get
            {
                return _parseMethod;
            }
            set
            {
                _parseMethod = value;
                if (_parseMethod.Trim().ToLower() == "fixedfieldparse") ux_radiobuttonFixedFieldParse.Checked = true;
                else if (_parseMethod.Trim().ToLower() == "delimitedparse") ux_radiobuttonDelimitedParse.Checked = true;
                else ux_radiobuttonSmartParse.Checked = true; //_parseMethod = "SmartParse";
            }
        }

        public bool CaseSensitive
        {
            get
            {
                return _caseSensitive;
            }
            set
            {
                _caseSensitive = value;
                ux_checkboxCaseSensitive.Checked = _caseSensitive;
            }
        }

        public bool IgnoreFolderNames
        {
            get
            {
                return _ignoreFolderNames;
            }
            set
            {
                _ignoreFolderNames = value;
                ux_checkboxIgnoreFolderNames.Checked = _ignoreFolderNames;
            }
        }

        public char[] Delimiters
        {
            get
            {
                return _delimiters;
            }
            set
            {
                _delimiters = value;
                ux_checkboxUnderscore.Checked = false;
                ux_checkboxSpace.Checked = false;
                ux_checkboxDash.Checked = false;
                ux_checkboxOther.Checked = false;
                if (_delimiters.Contains('_')) ux_checkboxUnderscore.Checked = true;
                if (_delimiters.Contains(' ')) ux_checkboxSpace.Checked = true;
                if (_delimiters.Contains('-')) ux_checkboxDash.Checked = true;
                string otherDelimiters = new string(_delimiters).ToString().Replace("_", "").Replace(" ", "").Replace("-", "").Trim();
                if (otherDelimiters.Length > 0)
                {
                    ux_textboxOther.Text = otherDelimiters[0].ToString();
                    if (ux_textboxOther.Text.Trim().Length > 0)
                    {
                        ux_checkboxOther.Checked = true;
                        ux_textboxOther.Enabled = true;
                    }
                    else
                    {
                        ux_textboxOther.Text = "";
                        ux_checkboxOther.Checked = false;
                        ux_textboxOther.Enabled = false;
                    }
                }
            }
        }

        public int[] FixedFieldTokenLengths
        {
            get
            {
                return _fixedFieldTokenLengths;
            }
            set
            {
                _fixedFieldTokenLengths = value;
                for (int i = 0; i < _fixedFieldTokenLengths.Length; i++)
                {
                    // Find the control...
                    Control textboxControl = ux_groupboxFixedFieldTokens.Controls["ux_textboxFixedToken" + i.ToString()];
                    if (textboxControl != null)
                    {
                        // If the control was found set the text in the control to the int passed into this dialogbox...
                        textboxControl.Text = _fixedFieldTokenLengths[i].ToString();
                    }
                }
            }
        }

        private void ux_buttonOK_Click(object sender, EventArgs e)
        {
            // Capture the include filename filter...
            _includeFilter = ux_textboxIncludeFilter.Text;

            // Capture the exclude filename filter...
            _excludeFilter = ux_textboxExcludeFilter.Text;

            // Capture the parsing method...
            if (ux_radiobuttonFixedFieldParse.Checked)
            {
                _parseMethod = "FixedFieldParse";
            }
            else if (ux_radiobuttonDelimitedParse.Checked)
            {
                _parseMethod = "DelimitedParse";
            }
            else
            {
                _parseMethod = "SmartParse";
            }

            // Capture case sensitivity...
            _caseSensitive = ux_checkboxCaseSensitive.Checked;

            // Capture ignore folder names...
            _ignoreFolderNames = ux_checkboxIgnoreFolderNames.Checked;

            // Capture the delimiters...
            string delimiters = "";
            if (ux_checkboxUnderscore.Checked) delimiters += "_";
            if (ux_checkboxSpace.Checked) delimiters += " ";
            if (ux_checkboxDash.Checked) delimiters += "-";
            if (ux_checkboxOther.Checked) delimiters += ux_textboxOther.Text;
            _delimiters = delimiters.ToCharArray();

            // Capture the fixed field token lengths...
            int length = 0;
            for(int i=0; i<12; i++)
            {
                // Find the control...
                Control textboxControl = ux_groupboxFixedFieldTokens.Controls["ux_textboxFixedToken" + i.ToString()];
                if(textboxControl != null)
                {
                    // Set the default length to zero...
                    _fixedFieldTokenLengths[i] = 0;
                    // If the control was found try to convert the text in the control to an int...
                    if(int.TryParse(textboxControl.Text, out length)) _fixedFieldTokenLengths[i] = length;
                }
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void ux_buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void ux_radiobuttonSmartParse_CheckedChanged(object sender, EventArgs e)
        {
            ux_groupboxDelimiters.Enabled = false;
            ux_groupboxFixedFieldTokens.Enabled = false;
        }

        private void ux_radiobuttonDelimitedParse_CheckedChanged(object sender, EventArgs e)
        {
            ux_groupboxDelimiters.Enabled = true;
            ux_groupboxFixedFieldTokens.Enabled = false;
        }

        private void ux_radiobuttonFixedFieldParse_CheckedChanged(object sender, EventArgs e)
        {
            ux_groupboxDelimiters.Enabled = false;
            ux_groupboxFixedFieldTokens.Enabled = true;
        }

        private void ux_checkboxOther_CheckedChanged(object sender, EventArgs e)
        {
            if (ux_checkboxOther.Checked)
            {
                ux_textboxOther.Enabled = true;
            }
            else
            {
                ux_textboxOther.Enabled = false;
                ux_textboxOther.Text = "";
            }
            
        }
    }
}
