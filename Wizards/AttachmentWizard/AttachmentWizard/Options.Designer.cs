﻿namespace AttachmentWizard
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_labelIncludeFilter = new System.Windows.Forms.Label();
            this.ux_labelExcludeFilter = new System.Windows.Forms.Label();
            this.ux_textboxIncludeFilter = new System.Windows.Forms.TextBox();
            this.ux_textboxExcludeFilter = new System.Windows.Forms.TextBox();
            this.ux_groupboxAttachmentFileFilters = new System.Windows.Forms.GroupBox();
            this.ux_buttonOK = new System.Windows.Forms.Button();
            this.ux_buttonCancel = new System.Windows.Forms.Button();
            this.ux_groupboxFileNameAutomation = new System.Windows.Forms.GroupBox();
            this.ux_checkboxIgnoreFolderNames = new System.Windows.Forms.CheckBox();
            this.ux_groupboxFixedFieldTokens = new System.Windows.Forms.GroupBox();
            this.ux_labelFixedToken0 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken1 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken2 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken3 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken4 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken5 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken6 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken7 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken8 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken9 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken10 = new System.Windows.Forms.Label();
            this.ux_labelFixedToken11 = new System.Windows.Forms.Label();
            this.ux_textboxFixedToken0 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken1 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken2 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken3 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken4 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken5 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken6 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken7 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken8 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken9 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken10 = new System.Windows.Forms.TextBox();
            this.ux_textboxFixedToken11 = new System.Windows.Forms.TextBox();
            this.ux_checkboxCaseSensitive = new System.Windows.Forms.CheckBox();
            this.ux_groupboxDelimiters = new System.Windows.Forms.GroupBox();
            this.ux_checkboxDash = new System.Windows.Forms.CheckBox();
            this.ux_textboxOther = new System.Windows.Forms.TextBox();
            this.ux_checkboxUnderscore = new System.Windows.Forms.CheckBox();
            this.ux_checkboxOther = new System.Windows.Forms.CheckBox();
            this.ux_checkboxSpace = new System.Windows.Forms.CheckBox();
            this.ux_radiobuttonFixedFieldParse = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonDelimitedParse = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonSmartParse = new System.Windows.Forms.RadioButton();
            this.ux_groupboxAttachmentFileFilters.SuspendLayout();
            this.ux_groupboxFileNameAutomation.SuspendLayout();
            this.ux_groupboxFixedFieldTokens.SuspendLayout();
            this.ux_groupboxDelimiters.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_labelIncludeFilter
            // 
            this.ux_labelIncludeFilter.AutoSize = true;
            this.ux_labelIncludeFilter.Location = new System.Drawing.Point(6, 16);
            this.ux_labelIncludeFilter.Name = "ux_labelIncludeFilter";
            this.ux_labelIncludeFilter.Size = new System.Drawing.Size(362, 13);
            this.ux_labelIncludeFilter.TabIndex = 0;
            this.ux_labelIncludeFilter.Text = "Include:     (e.g. *.jpg; *.png; *.gif; *.xlsx; *.docx; *.pptx; *.pdf; *.txt; *.r" +
    "tf; *.zip)";
            // 
            // ux_labelExcludeFilter
            // 
            this.ux_labelExcludeFilter.AutoSize = true;
            this.ux_labelExcludeFilter.Location = new System.Drawing.Point(6, 55);
            this.ux_labelExcludeFilter.Name = "ux_labelExcludeFilter";
            this.ux_labelExcludeFilter.Size = new System.Drawing.Size(165, 13);
            this.ux_labelExcludeFilter.TabIndex = 1;
            this.ux_labelExcludeFilter.Text = "Exclude:     (e.g. *.tif; *.tiff; *_t.jpg)";
            // 
            // ux_textboxIncludeFilter
            // 
            this.ux_textboxIncludeFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxIncludeFilter.Location = new System.Drawing.Point(9, 32);
            this.ux_textboxIncludeFilter.Name = "ux_textboxIncludeFilter";
            this.ux_textboxIncludeFilter.Size = new System.Drawing.Size(423, 20);
            this.ux_textboxIncludeFilter.TabIndex = 2;
            // 
            // ux_textboxExcludeFilter
            // 
            this.ux_textboxExcludeFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxExcludeFilter.Location = new System.Drawing.Point(9, 71);
            this.ux_textboxExcludeFilter.Name = "ux_textboxExcludeFilter";
            this.ux_textboxExcludeFilter.Size = new System.Drawing.Size(423, 20);
            this.ux_textboxExcludeFilter.TabIndex = 3;
            // 
            // ux_groupboxAttachmentFileFilters
            // 
            this.ux_groupboxAttachmentFileFilters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxAttachmentFileFilters.Controls.Add(this.ux_labelIncludeFilter);
            this.ux_groupboxAttachmentFileFilters.Controls.Add(this.ux_textboxExcludeFilter);
            this.ux_groupboxAttachmentFileFilters.Controls.Add(this.ux_textboxIncludeFilter);
            this.ux_groupboxAttachmentFileFilters.Controls.Add(this.ux_labelExcludeFilter);
            this.ux_groupboxAttachmentFileFilters.Location = new System.Drawing.Point(13, 12);
            this.ux_groupboxAttachmentFileFilters.Name = "ux_groupboxAttachmentFileFilters";
            this.ux_groupboxAttachmentFileFilters.Size = new System.Drawing.Size(438, 103);
            this.ux_groupboxAttachmentFileFilters.TabIndex = 4;
            this.ux_groupboxAttachmentFileFilters.TabStop = false;
            this.ux_groupboxAttachmentFileFilters.Text = "Attachment File Filters";
            // 
            // ux_buttonOK
            // 
            this.ux_buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonOK.Location = new System.Drawing.Point(287, 336);
            this.ux_buttonOK.Name = "ux_buttonOK";
            this.ux_buttonOK.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonOK.TabIndex = 5;
            this.ux_buttonOK.Text = "OK";
            this.ux_buttonOK.UseVisualStyleBackColor = true;
            this.ux_buttonOK.Click += new System.EventHandler(this.ux_buttonOK_Click);
            // 
            // ux_buttonCancel
            // 
            this.ux_buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCancel.Location = new System.Drawing.Point(376, 336);
            this.ux_buttonCancel.Name = "ux_buttonCancel";
            this.ux_buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonCancel.TabIndex = 6;
            this.ux_buttonCancel.Text = "Cancel";
            this.ux_buttonCancel.UseVisualStyleBackColor = true;
            this.ux_buttonCancel.Click += new System.EventHandler(this.ux_buttonCancel_Click);
            // 
            // ux_groupboxFileNameAutomation
            // 
            this.ux_groupboxFileNameAutomation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_checkboxIgnoreFolderNames);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_groupboxFixedFieldTokens);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_checkboxCaseSensitive);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_groupboxDelimiters);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_radiobuttonFixedFieldParse);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_radiobuttonDelimitedParse);
            this.ux_groupboxFileNameAutomation.Controls.Add(this.ux_radiobuttonSmartParse);
            this.ux_groupboxFileNameAutomation.Location = new System.Drawing.Point(13, 122);
            this.ux_groupboxFileNameAutomation.Name = "ux_groupboxFileNameAutomation";
            this.ux_groupboxFileNameAutomation.Size = new System.Drawing.Size(438, 205);
            this.ux_groupboxFileNameAutomation.TabIndex = 7;
            this.ux_groupboxFileNameAutomation.TabStop = false;
            this.ux_groupboxFileNameAutomation.Text = "File Name Automation";
            // 
            // ux_checkboxIgnoreFolderNames
            // 
            this.ux_checkboxIgnoreFolderNames.AutoSize = true;
            this.ux_checkboxIgnoreFolderNames.Location = new System.Drawing.Point(228, 19);
            this.ux_checkboxIgnoreFolderNames.Name = "ux_checkboxIgnoreFolderNames";
            this.ux_checkboxIgnoreFolderNames.Size = new System.Drawing.Size(124, 17);
            this.ux_checkboxIgnoreFolderNames.TabIndex = 11;
            this.ux_checkboxIgnoreFolderNames.Text = "Ignore Folder Names";
            this.ux_checkboxIgnoreFolderNames.UseVisualStyleBackColor = true;
            // 
            // ux_groupboxFixedFieldTokens
            // 
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken0);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken1);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken2);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken3);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken4);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken5);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken6);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken7);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken8);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken9);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken10);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_labelFixedToken11);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken0);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken1);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken2);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken3);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken4);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken5);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken6);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken7);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken8);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken9);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken10);
            this.ux_groupboxFixedFieldTokens.Controls.Add(this.ux_textboxFixedToken11);
            this.ux_groupboxFixedFieldTokens.Enabled = false;
            this.ux_groupboxFixedFieldTokens.Location = new System.Drawing.Point(120, 125);
            this.ux_groupboxFixedFieldTokens.Name = "ux_groupboxFixedFieldTokens";
            this.ux_groupboxFixedFieldTokens.Size = new System.Drawing.Size(312, 70);
            this.ux_groupboxFixedFieldTokens.TabIndex = 10;
            this.ux_groupboxFixedFieldTokens.TabStop = false;
            this.ux_groupboxFixedFieldTokens.Text = "Number of Characters in Each Token";
            // 
            // ux_labelFixedToken0
            // 
            this.ux_labelFixedToken0.AutoSize = true;
            this.ux_labelFixedToken0.Location = new System.Drawing.Point(6, 20);
            this.ux_labelFixedToken0.Name = "ux_labelFixedToken0";
            this.ux_labelFixedToken0.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken0.TabIndex = 0;
            this.ux_labelFixedToken0.Text = "1:";
            // 
            // ux_labelFixedToken1
            // 
            this.ux_labelFixedToken1.AutoSize = true;
            this.ux_labelFixedToken1.Location = new System.Drawing.Point(57, 20);
            this.ux_labelFixedToken1.Name = "ux_labelFixedToken1";
            this.ux_labelFixedToken1.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken1.TabIndex = 1;
            this.ux_labelFixedToken1.Text = "2:";
            // 
            // ux_labelFixedToken2
            // 
            this.ux_labelFixedToken2.AutoSize = true;
            this.ux_labelFixedToken2.Location = new System.Drawing.Point(108, 20);
            this.ux_labelFixedToken2.Name = "ux_labelFixedToken2";
            this.ux_labelFixedToken2.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken2.TabIndex = 2;
            this.ux_labelFixedToken2.Text = "3:";
            // 
            // ux_labelFixedToken3
            // 
            this.ux_labelFixedToken3.AutoSize = true;
            this.ux_labelFixedToken3.Location = new System.Drawing.Point(160, 20);
            this.ux_labelFixedToken3.Name = "ux_labelFixedToken3";
            this.ux_labelFixedToken3.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken3.TabIndex = 3;
            this.ux_labelFixedToken3.Text = "4:";
            // 
            // ux_labelFixedToken4
            // 
            this.ux_labelFixedToken4.AutoSize = true;
            this.ux_labelFixedToken4.Location = new System.Drawing.Point(210, 20);
            this.ux_labelFixedToken4.Name = "ux_labelFixedToken4";
            this.ux_labelFixedToken4.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken4.TabIndex = 4;
            this.ux_labelFixedToken4.Text = "5:";
            // 
            // ux_labelFixedToken5
            // 
            this.ux_labelFixedToken5.AutoSize = true;
            this.ux_labelFixedToken5.Location = new System.Drawing.Point(262, 20);
            this.ux_labelFixedToken5.Name = "ux_labelFixedToken5";
            this.ux_labelFixedToken5.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken5.TabIndex = 10;
            this.ux_labelFixedToken5.Text = "6:";
            // 
            // ux_labelFixedToken6
            // 
            this.ux_labelFixedToken6.AutoSize = true;
            this.ux_labelFixedToken6.Location = new System.Drawing.Point(5, 46);
            this.ux_labelFixedToken6.Name = "ux_labelFixedToken6";
            this.ux_labelFixedToken6.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken6.TabIndex = 12;
            this.ux_labelFixedToken6.Text = "7:";
            // 
            // ux_labelFixedToken7
            // 
            this.ux_labelFixedToken7.AutoSize = true;
            this.ux_labelFixedToken7.Location = new System.Drawing.Point(57, 46);
            this.ux_labelFixedToken7.Name = "ux_labelFixedToken7";
            this.ux_labelFixedToken7.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken7.TabIndex = 14;
            this.ux_labelFixedToken7.Text = "8:";
            // 
            // ux_labelFixedToken8
            // 
            this.ux_labelFixedToken8.AutoSize = true;
            this.ux_labelFixedToken8.Location = new System.Drawing.Point(110, 46);
            this.ux_labelFixedToken8.Name = "ux_labelFixedToken8";
            this.ux_labelFixedToken8.Size = new System.Drawing.Size(16, 13);
            this.ux_labelFixedToken8.TabIndex = 16;
            this.ux_labelFixedToken8.Text = "9:";
            // 
            // ux_labelFixedToken9
            // 
            this.ux_labelFixedToken9.AutoSize = true;
            this.ux_labelFixedToken9.Location = new System.Drawing.Point(155, 46);
            this.ux_labelFixedToken9.Name = "ux_labelFixedToken9";
            this.ux_labelFixedToken9.Size = new System.Drawing.Size(22, 13);
            this.ux_labelFixedToken9.TabIndex = 17;
            this.ux_labelFixedToken9.Text = "10:";
            // 
            // ux_labelFixedToken10
            // 
            this.ux_labelFixedToken10.AutoSize = true;
            this.ux_labelFixedToken10.Location = new System.Drawing.Point(206, 46);
            this.ux_labelFixedToken10.Name = "ux_labelFixedToken10";
            this.ux_labelFixedToken10.Size = new System.Drawing.Size(22, 13);
            this.ux_labelFixedToken10.TabIndex = 18;
            this.ux_labelFixedToken10.Text = "11:";
            // 
            // ux_labelFixedToken11
            // 
            this.ux_labelFixedToken11.AutoSize = true;
            this.ux_labelFixedToken11.Location = new System.Drawing.Point(255, 46);
            this.ux_labelFixedToken11.Name = "ux_labelFixedToken11";
            this.ux_labelFixedToken11.Size = new System.Drawing.Size(22, 13);
            this.ux_labelFixedToken11.TabIndex = 19;
            this.ux_labelFixedToken11.Text = "12:";
            // 
            // ux_textboxFixedToken0
            // 
            this.ux_textboxFixedToken0.Location = new System.Drawing.Point(22, 17);
            this.ux_textboxFixedToken0.Name = "ux_textboxFixedToken0";
            this.ux_textboxFixedToken0.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken0.TabIndex = 5;
            this.ux_textboxFixedToken0.Text = "0";
            // 
            // ux_textboxFixedToken1
            // 
            this.ux_textboxFixedToken1.Location = new System.Drawing.Point(74, 17);
            this.ux_textboxFixedToken1.Name = "ux_textboxFixedToken1";
            this.ux_textboxFixedToken1.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken1.TabIndex = 6;
            this.ux_textboxFixedToken1.Text = "0";
            // 
            // ux_textboxFixedToken2
            // 
            this.ux_textboxFixedToken2.Location = new System.Drawing.Point(126, 17);
            this.ux_textboxFixedToken2.Name = "ux_textboxFixedToken2";
            this.ux_textboxFixedToken2.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken2.TabIndex = 7;
            this.ux_textboxFixedToken2.Text = "0";
            // 
            // ux_textboxFixedToken3
            // 
            this.ux_textboxFixedToken3.Location = new System.Drawing.Point(177, 17);
            this.ux_textboxFixedToken3.Name = "ux_textboxFixedToken3";
            this.ux_textboxFixedToken3.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken3.TabIndex = 8;
            this.ux_textboxFixedToken3.Text = "0";
            // 
            // ux_textboxFixedToken4
            // 
            this.ux_textboxFixedToken4.Location = new System.Drawing.Point(227, 17);
            this.ux_textboxFixedToken4.Name = "ux_textboxFixedToken4";
            this.ux_textboxFixedToken4.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken4.TabIndex = 9;
            this.ux_textboxFixedToken4.Text = "0";
            // 
            // ux_textboxFixedToken5
            // 
            this.ux_textboxFixedToken5.Location = new System.Drawing.Point(278, 17);
            this.ux_textboxFixedToken5.Name = "ux_textboxFixedToken5";
            this.ux_textboxFixedToken5.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken5.TabIndex = 11;
            this.ux_textboxFixedToken5.Text = "0";
            // 
            // ux_textboxFixedToken6
            // 
            this.ux_textboxFixedToken6.Location = new System.Drawing.Point(22, 43);
            this.ux_textboxFixedToken6.Name = "ux_textboxFixedToken6";
            this.ux_textboxFixedToken6.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken6.TabIndex = 13;
            this.ux_textboxFixedToken6.Text = "0";
            // 
            // ux_textboxFixedToken7
            // 
            this.ux_textboxFixedToken7.Location = new System.Drawing.Point(74, 43);
            this.ux_textboxFixedToken7.Name = "ux_textboxFixedToken7";
            this.ux_textboxFixedToken7.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken7.TabIndex = 15;
            this.ux_textboxFixedToken7.Text = "0";
            // 
            // ux_textboxFixedToken8
            // 
            this.ux_textboxFixedToken8.Location = new System.Drawing.Point(126, 43);
            this.ux_textboxFixedToken8.Name = "ux_textboxFixedToken8";
            this.ux_textboxFixedToken8.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken8.TabIndex = 21;
            this.ux_textboxFixedToken8.Text = "0";
            // 
            // ux_textboxFixedToken9
            // 
            this.ux_textboxFixedToken9.Location = new System.Drawing.Point(177, 43);
            this.ux_textboxFixedToken9.Name = "ux_textboxFixedToken9";
            this.ux_textboxFixedToken9.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken9.TabIndex = 22;
            this.ux_textboxFixedToken9.Text = "0";
            // 
            // ux_textboxFixedToken10
            // 
            this.ux_textboxFixedToken10.Location = new System.Drawing.Point(228, 43);
            this.ux_textboxFixedToken10.Name = "ux_textboxFixedToken10";
            this.ux_textboxFixedToken10.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken10.TabIndex = 23;
            this.ux_textboxFixedToken10.Text = "0";
            // 
            // ux_textboxFixedToken11
            // 
            this.ux_textboxFixedToken11.Location = new System.Drawing.Point(278, 43);
            this.ux_textboxFixedToken11.Name = "ux_textboxFixedToken11";
            this.ux_textboxFixedToken11.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxFixedToken11.TabIndex = 24;
            this.ux_textboxFixedToken11.Text = "0";
            // 
            // ux_checkboxCaseSensitive
            // 
            this.ux_checkboxCaseSensitive.AutoSize = true;
            this.ux_checkboxCaseSensitive.Location = new System.Drawing.Point(126, 20);
            this.ux_checkboxCaseSensitive.Name = "ux_checkboxCaseSensitive";
            this.ux_checkboxCaseSensitive.Size = new System.Drawing.Size(96, 17);
            this.ux_checkboxCaseSensitive.TabIndex = 9;
            this.ux_checkboxCaseSensitive.Text = "Case Sensitive";
            this.ux_checkboxCaseSensitive.UseVisualStyleBackColor = true;
            // 
            // ux_groupboxDelimiters
            // 
            this.ux_groupboxDelimiters.Controls.Add(this.ux_checkboxDash);
            this.ux_groupboxDelimiters.Controls.Add(this.ux_textboxOther);
            this.ux_groupboxDelimiters.Controls.Add(this.ux_checkboxUnderscore);
            this.ux_groupboxDelimiters.Controls.Add(this.ux_checkboxOther);
            this.ux_groupboxDelimiters.Controls.Add(this.ux_checkboxSpace);
            this.ux_groupboxDelimiters.Enabled = false;
            this.ux_groupboxDelimiters.Location = new System.Drawing.Point(120, 52);
            this.ux_groupboxDelimiters.Name = "ux_groupboxDelimiters";
            this.ux_groupboxDelimiters.Size = new System.Drawing.Size(312, 67);
            this.ux_groupboxDelimiters.TabIndex = 8;
            this.ux_groupboxDelimiters.TabStop = false;
            this.ux_groupboxDelimiters.Text = "Token Delimiters";
            // 
            // ux_checkboxDash
            // 
            this.ux_checkboxDash.AutoSize = true;
            this.ux_checkboxDash.Location = new System.Drawing.Point(114, 19);
            this.ux_checkboxDash.Name = "ux_checkboxDash";
            this.ux_checkboxDash.Size = new System.Drawing.Size(51, 17);
            this.ux_checkboxDash.TabIndex = 5;
            this.ux_checkboxDash.Text = "Dash";
            this.ux_checkboxDash.UseVisualStyleBackColor = true;
            // 
            // ux_textboxOther
            // 
            this.ux_textboxOther.Location = new System.Drawing.Point(171, 41);
            this.ux_textboxOther.MaxLength = 1;
            this.ux_textboxOther.Name = "ux_textboxOther";
            this.ux_textboxOther.Size = new System.Drawing.Size(24, 20);
            this.ux_textboxOther.TabIndex = 7;
            // 
            // ux_checkboxUnderscore
            // 
            this.ux_checkboxUnderscore.AutoSize = true;
            this.ux_checkboxUnderscore.Checked = true;
            this.ux_checkboxUnderscore.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkboxUnderscore.Location = new System.Drawing.Point(6, 19);
            this.ux_checkboxUnderscore.Name = "ux_checkboxUnderscore";
            this.ux_checkboxUnderscore.Size = new System.Drawing.Size(81, 17);
            this.ux_checkboxUnderscore.TabIndex = 3;
            this.ux_checkboxUnderscore.Text = "Underscore";
            this.ux_checkboxUnderscore.UseVisualStyleBackColor = true;
            // 
            // ux_checkboxOther
            // 
            this.ux_checkboxOther.AutoSize = true;
            this.ux_checkboxOther.Location = new System.Drawing.Point(113, 42);
            this.ux_checkboxOther.Name = "ux_checkboxOther";
            this.ux_checkboxOther.Size = new System.Drawing.Size(52, 17);
            this.ux_checkboxOther.TabIndex = 6;
            this.ux_checkboxOther.Text = "Other";
            this.ux_checkboxOther.UseVisualStyleBackColor = true;
            this.ux_checkboxOther.CheckedChanged += new System.EventHandler(this.ux_checkboxOther_CheckedChanged);
            // 
            // ux_checkboxSpace
            // 
            this.ux_checkboxSpace.AutoSize = true;
            this.ux_checkboxSpace.Checked = true;
            this.ux_checkboxSpace.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ux_checkboxSpace.Location = new System.Drawing.Point(6, 43);
            this.ux_checkboxSpace.Name = "ux_checkboxSpace";
            this.ux_checkboxSpace.Size = new System.Drawing.Size(57, 17);
            this.ux_checkboxSpace.TabIndex = 4;
            this.ux_checkboxSpace.Text = "Space";
            this.ux_checkboxSpace.UseVisualStyleBackColor = true;
            // 
            // ux_radiobuttonFixedFieldParse
            // 
            this.ux_radiobuttonFixedFieldParse.AutoSize = true;
            this.ux_radiobuttonFixedFieldParse.Location = new System.Drawing.Point(6, 125);
            this.ux_radiobuttonFixedFieldParse.Name = "ux_radiobuttonFixedFieldParse";
            this.ux_radiobuttonFixedFieldParse.Size = new System.Drawing.Size(113, 17);
            this.ux_radiobuttonFixedFieldParse.TabIndex = 2;
            this.ux_radiobuttonFixedFieldParse.Text = "Fixed Field Parsing";
            this.ux_radiobuttonFixedFieldParse.UseVisualStyleBackColor = true;
            this.ux_radiobuttonFixedFieldParse.CheckedChanged += new System.EventHandler(this.ux_radiobuttonFixedFieldParse_CheckedChanged);
            // 
            // ux_radiobuttonDelimitedParse
            // 
            this.ux_radiobuttonDelimitedParse.AutoSize = true;
            this.ux_radiobuttonDelimitedParse.Location = new System.Drawing.Point(6, 52);
            this.ux_radiobuttonDelimitedParse.Name = "ux_radiobuttonDelimitedParse";
            this.ux_radiobuttonDelimitedParse.Size = new System.Drawing.Size(106, 17);
            this.ux_radiobuttonDelimitedParse.TabIndex = 1;
            this.ux_radiobuttonDelimitedParse.Text = "Delimited Parsing";
            this.ux_radiobuttonDelimitedParse.UseVisualStyleBackColor = true;
            this.ux_radiobuttonDelimitedParse.CheckedChanged += new System.EventHandler(this.ux_radiobuttonDelimitedParse_CheckedChanged);
            // 
            // ux_radiobuttonSmartParse
            // 
            this.ux_radiobuttonSmartParse.AutoSize = true;
            this.ux_radiobuttonSmartParse.Checked = true;
            this.ux_radiobuttonSmartParse.Location = new System.Drawing.Point(6, 19);
            this.ux_radiobuttonSmartParse.Name = "ux_radiobuttonSmartParse";
            this.ux_radiobuttonSmartParse.Size = new System.Drawing.Size(90, 17);
            this.ux_radiobuttonSmartParse.TabIndex = 0;
            this.ux_radiobuttonSmartParse.TabStop = true;
            this.ux_radiobuttonSmartParse.Text = "Smart Parsing";
            this.ux_radiobuttonSmartParse.UseVisualStyleBackColor = true;
            this.ux_radiobuttonSmartParse.CheckedChanged += new System.EventHandler(this.ux_radiobuttonSmartParse_CheckedChanged);
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 371);
            this.Controls.Add(this.ux_groupboxFileNameAutomation);
            this.Controls.Add(this.ux_buttonCancel);
            this.Controls.Add(this.ux_buttonOK);
            this.Controls.Add(this.ux_groupboxAttachmentFileFilters);
            this.Name = "Options";
            this.Text = "Options";
            this.ux_groupboxAttachmentFileFilters.ResumeLayout(false);
            this.ux_groupboxAttachmentFileFilters.PerformLayout();
            this.ux_groupboxFileNameAutomation.ResumeLayout(false);
            this.ux_groupboxFileNameAutomation.PerformLayout();
            this.ux_groupboxFixedFieldTokens.ResumeLayout(false);
            this.ux_groupboxFixedFieldTokens.PerformLayout();
            this.ux_groupboxDelimiters.ResumeLayout(false);
            this.ux_groupboxDelimiters.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label ux_labelIncludeFilter;
        private System.Windows.Forms.Label ux_labelExcludeFilter;
        private System.Windows.Forms.TextBox ux_textboxIncludeFilter;
        private System.Windows.Forms.TextBox ux_textboxExcludeFilter;
        private System.Windows.Forms.GroupBox ux_groupboxAttachmentFileFilters;
        private System.Windows.Forms.Button ux_buttonOK;
        private System.Windows.Forms.Button ux_buttonCancel;
        private System.Windows.Forms.GroupBox ux_groupboxFileNameAutomation;
        private System.Windows.Forms.CheckBox ux_checkboxCaseSensitive;
        private System.Windows.Forms.CheckBox ux_checkboxIgnoreFolderNames;
        private System.Windows.Forms.GroupBox ux_groupboxDelimiters;
        private System.Windows.Forms.CheckBox ux_checkboxDash;
        private System.Windows.Forms.TextBox ux_textboxOther;
        private System.Windows.Forms.CheckBox ux_checkboxUnderscore;
        private System.Windows.Forms.CheckBox ux_checkboxOther;
        private System.Windows.Forms.CheckBox ux_checkboxSpace;
        private System.Windows.Forms.RadioButton ux_radiobuttonFixedFieldParse;
        private System.Windows.Forms.RadioButton ux_radiobuttonDelimitedParse;
        private System.Windows.Forms.RadioButton ux_radiobuttonSmartParse;
        private System.Windows.Forms.GroupBox ux_groupboxFixedFieldTokens;
        private System.Windows.Forms.Label ux_labelFixedToken0;
        private System.Windows.Forms.Label ux_labelFixedToken1;
        private System.Windows.Forms.Label ux_labelFixedToken2;
        private System.Windows.Forms.Label ux_labelFixedToken3;
        private System.Windows.Forms.Label ux_labelFixedToken4;
        private System.Windows.Forms.Label ux_labelFixedToken5;
        private System.Windows.Forms.Label ux_labelFixedToken6;
        private System.Windows.Forms.Label ux_labelFixedToken7;
        private System.Windows.Forms.Label ux_labelFixedToken8;
        private System.Windows.Forms.Label ux_labelFixedToken9;
        private System.Windows.Forms.Label ux_labelFixedToken10;
        private System.Windows.Forms.Label ux_labelFixedToken11;
        private System.Windows.Forms.TextBox ux_textboxFixedToken0;
        private System.Windows.Forms.TextBox ux_textboxFixedToken1;
        private System.Windows.Forms.TextBox ux_textboxFixedToken2;
        private System.Windows.Forms.TextBox ux_textboxFixedToken3;
        private System.Windows.Forms.TextBox ux_textboxFixedToken4;
        private System.Windows.Forms.TextBox ux_textboxFixedToken5;
        private System.Windows.Forms.TextBox ux_textboxFixedToken6;
        private System.Windows.Forms.TextBox ux_textboxFixedToken7;
        private System.Windows.Forms.TextBox ux_textboxFixedToken8;
        private System.Windows.Forms.TextBox ux_textboxFixedToken9;
        private System.Windows.Forms.TextBox ux_textboxFixedToken10;
        private System.Windows.Forms.TextBox ux_textboxFixedToken11;
    }
}