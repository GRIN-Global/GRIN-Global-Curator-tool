﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GRINGlobal.Client.Common;

namespace ViabilityWizard
{
    public partial class ViabilityWizardNewTest : Form
    {
        SharedUtils _sharedUtils;
        string _inventoryList;
        string _orderList;
        DataSet _dsInventoryViability;
        DataTable _inventoryViability;
        char _lastDGVCharPressed;
        bool _dataIsSaved;
        bool _dataIsPrinted;

        public ViabilityWizardNewTest(SharedUtils sharedUtils, string inventoryList, string orderList)
        {
            _sharedUtils = sharedUtils;
            _inventoryList = inventoryList.Trim().TrimEnd(',').Trim();
            _orderList = orderList.Trim().TrimEnd(',').Trim();
            InitializeComponent();
        }

        public DataTable NewRecords
        {
            get
            {
                DataTable dt = new DataTable();
                //if (_changedRecords.Tables.Contains(_accession.TableName))
                //{
                //    dt = _changedRecords.Tables[_accession.TableName].Copy();
                //}
                return dt;
            }
        }

        private void ViabilityWizardNewTest_Load(object sender, EventArgs e)
        {
            // Go get any matching inventory_viability records for the collection of inventories...
            _dsInventoryViability = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability", ":inventoryid=" + _inventoryList + "; :orderrequestid=" + _orderList, 0, 0);

            if (_dsInventoryViability != null &&
                _dsInventoryViability.Tables.Count > 0 &&
                _dsInventoryViability.Tables.Contains("viability_wizard_get_inventory_viability") &&
                _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].Rows.Count >= 0)
            {
                // Check to see if any of the existing inventory_viability records are still 'open' (still a work in progress) and
                // if so ask the user if they want to create new inventory_viability records or edit the existing ones...
                _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].DefaultView.RowFilter = "percent_normal IS NULL AND percent_abnormal IS NULL AND percent_dormant IS NULL AND percent_viable IS NULL";

                if (_dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].DefaultView.Count > 0)
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There are {0} existing unfinished Viabilty records associated with this collection of inventory records.\n\n  Would you like to create new Viability records?\n\n Yes = Create new records\n No = Edit existing records\n Cancel = Quit without any changes", "Viability Wizard - Create New Tests", MessageBoxButtons.YesNoCancel, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ViabilityWizard_Load";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    if (!string.IsNullOrEmpty(_orderList))
                    {
                        // If an order number was used to generate this inventory collection apply the order number filter 
                        // to filter to only viablity test rows that are associated with that order number...
                        string origRowFilter = _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].DefaultView.RowFilter;
                        _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].DefaultView.RowFilter += string.Format(" AND note LIKE '%{0}%'", _orderList);
                        argsArray[0] = _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].DefaultView.Count.ToString();
                        _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].DefaultView.RowFilter = origRowFilter;
                    }
                    else
                    {
                        argsArray[0] = _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].DefaultView.Count.ToString();
                    }
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    DialogResult userResponse = ggMessageBox.ShowDialog();
                    if (DialogResult.Yes == userResponse)
                    {
                        // The user wants new inventory_viability records so create a new empty datatable
                        // (effectively discarding the existing records in the current dataset)
                        //_inventoryViability = _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].Clone();
                        _inventoryViability = BuildInventoryViabilityTable(true);
                        _dataIsSaved = false;
                        _dataIsPrinted = false;
                    }
                    else if (DialogResult.No == userResponse)
                    {
                        // The user wants to edit the existing inventory_viability records so create a new table with only those records...
                        //_inventoryViability = _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].DefaultView.ToTable();
                        _inventoryViability = _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].Clone();
                        if (!string.IsNullOrEmpty(_orderList)) _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].DefaultView.RowFilter += string.Format(" AND note LIKE '%{0}%'", _orderList);
                        _inventoryViability.Load(new DataTableReader(_dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].DefaultView.ToTable()));
                        _inventoryViability.AcceptChanges();
                        _dataIsSaved = true;
                    }
                    else
                    {
                        return;
                    }

                    // Wire up the new inventory_viability table to the datagridview...
                    _sharedUtils.BuildEditDataGridView(ux_datagridviewInventoryViability, _inventoryViability);
                }
                else
                {
                    _inventoryViability = BuildInventoryViabilityTable(false);

                    _dataIsSaved = false;
                    _dataIsPrinted = false;

                    // Wire up the new inventory_viability table to the datagridview...
                    _sharedUtils.BuildEditDataGridView(ux_datagridviewInventoryViability, _inventoryViability);
                }
            }

            // Set the column property for sorting...
            foreach (DataGridViewColumn dgvc in ux_datagridviewInventoryViability.Columns)
            {
                dgvc.SortMode = DataGridViewColumnSortMode.Automatic;
            }

            // Load the list of Crystal Reports...
            //System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.Windows.Forms.Application.StartupPath + "\\Reports");
            System.Collections.Generic.Dictionary<string, string> reportsMap = _sharedUtils.GetReportsMapping();
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports");
            foreach (string reportName in _sharedUtils.GetAppSettingValue("ViabilityWizardCrystalReports").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                foreach (System.IO.FileInfo fi in di.GetFiles(reportName.Trim(), System.IO.SearchOption.AllDirectories))
                {
                    if (fi.Name.Trim().ToUpper() == reportName.Trim().ToUpper())
                    {
                        ux_comboboxCrystalReports.Items.Add(fi.Name);
                    }
                }
            }
            // Initialize size/location/visibility/enable status for the button and combobox controls as needed...
            ux_buttonCreateTests.Enabled = true;
            ux_buttonCancel.Visible = true;
            ux_buttonDone.Location = ux_buttonCancel.Location;
            ux_buttonDone.Size = ux_buttonCancel.Size;
            ux_buttonDone.Visible = false;
            ux_buttonDone.Enabled = false;
            // Enable the 'Print Labels' button if all records have required data...
            if (_inventoryViability != null &&
                _inventoryViability.Select("inventory_viability_rule_id IS NULL OR total_tested_count IS NULL OR replication_count IS NULL").Length <= 0)
            {
                ux_buttonPrintLabels.Visible = true;
                ux_buttonPrintLabels.Enabled = true;
                ux_comboboxCrystalReports.Visible = true;
                ux_comboboxCrystalReports.Enabled = true;
            }
            else
            {
                ux_buttonPrintLabels.Visible = true;
                ux_buttonPrintLabels.Enabled = false;
                ux_comboboxCrystalReports.Visible = true;
                ux_comboboxCrystalReports.Enabled = false;
            }
            if (ux_comboboxCrystalReports.Items.Count > 0) ux_comboboxCrystalReports.SelectedIndex = 0;
        }

        private DataTable BuildInventoryViabilityTable(bool makeTestDateUnique)
        {
            string testDate = DateTime.Now.ToString("d");
            if (makeTestDateUnique) testDate = DateTime.Now.ToString("g");
            // The user wants new inventory_viability records so create a new empty datatable
            // (effectively discarding the existing records in the current dataset)
            DataTable inventoryViabilityTable = _dsInventoryViability.Tables["viability_wizard_get_inventory_viability"].Clone();
            // Get all of the inventory_viability_rules associated with the list of inventory_ids...
            DataSet inventoryViabilityRule = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_rule", ":inventoryid=" + _inventoryList + "; :orderrequestid=" + _orderList, 0, 0);
            // Get all of the inventory associated with the list of inventory_ids...
            DataSet inventory = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory", ":inventoryid=" + _inventoryList + "; :orderrequestid=" + _orderList, 0, 0);
            // Now populate the new table with new inventory_viability records based on the list of inventory_ids...
            //string[] inventoryIDs = _inventoryList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (inventoryViabilityTable != null &&
                inventoryViabilityRule != null &&
                inventoryViabilityRule.Tables.Count > 0 &&
                inventoryViabilityRule.Tables.Contains("viability_wizard_get_inventory_viability_rule") &&
                inventory != null &&
                inventory.Tables.Count > 0 &&
                inventory.Tables.Contains("viability_wizard_get_inventory"))
            {
                foreach (DataRow dr in inventory.Tables["viability_wizard_get_inventory"].Rows)
                {
                    // Create a new row...
                    DataRow newInventoryViabilityRow = inventoryViabilityTable.NewRow();
                    if (newInventoryViabilityRow.Table.Columns.Contains("inventory_id") && dr.Table.Columns.Contains("inventory_id") && dr["inventory_id"] != DBNull.Value) newInventoryViabilityRow["inventory_id"] = dr["inventory_id"].ToString();
                    if (newInventoryViabilityRow.Table.Columns.Contains("tested_date")) newInventoryViabilityRow["tested_date"] = testDate;
                    if (newInventoryViabilityRow.Table.Columns.Contains("order_request_sequence_number") && dr.Table.Columns.Contains("order_request_sequence_number") && dr["order_request_sequence_number"] != DBNull.Value) newInventoryViabilityRow["order_request_sequence_number"] = dr["order_request_sequence_number"].ToString();
                    if (newInventoryViabilityRow.Table.Columns.Contains("taxonomy_species_name") && dr.Table.Columns.Contains("taxonomy_species_name") && dr["taxonomy_species_name"] != DBNull.Value) newInventoryViabilityRow["taxonomy_species_name"] = dr["taxonomy_species_name"].ToString();
                    if (newInventoryViabilityRow.Table.Columns.Contains("note") && !string.IsNullOrEmpty(_orderList)) newInventoryViabilityRow["note"] = string.Format("Created from order number: {0} ", _orderList);
                    // And add the new row to the table...
                    inventoryViabilityTable.Rows.Add(newInventoryViabilityRow);
                }
            }

            return inventoryViabilityTable;
        }

        private void ViabilityWizardNewTest_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            if (!_dataIsSaved)
            {
                int intRowEdits = 0;

                ux_datagridviewInventoryViability.EndEdit();
                if (_inventoryViability != null && _inventoryViability.GetChanges() != null) intRowEdits = _inventoryViability.GetChanges().Rows.Count;
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to cancel your edits and close this window?", "Cancel Edits and Close", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ViabilityWizardNewTest_FormClosing1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = intRowEdits.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                if (intRowEdits > 0 && DialogResult.No == ggMessageBox.ShowDialog())
                {
                    e.Cancel = true;
                }
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }
            else if (!_dataIsPrinted && ux_buttonPrintLabels.Enabled)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have saved {0} new Viability Test records, would you like to print labels for these records now?", "Print Labels and Close", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ViabilityWizardNewTest_FormClosing2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                _inventoryViability.DefaultView.RowFilter = "created_date>'" + DateTime.Now.ToString("d") + "'";
                if (_inventoryViability.DefaultView.Count > 0)
                {
                    argsArray[0] = _inventoryViability.DefaultView.Count.ToString();
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    if (DialogResult.Yes == ggMessageBox.ShowDialog())
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        #region DGV control logic...
        private void ux_datagridview_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            // Get the defaultview of the datatable used by the dgv...
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }

            // Begin processing cell formatting based on the datatype of the field...
            if (dv != null &&
                e.ColumnIndex > -1 &&
                e.RowIndex > -1 &&
                e.RowIndex < dv.Count)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                // Format FKey fields...
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    e.RowIndex < dv.Count &&
                    dv[e.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (dv[e.RowIndex][e.ColumnIndex] != DBNull.Value)
                    {
                        e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim(), dv[e.RowIndex][e.ColumnIndex].ToString().Trim(), "", dv[e.RowIndex][e.ColumnIndex].ToString().Trim());
                    }
                    dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    e.FormattingApplied = true;
                }
                // Format date/time fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        e.Value = ((DateTime)dv[e.RowIndex][e.ColumnIndex]).ToString(dateFormat);
                        e.FormattingApplied = true;
                    }
                }
                // Format integer fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    (dc.DataType == typeof(int) ||
                    dc.DataType == typeof(Int16) ||
                    dc.DataType == typeof(Int32) ||
                    dc.DataType == typeof(Int64)))
                {
                    int junk;
                    if (!int.TryParse(e.Value.ToString(), out junk))
                    {
                        dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    }
                }

                if (dc.ReadOnly)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }

                if (dc.ExtendedProperties.Contains("is_nullable") &&
                    dc.ExtendedProperties["is_nullable"].ToString() == "N" &&
                    string.IsNullOrEmpty(dv[e.RowIndex][e.ColumnIndex].ToString()))
                {
                    e.CellStyle.BackColor = Color.Plum;
                }


                if (dgv.CurrentCell.OwningColumn.Name.ToLower().Trim() == "inventory_viability_rule_id")
                {
                    // Get the value of the inventory_viability_rule_id from the datatable...
                    DataRowView drv = (DataRowView)dgv.CurrentCell.OwningRow.DataBoundItem;
                    int ivrID = 0;
                    if (int.TryParse("", out ivrID))
                    {
                        int junk = (int)drv.Row[dgv.CurrentCell.OwningColumn.Name];
                    }
                }
            }
        }

        private void ux_datagridview_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }
            if (dv != null && e.ColumnIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        DateTime formattedDate;
                        if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out formattedDate))
                        {
                            e.Value = formattedDate;
                            e.ParsingApplied = true;
                        }
                    }
                }
            }
        }

        private void ux_datagridview_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            // If the changed cell is the viability rule then update the replication count and number of seeds in that record...
            foreach (DataGridViewCell dgvc in dgv.SelectedCells)
            {
                if (dgvc.OwningColumn.Name.ToLower().Trim() == "inventory_viability_rule_id")
                {
                    // Get the value of the inventory_viability_rule_id from the datatable...
                    DataRowView drv = (DataRowView)dgvc.OwningRow.DataBoundItem;
                    int ivrID = (int)drv.Row["inventory_viability_rule_id"];
                    // Use that ivrID value to retrieve the IVR record from the database...
                    DataSet ds = _sharedUtils.GetWebServiceData("viability_wizard_get_inventory_viability_rule", ":inventoryviabilityruleid=" + ivrID.ToString(), 0, 0);
                    // If something useful came back use the values in the replication_count and total number of seeds columns...
                    if (ds != null &&
                        ds.Tables.Contains("viability_wizard_get_inventory_viability_rule") &&
                        ds.Tables["viability_wizard_get_inventory_viability_rule"].Rows.Count == 1 &&
                        ds.Tables["viability_wizard_get_inventory_viability_rule"].Columns.Contains("number_of_replicates") &&
                        !string.IsNullOrEmpty(ds.Tables["viability_wizard_get_inventory_viability_rule"].Rows[0]["number_of_replicates"].ToString()) &&
                        ds.Tables["viability_wizard_get_inventory_viability_rule"].Columns.Contains("seeds_per_replicate") &&
                        !string.IsNullOrEmpty(ds.Tables["viability_wizard_get_inventory_viability_rule"].Rows[0]["seeds_per_replicate"].ToString()))
                    {
                        if (ds.Tables["viability_wizard_get_inventory_viability_rule"].Columns.Contains("seeds_per_replicate") &&
                            !string.IsNullOrEmpty(ds.Tables["viability_wizard_get_inventory_viability_rule"].Rows[0]["seeds_per_replicate"].ToString()))
                        {// replication_count, total_tested_count
                            int seedsPerRep;
                            int numberOfReps;
                            if (int.TryParse(ds.Tables["viability_wizard_get_inventory_viability_rule"].Rows[0]["number_of_replicates"].ToString(), out numberOfReps) &&
                                int.TryParse(ds.Tables["viability_wizard_get_inventory_viability_rule"].Rows[0]["seeds_per_replicate"].ToString(), out seedsPerRep))
                            {
                                int TotalTestedCount = ((int)(seedsPerRep * numberOfReps));
                                if (drv.Row.Table.Columns.Contains("replication_count")) drv.Row["replication_count"] = numberOfReps;
                                if (drv.Row.Table.Columns.Contains("total_tested_count")) drv.Row["total_tested_count"] = TotalTestedCount;
                            }
                        }
                    }
                }
            }
            // Update the buttons status if there are changes
            if (_inventoryViability.GetChanges() != null)
            {
                ux_buttonCancel.Visible = true;
                ux_buttonCancel.Enabled = true;
                ux_buttonCreateTests.Visible = true;
                ux_buttonCreateTests.Enabled = true;
            }
        }

        private void ux_datagridview_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string errorMessage = e.Exception.Message;
            int columnWithError = -1;

            // Find the cell the error belongs to (don't use e.ColumnIndex because it points to the current cell *NOT* the offending cell)...
            foreach (DataGridViewColumn col in dgv.Columns)
            {
                if (errorMessage.Contains(col.Name))
                {
                    dgv[col.Name, e.RowIndex].ErrorText = errorMessage;
                    columnWithError = col.Index;
                }
            }
        }

        private void ux_datagridview_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string columnName = dgv.CurrentCell.OwningColumn.Name;
            DataTable dt;
            DataRow dr;
            if (dgv != null &&
                dgv.DataSource != null &&
                dgv.DataSource.GetType() == typeof(BindingSource))
            {
                // Bind the new table to the default binding source (the bindingNavigator and mainDGV are both bound to this bindingSource)...
                dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;
            }
            else
            {
                dt = (DataTable)dgv.DataSource;
            }

            DataColumn dc = dt.Columns[columnName];

            if (_sharedUtils.LookupTablesIsValidFKField(dc))
            {
                string luTableName = dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim();
                dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
                string suggestedFilter = dgv.CurrentCell.EditedFormattedValue.ToString();
                if (_lastDGVCharPressed > 0) suggestedFilter = _lastDGVCharPressed.ToString();
                GRINGlobal.Client.Common.LookupTablePicker ltp = new GRINGlobal.Client.Common.LookupTablePicker(_sharedUtils, columnName, dr, suggestedFilter);
                //_lastDGVCharPressed = (char)0;
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    if (dr != null)
                    {
                        if (ltp.NewKey != null && dr[dgv.CurrentCell.ColumnIndex].ToString().Trim() != ltp.NewKey.Trim())
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = ltp.NewKey.Trim();
                            dgv.CurrentCell.Value = ltp.NewValue.Trim();
                        }
                        else if (ltp.NewKey == null)
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = DBNull.Value;
                            dgv.CurrentCell.Value = "";
                        }
                        dr.SetColumnError(dgv.CurrentCell.ColumnIndex, null);
                    }
                }
                dgv.EndEdit();
            }
        }

        private void ux_datagridview_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else if (dgv.DataSource.GetType() == typeof(DataTable))
            {
                dv = ((DataTable)(dgv.DataSource)).DefaultView;
            }

            // Reset the last char pressed global variable...
            _lastDGVCharPressed = (char)0;

            // If we are: 
            //  1) in edit mode, 
            //  2) the current cell is parked on a cell that is a FK lookup 
            //  3) and the Alt and Ctrl keys are not down
            // Then remember the keypress so that it can be passed into the Lookup Picker dialog...
            if (dv != null &&
                dgv.CurrentCell != null &&
                dgv.CurrentCell.ColumnIndex > -1 &&
                dgv.CurrentCell.RowIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[dgv.CurrentCell.ColumnIndex];
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    dgv.CurrentCell.RowIndex < dv.Count &&
                    dv[dgv.CurrentCell.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (!e.Alt && !e.Control)
                    {
                        KeysConverter kc = new KeysConverter();
                        string lastChar = kc.ConvertToString(e.KeyValue).Replace("NumPad", "");
                        if (lastChar.Length == 1)
                        {
                            if (e.Shift)
                            {
                                _lastDGVCharPressed = lastChar.ToUpper()[0];
                            }
                            else
                            {
                                _lastDGVCharPressed = lastChar.ToLower()[0];
                            }
                        }
                    }
                }
            }
        }

        private void ux_datagridview_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (_sharedUtils.ProcessDGVEditShortcutKeys(dgv, e, _sharedUtils.UserCooperatorID))
            {
                ux_datagridviewInventoryViability.Refresh();
                // Fire an event that the cell's value changed so that if it was the viability rule
                // the total seeds tested and number of replicates can be updated for the inventory_viability record...
                ux_datagridview_CellValueChanged(sender, null);
            }
        }

        #endregion

        private void ux_buttonCreateTests_Click(object sender, EventArgs e)
        {
            int errorCount = SaveViabilityData();
            if (errorCount == 0)
            {
                // Set the controls to reflect the new saved state...
                _dataIsSaved = true;
                ux_buttonCreateTests.Enabled = false;
                ux_buttonCancel.Visible = false;
                ux_buttonDone.Location = ux_buttonCancel.Location;
                ux_buttonDone.Size = ux_buttonCancel.Size;
                ux_buttonDone.Visible = true;
                ux_buttonDone.Enabled = true;
                if (_inventoryViability.Select("inventory_viability_rule_id IS NULL OR total_tested_count IS NULL OR replication_count IS NULL").Length <= 0)
                {
                    ux_buttonPrintLabels.Visible = true;
                    ux_buttonPrintLabels.Enabled = true;
                    ux_comboboxCrystalReports.Visible = true;
                    ux_comboboxCrystalReports.Enabled = true;
                }
                else
                {
                    ux_buttonPrintLabels.Visible = true;
                    ux_buttonPrintLabels.Enabled = false;
                    ux_comboboxCrystalReports.Visible = true;
                    ux_comboboxCrystalReports.Enabled = false;
                }
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Viability Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ViabilityWizard_ux_buttonCreateTests1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();
            }
            else
            {
                ux_buttonCreateTests.Enabled = true;
                ux_buttonCancel.Visible = true;
                ux_buttonDone.Location = ux_buttonCancel.Location;
                ux_buttonDone.Size = ux_buttonCancel.Size;
                ux_buttonDone.Visible = false;
                ux_buttonDone.Enabled = false;
                ux_buttonPrintLabels.Visible = true;
                ux_buttonPrintLabels.Enabled = false;
                ux_comboboxCrystalReports.Visible = true;
                ux_comboboxCrystalReports.Enabled = false;
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\n  Error Count: {0}", "Viability Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ViabilityWizard_ux_buttonCreateTests2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }
        }

        private void ux_buttonPrintLabels_Click(object sender, EventArgs e)
        {
            //string fullPathName = System.Windows.Forms.Application.StartupPath + "\\Reports\\" + ux_comboboxCrystalReports.Text;
            string fullPathName = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports\" + ux_comboboxCrystalReports.Text;

            if (System.IO.File.Exists(fullPathName))
            {
                // Build the list of viability_ids that will be used to retrieve inventory records to populate the labels...
                string viabilityIDs = "";
                foreach (DataRowView drv in _inventoryViability.DefaultView)
                {
                    viabilityIDs += drv["inventory_viability_id"].ToString() + ",";
                }
                viabilityIDs = viabilityIDs.TrimEnd(',');

                DataTable dtViabilityTestData = ViabilityWizard.BuildViabilityTestLabelData(viabilityIDs, _sharedUtils);

                // Resolve the FKEY and code_value data in the Inventory table to 'friendly names'...
                DataGridView dgv = new DataGridView();
                _sharedUtils.BuildReadOnlyDataGridView(dgv, dtViabilityTestData);
                if (dgv != null &&
                    dgv.DataSource != null &&
                    dgv.DataSource.GetType() == typeof(DataTable))
                {
                    // Okay it looks like we have a datagridview with a datasource = datatable (with FKeys and code_values resolved) - so extract it and use in the report...
                    DataTable dt = (DataTable)dgv.DataSource;
                    // Got the data so now we can generate the Crystal Report...
                    _sharedUtils.PrintReport(dt, fullPathName);
                    _dataIsPrinted = true;
                }
            }
        }

        private void ux_buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ux_buttonDone_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private int SaveViabilityData()
        {
            int errorCount = 0;
            DataSet inventoryViabilityChanges = new DataSet();
            DataSet inventoryViabilitySaveResults = new DataSet();
            // Get the changes (if any) for the inventory_viability table and commit them to the remote database...
            if (_inventoryViability != null && _inventoryViability.GetChanges() != null)
            {
                inventoryViabilityChanges.Tables.Add(_inventoryViability.GetChanges());
                ScrubData(inventoryViabilityChanges);
                EnforceUniqueness(inventoryViabilityChanges.Tables[_inventoryViability.TableName]);
                // Save the changes to the remote server...
                inventoryViabilitySaveResults = _sharedUtils.SaveWebServiceData(inventoryViabilityChanges);
                if (inventoryViabilitySaveResults.Tables.Contains(_inventoryViability.TableName))
                {
                    errorCount += SyncSavedResults(_inventoryViability, inventoryViabilitySaveResults.Tables[_inventoryViability.TableName]);
                }
            }

            return errorCount;
        }

        private void EnforceUniqueness(DataTable inventoryViabilityTable)
        {
            // Do a sanity check to make sure the records in this table follow the uniqueness constraint
            // for inventory_viability (must be unique based on inventory, rule, and test date)...
            foreach (DataRow dr in inventoryViabilityTable.Rows)
            {
                if (dr.RowState != DataRowState.Deleted &&
                    dr.RowState != DataRowState.Detached)
                {
                    string uniquenessConstraint = "";
                    if (!string.IsNullOrEmpty(dr["inventory_id"].ToString().Trim())) uniquenessConstraint += "inventory_id=" + dr["inventory_id"].ToString();
                    if (!string.IsNullOrEmpty(dr["inventory_viability_rule_id"].ToString().Trim())) uniquenessConstraint += " AND inventory_viability_rule_id=" + dr["inventory_viability_rule_id"].ToString();
                    if (!string.IsNullOrEmpty(dr["tested_date"].ToString().Trim())) uniquenessConstraint += " AND tested_date='" + dr["tested_date"].ToString() + "'";
                    DataRow[] duplicateRows = inventoryViabilityTable.Select(uniquenessConstraint, "inventory_viability_id DESC");
                    if (duplicateRows.Length > 1)
                    {
                        DateTime uniqueTestedDate = (DateTime)duplicateRows[0]["tested_date"];
                        for (int i = 1; i < duplicateRows.Length; i++)
                        {
                            if (dr.RowState == DataRowState.Added)
                            {
                                duplicateRows[i]["tested_date"] = uniqueTestedDate.AddMinutes(i);
                                dr.AcceptChanges();
                                dr.SetAdded();
                            }
                            else
                            {
                                duplicateRows[i]["tested_date"] = uniqueTestedDate.AddMinutes(i);
                            }
                        }
                    }
                }
            }
        }

        private void ScrubData(DataSet ds)
        {
            // Make sure all non-nullable fields do not contain a null value - if they do, replace it with the default value...
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr.RowState != DataRowState.Deleted)
                    {
                        foreach (DataColumn dc in dt.Columns)
                        {
                            if (dc.ExtendedProperties.Contains("is_nullable") &&
                                dc.ExtendedProperties["is_nullable"].ToString().Trim().ToUpper() == "N" &&
                                dr[dc] == DBNull.Value)
                            {
                                if (dc.ExtendedProperties.Contains("default_value") &&
                                    !string.IsNullOrEmpty(dc.ExtendedProperties["default_value"].ToString()) &&
                                    dc.ExtendedProperties["default_value"].ToString().Trim().ToUpper() != "{DBNULL.VALUE}")
                                {
                                    dr[dc] = dc.ExtendedProperties["default_value"].ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        private int SyncSavedResults(DataTable originalTable, DataTable savedResults)
        {
            int errorCount = 0;

            if (savedResults != null && savedResults.PrimaryKey.Length == 1)
            {
                DataSet ds;
                string pKeyCol = savedResults.PrimaryKey[0].ColumnName.Trim().ToUpper();
                savedResults.Columns[pKeyCol].ReadOnly = false;

                // Get a fresh copy of the newly saved records (because it will have created_by and owned_by data - which is needed for future updates)...
                if (!string.IsNullOrEmpty(_orderList))
                {
                    // If these records were derived from a germination order use the order number to retrieve the records
                    // because if the order number is provided the sequence number column will be populated...
                    ds = _sharedUtils.GetWebServiceData(originalTable.TableName, ":orderrequestid" + "=" + _orderList, 0, 0);
                }
                else
                {
                    string savedPKEYS = ":inventoryviabilityid=";
                    // Build the inventory_id list from the savedResults table...
                    foreach (DataRow dr in savedResults.Rows)
                    {
                        savedPKEYS += dr["NewPrimaryKeyID"].ToString() + ",";
                    }
                    ds = _sharedUtils.GetWebServiceData(originalTable.TableName, savedPKEYS.TrimEnd(','), 0, 0);
                }

                foreach (DataRow dr in savedResults.Rows)
                {
                    DataRow originalRow = originalTable.Rows.Find(dr["OriginalPrimaryKeyID"]);

                    switch (dr["SavedAction"].ToString())
                    {
                        case "Insert":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the originalTable row's status for this new row to committed (and update the pkey with the int returned from the server DB)...
                                if (originalRow != null)
                                {
                                    // Get the new row from the database (because it will have created_by and owned_by data - which is needed for future updates)...
                                    if (ds.Tables.Contains(originalTable.TableName) &&
                                        ds.Tables[originalTable.TableName].Rows.Count > 0 &&
                                        ds.Tables[originalTable.TableName].Rows.Contains(dr["NewPrimaryKeyID"]))
                                    {
                                        foreach (DataColumn dc in ds.Tables[originalTable.TableName].Columns)
                                        {
                                            if (dc.ReadOnly)
                                            {
                                                originalRow.Table.Columns[dc.ColumnName].ReadOnly = false;
                                                originalRow[dc.ColumnName] = ds.Tables[originalTable.TableName].Rows.Find(dr["NewPrimaryKeyID"])[dc.ColumnName];
                                                originalRow.Table.Columns[dc.ColumnName].ReadOnly = true;
                                            }
                                            else
                                            {
                                                originalRow[dc.ColumnName] = ds.Tables[originalTable.TableName].Rows.Find(dr["NewPrimaryKeyID"])[dc.ColumnName];
                                            }
                                        }
                                        originalRow.AcceptChanges();
                                        originalRow.ClearErrors();
                                    }
                                    else
                                    {
                                        bool origColumnReadOnlyValue = originalRow.Table.Columns[pKeyCol].ReadOnly;
                                        originalRow.Table.Columns[pKeyCol].ReadOnly = false;
                                        originalRow[pKeyCol] = dr["NewPrimaryKeyID"];
                                        originalRow.AcceptChanges();
                                        originalRow.Table.Columns[pKeyCol].ReadOnly = origColumnReadOnlyValue;
                                        originalRow.ClearErrors();
                                    }
                                }
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Update":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                originalRow.AcceptChanges();
                                originalRow.ClearErrors();
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Delete":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the row's status for this deleted row to committed...
                                if (originalRow != null)
                                {
                                    originalRow.AcceptChanges();
                                    originalRow.ClearErrors();
                                }
                                else
                                {
                                    DataRow deletedRow = null;
                                    foreach (DataRow deleteddr in originalTable.Rows)
                                    {
                                        if (deleteddr.RowState == DataRowState.Deleted && deleteddr[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                        {
                                            deletedRow = deleteddr;
                                        }
                                    }
                                    deletedRow.AcceptChanges();
                                    deletedRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                // Find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in originalTable.Rows)
                                {
                                    if (deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        deletedRow.RejectChanges();
                                        deletedRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            return errorCount;
        }
    }
}
