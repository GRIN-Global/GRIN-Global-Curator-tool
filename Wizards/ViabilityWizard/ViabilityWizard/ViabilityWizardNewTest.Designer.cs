﻿namespace ViabilityWizard
{
    partial class ViabilityWizardNewTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_datagridviewInventoryViability = new System.Windows.Forms.DataGridView();
            this.ux_buttonCreateTests = new System.Windows.Forms.Button();
            this.ux_buttonCancel = new System.Windows.Forms.Button();
            this.ux_buttonDone = new System.Windows.Forms.Button();
            this.ux_buttonPrintLabels = new System.Windows.Forms.Button();
            this.ux_comboboxCrystalReports = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewInventoryViability)).BeginInit();
            this.SuspendLayout();
            // 
            // ux_datagridviewInventoryViability
            // 
            this.ux_datagridviewInventoryViability.AllowUserToAddRows = false;
            this.ux_datagridviewInventoryViability.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_datagridviewInventoryViability.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ux_datagridviewInventoryViability.Location = new System.Drawing.Point(13, 12);
            this.ux_datagridviewInventoryViability.Name = "ux_datagridviewInventoryViability";
            this.ux_datagridviewInventoryViability.Size = new System.Drawing.Size(643, 275);
            this.ux_datagridviewInventoryViability.TabIndex = 0;
            this.ux_datagridviewInventoryViability.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ux_datagridview_CellFormatting);
            this.ux_datagridviewInventoryViability.CellParsing += new System.Windows.Forms.DataGridViewCellParsingEventHandler(this.ux_datagridview_CellParsing);
            this.ux_datagridviewInventoryViability.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.ux_datagridview_CellValueChanged);
            this.ux_datagridviewInventoryViability.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ux_datagridview_DataError);
            this.ux_datagridviewInventoryViability.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ux_datagridview_EditingControlShowing);
            this.ux_datagridviewInventoryViability.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ux_datagridview_KeyDown);
            this.ux_datagridviewInventoryViability.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.ux_datagridview_PreviewKeyDown);
            // 
            // ux_buttonCreateTests
            // 
            this.ux_buttonCreateTests.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCreateTests.Location = new System.Drawing.Point(500, 293);
            this.ux_buttonCreateTests.Name = "ux_buttonCreateTests";
            this.ux_buttonCreateTests.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonCreateTests.TabIndex = 1;
            this.ux_buttonCreateTests.Text = "Save Tests";
            this.ux_buttonCreateTests.UseVisualStyleBackColor = true;
            this.ux_buttonCreateTests.Click += new System.EventHandler(this.ux_buttonCreateTests_Click);
            // 
            // ux_buttonCancel
            // 
            this.ux_buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCancel.Location = new System.Drawing.Point(581, 293);
            this.ux_buttonCancel.Name = "ux_buttonCancel";
            this.ux_buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonCancel.TabIndex = 2;
            this.ux_buttonCancel.Text = "Cancel";
            this.ux_buttonCancel.UseVisualStyleBackColor = true;
            this.ux_buttonCancel.Click += new System.EventHandler(this.ux_buttonCancel_Click);
            // 
            // ux_buttonDone
            // 
            this.ux_buttonDone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonDone.Location = new System.Drawing.Point(405, 293);
            this.ux_buttonDone.Name = "ux_buttonDone";
            this.ux_buttonDone.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonDone.TabIndex = 3;
            this.ux_buttonDone.Text = "Done";
            this.ux_buttonDone.UseVisualStyleBackColor = true;
            this.ux_buttonDone.Visible = false;
            this.ux_buttonDone.Click += new System.EventHandler(this.ux_buttonDone_Click);
            // 
            // ux_buttonPrintLabels
            // 
            this.ux_buttonPrintLabels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_buttonPrintLabels.Location = new System.Drawing.Point(12, 293);
            this.ux_buttonPrintLabels.Name = "ux_buttonPrintLabels";
            this.ux_buttonPrintLabels.Size = new System.Drawing.Size(96, 23);
            this.ux_buttonPrintLabels.TabIndex = 4;
            this.ux_buttonPrintLabels.Text = "Print Labels";
            this.ux_buttonPrintLabels.UseVisualStyleBackColor = true;
            this.ux_buttonPrintLabels.Click += new System.EventHandler(this.ux_buttonPrintLabels_Click);
            // 
            // ux_comboboxCrystalReports
            // 
            this.ux_comboboxCrystalReports.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_comboboxCrystalReports.FormattingEnabled = true;
            this.ux_comboboxCrystalReports.Location = new System.Drawing.Point(114, 295);
            this.ux_comboboxCrystalReports.Name = "ux_comboboxCrystalReports";
            this.ux_comboboxCrystalReports.Size = new System.Drawing.Size(343, 21);
            this.ux_comboboxCrystalReports.TabIndex = 52;
            // 
            // ViabilityWizardNewTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 328);
            this.Controls.Add(this.ux_comboboxCrystalReports);
            this.Controls.Add(this.ux_buttonPrintLabels);
            this.Controls.Add(this.ux_buttonDone);
            this.Controls.Add(this.ux_buttonCancel);
            this.Controls.Add(this.ux_buttonCreateTests);
            this.Controls.Add(this.ux_datagridviewInventoryViability);
            this.Name = "ViabilityWizardNewTest";
            this.Text = "Viability Wizard - New Test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ViabilityWizardNewTest_FormClosing);
            this.Load += new System.EventHandler(this.ViabilityWizardNewTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ux_datagridviewInventoryViability)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ux_datagridviewInventoryViability;
        private System.Windows.Forms.Button ux_buttonCreateTests;
        private System.Windows.Forms.Button ux_buttonCancel;
        private System.Windows.Forms.Button ux_buttonDone;
        private System.Windows.Forms.Button ux_buttonPrintLabels;
        private System.Windows.Forms.ComboBox ux_comboboxCrystalReports;
    }
}