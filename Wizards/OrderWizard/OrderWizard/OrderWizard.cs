﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GRINGlobal.Client.Common;

namespace OrderWizard
{
    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
        //string PreferredDataview { get; }
        //bool EditMode { get; set; }
    }

    public partial class OrderWizard : Form, IGRINGlobalDataWizard
    {
        SharedUtils _sharedUtils;
        DataTable _orderRequest;
        DataTable _orderRequestItem;
        DataTable _orderRequestAction;
        DataTable _orderRequestAttach;
        DataTable _orderRequestPhytoLog;
        DataTable _orderRequestStatusCodes;
        DataTable _webOrderRequest;
        DataTable _webOrderRequestItem;
        DataTable _webOrderRequestAction;
        DataTable _webOrderRequestAttach;
        DataTable _webOrderRequestStatusCodes;
        BindingSource _orderRequestBindingSource;
        BindingSource _orderRequestItemBindingSource;
        BindingSource _orderRequestActionBindingSource;
        BindingSource _orderRequestAttachBindingSource;
        BindingSource _orderRequestPhytoLogBindingSource;
        BindingSource _webOrderRequestBindingSource;
        BindingSource _webOrderRequestItemBindingSource;
        string _originalPKeys = "";
        string _orderRequestPKeys = "";
        string _orderRequestStatusFilter = "";
        string _webOrderRequestStatusFilter = "";
        DataSet _changedRecords = new DataSet();
        int _mouseClickDGVColumnIndex;
        int _mouseClickDGVRowIndex;
        string _sortOrder = "";
        string _selectionList = "";
        char _lastDGVCharPressed;
        int[] _geoExceptionsList;
        string _selectedFinalRecipientCooperatorID = "";
        string _selectedShipToRecipientCooperatorID = "";
        string _cooperator_name = "";
        string _cooperator_city = "";
        string _cooperator_geography = "";
        string _cooperator_email = "";
        string _cooperator_site_longname = "";
        string _cooperator_site_shortname = "";

    public OrderWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            // Wire up the event handlers for Order Request binding source...
            _orderRequestBindingSource = new BindingSource();
            _orderRequestBindingSource.ListChanged += new ListChangedEventHandler(_orderRequestBindingSource_ListChanged);
            _orderRequestBindingSource.CurrentChanged += new EventHandler(_orderRequestBindingSource_CurrentChanged);
            _orderRequestItemBindingSource = new BindingSource();
            _orderRequestActionBindingSource = new BindingSource();
            _orderRequestAttachBindingSource = new BindingSource();
            _orderRequestPhytoLogBindingSource = new BindingSource();
            _sharedUtils = sharedUtils;
            _originalPKeys = pKeys;
            foreach (string pkeyToken in pKeys.Split(';'))
            {
                if (pkeyToken.Split('=')[0].Trim().ToUpper() == ":ORDERREQUESTID") _selectionList = pkeyToken.Split('=')[1];
            }

            // Wire up the event handlers for Web Order Request binding source...
            _webOrderRequestBindingSource = new BindingSource();
            _webOrderRequestBindingSource.CurrentChanged += new EventHandler(_webOrderRequestBindingSource_CurrentChanged);
            _webOrderRequestItemBindingSource = new BindingSource();

            // Make the filter groupboxes the same size and location...
            ux_groupboxWebOrderFilters.Size = ux_groupboxOrderFilters.Size;
            ux_groupboxWebOrderFilters.Location = ux_groupboxOrderFilters.Location;
            ux_groupboxWebOrderFilters.Visible = false;
            ux_groupboxOrderFilters.Visible = true;
        }

        private void OrderWizard_Load(object sender, EventArgs e)
        {
            this.Text = FormName;

            // Add items to the order request status checked listbox control...
            _orderRequestStatusCodes = _sharedUtils.GetLocalData("select * from code_value_lookup where group_name=@groupname", "@groupname=ORDER_REQUEST_ITEM_STATUS");
            if (_orderRequestStatusCodes != null &&
                _orderRequestStatusCodes.Columns.Contains("value_member") &&
                _orderRequestStatusCodes.Columns.Contains("display_member"))
            {
                ux_checkedlistboxOrderItemStatus.Items.Clear();
                _orderRequestStatusFilter = "";
                foreach (DataRow dr in _orderRequestStatusCodes.Rows)
                {
                    if (dr["value_member"].ToString().ToUpper() == "NEW")
                    {
                        ux_checkedlistboxOrderItemStatus.Items.Add(dr["display_member"].ToString(), true);
                    }
                    else
                    {
                        ux_checkedlistboxOrderItemStatus.Items.Add(dr["display_member"].ToString(), false);
                    }
                }
            }

            // Add items to the web order request status checked listbox control...
            // Attempt to get the WEB_ORDER_REQUEST_ITEM_STATUS codes first...
            _webOrderRequestStatusCodes = _sharedUtils.GetLocalData("select * from code_value_lookup where group_name=@groupname", "@groupname=WEB_ORDER_REQUEST_ITEM_STATUS");
            // If the WEB_ORDER_REQUEST_ITEM_STATUS codes don't exist use the WEB_ORDER_REQUEST_STATUS codes instead...
            if (_webOrderRequestStatusCodes.Rows.Count < 1) _webOrderRequestStatusCodes = _sharedUtils.GetLocalData("select * from code_value_lookup where group_name=@groupname", "@groupname=WEB_ORDER_REQUEST_STATUS");
            if (_webOrderRequestStatusCodes != null &&
                _webOrderRequestStatusCodes.Columns.Contains("value_member") &&
                _webOrderRequestStatusCodes.Columns.Contains("display_member"))
            {
                ux_checkedlistboxWebOrderItemStatus.Items.Clear();
                _webOrderRequestStatusFilter = "";
                foreach (DataRow dr in _webOrderRequestStatusCodes.Rows)
                {
                    if (dr["value_member"].ToString().ToUpper() == "SUBMITTED" ||
                        dr["value_member"].ToString().ToUpper() == "NEW")
                    {
                        ux_checkedlistboxWebOrderItemStatus.Items.Add(dr["display_member"].ToString(), true);
                    }
                    else
                    {
                        ux_checkedlistboxWebOrderItemStatus.Items.Add(dr["display_member"].ToString(), false);
                    }
                }
            }

            // Load the list of Crystal Reports...
//            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.Windows.Forms.Application.StartupPath + "\\Reports");
            System.Collections.Generic.Dictionary<string, string> reportsMap = _sharedUtils.GetReportsMapping();
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports");

            foreach (string reportName in _sharedUtils.GetAppSettingValue("OrderWizardCrystalReports").Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                foreach (System.IO.FileInfo fi in di.GetFiles(reportName.Trim(), System.IO.SearchOption.AllDirectories))
                {
                    if (fi.Name.Trim().ToUpper() == reportName.Trim().ToUpper())
                    {
                        ux_comboboxCrystalReports.Items.Add(fi.Name);
                    }
                }
            }

            ux_radiobuttonSelectionOrders.Checked = true;
            // Bind the bindingsource to the binding navigator toolstrip on the main form...
            RefreshOrderData();
            ux_bindingnavigatorForm.BindingSource = _orderRequestBindingSource;

            // Bind the bindingsource to the binding navigator toolstrip on the web_order_request tab page...
            RefreshWebOrderData();
            ux_bindingNavigatorWebOrders.BindingSource = _webOrderRequestBindingSource;

            // Bind and format the controls on the Order tab...
            bindControls(OrdersPage.Controls, _orderRequestBindingSource);
            formatControls(OrdersPage.Controls, _orderRequestBindingSource);

            // Populate the Order Type Filter combobox....
            _sharedUtils.BindComboboxToCodeValue(ux_comboboxOrderTypeFilter, _orderRequest.Columns["order_type_code"]);

            // Re-Bind (but don't format) the controls on the Web Order tab to a different binding source...
            bindControls(WebOrderPage.Controls, _webOrderRequestBindingSource);

            if (_orderRequestBindingSource.List.Count > 0)
            {
                OrdersPage.Enabled = true;
                ActionsPage.Enabled = true;
            }
            else
            {
                OrdersPage.Enabled = false;
                ActionsPage.Enabled = false;
            }

            // Populate the SMTA geography_id exceptions list...
            string[] geoExceptionsStringList = _sharedUtils.GetAppSettingValue("SMTA_GEOGRAPHY_ID_EXCEPTIONS_LIST").Split(',');
            int i = 0;
            if (geoExceptionsStringList != null && geoExceptionsStringList.Length > 0)
            {
                _geoExceptionsList = new int[geoExceptionsStringList.Length];
                int geography_id;
                foreach (string geo_id in geoExceptionsStringList)
                {
                    if (int.TryParse(geo_id, out geography_id))
                    {
                        _geoExceptionsList[i] = geography_id;
                        i++;
                    }
                }
            }
            else
            {
                _geoExceptionsList = new int[1] { 0 };
            }

            // Get the record for the cooperator using this wizard - this info will be used for detailed notes in 
            // the order_request_action table (the note is inserted during processing of the 'SaveOrderData()' and other methods)...
            DataSet ds = _sharedUtils.GetWebServiceData("get_cooperator", ":cooperatorid=" + _sharedUtils.UserCooperatorID.ToString(), 0, 0);
            if (ds.Tables.Contains("get_cooperator") &&
                ds.Tables["get_cooperator"].Rows.Count > 0)
            {
                DataRow drCoop = ds.Tables["get_cooperator"].Rows[0];
                if (!string.IsNullOrEmpty(drCoop["first_name"].ToString().Trim())) _cooperator_name = drCoop["first_name"].ToString().Trim();
                if (!string.IsNullOrEmpty(drCoop["last_name"].ToString().Trim())) _cooperator_name = _cooperator_name + " " + drCoop["last_name"].ToString().Trim();
                if (!string.IsNullOrEmpty(drCoop["city"].ToString().Trim())) _cooperator_city = drCoop["city"].ToString().Trim();
                if (!string.IsNullOrEmpty(drCoop["geography_id"].ToString().Trim())) _cooperator_geography = _sharedUtils.GetLookupDisplayMember("mailing_geography_lookup", drCoop["geography_id"].ToString().Trim(), "", drCoop["geography_id"].ToString().Trim());
                if (!string.IsNullOrEmpty(drCoop["email"].ToString().Trim())) _cooperator_email = drCoop["email"].ToString().Trim();
            }

            // Get the site record for the cooperator using this wizard - this info will be used for detailed notes in 
            // the order_request_action table (the note is inserted during processing of the 'SaveOrderData()' and other methods)...
            ds = _sharedUtils.GetWebServiceData("get_site", ":siteid=" + _sharedUtils.UserSiteID.ToString(), 0, 0);
            if (ds.Tables.Contains("get_site") &&
                ds.Tables["get_site"].Rows.Count > 0)
            {
                DataRow drSite = ds.Tables["get_site"].Rows[0];
                if (!string.IsNullOrEmpty(drSite["site_long_name"].ToString().Trim())) _cooperator_site_longname = drSite["site_long_name"].ToString().Trim();
                if (!string.IsNullOrEmpty(drSite["site_short_name"].ToString().Trim())) _cooperator_site_shortname = drSite["site_short_name"].ToString().Trim();
            }

            // Get language translations for the components and controls in this applicataion...
            if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);

            // Force the row filters to be applied...
            _orderRequestBindingSource_CurrentChanged(sender, e);
        }

        public string FormName
        {
            get
            {
                return "Order Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                if (_orderRequest != null && _changedRecords != null && _changedRecords.Tables.Contains(_orderRequest.TableName))
                {
                    dt = _changedRecords.Tables[_orderRequest.TableName].Copy();
                }
                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "order_request_id";
            }
        }

        private void OrderWizard_FormClosed(object sender, FormClosedEventArgs e)
        {
            _orderRequestBindingSource.ListChanged -= _orderRequestBindingSource_ListChanged;
            _orderRequestBindingSource.CurrentChanged -= _orderRequestBindingSource_CurrentChanged;
            _webOrderRequestBindingSource.CurrentChanged -= _webOrderRequestBindingSource_CurrentChanged;
        }

        private void OrderWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            // The user might be closing the form during the middle of edit changes in the datagridview - if so ask the
            // user if they would like to save their data...
            int intRowEdits = 0;

            _orderRequestBindingSource.EndEdit();
            if (_orderRequest != null && _orderRequest.GetChanges() != null) intRowEdits = _orderRequest.GetChanges().Rows.Count;
            _orderRequestItemBindingSource.EndEdit();
            if (_orderRequestItem != null && _orderRequestItem.GetChanges() != null) intRowEdits += _orderRequestItem.GetChanges().Rows.Count;
            _orderRequestActionBindingSource.EndEdit();
            if (_orderRequestAction != null && _orderRequestAction.GetChanges() != null) intRowEdits += _orderRequestAction.GetChanges().Rows.Count;
            _orderRequestAttachBindingSource.EndEdit();
            if (_orderRequestAttach != null && _orderRequestAttach.GetChanges() != null) intRowEdits += _orderRequestAttach.GetChanges().Rows.Count;
            _orderRequestPhytoLogBindingSource.EndEdit();
            if (_orderRequestPhytoLog != null && _orderRequestPhytoLog.GetChanges() != null) intRowEdits += _orderRequestPhytoLog.GetChanges().Rows.Count;
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to cancel your edits and close this window?", "Cancel Edits and Close", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "OrderWizard_FormClosingMessage1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = intRowEdits.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (intRowEdits > 0 && DialogResult.No == ggMessageBox.ShowDialog())
            {
                e.Cancel = true;
            }
        }

        #region Customizations to Dynamic Controls...
        private void ux_textboxFinalDestination_TextChanged(object sender, EventArgs e)
        {
            bindingNavigatorFormOrderNumber.Text = "";

            if (string.IsNullOrEmpty(ux_textboxFinalDestination.Text) &&
                _orderRequest.Columns[ux_textboxFinalDestination.Tag.ToString()].ExtendedProperties.Contains("is_nullable") &&
                _orderRequest.Columns[ux_textboxFinalDestination.Tag.ToString()].ExtendedProperties["is_nullable"].ToString() == "N" &&
                !_orderRequest.Columns[ux_textboxFinalDestination.Tag.ToString()].ReadOnly)
            {
                ux_textboxFinalDestination.BackColor = Color.Plum;
            }
            else
            {
                ux_textboxFinalDestination.BackColor = Color.Empty;
                if (string.IsNullOrEmpty(ux_textboxRequestor.Text))
                {
                    ux_textboxRequestor.Text = ux_textboxFinalDestination.Text;
                }
                if (string.IsNullOrEmpty(ux_textboxShipTo.Text))
                {
                    ux_textboxShipTo.Text = ux_textboxFinalDestination.Text;
                }
            }
        }
        #endregion

        #region Dynamic Controls logic...
        private void bindControls(Control.ControlCollection controlCollection, BindingSource bindingSource)
        {
            foreach (Control ctrl in controlCollection)
            {
                //if (ctrl != ux_bindingnavigatorForm)  // Leave the bindingnavigator alone
                if (!(ctrl is BindingNavigator))  // Leave the bindingnavigators alone
                {
                    // If the ctrl has children - bind them too...
                    if (ctrl.Controls.Count > 0)
                    {
                        bindControls(ctrl.Controls, bindingSource);
                    }
                    // Bind the control (by type)...
                    if (ctrl is ComboBox) bindComboBox((ComboBox)ctrl, bindingSource);
                    if (ctrl is TextBox) bindTextBox((TextBox)ctrl, bindingSource);
                    if (ctrl is CheckBox) bindCheckBox((CheckBox)ctrl, bindingSource);
                    if (ctrl is DateTimePicker) bindDateTimePicker((DateTimePicker)ctrl, bindingSource);
                    if (ctrl is Label) bindLabel((Label)ctrl, bindingSource);
                }
            }
        }

        private void formatControls(Control.ControlCollection controlCollection, BindingSource bindingSource)
        {
            foreach (Control ctrl in controlCollection)
            {
                if (ctrl != ux_bindingnavigatorForm)  // Leave the bindingnavigator alone
                {
                    // If the ctrl has children - set their edit mode too...
                    if (ctrl.Controls.Count > 0)
                    {
                        formatControls(ctrl.Controls, bindingSource);
                    }
                    // Set the edit mode for the control...
                    if (ctrl != null &&
                        ctrl.Tag != null &&
                        ctrl.Tag is string &&
                        bindingSource != null &&
                        bindingSource.DataSource is DataTable &&
                        ((DataTable)bindingSource.DataSource).Columns.Contains(ctrl.Tag.ToString().Trim().ToLower()))
                    {
                        // If the field is a ReadOnly field do not allow the tab key to be used to navigate to it...
                        ctrl.TabStop = !((DataTable)bindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        // Set the control's edit properties based on what type of control it is, what edit mode is current, and if the field is readonly...
                        if (ctrl is TextBox)
                        {
                            // TextBoxes have a ReadOnly property in addition to an Enabled property so we handle this one separate...
                            ((TextBox)ctrl).ReadOnly = ((DataTable)bindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        }
                        else if (ctrl is Label)
                        {
                            // Do nothing to the Label
                        }
                        else
                        {
                            // All other control types (ComboBox, CheckBox, DateTimePicker) except Labels...
                            ctrl.Enabled = !((DataTable)bindingSource.DataSource).Columns[ctrl.Tag.ToString().Trim().ToLower()].ReadOnly;
                        }
                    }
                }
            }
        }

        private void bindComboBox(ComboBox comboBox, BindingSource bindingSource)
        {
            comboBox.DataBindings.Clear();
            if (comboBox != null &&
                comboBox.Tag is string &&
                !string.IsNullOrEmpty(comboBox.Tag.ToString().Trim()))
            {
                comboBox.Enabled = false;
            }
            if (comboBox != null &&
                comboBox.Tag != null &&
                comboBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(comboBox.Tag.ToString().Trim().ToLower()))
            {
                if (_sharedUtils != null)
                {
                    DataColumn dc = ((DataTable)bindingSource.DataSource).Columns[comboBox.Tag.ToString().Trim().ToLower()];
                    _sharedUtils.BindComboboxToCodeValue(comboBox, dc);
                    if (comboBox.DataSource.GetType() == typeof(DataTable))
                    {
                        // Calculate the maximum width needed for displaying the dropdown items and set the combobox property...
                        int maxWidth = comboBox.DropDownWidth;
                        foreach (DataRow dr in ((DataTable)comboBox.DataSource).Rows)
                        {
                            if (TextRenderer.MeasureText(dr["display_member"].ToString().Trim(), comboBox.Font).Width > maxWidth)
                            {
                                maxWidth = TextRenderer.MeasureText(dr["display_member"].ToString().Trim(), comboBox.Font).Width;
                            }
                        }
                        comboBox.DropDownWidth = maxWidth;
                    }

                    // Bind the SelectedValue property to the binding source...
                    comboBox.DataBindings.Add("SelectedValue", bindingSource, comboBox.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);

                    // Wire up to an event handler if this column is a date_code (format) field...
                    if (dc.ColumnName.Trim().ToLower().EndsWith("_code") &&
                        dc.Table.Columns.Contains(dc.ColumnName.Trim().ToLower().Substring(0, dc.ColumnName.Trim().ToLower().LastIndexOf("_code"))))
                    {
                        comboBox.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
                    }
                }
                else
                {
                    // Bind the Text property to the binding source...
                    comboBox.DataBindings.Add("Text", bindingSource, comboBox.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);
                }
            }
        }

        private void bindTextBox(TextBox textBox, BindingSource bindingSource)
        {
            textBox.DataBindings.Clear();
            if (textBox != null &&
                textBox.Tag is string &&
                !string.IsNullOrEmpty(textBox.Tag.ToString().Trim()))
            {
                textBox.ReadOnly = true;
            }
            if (textBox != null &&
                textBox.Tag != null &&
                textBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(textBox.Tag.ToString().Trim().ToLower()))
            {
                DataTable dt = (DataTable)bindingSource.DataSource;
                DataColumn dc = dt.Columns[textBox.Tag.ToString().Trim().ToLower()];
                if (_sharedUtils.LookupTablesIsValidFKField(dc))
                {
                    // Create a new binding that handles display_member/value_member conversions...
                    Binding textBinding = new Binding("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                    textBinding.Format += new ConvertEventHandler(textLUBinding_Format);
                    textBinding.Parse += new ConvertEventHandler(textLUBinding_Parse);
                    textBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                    // Bind it to the textbox...
                    textBox.DataBindings.Add(textBinding);
                }
                else if (dc.DataType == typeof(DateTime))
                {
                    // Create a new binding that handles display_member/value_member conversions...
                    Binding textBinding = new Binding("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                    textBinding.Format += new ConvertEventHandler(textDateTimeBinding_Format);
                    textBinding.Parse += new ConvertEventHandler(textDateTimeBinding_Parse);
                    textBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                    // Bind it to the textbox...
                    textBox.DataBindings.Add(textBinding);
                }
                else
                {
                    // Bind to a plain-old text field in the database (no LU required)...
                    textBox.DataBindings.Add("Text", bindingSource, textBox.Tag.ToString().Trim().ToLower());
                }

                // Add an event handler for processing the first key press (to display the lookup picker dialog)...
                textBox.KeyDown += new KeyEventHandler(textBox_KeyDown);
                textBox.KeyPress += new KeyPressEventHandler(textBox_KeyPress);
            }
        }

        private void bindCheckBox(CheckBox checkBox, BindingSource bindingSource)
        {
            checkBox.DataBindings.Clear();
            if (checkBox != null &&
                checkBox.Tag is string &&
                !string.IsNullOrEmpty(checkBox.Tag.ToString().Trim()))
            {
                checkBox.Enabled = false;
            }
            if (checkBox != null &&
                checkBox.Tag != null &&
                checkBox.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(checkBox.Tag.ToString().Trim().ToLower()))
            {
                DataTable dt = (DataTable)bindingSource.DataSource;
                DataColumn dc = dt.Columns[checkBox.Tag.ToString().Trim().ToLower()];
                checkBox.Text = dc.Caption;
                Binding boolBinding = new Binding("Checked", bindingSource, checkBox.Tag.ToString().Trim().ToLower());
                boolBinding.Format += new ConvertEventHandler(boolBinding_Format);
                boolBinding.Parse += new ConvertEventHandler(boolBinding_Parse);
                boolBinding.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
                checkBox.DataBindings.Add(boolBinding);
            }
        }

        private void bindDateTimePicker(DateTimePicker dateTimePicker, BindingSource bindingSource)
        {
            dateTimePicker.DataBindings.Clear();
            if (dateTimePicker != null &&
                dateTimePicker.Tag is string &&
                !string.IsNullOrEmpty(dateTimePicker.Tag.ToString().Trim()))
            {
                dateTimePicker.Enabled = false;
            }
            if (dateTimePicker != null &&
                dateTimePicker.Tag != null &&
                dateTimePicker.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(dateTimePicker.Tag.ToString().Trim().ToLower()))
            {
                // Now bind the control to the column in the bindingSource...
                dateTimePicker.DataBindings.Add("Text", bindingSource, dateTimePicker.Tag.ToString().Trim().ToLower(), true, DataSourceUpdateMode.OnPropertyChanged);
            }
        }

        private void bindLabel(Label label, BindingSource bindingSource)
        {
            if (label != null &&
                label.Tag != null &&
                label.Tag is string &&
                bindingSource != null &&
                bindingSource.DataSource is DataTable &&
                ((DataTable)bindingSource.DataSource).Columns.Contains(label.Tag.ToString().Trim().ToLower()))
            {
                //label.DataBindings.Add("Text", bindingSource, label.Tag.ToString().Trim().ToLower());
                label.Text = ((DataTable)bindingSource.DataSource).Columns[label.Tag.ToString().Trim().ToLower()].Caption;
            }
        }

        void boolBinding_Format(object sender, ConvertEventArgs e)
        {
            switch (e.Value.ToString().ToUpper())
            {
                case "Y":
                    e.Value = true;
                    break;
                case "N":
                    e.Value = false;
                    break;
                default:
                    e.Value = false;
                    break;
            }
        }

        void boolBinding_Parse(object sender, ConvertEventArgs e)
        {
            if (e.Value != null)
            {
                switch ((bool)e.Value)
                {
                    case true:
                        e.Value = "Y";
                        break;
                    case false:
                        e.Value = "N";
                        break;
                    default:
                        e.Value = "N";
                        break;
                }
            }
            else
            {
                e.Value = "N";
            }
        }

        void textDateTimeBinding_Format(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (dc.DataType == typeof(DateTime) &&
                !string.IsNullOrEmpty(e.Value.ToString()) &&
                dt.Columns.Contains(dc.ColumnName.Trim().ToLower() + "_code"))
            {
                DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                string dateFormat = "MM/dd/yyyy";
                dateFormat = drv[dc.ColumnName + "_code"].ToString().Trim();
                e.Value = ((DateTime)e.Value).ToString(dateFormat);
            }
            else if (dc.ColumnName.ToLower().Trim() == "ordered_date" ||
                dc.ColumnName.ToLower().Trim() == "completed_date")
            {
                DateTime formattedDate;
                if (DateTime.TryParse(e.Value.ToString(), out formattedDate))
                {
                    e.Value = formattedDate.ToString("d");
                }
            }
        }

        void textDateTimeBinding_Parse(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (dc.DataType == typeof(DateTime) &&
                !string.IsNullOrEmpty(e.Value.ToString()) &&
                dt.Columns.Contains(dc.ColumnName.Trim().ToLower() + "_code"))
            {
                DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                string dateFormat = drv[dc.ColumnName + "_code"].ToString().Trim();
                DateTime parsedDateTime;
                if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out parsedDateTime))
                {
                    e.Value = parsedDateTime;
                }
            }
        }

        void textLUBinding_Format(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (!string.IsNullOrEmpty(e.Value.ToString()))
            {
                //e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_resultset_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
                e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
            }
        }

        void textLUBinding_Parse(object sender, ConvertEventArgs e)
        {
            Binding b = (Binding)sender;
            DataTable dt = (DataTable)((BindingSource)b.DataSource).DataSource;
            DataColumn dc = dt.Columns[b.BindingMemberInfo.BindingMember];
            if (!string.IsNullOrEmpty(e.Value.ToString()))
            {
                //e.Value = _sharedUtils.GetLookupValueMember(dc.ExtendedProperties["foreign_key_resultset_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());  ((DataRowView)((BindingSource)b.DataSource).Current).Row
                e.Value = _sharedUtils.GetLookupValueMember(((DataRowView)((BindingSource)b.DataSource).Current).Row, dc.ExtendedProperties["foreign_key_dataview_name"].ToString(), e.Value.ToString(), "", e.Value.ToString());
            }
        }

        void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            string dateColumnName = cb.Tag.ToString().Replace("_code", "");
            foreach (Binding b in cb.DataBindings)
            {
                foreach (Control ctrl in cb.Parent.Controls)
                {
                    if (ctrl.Tag.ToString() == dateColumnName &&
                        ctrl.GetType() == typeof(TextBox))
                    {
                        DataRowView drv = (DataRowView)((BindingSource)b.DataSource).Current;
                        string dateFormat = "MM/dd/yyyy";
                        dateFormat = drv[cb.Tag.ToString()].ToString().Trim();
                        DateTime dt;
                        if (DateTime.TryParseExact(drv[dateColumnName].ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                        {
                            ctrl.Text = ((DateTime)drv[dateColumnName]).ToString(dateFormat);
                        }
                    }
                }
            }
        }

        void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (TextBox)sender;


            if (!tb.ReadOnly)
            {
                foreach (Binding b in tb.DataBindings)
                {
                    if (b.BindingManagerBase != null &&
                        b.BindingManagerBase.Current != null &&
                        b.BindingManagerBase.Current is DataRowView &&
                        b.BindingMemberInfo.BindingField != null)
                    {
                        if (_sharedUtils.LookupTablesIsValidFKField(((DataRowView)b.BindingManagerBase.Current).Row.Table.Columns[b.BindingMemberInfo.BindingField]) &&
                            e.KeyChar != Convert.ToChar(Keys.Escape)) // Ignore the Escape key and process anything else...
                        {
                            string filterText = tb.Text;
                            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), "[a-zA-Z0-9]"))
                            {
                                filterText = e.KeyChar.ToString();
                            }
                            LookupTablePicker ltp = new LookupTablePicker(_sharedUtils, tb.Tag.ToString(), ((DataRowView)b.BindingManagerBase.Current).Row, filterText);
                            ltp.StartPosition = FormStartPosition.CenterParent;
                            if (DialogResult.OK == ltp.ShowDialog())
                            {
                                tb.Text = ltp.NewValue.Trim();
                            }
                            e.Handled = true;
                        }
                    }
                }
            }
        }

        void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) // Process the Delete key (since it is not passed on to the KeyPress event handler)...
            {
                TextBox tb = (TextBox)sender;

                if (!tb.ReadOnly)
                {
                    foreach (Binding b in tb.DataBindings)
                    {
                        if (b.BindingManagerBase != null &&
                            b.BindingManagerBase.Current != null &&
                            b.BindingManagerBase.Current is DataRowView &&
                            b.BindingMemberInfo.BindingField != null)
                        {
                            // Just in case the user selected only a part of the full text to delete - strip out the selected text and process normally...
                            string remainingText = tb.Text.Remove(tb.SelectionStart, tb.SelectionLength);
                            if (string.IsNullOrEmpty(remainingText))
                            {
                                // When a textbox is bound to a table - some datatypes will not revert to a DBNull via the bound control - so
                                // take control of the update and force the field back to a null (non-nullable fields should show up to the GUI with colored background)...
                                ((DataRowView)b.BindingManagerBase.Current).Row[b.BindingMemberInfo.BindingField] = DBNull.Value;
                                b.ReadValue();
                                e.Handled = true;
                            }
                            else
                            {
                                if (_sharedUtils.LookupTablesIsValidFKField(((DataRowView)b.BindingManagerBase.Current).Row.Table.Columns[b.BindingMemberInfo.BindingField]))
                                {
                                    LookupTablePicker ltp = new LookupTablePicker(_sharedUtils, tb.Tag.ToString(), ((DataRowView)b.BindingManagerBase.Current).Row, remainingText);
                                    ltp.StartPosition = FormStartPosition.CenterParent;
                                    if (DialogResult.OK == ltp.ShowDialog())
                                    {
                                        tb.Text = ltp.NewValue.Trim();
                                        b.WriteValue();
                                        e.Handled = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region DGV control logic...
        private void ux_datagridview_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            // Get the defaultview of the datatable used by the dgv...
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }

            // Begin processing cell formatting based on the datatype of the field...
            if (dv != null &&
                e.ColumnIndex > -1 &&
                e.RowIndex > -1 &&
                e.RowIndex < dv.Count)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                // Format FKey fields...
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    e.RowIndex < dv.Count &&
                    dv[e.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (dv[e.RowIndex][e.ColumnIndex] != DBNull.Value)
                    {
                        e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim(), dv[e.RowIndex][e.ColumnIndex].ToString().Trim(), "", dv[e.RowIndex][e.ColumnIndex].ToString().Trim());
                    }
                    dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    e.FormattingApplied = true;
                }
                // Format date/time fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        e.Value = ((DateTime)dv[e.RowIndex][e.ColumnIndex]).ToString(dateFormat);
                        e.FormattingApplied = true;
                    }
                }
                // Format integer fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    (dc.DataType == typeof(int) ||
                    dc.DataType == typeof(Int16) ||
                    dc.DataType == typeof(Int32) ||
                    dc.DataType == typeof(Int64)))
                {
                    int junk;
                    if (!int.TryParse(e.Value.ToString(), out junk))
                    {
                        dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    }
                }

                if (dc.ReadOnly)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }

                if (dc.ExtendedProperties.Contains("is_nullable") &&
                    dc.ExtendedProperties["is_nullable"].ToString() == "N" &&
                    string.IsNullOrEmpty(dv[e.RowIndex][e.ColumnIndex].ToString()))
                {
                    e.CellStyle.BackColor = Color.Plum;
                }
            }
        }

        private void ux_datagridview_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }
            if (dv != null && e.ColumnIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        DateTime formattedDate;
                        if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out formattedDate))
                        {
                            e.Value = formattedDate;
                            e.ParsingApplied = true;
                        }
                    }
                }
            }
        }

        private void ux_datagridview_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string errorMessage = e.Exception.Message;
            int columnWithError = -1;

            // Find the cell the error belongs to (don't use e.ColumnIndex because it points to the current cell *NOT* the offending cell)...
            foreach (DataGridViewColumn col in dgv.Columns)
            {
                if (errorMessage.Contains(col.Name))
                {
                    dgv[col.Name, e.RowIndex].ErrorText = errorMessage;
                    columnWithError = col.Index;
                }
            }
        }

        private void ux_datagridview_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataTable dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;
            string columnName = dgv.CurrentCell.OwningColumn.Name;
            DataColumn dc = dt.Columns[columnName];
            DataRow dr;

            if (_sharedUtils.LookupTablesIsValidFKField(dc))
            {
                //string luTableName = dc.ExtendedProperties["foreign_key_resultset_name"].ToString().Trim();
                string luTableName = dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim();
                DataGridViewCell currentDGVCell = dgv.CurrentCell;
                dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
                string suggestedFilter = dgv.CurrentCell.EditedFormattedValue.ToString();
                if (_lastDGVCharPressed > 0) suggestedFilter = _lastDGVCharPressed.ToString();
                GRINGlobal.Client.Common.LookupTablePicker ltp = new GRINGlobal.Client.Common.LookupTablePicker(_sharedUtils, columnName, dr, suggestedFilter);
                //_lastDGVCharPressed = (char)0;
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    if (dr != null)
                    {
                        if (ltp.NewKey != null && dr[dgv.CurrentCell.ColumnIndex].ToString().Trim() != ltp.NewKey.Trim())
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = ltp.NewKey.Trim();
                            if (dgv.CurrentCell != null && currentDGVCell == dgv.CurrentCell) dgv.CurrentCell.Value = ltp.NewValue.Trim();
                        }
                        else if (ltp.NewKey == null)
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = DBNull.Value;
                            if (dgv.CurrentCell != null && currentDGVCell == dgv.CurrentCell) dgv.CurrentCell.Value = "";
                        }
                        dr.SetColumnError(currentDGVCell.ColumnIndex, null);
                    }
                }
                dgv.EndEdit();
            }
        }

        private void ux_datagridviewOrderRequestAttach_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (ux_tabcontrolMain.SelectedTab == AttachmentsPage)
            {
                // Change cursor to the wait cursor...
                Cursor origCursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;

                ux_webbrowserAttachment.Navigate("http://");
                if (ux_datagridviewOrderRequestAttach.Columns.Contains("local_path"))
                {
                    string attachLocalPath = ux_datagridviewOrderRequestAttach.Rows[e.RowIndex].Cells["local_path"].FormattedValue.ToString();
                    string attachVirtualPath = ux_datagridviewOrderRequestAttach.Rows[e.RowIndex].Cells["virtual_path"].FormattedValue.ToString();
                    if (System.IO.File.Exists(attachLocalPath))
                    {
                        ux_webbrowserAttachment.Navigate(attachLocalPath);
                        try
                        {
                            if (!Image.FromFile(attachLocalPath).PhysicalDimension.IsEmpty)
                            {
                                SHDocVw.WebBrowser wb = (SHDocVw.WebBrowser)ux_webbrowserAttachment.ActiveXInstance;
                                Image tempImage = Image.FromFile(attachLocalPath);
                                int zoomWidth = 100 * ux_webbrowserAttachment.Width / tempImage.Width;
                                int zoomHeight = 100 * ux_webbrowserAttachment.Height / tempImage.Height;
                                int zoom = Math.Min(zoomWidth, zoomHeight);
                                if (zoom < 1) zoom = 100;
                                wb.ExecWB(SHDocVw.OLECMDID.OLECMDID_OPTICAL_ZOOM, SHDocVw.OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER, zoom, null);
                            }
                        }
                        catch
                        {
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(attachLocalPath) &&
                            !string.IsNullOrEmpty(attachVirtualPath))
                        {
                            // Get the attachment data...
                            byte[] attachBytes = _sharedUtils.GetAttachment(attachVirtualPath);
                            if (attachBytes != null && attachBytes.Length > 0)
                            {
                                // Make sure the directory exists before writing the attachment data to disk...
                                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(attachLocalPath));
                                // Create a local copy of the attachment...
                                System.IO.File.WriteAllBytes(attachLocalPath, attachBytes);
                                ux_webbrowserAttachment.Navigate(attachLocalPath);
                                try
                                {
                                    if (!Image.FromFile(attachLocalPath).PhysicalDimension.IsEmpty)
                                    {
                                        SHDocVw.WebBrowser wb = (SHDocVw.WebBrowser)ux_webbrowserAttachment.ActiveXInstance;
                                        Image tempImage = Image.FromFile(attachLocalPath);
                                        int zoomWidth = 100 * ux_webbrowserAttachment.Width / tempImage.Width;
                                        int zoomHeight = 100 * ux_webbrowserAttachment.Height / tempImage.Height;
                                        int zoom = Math.Min(zoomWidth, zoomHeight);
                                        if (zoom < 1) zoom = 100;
                                        wb.ExecWB(SHDocVw.OLECMDID.OLECMDID_OPTICAL_ZOOM, SHDocVw.OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER, zoom, null);
                                    }
                                }
                                catch
                                {
                                }
                            }
                        }
                        else
                        {
                            ux_webbrowserAttachment.Navigate("http://");
                        }
                    }
                }

                // Restore cursor to default cursor...
                Cursor.Current = origCursor;
            }
        }

        #endregion

        #region Binding Navigator Logic...
        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            // Pre-populate the form with default values...
            ux_textboxSite.Text = _sharedUtils.UserSite;
            ux_comboboxOrderType.SelectedValue = "DI";
            ux_comboboxStatus.SelectedValue = "NEW";
            ux_textboxOrderedDate.Text = DateTime.Today.ToString("d");
            ((DataRowView)_orderRequestBindingSource.Current)["ordered_date"] = DateTime.Today.ToString("d");
            ux_checkboxIsCompleted.Checked = false;
            ux_checkboxIsSupplyLow.Checked = false;

            // Set focus to the final_recipient...
            ux_textboxFinalDestination.Select();
            ux_textboxFinalDestination.Focus();

            // Create an Action for the status_code changes (one per staus_code with a count for how many items were changed to that status)...
            DataRow newOrderRequestAction = _orderRequestAction.NewRow();
            if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = ((DataRowView)_orderRequestBindingSource.Current)["order_request_id"].ToString();
            if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "NEW";
            if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
            if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
            if (_orderRequestAction.Columns.Contains("action_information")) newOrderRequestAction["action_information"] = DBNull.Value;
            if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
            if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
            if (_orderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "New order manually created by " + _cooperator_site_longname + " (" + _cooperator_site_shortname + "), " + _cooperator_city + ", " + _cooperator_geography + " by " + _cooperator_name + " (" + _cooperator_email + ")";
            // And add it to the Order Request Action Table...
            _orderRequestAction.Rows.Add(newOrderRequestAction);
        }

        private void bindingNavigatorSaveButton_Click(object sender, EventArgs e)
        {
            int errorCount = 0;
            errorCount = SaveOrderData();
            if (errorCount == 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Order Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "OrderWizard_bindingNavigatorSaveButtonMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();
            }
            else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\n  Error Count: {0}", "Order Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "OrderWizard_bindingNavigatorSaveButtonMessage2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }

            // Refresh the Order Request Items and Actions DGV formatting...
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
            RefreshDGVFormatting(ux_datagridviewOrderRequestAction);

            // Force the row filters to be applied to order items and order actions...
            _orderRequestBindingSource_CurrentChanged(sender, e);
        }

        private void bindingNavigatorSaveAndExitButton_Click(object sender, EventArgs e)
        {
            int errorCount = 0;
            errorCount = SaveOrderData();
            if (errorCount == 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Order Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "OrderWizard_bindingNavigatorSaveButtonMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();
                this.Close();
            }
            else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\nWould you like to review them now?\n\nClick Yes to review the errors now.\n(Click No to abandon the errors and exit the Order Wizard).\n\n  Error Count: {0}", "Order Wizard Data Save Results", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "OrderWizard_bindingNavigatorSaveButtonMessage3";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                if (DialogResult.No == ggMessageBox.ShowDialog())
                {
                    this.Close();
                }
                else
                {
                    // Update the row error message for this accession row...
                    if (string.IsNullOrEmpty(((DataRowView)_orderRequestBindingSource.Current).Row.RowError))
                    {
                        ux_textboxOrderRequestRowError.Visible = false;
                        ux_textboxOrderRequestRowError.Text = "";
                    }
                    else
                    {
                        ux_textboxOrderRequestRowError.Visible = true;
                        ux_textboxOrderRequestRowError.ReadOnly = false;
                        ux_textboxOrderRequestRowError.Enabled = true;
                        ux_textboxOrderRequestRowError.Text = ((DataRowView)_orderRequestBindingSource.Current).Row.RowError;
                    }
                }
            }
        }

        private void RefreshOrderData()
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Check to see if there is unsaved data and if some is found - ask to save before refreshing data...
            int intRowEdits = 0;

            _orderRequestBindingSource.EndEdit();
            if (_orderRequest != null && _orderRequest.GetChanges() != null) intRowEdits = _orderRequest.GetChanges().Rows.Count;
            _orderRequestItemBindingSource.EndEdit();
            if (_orderRequestItem != null && _orderRequestItem.GetChanges() != null) intRowEdits += _orderRequestItem.GetChanges().Rows.Count;
            _orderRequestActionBindingSource.EndEdit();
            if (_orderRequestAction != null && _orderRequestAction.GetChanges() != null) intRowEdits += _orderRequestAction.GetChanges().Rows.Count;
            _orderRequestAttachBindingSource.EndEdit();
            if (_orderRequestAttach != null && _orderRequestAttach.GetChanges() != null) intRowEdits += _orderRequestAttach.GetChanges().Rows.Count;
            _orderRequestPhytoLogBindingSource.EndEdit();
            if (_orderRequestPhytoLog != null && _orderRequestPhytoLog.GetChanges() != null) intRowEdits += _orderRequestPhytoLog.GetChanges().Rows.Count;
            //_webOrderRequestBindingSource.EndEdit();
            //if (_webOrderRequest.GetChanges() != null) intRowEdits += _webOrderRequest.GetChanges().Rows.Count;

            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s) that will be lost.\n\nWould you like to save them now?", "Save Edits", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "OrderWizard_ux_radiobuttonOrderFilterMessage1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = intRowEdits.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (intRowEdits > 0 && DialogResult.Yes == ggMessageBox.ShowDialog())
            {
                SaveOrderData();
            }

            Cursor.Current = Cursors.WaitCursor;

            // Calculate the Order Request Item Status filter...
            if (ux_tabcontrolMain.SelectedTab == OrdersPage ||
                ux_tabcontrolMain.SelectedTab == ActionsPage)
            {
                _orderRequestStatusFilter = " AND (";
                // Process the listbox items that are checked...
                for (int i = 0; i < ux_checkedlistboxOrderItemStatus.Items.Count; i++)
                {
                    if (ux_checkedlistboxOrderItemStatus.GetItemChecked(i))
                    {
                        // Add all listbox items that are checked...
                        _orderRequestStatusFilter += "@order_request_item.status_code='" + _orderRequestStatusCodes.Rows[i]["value_member"].ToString() + "' OR ";
                    }
                }
                // Drop the extra semicolon at the end of the string...
                if (_orderRequestStatusFilter.Length > 6)
                {
                    _orderRequestStatusFilter = _orderRequestStatusFilter.Remove(_orderRequestStatusFilter.Length - 4) + ")";
                }
                else
                {
                    _orderRequestStatusFilter = "";
                }
            }

            // Next get the order type filter...
            if (ux_comboboxOrderTypeFilter.SelectedValue != null &&
                ux_comboboxOrderTypeFilter.SelectedValue != DBNull.Value)
            {
                _orderRequestStatusFilter += " AND @order_request.order_type_code = '" + ux_comboboxOrderTypeFilter.SelectedValue.ToString().Trim() + "'";
            }

            // Now calculate the Order Request Date filter...
            if (!string.IsNullOrEmpty(ux_textboxOrderDateFilter.Text))
            {
                _orderRequestStatusFilter += " AND @order_request.ordered_date " + FormatSearchDateString(ux_textboxOrderDateFilter.Text);
                ux_textboxOrderDateFilter.Text = FormatSearchDateString(ux_textboxOrderDateFilter.Text).Replace("LIKE", "").Replace("'", "").Trim();
            }

            _orderRequestPKeys = "";
            string seQueryString = "";
            if (ux_radiobuttonMyOrders.Checked)
            {
                seQueryString = "(@order_request.owned_by=" + _sharedUtils.UserCooperatorID + " OR @inventory.owned_by=" + _sharedUtils.UserCooperatorID + ")" + _orderRequestStatusFilter;
            }
            else if (ux_radiobuttonMySitesOrders.Checked)
            {
                string siteCooperatorIDs = "";
                DataTable dt = _sharedUtils.GetLocalData("SELECT * FROM cooperator_lookup WHERE account_is_enabled = @accountisenabled AND site = @site", "@accountisenabled=Y; @site=" + _sharedUtils.UserSite);
                foreach (DataRow dr in dt.Rows)
                {
                    siteCooperatorIDs += dr["value_member"].ToString() + ",";
                }
                siteCooperatorIDs = siteCooperatorIDs.TrimEnd(',');
                if (string.IsNullOrEmpty(siteCooperatorIDs)) siteCooperatorIDs = "0";
                seQueryString = "(@order_request.owned_by IN (" + siteCooperatorIDs + ") OR @inventory.owned_by IN (" + siteCooperatorIDs + "))" + _orderRequestStatusFilter;
            }
            else if (ux_radiobuttonAllSitesOrders.Checked)
            {
                string allCooperatorIDs = "";
                DataTable dt = _sharedUtils.GetLocalData("SELECT * FROM cooperator_lookup WHERE account_is_enabled = @accountisenabled", "@accountisenabled=Y;");
                foreach (DataRow dr in dt.Rows)
                {
                    allCooperatorIDs += dr["value_member"].ToString() + ",";
                }
                allCooperatorIDs = allCooperatorIDs.TrimEnd(',');
                if (string.IsNullOrEmpty(allCooperatorIDs)) allCooperatorIDs = "0";
                seQueryString = "(@order_request.owned_by IN (" + allCooperatorIDs + ") OR @inventory.owned_by IN (" + allCooperatorIDs + "))" + _orderRequestStatusFilter;
            }
            else // ux_radiobuttonSelectionOrders.Checked must be true...
            {
                _selectionList = ScrubOrderList(ux_textboxSelection.Text);
                if (string.IsNullOrEmpty(_selectionList)) _selectionList = "0";

                if (ux_checkboxIsOrderNumber.Checked)
                {
                    seQueryString = "@order_request.order_request_id IN (" + _selectionList + ") OR ";
                }

                if (ux_checkboxIsWebOrderNumber.Checked)
                {
                    seQueryString += "@order_request.web_order_request_id IN (" + _selectionList + ") OR @order_request_action.note LIKE '%(#" + _selectionList + ")%' OR ";
                }

                if (ux_checkboxIsLocalNumber.Checked)
                {
                    string quotedStringList = "";
                    string[] pkeys = _selectionList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string pkey in pkeys)
                    {
                        quotedStringList += "'" + pkey.Trim().Replace("\"", "").Replace("'", "") + "'" + ",";
                    }
                    quotedStringList = quotedStringList.TrimEnd(',');
                    seQueryString += "@order_request.local_number IN (" + quotedStringList + ") OR ";
                }

                seQueryString = seQueryString.TrimEnd(new char[] { ' ', 'O', 'R' });
            }

            // Use the Search Engine to find the order_request records...
            DataSet ds = _sharedUtils.SearchWebService(seQueryString, true, true, null, "order_request", 0, 1000);
            // Build the parameter string to pass to the get_order_request, order_wizard_get_order_request_item, and get_order_request_action dataview...
            if (ds.Tables.Contains("SearchResult"))
            {
                // Build a list of order_request_ids to use for gathering the order_request_items...
                _orderRequestPKeys = ":orderrequestid=";
                foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                {
                    _orderRequestPKeys += dr["ID"].ToString() + ",";
                }
                _orderRequestPKeys = _orderRequestPKeys.TrimEnd(',');
            }

            // Get the order_request_item table and bind it to the main form on the General tabpage...
            ds = _sharedUtils.GetWebServiceData("order_wizard_get_order_request_item", _orderRequestPKeys, 0, 0);
            if (ds.Tables.Contains("order_wizard_get_order_request_item"))
            {
                // Load the order request items from the server DB...
                _orderRequestItem = ds.Tables["order_wizard_get_order_request_item"].Copy();
                // Load as a Read Only dataview to ensure all LU dictionaries have required LUs (this really 
                // speeds up the Edit dataview loading as well as the programmatic populating of sortable fields loaded next)...
                _sharedUtils.BuildReadOnlyDataGridView(new DataGridView(), _orderRequestItem);
                // Now that the LU dictionaries are pre-loaded - build the sortable fields and populate them...
                foreach (DataColumn dc in ds.Tables["order_wizard_get_order_request_item"].Columns)
                {
                    _orderRequestItem.Columns.Add(dc.ColumnName + "_sortable", typeof(string));
                    // Populate the new column...
                    foreach (DataRow dr in _orderRequestItem.Rows)
                    {
                        if (dr[dc.ColumnName] != DBNull.Value)
                        {
                            dr[dc.ColumnName + "_sortable"] = GetSortableColumnText(dr, dc.ColumnName);
                        }
                    }
                }
            }
            else
            {
                _orderRequestItem = new DataTable();
            }
            // Accept all the changes to the bound datatable (should only be the new sortable columns just added)...
            _orderRequestItemBindingSource.EndEdit();
            _orderRequestItem.AcceptChanges();
            ux_datagridviewOrderRequestItem.DataSource = _orderRequestItemBindingSource;
            // Now that the LU dictionaries are primed load the Edit dataview
            _sharedUtils.BuildEditDataGridView(ux_datagridviewOrderRequestItem, _orderRequestItem);
            // Set the default sorting order for the table...
            _orderRequestItem.DefaultView.Sort = "sequence_number ASC";
            // Set Sortmode for the columns...
            foreach (DataGridViewColumn dgvc in ux_datagridviewOrderRequestItem.Columns)
            {
                dgvc.SortMode = DataGridViewColumnSortMode.Programmatic;
                dgvc.HeaderCell.ContextMenuStrip = ux_contextmenustripDGVHeader;
                if (dgvc.Name.EndsWith("_sortable")) dgvc.Visible = false;
            }

            // Get the order_request_action table and bind it to the actions dgv...
            ds = _sharedUtils.GetWebServiceData("order_wizard_get_order_request_action", _orderRequestPKeys, 0, 0);
            if (ds.Tables.Contains("order_wizard_get_order_request_action"))
            {
                _orderRequestAction = ds.Tables["order_wizard_get_order_request_action"].Copy();
            }
            else
            {
                _orderRequestAction = new DataTable();
            }
            ux_datagridviewOrderRequestAction.DataSource = _orderRequestActionBindingSource;
            _sharedUtils.BuildEditDataGridView(ux_datagridviewOrderRequestAction, _orderRequestAction);
            // Set the default sorting order for the table...
            _orderRequestAction.DefaultView.Sort = "started_date ASC";
            // Set Sortmode for the columns...
            foreach (DataGridViewColumn dgvc in ux_datagridviewOrderRequestAction.Columns)
            {
                dgvc.SortMode = DataGridViewColumnSortMode.Automatic;
            }

            // Get the order_request_attach table and bind it to the attachment dgv...
            ds = _sharedUtils.GetWebServiceData("order_wizard_get_order_request_attach", _orderRequestPKeys, 0, 0);
            if (ds.Tables.Contains("order_wizard_get_order_request_attach"))
            {
                _orderRequestAttach = ds.Tables["order_wizard_get_order_request_attach"].Copy();
                _orderRequestAttach.Columns.Add("local_path", typeof(string));
                if (_orderRequestAttach.Columns.Contains("order_request_id") && _orderRequestAttach.Columns.Contains("virtual_path") && _orderRequestAttach.Columns.Contains("local_path"))
                {
                    foreach (DataRow dr in _orderRequestAttach.Rows)
                    {
                        string attachVirtualPath = dr["virtual_path"].ToString();
                        string fileName = System.IO.Path.GetFileName(attachVirtualPath);
                        string tempFilesPath = System.IO.Path.GetTempPath();
                        string attachLocalPath = tempFilesPath + "GRIN-Global CuratorTool\\Attachments\\Orders\\" + dr["order_request_id"].ToString() + "\\" + fileName;
                        dr["local_path"] = attachLocalPath;
                    }
                }
            }
            else
            {
                _orderRequestAttach = new DataTable();
                _orderRequestAttach.Columns.Add("local_path", typeof(string));
            }
            _orderRequestAttach.AcceptChanges();
            ux_datagridviewOrderRequestAttach.DataSource = _orderRequestAttachBindingSource;
            _sharedUtils.BuildEditDataGridView(ux_datagridviewOrderRequestAttach, _orderRequestAttach);
            // Hide the local_path column from view...
            if (ux_datagridviewOrderRequestAttach.Columns.Contains("local_path")) ux_datagridviewOrderRequestAttach.Columns["local_path"].Visible = false;
            // Set the default sorting order for the table...
            _orderRequestAttach.DefaultView.Sort = "attach_date ASC";
            // Set Sortmode for the columns...
            foreach (DataGridViewColumn dgvc in ux_datagridviewOrderRequestAttach.Columns)
            {
                dgvc.SortMode = DataGridViewColumnSortMode.Automatic;
            }

            // Get the order_request_phyto_log table and bind it to the phyto log dgv...
            ds = _sharedUtils.GetWebServiceData("order_wizard_get_order_request_phyto_log", _orderRequestPKeys, 0, 0);
            if (ds.Tables.Contains("order_wizard_get_order_request_phyto_log"))
            {
                _orderRequestPhytoLog = ds.Tables["order_wizard_get_order_request_phyto_log"].Copy();
            }
            else
            {
                _orderRequestPhytoLog = new DataTable();
                _orderRequestPhytoLog.Columns.Add("received_date", typeof(string));
            }
            ux_datagridviewOrderRequestPhytoLog.DataSource = _orderRequestPhytoLogBindingSource;
            _sharedUtils.BuildEditDataGridView(ux_datagridviewOrderRequestPhytoLog, _orderRequestPhytoLog);
            // Set the default sorting order for the table...
            _orderRequestPhytoLog.DefaultView.Sort = "received_date ASC";
            // Set Sortmode for the columns...
            foreach (DataGridViewColumn dgvc in ux_datagridviewOrderRequestPhytoLog.Columns)
            {
                dgvc.SortMode = DataGridViewColumnSortMode.Automatic;
            }

            // Get the order_request table and bind it to the main form on the General tabpage...
            // (do this one last so that the child tables get auto-filtered to match the current row in this table)
            ds = _sharedUtils.GetWebServiceData("order_wizard_get_order_request", _orderRequestPKeys, 0, 0);
            if (ds.Tables.Contains("order_wizard_get_order_request"))
            {
                _orderRequest = ds.Tables["order_wizard_get_order_request"].Copy();
            }
            else
            {
                _orderRequest = new DataTable();
            }
            _orderRequestBindingSource.DataSource = _orderRequest;

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void RefreshWebOrderData()
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Calculate the Order Request Item Status filter...
            if (ux_checkedlistboxWebOrderItemStatus.Items.Count > 0)
            {
                _webOrderRequestStatusFilter = "";
                string webOrderRequestStatusFilter = "(";
                string webOrderRequestItemStatusFilter = "(";
                // Process the listbox item that is changing by peeking at its 'new value'...
                for (int i = 0; i < ux_checkedlistboxWebOrderItemStatus.Items.Count; i++)
                {
                    if (ux_checkedlistboxWebOrderItemStatus.GetItemChecked(i))
                    {
                        // Add all listbox items that are checked to the Order Status Filter if the group_name='WEB_ORDER_REQUEST_STATUS'...
                        if (_webOrderRequestStatusCodes.Rows[i]["group_name"].ToString().ToUpper().Trim() == "WEB_ORDER_REQUEST_STATUS")
                        {
                            webOrderRequestStatusFilter += "@web_order_request.status_code='" + _webOrderRequestStatusCodes.Rows[i]["value_member"].ToString() + "' OR ";
                        }
                        // Add all listbox items that are checked to the Order Status Item Filter if the group_name='WEB_ORDER_REQUEST_ITEM_STATUS'...
                        if (_webOrderRequestStatusCodes.Rows[i]["group_name"].ToString().ToUpper().Trim() == "WEB_ORDER_REQUEST_ITEM_STATUS")
                        {
                            webOrderRequestItemStatusFilter += "@web_order_request_item.status_code='" + _webOrderRequestStatusCodes.Rows[i]["value_member"].ToString() + "' OR ";
                        }
                    }
                }
                // Drop the extra semicolon at the end of the Order Status Filter string...
                if (webOrderRequestStatusFilter.Length > 2)
                {
                    webOrderRequestStatusFilter = webOrderRequestStatusFilter.Remove(webOrderRequestStatusFilter.Length - 4) + ")";
                }
                else
                {
                    webOrderRequestStatusFilter = "";
                }
                // Drop the extra semicolon at the end of the Order Status Item Filter string...
                if (webOrderRequestItemStatusFilter.Length > 2)
                {
                    webOrderRequestItemStatusFilter = webOrderRequestItemStatusFilter.Remove(webOrderRequestItemStatusFilter.Length - 4) + ")";
                }
                else
                {
                    webOrderRequestItemStatusFilter = "";
                }

                // Build the filter text to use both order and order item filters...
                if (!string.IsNullOrEmpty(webOrderRequestStatusFilter) && !string.IsNullOrEmpty(webOrderRequestItemStatusFilter))
                {
                    _webOrderRequestStatusFilter = " AND (" + webOrderRequestStatusFilter + " OR " + webOrderRequestItemStatusFilter + ")";
                }
                else
                {
                    _webOrderRequestStatusFilter = " AND (" + webOrderRequestStatusFilter + webOrderRequestItemStatusFilter + ")";
                }
            }

            // Now calculate the Order Request Date filter...
            if (!string.IsNullOrEmpty(ux_textboxWebOrderDateFilter.Text))
            {
                _webOrderRequestStatusFilter += " AND @web_order_request.ordered_date " + FormatSearchDateString(ux_textboxWebOrderDateFilter.Text);
            }

            string webOrderRequestPKeys = "";
            string seQueryString = "";
            if (ux_radiobuttonMyWebOrders.Checked)
            {
                seQueryString = "@accession.owned_by=" + _sharedUtils.UserCooperatorID + _webOrderRequestStatusFilter;
            }
            else if (ux_radiobuttonMySitesWebOrders.Checked)
            {
                string siteCooperatorIDs = "";
                DataTable dt = _sharedUtils.GetLocalData("SELECT * FROM cooperator_lookup WHERE account_is_enabled = @accountisenabled AND site = @site", "@accountisenabled=Y; @site=" + _sharedUtils.UserSite);
                foreach (DataRow dr in dt.Rows)
                {
                    siteCooperatorIDs += dr["value_member"].ToString() + ",";
                }
                siteCooperatorIDs = siteCooperatorIDs.TrimEnd(',');
                if (string.IsNullOrEmpty(siteCooperatorIDs)) siteCooperatorIDs = "null";
                seQueryString = "@accession.owned_by IN (" + siteCooperatorIDs + ")" + _webOrderRequestStatusFilter;
            }
            else if (ux_radiobuttonAllSitesWebOrders.Checked)
            {
                string allCooperatorIDs = "";
                DataTable dt = _sharedUtils.GetLocalData("SELECT * FROM cooperator_lookup WHERE account_is_enabled = @accountisenabled", "@accountisenabled=Y;");
                foreach (DataRow dr in dt.Rows)
                {
                    allCooperatorIDs += dr["value_member"].ToString() + ",";
                }
                allCooperatorIDs = allCooperatorIDs.TrimEnd(',');
                if (string.IsNullOrEmpty(allCooperatorIDs)) allCooperatorIDs = "null";
                seQueryString = "@accession.owned_by IN (" + allCooperatorIDs + ")" + _webOrderRequestStatusFilter;
            }
            else
            {
                string webSelectionList = ScrubOrderList(ux_textboxWebSelection.Text);
                seQueryString = "@web_order_request.web_order_request_id IN (" + webSelectionList + ")";
            }

            // Use the Search Engine to find the order_request records...
            DataSet ds = _sharedUtils.SearchWebService(seQueryString, true, true, null, "web_order_request", 0, 0);
            // Build the parameter string to pass to the get_order_request, order_wizard_get_order_request_item, and get_order_request_action dataview...
            if (ds.Tables.Contains("SearchResult"))
            {
                // Build a list of order_request_ids to use for gathering the order_request_items...
                webOrderRequestPKeys = ":weborderrequestid=";
                foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                {
                    webOrderRequestPKeys += dr["ID"].ToString() + ",";
                }
                webOrderRequestPKeys = webOrderRequestPKeys.TrimEnd(',');
            }

            // Get the web_order_request table and bind it to the main form on the OrdersPage tabpage...
            ds = _sharedUtils.GetWebServiceData("order_wizard_get_web_order_request", webOrderRequestPKeys, 0, 0);
            if (ds.Tables.Contains("order_wizard_get_web_order_request"))
            {
                ds.Tables["order_wizard_get_web_order_request"].DefaultView.Sort = "web_order_request_id ASC";
                _webOrderRequest = ds.Tables["order_wizard_get_web_order_request"];
            }
            else
            {
                _webOrderRequest = new DataTable();
            }

            // Get the web_order_request_item table and bind it to the dgv on the WebOrdersPage tabpage...
            ds = _sharedUtils.GetWebServiceData("order_wizard_get_web_order_request_item", webOrderRequestPKeys, 0, 0);
            if (ds.Tables.Contains("order_wizard_get_web_order_request_item"))
            {
                _webOrderRequestItem = ds.Tables["order_wizard_get_web_order_request_item"].Copy();
                _webOrderRequestItem.DefaultView.Sort = "web_order_request_id ASC, sequence_number ASC";
                ux_datagridviewWebOrderRequestItem.DataSource = _webOrderRequestItemBindingSource;
                _sharedUtils.BuildReadOnlyDataGridView(ux_datagridviewWebOrderRequestItem, _webOrderRequestItem);
            }
            else
            {
                _webOrderRequestItem = new DataTable();
            }

            // Get the web_order_request_action table (used for harvesting SMTA actions for the orders)...
            ds = _sharedUtils.GetWebServiceData("order_wizard_get_web_order_request_action", webOrderRequestPKeys, 0, 0);
            if (ds.Tables.Contains("order_wizard_get_web_order_request_action"))
            {
                _webOrderRequestAction = ds.Tables["order_wizard_get_web_order_request_action"].DefaultView.ToTable();
                _webOrderRequestAction.DefaultView.Sort = "web_order_request_id ASC, acted_date ASC";
            }
            else
            {
                _webOrderRequestAction = new DataTable();
            }

            // Get the web_order_request_attach table (requestor may have uploaded permits or other images pertaining to this order)...
            ds = _sharedUtils.GetWebServiceData("order_wizard_get_web_order_request_attach", webOrderRequestPKeys, 0, 0);
            if (ds.Tables.Contains("order_wizard_get_web_order_request_attach"))
            {
                _webOrderRequestAttach = ds.Tables["order_wizard_get_web_order_request_attach"].DefaultView.ToTable();
                _webOrderRequestAttach.DefaultView.Sort = "web_order_request_id ASC, web_order_request_attach_id ASC";
            }
            else
            {
                _webOrderRequestAttach = new DataTable();
            }

            // Re-bind the bindingsource to the new table of web orders...
            // NOTE: this will force a change in the child table filter and this should not happen
            //       until all of the child table DGV have been built, so this should be done last...
            _webOrderRequestBindingSource.DataSource = _webOrderRequest;

            // Enable (or disable) the buttons for creating/deleting orders and cooperators...
            if (_webOrderRequest != null &&
                _webOrderRequest.Rows.Count > 0)
            {
                ux_buttonCreateOrderRequest.Enabled = true;
                ux_checkboxMySitesAccessionsOnly.Enabled = true;
                ux_buttonCreateCooperator.Enabled = true;
                ux_buttonCancelWebOrderRequest.Enabled = true;
                bindingNavigatorWebAddNewItem.Enabled = false;
                bindingNavigatorWebDeleteItem.Enabled = false;
            }
            else
            {
                ux_buttonCreateOrderRequest.Enabled = false;
                ux_checkboxMySitesAccessionsOnly.Enabled = false;
                ux_buttonCreateCooperator.Enabled = false;
                ux_buttonCancelWebOrderRequest.Enabled = false;
                bindingNavigatorWebAddNewItem.Enabled = false;
                bindingNavigatorWebDeleteItem.Enabled = false;
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private string ScrubOrderList(string selectionList)
        {
            string returnList = "";
            string[] pkeys = selectionList.Split(new char[] { ',', ' ', '\t', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string pkey in pkeys)
            {
                returnList += pkey + ",";
            }
            returnList = returnList.TrimEnd(',');

            return returnList;
        }

        private string FormatSearchDateString(string rawDateTextString)
        {
            string formattedSearchDateString = "";
            string scrubbedDateText = rawDateTextString.Replace("/", "-").Replace("*", "%").Replace("'", "");
            // Check to see if user prefers month/day/year or day/month/year...
            bool monthFirst = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToLower().StartsWith("m");
            string[] dateTokens = scrubbedDateText.Replace("<", "").Replace(">", "").Split(new char[] { '-' });
            string day = "";
            string month = "";
            string year = "";
            int intDay;
            int intMonth;
            int intYear;
            switch (dateTokens.Length)
            {
                case 3:
                    // Assume format is dd-mm-yyyy...
                    int intDayTokenIndex = 1;
                    int intMonthTokenIndex = 0;
                    int intYearTokenIndex = 2;
                    // Check to see if user specified date as the sortable date format of yyyy-mm-dd...
                    if (int.TryParse(dateTokens[0].Replace("%", ""), out intYear))
                    {
                        if (intYear > 31)  // There are no days or months greater than 31 so this must be the year
                        {
                            intYearTokenIndex = 0;
                            intMonthTokenIndex = 1;
                            intDayTokenIndex = 2;
                        }
                        // Otherwise assume user specified date in localized format (typically dd-mm-yyyy for International or mm-dd-yyyy for USA)
                        else if (monthFirst && intYear < 13)  // There is no month greater than 12
                        {
                            intDayTokenIndex = 1;
                            intMonthTokenIndex = 0;
                        }
                        else
                        {
                            intDayTokenIndex = 0;
                            intMonthTokenIndex = 1;
                        }
                    }
                    // Parse out the day token...
                    if (int.TryParse(dateTokens[intDayTokenIndex], out intDay))
                    {
                        day = intDay.ToString("00");
                    }
                    else
                    {
                        day = dateTokens[intDayTokenIndex];
                    }
                    // Parse out the month token...
                    if (int.TryParse(dateTokens[intMonthTokenIndex], out intMonth))
                    {
                        month = intMonth.ToString("00");
                    }
                    else
                    {
                        month = dateTokens[intMonthTokenIndex];
                    }
                    // Parse out the year token...
                    if (int.TryParse(dateTokens[intYearTokenIndex], out intYear))
                    {
                        // if only two digits were specified take your best guess at 1900s or 2000s date...
                        if (intYear < 50) // years 00-49 are assumed to be in 2000s
                        {
                            intYear += 2000;
                        }
                        else if (intYear < 100) // years 50-99 are assumed to be in year 1900s
                        {
                            intYear += 1900;
                        }
                        year = intYear.ToString("0000");
                    }
                    else
                    {
                        year = dateTokens[intYearTokenIndex];
                    }
                    break;
                case 2:
                    // Parse out the month...
                    // Check to see if first token can be interpreted as a month and if so then
                    // assume the two tokens have this format: mm/yyyy and process...
                    // Assume format is dd-mm-yyyy...
                    if (int.TryParse(dateTokens[0], out intMonth) &&
                        intMonth > 12)
                    {
                        intMonthTokenIndex = 1;
                        intYearTokenIndex = 0;
                    }
                    else
                    {
                        intMonthTokenIndex = 0;
                        intYearTokenIndex = 1;
                    }
                    if (int.TryParse(dateTokens[intMonthTokenIndex], out intMonth))
                    {
                        month = intMonth.ToString("00");
                    }
                    else
                    {
                        month = dateTokens[intMonthTokenIndex];
                    }
                    // Parse out the year token...
                    if (int.TryParse(dateTokens[intYearTokenIndex], out intYear))
                    {
                        // if only two digits were specified take your best guess at 1900s or 2000s date...
                        if (intYear < 50) // years 00-49 are assumed to be in 2000s
                        {
                            intYear += 2000;
                        }
                        else if (intYear < 100) // years 50-99 are assumed to be in year 1900s
                        {
                            intYear += 1900;
                        }
                        year = intYear.ToString("0000");
                    }
                    break;
                case 1:
                    // Only one token so assume year only specified...
                    if (int.TryParse(dateTokens[0], out intYear))
                    {
                        // if only two digits were specified take your best guess at 1900s or 2000s date...
                        if (intYear < 50) // years 00-49 are assumed to be in 2000s
                        {
                            intYear += 2000;
                        }
                        else if (intYear < 100) // years 50-99 are assumed to be in year 1900s
                        {
                            intYear += 1900;
                        }
                        year = intYear.ToString("0000");
                    }
                    else
                    {
                        year = dateTokens[0];
                    }
                    break;
                default:
                    break;
            }

            if (scrubbedDateText.Contains("%"))
            {
                if (string.IsNullOrEmpty(day)) day = "%";
                if (string.IsNullOrEmpty(month)) month = "%";
                formattedSearchDateString += " LIKE '" + year + "-" + month + "-" + day + "%'";
            }
            else if (scrubbedDateText.Trim().StartsWith("<"))
            {
                if (string.IsNullOrEmpty(day)) day = "01";
                if (string.IsNullOrEmpty(month)) month = "01";
                formattedSearchDateString += " <'" + year + "-" + month + "-" + day + "'";
            }
            else if (scrubbedDateText.Trim().StartsWith(">"))
            {
                if (string.IsNullOrEmpty(day)) day = "01";
                if (string.IsNullOrEmpty(month)) month = "01";
                formattedSearchDateString += " >'" + year + "-" + month + "-" + day + "'";
            }
            else
            {
                if (string.IsNullOrEmpty(day)) day = "%";
                if (string.IsNullOrEmpty(month)) month = "%";
                formattedSearchDateString += " LIKE '" + year + "-" + month + "-" + day + "%'";
            }

            return formattedSearchDateString.Replace("%%", "%");
        }

        private int SaveOrderData()
        {
            int errorCount = 0;
            DataSet orderRequestChanges = new DataSet();
            DataSet orderRequestSaveResults = new DataSet();
            DataSet orderRequestItemChanges = new DataSet();
            DataSet orderRequestItemSaveResults = new DataSet();
            DataSet orderRequestActionChanges = new DataSet();
            DataSet orderRequestActionSaveResults = new DataSet();
            DataSet orderRequestAttachChanges = new DataSet();
            DataSet orderRequestAttachSaveResults = new DataSet();
            DataSet orderRequestPhytoLogChanges = new DataSet();
            DataSet orderRequestPhytoLogSaveResults = new DataSet();
            DataSet webOrderRequestChanges = new DataSet();
            DataSet webOrderRequestSaveResults = new DataSet();
            DataSet inventoryOrderRequestItemChanges = new DataSet();

            // Process Order Requests...
            // Make sure the last edited row in the Order Header Form has been commited to the datatable...
            _orderRequestBindingSource.EndEdit();
            // Make sure no Order Item DGV cells are being edited...
            ux_datagridviewOrderRequestItem.EndEdit();
            // Make sure the last edited row in the Order Item DGV has been commited to the datatable...
            _orderRequestItemBindingSource.EndEdit();
            // Make sure no Order Action DGV cells are being edited...
            ux_datagridviewOrderRequestAction.EndEdit();
            // Make sure the last edited row in the Order Action DGV has been commited to the datatable...
            _orderRequestActionBindingSource.EndEdit();
            // Make sure no Order Attach DGV cells are being edited...
            ux_datagridviewOrderRequestAttach.EndEdit();
            // Make sure the last edited row in the Order Attach DGV has been commited to the datatable...
            _orderRequestAttachBindingSource.EndEdit();
            // Make sure the last edited row in the Order Phyto Log DGV has been commited to the datatable...
            _orderRequestPhytoLogBindingSource.EndEdit();

            // Make sure the navigator is not still editing a cell...
            foreach (DataRowView drv in _orderRequestBindingSource.List)
            {
                if (drv.IsEdit ||
                    drv.Row.RowState == DataRowState.Added ||
                    drv.Row.RowState == DataRowState.Deleted ||
                    drv.Row.RowState == DataRowState.Detached ||
                    drv.Row.RowState == DataRowState.Modified)
                {

                    drv.EndEdit();
                    //drv.Row.ClearErrors();
                }
            }

            // Get the changes (if any) for the order_request table and commit them to the remote database...
            if (_orderRequest != null && _orderRequest.GetChanges() != null)
            {
                orderRequestChanges.Tables.Add(_orderRequest.GetChanges());
                // Save the changes to the remote server...
                orderRequestSaveResults = _sharedUtils.SaveWebServiceData(orderRequestChanges);
                if (orderRequestSaveResults.Tables.Contains(_orderRequest.TableName))
                {
                    errorCount += SyncSavedResults(_orderRequest, orderRequestSaveResults.Tables[_orderRequest.TableName]);
                }
            }

            // Process Order Request Items...
            //
            // Get the changes (if any) for the order_request_item table and commit them to the remote database...
            if (_orderRequestItem != null && _orderRequestItem.GetChanges() != null)
            {
                // Before saving the results to the remote server check to see if any new rows in order_request_item have
                // a FK related to a new row in the order_request table (pkey < 0).  If so get the new pkey returned from
                // the get_order_reqeust save and update the records in order_request_item...
                DataRow[] orderRequestItemRowsWithNewParent = _orderRequestItem.Select("order_request_id<0");
                foreach (DataRow dr in orderRequestItemRowsWithNewParent)
                {
                    // "OriginalPrimaryKeyID" "NewPrimaryKeyID"
                    DataRow[] newParent = orderRequestSaveResults.Tables["order_wizard_get_order_request"].Select("OriginalPrimaryKeyID=" + dr["order_request_id"].ToString());
                    if (newParent != null && newParent.Length > 0)
                    {
                        dr["order_request_id"] = newParent[0]["NewPrimaryKeyID"];
                    }
                }

                DataTable dtItemChanges = _orderRequestItem.GetChanges();
                // Drop the columns used for sorting...
                foreach (DataColumn dc in _orderRequestItem.Clone().Columns)
                {
                    if (dc.ColumnName.EndsWith("_sortable") &&
                        dtItemChanges.Columns.Contains(dc.ColumnName))
                    {
                        dtItemChanges.Columns.Remove(dc.ColumnName);
                    }
                }
                orderRequestItemChanges.Tables.Add(dtItemChanges);
                ScrubData(orderRequestItemChanges);

                // Make a copy of the changed Order Request Items (for processing auto-deduct further down)...
                inventoryOrderRequestItemChanges = orderRequestItemChanges.Copy();

                // For each Order Request Item that has a changed status_code - update the status_date field...
                Dictionary<string, int> statusChangeCounts = new Dictionary<string, int>();
                foreach (DataRow dr in orderRequestItemChanges.Tables[_orderRequestItem.TableName].Rows)
                {
                    // For any status changes - set the status date...
                    if (dr.Table.Columns.Contains("status_code") &&
                        (dr.RowState == DataRowState.Modified && dr["status_code", DataRowVersion.Original].ToString().Trim() != dr["status_code", DataRowVersion.Current].ToString().Trim()))
                    {
                        string orderStatusCode = dr["order_request_id", DataRowVersion.Current].ToString() + "|" + dr["status_code", DataRowVersion.Current].ToString();
                        if (statusChangeCounts.ContainsKey(orderStatusCode))
                        {
                            statusChangeCounts[orderStatusCode] = statusChangeCounts[orderStatusCode] + 1;
                        }
                        else
                        {
                            statusChangeCounts.Add(orderStatusCode, 1);
                        }
                    }
                }

                foreach (string orderStatusCode in statusChangeCounts.Keys)
                {
                    // Create an Action for the status_code changes (one per staus_code with a count for how many items were changed to that status)...
                    DataRow newOrderRequestAction = _orderRequestAction.NewRow();
                    string currentOrderRequestPKey = orderStatusCode.Split(new char[] { '|' })[0];
                    string currentOrderRequestAction = orderStatusCode.Split(new char[] { '|' })[1];
                    if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = currentOrderRequestPKey;
                    if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = currentOrderRequestAction;
                    if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("action_information")) newOrderRequestAction["action_information"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
                    if (_orderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "Order request item status changed to " + currentOrderRequestAction + " for " + statusChangeCounts[orderStatusCode].ToString() + " item(s) by " + _cooperator_site_longname + " (" + _cooperator_site_shortname + "), " + _cooperator_city + ", " + _cooperator_geography + " by " + _cooperator_name + " (" + _cooperator_email + ")";
                    // And add it to the Order Request Action Table...
                    _orderRequestAction.Rows.Add(newOrderRequestAction);
                }

                // Save the order request item changes to the remote server...
                orderRequestItemSaveResults = _sharedUtils.SaveWebServiceData(orderRequestItemChanges);
                // Sync the saved results with the original table (to give user feedback about results of save)...
                if (orderRequestItemSaveResults.Tables.Contains(_orderRequestItem.TableName))
                {
                    errorCount += SyncSavedResults(_orderRequestItem, orderRequestItemSaveResults.Tables[_orderRequestItem.TableName]);
                }
            }

            // Check to see if any Order Request Item rows status_code column has changed to 'SHIPPED'
            // and if so auto-deduct the inventory (when auto_deduct is set in the inventory table)...
            if (orderRequestItemChanges.Tables.Contains(_orderRequestItem.TableName) &&
                orderRequestItemSaveResults.Tables.Contains(_orderRequestItem.TableName))
            //if (inventoryOrderRequestItemChanges.Tables.Contains(_orderRequestItem.TableName))
            {
                List<int> autoDeductList = new List<int>();
                foreach (DataRow dr in orderRequestItemChanges.Tables[_orderRequestItem.TableName].Rows)
                //foreach (DataRow dr in inventoryOrderRequestItemChanges.Tables[_orderRequestItem.TableName].Rows)
                {
                    // Pull the results from the save re
                    // For status changes of 'SHIPPED' - auto deduct the inventory amount...
                    if (dr.RowState != DataRowState.Deleted)
                    {
                        int origPKey = (int)dr[dr.Table.PrimaryKey[0].ColumnName];
                        DataRow saveResultsDR = orderRequestItemSaveResults.Tables[_orderRequestItem.TableName].Rows.Find(origPKey);
                        int newPKey = origPKey;
                        string savedStatus = "";
                        if (saveResultsDR != null)
                        {
                            newPKey = (int)saveResultsDR["NewPrimaryKeyID"];
                            savedStatus = saveResultsDR["SavedStatus"].ToString().Trim().ToUpper();
                        }
                        string originalStatus = "";
                        if (dr.RowState == DataRowState.Modified)
                        {
                            originalStatus = dr["status_code", DataRowVersion.Original].ToString().Trim().ToUpper();
                        }
                        string currentStatus = dr["status_code", DataRowVersion.Current].ToString().Trim().ToUpper();
                        if (savedStatus == "SUCCESS" &&
                            dr.RowState != DataRowState.Deleted &&
                            dr.Table.Columns.Contains("status_code") &&
                            dr.Table.Columns.Contains("inventory_id") &&
                            ((currentStatus == "INSPECT" && dr.RowState == DataRowState.Added) ||
                             (currentStatus == "INSPECT" && dr.RowState == DataRowState.Modified && originalStatus != currentStatus && originalStatus != "SHIPPED") ||
                             (currentStatus == "SHIPPED" && dr.RowState == DataRowState.Added) ||
                             (currentStatus == "SHIPPED" && dr.RowState == DataRowState.Modified && originalStatus != currentStatus && originalStatus != "INSPECT")
                            ))
                        {
                            // Save the pkey for the order request item to a list
                            autoDeductList.Add(newPKey);
                        }
                    }
                }

                // Build the inventory pkey list string for the web service...
                string inventoryAutoDeductString = "";
                foreach (int orderRequestAutoDeductPkey in autoDeductList)
                {
                    // Find the order_request_item row (assumes the record status is modified - NOT new)...
                    DataRow orderRequestItemRow = orderRequestItemSaveResults.Tables[_orderRequestItem.TableName].Rows.Find(orderRequestAutoDeductPkey);
                    // If a record was not found search the 'NewPrimaryKeyID' field for a match (assumes the record status is new)... 
                    if(orderRequestItemRow == null)
                    {
                        DataRow[] drs = orderRequestItemSaveResults.Tables[_orderRequestItem.TableName].Select("NewPrimaryKeyID = " + orderRequestAutoDeductPkey);
                        if (drs.Length > 0) orderRequestItemRow = drs[0];
                    }
                    if (orderRequestItemRow != null) inventoryAutoDeductString += orderRequestItemRow["inventory_id"].ToString() + ",";
                }
                inventoryAutoDeductString = inventoryAutoDeductString.TrimEnd(new char[] { ',' });

                // Go get the inventory records...
                DataSet orderRequestItemInventoryRows = _sharedUtils.GetWebServiceData("get_inventory", ":inventoryid=" + inventoryAutoDeductString, 0, 0);
                if (orderRequestItemInventoryRows != null &&
                    orderRequestItemInventoryRows.Tables.Contains("get_inventory") &&
                    orderRequestItemInventoryRows.Tables["get_inventory"].Rows.Count > 0)
                {
                    foreach (int orderRequestAutoDeductPkey in autoDeductList)
                    {
                        // Find the order_request_item row (assumes the record status is modified - NOT new)...
                        DataRow oriDataRow = orderRequestItemSaveResults.Tables[_orderRequestItem.TableName].Rows.Find(orderRequestAutoDeductPkey);
                        // If a record was not found search the 'NewPrimaryKeyID' field for a match (assumes the record status is new)... 
                        if (oriDataRow == null)
                        {
                            DataRow[] drs = orderRequestItemSaveResults.Tables[_orderRequestItem.TableName].Select("NewPrimaryKeyID = " + orderRequestAutoDeductPkey);
                            if (drs.Length > 0) oriDataRow = drs[0];
                        }
                        DataRow invDataRow = orderRequestItemInventoryRows.Tables["get_inventory"].Rows.Find(oriDataRow["inventory_id"]);
                        if (oriDataRow != null &&
                            invDataRow != null &&
                            invDataRow.Table.Columns.Contains("quantity_on_hand") &&
                            invDataRow.Table.Columns.Contains("quantity_on_hand_unit_code") &&
                            oriDataRow.Table.Columns.Contains("quantity_shipped") &&
                            oriDataRow.Table.Columns.Contains("quantity_shipped_unit_code") &&
                            invDataRow["quantity_on_hand_unit_code"].ToString().Trim().ToUpper() == oriDataRow["quantity_shipped_unit_code"].ToString().Trim().ToUpper() &&
                            invDataRow.Table.Columns.Contains("is_auto_deducted") &&
                            invDataRow["is_auto_deducted"].ToString().Trim().ToUpper() == "Y")
                        {
                            // Deduct the inventory amount if distribution units match...
                            invDataRow["quantity_on_hand"] = (decimal)invDataRow["quantity_on_hand"] - (decimal)oriDataRow["quantity_shipped"];
                            if (_orderRequestItem.Columns.Contains("quantity_on_hand"))
                            {
                                bool origReadOnlyValue = false;
                                origReadOnlyValue = _orderRequestItem.Columns["quantity_on_hand"].ReadOnly;
                                _orderRequestItem.Columns["quantity_on_hand"].ReadOnly = false;
                                _orderRequestItem.Rows.Find(orderRequestAutoDeductPkey)["quantity_on_hand"] = invDataRow["quantity_on_hand"];
                                _orderRequestItem.Rows.Find(orderRequestAutoDeductPkey).AcceptChanges();
                                _orderRequestItem.Columns["quantity_on_hand"].ReadOnly = origReadOnlyValue;
                            }
                        }
                    }

                    // Save the inventory deductions to the remote server...
                    DataSet invSaveResults = _sharedUtils.SaveWebServiceData(orderRequestItemInventoryRows);
                    // If there are errors in saving the inventory records - manually increment the error count and add the error message to the base record...
                    if (invSaveResults.Tables.Contains("ExceptionTable") && invSaveResults.Tables["ExceptionTable"].Rows.Count > 0)
                    {
                        foreach (DataRow drErr in invSaveResults.Tables["get_inventory"].Rows)
                        {
                            errorCount++;
                            string errorMsg = "\tError auto-deducting inventory amount.  Error Message: " + invSaveResults.Tables["ExceptionTable"].Rows[0]["Message"].ToString();
                            string oriCurrentRowFilter = _orderRequestItem.DefaultView.RowFilter;
                            _orderRequestItem.DefaultView.RowFilter = oriCurrentRowFilter + " AND inventory_id=" + drErr["inventory_id"].ToString();
                            foreach (DataRowView drv in _orderRequestItem.DefaultView)
                            {
                                drv.Row.RowError += errorMsg;
                            }
                            _orderRequestItem.DefaultView.RowFilter = oriCurrentRowFilter;
                        }
                    }
                }
            }

            // Process Order Request Actions...
            //
            // Get the changes (if any) for the order_request_action table and commit them to the remote database...
            if (_orderRequestAction != null && _orderRequestAction.GetChanges() != null)
            {
                // Before saving the results to the remote server check to see if any new rows in order_request_item have
                // a FK related to a new row in the order_request table (pkey < 0).  If so get the new pkey returned from
                // the get_order_reqeust save and update the records in order_request_item...
                DataRow[] orderRequestActionRowsWithNewParent = _orderRequestAction.Select("order_request_id<0");
                foreach (DataRow dr in orderRequestActionRowsWithNewParent)
                {
                    // "OriginalPrimaryKeyID" "NewPrimaryKeyID"
                    DataRow[] newParent = orderRequestSaveResults.Tables["order_wizard_get_order_request"].Select("OriginalPrimaryKeyID=" + dr["order_request_id"].ToString());
                    if (newParent != null && newParent.Length > 0)
                    {
                        dr["order_request_id"] = newParent[0]["NewPrimaryKeyID"];
                    }
                }

                orderRequestActionChanges.Tables.Add(_orderRequestAction.GetChanges());
                ScrubData(orderRequestActionChanges);

                // Save the order request action changes to the remote server...
                orderRequestActionSaveResults = _sharedUtils.SaveWebServiceData(orderRequestActionChanges);
                // Sync the saved results with the original table (to give user feedback about results of save)...
                if (orderRequestActionSaveResults.Tables.Contains(_orderRequestAction.TableName))
                {
                    errorCount += SyncSavedResults(_orderRequestAction, orderRequestActionSaveResults.Tables[_orderRequestAction.TableName]);
                }
            }

            // Process Order Request Attachments...
            //
            // Get the changes (if any) for the order_request_attach table and commit them to the remote database...
            if (_orderRequestAttach != null && _orderRequestAttach.GetChanges() != null)
            {
                // Before saving the results to the remote server check to see if any new rows in order_request_item have
                // a FK related to a new row in the order_request table (pkey < 0).  If so get the new pkey returned from
                // the get_order_reqeust save and update the records in order_request_item...
                DataRow[] orderRequestAttachRowsWithNewParent = _orderRequestAttach.Select("order_request_id<0");
                foreach (DataRow dr in orderRequestAttachRowsWithNewParent)
                {
                    // "OriginalPrimaryKeyID" "NewPrimaryKeyID"
                    DataRow[] newParent = orderRequestSaveResults.Tables["order_wizard_get_order_request"].Select("OriginalPrimaryKeyID=" + dr["order_request_id"].ToString());
                    if (newParent != null && newParent.Length > 0)
                    {
                        dr["order_request_id"] = newParent[0]["NewPrimaryKeyID"];
                    }
                }

                // Get the rows in the _orderRequestAttach table that have changes...
                DataTable dtAttachChanges = _orderRequestAttach.GetChanges();

                // Accept changes for any rows that do not have any remaining changes (in other words the only changes were in the now removed local_path column)...
                foreach (DataRow dr in dtAttachChanges.Rows)
                {
                    if (dr.RowState == DataRowState.Modified)
                    {
                        bool rowIsClean = true;
                        foreach (DataColumn dc in dtAttachChanges.Columns)
                        {
                            if (dc.ColumnName != "local_path" && dr[dc, DataRowVersion.Original].ToString() != dr[dc, DataRowVersion.Current].ToString()) rowIsClean = false;
                        }
                        if (rowIsClean) dr.AcceptChanges();
                    }
                }

                // Now add the table only if there are still remaining changes in the datatable...
                if (dtAttachChanges != null && dtAttachChanges.GetChanges() != null)
                {
                    orderRequestAttachChanges.Tables.Add(dtAttachChanges.GetChanges());
                    ScrubData(orderRequestAttachChanges);

                    // Find all the records that are new (or where the virtual_path has been modified) and thus need to have the image file uploaded...
                    foreach (DataRow dr in orderRequestAttachChanges.Tables[_orderRequestAttach.TableName].Rows)
                    {
                        if (dr.RowState != DataRowState.Deleted &&
                            dr.RowState != DataRowState.Detached &&
                            (dr.RowState == DataRowState.Added || dr["virtual_path", DataRowVersion.Original] != dr["virtual_path", DataRowVersion.Current]))
                        {
                            string virtualPath = dr["virtual_path"].ToString();
                            // Strip out any Windows/DOS path delimiters...
                            if (virtualPath.Contains(@"\"))
                            {
                                virtualPath = virtualPath.Substring(virtualPath.LastIndexOf(@"\") + 1);
                            }
                            // Strip out any HTML path delimiters...
                            if (virtualPath.Contains(@"/"))
                            {
                                virtualPath = virtualPath.Substring(virtualPath.LastIndexOf(@"/") + 1);
                            }
                            virtualPath = @"\order_request_attach\" + dr["order_request_id"].ToString() + @"\" + virtualPath;
                            string returnedPath = "";
                            // Get the byte array for the attachment...
                            if (dr.Table.Columns.Contains("local_path") && !string.IsNullOrEmpty(dr["local_path"].ToString()))
                            {
                                byte[] attachBytes = System.IO.File.ReadAllBytes(dr["local_path"].ToString());
                                // Attempt to upload the image to the remote server...
                                if (attachBytes != null && !string.IsNullOrEmpty(virtualPath))
                                {
                                    returnedPath = _sharedUtils.SaveAttachment(virtualPath, attachBytes, true, true);
                                    if (!string.IsNullOrEmpty(returnedPath))
                                    {
                                        dr["virtual_path"] = virtualPath;
                                        dr["thumbnail_virtual_path"] = virtualPath.Insert(virtualPath.LastIndexOf('.'), "_thumbnail");
                                    }
                                }
                            }
                        }
                    }

                    // Drop the local_path column used internally (because it is not saved to the database)...
                    orderRequestAttachChanges.Tables[_orderRequestAttach.TableName].Columns.Remove("local_path");

                    // Save the order request attachment changes to the remote server...
                    orderRequestAttachSaveResults = _sharedUtils.SaveWebServiceData(orderRequestAttachChanges);
                    // Sync the saved results with the original table (to give user feedback about results of save)...
                    if (orderRequestAttachSaveResults.Tables.Contains(_orderRequestAttach.TableName))
                    {
                        errorCount += SyncSavedResults(_orderRequestAttach, orderRequestAttachSaveResults.Tables[_orderRequestAttach.TableName]);
                    }
                }
                else
                {
                    // The only changes to this table were in the 'local_path' column so accept all changes to this table...
                    _orderRequestAttach.AcceptChanges();
                }
            }

            // Process Phyto Log...
            //
            // Get the changes (if any) for the order_request_phyto_log table and commit them to the remote database...
            if (_orderRequestPhytoLog != null && _orderRequestPhytoLog.GetChanges() != null)
            {
                // Before saving the results to the remote server check to see if any new rows in order_request_phyto_log have
                // a FK related to a new row in the order_request table (pkey < 0).  If so get the new pkey returned from
                // the get_order_reqeust save and update the records in order_request_item...
                DataRow[] orderRequestPhytoLogRowsWithNewParent = _orderRequestPhytoLog.Select("order_request_id<0");
                foreach (DataRow dr in orderRequestPhytoLogRowsWithNewParent)
                {
                    // "OriginalPrimaryKeyID" "NewPrimaryKeyID"
                    DataRow[] newParent = orderRequestSaveResults.Tables["order_wizard_get_order_request"].Select("OriginalPrimaryKeyID=" + dr["order_request_id"].ToString());
                    if (newParent != null && newParent.Length > 0)
                    {
                        dr["order_request_id"] = newParent[0]["NewPrimaryKeyID"];
                    }
                }

                orderRequestPhytoLogChanges.Tables.Add(_orderRequestPhytoLog.GetChanges());
                ScrubData(orderRequestPhytoLogChanges);

                // Save the order request action changes to the remote server...
                orderRequestPhytoLogSaveResults = _sharedUtils.SaveWebServiceData(orderRequestPhytoLogChanges);
                // Sync the saved results with the original table (to give user feedback about results of save)...
                if (orderRequestPhytoLogSaveResults.Tables.Contains(_orderRequestPhytoLog.TableName))
                {
                    errorCount += SyncSavedResults(_orderRequestPhytoLog, orderRequestPhytoLogSaveResults.Tables[_orderRequestPhytoLog.TableName]);
                }
            }

            // Now add the new changes to the _changedRecords dataset (this data will be passed back to the calling program)...
            if (_orderRequest != null && orderRequestSaveResults != null && orderRequestSaveResults.Tables.Contains(_orderRequest.TableName))
            {
                string pkeyName = orderRequestSaveResults.Tables[_orderRequest.TableName].PrimaryKey[0].ColumnName;
                bool origColumnReadOnlyValue = orderRequestSaveResults.Tables[_orderRequest.TableName].Columns[pkeyName].ReadOnly;
                foreach (DataRow dr in orderRequestSaveResults.Tables[_orderRequest.TableName].Rows)
                {
                    if (dr["SavedAction"].ToString().ToUpper() == "INSERT" &&
                        dr["SavedStatus"].ToString().ToUpper() == "SUCCESS")
                    {
                        dr.Table.Columns[pkeyName].ReadOnly = false;
                        dr[pkeyName] = dr["NewPrimaryKeyID"];
                        dr.AcceptChanges();
                    }
                }
                orderRequestSaveResults.Tables[_orderRequest.TableName].Columns[pkeyName].ReadOnly = origColumnReadOnlyValue;

                if (_changedRecords.Tables.Contains(_orderRequest.TableName))
                {
                    // If the saved results table exists - update or insert the new records...
                    _changedRecords.Tables[_orderRequest.TableName].Load(orderRequestSaveResults.Tables[_orderRequest.TableName].CreateDataReader(), LoadOption.Upsert);
                    _changedRecords.Tables[_orderRequest.TableName].AcceptChanges();

                }
                else
                {
                    // If the saved results table doesn't exist - create it (and include the new records)...
                    _changedRecords.Tables.Add(orderRequestSaveResults.Tables[_orderRequest.TableName].Copy());
                    _changedRecords.AcceptChanges();
                }
            }

            return errorCount;
        }

        private void ScrubData(DataSet ds)
        {
            // Make sure all non-nullable fields do not contain a null value - if they do, replace it with the default value...
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr.RowState != DataRowState.Deleted)
                    {
                        foreach (DataColumn dc in dt.Columns)
                        {
                            if (dc.ExtendedProperties.Contains("is_nullable") &&
                                dc.ExtendedProperties["is_nullable"].ToString().Trim().ToUpper() == "N" &&
                                dr[dc] == DBNull.Value)
                            {
                                if (dc.ExtendedProperties.Contains("default_value") &&
                                    !string.IsNullOrEmpty(dc.ExtendedProperties["default_value"].ToString()) &&
                                    dc.ExtendedProperties["default_value"].ToString().Trim().ToUpper() != "{DBNULL.VALUE}")
                                {
                                    dr[dc] = dc.ExtendedProperties["default_value"].ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        private int SyncSavedResults(DataTable originalTable, DataTable savedResults)
        {
            int errorCount = 0;

            if (savedResults != null && savedResults.PrimaryKey.Length == 1)
            {
                string pKeyCol = savedResults.PrimaryKey[0].ColumnName.Trim().ToUpper();
                savedResults.Columns[pKeyCol].ReadOnly = false;
                foreach (DataRow dr in savedResults.Rows)
                {
                    DataRow originalRow = originalTable.Rows.Find(dr["OriginalPrimaryKeyID"]);

                    switch (dr["SavedAction"].ToString())
                    {
                        case "Insert":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the originalTable row's status for this new row to committed (and update the pkey with the int returned from the server DB)...
                                if (originalRow != null)
                                {
                                    bool origColumnReadOnlyValue = originalRow.Table.Columns[pKeyCol].ReadOnly;
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = false;
                                    originalRow[pKeyCol] = dr["NewPrimaryKeyID"];
                                    originalRow.AcceptChanges();
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = origColumnReadOnlyValue;
                                    originalRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Update":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                originalRow.AcceptChanges();
                                originalRow.ClearErrors();
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Delete":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the row's status for this deleted row to committed...
                                if (originalRow != null)
                                {
                                    originalRow.AcceptChanges();
                                    originalRow.ClearErrors();
                                }
                                else
                                {
                                    DataRow deletedRow = null;
                                    foreach (DataRow deleteddr in originalTable.Rows)
                                    {
                                        if (deleteddr.RowState == DataRowState.Deleted && deleteddr[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                        {
                                            deletedRow = deleteddr;
                                        }
                                    }
                                    deletedRow.AcceptChanges();
                                    deletedRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                // Find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in originalTable.Rows)
                                {
                                    if (deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        deletedRow.RejectChanges();
                                        deletedRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }

                // WARNING!!!  This is a HACK to handle a bug in the Middle Tier!!!  The bug is that the Middle Tier will not return a status 
                //             for some of the records sent for saving (reason is unclear).  What should happen is that the Middle Tier should
                //             return all records the CT sends (to be saved to the database) and the returned records should have a 'SavedStatus'  
                //             value that can be consumed by the CT so that feedback can be provided to the end user.  If the record is simply removed 
                //             from the returned save results - it is unknown whether or not the Middle Tier was successful or unsuccessful.  So until
                //             the Middle Tier has this bug fixed we will use this HACK below to determine if a record was successfully saved by
                //             comparing the local CT 'original version with the 'current' version of each field in the table.
                //             
                //             (I know...  It only works for UPDATES (and it's ugly) - but hey...
                //              it works so it stays in the CT code until the Middle Tier is fixed)
                //
                // BEGIN HACK...
                //
                foreach (DataRow originalRow in originalTable.Rows)
                {
                    if (originalRow.RowState == DataRowState.Modified)
                    {
                        DataRow[] matchingRows = savedResults.Select("OriginalPrimaryKeyID=" + originalRow[pKeyCol]);

                        if (matchingRows.Length < 1)
                        {
                            // The Middle Tier did not return a record that matches the record sent for INSERT/DELETE/UPDATE so
                            // let's go through each field in the record not returned by the Middle Tier and compare the 'original' value 
                            // with the 'current' value - and if everything is the same 'accept' changes for the record.

                            // Start out assuming the records are identical...
                            bool identicalRow = true;
                            foreach (DataColumn dc in originalTable.Columns)
                            {
                                if (originalRow[dc.ColumnName, DataRowVersion.Current].ToString() != originalRow[dc.ColumnName, DataRowVersion.Original].ToString())
                                {
                                    identicalRow = false;
                                }
                            }

                            if (identicalRow)
                            {
                                // The rows appear to be the same on the database and the CT so accept the changes...
                                originalRow.AcceptChanges();
                                originalRow.ClearErrors();
                            }
                        }
                    }
                }
                //
                //  END HACK...
                //
            }
            return errorCount;
        }

        void _orderRequestBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (_orderRequestBindingSource.List.Count > 0)
            {
                OrdersPage.Enabled = true;
                ActionsPage.Enabled = true;
            }
            else
            {
                OrdersPage.Enabled = false;
                ActionsPage.Enabled = false;
            }

            formatControls(OrdersPage.Controls, _orderRequestBindingSource);
        }

        void _orderRequestBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            if (_orderRequestBindingSource.List.Count > 0)
            {
                // Update the row error message for this accession row...
                if (string.IsNullOrEmpty(((DataRowView)_orderRequestBindingSource.Current).Row.RowError))
                {
                    if (ux_textboxOrderRequestRowError.Visible)
                    {
                        // Hide the error textbox...
                        ux_textboxOrderRequestRowError.Visible = false;
                        ux_textboxOrderRequestRowError.Text = "";
                        // Resize the order_request_items datagridview to fill in the space previously used by the error textbox...
                        int dgvBottom = ux_datagridviewOrderRequestItem.Location.Y + ux_datagridviewOrderRequestItem.Height - ux_textboxOrderRequestRowError.Height / 2;
                        int txtbxTop = ux_textboxOrderRequestRowError.Location.Y;
                        if (dgvBottom < txtbxTop)
                        {
                            ux_datagridviewOrderRequestItem.Height += ux_textboxOrderRequestRowError.Height;
                        }
                    }
                }
                else
                {
                    if (!ux_textboxOrderRequestRowError.Visible)
                    {
                        // Show the error textbox and populate it with the order_request row error text...
                        ux_textboxOrderRequestRowError.Visible = true;
                        ux_textboxOrderRequestRowError.ReadOnly = false;
                        ux_textboxOrderRequestRowError.Enabled = true;
                        ux_textboxOrderRequestRowError.Text = ((DataRowView)_orderRequestBindingSource.Current).Row.RowError;
                        // Resize the order_request_items datagridview to fit both the datagridview and the error textbox in the order wizard...
                        int dgvBottom = ux_datagridviewOrderRequestItem.Location.Y + ux_datagridviewOrderRequestItem.Height - ux_textboxOrderRequestRowError.Height / 2;
                        int txtbxTop = ux_textboxOrderRequestRowError.Location.Y;
                        if (dgvBottom > txtbxTop)
                        {
                            ux_datagridviewOrderRequestItem.Height -= ux_textboxOrderRequestRowError.Height;
                        }
                    }
                }
                string pkey = ((DataRowView)_orderRequestBindingSource.Current)[_orderRequest.PrimaryKey[0].ColumnName].ToString();
                // Process order items...
                ux_labelOrderItemAlertFlag.Visible = false;
                ux_datagridviewOrderRequestItem.EndEdit();
                _orderRequestItemBindingSource.EndEdit();
                if (_orderRequestItem != null && !string.IsNullOrEmpty(pkey) && _orderRequestItem.Columns.Contains("order_request_id"))
                {
                    // Filter items to the order request id...
                    _orderRequestItem.DefaultView.RowFilter = "order_request_id=" + pkey.Trim().ToLower();
                    if (OrderItemsContainAlerts()) ux_labelOrderItemAlertFlag.Visible = true;
                    if (OrderItemsContainDuplicates()) ux_labelOrderItemDuplicateFlag.Visible = true;
                    // Refresh the Order Items formatting...
                    RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
                    // Refresh the 'Ship All Remaining Items' button tool tip text...
                    int itemsShippedOrAvailableToShip = 0;
                    foreach (DataRowView drv in _orderRequestItem.DefaultView)
                    {
                        switch (drv["status_code"].ToString().ToUpper())
                        {
                            case "NEW":
                                itemsShippedOrAvailableToShip++;
                                break;
                            case "PENDING":
                                itemsShippedOrAvailableToShip++;
                                break;
                            case "INSPECT":
                                itemsShippedOrAvailableToShip++;
                                break;
                            case "SHIP":
                                itemsShippedOrAvailableToShip++;
                                break;
                            default:
                                break;
                        }

                    }
                    toolTip1.SetToolTip(ux_buttonShipAllRemaining, "NOTE: " + (_orderRequestItem.DefaultView.Count - itemsShippedOrAvailableToShip).ToString() + " item(s) will remain un-shipped");
                }
                // Process order actions...
                ux_datagridviewOrderRequestAction.EndEdit();
                _orderRequestActionBindingSource.EndEdit();
                if (_orderRequestAction != null && !string.IsNullOrEmpty(pkey) && _orderRequestAction.Columns.Contains("order_request_id"))
                {
                    // Filter items to the order request id...
                    _orderRequestAction.DefaultView.RowFilter = "order_request_id=" + pkey.Trim().ToLower();
                    // Calculate the total cost of all actions on this order request id...
                    decimal totalCost = 0;
                    foreach (DataRowView drv in _orderRequestAction.DefaultView)
                    {
                        decimal itemCost = 0;
                        if (decimal.TryParse(drv["action_cost"].ToString(), out itemCost))
                        {
                            totalCost += itemCost;
                        }
                    }
                    ux_textboxTotalCost.Text = totalCost.ToString("c");
                }
                // Process order attachments...
                ux_datagridviewOrderRequestAttach.EndEdit();
                _orderRequestAttachBindingSource.EndEdit();
                if (_orderRequestAttach != null && !string.IsNullOrEmpty(pkey) && _orderRequestAttach.Columns.Contains("order_request_id"))
                {
                    // Filter items to the order request id...
                    _orderRequestAttach.DefaultView.RowFilter = "order_request_id=" + pkey.Trim().ToLower();
                }
                // Process order phyto log...
                ux_datagridviewOrderRequestPhytoLog.EndEdit();
                _orderRequestPhytoLogBindingSource.EndEdit();
                if (_orderRequestPhytoLog != null && !string.IsNullOrEmpty(pkey) && _orderRequestPhytoLog.Columns.Contains("order_request_id"))
                {
                    // Filter items to the order request id...
                    _orderRequestPhytoLog.DefaultView.RowFilter = "order_request_id=" + pkey.Trim().ToLower();
                }

                // Update the info displayed in the Navigation Toolbar for this order...
                bindingNavigatorFormOrderNumber.Text = ((DataRowView)_orderRequestBindingSource.Current).Row["order_request_id"].ToString();
                bindingNavigatorFormItemCount.Text = ux_datagridviewOrderRequestItem.Rows.Count.ToString();
            }
            else
            {
                bindingNavigatorFormOrderNumber.Text = "";
                bindingNavigatorFormItemCount.Text = "0";
                ux_labelOrderItemAlertFlag.Visible = false;
            }
        }

        private bool OrderItemsContainAlerts()
        {
            bool hasAlerts = false;
            // Visually flag IPR items in the order...
            foreach (DataGridViewRow dgvr in ux_datagridviewOrderRequestItem.Rows)
            {
                if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("ipr_restriction") && !string.IsNullOrEmpty(dgvr.Cells["ipr_restriction"].Value.ToString()))
                {
                    hasAlerts = true;
                }
                if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("xpvp_warning") && !string.IsNullOrEmpty(dgvr.Cells["xpvp_warning"].Value.ToString()))
                {
                    hasAlerts = true;
                }
                if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("quarantine_restriction") && !string.IsNullOrEmpty(dgvr.Cells["quarantine_restriction"].Value.ToString()))
                {
                    hasAlerts = true;
                }
                if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("noxious_restriction") && !string.IsNullOrEmpty(dgvr.Cells["noxious_restriction"].Value.ToString()))
                {
                    hasAlerts = true;
                }
                if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("quantity_on_hand") &&
                    ((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("quantity_on_hand_unit_code") &&
                    ((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("quantity_shipped") &&
                    ((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("quantity_shipped_unit_code"))
                {
                    if (dgvr.Cells["quantity_on_hand_unit_code"].Value.ToString().Trim().ToUpper() == dgvr.Cells["quantity_shipped_unit_code"].Value.ToString().Trim().ToUpper())
                    {
                        double quantity_on_hand;
                        double quantity_shipped;
                        if (!double.TryParse(dgvr.Cells["quantity_on_hand"].Value.ToString(), out quantity_on_hand) ||
                            !double.TryParse(dgvr.Cells["quantity_shipped"].Value.ToString(), out quantity_shipped) ||
                            quantity_on_hand - quantity_shipped < 0.0)
                        {
                            hasAlerts = true;
                        }
                    }
                }
            }

            return hasAlerts;
        }

        private bool OrderItemsContainDuplicates()
        {
            bool hasDuplicates = false;
            // Visually flag duplicate rows...
            if (((DataTable)((BindingSource)ux_datagridviewOrderRequestItem.DataSource).DataSource).Columns.Contains("inventory_id"))
            {
                DataTable dt = ((DataTable)((BindingSource)ux_datagridviewOrderRequestItem.DataSource).DataSource).DefaultView.ToTable(true, new string[] { "inventory_id" });
                if (dt.Rows.Count < ux_datagridviewOrderRequestItem.Rows.Count) hasDuplicates = true;
            }


            return hasDuplicates;
        }

        void _webOrderRequestBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            ux_groupboxMergeOrderRequest.Visible = false;
            if (_webOrderRequestBindingSource.List.Count > 0)
            {
                string pkey = ((DataRowView)_webOrderRequestBindingSource.Current)[_webOrderRequest.PrimaryKey[0].ColumnName].ToString();
                //if (_webOrderRequestItem != null && !string.IsNullOrEmpty(pkey)) _webOrderRequestItem.DefaultView.RowFilter = "web_order_request_id=" + pkey.Trim().ToLower();
                if (_webOrderRequestItemBindingSource != null && !string.IsNullOrEmpty(pkey) && ((DataTable)_webOrderRequestItemBindingSource.DataSource).Columns.Contains("web_order_request_id")) ((DataTable)_webOrderRequestItemBindingSource.DataSource).DefaultView.RowFilter = "web_order_request_id=" + pkey.Trim().ToLower();
                if (_webOrderRequestAction != null && !string.IsNullOrEmpty(pkey) && _webOrderRequestAction.Columns.Contains("web_order_request_id")) _webOrderRequestAction.DefaultView.RowFilter = "web_order_request_id=" + pkey.Trim().ToLower();
                if (_webOrderRequestAttach != null && !string.IsNullOrEmpty(pkey) && _webOrderRequestAttach.Columns.Contains("web_order_request_id")) _webOrderRequestAttach.DefaultView.RowFilter = "web_order_request_id=" + pkey.Trim().ToLower();
                bindingNavigatorWebItemCount.Text = ux_datagridviewWebOrderRequestItem.Rows.Count.ToString();
                // Check to see if there are any open orders from this same web cooperator...
                if (_webOrderRequest.Columns.Contains("current_open_orders") &&
                    int.Parse(((DataRowView)_webOrderRequestBindingSource.Current)["current_open_orders"].ToString()) > 0 &&
                    _webOrderRequest.Columns.Contains("web_cooperator_id"))
                {
                    // @order_request.completed_date IS NULL AND @site.site_id IN (16) AND @order_request.web_order_request_id IN (SELECT web_order_request_id FROM web_order_request WHERE web_cooperator_id  = 22847)
                    string openOrdersQuery = "@order_request.completed_date IS NULL ";
                    openOrdersQuery += "AND @site.site_id IN (" + _sharedUtils.UserSiteID + ") ";
                    openOrdersQuery += "AND @order_request.web_order_request_id IN (SELECT web_order_request_id FROM web_order_request WHERE web_cooperator_id = " + ((DataRowView)_webOrderRequestBindingSource.Current)["web_cooperator_id"].ToString() + ")";

                    DataSet ds = _sharedUtils.SearchWebService(openOrdersQuery, true, true, "", "order_request", 0, 0);
                    if (ds.Tables.Count == 2 &&
                        ds.Tables.Contains("SearchResult") &&
                        ds.Tables["SearchResult"].Rows.Count > 0)
                    {
                        DataTable dtOpenOrders = new DataTable();
                        dtOpenOrders.Columns.Add("value_member");
                        dtOpenOrders.Columns.Add("display_member");
                        dtOpenOrders.DefaultView.Sort = "value_member DESC";
                        foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                        {
                            DataRow newRow = dtOpenOrders.NewRow();
                            newRow["value_member"] = dr["ID"].ToString();
                            newRow["display_member"] = _sharedUtils.GetLookupDisplayMember("order_request_lookup", dr["ID"].ToString(), "", dr["ID"].ToString());
                            dtOpenOrders.Rows.Add(newRow);
                        }
                        ux_comboboxMergeOrderRequest.ValueMember = "value_member";
                        ux_comboboxMergeOrderRequest.DisplayMember = "display_member";
                        ux_comboboxMergeOrderRequest.DataSource = dtOpenOrders;
                        ux_groupboxMergeOrderRequest.Visible = true;
                    }
                }
            }
            else
            {
                bindingNavigatorWebItemCount.Text = "0";
            }
        }

        #endregion

        #region Tab Control Logic...
        private void ux_tabcontrolMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ux_tabcontrolMain.SelectedTab == OrdersPage)
            {
                bindingNavigatorFormAddNewItem.Enabled = true;
                bindingNavigatorFormDeleteItem.Enabled = true;
                ux_bindingnavigatorForm.Enabled = true;
                ux_bindingnavigatorForm.Visible = true;
                ux_groupboxOrderFilters.Visible = true;
                ux_groupboxWebOrderFilters.Visible = true;
                ux_groupboxPrinting.Visible = true;
            }
            else if (ux_tabcontrolMain.SelectedTab == WebOrderPage)
            {
                bindingNavigatorFormAddNewItem.Enabled = false;
                bindingNavigatorFormDeleteItem.Enabled = false;
                ux_bindingnavigatorForm.Enabled = false;
                ux_bindingnavigatorForm.Visible = false;
                ux_groupboxOrderFilters.Visible = false;
                ux_groupboxWebOrderFilters.Visible = true;
                bindingNavigatorWebAddNewItem.Enabled = false;
                bindingNavigatorWebDeleteItem.Enabled = false;
                ux_groupboxPrinting.Visible = false;
            }
            else
            {
                bindingNavigatorFormAddNewItem.Enabled = false;
                bindingNavigatorFormDeleteItem.Enabled = false;
                ux_bindingnavigatorForm.Enabled = true;
                ux_bindingnavigatorForm.Visible = true;
                ux_groupboxOrderFilters.Visible = true;
                ux_groupboxWebOrderFilters.Visible = false;
                ux_groupboxPrinting.Visible = false;

                foreach (DataRowView drv in _orderRequestBindingSource.List)
                {
                    if (drv.IsEdit ||
                        drv.Row.RowState == DataRowState.Added ||
                        drv.Row.RowState == DataRowState.Deleted ||
                        drv.Row.RowState == DataRowState.Detached ||
                        drv.Row.RowState == DataRowState.Modified)
                    {
                        drv.EndEdit();
                        //drv.Row.ClearErrors();
                    }
                }
                // If the user is looking at the Attachments tab page ensure that the image has been loaded...
                if (ux_tabcontrolMain.SelectedTab == AttachmentsPage &&
                    ux_datagridviewOrderRequestAttach.Rows.Count > 0)
                {
                    ux_datagridviewOrderRequestAttach.CurrentCell = null;
                    ux_datagridviewOrderRequestAttach.CurrentCell = ux_datagridviewOrderRequestAttach[0, 0];
                }
            }
        }
        #endregion

        #region Order Request Action logic...
        private void ux_buttonNewOrderRequestActionRow_Click(object sender, EventArgs e)
        {
            string pkey = ((DataRowView)_orderRequestBindingSource.Current)[_orderRequest.PrimaryKey[0].ColumnName].ToString();
            DataRow newOrderRequestAction = _orderRequestAction.NewRow();
            newOrderRequestAction["order_request_id"] = pkey;
            _orderRequestAction.Rows.Add(newOrderRequestAction);
            // Calculate row/column location for active cell (cell with focus)...
            //ux_datagridviewOrderRequestAction.CurrentCell = ux_datagridviewOrderRequestAction.Rows[ux_datagridviewOrderRequestAction.Rows.GetLastRow(DataGridViewElementStates.Displayed)].Cells["name"];
            int newRowIndex = ux_datagridviewOrderRequestAction.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewOrderRequestAction.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            // For some reason the code below does not work because the new row is out of order (at index=0) with the rest
            // of the rows - so just use the last row found above instead...
            //for (int i = 0; i < ux_datagridviewOrderRequestAction.Rows.Count; i++)
            //{
            //    if (ux_datagridviewOrderRequestAction["order_request_action_id", i].Value.Equals(newOrderRequestAction["order_request_action_id"])) newRowIndex = i;
            //}
            foreach (DataGridViewColumn dgvc in ux_datagridviewOrderRequestAction.Columns)
            {
                if (dgvc.Name == "action_name_code")
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            // Populate the started_date and started_date_code fields
            if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
            if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
            // Set focus to current cell (active cell)...
            ux_datagridviewOrderRequestAction.CurrentCell = ux_datagridviewOrderRequestAction[newColIndex, newRowIndex];
        }

        private void ux_buttonAlertedOrderRequestAction_Click(object sender, EventArgs e)
        {
            string OrderRequestPKey = ((DataRowView)_orderRequestBindingSource.Current)[_orderRequest.PrimaryKey[0].ColumnName].ToString();
            DataRow newOrderRequestAction = _orderRequestAction.NewRow();
            newOrderRequestAction["order_request_id"] = OrderRequestPKey;
            _orderRequestAction.Rows.Add(newOrderRequestAction);
            // Calculate row/column location for active cell (cell with focus)...
            //ux_datagridviewOrderRequestAction.CurrentCell = ux_datagridviewOrderRequestAction.Rows[ux_datagridviewOrderRequestAction.Rows.GetLastRow(DataGridViewElementStates.Displayed)].Cells["name"];
            int newRowIndex = ux_datagridviewOrderRequestAction.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewOrderRequestAction.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            // For some reason the code below does not work because the new row is out of order (at index=0) with the rest
            // of the rows - so just use the last row found above instead...
            //for (int i = 0; i < ux_datagridviewOrderRequestAction.Rows.Count; i++)
            //{
            //    if (ux_datagridviewOrderRequestAction["order_request_action_id", i].Value.Equals(newOrderRequestAction["order_request_action_id"])) newRowIndex = i;
            //}
            foreach (DataGridViewColumn dgvc in ux_datagridviewOrderRequestAction.Columns)
            {
                if (dgvc.Name == "cooperator_id")
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            // Populate the action code...
            if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "ALERTED";
            // Populate the started_date and started_date_code fields
            if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
            // Set focus to current cell (active cell)...
            ux_datagridviewOrderRequestAction.CurrentCell = ux_datagridviewOrderRequestAction[newColIndex, newRowIndex];
        }

        private void ux_buttonClearedOrderRequestAction_Click(object sender, EventArgs e)
        {
            string OrderRequestPKey = ((DataRowView)_orderRequestBindingSource.Current)[_orderRequest.PrimaryKey[0].ColumnName].ToString();
            DataRow newOrderRequestAction = _orderRequestAction.NewRow();
            newOrderRequestAction["order_request_id"] = OrderRequestPKey;
            _orderRequestAction.Rows.Add(newOrderRequestAction);
            // Calculate row/column location for active cell (cell with focus)...
            //ux_datagridviewOrderRequestAction.CurrentCell = ux_datagridviewOrderRequestAction.Rows[ux_datagridviewOrderRequestAction.Rows.GetLastRow(DataGridViewElementStates.Displayed)].Cells["name"];
            int newRowIndex = ux_datagridviewOrderRequestAction.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewOrderRequestAction.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            // For some reason the code below does not work because the new row is out of order (at index=0) with the rest
            // of the rows - so just use the last row found above instead...
            //for (int i = 0; i < ux_datagridviewOrderRequestAction.Rows.Count; i++)
            //{
            //    if (ux_datagridviewOrderRequestAction["order_request_action_id", i].Value.Equals(newOrderRequestAction["order_request_action_id"])) newRowIndex = i;
            //}
            foreach (DataGridViewColumn dgvc in ux_datagridviewOrderRequestAction.Columns)
            {
                if (dgvc.Name == "action_name_code")
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            // Populate the action code...
            if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "CUR_CLEARED";
            // Populate the started_date and started_date_code fields
            if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
            // Populate the completed_date and completed_date_code fields
            if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
            // Populate the cooperator field...
            if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
            // Set focus to current cell (active cell)...
            ux_datagridviewOrderRequestAction.CurrentCell = ux_datagridviewOrderRequestAction[newColIndex, newRowIndex];
        }

        private void ux_buttonNRROrderRequestAction_Click(object sender, EventArgs e)
        {
            string OrderRequestPKey = ((DataRowView)_orderRequestBindingSource.Current)[_orderRequest.PrimaryKey[0].ColumnName].ToString();
            DataRow newOrderRequestAction = _orderRequestAction.NewRow();
            newOrderRequestAction["order_request_id"] = OrderRequestPKey;
            _orderRequestAction.Rows.Add(newOrderRequestAction);
            // Calculate row/column location for active cell (cell with focus)...
            //ux_datagridviewOrderRequestAction.CurrentCell = ux_datagridviewOrderRequestAction.Rows[ux_datagridviewOrderRequestAction.Rows.GetLastRow(DataGridViewElementStates.Displayed)].Cells["name"];
            int newRowIndex = ux_datagridviewOrderRequestAction.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewOrderRequestAction.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            // For some reason the code below does not work because the new row is out of order (at index=0) with the rest
            // of the rows - so just use the last row found above instead...
            //for (int i = 0; i < ux_datagridviewOrderRequestAction.Rows.Count; i++)
            //{
            //    if (ux_datagridviewOrderRequestAction["order_request_action_id", i].Value.Equals(newOrderRequestAction["order_request_action_id"])) newRowIndex = i;
            //}
            foreach (DataGridViewColumn dgvc in ux_datagridviewOrderRequestAction.Columns)
            {
                if (dgvc.Name == "action_name_code")
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            // Populate the action code...
            if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "CUR_NRR";
            // Populate the started_date and started_date_code fields
            if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
            // Populate the completed_date and completed_date_code fields
            if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
            // Populate the cooperator field...
            if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
            // Set focus to current cell (active cell)...
            ux_datagridviewOrderRequestAction.CurrentCell = ux_datagridviewOrderRequestAction[newColIndex, newRowIndex];
        }

        private void ux_buttonFilledOrderRequestAction_Click(object sender, EventArgs e)
        {
            string OrderRequestPKey = ((DataRowView)_orderRequestBindingSource.Current)[_orderRequest.PrimaryKey[0].ColumnName].ToString();
            DataRow newOrderRequestAction = _orderRequestAction.NewRow();
            newOrderRequestAction["order_request_id"] = OrderRequestPKey;
            _orderRequestAction.Rows.Add(newOrderRequestAction);
            // Calculate row/column location for active cell (cell with focus)...
            //ux_datagridviewOrderRequestAction.CurrentCell = ux_datagridviewOrderRequestAction.Rows[ux_datagridviewOrderRequestAction.Rows.GetLastRow(DataGridViewElementStates.Displayed)].Cells["name"];
            int newRowIndex = ux_datagridviewOrderRequestAction.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewOrderRequestAction.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            // For some reason the code below does not work because the new row is out of order (at index=0) with the rest
            // of the rows - so just use the last row found above instead...
            //for (int i = 0; i < ux_datagridviewOrderRequestAction.Rows.Count; i++)
            //{
            //    if (ux_datagridviewOrderRequestAction["order_request_action_id", i].Value.Equals(newOrderRequestAction["order_request_action_id"])) newRowIndex = i;
            //}
            foreach (DataGridViewColumn dgvc in ux_datagridviewOrderRequestAction.Columns)
            {
                if (dgvc.Name == "action_name_code")
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            // Populate the action code...
            if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "ORDFILLED";
            // Populate the started_date and started_date_code fields
            if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
            // Populate the completed_date and completed_date_code fields
            if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
            // Populate the cooperator field...
            if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
            // Set focus to current cell (active cell)...
            ux_datagridviewOrderRequestAction.CurrentCell = ux_datagridviewOrderRequestAction[newColIndex, newRowIndex];
        }
        #endregion

        #region Order Items DGV logic...
        private void ux_datagridviewOrderRequestItem_DragOver(object sender, DragEventArgs e)
        {
            // Okay we are in the middle of a Drag and Drop operation and the mouse is in 
            // the DGV control so lets handle this event...

            // This code will change the cursor icon to give the user feedback about whether or not
            // the drag-drop operation is allowed...
            //

            // Get the DGV object...
            DataGridView dgv = (DataGridView)sender;

            // Convert the mouse coordinates from screen to client...
            Point ptClientCoord = dgv.PointToClient(new Point(e.X, e.Y));

            // Is this a string being dragged to the DGV...
            if (e.Data.GetDataPresent("System.String") && !dgv.ReadOnly)
            {
                e.Effect = DragDropEffects.Copy;
            }
            else if (e.Data.GetDataPresent("System.Data.DataSet") && !dgv.ReadOnly)
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ux_datagridviewOrderRequestItem_DragDrop(object sender, DragEventArgs e)
        {
            // The drag-drop event is coming to a close process this event to handle the dropping of
            // data into the treeview...

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Get the DGV object...
            DataGridView dgv = (DataGridView)sender;
            DataTable destinationTable = (DataTable)((BindingSource)dgv.DataSource).DataSource;

            // Is this an allowed drop???
            if (e.Effect != DragDropEffects.None)
            {
                if (e.Data.GetDataPresent("System.Data.DataSet") && e.Effect != DragDropEffects.None)
                {
                    // Is this a collection of dataset rows being dragged to the DGV...
                    DataSet dndData = (DataSet)e.Data.GetData("System.Data.DataSet");
                    DataTable sourceTable = dndData.Tables[0];
                    if (sourceTable.PrimaryKey.Length == 1)
                    {
                        if (sourceTable.PrimaryKey[0].ColumnName.ToUpper() == "INVENTORY_ID")
                        {
                            // Build the inventory pkey list...
                            string inventoryIDs = "";
                            foreach (DataRow dr in sourceTable.Rows)
                            {
                                inventoryIDs += dr["inventory_id"].ToString() + ",";
                            }
                            inventoryIDs = inventoryIDs.TrimEnd(new char[] { ',' });
                            // Retrieve all possible inventory records associated with the accessions pkey list...
                            DataSet allPossibleInventories = _sharedUtils.GetWebServiceData("order_wizard_get_inventory", ":inventoryid=" + inventoryIDs, 0, 0);
                            foreach (DataRow dr in sourceTable.Rows)
                            {
                                DataRow newOrderItem = BuildOrderRequestItemRow(dr[sourceTable.PrimaryKey[0].ColumnName].ToString(), destinationTable, allPossibleInventories.Tables["order_wizard_get_inventory"]);
                                if (newOrderItem != null) destinationTable.Rows.Add(newOrderItem);
                            }
                        }
                        else if (sourceTable.PrimaryKey[0].ColumnName.ToUpper() == "ACCESSION_ID")
                        {
                            // Build the accession pkey list...
                            string accessionIDs = "";
                            foreach (DataRow dr in sourceTable.Rows)
                            {
                                accessionIDs += dr["accession_id"].ToString() + ",";
                            }
                            accessionIDs = accessionIDs.TrimEnd(new char[] { ',' });
                            // Retrieve all possible inventory records associated with the accessions pkey list...
                            DataTable distributionInventories = FindDistributionInventoryFromAccessionList(accessionIDs);
                            foreach (DataRow dr in sourceTable.Rows)
                            {
                                string inventoryID = FindInventoryBestMatch(distributionInventories, dr["accession_id"].ToString(), "");

                                DataRow newOrderItem = BuildOrderRequestItemRow(inventoryID, destinationTable, distributionInventories);
                                if (newOrderItem != null) destinationTable.Rows.Add(newOrderItem);
                            }
                        }
                    }
                }
                else if (e.Data.GetDataPresent("System.String"))
                {
                    // Is this a string being dragged to the DGV...
                    char[] rowDelimiters = new char[] { '\r', '\n' };
                    char[] columnDelimiters = new char[] { '\t', ' ', ',' };
                    string rawText = (string)e.Data.GetData("System.String");
                    int badRows = 0;
                    int missingRows = 0;
                    bool importSuccess = false;
                    importSuccess = ImportTextToDataTableUsingAltKeys(rawText, destinationTable, rowDelimiters, columnDelimiters, out badRows, out missingRows);
                }
            }

            // Refresh order item formatting for the new rows...
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private bool ImportTextToDataTableUsingAltKeys(string rawImportText, DataTable destinationTable, char[] rowDelimiters, char[] columnDelimiters, out int badRows, out int missingRows)
        {
            string[] rawImportRows = rawImportText.Split(rowDelimiters, StringSplitOptions.RemoveEmptyEntries);
            List<string> accessionList = new List<string>();
            List<string> inventoryList = new List<string>();
            bool processedImportSuccessfully = false;
            badRows = 0;
            missingRows = 0;
            // Make sure there is text to process - if not bail out now...
            if (rawImportRows == null || rawImportRows.Length <= 0) return false;

            // Now start processing the rows...
            for (int i = 0; i < rawImportRows.Length; i++)
            {
                // Split the row into id parts - then reassemble it using only space delimiter...
                string distributionInventoryID = "";
                string[] idParts = rawImportRows[i].Split(columnDelimiters, StringSplitOptions.None);
                string fullID = "";
                foreach (string idPart in idParts)
                {
                    fullID += " " + idPart.Trim();
                }
                // Try to find a matching accession number first...
                string accessionPKey = _sharedUtils.GetLookupValueMember(null, "accession_lookup", fullID.Trim(), "", "");
                if (!string.IsNullOrEmpty(accessionPKey))
                {
                    // Found an accession number - add it to the accession list...
                    accessionList.Add(accessionPKey);
                }
                else
                {
                    // Couldn't find an accession number so try to find an inventory number...
                    distributionInventoryID = _sharedUtils.GetLookupValueMember(null, "inventory_lookup", fullID.Trim(), "", "");
                    if (!string.IsNullOrEmpty(distributionInventoryID))
                    {
                        inventoryList.Add(distributionInventoryID);
                    }
                }
            }

            // Retrieve all inventory records associated with the accessions and inventory numbers in the list(s)...

            // Build the accession pkey list...
            string accessionIDs = "";
            foreach (string accessionID in accessionList)
            {
                accessionIDs += accessionID + ",";
            }
            accessionIDs = accessionIDs.TrimEnd(new char[] { ',' });
            // Build the inventory pkey list...
            string inventoryIDs = "";
            foreach (string inventoryID in inventoryList)
            {
                inventoryIDs += inventoryID + ",";
            }
            inventoryIDs = inventoryIDs.TrimEnd(new char[] { ',' });

            // Retrieve all possible inventory records associated with the accessions pkey list...
            DataTable distributionInventories = FindDistributionInventoryFromAccessionList(accessionIDs);
            // Find the distribution inventory for each of the accessions in the list and build a new order request item record for each one...
            foreach (string accessionID in accessionList)
            {
                string inventoryID = FindInventoryBestMatch(distributionInventories, accessionID, "");
                DataRow newOrderItem = BuildOrderRequestItemRow(inventoryID, destinationTable, distributionInventories);

                if (newOrderItem != null) destinationTable.Rows.Add(newOrderItem);
                processedImportSuccessfully = true;
            }

            DataSet allDirectlyRequestedInventories = _sharedUtils.GetWebServiceData("order_wizard_get_inventory", ":inventoryid=" + inventoryIDs, 0, 0);
            // Build a new order request items record for each directly requested inventory...
            foreach (string inventoryID in inventoryList)
            {
                DataRow newOrderItem = BuildOrderRequestItemRow(inventoryID, destinationTable, allDirectlyRequestedInventories.Tables["order_wizard_get_inventory"]);
                destinationTable.Rows.Add(newOrderItem);
                processedImportSuccessfully = true;
            }

            return processedImportSuccessfully;
        }

        private bool ImportTextToDataTableUsingBlockStyle(string rawImportText, DataGridView dgv, char[] rowDelimiters, char[] columnDelimiters, out int badRows, out int missingRows)
        {
            bool processedImportSuccessfully = true;
            DataTable destinationTable = (DataTable)((BindingSource)dgv.DataSource).DataSource;
            string[] rawImportRows = rawImportText.Split(rowDelimiters, StringSplitOptions.RemoveEmptyEntries);
            string[] tempColumns = null;
            string newImportText = "";
            string newImportRowText = "";
            badRows = 0;
            missingRows = 0;

            // If the DGV does not have a currently active cell bail out now...
            if (dgv.CurrentCell == null) return false;
            // If the import string is empty bail out now...
            if (string.IsNullOrEmpty(rawImportText) || rawImportRows.Length < 1) return false;

            // Okay we need to build a new importText string that has column headers that include the friendly names for the primary key columns
            // and the friendly names for the dgv columns starting at the currenly active cell in the dgv...  Why are we doing this?  Because
            // we are going to pass this new importText string off to the 'ImportTextToDataTableUsingKeys' method, and since that method
            // requires a primary key or alternate pkey we are going to get them from the dgv starting at the current row of the current cell...

            // Step 1 - Determine the number of rows and columns in the incoming rawImportText (to use later for building the new ImportText string)...
            int rawImportRowCount = 0;
            int rawImportColCount = 0;
            // Estimate the number of rows and columns in the import text (assumes a rectangular shape)
            if (rawImportRows != null && rawImportRows.Length > 0)
            {
                rawImportRowCount = rawImportRows.Length;
                tempColumns = rawImportRows[0].Split(columnDelimiters, StringSplitOptions.None);
                if (tempColumns != null && tempColumns.Length > 0)
                {
                    rawImportColCount = tempColumns.Length;
                }
            }

            int minSelectedCol = dgv.Columns.Count;
            int maxSelectedCol = -1;
            int minSelectedRow = dgv.Rows.Count;
            int maxSelectedRow = -1;
            // Check to see if the datagridview's selected cells contains the CurrentCell
            // and if so use the selected cells as the destination cells...

            // Find the bounding rectangle for the selected cells...
            if (dgv.SelectedCells.Count == 1)
            {
                minSelectedCol = dgv.CurrentCell.ColumnIndex;
                maxSelectedCol = dgv.CurrentCell.ColumnIndex + rawImportColCount - 1;
                minSelectedRow = dgv.CurrentCell.RowIndex;
                maxSelectedRow = dgv.CurrentCell.RowIndex + rawImportRowCount - 1;
            }
            else
            {
                foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                {
                    if (dgvc.ColumnIndex < minSelectedCol) minSelectedCol = dgvc.ColumnIndex;
                    if (dgvc.ColumnIndex > maxSelectedCol) maxSelectedCol = dgvc.ColumnIndex;
                    if (dgvc.RowIndex < minSelectedRow) minSelectedRow = dgvc.RowIndex;
                    if (dgvc.RowIndex > maxSelectedRow) maxSelectedRow = dgvc.RowIndex;
                }
                if ((maxSelectedCol - minSelectedCol) < (rawImportColCount - 1)) maxSelectedCol = minSelectedCol + rawImportColCount - 1;
                if ((maxSelectedRow - minSelectedRow) < (rawImportRowCount - 1)) maxSelectedRow = minSelectedRow + rawImportRowCount - 1;
            }

            string modifiedImportText = "";
            // Now fill (or clip) the import data to fit the selected cells...
            for (int iSelectedRow = 0; iSelectedRow <= (maxSelectedRow - minSelectedRow); iSelectedRow++)
            {
                // 
                tempColumns = rawImportRows[iSelectedRow % rawImportRowCount].Split(columnDelimiters, StringSplitOptions.None);
                for (int iSelectedCol = 0; iSelectedCol <= (maxSelectedCol - minSelectedCol); iSelectedCol++)
                {
                    //
                    modifiedImportText += tempColumns[iSelectedCol % rawImportColCount] + "\t";
                }
                // Strip the last tab character and add a CR LF...
                modifiedImportText = modifiedImportText.Substring(0, modifiedImportText.Length - 1) + "\r\n";
            }

            // Step 2 - Get the primary key column names for the new column header row text...
            if (destinationTable.PrimaryKey.Length > 0)
            {
                foreach (DataColumn pKeyColumn in destinationTable.PrimaryKey)
                {
                    newImportText += _sharedUtils.GetFriendlyFieldName(pKeyColumn, pKeyColumn.ColumnName) + "\t";
                }
            }

            // Step 3 - Continue adding friendly column names to the import text (starting with the column name of the current cell's column HeaderText)...
            DataGridViewColumn currColumn = dgv.Columns[minSelectedCol];

            // Step 4 - Now repeat this process for each additional column in the rawImportText...
            for (int i = 0; i < Math.Max(rawImportColCount, maxSelectedCol - minSelectedCol + 1); i++)
            {
                if (currColumn != null)
                {
                    newImportText += currColumn.HeaderText + "\t";
                }
                else
                {
                    newImportText += "\t";
                }
                // Try to find the next visible column...
                currColumn = dgv.Columns.GetNextColumn(currColumn, DataGridViewElementStates.Visible, DataGridViewElementStates.Frozen);
            }
            // Strip the last tab character and add a CR LF...
            newImportText = newImportText.Substring(0, newImportText.Length - 1) + "\r\n";

            // Step 5 - Get the primary key for each row receiving pasted text and prepend it to the orginal import raw text...
            string[] modifiedImportRows = modifiedImportText.Split(rowDelimiters, StringSplitOptions.RemoveEmptyEntries);
            DataGridViewRow currRow = dgv.Rows[minSelectedRow];
            int nextRowIndex = currRow.Index;
            for (int i = 0; i < modifiedImportRows.Length; i++)
            {
                newImportRowText = "";
                if (currRow != null)
                {
                    if (destinationTable.PrimaryKey.Length > 0)
                    {
                        foreach (DataColumn pKeyColumn in destinationTable.PrimaryKey)
                        {
                            newImportRowText += ((DataRowView)currRow.DataBoundItem).Row[pKeyColumn].ToString() + "\t";
                        }
                    }
                    // Now add the original import row text to the new import row text...
                    newImportRowText += modifiedImportRows[i] + "\r\n";
                    // And now add it to the new import text string...
                    newImportText += newImportRowText;
                }

                // Finally, try to find the next visible row...
                nextRowIndex = dgv.Rows.GetNextRow(currRow.Index, DataGridViewElementStates.Visible);
                if (nextRowIndex != -1 &&
                    !dgv.Rows[nextRowIndex].IsNewRow &&
                    nextRowIndex >= minSelectedRow &&
                    nextRowIndex <= maxSelectedRow)
                {
                    currRow = dgv.Rows[nextRowIndex];
                }
                else
                {
                    // Looks like we hit the end of the rows in the DGV - bailout now...
                    break;
                }
            }

            // Step 6 - Now that we have built a new ImportText string that contains pkeys, we can pass it off to the 'ImportTextToDataTableUsingKeys' 
            processedImportSuccessfully = _sharedUtils.ImportTextToDataTableUsingKeys(newImportText, destinationTable, rowDelimiters, columnDelimiters, out badRows, out missingRows);

            return processedImportSuccessfully;
        }

        private DataTable FindDistributionInventoryFromAccessionList(string accessionPkeys)
        {
            string accessionList = accessionPkeys;
            DataTable distributionInventories = new DataTable();
            DataSet dsFoundDistributionInventory = _sharedUtils.GetWebServiceData("order_wizard_get_inventory", ":inventoryid=-1", 0, 0);
            distributionInventories = dsFoundDistributionInventory.Tables["order_wizard_get_inventory"].Clone();

            // Build the search criteria...
            List<string> searchCriteria = new List<string>();
            // The searchCriteria has been widened to include all inventory lotcodes for all accession in order to properly process requests for multiple form_code_types (not just 'SD')...
            //searchCriteria.Add("@inventory.accession_id IN (___accessionListPlaceHolder___) AND @inventory.is_distributable = 'Y' AND @inventory.availability_status_code='AVAIL' AND @inventory.is_available = 'Y' AND @site.site_id=" + _sharedUtils.UserSiteID);
            //searchCriteria.Add("@inventory.accession_id IN (___accessionListPlaceHolder___) AND @inventory.is_distributable = 'Y' AND @inventory.availability_status_code='AVAIL' AND @site.site_id=" + _sharedUtils.UserSiteID);
            //searchCriteria.Add("@inventory.accession_id IN (___accessionListPlaceHolder___) AND @inventory.is_distributable = 'Y' AND @site.site_id=" + _sharedUtils.UserSiteID);
            //searchCriteria.Add("@inventory.accession_id IN (___accessionListPlaceHolder___) AND @site.site_id=" + _sharedUtils.UserSiteID);
            //searchCriteria.Add("@inventory.accession_id IN (___accessionListPlaceHolder___) AND @inventory.is_distributable = 'Y' AND @inventory.availability_status_code='AVAIL' AND @inventory.is_available = 'Y'");
            //searchCriteria.Add("@inventory.accession_id IN (___accessionListPlaceHolder___) AND @inventory.is_distributable = 'Y' AND @inventory.availability_status_code='AVAIL'");
            //searchCriteria.Add("@inventory.accession_id IN (___accessionListPlaceHolder___) AND @inventory.is_distributable = 'Y'");
            searchCriteria.Add("@inventory.accession_id IN (___accessionListPlaceHolder___)");
            int i = 0;
            while (!string.IsNullOrEmpty(accessionList) && i < searchCriteria.Count)
            {
                // Build the search criteria...
                string searchEngineCriteria = searchCriteria[i].Replace("___accessionListPlaceHolder___", accessionList);
                // Search for distribution inventory that match the criteria...
                DataSet dsSearchDistributionInventory = _sharedUtils.SearchWebService(searchEngineCriteria, true, true, null, "inventory", 0, 0);

                // Show the progress bar and initialize it...
                bindingNavigatorWebProgressBarLabel.Visible = true;
                bindingNavigatorWebProgressBarLabel.Text = "Retrieving Inventory Records From Server:";
                bindingNavigatorWebProgressBar.Visible = true;
                bindingNavigatorWebProgressBar.Value = 0;
                bindingNavigatorWebProgressBar.Step = Math.Min(1000, dsSearchDistributionInventory.Tables["SearchResult"].Rows.Count);
                bindingNavigatorWebProgressBar.Minimum = 0;
                bindingNavigatorWebProgressBar.Maximum = dsSearchDistributionInventory.Tables["SearchResult"].Rows.Count;
                Application.DoEvents();

                int pageSize = 1000;
                int pageStart = 0;
                int pageStop = Math.Min(pageSize, dsSearchDistributionInventory.Tables["SearchResult"].Rows.Count);
                while (pageStart < dsSearchDistributionInventory.Tables["SearchResult"].Rows.Count)
                {
                    // Build the list of inventory ids for each page...
                    string inventoryIDs = ":inventoryid=";
                    for (int j = pageStart; j < pageStop; j++)
                    {
                        inventoryIDs += dsSearchDistributionInventory.Tables["SearchResult"].Rows[j]["ID"].ToString() + ",";
                    }
                    inventoryIDs = inventoryIDs.TrimEnd(new char[] { ',' });

                    // Use the inventory list to get inventory data...
                    dsFoundDistributionInventory = _sharedUtils.GetWebServiceData("order_wizard_get_inventory", inventoryIDs, 0, 0);

                    // Copy the inventory data to the datatable that will be returned...
                    distributionInventories.Merge(dsFoundDistributionInventory.Tables["order_wizard_get_inventory"].Copy());

                    // Update the paging indexes...
                    pageStart = pageStop;
                    pageStop = Math.Min((pageStart + pageSize), dsSearchDistributionInventory.Tables["SearchResult"].Rows.Count);

                    // Update the progress bar...
                    bindingNavigatorWebProgressBar.PerformStep();
                }
                // Remove the accessions where at least one distribution inventory was found...
                foreach (DataRow dr in distributionInventories.Rows)
                {
                    accessionList = accessionList.Replace(dr["accession_id"].ToString() + ",", "");
                    accessionList = accessionList.Replace(dr["accession_id"].ToString(), "");
                }
                accessionList = accessionList.TrimEnd(new char[] { ',' });
            }

            // Increment the criteria index...
            i++;

            // Hide the progress bar...
            bindingNavigatorWebProgressBarLabel.Visible = false;
            bindingNavigatorWebProgressBar.Visible = false;

            return distributionInventories;
        }

        private DataRow BuildOrderRequestItemRow(string distributionInventoryID, DataTable destinationTable, DataTable inventoryTable)
        {
            // Create a new order_request_item row...
            DataRow newOrderItem = destinationTable.NewRow();
            // Find the maximum sequence number for this order...
            int maxSequence = 1;
            if (destinationTable.DefaultView.Count > 0)
            {
                string currentSort = destinationTable.DefaultView.Sort;
                destinationTable.DefaultView.Sort = "sequence_number DESC";
                if (int.TryParse(destinationTable.DefaultView[0]["sequence_number"].ToString(), out maxSequence))
                {
                    maxSequence++;
                }
                else
                {
                    maxSequence = 1;
                }
                destinationTable.DefaultView.Sort = currentSort;
            }
            // Populate the fields not dependent on inventory_id...
            newOrderItem["order_request_id"] = ((DataRowView)_orderRequestBindingSource.Current)[_orderRequest.PrimaryKey[0].ColumnName].ToString();
            newOrderItem["sequence_number"] = maxSequence;

            // Now go get all the inventory data for the chosen distribution inventory lot...
            if (inventoryTable.Rows.Count > 0 &&
                inventoryTable.Rows.Contains(distributionInventoryID))
            {
                // Now populate the new order_request_item row with the inventory default distribution data...
                DataRow inventoryDataRow = inventoryTable.Rows.Find(distributionInventoryID);
                if (inventoryTable.Columns.Contains("accession_id")) newOrderItem["accession_id"] = inventoryDataRow["accession_id"].ToString();
                if (newOrderItem.Table.Columns.Contains("site_id") && inventoryTable.Columns.Contains("site_id") && inventoryDataRow["site_id"] != DBNull.Value) newOrderItem["site_id"] = inventoryDataRow["site_id"].ToString();
                if (newOrderItem.Table.Columns.Contains("inventory_id")) newOrderItem["inventory_id"] = distributionInventoryID;
                if (newOrderItem.Table.Columns.Contains("name") && inventoryTable.Columns.Contains("plant_name") && inventoryDataRow["plant_name"] != DBNull.Value) newOrderItem["name"] = inventoryDataRow["plant_name"].ToString();
                if (newOrderItem.Table.Columns.Contains("external_taxonomy") && inventoryTable.Columns.Contains("taxonomy_species_id") && inventoryDataRow["taxonomy_species_id"] != DBNull.Value) newOrderItem["external_taxonomy"] = _sharedUtils.GetLookupDisplayMember("taxonomy_species_lookup", inventoryDataRow["taxonomy_species_id"].ToString(), "", inventoryDataRow["taxonomy_species_id"].ToString());
                if (newOrderItem.Table.Columns.Contains("quantity_on_hand") && inventoryTable.Columns.Contains("quantity_on_hand") && inventoryDataRow["quantity_on_hand"] != DBNull.Value) newOrderItem["quantity_on_hand"] = inventoryDataRow["quantity_on_hand"].ToString();
                if (newOrderItem.Table.Columns.Contains("quantity_on_hand_unit_code") && inventoryTable.Columns.Contains("quantity_on_hand_unit_code") && inventoryDataRow["quantity_on_hand_unit_code"] != DBNull.Value) newOrderItem["quantity_on_hand_unit_code"] = inventoryDataRow["quantity_on_hand_unit_code"].ToString();
                if (newOrderItem.Table.Columns.Contains("quantity_shipped") && inventoryTable.Columns.Contains("distribution_default_quantity") && inventoryDataRow["distribution_default_quantity"] != DBNull.Value) newOrderItem["quantity_shipped"] = inventoryDataRow["distribution_default_quantity"].ToString();
                if (newOrderItem.Table.Columns.Contains("quantity_shipped_unit_code") && inventoryTable.Columns.Contains("distribution_unit_code") && inventoryDataRow["distribution_unit_code"] != DBNull.Value) newOrderItem["quantity_shipped_unit_code"] = inventoryDataRow["distribution_unit_code"].ToString();
                if (newOrderItem.Table.Columns.Contains("distribution_form_code") && inventoryTable.Columns.Contains("distribution_default_form_code") && inventoryDataRow["distribution_default_form_code"] != DBNull.Value) newOrderItem["distribution_form_code"] = inventoryDataRow["distribution_default_form_code"].ToString();
                if (newOrderItem.Table.Columns.Contains("availability_status_code") && inventoryTable.Columns.Contains("availability_status_code") && inventoryDataRow["availability_status_code"] != DBNull.Value) newOrderItem["availability_status_code"] = inventoryDataRow["availability_status_code"].ToString();
                if (newOrderItem.Table.Columns.Contains("geography_id") && inventoryTable.Columns.Contains("geography_id") && inventoryDataRow["geography_id"] != DBNull.Value) newOrderItem["geography_id"] = inventoryDataRow["geography_id"].ToString();
                if (newOrderItem.Table.Columns.Contains("is_distributable") && inventoryTable.Columns.Contains("is_distributable") && inventoryDataRow["is_distributable"] != DBNull.Value) newOrderItem["is_distributable"] = inventoryDataRow["is_distributable"].ToString();
                if (newOrderItem.Table.Columns.Contains("distribution_default_form_code") && inventoryTable.Columns.Contains("distribution_default_form_code") && inventoryDataRow["distribution_default_form_code"] != DBNull.Value) newOrderItem["distribution_default_form_code"] = inventoryDataRow["distribution_default_form_code"].ToString();
                if (newOrderItem.Table.Columns.Contains("distribution_unit_code") && inventoryTable.Columns.Contains("distribution_unit_code") && inventoryDataRow["distribution_unit_code"] != DBNull.Value) newOrderItem["distribution_unit_code"] = inventoryDataRow["distribution_unit_code"].ToString();
                //if (newOrderItem.Table.Columns.Contains("storage_location_part1") && inventoryTable.Columns.Contains("storage_location_part1") && inventoryDataRow["storage_location_part1"] != DBNull.Value) newOrderItem["storage_location_part1"] = inventoryDataRow["storage_location_part1"].ToString();
                //if (newOrderItem.Table.Columns.Contains("storage_location_part2") && inventoryTable.Columns.Contains("storage_location_part2") && inventoryDataRow["storage_location_part2"] != DBNull.Value) newOrderItem["storage_location_part2"] = inventoryDataRow["storage_location_part2"].ToString();
                //if (newOrderItem.Table.Columns.Contains("storage_location_part3") && inventoryTable.Columns.Contains("storage_location_part3") && inventoryDataRow["storage_location_part3"] != DBNull.Value) newOrderItem["storage_location_part3"] = inventoryDataRow["storage_location_part3"].ToString();
                //if (newOrderItem.Table.Columns.Contains("storage_location_part4") && inventoryTable.Columns.Contains("storage_location_part4") && inventoryDataRow["storage_location_part4"] != DBNull.Value) newOrderItem["storage_location_part4"] = inventoryDataRow["storage_location_part4"].ToString();
                if (newOrderItem.Table.Columns.Contains("status_code")) newOrderItem["status_code"] = "NEW";
                if (newOrderItem.Table.Columns.Contains("status_date")) newOrderItem["status_date"] = DateTime.Now.ToString("d");
                if (newOrderItem.Table.Columns.Contains("ipr_restriction") && inventoryTable.Columns.Contains("ipr_restriction") && inventoryDataRow["ipr_restriction"] != DBNull.Value) newOrderItem["ipr_restriction"] = inventoryDataRow["ipr_restriction"].ToString();
                if (newOrderItem.Table.Columns.Contains("xpvp_warning") && inventoryTable.Columns.Contains("xpvp_warning") && inventoryDataRow["xpvp_warning"] != DBNull.Value) newOrderItem["xpvp_warning"] = inventoryDataRow["xpvp_warning"].ToString();
                if (newOrderItem.Table.Columns.Contains("quarantine_restriction") && inventoryTable.Columns.Contains("quarantine_restriction") && inventoryDataRow["quarantine_restriction"] != DBNull.Value) newOrderItem["quarantine_restriction"] = inventoryDataRow["quarantine_restriction"].ToString();
                if (newOrderItem.Table.Columns.Contains("noxious_restriction") && inventoryTable.Columns.Contains("noxious_restriction") && inventoryDataRow["noxious_restriction"] != DBNull.Value) newOrderItem["noxious_restriction"] = inventoryDataRow["noxious_restriction"].ToString();
                if (newOrderItem.Table.Columns.Contains("smta_warning") && inventoryTable.Columns.Contains("smta_warning") && inventoryDataRow["smta_warning"] != DBNull.Value) newOrderItem["smta_warning"] = inventoryDataRow["smta_warning"].ToString();

                if (newOrderItem.Table.Columns.Contains("web_order_request") && inventoryTable.Columns.Contains("web_order_request") && inventoryDataRow["web_order_request"] != DBNull.Value) newOrderItem["web_order_request"] = inventoryDataRow["web_order_request"].ToString();
                if (newOrderItem.Table.Columns.Contains("inventory_number_part1") && inventoryTable.Columns.Contains("inventory_number_part1") && inventoryDataRow["inventory_number_part1"] != DBNull.Value) newOrderItem["inventory_number_part1"] = inventoryDataRow["inventory_number_part1"].ToString();
                if (newOrderItem.Table.Columns.Contains("inventory_number_part2") && inventoryTable.Columns.Contains("inventory_number_part2") && inventoryDataRow["inventory_number_part2"] != DBNull.Value) newOrderItem["inventory_number_part2"] = inventoryDataRow["inventory_number_part2"].ToString();
                if (newOrderItem.Table.Columns.Contains("inventory_number_part3") && inventoryTable.Columns.Contains("inventory_number_part3") && inventoryDataRow["inventory_number_part3"] != DBNull.Value) newOrderItem["inventory_number_part3"] = inventoryDataRow["inventory_number_part3"].ToString();
                if (newOrderItem.Table.Columns.Contains("inventory_maint_policy_id") && inventoryTable.Columns.Contains("inventory_maint_policy_id") && inventoryDataRow["inventory_maint_policy_id"] != DBNull.Value) newOrderItem["inventory_maint_policy_id"] = inventoryDataRow["inventory_maint_policy_id"].ToString();
                if (newOrderItem.Table.Columns.Contains("distribution_default_quantity") && inventoryTable.Columns.Contains("distribution_default_quantity") && inventoryDataRow["distribution_default_quantity"] != DBNull.Value) newOrderItem["distribution_default_quantity"] = inventoryDataRow["distribution_default_quantity"].ToString();
                if (newOrderItem.Table.Columns.Contains("policy_distribution_default_quantity") && inventoryTable.Columns.Contains("policy_distribution_default_quantity") && inventoryDataRow["policy_distribution_default_quantity"] != DBNull.Value) newOrderItem["policy_distribution_default_quantity"] = inventoryDataRow["policy_distribution_default_quantity"].ToString();
                if (newOrderItem.Table.Columns.Contains("taxonomy_species_id") && inventoryTable.Columns.Contains("taxonomy_species_id") && inventoryDataRow["taxonomy_species_id"] != DBNull.Value) newOrderItem["taxonomy_species_id"] = inventoryDataRow["taxonomy_species_id"].ToString();
                if (newOrderItem.Table.Columns.Contains("taxonomy_genus_id") && inventoryTable.Columns.Contains("taxonomy_genus_id") && inventoryDataRow["taxonomy_genus_id"] != DBNull.Value) newOrderItem["taxonomy_genus_id"] = inventoryDataRow["taxonomy_genus_id"].ToString();

                if (newOrderItem.Table.Columns.Contains("storage_location_part1") && inventoryTable.Columns.Contains("storage_location_part1") && inventoryDataRow["storage_location_part1"] != DBNull.Value) newOrderItem["storage_location_part1"] = inventoryDataRow["storage_location_part1"].ToString();
                if (newOrderItem.Table.Columns.Contains("storage_location_part2") && inventoryTable.Columns.Contains("storage_location_part2") && inventoryDataRow["storage_location_part2"] != DBNull.Value) newOrderItem["storage_location_part2"] = inventoryDataRow["storage_location_part2"].ToString();
                if (newOrderItem.Table.Columns.Contains("storage_location_part3") && inventoryTable.Columns.Contains("storage_location_part3") && inventoryDataRow["storage_location_part3"] != DBNull.Value) newOrderItem["storage_location_part3"] = inventoryDataRow["storage_location_part3"].ToString();
                if (newOrderItem.Table.Columns.Contains("storage_location_part4") && inventoryTable.Columns.Contains("storage_location_part4") && inventoryDataRow["storage_location_part4"] != DBNull.Value) newOrderItem["storage_location_part4"] = inventoryDataRow["storage_location_part4"].ToString();
                if (newOrderItem.Table.Columns.Contains("plant_name") && inventoryTable.Columns.Contains("plant_name") && inventoryDataRow["plant_name"] != DBNull.Value) newOrderItem["plant_name"] = inventoryDataRow["plant_name"].ToString();
                if (newOrderItem.Table.Columns.Contains("inv_name") && inventoryTable.Columns.Contains("inv_name") && inventoryDataRow["inv_name"] != DBNull.Value) newOrderItem["inv_name"] = inventoryDataRow["inv_name"].ToString();
                if (newOrderItem.Table.Columns.Contains("percent_viable") && inventoryTable.Columns.Contains("percent_viable") && inventoryDataRow["percent_viable"] != DBNull.Value) newOrderItem["percent_viable"] = inventoryDataRow["percent_viable"].ToString();
                if (newOrderItem.Table.Columns.Contains("tested_date") && inventoryTable.Columns.Contains("tested_date") && inventoryDataRow["tested_date"] != DBNull.Value) newOrderItem["tested_date"] = inventoryDataRow["tested_date"].ToString();

                // Next populate the sortable fields...
                foreach (DataColumn dc in newOrderItem.Table.Columns)
                {
                    if (dc.ColumnName.EndsWith("_sortable"))
                    {
                        newOrderItem[dc.ColumnName] = GetSortableColumnText(newOrderItem, dc.ColumnName.Replace("_sortable", ""));
                    }
                }
            }

            return newOrderItem;
        }

        private void ux_datagridviewOrderRequestItem_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            // Check to see if this cell is in a column that needs a FK lookup...
            DataView dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            if (dv != null && e.ColumnIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    e.RowIndex < dv.Count &&
                    dv[e.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (dv[e.RowIndex][e.ColumnIndex] != DBNull.Value)
                    {
                        e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim(), dv[e.RowIndex][e.ColumnIndex].ToString().Trim(), "", dv[e.RowIndex][e.ColumnIndex].ToString().Trim());
                    }
                    dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    e.FormattingApplied = true;
                }

                if (dc.ReadOnly)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }
            }
        }

        private void ux_datagridviewOrderRequestItem_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string errorMessage = e.Exception.Message;
            int columnWithError = -1;

            // Find the cell the error belongs to (don't use e.ColumnIndex because it points to the current cell *NOT* the offending cell)...
            foreach (DataGridViewColumn col in dgv.Columns)
            {
                if (errorMessage.Contains(col.Name))
                {
                    dgv[col.Name, e.RowIndex].ErrorText = errorMessage;
                    columnWithError = col.Index;
                }
            }
        }

        private void ux_datagridviewOrderRequestItem_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataTable dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;
            string columnName = dgv.CurrentCell.OwningColumn.Name;
            DataColumn dc = dt.Columns[columnName];
            DataRow dr;

            if (_sharedUtils.LookupTablesIsValidFKField(dc))
            {
                string luTableName = dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim();
                DataGridViewCell currentDGVCell = dgv.CurrentCell;
                dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
                string suggestedFilter = "";

                if (dc.ColumnName.Trim().ToLower() != "inventory_id")
                {
                    if (dgv.CurrentRow != null &&
                       dgv.CurrentCell.EditedFormattedValue != null)
                    {
                        suggestedFilter = dgv.CurrentCell.EditedFormattedValue.ToString();
                    }

                    if (_lastDGVCharPressed > 0) suggestedFilter = _lastDGVCharPressed.ToString();

                    GRINGlobal.Client.Common.LookupTablePicker ltp = new GRINGlobal.Client.Common.LookupTablePicker(_sharedUtils, columnName, dr, suggestedFilter);
                    ltp.StartPosition = FormStartPosition.CenterParent;
                    if (DialogResult.OK == ltp.ShowDialog())
                    {
                        if (dr != null)
                        {
                            if (ltp.NewKey != null && dr[dgv.CurrentCell.ColumnIndex].ToString().Trim() != ltp.NewKey.Trim())
                            {
                                dr[dgv.CurrentCell.ColumnIndex] = ltp.NewKey.Trim();
                                if (dgv.CurrentCell != null && currentDGVCell == dgv.CurrentCell) dgv.CurrentCell.Value = ltp.NewValue.Trim();
                            }
                            else if (ltp.NewKey == null)
                            {
                                dr[dgv.CurrentCell.ColumnIndex] = DBNull.Value;
                                if (dgv.CurrentCell != null && currentDGVCell == dgv.CurrentCell) dgv.CurrentCell.Value = "";
                            }
                            dr.SetColumnError(currentDGVCell.ColumnIndex, null);
                        }
                    }
                }
                else
                {
                    if (dgv.CurrentRow != null &&
                       dgv.CurrentRow.Cells["accession_id"].EditedFormattedValue != null)
                    {
                        suggestedFilter = dgv.CurrentRow.Cells["accession_id"].EditedFormattedValue.ToString();
                    }

                    FKeyPicker fkp = new FKeyPicker(_sharedUtils, columnName, dr, suggestedFilter);
                    fkp.StartPosition = FormStartPosition.CenterParent;
                    if (DialogResult.OK == fkp.ShowDialog())
                    {
                        if (dr != null)
                        {
                            if (fkp.NewKey != null && dr[dgv.CurrentCell.ColumnIndex].ToString().Trim() != fkp.NewKey.Trim())
                            {
                                dr[dgv.CurrentCell.ColumnIndex] = fkp.NewKey.Trim();
                                if (dgv.CurrentCell != null && currentDGVCell == dgv.CurrentCell) dgv.CurrentCell.Value = fkp.NewValue.Trim();
                            }
                            else if (fkp.NewKey == null)
                            {
                                dr[dgv.CurrentCell.ColumnIndex] = DBNull.Value;
                                if (dgv.CurrentCell != null && currentDGVCell == dgv.CurrentCell) dgv.CurrentCell.Value = "";
                            }
                            dr.SetColumnError(currentDGVCell.ColumnIndex, null);
                        }
                    }
                }

                dgv.EndEdit();
            }
        }

        #endregion

        private void ux_datagridviewOrderRequestItem_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            // Set the global variables so that later processing will know where to apply the command from the context menu...
            _mouseClickDGVColumnIndex = e.ColumnIndex;
            _mouseClickDGVRowIndex = e.RowIndex;
            DataGridView dgv = (DataGridView)sender;
            ContextMenuStrip cms = dgv.ContextMenuStrip;
            DataTable dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;

            // Change the color of the cell background so that the user
            // knows what cell the context menu applies to...
            if (e.RowIndex > -1 && e.ColumnIndex > -1)
            {
                // If the user right clicks outside of the currently selected cells - reset the selected cells to the one under the mouser cursor...
                if (!dgv.SelectedCells.Contains(dgv[e.ColumnIndex, e.RowIndex]))
                {
                    dgv.CurrentCell = dgv[e.ColumnIndex, e.RowIndex];
                }
                dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Red;
                dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionBackColor = Color.Red;
            }

            // Refresh the reports tool strip menu items...
            ux_dgvcellmenuReports.DropDownItems.Clear();
            //System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.Windows.Forms.Application.StartupPath + "\\Reports");
            System.Collections.Generic.Dictionary<string, string> reportsMap = _sharedUtils.GetReportsMapping();
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports");
            string dataviewName = ((DataTable)((BindingSource)ux_datagridviewOrderRequestItem.DataSource).DataSource).TableName.ToUpper();
            //foreach (System.IO.FileInfo fi in di.GetFiles("*.*", System.IO.SearchOption.AllDirectories))
            //{
            //    if (reportsMap.ContainsKey(fi.Name.Trim().ToUpper()) &&
            //        reportsMap[fi.Name.Trim().ToUpper()].Contains(" " + dataviewName + " "))
            //    {
            //        ToolStripItem tsi = ux_dgvcellmenuReports.DropDownItems.Add(fi.Name, null, ux_DGVCellReport_Click);
            //        tsi.Tag = fi.FullName;
            //    }
            //}
            foreach (string key in reportsMap.Keys)
            {
                if (reportsMap[key].Contains(" " + dataviewName + " ") &&
                    System.IO.File.Exists(key))
                {
                    ToolStripItem tsi = ux_dgvcellmenuReports.DropDownItems.Add(System.IO.Path.GetFileName(key), null, ux_DGVCellReport_Click);
                    tsi.Tag = key;
                }
            }
            if (ux_dgvcellmenuReports.DropDownItems.Count > 0)
            {
                ux_dgvcellmenuReports.Enabled = true;
            }
            else
            {
                ux_dgvcellmenuReports.Enabled = false;
            }

            foreach (object o in cms.Items)
            {
                if (o.GetType() == typeof(ToolStripMenuItem))
                {
                    ToolStripMenuItem tsmi = (ToolStripMenuItem)o;
                    if (tsmi.Tag != null &&
                        !string.IsNullOrEmpty(tsmi.Tag.ToString()) &&
                        dt.Columns.Contains(tsmi.Tag.ToString().Trim().ToLower()) &&
                        dt.Columns[tsmi.Tag.ToString().Trim().ToLower()].ExtendedProperties.Contains("gui_hint") &&
                        dt.Columns[tsmi.Tag.ToString().Trim().ToLower()].ExtendedProperties["gui_hint"].ToString().Trim().ToUpper() == "SMALL_SINGLE_SELECT_CONTROL" &&
                        dt.Columns[tsmi.Tag.ToString().Trim().ToLower()].ExtendedProperties.Contains("group_name") &&
                        !string.IsNullOrEmpty(dt.Columns[tsmi.Tag.ToString().Trim().ToLower()].ExtendedProperties["group_name"].ToString()))
                    {
                        tsmi.Text = dt.Columns[tsmi.Tag.ToString().Trim().ToLower()].Caption + "...";
                        tsmi.DropDownItems.Clear();
                        DataTable menuCodes = _sharedUtils.GetLocalData("SELECT * FROM code_value_lookup WHERE group_name = @groupname", "@groupname=" + dt.Columns[tsmi.Tag.ToString().Trim().ToLower()].ExtendedProperties["group_name"].ToString());
                        if (menuCodes != null &&
                            menuCodes.Rows.Count > 0)
                        {
                            foreach (DataRow dr in menuCodes.Rows)
                            {
                                ToolStripItem tsiCode = tsmi.DropDownItems.Add(dr["display_member"].ToString(), null, ux_AutoDGVColumnCode_Click);
                                tsiCode.Tag = dr["value_member"].ToString();
                            }
                        }
                    }
                }
            }
        }

        private void ux_buttonShipAllRemaining_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvr in ux_datagridviewOrderRequestItem.Rows)
            {
                if (ux_datagridviewOrderRequestItem.Columns.Contains("status_code") &&
                    ux_datagridviewOrderRequestItem.Columns.Contains("status_date") &&
                    (((DataRowView)dgvr.DataBoundItem).Row["status_code"].ToString() == "NEW" ||
                    ((DataRowView)dgvr.DataBoundItem).Row["status_code"].ToString() == "PENDING" ||
                    ((DataRowView)dgvr.DataBoundItem).Row["status_code"].ToString() == "INSPECT"))
                {
                    ((DataRowView)dgvr.DataBoundItem).Row["status_code"] = "SHIPPED";
                    ApplyOrderItemBusinessRules(dgvr);
                }
            }
            ApplyOrderBusinessRules();
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
        }

        private void ux_buttonCancelAllRemaining_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvr in ux_datagridviewOrderRequestItem.Rows)
            {
                if (ux_datagridviewOrderRequestItem.Columns.Contains("status_code") &&
                    ux_datagridviewOrderRequestItem.Columns.Contains("status_date") &&
                    (((DataRowView)dgvr.DataBoundItem).Row["status_code"].ToString() == "NEW"))
                {
                    ((DataRowView)dgvr.DataBoundItem).Row["status_code"] = "CANCEL";
                    ApplyOrderItemBusinessRules(dgvr);
                }
            }
            ApplyOrderBusinessRules();
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
        }

        private void ux_contextmenustripDGVCell_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
        }

        void ux_AutoDGVColumnCode_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            DataGridView dgv = ux_datagridviewOrderRequestItem;
            DataTable dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;

            if (tsmi.Tag.ToString().Trim().ToUpper() == "SPLIT")
            {
                SplitOrder(dgv);
                // Since the current item in the binding source may have been a new (ADDED) record
                // that had to be saved (thus getting a new PKEY) we need to update the filters for
                // the child tables attached to this order request so let's refresh the binding source...
                ux_bindingnavigatorForm.BindingSource.CurrencyManager.Refresh();
            }
            else if (tsmi.Tag.ToString().Trim().ToUpper() == "INSPECT")
            {
                // Since the current item in the binding source may have been a new (ADDED) record
                // that had to be saved (thus getting a new PKEY) we need to update the filters for
                // the child tables attached to this order request so let's refresh the binding source...
                InspectOrder(dgv);
            }
            else if (tsmi.Tag.ToString().Trim().ToUpper() == "QUALITYTEST")
            {
                // Since the current item in the binding source may have been a new (ADDED) record
                // that had to be saved (thus getting a new PKEY) we need to update the filters for
                // the child tables attached to this order request so let's refresh the binding source...
                QualityTestOrder(dgv);
            }
            else
            {
                // Iterate through the selected cells and modify the data in the code column associated with this context menu item...
                if (dgv.SelectedRows.Count > 0)
                {
                    // If the entire row was selected use it directly for processing the menu action...
                    foreach (DataGridViewRow dgvr in dgv.SelectedRows)
                    {
                        DataRow dr = ((DataRowView)dgvr.DataBoundItem).Row;
                        if (dr != null)
                        {
                            string codeColumnName = tsmi.OwnerItem.Tag.ToString().Trim().ToLower();
                            string code = tsmi.Tag.ToString().Trim().ToUpper();
                            if (!string.IsNullOrEmpty(codeColumnName) &&
                                !string.IsNullOrEmpty(code))
                            {
                                if (dt.Columns.Contains(codeColumnName)) dr[codeColumnName] = code;
                                ApplyOrderItemBusinessRules(dgvr);
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                    {
                        DataRow dr = ((DataRowView)dgvc.OwningRow.DataBoundItem).Row;
                        if (dr != null)
                        {
                            string codeColumnName = tsmi.OwnerItem.Tag.ToString().Trim().ToLower();
                            string code = tsmi.Tag.ToString().Trim().ToUpper();
                            if (!string.IsNullOrEmpty(codeColumnName) &&
                                !string.IsNullOrEmpty(code))
                            {
                                if (dt.Columns.Contains(codeColumnName)) dr[codeColumnName] = code;
                                ApplyOrderItemBusinessRules(dgvc.OwningRow);
                            }
                        }
                    }
                }
            }
            ApplyOrderBusinessRules();
            RefreshDGVFormatting(dgv);
        }

        private void SplitOrder(DataGridView dgv)
        {
            // Process for splitting an order in to two orders with only selected rows going to the new order... 
            DataRow currentOrderRequest = ((DataRowView)_orderRequestBindingSource.Current).Row;
            List<DataRow> selectedRows = new List<DataRow>();

            // Selected rows are different than selected cells - so look for both...
            if (dgv.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dgvr in dgv.SelectedRows)
                {
                    selectedRows.Add(((DataRowView)dgvr.DataBoundItem).Row);
                }
            }
            else if (dgv.SelectedCells.Count > 0)
            {
                foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                {
                    if (!selectedRows.Contains(((DataRowView)dgvc.OwningRow.DataBoundItem).Row))
                    {
                        selectedRows.Add(((DataRowView)dgvc.OwningRow.DataBoundItem).Row);
                    }
                }
            }

            // Warn the user about what they are about to do and prompt to save if the order is brand new...
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You are about to split {0} items from this order.\n\nAre you sure you want to do this?", "Split Order Confirmation", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "OrderWizard_ux_AutoDGVColumnCodeMessage1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = selectedRows.Count.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (DialogResult.Yes == ggMessageBox.ShowDialog())
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox1 = new GRINGlobal.Client.Common.GGMessageBox("You must save this order before you can split it.\n\nWould you like to do this now?", "Save Data Confirmation", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox1.Name = "OrderWizard_ux_AutoDGVColumnCodeMessage2";
                _sharedUtils.UpdateControls(ggMessageBox1.Controls, ggMessageBox1.Name);
                if ((int)currentOrderRequest[currentOrderRequest.Table.PrimaryKey[0].ColumnName] < 0 &&
                    DialogResult.Yes == ggMessageBox1.ShowDialog())
                {
                    int errorCount = SaveOrderData();
                }

                if ((int)currentOrderRequest[currentOrderRequest.Table.PrimaryKey[0].ColumnName] > 0)
                {
                    // First create a copy of the Order Request record...
                    DataRow newOrderRequest = _orderRequest.NewRow();
                    if (_orderRequest.Columns.Contains("original_order_request_id")) newOrderRequest["original_order_request_id"] = currentOrderRequest["order_request_id"];
                    if (_orderRequest.Columns.Contains("web_order_request_id")) newOrderRequest["web_order_request_id"] = currentOrderRequest["web_order_request_id"];
                    if (_orderRequest.Columns.Contains("order_type_code")) newOrderRequest["order_type_code"] = currentOrderRequest["order_type_code"];
                    if (_orderRequest.Columns.Contains("ordered_date")) newOrderRequest["ordered_date"] = currentOrderRequest["ordered_date"];
                    if (_orderRequest.Columns.Contains("intended_use_code")) newOrderRequest["intended_use_code"] = currentOrderRequest["intended_use_code"];
                    if (_orderRequest.Columns.Contains("intended_use_note")) newOrderRequest["intended_use_note"] = currentOrderRequest["intended_use_note"];
                    if (_orderRequest.Columns.Contains("requestor_cooperator_id")) newOrderRequest["requestor_cooperator_id"] = currentOrderRequest["requestor_cooperator_id"];
                    if (_orderRequest.Columns.Contains("ship_to_cooperator_id")) newOrderRequest["ship_to_cooperator_id"] = currentOrderRequest["ship_to_cooperator_id"];
                    if (_orderRequest.Columns.Contains("final_recipient_cooperator_id")) newOrderRequest["final_recipient_cooperator_id"] = currentOrderRequest["final_recipient_cooperator_id"];
                    if (_orderRequest.Columns.Contains("order_obtained_via")) newOrderRequest["order_obtained_via"] = currentOrderRequest["order_obtained_via"];
                    if (_orderRequest.Columns.Contains("special_instruction")) newOrderRequest["special_instruction"] = currentOrderRequest["special_instruction"];
                    if (_orderRequest.Columns.Contains("note")) newOrderRequest["note"] = currentOrderRequest["note"];
                    if (_orderRequest.Columns.Contains("owner_site_id")) newOrderRequest["owner_site_id"] = currentOrderRequest["owner_site_id"];
                    if (_orderRequest.Columns.Contains("web_cooperator_id")) newOrderRequest["web_cooperator_id"] = currentOrderRequest["web_cooperator_id"];
                    if (_orderRequest.Columns.Contains("email")) newOrderRequest["email"] = currentOrderRequest["email"];
                    if (_orderRequest.Columns.Contains("primary_phone")) newOrderRequest["primary_phone"] = currentOrderRequest["primary_phone"];
                    // And add it to the Order Request Table...
                    _orderRequest.Rows.Add(newOrderRequest);

                    // Now move each Order Request Item record selected to the new Order Request record...
                    foreach (DataRow dr in selectedRows)
                    {
                        if (dr.Table.Columns.Contains("order_request_id")) dr["order_request_id"] = newOrderRequest["order_request_id"];
                        //if (dr.Table.Columns.Contains("status_code")) dr["status_code"] = DBNull.Value;
                        if (dr.Table.Columns.Contains("status_code")) dr["status_code"] = "NEW";
                        if (dr.Table.Columns.Contains("status_date")) dr["status_date"] = DateTime.Now.ToString("d");
                    }

                    // Now create an Action for the original order...
                    DataRow newOrderRequestAction = _orderRequestAction.NewRow();
                    if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = currentOrderRequest["order_request_id"];
                    if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "SPLIT";
                    if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("action_information")) newOrderRequestAction["action_information"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
                    if (_orderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "Order was split by " + _cooperator_site_longname + " (" + _cooperator_site_shortname + "), " + _cooperator_city + ", " + _cooperator_geography + " by " + _cooperator_name + " (" + _cooperator_email + ")";
                    // And add it to the Order Request Action Table...
                    _orderRequestAction.Rows.Add(newOrderRequestAction);

                    // Finally create an Action for the new (split out) order...
                    newOrderRequestAction = _orderRequestAction.NewRow();
                    if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = newOrderRequest["order_request_id"];
                    if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "SPLIT";
                    if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("action_information")) newOrderRequestAction["action_information"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
                    if (_orderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "Order created by splitting items from Order: " + currentOrderRequest["order_request_id"].ToString() + " into this sub-order by " + _cooperator_site_longname + " (" + _cooperator_site_shortname + "), " + _cooperator_city + ", " + _cooperator_geography + " by " + _cooperator_name + " (" + _cooperator_email + ")";
                    // And add it to the Order Request Action Table...
                    _orderRequestAction.Rows.Add(newOrderRequestAction);
                }
            }
        }

        private void InspectOrder(DataGridView dgv)
        {
            // Process for marking an order for phyto inspection... 
            DataRow currentOrderRequest = ((DataRowView)_orderRequestBindingSource.Current).Row;
            List<DataRow> selectedRows = new List<DataRow>();

            // Selected rows are different than selected cells - so look for both...
            if (dgv.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dgvr in dgv.SelectedRows)
                {
                    selectedRows.Add(((DataRowView)dgvr.DataBoundItem).Row);
                }
            }
            else if (dgv.SelectedCells.Count > 0)
            {
                foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                {
                    if (!selectedRows.Contains(((DataRowView)dgvc.OwningRow.DataBoundItem).Row))
                    {
                        selectedRows.Add(((DataRowView)dgvc.OwningRow.DataBoundItem).Row);
                    }
                }
            }

            // Warn the user about what they are about to do and prompt to save if the order is brand new...
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You are about to send {0} items to Plant Inspection.\n\nAre you sure you want to do this?", "Plant Inspection Order Confirmation", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "OrderWizard_ux_AutoDGVColumnCodeMessage3";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = selectedRows.Count.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (DialogResult.Yes == ggMessageBox.ShowDialog())
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox1 = new GRINGlobal.Client.Common.GGMessageBox("You must save this order before you send it to Plant Inspection.\n\nWould you like to do this now?", "Save Data Confirmation", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox1.Name = "OrderWizard_ux_AutoDGVColumnCodeMessage4";
                _sharedUtils.UpdateControls(ggMessageBox1.Controls, ggMessageBox.Name);
                if ((int)currentOrderRequest[currentOrderRequest.Table.PrimaryKey[0].ColumnName] < 0 &&
                    DialogResult.Yes == ggMessageBox1.ShowDialog())
                {
                    int errorCount = SaveOrderData();
                }

                if ((int)currentOrderRequest[currentOrderRequest.Table.PrimaryKey[0].ColumnName] > 0)
                {
                    // Modify each Order Request Item record selected...
                    foreach (DataRow dr in selectedRows)
                    {
                        if (dr.Table.Columns.Contains("status_code")) dr["status_code"] = "INSPECT";
                        if (dr.Table.Columns.Contains("status_date")) dr["status_date"] = DateTime.Now.ToString("d");
                    }

                    // Finally create an Action for this order...
                    DataRow newOrderRequestAction = _orderRequestAction.NewRow();
                    if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = currentOrderRequest["order_request_id"];
                    if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "INSPECT";
                    if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("action_information")) newOrderRequestAction["action_information"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
                    if (_orderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "Order sent to Plant Inspection by " + _cooperator_site_longname + " (" + _cooperator_site_shortname + "), " + _cooperator_city + ", " + _cooperator_geography + " by " + _cooperator_name + " (" + _cooperator_email + ")";
                    // And add it to the Order Request Action Table...
                    _orderRequestAction.Rows.Add(newOrderRequestAction);
                }
            }
        }

        private void QualityTestOrder(DataGridView dgv)
        {
            // Process for duplicating an order with the duplicate order being sent to quality testing... 
            DataRow currentOrderRequest = ((DataRowView)_orderRequestBindingSource.Current).Row;
            List<DataRow> selectedRows = new List<DataRow>();

            // Selected rows are different than selected cells - so look for both...
            if (dgv.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dgvr in dgv.SelectedRows)
                {
                    selectedRows.Add(((DataRowView)dgvr.DataBoundItem).Row);
                }
            }
            else if (dgv.SelectedCells.Count > 0)
            {
                foreach (DataGridViewCell dgvc in dgv.SelectedCells)
                {
                    if (!selectedRows.Contains(((DataRowView)dgvc.OwningRow.DataBoundItem).Row))
                    {
                        selectedRows.Add(((DataRowView)dgvc.OwningRow.DataBoundItem).Row);
                    }
                }
            }

            // Warn the user about what they are about to do and prompt to save if the order is brand new...
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You are about to create a new order with copies of {0} items from this order for Quality Testing.\n\nAre you sure you want to do this?", "Quality Testing Order Confirmation", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "OrderWizard_ux_AutoDGVColumnCodeMessage5";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = selectedRows.Count.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (DialogResult.Yes == ggMessageBox.ShowDialog())
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox1 = new GRINGlobal.Client.Common.GGMessageBox("You must save this order before you can create the duplicate order for Quality Testing.\n\nWould you like to do this now?", "Save Data Confirmation", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox1.Name = "OrderWizard_ux_AutoDGVColumnCodeMessage2";
                _sharedUtils.UpdateControls(ggMessageBox1.Controls, ggMessageBox.Name);
                if ((int)currentOrderRequest[currentOrderRequest.Table.PrimaryKey[0].ColumnName] < 0 &&
                    DialogResult.Yes == ggMessageBox1.ShowDialog())
                {
                    int errorCount = SaveOrderData();
                }

                if ((int)currentOrderRequest[currentOrderRequest.Table.PrimaryKey[0].ColumnName] > 0)
                {
                    // First create a copy of the Order Request record...
                    DataRow newOrderRequest = _orderRequest.NewRow();
                    if (_orderRequest.Columns.Contains("original_order_request_id")) newOrderRequest["original_order_request_id"] = currentOrderRequest["order_request_id"];
                    if (_orderRequest.Columns.Contains("web_order_request_id")) newOrderRequest["web_order_request_id"] = DBNull.Value;
                    if (_orderRequest.Columns.Contains("order_type_code")) newOrderRequest["order_type_code"] = "PT";
                    if (_orderRequest.Columns.Contains("ordered_date")) newOrderRequest["ordered_date"] = DateTime.Now.ToString("d");
                    if (_orderRequest.Columns.Contains("intended_use_code")) newOrderRequest["intended_use_code"] = currentOrderRequest["intended_use_code"];
                    if (_orderRequest.Columns.Contains("intended_use_note")) newOrderRequest["intended_use_note"] = currentOrderRequest["intended_use_note"];
                    if (_orderRequest.Columns.Contains("requestor_cooperator_id")) newOrderRequest["requestor_cooperator_id"] = currentOrderRequest["requestor_cooperator_id"];
                    if (_orderRequest.Columns.Contains("ship_to_cooperator_id")) newOrderRequest["ship_to_cooperator_id"] = currentOrderRequest["ship_to_cooperator_id"];
                    if (_orderRequest.Columns.Contains("final_recipient_cooperator_id")) newOrderRequest["final_recipient_cooperator_id"] = currentOrderRequest["final_recipient_cooperator_id"];
                    if (_orderRequest.Columns.Contains("order_obtained_via")) newOrderRequest["order_obtained_via"] = currentOrderRequest["order_obtained_via"];
                    if (_orderRequest.Columns.Contains("special_instruction")) newOrderRequest["special_instruction"] = currentOrderRequest["special_instruction"];
                    if (_orderRequest.Columns.Contains("note")) newOrderRequest["note"] = currentOrderRequest["note"];
                    if (_orderRequest.Columns.Contains("owner_site_id")) newOrderRequest["owner_site_id"] = currentOrderRequest["owner_site_id"];
                    if (_orderRequest.Columns.Contains("web_cooperator_id")) newOrderRequest["web_cooperator_id"] = currentOrderRequest["web_cooperator_id"];
                    if (_orderRequest.Columns.Contains("email")) newOrderRequest["email"] = currentOrderRequest["email"];
                    if (_orderRequest.Columns.Contains("primary_phone")) newOrderRequest["primary_phone"] = currentOrderRequest["primary_phone"];
                    // And add it to the Order Request Table...
                    _orderRequest.Rows.Add(newOrderRequest);

                    // Now move each Order Request Item record selected to the new Order Request record...
                    int seqNum = 1;
                    foreach (DataRow dr in selectedRows)
                    {
                        DataRow newOrderRequestItem = _orderRequestItem.NewRow();
                        if (dr.Table.Columns.Contains("order_request_id")) newOrderRequestItem["order_request_id"] = newOrderRequest["order_request_id"];
                        if (dr.Table.Columns.Contains("sequence_number")) newOrderRequestItem["sequence_number"] = seqNum++;
                        if (dr.Table.Columns.Contains("name")) newOrderRequestItem["name"] = dr["name"];
                        if (dr.Table.Columns.Contains("quantity_shipped")) newOrderRequestItem["quantity_shipped"] = dr["quantity_shipped"];
                        if (dr.Table.Columns.Contains("quantity_shipped_unit_code")) newOrderRequestItem["quantity_shipped_unit_code"] = dr["quantity_shipped_unit_code"];
                        if (dr.Table.Columns.Contains("distribution_form_code")) newOrderRequestItem["distribution_form_code"] = dr["distribution_form_code"];
                        if (dr.Table.Columns.Contains("status_code")) newOrderRequestItem["status_code"] = "NEW";
                        if (dr.Table.Columns.Contains("status_date")) newOrderRequestItem["status_date"] = DateTime.Now.ToString("d");
                        if (dr.Table.Columns.Contains("inventory_id")) newOrderRequestItem["inventory_id"] = dr["inventory_id"];
                        if (dr.Table.Columns.Contains("accession_id")) newOrderRequestItem["accession_id"] = dr["accession_id"];
                        if (dr.Table.Columns.Contains("external_taxonomy")) newOrderRequestItem["external_taxonomy"] = dr["external_taxonomy"];
                        if (dr.Table.Columns.Contains("note")) newOrderRequestItem["note"] = dr["note"];
                        if (dr.Table.Columns.Contains("source_cooperator_id")) newOrderRequestItem["source_cooperator_id"] = dr["source_cooperator_id"];
                        // And add it to the Order Request Item Table...
                        _orderRequestItem.Rows.Add(newOrderRequestItem);
                    }

                    // Now create an Action for the original order...
                    DataRow origOrderRequestAction = _orderRequestAction.NewRow();
                    if (_orderRequestAction.Columns.Contains("order_request_id")) origOrderRequestAction["order_request_id"] = currentOrderRequest["order_request_id"];
                    if (_orderRequestAction.Columns.Contains("action_name_code")) origOrderRequestAction["action_name_code"] = "QUALITYTEST";
                    if (_orderRequestAction.Columns.Contains("started_date")) origOrderRequestAction["started_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("started_date_code")) origOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("completed_date")) origOrderRequestAction["completed_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("completed_date_code")) origOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("action_information")) origOrderRequestAction["action_information"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("action_cost")) origOrderRequestAction["action_cost"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("cooperator_id")) origOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
                    if (_orderRequestAction.Columns.Contains("note")) origOrderRequestAction["note"] = "Order sent to Quality Testing by " + _cooperator_site_longname + " (" + _cooperator_site_shortname + "), " + _cooperator_city + ", " + _cooperator_geography + " by " + _cooperator_name + " (" + _cooperator_email + ")";
                    // And add it to the Order Request Action Table...
                    _orderRequestAction.Rows.Add(origOrderRequestAction);

                    // Finally create an Action for the new order...
                    DataRow newOrderRequestAction = _orderRequestAction.NewRow();
                    if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = newOrderRequest["order_request_id"];
                    if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "NEW";
                    if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("action_information")) newOrderRequestAction["action_information"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
                    if (_orderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "Order created for Quality Testing by " + _cooperator_site_longname + " (" + _cooperator_site_shortname + "), " + _cooperator_city + ", " + _cooperator_geography + " by " + _cooperator_name + " (" + _cooperator_email + ")";
                    // And add it to the Order Request Action Table...
                    _orderRequestAction.Rows.Add(newOrderRequestAction);
                }
            }
        }

        void ux_DGVCellReport_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            DataGridView dgv = ux_datagridviewOrderRequestItem;
            DataTable dtSelectedRows = ((DataTable)((BindingSource)dgv.DataSource).DataSource).Clone();

            // NOTE: because of the way the DGV adds rows to the selectedRows collection
            //       we have to process the rows in the opposite direction they were selected in...
            int rowStart = 0;
            int rowStop = dgv.SelectedRows.Count;
            int stepValue = 1;
            if (dgv.SelectedRows.Count > 1 && dgv.SelectedRows[0].Index > dgv.SelectedRows[1].Index)
            {
                rowStart = dgv.SelectedRows.Count - 1;
                rowStop = -1;
                stepValue = -1;
            }

            DataGridViewRow dgvrow = null;
            if (dgv.SelectedRows.Count > 0)
            {
                // Process the selected rows in the opposite direction they were selected by the user...
                for (int i = rowStart; i != rowStop; i += stepValue)
                {
                    dgvrow = dgv.SelectedRows[i];
                    if (!dgvrow.IsNewRow)
                    {
                        dtSelectedRows.Rows.Add(((DataRowView)dgvrow.DataBoundItem).Row.ItemArray);
                    }
                }
            }
            else
            {
                // No row was selected (aka full row selection via row header) so presume user wants only current row selected...
                dgvrow = dgv.CurrentRow;
                dtSelectedRows.Rows.Add(((DataRowView)dgvrow.DataBoundItem).Row.ItemArray);
            }

            string fullPathName = tsmi.Tag.ToString();
            DataTable dtReport = new DataTable();
            // Make sure the table being passed in is the preferred one for the selected Crystal Report...
            System.Collections.Generic.Dictionary<string, string> reportsMap = _sharedUtils.GetReportsMapping();
            string PreferredDataviewName = "";
            if (reportsMap.ContainsKey(tsmi.Text.ToUpper()))
            {
                string dataviewNames = reportsMap[tsmi.Text.ToUpper()];
                if (dataviewNames.Length > 0)
                {
                    PreferredDataviewName = dataviewNames.Split('|')[0].Trim().ToLower();
                }
            }
            if (dtSelectedRows.TableName.ToLower() != PreferredDataviewName &&
                dtSelectedRows.Rows.Count > 0)
            {
                // If we are here then the datatable dtSelectedRows is not the preferred datatable so go 
                // get the preferred datatable now (based on the order_request_id) and filter to only the selected rows...
                string order_request_id = dtSelectedRows.Rows[0]["order_request_id"].ToString();
                DataSet dsPreferred = _sharedUtils.GetWebServiceData(PreferredDataviewName, ":orderrequestid=" + order_request_id, 0, 1000000);
                if (dsPreferred.Tables.Contains(PreferredDataviewName) &&
                    dsPreferred.Tables[PreferredDataviewName].Rows.Count > 0 &&
                    dtSelectedRows.Columns.Contains(dsPreferred.Tables[PreferredDataviewName].PrimaryKey[0].ColumnName))
                {
                    dtReport = dsPreferred.Tables[PreferredDataviewName].Clone();
                    string primaryKeyName = dsPreferred.Tables[PreferredDataviewName].PrimaryKey[0].ColumnName;
                    foreach (DataRow dr in dtSelectedRows.Rows)
                    {
                        // Attempt to find the row in the preferred datatable that matches the row in the selected datatable...
                        DataRow preferredRow = dsPreferred.Tables[PreferredDataviewName].Rows.Find(dr[primaryKeyName]);
                        if (preferredRow != null)
                        {
                            dtReport.Rows.Add(preferredRow.ItemArray);
                        }
                    }
                }
            }
            else
            {
                dtReport = dtSelectedRows;
            }

            // Make sure the Crystal Reports exists at the file path specified and if so process the print request...
            if (System.IO.File.Exists(fullPathName))
            {
                DataGridView dgvReport = new DataGridView();
                _sharedUtils.BuildReadOnlyDataGridView(dgvReport, dtReport);
                if (dgvReport != null &&
                    dgvReport.DataSource != null &&
                    dgvReport.DataSource.GetType() == typeof(DataTable))
                {
                    // Okay it looks like we have a datagridview with a datasource = datatable (with FKeys and code_values resolved) - so extract it and use in the report...
                    DataTable dt = (DataTable)dgvReport.DataSource;
                    // Got the data so now we can generate the Crystal Report...
                    _sharedUtils.PrintReport(dt, fullPathName);
                }
            }
        }

        private void ApplyOrderItemBusinessRules(DataGridViewRow dgvr)
        {
            DataGridView dgv = dgvr.DataGridView;
            DataRow dr = ((DataRowView)dgvr.DataBoundItem).Row;
            dr.EndEdit();
            // If this is an order_request_item row and it has changes - apply business rules to harmonize data within the row...
            if (dr.Table.TableName == "order_wizard_get_order_request_item")
            {
                // If the inventory_id changed for this order_request_item...
                if ((dr.RowState == DataRowState.Added && !dr["inventory_id", DataRowVersion.Default].Equals(dr["inventory_id", DataRowVersion.Current])) ||
                    (dr.RowState == DataRowState.Modified && !dr["inventory_id", DataRowVersion.Original].Equals(dr["inventory_id", DataRowVersion.Current])))
                {
                    DataSet ds = _sharedUtils.GetWebServiceData("order_wizard_get_inventory", ":inventoryid=" + dr["inventory_id", DataRowVersion.Current], 0, 0);
                    if (ds != null &&
                        ds.Tables.Contains("order_wizard_get_inventory") &&
                        ds.Tables["order_wizard_get_inventory"].Rows.Count > 0)
                    {
                        DataRow invRow = ds.Tables["order_wizard_get_inventory"].Rows.Find(dr["inventory_id"]);
                        // If the inventory row was found - copy all data that has matching column names AND is different...
                        if (invRow != null)
                        {
                            foreach (DataColumn dc in dr.Table.Columns)
                            {
                                syncDataRow(invRow, dc.ColumnName, dr, dc.ColumnName);
                            }
                            // Now manually sync data for mismatched column names...
                            syncDataRow(invRow, "plant_name", dr, "name");
                            syncDataRow(invRow, "taxonomy_species_id", dr, "external_taxonomy");
                            syncDataRow(invRow, "distribution_default_quantity", dr, "quantity_shipped");
                            syncDataRow(invRow, "distribution_unit_code", dr, "quantity_shipped_unit_code");
                            syncDataRow(invRow, "distribution_default_form_code", dr, "distribution_form_code");
                        }
                    }
                }
                // If an order item's status has changed...
                if (dr.RowState == DataRowState.Added ||
                (dr.RowState == DataRowState.Modified && !dr["status_code", DataRowVersion.Original].Equals(dr["status_code", DataRowVersion.Current])))
                {
                    // Update the status date...
                    if (dr.RowState == DataRowState.Modified && dr.Table.Columns.Contains("status_date") &&
                        dr["status_date", DataRowVersion.Original].Equals(dr["status_date", DataRowVersion.Current]) &&
                        (dr["status_date", DataRowVersion.Original] == DBNull.Value ||
                        ((DateTime)dr["status_date", DataRowVersion.Current]).ToString("d").Trim().ToUpper() != DateTime.Now.ToString("d").Trim().ToUpper()))
                    {
                        dr["status_date"] = DateTime.Now.ToString("d");
                        if (dr.Table.Columns.Contains("status_date_sortable")) dr["status_date_sortable"] = GetSortableColumnText(dr, "status_date");
                    }

                    // If the item is being canceled - zero the quantity_shipped field...
                    if (dr.Table.Columns.Contains("status_code") && dr.Table.Columns.Contains("quantity_shipped") && 
                        dr["status_code"].ToString().ToUpper() == "CANCEL")
                    {
                        dr["quantity_shipped"] = 0;
                        if (dr.Table.Columns.Contains("quantity_shipped_sortable")) dr["quantity_shipped_sortable"] = GetSortableColumnText(dr, "quantity_shipped");
                    }
                }
            }
        }

        private void ApplyOrderBusinessRules()
        {
            // Get the current order request record...
            DataRowView drv = ((DataRowView)_orderRequestBindingSource.Current);
            _orderRequestBindingSource.EndEdit();
            _orderRequestItemBindingSource.EndEdit();

            // Check to see if order items need an SMTA but there is no evidence of the 
            // user accepting the SMTA
            if (!IsOrderSMTACompliant(drv) &&
                ux_checkboxSMTACheck.Checked)
            {
                //MessageBox.Show("Current business rules indicate that acceptance of the SMTA is required for this order.  There is no record in the Order Action table indicating that SMTA acceptance has been completed.", "SMTA Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MessageBox.Show("No SMTA acceptance documented in order actions.\r\n\r\nAdditional Information: Secure and attach SMTA acceptance from the requestor to this order prior to fulfillment of items with an SMTA.", "SMTA Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            // Now check to see if all items in this order are either marked as 'SHIPPED' or 'CANCEL'
            // and if so set the order request header's completed_date...
            bool orderComplete = true;
            foreach (DataRowView drvItem in _orderRequestItem.DefaultView)
            {
                if (drvItem["status_code"].ToString().Trim().ToUpper() != "SHIPPED" &&
                    drvItem["status_code"].ToString().Trim().ToUpper() != "CANCEL") orderComplete = false;
            }

            if (orderComplete && _orderRequestItem.DefaultView.Count > 0)
            {
                if (drv != null)
                {
                    // Mark the order_request completed_date to reflect the order is completed...
                    drv["completed_date"] = DateTime.Now.ToString("d");

                    // Put an order_request_action in to document the order is completed...
                    // Create an Action for the status_code changes (one per staus_code with a count for how many items were changed to that status)...
                    DataRow newOrderRequestAction = _orderRequestAction.NewRow();
                    if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = drv["order_request_id"];
                    if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "DONE";
                    if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
                    if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
                    if (_orderRequestAction.Columns.Contains("action_information")) newOrderRequestAction["action_information"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
                    //if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = DBNull.Value;
                    if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
                    //if (_orderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "Order Request has been marked as COMPLETED by " + _sharedUtils.Username;
                    if (_orderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "Order Request has been marked as COMPLETED by " + _cooperator_site_longname + " (" + _cooperator_site_shortname + "), " + _cooperator_city + ", " + _cooperator_geography + " by " + _cooperator_name + " (" + _cooperator_email + ")";
                    // And add it to the Order Request Action Table...
                    // Check to see if this row already has been added in this session...
                    DataRow[] dupActionRecords = _orderRequestAction.Select("order_request_id=" + drv["order_request_id"].ToString() + " AND action_name_code='DONE'");
                    if (dupActionRecords.Length == 0) _orderRequestAction.Rows.Add(newOrderRequestAction);
                }
            }
            else
            {
                if (drv != null)
                {
                    // Mark the order_request completed_date to reflect the order is not completed...
                    if (drv["completed_date"] != DBNull.Value)
                    {
                        drv["completed_date"] = DBNull.Value;
                    }
                }
            }
        }

        private bool IsOrderSMTACompliant(DataRowView drv)
        {
            bool orderIsCompliant = false;

            // Phase 1 - if SMTA acceptance is on record - nothing else matters...
            foreach (DataRowView actionDRV in _orderRequestAction.DefaultView)
            {
                if (actionDRV["action_name_code"].ToString().ToUpper() == "SMTAACCEPT" &&
                    actionDRV["order_request_id"].ToString().Trim() == drv["order_request_id"].ToString().Trim()) orderIsCompliant = true;
            }

            // Phase 2 - if accession has no SMTA IPR and the ship-to geography_id
            //           is on the geography_id exceptions list...
            if (!orderIsCompliant)
            {
                string shipToCooperator = drv["ship_to_cooperator_id"].ToString().Trim();
                string finalRecipientCooperator = drv["final_recipient_cooperator_id"].ToString().Trim();
                if (!string.IsNullOrEmpty(finalRecipientCooperator))
                {
                    DataSet ds = _sharedUtils.GetWebServiceData("get_cooperator", ":cooperatorid=" + finalRecipientCooperator + ";", 0, 0);
                    if (ds != null &&
                        ds.Tables.Contains("get_cooperator") &&
                        ds.Tables["get_cooperator"].Rows.Count > 0 &&
                        ds.Tables["get_cooperator"].Columns.Contains("geography_id") &&
                        ds.Tables["get_cooperator"].Rows[0]["geography_id"] != DBNull.Value)
                    {
                        int geography_id = (int)ds.Tables["get_cooperator"].Rows[0]["geography_id"];
                        // See if there are geographyIDs that do not hve to follow the 'All Accessions Must Have SMTA Agreement' rule...
                        // (for example in USA all accessions shipped to domestic/USA geographyIDs do not have to have an SMTA agreement unless
                        // the accession was aquired with an SMTA agreement)
                        if (_geoExceptionsList.Contains(geography_id) &&
                            _orderRequestItem.Columns.Contains("smta_warning") &&
                            _orderRequestItem.Columns.Contains("status_code"))
                        {
                            int smtaAccessionCount = 0;
                            foreach (DataRowView itemDRV in _orderRequestItem.DefaultView)
                            {
                                if (!string.IsNullOrEmpty(itemDRV["smta_warning"].ToString().Trim()) &&
                                    itemDRV["status_code"].ToString().Trim().ToUpper() != "NEW" &&
                                    itemDRV["status_code"].ToString().Trim().ToUpper() != "CANCEL")
                                {
                                    smtaAccessionCount++;
                                }
                            }
                            if (smtaAccessionCount == 0) orderIsCompliant = true;
                        }
                        else if (_orderRequestItem.Columns.Contains("status_code"))
                        {
                            int accessionCount = 0;
                            foreach (DataRowView itemDRV in _orderRequestItem.DefaultView)
                            {
                                // The final destination geographyID is not on the execption list - so any accession
                                // that is not CANCELLED or is still unhandled (aka 'NEW') - should be considered out of SMTA compliance...
                                if (itemDRV["status_code"].ToString().Trim().ToUpper() != "NEW" &&
                                itemDRV["status_code"].ToString().Trim().ToUpper() != "CANCEL")
                                {
                                    accessionCount++;
                                }
                            }
                            if (accessionCount == 0) orderIsCompliant = true;
                        }
                    }
                }
                else
                {
                    // The final recipient field is still null so this order is not completely built yet
                    // so suppress the SMTA warning until we at least know where the order is going...
                    orderIsCompliant = true;
                }
            }
            return orderIsCompliant;
        }

        private void RefreshDGVFormatting(DataGridView dgv)
        {
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                RefreshDGVRowFormatting(dgvr);
            }

            if (OrderItemsContainAlerts())
            {
                ux_labelOrderItemAlertFlag.Visible = true;
            }
            else
            {
                ux_labelOrderItemAlertFlag.Visible = false;
            }

            if (OrderItemsContainDuplicates())
            {
                ux_labelOrderItemDuplicateFlag.Visible = true;
                DataTable dt = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView.ToTable();
                if (dt.Columns.Contains("inventory_id"))
                {
                    foreach (DataGridViewRow dgvr in dgv.Rows)
                    {
                        dt.DefaultView.RowFilter = "inventory_id=" + ((DataRowView)dgvr.DataBoundItem).Row["inventory_id"].ToString();
                        if (dt.DefaultView.Count > 1)
                        {
                            dgvr.DefaultCellStyle.BackColor = Color.Orange;
                        }
                    }
                }
            }
            else
            {
                ux_labelOrderItemDuplicateFlag.Visible = false;
            }

            // Show SortGlyphs for the column headers (this takes two steps)...
            // First reset them all to No Sort...
            foreach (DataGridViewColumn dgvc in dgv.Columns)
            {
                dgvc.HeaderCell.SortGlyphDirection = SortOrder.None;
            }
            // Now inspect the sort string from the datatable in use to set the SortGlyphs...
            string strOrder = "";
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                strOrder = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView.Sort;
            }
            else
            {
                strOrder = ((DataTable)dgv.DataSource).DefaultView.Sort;
            }
            char[] chararrDelimiters = { ',' };
            string[] strarrSortCols = strOrder.Split(chararrDelimiters);
            foreach (string strSortCol in strarrSortCols)
            {
                if (strSortCol.Contains("ASC"))
                {
                    if (dgv.Columns.Contains(strSortCol.Replace(" ASC", "").Replace("_sortable", "").Trim())) dgv.Columns[strSortCol.Replace(" ASC", "").Replace("_sortable", "").Trim()].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                }
                if (strSortCol.Contains("DESC"))
                {
                    if (dgv.Columns.Contains(strSortCol.Replace(" DESC", "").Replace("_sortable", "").Trim())) dgv.Columns[strSortCol.Replace(" DESC", "").Replace("_sortable", "").Trim()].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                }
            }
        }

        private void RefreshDGVRowFormatting(DataGridViewRow dgvr)
        {
            foreach (DataGridViewCell dgvc in dgvr.Cells)
            {
                // Reset the background and foreground color...
                dgvc.Style.BackColor = Color.Empty;
                dgvc.Style.ForeColor = Color.Empty;
                dgvc.Style.SelectionBackColor = Color.Empty;
                dgvc.Style.SelectionForeColor = Color.Empty;
            }

            // Reset the row's default background color...
            dgvr.DefaultCellStyle.BackColor = Color.Empty;

            // Visually flag IPR items in the order...
            if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("ipr_restriction") && !string.IsNullOrEmpty(dgvr.Cells["ipr_restriction"].Value.ToString()))
            {
                dgvr.DefaultCellStyle.BackColor = Color.SkyBlue;
            }
            if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("xpvp_warning") && !string.IsNullOrEmpty(dgvr.Cells["xpvp_warning"].Value.ToString()))
            {
                dgvr.DefaultCellStyle.BackColor = Color.SkyBlue;
            }
            if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("quarantine_restriction") && !string.IsNullOrEmpty(dgvr.Cells["quarantine_restriction"].Value.ToString()))
            {
                dgvr.DefaultCellStyle.BackColor = Color.SkyBlue;
            }
            if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("noxious_restriction") && !string.IsNullOrEmpty(dgvr.Cells["noxious_restriction"].Value.ToString()))
            {
                dgvr.DefaultCellStyle.BackColor = Color.SkyBlue;
            }
            if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("smta_warning") && !string.IsNullOrEmpty(dgvr.Cells["smta_warning"].Value.ToString()))
            {
                dgvr.DefaultCellStyle.BackColor = Color.Red;
            }

            // Get the datatable row for data processing...
            DataRow dr = ((DataRowView)dgvr.DataBoundItem).Row;

            // If the row has changes make each changed cell yellow...
            if (dr.RowState == DataRowState.Modified)
            {
                foreach (DataGridViewCell dgvc in dgvr.Cells)
                {
                    string dcName = dgvc.OwningColumn.Name;
                    // If the cell has been changed make it yellow...
                    if (dr[dcName, DataRowVersion.Original].ToString().Trim() != dr[dcName, DataRowVersion.Current].ToString().Trim())
                    {
                        dgvc.Style.BackColor = Color.Yellow;
                        dr.SetColumnError(dcName, null);
                    }
                }
            }

            // If an order item's shipped amount has changed warn the user if the shipped amount exceeds on hand amount...
            if (dr.Table.Columns.Contains("quantity_on_hand") &&
                dr.Table.Columns.Contains("quantity_on_hand_unit_code") &&
                dr.Table.Columns.Contains("quantity_shipped") &&
                dr.Table.Columns.Contains("quantity_shipped_unit_code"))
            {
                if (dgvr.Cells["quantity_on_hand_unit_code"].Value.ToString().Trim().ToUpper() == dgvr.Cells["quantity_shipped_unit_code"].Value.ToString().Trim().ToUpper())
                {
                    double quantity_on_hand = -1.0;
                    double quantity_shipped = -1.0;
                    if (!double.TryParse(dgvr.Cells["quantity_on_hand"].Value.ToString(), out quantity_on_hand))
                    {
                        dgvr.DefaultCellStyle.BackColor = Color.SkyBlue;
                        dgvr.Cells["quantity_on_hand"].Style.BackColor = Color.Red;
                        dr.SetColumnError("quantity_on_hand", "Quantity on hand amount missing or invalid");
                        quantity_on_hand = -1.0;
                    }
                    else if (!double.TryParse(dgvr.Cells["quantity_shipped"].Value.ToString(), out quantity_shipped))
                    {
                        dgvr.DefaultCellStyle.BackColor = Color.SkyBlue;
                        dgvr.Cells["quantity_shipped"].Style.BackColor = Color.Red;
                        dr.SetColumnError("quantity_shipped", "Quantity shippped amount missing or invalid");
                        quantity_shipped = -1.0;
                    }
                    else if (quantity_on_hand - quantity_shipped < 0.0)
                    {
                        dgvr.DefaultCellStyle.BackColor = Color.SkyBlue;
                        dgvr.Cells["quantity_shipped"].Style.BackColor = Color.Red;
                        dr.SetColumnError("quantity_shipped", "Shipped amount exceeds on-hand amount");
                    }
                    else
                    {
                        if (dr.GetColumnError("quantity_shipped") == "Shipped amount exceeds on-hand amount" ||
                            dr.GetColumnError("quantity_shipped") == "Quantity shippped amount missing or invalid")
                        {
                            dr.SetColumnError("quantity_shipped", null);
                        }
                        if (dr.GetColumnError("quantity_on_hand") == "Quantity on hand amount missing or invalid")
                        {
                            dr.SetColumnError("quantity_on_hand", null);
                        }
                    }
                }
            }

            //// Visually flag duplicate rows...
            //if (((DataRowView)dgvr.DataBoundItem).Row.Table.Columns.Contains("inventory_id") && !string.IsNullOrEmpty(((DataRowView)dgvr.DataBoundItem).Row["inventory_id"].ToString()) &&
            //    ((DataRowView)dgvr.DataBoundItem).Row.Table.DefaultView.ToTable().Select("inventory_id=" + ((DataRowView)dgvr.DataBoundItem).Row["inventory_id"].ToString()).Length > 1)
            //{
            //    dgvr.DefaultCellStyle.BackColor = Color.Orange;
            //}
        }

        private void ux_datagridview_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else if (dgv.DataSource.GetType() == typeof(DataTable))
            {
                dv = ((DataTable)(dgv.DataSource)).DefaultView;
            }

            // Reset the last char pressed global variable...
            _lastDGVCharPressed = (char)0;

            // If we are: 
            //  1) in edit mode, 
            //  2) the current cell is parked on a cell that is a FK lookup 
            //  3) and the Alt and Ctrl keys are not down
            // Then remember the keypress so that it can be passed into the Lookup Picker dialog...
            if (dv != null &&
                dgv.CurrentCell != null &&
                dgv.CurrentCell.ColumnIndex > -1 &&
                dgv.CurrentCell.RowIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[dgv.CurrentCell.ColumnIndex];
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    dgv.CurrentCell.RowIndex < dv.Count &&
                    dv[dgv.CurrentCell.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (!e.Alt && !e.Control)
                    {
                        KeysConverter kc = new KeysConverter();
                        string lastChar = kc.ConvertToString(e.KeyValue).Replace("NumPad", "");
                        if (lastChar.Length == 1)
                        {
                            if (e.Shift)
                            {
                                _lastDGVCharPressed = lastChar.ToUpper()[0];
                            }
                            else
                            {
                                _lastDGVCharPressed = lastChar.ToLower()[0];
                            }
                        }
                    }
                }
            }
        }

        private void ux_datagridview_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            bool pastedNewOrderRequestItems = false;

            //_sharedUtils.ProcessDGVEditShortcutKeys(dgv, e, _sharedUtils.UserCooperatorID, ImportTextToDataTableUsingAltKeys, ImportTextToDataTableUsingBlockStyle);

            // If the key press is "CTRL+V" we need to check to see if the values on the clipboard
            // are a list of accession numbers and if so import them properly by doing an end-run around 
            // default key press procesing that simply pastes values directly into matching row/columns in the dgv...
            if (e.KeyCode == Keys.V && e.Control)
            {
                IDataObject dataObj = Clipboard.GetDataObject();
                string pasteText = "";
                if (dataObj.GetDataPresent(System.Windows.Forms.DataFormats.UnicodeText))
                {
                    pasteText = dataObj.GetData(System.Windows.Forms.DataFormats.UnicodeText).ToString();
                    DataTable dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;
                    char[] rowDelimiters = new char[] { '\r', '\n' };
                    char[] columnDelimiters = new char[] { '\t' };
                    int badRows = 0;
                    int missingRows = 0;
                    pastedNewOrderRequestItems = ImportTextToDataTableUsingAltKeys(pasteText, dt, rowDelimiters, columnDelimiters, out badRows, out missingRows);
                }
            }

            // If the key press is "CTRL+V" and the pasted text was a list of accesion numbers OR
            // the key press was handled using the default keyboard processing logic (but it was not
            // a CTRL+C or CTRL+E press) then apply the business logic to each item and the order request...
            if (pastedNewOrderRequestItems || 
                (_sharedUtils.ProcessDGVEditShortcutKeys(dgv, e, _sharedUtils.UserCooperatorID) &&
                !(e.KeyCode == Keys.C && e.Control) &&
                !(e.KeyCode == Keys.E && e.Control)))
            {
                foreach (DataGridViewRow dgvr in dgv.Rows)
                {
                    ApplyOrderItemBusinessRules(dgvr);
                }
                ApplyOrderBusinessRules();
                RefreshDGVFormatting(dgv);
            }
        }

        private void ux_radiobuttonOrderFilter_CheckedChanged(object sender, EventArgs e)
        {
            // This event is fired for radio buttons that are being checked and unchecked
            // so ignore the event for radio buttons that are being unchecked...
            if (((RadioButton)sender).Checked)
            {
                if (ux_tabcontrolMain.SelectedTab == OrdersPage || ux_tabcontrolMain.SelectedTab == ActionsPage)
                {
                    if (ux_radiobuttonSelectionOrders.Checked)
                    {
                        //ux_textboxSelection.Size = ux_checkedlistboxOrderItemStatus.Size;
                        //ux_textboxSelection.Location = ux_checkedlistboxOrderItemStatus.Location;
                        ux_textboxSelection.Text = _selectionList;
                        ux_textboxSelection.Enabled = true;
                        ux_textboxSelection.Visible = true;
                        ux_checkedlistboxOrderItemStatus.Enabled = false;
                        ux_checkedlistboxOrderItemStatus.Visible = false;
                        ux_textboxOrderDateFilter.Enabled = false;
                        ux_textboxOrderDateFilter.Visible = false;
                        ux_comboboxOrderTypeFilter.Enabled = false;
                        ux_comboboxOrderTypeFilter.Visible = false;
                        ux_labelOrderDateFilter.Visible = false;
                        ux_labelOrderTypeFilter.Visible = false;
                        ux_labelItemStatus.Visible = false;
                        ux_checkboxIsOrderNumber.Visible = true;
                        ux_checkboxIsWebOrderNumber.Visible = true;
                        ux_checkboxIsLocalNumber.Visible = true;
                    }
                    else
                    {
                        ux_textboxSelection.Enabled = false;
                        ux_textboxSelection.Visible = false;
                        ux_checkedlistboxOrderItemStatus.Enabled = true;
                        ux_checkedlistboxOrderItemStatus.Visible = true;
                        ux_textboxOrderDateFilter.Enabled = true;
                        ux_textboxOrderDateFilter.Visible = true;
                        ux_comboboxOrderTypeFilter.Enabled = true;
                        ux_comboboxOrderTypeFilter.Visible = true;
                        ux_labelOrderDateFilter.Visible = true;
                        ux_labelOrderTypeFilter.Visible = true;
                        ux_labelItemStatus.Visible = true;
                        ux_checkboxIsOrderNumber.Visible = false;
                        ux_checkboxIsWebOrderNumber.Visible = false;
                        ux_checkboxIsLocalNumber.Visible = false;
                    }
                }
                if (ux_tabcontrolMain.SelectedTab == WebOrderPage)
                {
                    if (ux_radiobuttonSelectionWebOrders.Checked)
                    {
                        ux_textboxWebSelection.Size = ux_checkedlistboxWebOrderItemStatus.Size;
                        ux_textboxWebSelection.Location = ux_checkedlistboxWebOrderItemStatus.Location;
                        ux_textboxWebSelection.Enabled = true;
                        ux_textboxWebSelection.Visible = true;
                        ux_checkedlistboxWebOrderItemStatus.Enabled = false;
                        ux_checkedlistboxWebOrderItemStatus.Visible = false;
                        ux_textboxWebOrderDateFilter.Enabled = false;
                        ux_textboxWebOrderDateFilter.Visible = false;
                        ux_labelWebOrderDateFilter.Visible = false;
                        ux_labelWebOrderStatus.Visible = false;
                    }
                    else
                    {
                        ux_textboxWebSelection.Enabled = false;
                        ux_textboxWebSelection.Visible = false;
                        ux_checkedlistboxWebOrderItemStatus.Enabled = true;
                        ux_checkedlistboxWebOrderItemStatus.Visible = true;
                        ux_textboxWebOrderDateFilter.Enabled = true;
                        ux_textboxWebOrderDateFilter.Visible = true;
                        ux_labelWebOrderDateFilter.Visible = true;
                        ux_labelWebOrderStatus.Visible = true;
                    }
                    RefreshWebOrderData();
                }
            }
        }

        private void ux_checkedlistboxOrderItemStatus_ItemCheck(object sender, ItemCheckEventArgs e)
        {
        }

        private void ux_buttonFindWebOrders_Click(object sender, EventArgs e)
        {
            // If the Order tab is active, refresh the Order data...
            if (ux_tabcontrolMain.SelectedTab == OrdersPage || ux_tabcontrolMain.SelectedTab == ActionsPage) RefreshOrderData();
            // If the Web Order tab is active, refresh the Web Order data...
            if (ux_tabcontrolMain.SelectedTab == WebOrderPage) RefreshWebOrderData();
        }

        private void ux_buttonCreateOrderRequest_Click(object sender, EventArgs e)
        {
            // If there is no active row in the web_order_request DGV - bail out now...
            if (ux_bindingNavigatorWebOrders.BindingSource.Current == null) return;

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Freeze the interface to prevent async user events causing problems...
            ux_tabcontrolMain.Enabled = false;

            // Create a new order_request record...
            DataRow newOrderRequest = ((DataTable)ux_bindingnavigatorForm.BindingSource.DataSource).NewRow();
            ((DataTable)ux_bindingnavigatorForm.BindingSource.DataSource).Rows.Add(newOrderRequest);
            // Move to it in the DGV...
            ux_bindingnavigatorForm.BindingSource.MoveLast();

            // Populate the new order_request record with data from the web_order_request record...
            DataRow webOrderRequest = ((DataRowView)ux_bindingNavigatorWebOrders.BindingSource.Current).Row;
            if (newOrderRequest.Table.Columns.Contains("ordered_date") && webOrderRequest.Table.Columns.Contains("ordered_date")) newOrderRequest["ordered_date"] = DateTime.Parse(webOrderRequest["ordered_date"].ToString()).ToString("d");
            if (newOrderRequest.Table.Columns.Contains("order_type_code") && webOrderRequest.Table.Columns.Contains("intended_use_code"))
            {
                string orderType = "DI";
                switch (webOrderRequest["intended_use_code"].ToString().Trim().ToUpper())
                {
                    case "HOME":
                        orderType = "NR";
                        break;
                    case "REPATRIATION":
                        orderType = "RP";
                        break;
                    case "RESEARCH":
                    case "EDUCATION":
                    case "OTHER":
                        orderType = "DI";
                        break;
                    default:
                        orderType = "DI";
                        break;
                }
                newOrderRequest["order_type_code"] = orderType;
            }
            if (newOrderRequest.Table.Columns.Contains("order_obtained_via")) newOrderRequest["order_obtained_via"] = "Web Order";
            if (newOrderRequest.Table.Columns.Contains("intended_use_code") && webOrderRequest.Table.Columns.Contains("intended_use_code")) newOrderRequest["intended_use_code"] = webOrderRequest["intended_use_code"];
            if (newOrderRequest.Table.Columns.Contains("intended_use_note") && webOrderRequest.Table.Columns.Contains("intended_use_note")) newOrderRequest["intended_use_note"] = webOrderRequest["intended_use_note"];
            if (newOrderRequest.Table.Columns.Contains("web_order_request_id") && webOrderRequest.Table.Columns.Contains("web_order_request_id")) newOrderRequest["web_order_request_id"] = webOrderRequest["web_order_request_id"];
            if (newOrderRequest.Table.Columns.Contains("web_cooperator_id") && webOrderRequest.Table.Columns.Contains("web_cooperator_id")) newOrderRequest["web_cooperator_id"] = webOrderRequest["web_cooperator_id"];
            if (newOrderRequest.Table.Columns.Contains("email") && webOrderRequest.Table.Columns.Contains("email")) newOrderRequest["email"] = webOrderRequest["email"];
            if (newOrderRequest.Table.Columns.Contains("primary_phone") && webOrderRequest.Table.Columns.Contains("primary_phone")) newOrderRequest["primary_phone"] = webOrderRequest["primary_phone"];
            if (newOrderRequest.Table.Columns.Contains("note") && webOrderRequest.Table.Columns.Contains("note")) newOrderRequest["note"] = webOrderRequest["note"];
            if (newOrderRequest.Table.Columns.Contains("note") && webOrderRequest.Table.Columns.Contains("web_cooperator_note")) newOrderRequest["note"] = newOrderRequest["note"].ToString() + webOrderRequest["web_cooperator_note"].ToString();
            if (newOrderRequest.Table.Columns.Contains("special_instruction") && webOrderRequest.Table.Columns.Contains("special_instruction")) newOrderRequest["special_instruction"] = webOrderRequest["special_instruction"];

            if (webOrderRequest.Table.Columns.Contains("last_name") &&
                webOrderRequest.Table.Columns.Contains("first_name") &&
                webOrderRequest.Table.Columns.Contains("organization") &&
                webOrderRequest.Table.Columns.Contains("geography_id") &&
                webOrderRequest.Table.Columns.Contains("address_line1") &&
                webOrderRequest.Table.Columns.Contains("web_cooperator_id"))
            {
                // Invoke the Cooperator Wizard and pass the web order number in...
                CreateCooperatorsFromWebOrder(webOrderRequest["web_order_request_id"].ToString().Trim());
                if (!string.IsNullOrEmpty(_selectedShipToRecipientCooperatorID) && newOrderRequest.Table.Columns.Contains("ship_to_cooperator_id")) newOrderRequest["ship_to_cooperator_id"] = _selectedShipToRecipientCooperatorID;
                if (!string.IsNullOrEmpty(_selectedFinalRecipientCooperatorID) && newOrderRequest.Table.Columns.Contains("final_recipient_cooperator_id")) newOrderRequest["final_recipient_cooperator_id"] = _selectedFinalRecipientCooperatorID;
            }

            // Build the accession pkey list...
            string accessionIDs = "";
            foreach (DataRowView drv in ((DataTable)_webOrderRequestItemBindingSource.DataSource).DefaultView)
            {
                if (!ux_checkboxMySitesAccessionsOnly.Checked ||
                    !drv.Row.Table.Columns.Contains("site_id") ||
                    drv["site_id"].ToString().Trim().ToUpper() == _sharedUtils.UserSite.Trim().ToUpper())
                {
                    accessionIDs += _webOrderRequestItem.Rows.Find(drv["web_order_request_item_id"])["accession_id"].ToString() + ",";
                }
            }
            accessionIDs = accessionIDs.TrimEnd(new char[] { ',' });

            // New SE processing method...
            DataTable distributionInventories = FindDistributionInventoryFromAccessionList(accessionIDs);
            // Reset the accession pkey list (because we are going to rebuild it for only 
            // the accessions being added to this order (based on the 'MySiteOnly' checkbox)...
            accessionIDs = "";

            // Show the progress bar and initialize it...
            bindingNavigatorWebProgressBarLabel.Visible = true;
            bindingNavigatorWebProgressBarLabel.Text = "Selecting Best Matching Inventory Record:";
            bindingNavigatorWebProgressBar.Visible = true;
            bindingNavigatorWebProgressBar.Value = 0;
            bindingNavigatorWebProgressBar.Step = 1;
            bindingNavigatorWebProgressBar.Minimum = 0;
            bindingNavigatorWebProgressBar.Maximum = ((DataTable)_webOrderRequestItemBindingSource.DataSource).DefaultView.Count;
            Application.DoEvents();

            // Iterate through the accessions from the shopping cart to find the best match for inventory...
            foreach (DataRowView drv in ((DataTable)_webOrderRequestItemBindingSource.DataSource).DefaultView)
            {
                DataRow newOrderItem = null;

                if (!ux_checkboxMySitesAccessionsOnly.Checked ||
                    !drv.Row.Table.Columns.Contains("site_id") ||
                    drv["site_id"].ToString().Trim().ToUpper() == _sharedUtils.UserSite.Trim().ToUpper())
                {
                    string accessionID = _webOrderRequestItem.Rows.Find(drv["web_order_request_item_id"])["accession_id"].ToString();
                    // Add this accession to the pkey list as one being processed in this order...
                    accessionIDs += accessionID + ",";
                    string inventoryID = "";
                    // Use the inventory_id provided by the web_order_request_item table (if it exists) - otherwise use the accession_id... 
                    if (drv.Row.Table.Columns.Contains("inventory_id") && !string.IsNullOrEmpty(drv["inventory_id"].ToString().Trim()))
                    {
                        inventoryID = _webOrderRequestItem.Rows.Find(drv["web_order_request_item_id"])["inventory_id"].ToString();
                    }
                    else
                    {
                        inventoryID = FindInventoryBestMatch(distributionInventories, accessionID, drv["distribution_form_code"].ToString());
                    }
                    newOrderItem = BuildOrderRequestItemRow(inventoryID, _orderRequestItem, distributionInventories);
                    // Populate fields pulled from web_order_item now...
                    if (drv.Row.Table.Columns.Contains("web_order_request_item_id") && _orderRequestItem.Columns.Contains("web_order_request_item_id")) newOrderItem["web_order_request_item_id"] = drv["web_order_request_item_id"];
                    if (drv.Row.Table.Columns.Contains("user_note") && _orderRequestItem.Columns.Contains("web_user_note")) newOrderItem["web_user_note"] = drv["user_note"];
                    _orderRequestItem.Rows.Add(newOrderItem);
                    // Update the Progress Bar...
                    bindingNavigatorWebProgressBar.PerformStep();
                }
            }
            accessionIDs = accessionIDs.TrimEnd(new char[] { ',' });

            // Create a 'NEW' Action for this order...
            DataRow newOrderRequestAction = _orderRequestAction.NewRow();
            if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = newOrderRequest["order_request_id"];
            if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "NEW";
            if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
            if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
            if (_orderRequestAction.Columns.Contains("action_information")) newOrderRequestAction["action_information"] = DBNull.Value;
            if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
            if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
            if (_orderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "New order created (from web request) with " + accessionIDs.Split(',').Length.ToString() + " item(s) by " + _cooperator_site_longname + " (" + _cooperator_site_shortname + "), " + _cooperator_city + ", " + _cooperator_geography + " by " + _cooperator_name + " (" + _cooperator_email + ")";
            _orderRequestAction.Rows.Add(newOrderRequestAction);

            // Harvest all web order actions...
            foreach (DataRowView drv in _webOrderRequestAction.DefaultView)
            {
                newOrderRequestAction = _orderRequestAction.NewRow();
                if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = newOrderRequest["order_request_id"];
                if (_orderRequestAction.Columns.Contains("action_name_code") && _webOrderRequestAction.Columns.Contains("action_code")) newOrderRequestAction["action_name_code"] = drv["action_code"];
                if (_orderRequestAction.Columns.Contains("started_date") && _webOrderRequestAction.Columns.Contains("acted_date")) newOrderRequestAction["started_date"] = drv["acted_date"];
                if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
                if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
                if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
                if (_orderRequestAction.Columns.Contains("action_information") && _webOrderRequestAction.Columns.Contains("action_for_id") && !string.IsNullOrEmpty(drv["action_for_id"].ToString().Trim())) newOrderRequestAction["action_information"] = BuildAccessionFriendlyNamesFromWebOrderActionIDs(drv["action_for_id"].ToString(), accessionIDs);
                if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
                if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
                if (_orderRequestAction.Columns.Contains("note") && _webOrderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "Automatically transferred " + drv["action_code"] + " action from web_order_request_action table.  WEB NOTE: " + drv["note"];
                if (drv["action_code"].ToString().Trim().ToUpper() != "SMTAACCEPT")
                {
                    // Add all non-SMTA accept actions from the web order...
                    _orderRequestAction.Rows.Add(newOrderRequestAction);
                }
                else
                {
                    // Only add the SMTA accept action if the order contains SMTA material...
                    if (!string.IsNullOrEmpty(newOrderRequestAction["action_information"].ToString().Trim())) _orderRequestAction.Rows.Add(newOrderRequestAction);
                }
            }

            // Harvest any web order attachments the requestor may have uploaded...
            foreach (DataRowView drv in _webOrderRequestAttach.DefaultView)
            {
                DataRow newOrderRequestAttach = _orderRequestAttach.NewRow();
                if (_orderRequestAttach.Columns.Contains("order_request_id")) newOrderRequestAttach["order_request_id"] = newOrderRequest["order_request_id"];
                if (_orderRequestAttach.Columns.Contains("virtual_path") && _webOrderRequestAttach.Columns.Contains("virtual_path")) newOrderRequestAttach["virtual_path"] = drv["virtual_path"];
                if (_orderRequestAttach.Columns.Contains("thumbnail_virtual_path") && _webOrderRequestAttach.Columns.Contains("virtual_path")) newOrderRequestAttach["thumbnail_virtual_path"] = drv["virtual_path"];
                if (_orderRequestAttach.Columns.Contains("sort_order")) newOrderRequestAttach["sort_order"] = DBNull.Value;
                if (_orderRequestAttach.Columns.Contains("title") && _webOrderRequestAttach.Columns.Contains("title")) newOrderRequestAttach["title"] = drv["title"];
                if (_orderRequestAttach.Columns.Contains("description")) newOrderRequestAttach["description"] = DBNull.Value;
                if (_orderRequestAttach.Columns.Contains("content_type") && _webOrderRequestAttach.Columns.Contains("content_type")) newOrderRequestAttach["content_type"] = drv["content_type"];
                if (_orderRequestAttach.Columns.Contains("category_code")) newOrderRequestAttach["category_code"] = DBNull.Value;
                if (_orderRequestAttach.Columns.Contains("is_web_visible")) newOrderRequestAttach["is_web_visible"] = "N";
                if (_orderRequestAttach.Columns.Contains("copyright_information")) newOrderRequestAttach["copyright_information"] = DBNull.Value;
                if (_orderRequestAttach.Columns.Contains("attach_cooperator_id")) newOrderRequestAttach["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
                if (_orderRequestAttach.Columns.Contains("attach_date")) newOrderRequestAttach["attach_date"] = DateTime.Now;
                if (_orderRequestAttach.Columns.Contains("attach_date_code")) newOrderRequestAttach["attach_date_code"] = "MM/dd/yyyy";
                if (_orderRequestAttach.Columns.Contains("note") && _webOrderRequestAttach.Columns.Contains("note")) newOrderRequestAttach["note"] = drv["note"];
                _orderRequestAttach.Rows.Add(newOrderRequestAttach);
            }

            // Get the binary data for the attachments and save them to the local temp directory (for later uploading during save order processing)...
            foreach (DataRowView drv in _orderRequestAttach.DefaultView)
            {
                // Build the virtual and local path names...
                string attachVirtualPath = drv["virtual_path"].ToString();
                string fileName = System.IO.Path.GetFileName(attachVirtualPath);
                string tempFilesPath = System.IO.Path.GetTempPath();
                string attachLocalPath = tempFilesPath + "GRIN-Global CuratorTool\\Attachments\\Web Orders\\" + webOrderRequest["web_order_request_id"].ToString() + "\\" + fileName;
                // Get the attachment data...
                byte[] attachBytes = _sharedUtils.GetAttachment(attachVirtualPath);
                // Make sure the directory exists before writing the attachment data to disk...
                System.IO.Directory.CreateDirectory(tempFilesPath + "GRIN-Global CuratorTool\\Attachments\\Web Orders\\" + webOrderRequest["web_order_request_id"].ToString());
                // Create a local copy of the attachment...
                System.IO.File.WriteAllBytes(attachLocalPath, attachBytes);
                // Store the location of the local copy of the attachment in the database table...
                if (_orderRequestAttach.Columns.Contains("local_path")) drv["local_path"] = attachLocalPath;
            }
            // Change the status of the web order request...
            webOrderRequest["status_code"] = "ACCEPTED";

            // Inspect the new order request items to see if any IPR exists on them and if so visually flag the for the user...
            ux_labelOrderItemAlertFlag.Visible = false;
            if (OrderItemsContainAlerts()) ux_labelOrderItemAlertFlag.Visible = true;
            if (OrderItemsContainDuplicates()) ux_labelOrderItemDuplicateFlag.Visible = true;

            // Refresh the Order Items formatting...
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);

            // Hide the progress bar...
            bindingNavigatorWebProgressBarLabel.Visible = false;
            bindingNavigatorWebProgressBar.Visible = false;

            // Unfreeze the interface to enable user events again...
            ux_tabcontrolMain.Enabled = true;

            // Put focus on the Order tab page...
            ux_tabcontrolMain.SelectedTab = OrdersPage;

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void ux_buttonMergeOrderRequest_Click(object sender, EventArgs e)
        {
            string destinationOrderNumber = ux_comboboxMergeOrderRequest.SelectedValue.ToString();

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Freeze the interface to prevent async user events causing problems...
            ux_tabcontrolMain.Enabled = false;

            // If this order is not loaded - go load it now...
            if (_orderRequest.Rows.Find(destinationOrderNumber) == null)
            {
                // The order to merge the web request with is not currently in the Order Wizard
                // so we will need to go get it now...

                // First - get the order_request_item record(s) and merge with current order item records...
                DataSet ds = _sharedUtils.GetWebServiceData("order_wizard_get_order_request_item", ":orderrequestid=" + destinationOrderNumber, 0, 0);
                if (ds.Tables.Contains("order_wizard_get_order_request_item") &&
                    ds.Tables["order_wizard_get_order_request_item"].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables["order_wizard_get_order_request_item"].Rows)
                    {
                        _orderRequestItem.ImportRow(dr);
                        DataRow tempDR = _orderRequestItem.Rows.Find(dr["order_request_item_id"]);
                        // Populate the sortable columns (if possible)...
                        if (tempDR != null)
                        {
                            foreach (DataColumn dc in ds.Tables["order_wizard_get_order_request_item"].Columns)
                            {
                                if (_orderRequestItem.Columns.Contains(dc.ColumnName + "_sortable"))
                                {
                                    tempDR[dc.ColumnName + "_sortable"] = GetSortableColumnText(dr, dc.ColumnName);
                                }
                            }
                        }
                        // Accept the sortable column changes we just made...
                        tempDR.AcceptChanges();
                    }
                }

                // Second - get the order_request_action record(s) and merge with current order action records...
                ds = _sharedUtils.GetWebServiceData("order_wizard_get_order_request_action", ":orderrequestid=" + destinationOrderNumber, 0, 0);
                if (ds.Tables.Contains("order_wizard_get_order_request_action") &&
                    ds.Tables["order_wizard_get_order_request_action"].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables["order_wizard_get_order_request_action"].Rows)
                    {
                        _orderRequestAction.ImportRow(dr);
                    }
                }

                // Third - get the order_request_attach record(s) and merge with current order attach records...
                ds = _sharedUtils.GetWebServiceData("order_wizard_get_order_request_attach", ":orderrequestid=" + destinationOrderNumber, 0, 0);
                if (ds.Tables.Contains("order_wizard_get_order_request_attach") &&
                    ds.Tables["order_wizard_get_order_request_attach"].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables["order_wizard_get_order_request_attach"].Rows)
                    {
                        _orderRequestAttach.ImportRow(dr);
                    }
                }

                // Fourth - get the order_request_phyto_log record(s) and merge with current order phyto log records...
                ds = _sharedUtils.GetWebServiceData("order_wizard_get_order_request_phyto_log", ":orderrequestid=" + destinationOrderNumber, 0, 0);
                if (ds.Tables.Contains("order_wizard_get_order_request_phyto_log") &&
                    ds.Tables["order_wizard_get_order_request_phyto_log"].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables["order_wizard_get_order_request_phyto_log"].Rows)
                    {
                        _orderRequestPhytoLog.ImportRow(dr);
                    }
                }

                // Finally - get the order_request record and merge it with any current order records...
                // (do this one last so that the child tables get auto-filtered to match the current row in this table)
                ds = _sharedUtils.GetWebServiceData("order_wizard_get_order_request", ":orderrequestid=" + destinationOrderNumber, 0, 0);
                if (ds.Tables.Contains("order_wizard_get_order_request") &&
                    ds.Tables["order_wizard_get_order_request"].Rows.Count > 0)
                {
                    _orderRequest.ImportRow(ds.Tables["order_wizard_get_order_request"].Rows[0]);
                }
            }

            // Move to that record in binding source so that it is displayed when all is done...
            ux_bindingnavigatorForm.BindingSource.Position = ux_bindingnavigatorForm.BindingSource.Find("order_request_id", destinationOrderNumber);

            DataRow mergeOrderRequest = ((DataRowView)ux_bindingnavigatorForm.BindingSource.Current).Row;

            DataRow webOrderRequest = ((DataRowView)ux_bindingNavigatorWebOrders.BindingSource.Current).Row;

            // Merge the notes from the web order being merged into this order request...
            if (mergeOrderRequest.Table.Columns.Contains("intended_use_note") && webOrderRequest.Table.Columns.Contains("intended_use_note") && !string.IsNullOrEmpty(webOrderRequest["intended_use_note"].ToString())) mergeOrderRequest["intended_use_note"] = mergeOrderRequest["intended_use_note"].ToString() + " Intended use note merged from web request #" + webOrderRequest["web_order_request_id"].ToString() + ": " + webOrderRequest["intended_use_note"].ToString();
            if (mergeOrderRequest.Table.Columns.Contains("note") && webOrderRequest.Table.Columns.Contains("note") && !string.IsNullOrEmpty(webOrderRequest["note"].ToString())) mergeOrderRequest["note"] = mergeOrderRequest["note"].ToString() + " Note merged from web request #" + webOrderRequest["web_order_request_id"].ToString() + ": " + webOrderRequest["note"].ToString();
            if (mergeOrderRequest.Table.Columns.Contains("special_instruction") && webOrderRequest.Table.Columns.Contains("special_instruction") && !string.IsNullOrEmpty(webOrderRequest["special_instruction"].ToString())) mergeOrderRequest["special_instruction"] = mergeOrderRequest["special_instruction"] + " Special instructions merged from web request #" + webOrderRequest["web_order_request_id"].ToString() + ": " + webOrderRequest["special_instruction"].ToString();
            //"Order merged with open web request (#" + webOrderRequest["web_order_request_id"].ToString() + ")

            // Now we can begin the process of merging the web order items, actions, and attachments into this existing order...
            // Build the accession pkey list...
            string accessionIDs = "";
            foreach (DataRowView drv in ((DataTable)_webOrderRequestItemBindingSource.DataSource).DefaultView)
            {
                accessionIDs += _webOrderRequestItem.Rows.Find(drv["web_order_request_item_id"])["accession_id"].ToString() + ",";
            }
            accessionIDs = accessionIDs.TrimEnd(new char[] { ',' });

            // New SE processing method...
            DataTable distributionInventories = FindDistributionInventoryFromAccessionList(accessionIDs);
            // Reset the accession pkey list (because we are going to rebuild it for only 
            // the accessions being added to this order (based on the 'MySiteOnly' checkbox)...
            accessionIDs = "";
            foreach (DataRowView drv in ((DataTable)_webOrderRequestItemBindingSource.DataSource).DefaultView)
            {
                DataRow newOrderItem = null;

                if (!ux_checkboxMySitesAccessionsOnly.Checked ||
                    !drv.Row.Table.Columns.Contains("site_id") ||
                    drv["site_id"].ToString().Trim().ToUpper() == _sharedUtils.UserSite.Trim().ToUpper())
                {
                    string accessionID = _webOrderRequestItem.Rows.Find(drv["web_order_request_item_id"])["accession_id"].ToString();
                    // Add this accession to the pkey list as one being processed in this order...
                    accessionIDs += accessionID + ",";
                    string inventoryID = "";
                    // Use the inventory_id provided by the web_order_request_item table (if it exists) - otherwise use the accession_id... 
                    if (drv.Row.Table.Columns.Contains("inventory_id") && !string.IsNullOrEmpty(drv["inventory_id"].ToString().Trim()))
                    {
                        inventoryID = _webOrderRequestItem.Rows.Find(drv["web_order_request_item_id"])["inventory_id"].ToString();
                    }
                    else
                    {
                        inventoryID = FindInventoryBestMatch(distributionInventories, accessionID, drv["distribution_form_code"].ToString());
                    }
                    newOrderItem = BuildOrderRequestItemRow(inventoryID, _orderRequestItem, distributionInventories);
                    // Populate fields pulled from web_order_item now...
                    if (drv.Row.Table.Columns.Contains("web_order_request_item_id") && _orderRequestItem.Columns.Contains("web_order_request_item_id")) newOrderItem["web_order_request_item_id"] = drv["web_order_request_item_id"];
                    if (drv.Row.Table.Columns.Contains("user_note") && _orderRequestItem.Columns.Contains("web_user_note")) newOrderItem["web_user_note"] = drv["user_note"];
                    _orderRequestItem.Rows.Add(newOrderItem);
                }
            }
            accessionIDs = accessionIDs.TrimEnd(new char[] { ',' });

            // Create a 'MERGE' Action for this order...
            DataRow newOrderRequestAction = _orderRequestAction.NewRow();
            if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = destinationOrderNumber;
            if (_orderRequestAction.Columns.Contains("action_name_code")) newOrderRequestAction["action_name_code"] = "MERGE";
            if (_orderRequestAction.Columns.Contains("started_date")) newOrderRequestAction["started_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
            if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
            if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
            if (_orderRequestAction.Columns.Contains("action_information")) newOrderRequestAction["action_information"] = DBNull.Value;
            if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
            if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
            if (_orderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "Order merged with open web request (#" + webOrderRequest["web_order_request_id"].ToString() + ") - adding " + accessionIDs.Split(',').Length.ToString() + " new item(s) by " + _cooperator_site_longname + " (" + _cooperator_site_shortname + "), " + _cooperator_city + ", " + _cooperator_geography + " by " + _cooperator_name + " (" + _cooperator_email + ")";
            _orderRequestAction.Rows.Add(newOrderRequestAction);

            // Harvest all web order actions...
            foreach (DataRowView drv in _webOrderRequestAction.DefaultView)
            {
                newOrderRequestAction = _orderRequestAction.NewRow();
                if (_orderRequestAction.Columns.Contains("order_request_id")) newOrderRequestAction["order_request_id"] = destinationOrderNumber;
                if (_orderRequestAction.Columns.Contains("action_name_code") && _webOrderRequestAction.Columns.Contains("action_code")) newOrderRequestAction["action_name_code"] = drv["action_code"];
                if (_orderRequestAction.Columns.Contains("started_date") && _webOrderRequestAction.Columns.Contains("acted_date")) newOrderRequestAction["started_date"] = drv["acted_date"];
                if (_orderRequestAction.Columns.Contains("started_date_code")) newOrderRequestAction["started_date_code"] = "MM/dd/yyyy";
                if (_orderRequestAction.Columns.Contains("completed_date")) newOrderRequestAction["completed_date"] = DateTime.Now;
                if (_orderRequestAction.Columns.Contains("completed_date_code")) newOrderRequestAction["completed_date_code"] = "MM/dd/yyyy";
                if (_orderRequestAction.Columns.Contains("action_information") && _webOrderRequestAction.Columns.Contains("action_for_id") && !string.IsNullOrEmpty(drv["action_for_id"].ToString().Trim())) newOrderRequestAction["action_information"] = BuildAccessionFriendlyNamesFromWebOrderActionIDs(drv["action_for_id"].ToString(), accessionIDs);
                if (_orderRequestAction.Columns.Contains("action_cost")) newOrderRequestAction["action_cost"] = DBNull.Value;
                if (_orderRequestAction.Columns.Contains("cooperator_id")) newOrderRequestAction["cooperator_id"] = _sharedUtils.UserCooperatorID;
                if (_orderRequestAction.Columns.Contains("note") && _webOrderRequestAction.Columns.Contains("note")) newOrderRequestAction["note"] = "Automatically transferred " + drv["action_code"] + " action from web_order_request_action table.  WEB NOTE: " + drv["note"];
                if (drv["action_code"].ToString().Trim().ToUpper() != "SMTAACCEPT")
                {
                    // Add all non-SMTA accept actions from the web order...
                    _orderRequestAction.Rows.Add(newOrderRequestAction);
                }
                else
                {
                    // Only add the SMTA accept action if the order contains SMTA material...
                    if (!string.IsNullOrEmpty(newOrderRequestAction["action_information"].ToString().Trim())) _orderRequestAction.Rows.Add(newOrderRequestAction);
                }
            }

            // Harvest any web order attachments the requestor may have uploaded...
            foreach (DataRowView drv in _webOrderRequestAttach.DefaultView)
            {
                DataRow newOrderRequestAttach = _orderRequestAttach.NewRow();
                if (_orderRequestAttach.Columns.Contains("order_request_id")) newOrderRequestAttach["order_request_id"] = destinationOrderNumber;
                if (_orderRequestAttach.Columns.Contains("virtual_path") && _webOrderRequestAttach.Columns.Contains("virtual_path")) newOrderRequestAttach["virtual_path"] = drv["virtual_path"];
                if (_orderRequestAttach.Columns.Contains("thumbnail_virtual_path") && _webOrderRequestAttach.Columns.Contains("virtual_path")) newOrderRequestAttach["thumbnail_virtual_path"] = drv["virtual_path"];
                if (_orderRequestAttach.Columns.Contains("sort_order")) newOrderRequestAttach["sort_order"] = DBNull.Value;
                if (_orderRequestAttach.Columns.Contains("title") && _webOrderRequestAttach.Columns.Contains("title")) newOrderRequestAttach["title"] = drv["title"];
                if (_orderRequestAttach.Columns.Contains("description")) newOrderRequestAttach["description"] = DBNull.Value;
                if (_orderRequestAttach.Columns.Contains("content_type") && _webOrderRequestAttach.Columns.Contains("content_type")) newOrderRequestAttach["content_type"] = drv["content_type"];
                if (_orderRequestAttach.Columns.Contains("category_code")) newOrderRequestAttach["category_code"] = DBNull.Value;
                if (_orderRequestAttach.Columns.Contains("is_web_visible")) newOrderRequestAttach["is_web_visible"] = "N";
                if (_orderRequestAttach.Columns.Contains("copyright_information")) newOrderRequestAttach["copyright_information"] = DBNull.Value;
                if (_orderRequestAttach.Columns.Contains("attach_cooperator_id")) newOrderRequestAttach["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
                if (_orderRequestAttach.Columns.Contains("attach_date")) newOrderRequestAttach["attach_date"] = DateTime.Now;
                if (_orderRequestAttach.Columns.Contains("attach_date_code")) newOrderRequestAttach["attach_date_code"] = "MM/dd/yyyy";
                if (_orderRequestAttach.Columns.Contains("note") && _webOrderRequestAttach.Columns.Contains("note")) newOrderRequestAttach["note"] = drv["note"];
                _orderRequestAttach.Rows.Add(newOrderRequestAttach);
            }

            // Get the binary data for the attachments and save them to the local temp directory (for later uploading during save order processing)...
            foreach (DataRowView drv in _orderRequestAttach.DefaultView)
            {
                // Build the virtual and local path names...
                string attachVirtualPath = drv["virtual_path"].ToString();
                //if (!attachVirtualPath.Contains("~/uploads/imports/")) attachVirtualPath = "~/uploads/imports/" + attachVirtualPath; // HACK until PW puts full path into the database field 'thumbnail_virtual_path'
                string fileName = System.IO.Path.GetFileName(attachVirtualPath);
                string tempFilesPath = System.IO.Path.GetTempPath();
                string attachLocalPath = tempFilesPath + "GRIN-Global CuratorTool\\Attachments\\Web Orders\\" + webOrderRequest["web_order_request_id"].ToString() + "\\" + fileName;
                // Get the attachment data...
                byte[] attachBytes = _sharedUtils.GetAttachment(attachVirtualPath);
                // Make sure the directory exists before writing the attachment data to disk...
                System.IO.Directory.CreateDirectory(tempFilesPath + "GRIN-Global CuratorTool\\Attachments\\Web Orders\\" + webOrderRequest["web_order_request_id"].ToString());
                // Create a local copy of the attachment...
                System.IO.File.WriteAllBytes(attachLocalPath, attachBytes);
                // Store the location of the local copy of the attachment in the database table...
                if (_orderRequestAttach.Columns.Contains("local_path")) drv["local_path"] = attachLocalPath;
            }
            // Change the status of the web order request...
            webOrderRequest["status_code"] = "ACCEPTED";

            // Inspect the new order request items to see if any IPR exists on them and if so visually flag the for the user...
            ux_labelOrderItemAlertFlag.Visible = false;
            if (OrderItemsContainAlerts()) ux_labelOrderItemAlertFlag.Visible = true;
            if (OrderItemsContainDuplicates()) ux_labelOrderItemDuplicateFlag.Visible = true;

            // Refresh the Order Items formatting...
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);

            // Unfreeze the interface to enable user events again...
            ux_tabcontrolMain.Enabled = true;

            // Put focus on the Order tab page...
            ux_tabcontrolMain.SelectedTab = OrdersPage;

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void ux_buttonCancelWebOrderRequest_Click(object sender, EventArgs e)
        {
            DataSet webOrderRequestChanges = new DataSet();
            DataSet webOrderRequestSaveResults = new DataSet();

            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You are requesting to cancel all 'Web Order Items' for your site - are you sure you want to cancel these items?", "Cancel Web Order Items", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "ux_buttonCancelWebOrderRequest_Click";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            // Show the warning dialog message if there are unsaved edits...
            if (DialogResult.No == ggMessageBox.ShowDialog())
            {
                return;
            }

            // If there is no active row in the web_order_request DGV - bail out now...
            if (ux_bindingNavigatorWebOrders.BindingSource.Current == null) return;

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Change the web_order_request record status to 'CANCELED'...
            DataRow webOrderRequest = ((DataRowView)ux_bindingNavigatorWebOrders.BindingSource.Current).Row;
            webOrderRequest["status_code"] = "CANCELED";
            // Commit the status_code update in the web_order_request table to the remote database...
            if (_webOrderRequest != null && _webOrderRequest.GetChanges() != null)
            {
                webOrderRequestChanges.Tables.Add(_webOrderRequest.GetChanges());
                ScrubData(webOrderRequestChanges);

                // Save the web order request changes to the remote server...
                webOrderRequestSaveResults = _sharedUtils.SaveWebServiceData(webOrderRequestChanges);
                // Sync the saved results with the original table (to give user feedback about results of save)...
                if (webOrderRequestSaveResults.Tables.Contains(_webOrderRequest.TableName))
                {
                    SyncSavedResults(_webOrderRequest, webOrderRequestSaveResults.Tables[_webOrderRequest.TableName]);
                }
            }
            Cursor.Current = origCursor;
        }

        private string BuildAccessionFriendlyNamesFromWebOrderActionIDs(string webOrderRequestActionIDs, string accessionIDs)
        {
            string returnedNames = "";
            // Create an array of web_order_request_ids string from the web_order_request_action records
            string[] webOrderRequestItemIDs = webOrderRequestActionIDs.Split(';');
            // Get the friendly name for each accession 
            foreach (string woriID in webOrderRequestItemIDs)
            {
                // First find the matching web_order_request_item record...
                DataRow[] drs = _webOrderRequestItem.Select("web_order_request_item_id=" + woriID.Trim());
                if (drs != null && drs.Length > 0)
                {
                    string accessionNumber = _sharedUtils.GetLookupDisplayMember("accession_lookup", drs[0]["accession_id"].ToString().Trim(), "", woriID) + ", ";
                    // Filter accession name list based on the accession list of items in the order...
                    if (accessionIDs.Contains(drs[0]["accession_id"].ToString().Trim())) returnedNames += accessionNumber;
                }
            }
            return returnedNames.Trim().TrimEnd(',');
        }

        private string FindInventoryBestMatch(DataTable distributionInventories, string accessionID, string requestedFormType)
        {
            string returnInventoryID = "";

            // Look for an inventory that matches the requested form type...
            if (distributionInventories.Select("accession_id=" + accessionID + " AND distribution_default_form_code='" + requestedFormType + "' AND is_distributable='Y' AND is_available='Y'").Count() > 0)
            {
                returnInventoryID = distributionInventories.Select("accession_id=" + accessionID + " AND distribution_default_form_code='" + requestedFormType + "' AND is_distributable='Y' AND is_available='Y'")[0]["inventory_id"].ToString();
            }
            else if (distributionInventories.Select("accession_id=" + accessionID + " AND distribution_default_form_code='" + requestedFormType + "' AND is_distributable='Y'").Count() > 0)
            {
                returnInventoryID = distributionInventories.Select("accession_id=" + accessionID + " AND distribution_default_form_code='" + requestedFormType + "' AND is_distributable='Y'")[0]["inventory_id"].ToString();
            }
            else if (distributionInventories.Select("accession_id=" + accessionID + " AND distribution_default_form_code='" + requestedFormType + "'").Count() > 0)
            {
                returnInventoryID = distributionInventories.Select("accession_id=" + accessionID + " AND distribution_default_form_code='" + requestedFormType + "'")[0]["inventory_id"].ToString();
            }

            // No inventory record has a matching requested form type so look for other inventory form types that are distributable... 
            if (string.IsNullOrEmpty(returnInventoryID))
            {
                if (distributionInventories.Select("accession_id=" + accessionID + " AND (distribution_default_form_code IS NULL OR distribution_default_form_code <> '**') AND is_distributable='Y' AND is_available='Y'").Count() > 0)
                {
                    returnInventoryID = distributionInventories.Select("accession_id=" + accessionID + " AND (distribution_default_form_code IS NULL OR distribution_default_form_code <> '**') AND is_distributable='Y' AND is_available='Y'")[0]["inventory_id"].ToString();
                }
                else if (distributionInventories.Select("accession_id=" + accessionID + " AND (distribution_default_form_code IS NULL OR distribution_default_form_code <> '**') AND is_distributable='Y'").Count() > 0)
                {
                    returnInventoryID = distributionInventories.Select("accession_id=" + accessionID + " AND (distribution_default_form_code IS NULL OR distribution_default_form_code <> '**') AND is_distributable='Y'")[0]["inventory_id"].ToString();
                }
                else if (distributionInventories.Select("accession_id=" + accessionID + " AND (distribution_default_form_code IS NULL OR distribution_default_form_code <> '**')").Count() > 0)
                {
                    returnInventoryID = distributionInventories.Select("accession_id=" + accessionID + " AND (distribution_default_form_code IS NULL OR distribution_default_form_code <> '**')")[0]["inventory_id"].ToString();
                }
                else if (distributionInventories.Select("accession_id=" + accessionID).Count() > 0)
                {
                    returnInventoryID = distributionInventories.Select("accession_id=" + accessionID)[0]["inventory_id"].ToString();
                }
            }
            return returnInventoryID;
        }

        private string FindCooperator(string last_name, string first_name, string organization, string geography_id, string address_line1, string web_cooperator_id)
        {
            string cooperator_id = "";
            string findText = "";
            string searchPKeys = ":cooperatorid=";
            DataTable cooperators = new DataTable();

            // First get the cooperator_ids from the search engine that match the lastname...
            if (!string.IsNullOrEmpty(last_name)) findText += "@cooperator.last_name = '" + last_name + "'";
            if (!string.IsNullOrEmpty(findText))
            {
                DataSet dsSearch = _sharedUtils.SearchWebService(findText, true, true, null, "cooperator", 0, 0);
                if (dsSearch.Tables.Contains("SearchResult"))
                {
                    foreach (DataRow dr in dsSearch.Tables["SearchResult"].Rows)
                    {
                        searchPKeys += dr["ID"].ToString() + ",";
                    }
                }
            }
            // Remove the last trailing comma...
            searchPKeys = searchPKeys.TrimEnd(',');
            // Next get the full collection of cooperator_ids that match the last name of the web_cooperator...
            DataSet dsCooperators = _sharedUtils.GetWebServiceData("get_cooperator", searchPKeys, 0, 0);
            if (dsCooperators.Tables.Contains("get_cooperator"))
            {
                cooperators = dsCooperators.Tables["get_cooperator"].Copy();
            }
            // Next iterate through the cooperator records to see if there is a perfect match for the web_cooperator info in the web_order_request...
            // based on last_name, first_name, organization, geography_id, and address_line1 (giving preference to the newest cooperator id record)...
            cooperators.DefaultView.Sort = "cooperator_id ASC";
            foreach (DataRowView drv in cooperators.DefaultView)
            {
                bool match = true;
                if (drv.DataView.Table.Columns.Contains("last_name") && (drv["last_name"].ToString().ToLower() != last_name.ToLower())) match = false;
                if (drv.DataView.Table.Columns.Contains("first_name") && (drv["first_name"].ToString().ToLower() != first_name.ToLower())) match = false;
                if (drv.DataView.Table.Columns.Contains("organization") && (drv["organization"].ToString().ToLower() != organization.ToLower())) match = false;
                if (drv.DataView.Table.Columns.Contains("geography_id") && (drv["geography_id"].ToString().ToLower() != geography_id.ToLower())) match = false;
                if (drv.DataView.Table.Columns.Contains("address_line1") && (drv["address_line1"].ToString().ToLower() != address_line1.ToLower())) match = false;
                if (match && drv.DataView.Table.Columns.Contains("cooperator_id"))
                {
                    cooperator_id = drv["cooperator_id"].ToString().Trim();
                }
            }

            return cooperator_id;
        }

        private void ux_buttonCreateCooperator_Click(object sender, EventArgs e)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            ux_buttonCancelWebOrderRequest.Visible = true;

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }


        private void CreateCooperatorsFromWebOrder(string webOrderRequestID)
        {
            // If there is no active row in the web_order_request DGV - bail out now...
            if (ux_bindingNavigatorWebOrders.BindingSource.Current == null) return;

            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory());
            System.IO.FileInfo[] dllFiles = di.GetFiles("Wizards\\CooperatorWizard.dll", System.IO.SearchOption.AllDirectories);
            if (dllFiles != null && dllFiles.Length > 0)
            {
                for (int i = 0; i < dllFiles.Length; i++)
                {
                    System.Reflection.Assembly newAssembly = System.Reflection.Assembly.LoadFile(dllFiles[i].FullName);
                    foreach (System.Type t in newAssembly.GetTypes())
                    {
                        if (t.GetInterface("IGRINGlobalDataWizard", true) != null)
                        {
                            System.Reflection.ConstructorInfo constInfo = t.GetConstructor(new Type[] { typeof(string), typeof(SharedUtils) });
                            if (constInfo != null)
                            {
                                DataRow currentOrderRequest = ((DataRowView)_orderRequestBindingSource.Current).Row;
                                if (currentOrderRequest != null)
                                {
                                    string pkeyCollection = ":weborderrequestid=" + webOrderRequestID.Trim();
                                    // Instantiate an object of this type to load...
                                    Form wizardForm = (Form)constInfo.Invoke(new object[] { pkeyCollection, _sharedUtils });
                                    wizardForm.Owner = this;
                                    wizardForm.FormClosing += new FormClosingEventHandler(cooperatorWizard_FormClosing);
                                    wizardForm.ShowDialog(this);
                                }
                            }
                        }
                    }
                }
            }

            // Force the new cooperator to be loaded in the Cooperator LU table by performing an update on the LU table in the foreground...
            _sharedUtils.LookupTablesUpdateTable("cooperator_lookup", false);
        }

        void cooperatorWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form wizard = (Form)sender;
            if (e.Cancel)
            {
                wizard.Activate();
            }
            else
            {
                System.Reflection.PropertyInfo pi = wizard.GetType().GetProperty("FinalRecipientCooperatorID");
                if (pi != null &&
                    pi.PropertyType.Name.ToUpper() == "STRING")
                {
                    _selectedFinalRecipientCooperatorID = pi.GetValue(wizard, null).ToString().Trim();
                }
                pi = wizard.GetType().GetProperty("ShipToCooperatorID");
                if (pi != null &&
                    pi.PropertyType.Name.ToUpper() == "STRING")
                {
                    _selectedShipToRecipientCooperatorID = pi.GetValue(wizard, null).ToString().Trim();
                }

                // Since this is a modal dialog box - we need to handle the dispose method manually when we are done with it...
                wizard.Dispose();
            }
        }

        private void ux_buttonNewOrderRequestItemRow_Click(object sender, EventArgs e)
        {
            string pkey = ((DataRowView)_orderRequestBindingSource.Current)[_orderRequest.PrimaryKey[0].ColumnName].ToString();
            DataRow newOrderRequestItem = _orderRequestItem.NewRow();
            newOrderRequestItem["order_request_id"] = pkey;

            // Set the current cell at the beginning of this new record...
            int newRowIndex = ux_datagridviewOrderRequestItem.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewOrderRequestItem.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;

            // Find the row index of the new record...
            for (int i = 0; i < ux_datagridviewOrderRequestItem.Rows.Count; i++)
            {
                if (ux_datagridviewOrderRequestItem["order_request_item_id", i].Value.Equals(newOrderRequestItem["order_request_item_id"])) newRowIndex = i;
            }

            // Find the left-most visible column index...
            foreach (DataGridViewColumn dgvc in ux_datagridviewOrderRequestItem.Columns)
            {
                if (dgvc.DisplayIndex == 0)
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }

            // Set the current cell to the new records left-most column...
            if (newColIndex > -1 && newRowIndex > -1) ux_datagridviewOrderRequestItem.CurrentCell = ux_datagridviewOrderRequestItem[newColIndex, newRowIndex];

            GRINGlobal.Client.Common.FKeyPicker ftp = new GRINGlobal.Client.Common.FKeyPicker(_sharedUtils, "inventory_id", newOrderRequestItem, "");
            ftp.StartPosition = FormStartPosition.CenterParent;
            if (DialogResult.OK == ftp.ShowDialog())
            {
                if (_orderRequestItem != null)
                {
                    string distributionInventoryID = "";
                    if (!string.IsNullOrEmpty(ftp.NewKey)) distributionInventoryID = ftp.NewKey.Trim();
                    DataSet distributionInventory = _sharedUtils.GetWebServiceData("order_wizard_get_inventory", ":inventoryid=" + distributionInventoryID, 0, 0);
                    newOrderRequestItem = BuildOrderRequestItemRow(distributionInventoryID, _orderRequestItem, distributionInventory.Tables["order_wizard_get_inventory"]);
                    //newOrderRequestItem.SetColumnError("inventory_id", null);
                    _orderRequestItem.Rows.Add(newOrderRequestItem);
                }
                RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
            }
        }

        private void ux_buttonOrderRequestItemRenumberItems_Click(object sender, EventArgs e)
        {
            int seqNo = 1;
            foreach (DataRowView drv in _orderRequestItem.DefaultView)
            {
                drv.Row["sequence_number"] = seqNo++;
            }
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
        }

        private void ux_buttonFindOrders_Click(object sender, EventArgs e)
        {
            // If the Order tab is active, refresh the Order data...
            if (ux_tabcontrolMain.SelectedTab == OrdersPage || ux_tabcontrolMain.SelectedTab == ActionsPage) RefreshOrderData();
            // If the Web Order tab is active, refresh the Web Order data...
            if (ux_tabcontrolMain.SelectedTab == WebOrderPage) RefreshWebOrderData();
            if(_orderRequest.Rows.Count < 1)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("No orders found...", "Order Wizard Find Orders", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "OrderWizard_ux_buttonFindOrders_ClickMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();
            }
        }

        private void ux_dgvheadermenuSortAscending_Click(object sender, EventArgs e)
        {
            string fieldName = "";

            fieldName = ux_datagridviewOrderRequestItem.Columns[_mouseClickDGVColumnIndex].Name + "_sortable";
            if (_sortOrder.Contains(fieldName + " DESC"))
            {
                _sortOrder = _sortOrder.Replace(fieldName + " DESC", fieldName + " ASC");
            }
            else if (!_sortOrder.Contains(fieldName + " ASC"))
            {
                if (_sortOrder.Length > 0)
                {
                    _sortOrder += "," + fieldName + " ASC";
                }
                else
                {
                    _sortOrder += fieldName + " ASC";
                }
            }
            _sortOrder = _sortOrder.Replace(",,", ",");
            ((DataTable)((BindingSource)ux_datagridviewOrderRequestItem.DataSource).DataSource).DefaultView.Sort = _sortOrder;
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
        }

        private void ux_dgvheadermenuSortDescending_Click(object sender, EventArgs e)
        {
            string fieldName = "";

            fieldName = ux_datagridviewOrderRequestItem.Columns[_mouseClickDGVColumnIndex].Name + "_sortable";
            if (_sortOrder.Contains(fieldName + " ASC"))
            {
                _sortOrder = _sortOrder.Replace(fieldName + " ASC", fieldName + " DESC");
            }
            else if (!_sortOrder.Contains(fieldName + " DESC"))
            {
                if (_sortOrder.Length > 0)
                {
                    _sortOrder += "," + fieldName + " DESC";
                }
                else
                {
                    _sortOrder += fieldName + " DESC";
                }
            }
            _sortOrder = _sortOrder.Replace(",,", ",");
            ((DataTable)((BindingSource)ux_datagridviewOrderRequestItem.DataSource).DataSource).DefaultView.Sort = _sortOrder;
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
        }

        private void ux_dgvheadermenuNoSort_Click(object sender, EventArgs e)
        {
            string fieldName = "";

            fieldName = ux_datagridviewOrderRequestItem.Columns[_mouseClickDGVColumnIndex].Name + "_sortable";
            if (_sortOrder.Contains(fieldName + " DESC"))
            {
                _sortOrder = _sortOrder.Replace(fieldName + " DESC", "");
            }
            else if (_sortOrder.Contains(fieldName + " ASC"))
            {
                _sortOrder = _sortOrder.Replace(fieldName + " ASC", "");
            }
            // Remove all commas at the start and end of the string...
            _sortOrder = _sortOrder.TrimStart(',').TrimEnd(',');
            // Remove any double commas from the string...
            _sortOrder = _sortOrder.Replace(",,", ",");

            ((DataTable)((BindingSource)ux_datagridviewOrderRequestItem.DataSource).DataSource).DefaultView.Sort = _sortOrder;
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
        }

        private void ux_dgvheadermenuResetAllSorting_Click(object sender, EventArgs e)
        {
            _sortOrder = "";
            ((DataTable)((BindingSource)ux_datagridviewOrderRequestItem.DataSource).DataSource).DefaultView.Sort = _sortOrder;
            RefreshDGVFormatting(ux_datagridviewOrderRequestItem);
        }

        private void ux_datagridviewOrderRequestItem_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void syncDataRow(DataRow fromRow, string fromColumnName, DataRow toRow, string toColumnName)
        {
            if (fromRow.Table.Columns.Contains(fromColumnName) &&
                toRow.Table.Columns.Contains(toColumnName) &&
                toRow[toColumnName] != fromRow[fromColumnName])
            {
                // Make sure the data types match...
                if (fromRow.Table.Columns[fromColumnName].DataType == toRow.Table.Columns[toColumnName].DataType)
                {
                    if (toRow.Table.Columns[toColumnName].ReadOnly)
                    {
                        toRow.Table.Columns[toColumnName].ReadOnly = false;
                        toRow[toColumnName] = fromRow[fromColumnName];
                        toRow.Table.Columns[toColumnName].ReadOnly = true;
                    }
                    else
                    {
                        toRow[toColumnName] = fromRow[fromColumnName];
                    }
                }
                // If the data types do not match attempt to convert...
                else
                {
                    if (toRow.Table.Columns[toColumnName].DataType == typeof(string))
                    {
                        if (_sharedUtils.LookupTablesIsValidFKField(fromRow.Table.Columns[fromColumnName]))
                        {
                            if (toRow.Table.Columns[toColumnName].ReadOnly)
                            {
                                toRow.Table.Columns[toColumnName].ReadOnly = false;
                                toRow[toColumnName] = _sharedUtils.GetLookupDisplayMember(fromRow.Table.Columns[fromColumnName].ExtendedProperties["foreign_key_dataview_name"].ToString(), fromRow[fromColumnName].ToString(), "", fromRow[fromColumnName].ToString());
                                toRow.Table.Columns[toColumnName].ReadOnly = true;
                            }
                            else
                            {
                                toRow[toColumnName] = _sharedUtils.GetLookupDisplayMember(fromRow.Table.Columns[fromColumnName].ExtendedProperties["foreign_key_dataview_name"].ToString(), fromRow[fromColumnName].ToString(), "", fromRow[fromColumnName].ToString());
                            }
                        }
                        else if (_sharedUtils.LookupTablesIsValidCodeValueField(fromRow.Table.Columns[fromColumnName]))
                        {
                            if (toRow.Table.Columns[toColumnName].ReadOnly)
                            {
                                toRow.Table.Columns[toColumnName].ReadOnly = false;
                                toRow[toColumnName] = _sharedUtils.GetLookupDisplayMember("code_value_lookup", fromRow[fromColumnName].ToString(), fromRow.Table.Columns[fromColumnName].ExtendedProperties["group_name"].ToString(), fromRow[fromColumnName].ToString());
                                toRow.Table.Columns[toColumnName].ReadOnly = true;
                            }
                            else
                            {
                                toRow[toColumnName] = _sharedUtils.GetLookupDisplayMember("code_value_lookup", fromRow[fromColumnName].ToString(), fromRow.Table.Columns[fromColumnName].ExtendedProperties["group_name"].ToString(), fromRow[fromColumnName].ToString());
                            }
                        }
                        else
                        {
                            if (toRow.Table.Columns[toColumnName].ReadOnly)
                            {
                                toRow.Table.Columns[toColumnName].ReadOnly = false;
                                toRow[toColumnName] = fromRow[fromColumnName].ToString();
                                toRow.Table.Columns[toColumnName].ReadOnly = true;
                            }
                            else
                            {
                                toRow[toColumnName] = fromRow[fromColumnName].ToString();
                            }
                        }
                    }
                }
                // Sync the sortable text field if one exists...
                if (toRow.Table.Columns.Contains(toColumnName + "_sortable")) toRow[toColumnName + "_sortable"] = GetSortableColumnText(toRow, toColumnName);
            }
        }

        private void ux_datagridviewOrderRequestItem_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            // Next line is commented out because under certain circumstances the 'EndEdit()' method would invoke an infinite recurrsion...
            //dgv.EndEdit();
            // EndEdit() commented out in line above to prevent infinite recurrsion...
            if (e.ColumnIndex > -1 && e.RowIndex > -1)
            {
                DataGridViewCell dgvc = dgv[e.ColumnIndex, e.RowIndex];
                DataRow dr = ((DataRowView)((DataGridViewRow)dgvc.OwningRow).DataBoundItem).Row;
                dr.EndEdit();
                string dcName = ((DataGridViewColumn)dgvc.OwningColumn).Name;
                string currentValue = dr[dcName, DataRowVersion.Current].ToString().Trim();
                string originalValue = currentValue;
                if (dr.RowState == DataRowState.Modified) originalValue = dr[dcName, DataRowVersion.Original].ToString().Trim();
                string sortableValue = dr[dcName + "_sortable", DataRowVersion.Current].ToString().Trim();
                if (originalValue != currentValue ||
                    GetSortableColumnText(dr, dcName) != sortableValue)
                {
                    if (dr.Table.Columns.Contains(dcName + "_sortable")) dr[dcName + "_sortable"] = GetSortableColumnText(dr, dcName);
                    ApplyOrderItemBusinessRules(dgv.Rows[e.RowIndex]);
                    ApplyOrderBusinessRules();
                    RefreshDGVFormatting(dgv);
                }
            }
        }

        private string GetSortableColumnText(DataRow dr, string dcName)
        {
            string sortableValue = "";
            DataColumn dc = dr.Table.Columns[dcName];
            // Calculate the sortable text string...
            if (_sharedUtils.LookupTablesIsValidFKField(dc))
            {
                string lookupTable = dc.ExtendedProperties["foreign_key_dataview_name"].ToString();
                if (dr[dcName] != DBNull.Value)
                {
                    sortableValue = _sharedUtils.GetLookupDisplayMember(lookupTable, dr[dcName].ToString(), "", dr[dcName].ToString());
                    char[] Delimiter = new char[] { ' ', '\t', '\n', '\r' };
                    string[] stringTokens = sortableValue.Split(Delimiter, StringSplitOptions.RemoveEmptyEntries);
                    Decimal paddingNumber = 0.0M;
                    string alphanumericSortableValue = "";
                    foreach (string token in stringTokens)
                    {
                        if (Decimal.TryParse(token, out paddingNumber))
                        {
                            alphanumericSortableValue += paddingNumber.ToString("00000000000000000000.0000000000") + " ";
                        }
                        else
                        {
                            alphanumericSortableValue += token.PadRight(40, ' ') + " ";
                        }
                    }
                    sortableValue = alphanumericSortableValue;
                }
            }
            else if (_sharedUtils.LookupTablesIsValidCodeValueField(dc))
            {
                if (dr[dcName] != DBNull.Value)
                {
                    sortableValue = _sharedUtils.GetLookupDisplayMember("code_value_lookup", dr[dcName].ToString(), dc.ExtendedProperties["group_name"].ToString(), dr[dcName].ToString());
                }
            }
            else if (dc.DataType == typeof(int) || dc.DataType == typeof(decimal))
            {
                if (dr[dcName] != DBNull.Value)
                {
                    sortableValue = decimal.Parse(dr[dcName].ToString()).ToString("00000000000000000000.0000000000");
                }
            }
            else if (dc.DataType == typeof(DateTime))
            {
                if (dr[dcName] != DBNull.Value)
                {
                    //sortableValue = ((DateTime)dr[dcName]).ToString("u");
                    sortableValue = ((DateTime)dr[dcName]).ToString("s");
                }
            }
            else
            {
                if (dr[dcName] != DBNull.Value)
                {
                    sortableValue = dr[dcName].ToString().ToLower();
                }
            }

            return sortableValue;
        }

        private void ux_buttonPrintCrystalReport_Click(object sender, EventArgs e)
        {
            //string fullPathName = System.Windows.Forms.Application.StartupPath + "\\Reports\\" + ux_comboboxCrystalReports.Text;
            string fullPathName = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports\" + ux_comboboxCrystalReports.Text;
            if (System.IO.File.Exists(fullPathName))
            {
                // By default print just the current order...
                string pkeys = ":orderrequestid=" + ((DataRowView)_orderRequestBindingSource.Current).Row["order_request_id"].ToString();
                // Unless the user wants all orders in the Wizard printed at once...
                if (ux_checkboxPrintAllOrders.Checked) pkeys = _orderRequestPKeys;
                // Find a compatible dataview for the selected Crystal Report...
                System.Collections.Generic.Dictionary<string, string> reportsMap = _sharedUtils.GetReportsMapping();
                string dataviewName = "";
                //if (reportsMap.ContainsKey(ux_comboboxCrystalReports.Text.Trim().ToUpper()))
                if (reportsMap.ContainsKey(fullPathName.Trim().ToUpper()))
                {
                    //string dataviewNames = reportsMap[ux_comboboxCrystalReports.Text.Trim().ToUpper()];
                    string dataviewNames = reportsMap[fullPathName.Trim().ToUpper()];
                    if (dataviewNames.Length > 0)
                    {
                        dataviewName = dataviewNames.Split('|')[0].Trim().ToLower();
                    }
                }

                if (!string.IsNullOrEmpty(dataviewName))
                {
                    // Get the data using the suggested dataview...
                    DataSet ds = _sharedUtils.GetWebServiceData(dataviewName, pkeys, 0, 1000000);
                    // Get a copy of the Reports-->Dataviews Mapping...
                    if (ds.Tables.Contains(dataviewName))
                    {
                        DataGridView dgv = new DataGridView();
                        _sharedUtils.BuildReadOnlyDataGridView(dgv, ds.Tables[dataviewName]);
                        if (dgv != null &&
                            dgv.DataSource != null &&
                            dgv.DataSource.GetType() == typeof(DataTable))
                        {
                            // Okay it looks like we have a datagridview with a datasource = datatable (with FKeys and code_values resolved) - so extract it and use in the report...
                            DataTable dt = (DataTable)dgv.DataSource;
                            // Got the data so now we can generate the Crystal Report...
                            _sharedUtils.PrintReport(dt, fullPathName);
                        }
                    }
                }
            }
        }

        private void ux_buttonUpdateAttachments_Click(object sender, EventArgs e)
        {
            string orderRequestID = ((DataRowView)_orderRequestBindingSource.Current).Row["order_request_id"].ToString();
            string webOrderRequestID = ((DataRowView)_orderRequestBindingSource.Current).Row["web_order_request_id"].ToString();
            // Get the web_order_request_attach table (requestor may have uploaded NEW permits or other images pertaining to this order)...
            DataSet webOrderRequestAttachDS = _sharedUtils.GetWebServiceData("order_wizard_get_web_order_request_attach", ":weborderrequestid=" + webOrderRequestID, 0, 0);
            if (webOrderRequestAttachDS != null &&
                webOrderRequestAttachDS.Tables.Contains("order_wizard_get_web_order_request_attach"))
            {
                // Get the web order attachments the requestor may have uploaded...
                DataTable woraDT = webOrderRequestAttachDS.Tables["order_wizard_get_web_order_request_attach"];
                foreach (DataRow dr in woraDT.Rows)
                {
                    // Try to find a matching order_request_attach row for this web_order_request_attach and create one if it's not there...
                    if (_orderRequestAttach.Select("virtual_path LIKE '%" + System.IO.Path.GetFileName(dr["virtual_path"].ToString()) + "'").Length == 0)
                    {
                        DataRow newOrderRequestAttach = _orderRequestAttach.NewRow();
                        if (_orderRequestAttach.Columns.Contains("order_request_id")) newOrderRequestAttach["order_request_id"] = orderRequestID;
                        if (_orderRequestAttach.Columns.Contains("virtual_path") && woraDT.Columns.Contains("virtual_path")) newOrderRequestAttach["virtual_path"] = dr["virtual_path"];
                        if (_orderRequestAttach.Columns.Contains("thumbnail_virtual_path") && woraDT.Columns.Contains("virtual_path")) newOrderRequestAttach["thumbnail_virtual_path"] = dr["virtual_path"];
                        if (_orderRequestAttach.Columns.Contains("sort_order")) newOrderRequestAttach["sort_order"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("title") && woraDT.Columns.Contains("title")) newOrderRequestAttach["title"] = dr["title"];
                        if (_orderRequestAttach.Columns.Contains("description")) newOrderRequestAttach["description"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("content_type") && woraDT.Columns.Contains("content_type")) newOrderRequestAttach["content_type"] = dr["content_type"];
                        if (_orderRequestAttach.Columns.Contains("category_code")) newOrderRequestAttach["category_code"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("is_web_visible")) newOrderRequestAttach["is_web_visible"] = "N";
                        if (_orderRequestAttach.Columns.Contains("copyright_information")) newOrderRequestAttach["copyright_information"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("attach_cooperator_id")) newOrderRequestAttach["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
                        if (_orderRequestAttach.Columns.Contains("attach_date")) newOrderRequestAttach["attach_date"] = DateTime.Now;
                        if (_orderRequestAttach.Columns.Contains("attach_date_code")) newOrderRequestAttach["attach_date_code"] = "MM/dd/yyyy";
                        if (_orderRequestAttach.Columns.Contains("note") && woraDT.Columns.Contains("note")) newOrderRequestAttach["note"] = dr["note"];
                        _orderRequestAttach.Rows.Add(newOrderRequestAttach);

                        // Get the binary data for the attachments and save it to the local temp directory (for later uploading during save order processing)...
                        // Build the virtual and local path names...
                        string attachVirtualPath = dr["virtual_path"].ToString();
                        //if (!attachVirtualPath.Contains("~/uploads/imports/")) attachVirtualPath = "~/uploads/imports/" + attachVirtualPath; // HACK until PW puts full path into the database field 'thumbnail_virtual_path'
                        string fileName = System.IO.Path.GetFileName(attachVirtualPath);
                        string tempFilesPath = System.IO.Path.GetTempPath();
                        string attachLocalPath = tempFilesPath + "GRIN-Global CuratorTool\\Attachments\\Web Orders\\" + webOrderRequestID + "\\" + fileName;
                        // Get the attachment data...
                        byte[] attachBytes = _sharedUtils.GetAttachment(attachVirtualPath);
                        // Make sure the directory exists before writing the attachment data to disk...
                        System.IO.Directory.CreateDirectory(tempFilesPath + "GRIN-Global CuratorTool\\Attachments\\Web Orders\\" + webOrderRequestID);
                        // Create a local copy of the attachment...
                        System.IO.File.WriteAllBytes(attachLocalPath, attachBytes);
                        // Store the location of the local copy of the attachment in the database table...
                        if (_orderRequestAttach.Columns.Contains("local_path")) newOrderRequestAttach["local_path"] = attachLocalPath;
                    }
                }
            }
        }

        private void ux_datagridviewOrderRequestAttach_DragOver(object sender, DragEventArgs e)
        {
            // Okay we are in the middle of a Drag and Drop operation and the mouse is in 
            // the DGV control so lets handle this event...

            // This code will change the cursor icon to give the user feedback about whether or not
            // the drag-drop operation is allowed...
            //

            // Get the DGV object...
            DataGridView dgv = (DataGridView)sender;

            // Convert the mouse coordinates from screen to client...
            Point ptClientCoord = dgv.PointToClient(new Point(e.X, e.Y));

            // Is this an attachment file being dragged to the DGV...
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ux_datagridviewOrderRequestAttach_DragDrop(object sender, DragEventArgs e)
        {
            // The drag-drop event is coming to a close process this event to handle the dropping of
            // files into the DataGridView...

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Get the DGV object...
            DataGridView dgv = (DataGridView)sender;

            // Is this an allowed drop???
            if (e.Effect != DragDropEffects.None)
            {
                // Or are these image files being dragged in from the File Explorer...
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    string[] attachLocalPathCollection = (string[])e.Data.GetData(DataFormats.FileDrop);
                    foreach (string attachLocalPath in attachLocalPathCollection)
                    {
                        // Build the virtual and local path names...
                        string orderNumber = ((DataRowView)_orderRequestBindingSource.Current)["order_request_id"].ToString();
                        string fileName = System.IO.Path.GetFileName(attachLocalPath);
                        string attachVirtualPath = @"\order_request_attach\" + orderNumber + @"\" + fileName;

                        string returnedPath = "";
                        DataRow newOrderRequestAttach = _orderRequestAttach.NewRow();
                        if (_orderRequestAttach.Columns.Contains("order_request_id")) newOrderRequestAttach["order_request_id"] = orderNumber;
                        if (_orderRequestAttach.Columns.Contains("virtual_path")) newOrderRequestAttach["virtual_path"] = attachVirtualPath;
                        if (_orderRequestAttach.Columns.Contains("thumbnail_virtual_path")) newOrderRequestAttach["thumbnail_virtual_path"] = attachVirtualPath.Insert(attachVirtualPath.LastIndexOf('.'), "_thumbnail");
                        if (_orderRequestAttach.Columns.Contains("sort_order")) newOrderRequestAttach["sort_order"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("title")) newOrderRequestAttach["title"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("description")) newOrderRequestAttach["description"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("content_type")) newOrderRequestAttach["content_type"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("category_code")) newOrderRequestAttach["category_code"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("is_web_visible")) newOrderRequestAttach["is_web_visible"] = "N";
                        if (_orderRequestAttach.Columns.Contains("copyright_information")) newOrderRequestAttach["copyright_information"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("attach_cooperator_id")) newOrderRequestAttach["attach_cooperator_id"] = _sharedUtils.UserCooperatorID;
                        if (_orderRequestAttach.Columns.Contains("attach_date")) newOrderRequestAttach["attach_date"] = DateTime.Now;
                        if (_orderRequestAttach.Columns.Contains("attach_date_code")) newOrderRequestAttach["attach_date_code"] = "MM/dd/yyyy";
                        if (_orderRequestAttach.Columns.Contains("note")) newOrderRequestAttach["note"] = DBNull.Value;
                        if (_orderRequestAttach.Columns.Contains("local_path")) newOrderRequestAttach["local_path"] = attachLocalPath;
                        _orderRequestAttach.Rows.Add(newOrderRequestAttach);
                    }
                }
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void ux_buttonEditCooperator_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string fieldName = btn.Tag.ToString();
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory());
            System.IO.FileInfo[] dllFiles = di.GetFiles("Wizards\\CooperatorWizard.dll", System.IO.SearchOption.AllDirectories);
            if (dllFiles != null && dllFiles.Length > 0)
            {
                for (int i = 0; i < dllFiles.Length; i++)
                {
                    System.Reflection.Assembly newAssembly = System.Reflection.Assembly.LoadFile(dllFiles[i].FullName);
                    foreach (System.Type t in newAssembly.GetTypes())
                    {
                        if (t.GetInterface("IGRINGlobalDataWizard", true) != null)
                        {
                            System.Reflection.ConstructorInfo constInfo = t.GetConstructor(new Type[] { typeof(string), typeof(SharedUtils) });
                            if (constInfo != null)
                            {
                                DataRow currentOrderRequest = ((DataRowView)_orderRequestBindingSource.Current).Row;
                                if (currentOrderRequest != null &&
                                    currentOrderRequest.Table.Columns.Contains(fieldName))
                                {
                                    string pkeyCollection = ":cooperatorid=" + currentOrderRequest[fieldName].ToString().Trim();
                                    // Instantiate an object of this type to load...
                                    Form wizardForm = (Form)constInfo.Invoke(new object[] { pkeyCollection, _sharedUtils });
                                    //wizardForm.Owner = this;
                                    //wizardForm.FormClosing += new FormClosingEventHandler(wizard_FormClosing);
                                    wizardForm.Show();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
