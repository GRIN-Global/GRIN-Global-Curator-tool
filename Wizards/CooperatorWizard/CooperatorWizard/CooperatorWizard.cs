﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GRINGlobal.Client.Common;

namespace CooperatorWizard
{

    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class CooperatorWizard : Form, IGRINGlobalDataWizard
    {
        SharedUtils _sharedUtils;
        string _originalPKeys = "";
        string _cooperatorPKeys = "";
        string _webCooperatorPKeys = "";
        string _webOrderRequestPKeys = "";
        DataTable _cooperator;
        BindingSource _cooperatorBindingSource;
        DataTable _webCooperator;
        BindingSource _webCooperatorBindingSource;
        DataTable _webOrderRequestCooperator;
        BindingSource _webOrderRequestCooperatorBindingSource;
        DataSet _changedRecords = new DataSet();
        char _lastDGVCharPressed;
        string _finalRecipientCooperatorID = "";
        string _shipToCooperatorID = "";
        DataRow _webOrderRequestRow;

        public CooperatorWizard(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            _cooperatorBindingSource = new BindingSource();
            _webCooperatorBindingSource = new BindingSource();
            _webOrderRequestCooperatorBindingSource = new BindingSource();
            _sharedUtils = sharedUtils;
            _originalPKeys = pKeys;
            // Ignore all pkey tokens except the cooperator_id pkeys...
            foreach (string pkeyToken in pKeys.Split(';'))
            {
                if (pkeyToken.Split('=')[0].Trim().ToUpper() == ":COOPERATORID") _cooperatorPKeys = pkeyToken;
                if (pkeyToken.Split('=')[0].Trim().ToUpper() == ":WEBCOOPERATORID") _webCooperatorPKeys = pkeyToken;
                if (pkeyToken.Split('=')[0].Trim().ToUpper() == ":WEBORDERREQUESTID") _webOrderRequestPKeys = pkeyToken;
            }
        }

        private void CooperatorWizard_Load(object sender, EventArgs e)
        {
            this.Text = FormName;

            // Set focus to the appropiate tabpage based on parameters passed in...
            if (!string.IsNullOrEmpty(_webOrderRequestPKeys))
            {
                ux_tabcontrolMain.SelectedTab = ux_tabpageWebOrder;
            }
            else if (!string.IsNullOrEmpty(_webCooperatorPKeys))
            {
                ux_tabcontrolMain.SelectedTab = ux_tabpageWebCooperator;
            }
            else
            {
                ux_tabcontrolMain.SelectedTab = ux_tabpageCooperator;
            }

            // BUild the three tab pages...
            BuildCooperatorPage();
            BuildWebCooperatorPage();
            BuildWebOrderRequestPage();

            // Get language translations for the components and controls in this applicataion...
            if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
        }

        public string FormName
        {
            get
            {
                return "Cooperator Wizard";
            }
        }

        public DataTable ChangedRecords
        {
            get
            {
                DataTable dt = new DataTable();
                if (_changedRecords.Tables.Contains(_cooperator.TableName))
                {
                    dt = _changedRecords.Tables[_cooperator.TableName].Copy();
                }
                return dt;
            }
        }

        public string PKeyName
        {
            get
            {
                return "cooperator_id";
            }
        }

        public string FinalRecipientCooperatorID
        {
            get
            {
                return _finalRecipientCooperatorID;
            }
        }
        public string ShipToCooperatorID
        {
            get
            {
                return _shipToCooperatorID;
            }
        }

        private void CooperatorWizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form wizard = (Form)sender;

            // The user might be closing the form during the middle of edit changes in the datagridview - if so ask the
            // user if they would like to save their data...
            int intRowEdits = 0;

            _cooperatorBindingSource.EndEdit();
            if (_cooperator.GetChanges() != null) intRowEdits = _cooperator.GetChanges().Rows.Count;

            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to cancel your edits and close this window?", "Cancel Edits and Close", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "CooperatorWizard_FormClosingMessage1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = intRowEdits.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            // Show the warning dialog message if there are unsaved edits...
            if (intRowEdits > 0 && DialogResult.No == ggMessageBox.ShowDialog())
            {
                e.Cancel = true;
            }

            ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have not selected a cooperator for all addresses in this order ({0} and/or {1}) - are you sure you want to close this window?", "Cancel Close", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "CooperatorWizard_FormClosingMessage2";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            argsArray = new string[100];
            argsArray[0] = ux_radiobuttonPrimaryAddress.Text.ToString();
            argsArray[1] = ux_radiobuttonShipToAddress.Text.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            // Show the warning dialog message if there are unsaved edits and this wizard was
            // launched from another source (like the Order Wizard) - this can be tested by checking
            // the _webOrderRequestPKeys to see if pkeys were passed into this wizard...
            if (wizard.Visible &&
                !string.IsNullOrEmpty(_webOrderRequestPKeys) &&
                !string.IsNullOrEmpty(ux_textboxWebOrderNumber.Text) &&
                ((ux_radiobuttonPrimaryAddress.Enabled && string.IsNullOrEmpty(_finalRecipientCooperatorID)) ||
                (ux_radiobuttonShipToAddress.Enabled && string.IsNullOrEmpty(_shipToCooperatorID))) &&
                DialogResult.No == ggMessageBox.ShowDialog())
            {
                e.Cancel = true;
            }
        }

        #region DGV control logic...
        private void ux_datagridview_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            // Get the defaultview of the datatable used by the dgv...
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }

            // Begin processing cell formatting based on the datatype of the field...
            if (dv != null &&
                e.ColumnIndex > -1 &&
                e.RowIndex > -1 &&
                e.RowIndex < dv.Count)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                // Format FKey fields...
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    e.RowIndex < dv.Count &&
                    dv[e.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (dv[e.RowIndex][e.ColumnIndex] != DBNull.Value)
                    {
                        e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim(), dv[e.RowIndex][e.ColumnIndex].ToString().Trim(), "", dv[e.RowIndex][e.ColumnIndex].ToString().Trim());
                    }
                    dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    e.FormattingApplied = true;
                }
                // Format date/time fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        e.Value = ((DateTime)dv[e.RowIndex][e.ColumnIndex]).ToString(dateFormat);
                        e.FormattingApplied = true;
                    }
                }
                // Format integer fields...
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    (dc.DataType == typeof(int) ||
                    dc.DataType == typeof(Int16) ||
                    dc.DataType == typeof(Int32) ||
                    dc.DataType == typeof(Int64)))
                {
                    int junk;
                    if (!int.TryParse(e.Value.ToString(), out junk))
                    {
                        dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    }
                }

                if (dc.ReadOnly)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }

                if (dc.ExtendedProperties.Contains("is_nullable") &&
                    dc.ExtendedProperties["is_nullable"].ToString() == "N" &&
                    string.IsNullOrEmpty(dv[e.RowIndex][e.ColumnIndex].ToString()))
                {
                    e.CellStyle.BackColor = Color.Plum;
                }
            }
        }

        private void ux_datagridview_CellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }
            if (dv != null && e.ColumnIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        DateTime formattedDate;
                        if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out formattedDate))
                        {
                            e.Value = formattedDate;
                            e.ParsingApplied = true;
                        }
                    }
                }
            }
        }

        private void ux_datagridview_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            string errorMessage = e.Exception.Message;
            int columnWithError = -1;

            // Find the cell the error belongs to (don't use e.ColumnIndex because it points to the current cell *NOT* the offending cell)...
            foreach (DataGridViewColumn col in dgv.Columns)
            {
                if (errorMessage.Contains(col.Name))
                {
                    dgv[col.Name, e.RowIndex].ErrorText = errorMessage;
                    columnWithError = col.Index;
                }
            }
        }

        private void ux_datagridview_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataTable dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;
            string columnName = dgv.CurrentCell.OwningColumn.Name;
            DataColumn dc = dt.Columns[columnName];
            DataRow dr;

            if (_sharedUtils.LookupTablesIsValidFKField(dc))
            {
                string luTableName = dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim();
                dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
                string suggestedFilter = dgv.CurrentCell.EditedFormattedValue.ToString();
                if (_lastDGVCharPressed > 0) suggestedFilter = _lastDGVCharPressed.ToString();
                GRINGlobal.Client.Common.LookupTablePicker ltp = new GRINGlobal.Client.Common.LookupTablePicker(_sharedUtils, columnName, dr, suggestedFilter);
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    if (dr != null)
                    {
                        if (ltp.NewKey != null && dr[dgv.CurrentCell.ColumnIndex].ToString().Trim() != ltp.NewKey.Trim())
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = ltp.NewKey.Trim();
                            dgv.CurrentCell.Value = ltp.NewValue.Trim();
                        }
                        else if (ltp.NewKey == null)
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = DBNull.Value;
                            dgv.CurrentCell.Value = "";
                        }
                        dr.SetColumnError(dgv.CurrentCell.ColumnIndex, null);
                    }
                }
                dgv.EndEdit();
            }
        }

        private void ux_datagridview_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else if (dgv.DataSource.GetType() == typeof(DataTable))
            {
                dv = ((DataTable)(dgv.DataSource)).DefaultView;
            }

            // Reset the last char pressed global variable...
            _lastDGVCharPressed = (char)0;

            // If we are: 
            //  1) in edit mode, 
            //  2) the current cell is parked on a cell that is a FK lookup 
            //  3) and the Alt and Ctrl keys are not down
            // Then remember the keypress so that it can be passed into the Lookup Picker dialog...
            if (dv != null &&
                dgv.CurrentCell != null &&
                dgv.CurrentCell.ColumnIndex > -1 &&
                dgv.CurrentCell.RowIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[dgv.CurrentCell.ColumnIndex];
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    dgv.CurrentCell.RowIndex < dv.Count &&
                    dv[dgv.CurrentCell.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (!e.Alt && !e.Control)
                    {
                        KeysConverter kc = new KeysConverter();
                        string lastChar = kc.ConvertToString(e.KeyValue).Replace("NumPad", "");
                        if (lastChar.Length == 1)
                        {
                            if (e.Shift)
                            {
                                _lastDGVCharPressed = lastChar.ToUpper()[0];
                            }
                            else
                            {
                                _lastDGVCharPressed = lastChar.ToLower()[0];
                            }
                        }
                    }
                }
            }
        }

        private void ux_datagridview_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (_sharedUtils.ProcessDGVEditShortcutKeys(dgv, e, _sharedUtils.UserCooperatorID))
            {
                RefreshDGVFormatting(dgv);
            }
        }

        private void RefreshDGVFormatting(DataGridView dgv)
        {
            // Refresh the format for each row in the DGV...
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                RefreshDGVRowFormatting(dgvr);
            }
            // Now force a re-paint of the DGV...
            dgv.Refresh();
        }

        private void RefreshDGVRowFormatting(DataGridViewRow dgvr)
        {
            foreach (DataGridViewCell dgvc in dgvr.Cells)
            {
                // Reset the background and foreground color...
                dgvc.Style.BackColor = Color.Empty;
                dgvc.Style.ForeColor = Color.Empty;
                dgvc.Style.SelectionBackColor = Color.Empty;
                dgvc.Style.SelectionForeColor = Color.Empty;
            }
            // If the row has changes make each changed cell yellow...
            DataRow dr = ((DataRowView)dgvr.DataBoundItem).Row;
            if (dr.RowState == DataRowState.Modified)
            {
                foreach (DataGridViewCell dgvc in dgvr.Cells)
                {
                    // If the cell has been changed make it yellow...
                    if (!dr[dgvc.ColumnIndex, DataRowVersion.Original].Equals(dr[dgvc.ColumnIndex, DataRowVersion.Current]))
                    {
                        dgvc.Style.BackColor = Color.Yellow;
                        dr.SetColumnError(dgvc.ColumnIndex, null);
                    }
                    // Use default background color for this cell...
                    else
                    {
                        dgvc.Style.BackColor = Color.Empty;
                    }
                }
            }
        }

        #endregion

        private void BuildCooperatorPage()
        {
            // Get a refreshed list of the cooperators for the cooperator table...
            RefreshCooperatorList();

            // Now bind it to the DGV on the first tabpage...
            if (_cooperator != null)
            {
                // Bind the DGV to the binding source...
                ux_datagridviewCooperator.DataSource = _cooperatorBindingSource;
                // Build the DGV using the new table and bind it to the DGV's binding source (this happens in the Build method)...
                _sharedUtils.BuildEditDataGridView(ux_datagridviewCooperator, _cooperator);
            }
        }

        private void BuildWebCooperatorPage()
        {
            // If the user is working in the Web Cooperator tab and
            // the Last Name was supplied by the user pre-populate the cooperator table with matching records...
            //if(ux_tabcontrolMain.SelectedTab.Name == "ux_tabpageWebCooperator" && !string.IsNullOrEmpty(ux_textboxWebLastName.Text))
            if (ux_tabcontrolMain.SelectedTab.Name == "ux_tabpageWebCooperator")
            {
                string wildCard = "";
                if (ux_radiobuttonPartialWebMatch.Checked) wildCard = "%";
                if (ux_checkboxWebCooperatorLastName.Checked && !string.IsNullOrEmpty(ux_textboxWebLastName.Text))
                {
                    ux_textboxLastName.Text = wildCard + ux_textboxWebLastName.Text + wildCard;
                }
                else
                {
                    ux_textboxLastName.Text = "";
                }
                if (ux_checkboxWebCooperatorFirstName.Checked && !string.IsNullOrEmpty(ux_textboxWebFirstName.Text))
                {
                    ux_textboxFirstName.Text = wildCard + ux_textboxWebFirstName.Text + wildCard;
                }
                else
                {
                    ux_textboxFirstName.Text = "";
                }
                if (ux_checkboxWebCooperatorOrganization.Checked && !string.IsNullOrEmpty(ux_textboxWebOrganization.Text))
                {
                    ux_textboxOrganization.Text = wildCard + ux_textboxWebOrganization.Text + wildCard;
                }
                else
                {
                    ux_textboxOrganization.Text = "";
                }
                if (!string.IsNullOrEmpty(ux_textboxWebEMail.Text))
                {
                    ux_textboxEMail.Text = wildCard + ux_textboxWebEMail.Text + wildCard;
                }
                else
                {
                    ux_textboxEMail.Text = "";
                }
                if (ux_checkboxWebCooperatorLastName.Checked && !string.IsNullOrEmpty(ux_textboxWebPhoneNumber.Text))
                {
                    ux_textboxPhoneNumber.Text = wildCard + ux_textboxWebPhoneNumber.Text + wildCard;
                }
                else
                {
                    ux_textboxPhoneNumber.Text = "";
                }
                ux_textboxCooperatorID.Text = "";
                RefreshCooperatorList();
                // And then update the binding source's data...
                _cooperatorBindingSource.DataSource = _cooperator;
                // Bind the web cooperator detail DGV to this binding source...
                ux_datagridviewWebCooperatorDetail.DataSource = _cooperatorBindingSource;
                // Build the DGV using the new table and bind it to the DGV's binding source (this happens in the Build method)...
                _sharedUtils.BuildEditDataGridView(ux_datagridviewWebCooperatorDetail, _cooperator);
            }

            // Get a refreshed list of the web cooperators for the web cooperator table...
            RefreshWebCooperatorList();

            // Now bind it to the DGV on the second tabpage...
            if (_webCooperator != null)
            {
                // Bind the DGV to the binding source...
                ux_datagridviewWebCooperatorMaster.DataSource = _webCooperatorBindingSource;
                // Build the DGV using the new table and bind it to the DGV's binding source (this happens in the Build method)...
                _sharedUtils.BuildReadOnlyDataGridView(ux_datagridviewWebCooperatorMaster, _webCooperator);
            }
        }

        private void ux_datagridviewWebCooperatorMaster_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (dgv.DataSource.GetType() == typeof(BindingSource) &&
                ((BindingSource)dgv.DataSource).DataSource.GetType() == typeof(DataTable) &&
                ((BindingSource)dgv.DataSource).DataSource != null &&
                ((DataTable)((BindingSource)dgv.DataSource).DataSource).Rows.Count > 0 &&
                e.RowIndex >= 0)
            {
                DataRow dr = ((DataRowView)dgv.Rows[e.RowIndex].DataBoundItem).Row;
                ApplyMasterDetailBusinessRules(dr);
            }
        }

        private void ApplyMasterDetailBusinessRules(DataRow drMaster)
        {
            string cooperatorRowFilter = "";
            string duplicateCooperatorCheck = "";

            // Process Last Name textbox...
            if (!string.IsNullOrEmpty(drMaster["last_name"].ToString()))
            {
                duplicateCooperatorCheck += "last_name = '" + drMaster["last_name"].ToString().Replace("'", "''") + "' AND ";
                if (ux_checkboxWebCooperatorLastName.Checked) cooperatorRowFilter += "last_name = '" + drMaster["last_name"].ToString().Replace("'", "''") + "' AND ";
            }

            // Process First Name textbox...
            if (!string.IsNullOrEmpty(drMaster["first_name"].ToString()))
            {
                duplicateCooperatorCheck += "first_name = '" + drMaster["first_name"].ToString().Replace("'", "''") + "' AND ";
                if (ux_checkboxWebCooperatorFirstName.Checked) cooperatorRowFilter += "first_name = '" + drMaster["first_name"].ToString().Replace("'", "''") + "' AND ";
            }

            // Process Organization textbox...
            if (!string.IsNullOrEmpty(drMaster["organization"].ToString()))
            {
                duplicateCooperatorCheck += "organization = '" + drMaster["organization"].ToString().Replace("'", "''") + "' AND ";
                if (ux_checkboxWebCooperatorOrganization.Checked) cooperatorRowFilter += "organization = '" + drMaster["organization"].ToString().Replace("'", "''") + "' AND ";
            }

            // Process Address Line 1 textbox...
            if (!string.IsNullOrEmpty(drMaster["address_line1"].ToString()))
            {
                duplicateCooperatorCheck += "address_line1 = '" + drMaster["address_line1"].ToString().Replace("'", "''") + "' AND ";
                if (ux_checkboxWebCooperatorAddressLine1.Checked) cooperatorRowFilter += "address_line1 = '" + drMaster["address_line1"].ToString().Replace("'", "''") + "' AND ";
            }

            // Process geography_id textbox...
            if (!string.IsNullOrEmpty(drMaster["geography_id"].ToString()))
            {
                string geo_id = _sharedUtils.GetLookupValueMember(null, drMaster.Table.Columns["geography_id"].ExtendedProperties["foreign_key_dataview_name"].ToString(), drMaster["geography_id"].ToString(), "", "-1");
                if (geo_id != "-1") duplicateCooperatorCheck += "geography_id = " + geo_id + " AND ";
                if (ux_checkboxWebCooperatorGeography.Checked && geo_id != "-1") cooperatorRowFilter += "geography_id = " + geo_id + " AND ";
            }

            cooperatorRowFilter = cooperatorRowFilter.Trim().TrimEnd(new char[] { 'A', 'N', 'D' });
            duplicateCooperatorCheck = duplicateCooperatorCheck.Trim().TrimEnd(new char[] { 'A', 'N', 'D' });

            // Update the controls for the web cooperator tab...
            if (ux_datagridviewWebCooperatorDetail.DataSource != null)
            {
                // Filter the records showing in the details DGV to only those associated with the selected web cooperator (master record)...
                ((DataTable)((BindingSource)ux_datagridviewWebCooperatorDetail.DataSource).DataSource).DefaultView.RowFilter = cooperatorRowFilter;
                // Disable the button to create a cooperator if one already exists...
                ux_buttonCreateNewCooperator.Enabled = true;
                if (((DataTable)((BindingSource)ux_datagridviewWebCooperatorDetail.DataSource).DataSource).Select(duplicateCooperatorCheck).Length > 0) ux_buttonCreateNewCooperator.Enabled = false;
            }

        }

        private void BuildWebOrderRequestPage()
        {
            // HACK!!!  These AutoScroll lines are added here so that the VS Designer would stop messing up the width
            //          of Panel1 to accomodate the vertical scrollbar...
            ux_splitcontainerWebOrder.Panel1.AutoScroll = false;
            if (ux_panelWebOrderRequestSearchCriteria.Size.Height > ux_splitcontainerWebOrder.Panel1.Height) ux_splitcontainerWebOrder.Panel1.AutoScroll = true;

            // Get a refreshed list of the web cooperators for the web cooperator table...
            ux_datagridviewWebOrderRequestCooperator.ClearSelection();
            RefreshWebOrderRequestList();

            // Adjust the user interface to fit the current situation...

            // If this wizard was started from an app that did not pass in web order request PKEYS
            // make the web order request tab read-only...
            if (string.IsNullOrEmpty(_webOrderRequestPKeys))
            {
                //ux_groupboxWebOrderAddress.Enabled = false;
                ux_groupboxWebOrderCooperatorMatchCriteria.Enabled = false;
                ux_buttonWebOrderCreateNewCooperator.Enabled = false;
                ux_datagridviewWebOrderRequestCooperator.Enabled = false;
            }
            else
            {
                ux_groupboxWebOrderAddress.Enabled = true;
                ux_groupboxWebOrderCooperatorMatchCriteria.Enabled = true;
                ux_buttonWebOrderCreateNewCooperator.Enabled = true;
                ux_datagridviewWebOrderRequestCooperator.Enabled = true;
            }

            if (_webOrderRequestRow != null &&
                _webOrderRequestRow.Table.Columns.Contains("organization") &&
                _webOrderRequestRow.Table.Columns.Contains("address_line1") &&
                _webOrderRequestRow.Table.Columns.Contains("address_line2") &&
                _webOrderRequestRow.Table.Columns.Contains("address_line3") &&
                _webOrderRequestRow.Table.Columns.Contains("city") &&
                _webOrderRequestRow.Table.Columns.Contains("postal_index") &&
                _webOrderRequestRow.Table.Columns.Contains("geography_id") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_address_line1") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_address_line2") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_address_line3") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_city") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_postal_index") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_geography_id") &&
                _webOrderRequestRow["address_line1"].ToString() == _webOrderRequestRow["ship_to_address_line1"].ToString() &&
                _webOrderRequestRow["address_line2"].ToString() == _webOrderRequestRow["ship_to_address_line2"].ToString() &&
                _webOrderRequestRow["address_line3"].ToString() == _webOrderRequestRow["ship_to_address_line3"].ToString() &&
                _webOrderRequestRow["city"].ToString() == _webOrderRequestRow["ship_to_city"].ToString() &&
                _webOrderRequestRow["postal_index"].ToString() == _webOrderRequestRow["ship_to_postal_index"].ToString() &&
                _webOrderRequestRow["geography_id"].ToString() == _webOrderRequestRow["ship_to_geography_id"].ToString())
            {
                ux_radiobuttonShipToAddress.Enabled = false;
                ux_radiobuttonShipToAddress.Visible = false;
            }
            else
            {
                ux_radiobuttonShipToAddress.Enabled = true;
                ux_radiobuttonShipToAddress.Visible = true;
            }

            // Update the color of the final recipient radio button...
            if (string.IsNullOrEmpty(_finalRecipientCooperatorID))
            {
                ux_radiobuttonPrimaryAddress.ForeColor = Color.Red;
            }
            else
            {
                ux_radiobuttonPrimaryAddress.ForeColor = Color.Empty;
            }
            // Update the color of the ship_to radio button...
            if (string.IsNullOrEmpty(_shipToCooperatorID))
            {
                ux_radiobuttonShipToAddress.ForeColor = Color.Red;
            }
            else
            {
                ux_radiobuttonShipToAddress.ForeColor = Color.Empty;
            }

            // Now bind it to the DGV on the Web Order tabpage...
            if (_webOrderRequestCooperator != null)
            {
                // Bind the DGV to the binding source...
                ux_datagridviewWebOrderRequestCooperator.DataSource = _webOrderRequestCooperatorBindingSource;
                // Build the DGV using the new table and bind it to the DGV's binding source (this happens in the Build method)...
                _sharedUtils.BuildReadOnlyDataGridView(ux_datagridviewWebOrderRequestCooperator, _webOrderRequestCooperator);
                ux_datagridviewWebOrderRequestCooperator.Sort(ux_datagridviewWebOrderRequestCooperator.Columns["match_score"], ListSortDirection.Descending);

                // Toggle the radio buttons to force a refresh of the data in the controls...
                if (ux_radiobuttonPrimaryAddress.Checked)
                {
                    ux_radiobuttonPrimaryAddress.Checked = false;
                    ux_radiobuttonPrimaryAddress.Checked = true;
                }
                else
                {
                    ux_radiobuttonShipToAddress.Checked = false;
                    ux_radiobuttonShipToAddress.Checked = true;
                }

                // Populate the coop and web coop tabs with search criteria (in case the user wants to edit existing coop records)...
                if (ux_checkboxWebOrderCooperatorLastName.Checked)
                {
                    ux_textboxWebLastName.Text = ux_textboxWebOrderRequestLastName.Text;
                    ux_textboxLastName.Text = ux_textboxWebOrderRequestLastName.Text;
                }
                if (ux_checkboxWebOrderCooperatorFirstName.Checked)
                {
                    ux_textboxWebFirstName.Text = ux_textboxWebOrderRequestFirstName.Text;
                    ux_textboxFirstName.Text = ux_textboxWebOrderRequestFirstName.Text;
                }
                if (ux_checkboxWebOrderCooperatorOrganization.Checked)
                {
                    ux_textboxWebOrganization.Text = ux_textboxWebOrderRequestOrganization.Text;
                    ux_textboxOrganization.Text = ux_textboxWebOrderRequestOrganization.Text;
                }
            }

            // Populate the web order request textbox with the assembly parameters passed in...
            string[] webOrderNos = _webOrderRequestPKeys.Split('=');
            if (webOrderNos.Length == 2) ux_textboxWebOrderNumber.Text = webOrderNos[1];

            // Make sure the cooperator datagridview is showing the first row (which should be the best match)...
            if (ux_datagridviewWebOrderRequestCooperator.Rows.Count > 0)
            {
                ux_datagridviewWebOrderRequestCooperator.FirstDisplayedScrollingRowIndex = 0;
            }
        }

        private void RefreshCooperatorList()
        {
            DataSet ds = new DataSet();
            string seQuery = "";
            string seWildCard = "";
            string searchPKeys = "";

            // First get the cooperator_ids for the orginal search criteria (_originalPKeys)...
            ds = _sharedUtils.GetWebServiceData("cooperator_wizard_get_cooperator", _cooperatorPKeys, 0, 0);
            searchPKeys = ":cooperatorid=";
            if (ds.Tables.Contains("cooperator_wizard_get_cooperator"))
            {
                foreach (DataRow dr in ds.Tables["cooperator_wizard_get_cooperator"].Rows)
                {
                    searchPKeys += dr["cooperator_id"].ToString() + ",";
                }
            }

            if (ux_radiobuttonPartialMatch.Checked) seWildCard = "%";

            // Process Last Name textbox...
            if (!string.IsNullOrEmpty(ux_textboxLastName.Text))
            {
                if (ux_textboxLastName.Text.Contains("*") ||
                    ux_textboxLastName.Text.Contains("%") ||
                    ux_textboxLastName.Text.Contains("_") ||
                    ux_radiobuttonPartialMatch.Checked)
                {
                    seQuery += "@cooperator.last_name LIKE '" + seWildCard + ux_textboxLastName.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@cooperator.last_name LIKE '" + ux_textboxLastName.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process First Name textbox...
            if (!string.IsNullOrEmpty(ux_textboxFirstName.Text))
            {
                if (ux_textboxFirstName.Text.Contains("*") ||
                    ux_textboxFirstName.Text.Contains("%") ||
                    ux_textboxFirstName.Text.Contains("_") ||
                    ux_radiobuttonPartialMatch.Checked)
                {
                    seQuery += "@cooperator.first_name LIKE '" + seWildCard + ux_textboxFirstName.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@cooperator.first_name LIKE '" + ux_textboxFirstName.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process Organization textbox...
            if (!string.IsNullOrEmpty(ux_textboxOrganization.Text))
            {
                if (ux_textboxOrganization.Text.Contains("*") ||
                    ux_textboxOrganization.Text.Contains("%") ||
                    ux_textboxOrganization.Text.Contains("_") ||
                    ux_radiobuttonPartialMatch.Checked)
                {
                    seQuery += "@cooperator.organization LIKE '" + seWildCard + ux_textboxOrganization.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@cooperator.organization LIKE '" + ux_textboxOrganization.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process EMail textbox...
            if (!string.IsNullOrEmpty(ux_textboxEMail.Text))
            {
                if (ux_textboxEMail.Text.Contains("*") ||
                    ux_textboxEMail.Text.Contains("%") ||
                    ux_textboxEMail.Text.Contains("_") ||
                    ux_radiobuttonPartialMatch.Checked)
                {
                    seQuery += "@cooperator.email LIKE '" + seWildCard + ux_textboxEMail.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@cooperator.email LIKE '" + ux_textboxEMail.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process Phone Number textbox...
            if (!string.IsNullOrEmpty(ux_textboxPhoneNumber.Text))
            {
                if (ux_textboxPhoneNumber.Text.Contains("*") ||
                    ux_textboxPhoneNumber.Text.Contains("%") ||
                    ux_textboxPhoneNumber.Text.Contains("_") ||
                    ux_radiobuttonPartialMatch.Checked)
                {
                    seQuery += "@cooperator.primary_phone LIKE '" + seWildCard + ux_textboxPhoneNumber.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@cooperator.primary_phone LIKE '" + ux_textboxPhoneNumber.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process CooperatorID textbox...
            if (!string.IsNullOrEmpty(ux_textboxCooperatorID.Text))
            {
                if (ux_textboxCooperatorID.Text.Contains("*") ||
                    ux_textboxCooperatorID.Text.Contains("%") ||
                    ux_textboxCooperatorID.Text.Contains("_") ||
                    ux_radiobuttonPartialMatch.Checked)
                {
                    seQuery += "@cooperator.cooperator_id LIKE '" + seWildCard + ux_textboxCooperatorID.Text.Replace("*", "%") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@cooperator.cooperator_id LIKE '" + ux_textboxCooperatorID.Text + "' AND ";
                }
            }

            DataSet dsSEPKeysResults = _sharedUtils.SearchWebService(seQuery.Trim().TrimEnd(new char[] { 'A', 'N', 'D' }), true, true, "", "cooperator", 0, 0);
            if (dsSEPKeysResults.Tables.Contains("SearchResult"))
            {
                foreach (DataRow dr in dsSEPKeysResults.Tables["SearchResult"].Rows)
                {
                    searchPKeys += dr["ID"].ToString() + ",";
                }
            }

            // Remove the last trailing comma...
            searchPKeys = searchPKeys.TrimEnd(',');

            // Finally go get the full collection of cooperator_ids...
            ds = _sharedUtils.GetWebServiceData("cooperator_wizard_get_cooperator", searchPKeys, 0, 0);
            if (ds.Tables.Contains("cooperator_wizard_get_cooperator"))
            {
                _cooperator = ds.Tables["cooperator_wizard_get_cooperator"].Copy();
            }
            else if (_cooperator == null)
            {
                _cooperator = new DataTable();
            }
            // And then update the binding source's data...
            _cooperatorBindingSource.DataSource = _cooperator;
        }

        private void RefreshWebCooperatorList()
        {
            DataSet ds = new DataSet();
            string seQuery = "";
            string seWildCard = "";
            string searchPKeys = "";

            // First get the cooperator_ids for the orginal search criteria (_originalPKeys)...
            ds = _sharedUtils.GetWebServiceData("cooperator_wizard_get_web_cooperator", _webCooperatorPKeys, 0, 0);
            searchPKeys = ":webcooperatorid=";
            if (ds.Tables.Contains("cooperator_wizard_get_web_cooperator"))
            {
                foreach (DataRow dr in ds.Tables["cooperator_wizard_get_web_cooperator"].Rows)
                {
                    searchPKeys += dr["web_cooperator_id"].ToString() + ",";
                }
            }
            if (ux_radiobuttonPartialWebMatch.Checked) seWildCard = "%";

            // Process Last Name textbox...
            if (!string.IsNullOrEmpty(ux_textboxWebLastName.Text))
            {
                if (ux_textboxWebLastName.Text.Contains("*") ||
                    ux_textboxWebLastName.Text.Contains("%") ||
                    ux_textboxWebLastName.Text.Contains("_") ||
                    ux_radiobuttonPartialWebMatch.Checked)
                {
                    seQuery += "@web_cooperator.last_name LIKE '" + seWildCard + ux_textboxWebLastName.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@web_cooperator.last_name LIKE '" + ux_textboxWebLastName.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process First Name textbox...
            if (!string.IsNullOrEmpty(ux_textboxWebFirstName.Text))
            {
                if (ux_textboxWebFirstName.Text.Contains("*") ||
                    ux_textboxWebFirstName.Text.Contains("%") ||
                    ux_textboxWebFirstName.Text.Contains("_") ||
                    ux_radiobuttonPartialWebMatch.Checked)
                {
                    seQuery += "@web_cooperator.first_name LIKE '" + seWildCard + ux_textboxWebFirstName.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@web_cooperator.first_name LIKE '" + ux_textboxWebFirstName.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process Organization textbox...
            if (!string.IsNullOrEmpty(ux_textboxWebOrganization.Text))
            {
                if (ux_textboxWebOrganization.Text.Contains("*") ||
                    ux_textboxWebOrganization.Text.Contains("%") ||
                    ux_textboxWebOrganization.Text.Contains("_") ||
                    ux_radiobuttonPartialWebMatch.Checked)
                {
                    seQuery += "@web_cooperator.organization LIKE '" + seWildCard + ux_textboxWebOrganization.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@web_cooperator.organization LIKE '" + ux_textboxWebOrganization.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process EMail textbox...
            if (!string.IsNullOrEmpty(ux_textboxWebEMail.Text))
            {
                if (ux_textboxWebEMail.Text.Contains("*") ||
                    ux_textboxWebEMail.Text.Contains("%") ||
                    ux_textboxWebEMail.Text.Contains("_") ||
                    ux_radiobuttonPartialWebMatch.Checked)
                {
                    seQuery += "@web_cooperator.email LIKE '" + seWildCard + ux_textboxWebEMail.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@web_cooperator.email LIKE '" + ux_textboxWebEMail.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process Phone Number textbox...
            if (!string.IsNullOrEmpty(ux_textboxWebPhoneNumber.Text))
            {
                if (ux_textboxWebPhoneNumber.Text.Contains("*") ||
                    ux_textboxWebPhoneNumber.Text.Contains("%") ||
                    ux_textboxWebPhoneNumber.Text.Contains("_") ||
                    ux_radiobuttonPartialWebMatch.Checked)
                {
                    seQuery += "@web_cooperator.primary_phone LIKE '" + seWildCard + ux_textboxWebPhoneNumber.Text.Replace("*", "%").Replace("'", "''") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@web_cooperator.primary_phone LIKE '" + ux_textboxWebPhoneNumber.Text.Replace("'", "''") + "' AND ";
                }
            }

            // Process CooperatorID textbox...
            if (!string.IsNullOrEmpty(ux_textboxWebCooperatorID.Text))
            {
                if (ux_textboxWebCooperatorID.Text.Contains("*") ||
                    ux_textboxWebCooperatorID.Text.Contains("%") ||
                    ux_textboxWebCooperatorID.Text.Contains("_") ||
                    ux_radiobuttonPartialWebMatch.Checked)
                {
                    seQuery += "@web_cooperator.web_cooperator_id LIKE '" + seWildCard + ux_textboxWebCooperatorID.Text.Replace("*", "%") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@web_cooperator.web_cooperator_id LIKE '" + ux_textboxWebCooperatorID.Text + "' AND ";
                }
            }

            DataSet dsSEPKeysResults = _sharedUtils.SearchWebService(seQuery.Trim().TrimEnd(new char[] { 'A', 'N', 'D' }), true, true, "", "web_cooperator", 0, 0);
            if (dsSEPKeysResults.Tables.Contains("SearchResult"))
            {
                foreach (DataRow dr in dsSEPKeysResults.Tables["SearchResult"].Rows)
                {
                    searchPKeys += dr["ID"].ToString() + ",";
                }
            }

            // Remove the last trailing comma...
            searchPKeys = searchPKeys.TrimEnd(',');

            // Finally go get the full collection of web_cooperator_ids...
            ds = _sharedUtils.GetWebServiceData("cooperator_wizard_get_web_cooperator", searchPKeys, 0, 0);
            if (ds.Tables.Contains("cooperator_wizard_get_web_cooperator"))
            {
                _webCooperator = ds.Tables["cooperator_wizard_get_web_cooperator"].Copy();
            }
            else if (_webCooperator == null)
            {
                _webCooperator = new DataTable();
            }

            if (ux_checkboxWebCooperatorIncludeShippingAdresses.Checked)
            {
                ds = _sharedUtils.GetWebServiceData("cooperator_wizard_get_web_user_shipping_address", searchPKeys, 0, 0);
                if (ds.Tables.Contains("cooperator_wizard_get_web_user_shipping_address") &&
                    ds.Tables["cooperator_wizard_get_web_user_shipping_address"].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables["cooperator_wizard_get_web_user_shipping_address"].Rows)
                    {
                        DataRow[] drs = _webCooperator.Select("web_cooperator_id=" + dr["web_cooperator_id"].ToString());
                        if (drs != null &&
                            drs.Length > 0)
                        {
                            DataRow drShippingAddress = _webCooperator.NewRow();
                            drShippingAddress["last_name"] = drs[0]["last_name"];
                            drShippingAddress["title"] = drs[0]["title"];
                            drShippingAddress["first_name"] = drs[0]["first_name"];
                            drShippingAddress["job"] = drs[0]["job"];
                            drShippingAddress["organization"] = drs[0]["organization"];
                            drShippingAddress["organization_code"] = drs[0]["organization_code"];

                            drShippingAddress["address_line1"] = dr["address_line1"];
                            drShippingAddress["address_line2"] = dr["address_line2"];
                            drShippingAddress["address_line3"] = dr["address_line3"];
                            drShippingAddress["city"] = dr["city"];
                            drShippingAddress["postal_index"] = dr["postal_index"];
                            drShippingAddress["geography_id"] = dr["geography_id"];

                            drShippingAddress["primary_phone"] = drs[0]["primary_phone"];
                            drShippingAddress["secondary_phone"] = drs[0]["secondary_phone"];
                            drShippingAddress["fax"] = drs[0]["fax"];
                            drShippingAddress["email"] = drs[0]["email"];
                            drShippingAddress["is_active"] = drs[0]["is_active"];
                            drShippingAddress["category_code"] = drs[0]["category_code"];
                            drShippingAddress["organization_region"] = drs[0]["organization_region"];
                            drShippingAddress["discipline"] = drs[0]["discipline"];
                            drShippingAddress["initials"] = drs[0]["initials"];
                            drShippingAddress["note"] = drs[0]["note"];

                            if (drShippingAddress["address_line1"].ToString() != drs[0]["address_line1"].ToString() ||
                                drShippingAddress["address_line2"].ToString() != drs[0]["address_line2"].ToString() ||
                                drShippingAddress["address_line3"].ToString() != drs[0]["address_line3"].ToString() ||
                                drShippingAddress["city"].ToString() != drs[0]["city"].ToString() ||
                                drShippingAddress["postal_index"].ToString() != drs[0]["postal_index"].ToString() ||
                                drShippingAddress["geography_id"].ToString() != drs[0]["geography_id"].ToString())
                            {
                                _webCooperator.Rows.Add(drShippingAddress);
                            }
                        }
                    }
                }
            }
            // And then update the binding source's data...
            _webCooperatorBindingSource.DataSource = _webCooperator;
        }

        private void RefreshWebOrderRequestList()
        {
            DataSet ds = new DataSet();
            string seQuery = "";
            string seWildCard = "";
            string searchPKeys = "";

            // First get the cooperator_ids for the orginal search criteria (_originalPKeys)...
            if (string.IsNullOrEmpty(_webOrderRequestPKeys)) searchPKeys = ":weborderrequestid=" + ux_textboxWebOrderNumber.Text;
            else searchPKeys = _webOrderRequestPKeys + ", " + ux_textboxWebOrderNumber.Text;
            ds = _sharedUtils.GetWebServiceData("cooperator_wizard_get_web_order_request", searchPKeys, 0, 0);  // ":WEBORDERREQUESTID="
            searchPKeys = ":cooperatorid=";
            if (ds.Tables.Contains("cooperator_wizard_get_web_order_request") &&
                ds.Tables["cooperator_wizard_get_web_order_request"].Rows.Count > 0)
            {
                _webOrderRequestRow = ds.Tables["cooperator_wizard_get_web_order_request"].Rows[0];
                ux_textboxWebOrderRequestLastName.Text = _webOrderRequestRow["last_name"].ToString();
                ux_textboxWebOrderRequestTitle.Text = _webOrderRequestRow["title"].ToString();
                ux_textboxWebOrderRequestFirstName.Text = _webOrderRequestRow["first_name"].ToString();
                ux_textboxWebOrderRequestOrganization.Text = _webOrderRequestRow["organization"].ToString();
                if (ux_radiobuttonPrimaryAddress.Checked)
                {
                    ux_textboxWebOrderRequestAddressLine1.Text = _webOrderRequestRow["address_line1"].ToString();
                    ux_textboxWebOrderRequestAddressLine2.Text = _webOrderRequestRow["address_line2"].ToString();
                    ux_textboxWebOrderRequestAddressLine3.Text = _webOrderRequestRow["address_line3"].ToString();
                    ux_textboxWebOrderRequestCity.Text = _webOrderRequestRow["city"].ToString();
                    ux_textboxWebOrderRequestPostalIndex.Text = _webOrderRequestRow["postal_index"].ToString();
                    ux_textboxWebOrderRequestGeography.Tag = _webOrderRequestRow["geography_id"].ToString();
                    ux_textboxWebOrderRequestGeography.Text = _sharedUtils.GetLookupDisplayMember("mailing_geography_lookup", _webOrderRequestRow["geography_id"].ToString(), "", _webOrderRequestRow["geography_id"].ToString());
                }
                else
                {
                    ux_textboxWebOrderRequestAddressLine1.Text = _webOrderRequestRow["ship_to_address_line1"].ToString();
                    ux_textboxWebOrderRequestAddressLine2.Text = _webOrderRequestRow["ship_to_address_line2"].ToString();
                    ux_textboxWebOrderRequestAddressLine3.Text = _webOrderRequestRow["ship_to_address_line3"].ToString();
                    ux_textboxWebOrderRequestCity.Text = _webOrderRequestRow["ship_to_city"].ToString();
                    ux_textboxWebOrderRequestPostalIndex.Text = _webOrderRequestRow["ship_to_postal_index"].ToString();
                    ux_textboxWebOrderRequestGeography.Tag = _webOrderRequestRow["ship_to_geography_id"].ToString();
                    ux_textboxWebOrderRequestGeography.Text = _sharedUtils.GetLookupDisplayMember("mailing_geography_lookup", _webOrderRequestRow["ship_to_geography_id"].ToString(), "", _webOrderRequestRow["ship_to_geography_id"].ToString());
                }
                ux_textboxWebOrderRequestPrimaryPhone.Text = _webOrderRequestRow["primary_phone"].ToString();
                ux_textboxWebOrderRequestEmail.Text = _webOrderRequestRow["email"].ToString();
            }
            else
            {
                _webOrderRequestRow = null;
            }

            // Process Last Name textbox...
            if (ux_checkboxWebOrderCooperatorLastName.Checked &&
                !string.IsNullOrEmpty(ux_textboxWebOrderRequestLastName.Text))
            {
                if (ux_textboxWebOrderRequestLastName.Text.Contains("*") ||
                    ux_textboxWebOrderRequestLastName.Text.Contains("%") ||
                    ux_textboxWebOrderRequestLastName.Text.Contains("_"))
                {
                    seQuery += "@cooperator.last_name LIKE '" + seWildCard + ux_textboxWebOrderRequestLastName.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@cooperator.last_name LIKE '" + ux_textboxWebOrderRequestLastName.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process First Name textbox...
            if (ux_checkboxWebOrderCooperatorFirstName.Checked &&
                !string.IsNullOrEmpty(ux_textboxWebOrderRequestFirstName.Text))
            {
                if (ux_textboxWebOrderRequestFirstName.Text.Contains("*") ||
                    ux_textboxWebOrderRequestFirstName.Text.Contains("%") ||
                    ux_textboxWebOrderRequestFirstName.Text.Contains("_"))
                {
                    seQuery += "@cooperator.first_name LIKE '" + seWildCard + ux_textboxWebOrderRequestFirstName.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@cooperator.first_name LIKE '" + ux_textboxWebOrderRequestFirstName.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process Organization textbox...
            if (ux_checkboxWebOrderCooperatorOrganization.Checked &&
                !string.IsNullOrEmpty(ux_textboxWebOrderRequestOrganization.Text))
            {
                if (ux_textboxWebOrderRequestOrganization.Text.Contains("*") ||
                    ux_textboxWebOrderRequestOrganization.Text.Contains("%") ||
                    ux_textboxWebOrderRequestOrganization.Text.Contains("_"))
                {
                    seQuery += "@cooperator.organization LIKE '" + seWildCard + ux_textboxWebOrderRequestOrganization.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@cooperator.organization LIKE '" + ux_textboxWebOrderRequestOrganization.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process AddressLine1 textbox...
            if (ux_checkboxWebOrderCooperatorAddressLine1.Checked &&
                !string.IsNullOrEmpty(ux_textboxWebOrderRequestAddressLine1.Text))
            {
                if (ux_textboxWebOrderRequestAddressLine1.Text.Contains("*") ||
                    ux_textboxWebOrderRequestAddressLine1.Text.Contains("%") ||
                    ux_textboxWebOrderRequestAddressLine1.Text.Contains("_"))
                {
                    seQuery += "@cooperator.address_line1 LIKE '" + seWildCard + ux_textboxWebOrderRequestAddressLine1.Text.Replace("*", "%").Replace("'", "_") + seWildCard + "' AND ";
                }
                else
                {
                    seQuery += "@cooperator.address_line1 LIKE '" + ux_textboxWebOrderRequestAddressLine1.Text.Replace("'", "_") + "' AND ";
                }
            }

            // Process Geography textbox...
            if (ux_checkboxWebOrderCooperatorGeography.Checked &&
                _webOrderRequestRow != null)
            {
                seQuery += "@cooperator.geography_id IN (" + _webOrderRequestRow["geography_id"].ToString() + ", " + _webOrderRequestRow["ship_to_geography_id"].ToString() + ") AND "; //_webOrderRequestRow["ship_to_geography_id"].ToString();
            }

            DataSet dsSEPKeysResults = _sharedUtils.SearchWebService(seQuery.Trim().TrimEnd(new char[] { 'A', 'N', 'D' }), true, true, "", "cooperator", 0, 0);
            if (dsSEPKeysResults.Tables.Contains("SearchResult"))
            {
                foreach (DataRow dr in dsSEPKeysResults.Tables["SearchResult"].Rows)
                {
                    searchPKeys += dr["ID"].ToString() + ",";
                }
            }

            // Remove the last trailing comma...
            searchPKeys = searchPKeys.TrimEnd(',');

            // Finally go get the full collection of cooperator_ids...
            ds = _sharedUtils.GetWebServiceData("cooperator_wizard_get_cooperator", searchPKeys, 0, 0);
            if (ds.Tables.Contains("cooperator_wizard_get_cooperator"))
            {
                _webOrderRequestCooperator = ds.Tables["cooperator_wizard_get_cooperator"].Copy();
            }
            else if (string.IsNullOrEmpty(_webOrderRequestPKeys))
            {
                _webOrderRequestCooperator = new DataTable();
            }

            // Make sure to create the new column to store the similarity score (if it does not exist yet)...
            if (!_webOrderRequestCooperator.Columns.Contains("match_score"))
            {
                _webOrderRequestCooperator.Columns.Add("match_score", typeof(string)).SetOrdinal(0);
                _webOrderRequestCooperator.Columns["match_score"].Caption = "Match Score";
                _webOrderRequestCooperator.Columns["match_score"].ExtendedProperties.Add("title", "Match Score");
            }

            // And then update the binding source's data...
            ux_datagridviewWebOrderRequestCooperator.ClearSelection();
            _webOrderRequestCooperatorBindingSource.DataSource = null;
            _webOrderRequestCooperatorBindingSource.DataSource = _webOrderRequestCooperator;
        }

        private void ComputeNewMatchScores()
        {
            if (_webOrderRequestCooperatorBindingSource.DataSource == null ||
                _webOrderRequestCooperatorBindingSource.DataSource.GetType() != typeof(DataTable)) return;
            DataTable dt = (DataTable)_webOrderRequestCooperatorBindingSource.DataSource;
            // Now iteratively calculate the similarity of the web_cooperator record with
            // each of the returned cooperator records...
            string compareLastName = ux_textboxWebOrderRequestLastName.Text.ToLower().Replace(" ", "");
            string compareTitle = ux_textboxWebOrderRequestTitle.Text.ToLower().Replace(" ", "");
            string compareFirstName = ux_textboxWebOrderRequestFirstName.Text.ToLower().Replace(" ", "");
            string compareOrganization = ux_textboxWebOrderRequestOrganization.Text.ToLower().Replace(" ", "");
            string compareAddressLine1 = ux_textboxWebOrderRequestAddressLine1.Text.ToLower().Replace(" ", "");
            string compareAddressLine2 = ux_textboxWebOrderRequestAddressLine2.Text.ToLower().Replace(" ", "");
            string compareAddressLine3 = ux_textboxWebOrderRequestAddressLine3.Text.ToLower().Replace(" ", "");
            string compareCity = ux_textboxWebOrderRequestCity.Text.ToLower().Replace(" ", "");
            string comparePostalIndex = ux_textboxWebOrderRequestPostalIndex.Text.ToLower().Replace(" ", "");
            string compareGeography = ux_textboxWebOrderRequestGeography.Text.ToLower().Replace(" ", "");
            string comparePrimaryPhone = ux_textboxWebOrderRequestPrimaryPhone.Text.ToLower().Replace(" ", "").Replace("-", "");
            string compareEmail = ux_textboxWebOrderRequestEmail.Text.ToLower().Replace(" ", "");
            foreach (DataRow dr in dt.Rows)
            {
                Int64 matchScore = 0;
                matchScore += stringSimilarityScore(compareLastName, dr["last_name"].ToString().ToLower().Replace(" ", "")) * 1000;
                //matchScore += stringSimilarityScore(compareTitle, dr["title"].ToString().ToLower().Replace(" ", "")) * 500;
                matchScore += stringSimilarityScore(compareFirstName, dr["first_name"].ToString().ToLower().Replace(" ", "")) * 500;
                matchScore += stringSimilarityScore(compareOrganization, dr["organization"].ToString().ToLower().Replace(" ", "")) * 500;
                matchScore += stringSimilarityScore(compareAddressLine1, dr["address_line1"].ToString().ToLower().Replace(" ", "")) * 1000;
                //matchScore += stringSimilarityScore(compareAddressLine1, dr["address_line2"].ToString().ToLower().Replace(" ", "")) * 500;
                //matchScore += stringSimilarityScore(compareAddressLine1, dr["address_line3"].ToString().ToLower().Replace(" ", "")) * 500;
                //matchScore += stringSimilarityScore(compareAddressLine2, dr["address_line1"].ToString().ToLower().Replace(" ", "")) * 500;
                matchScore += stringSimilarityScore(compareAddressLine2, dr["address_line2"].ToString().ToLower().Replace(" ", "")) * 1000;
                //matchScore += stringSimilarityScore(compareAddressLine2, dr["address_line3"].ToString().ToLower().Replace(" ", "")) * 500;
                //matchScore += stringSimilarityScore(compareAddressLine3, dr["address_line1"].ToString().ToLower().Replace(" ", "")) * 500;
                //matchScore += stringSimilarityScore(compareAddressLine3, dr["address_line2"].ToString().ToLower().Replace(" ", "")) * 500;
                matchScore += stringSimilarityScore(compareAddressLine3, dr["address_line3"].ToString().ToLower().Replace(" ", "")) * 1000;
                //matchScore += stringSimilarityScore(compareCity, dr["city"].ToString().ToLower().Replace(" ", "")) * 500;
                //matchScore += stringSimilarityScore(comparePostalIndex, dr["postal_index"].ToString().ToLower().Replace(" ", "")) * 500;
                matchScore += stringSimilarityScore(compareGeography, _sharedUtils.GetLookupDisplayMember("mailing_geography_lookup", dr["geography_id"].ToString(), "", dr["geography_id"].ToString()).ToString().ToLower().Replace(" ", "")) * 1000;
                matchScore += stringSimilarityScore(comparePrimaryPhone, dr["primary_phone"].ToString().ToLower().Replace(" ", "").Replace("-", "")) * 500;
                matchScore += stringSimilarityScore(compareEmail, dr["email"].ToString().ToLower().Replace(" ", "")) * 500;
                matchScore = matchScore / 7000;  // 11,500 total is the weighting factors (to get a score range of 0-100)
                dr["match_score"] = matchScore.ToString("000");
            }
        }

        private Int64 stringSimilarityScore(string string1, string string2)
        {
            Int64 score = 0;
            if (string1.Length > 0)
            {
                if (string1.Length >= string2.Length)
                {
                    string2 = string2.PadRight(string1.Length);
                }
                else
                {
                    string1 = string1.PadRight(string2.Length);
                }
                for (int i = 0; i < string1.Length; i++)
                {
                    if (string1[i] == string2[i]) score++;
                }

                score = (score * 100) / string1.Length;
            }
            else
            {
                if (string2.Length == 0) score = 100;
            }

            return score;
        }

        private int SaveCooperatorData()
        {
            int errorCount = 0;
            DataSet cooperatorChanges = new DataSet();
            DataSet cooperatorSaveResults = new DataSet();

            // Process COOPERATOR...
            // Make sure the last edited row in the Accessions Form has been commited to the datatable...
            _cooperatorBindingSource.EndEdit();

            // Make sure the navigator is not currently editing a cell...
            foreach (DataRowView drv in _cooperatorBindingSource.List)
            {
                if (drv.IsEdit ||
                    drv.Row.RowState == DataRowState.Added ||
                    drv.Row.RowState == DataRowState.Deleted ||
                    drv.Row.RowState == DataRowState.Detached ||
                    drv.Row.RowState == DataRowState.Modified)
                {
                    drv.EndEdit();
                    //drv.Row.ClearErrors();
                }
            }

            // Get the changes (if any) for the cooperator table and commit them to the remote database...
            if (_cooperator.GetChanges() != null)
            {
                cooperatorChanges.Tables.Add(_cooperator.GetChanges());
                ScrubData(cooperatorChanges);
                // Save the changes to the remote server...
                cooperatorSaveResults = _sharedUtils.SaveWebServiceData(cooperatorChanges);
                if (cooperatorSaveResults.Tables.Contains(_cooperator.TableName))
                {
                    errorCount += SyncSavedResults(_cooperator, cooperatorSaveResults.Tables[_cooperator.TableName]);
                }
            }

            // Now add the new changes to the _changedRecords dataset (this data will be passed back to the calling program)...
            if (cooperatorSaveResults != null && cooperatorSaveResults.Tables.Contains(_cooperator.TableName))
            {
                string pkeyName = cooperatorSaveResults.Tables[_cooperator.TableName].PrimaryKey[0].ColumnName;
                bool origColumnReadOnlyValue = cooperatorSaveResults.Tables[_cooperator.TableName].Columns[pkeyName].ReadOnly;
                foreach (DataRow dr in cooperatorSaveResults.Tables[_cooperator.TableName].Rows)
                {
                    dr.Table.Columns[pkeyName].ReadOnly = false;
                    dr[pkeyName] = dr["NewPrimaryKeyID"];
                    dr.AcceptChanges();
                }
                cooperatorSaveResults.Tables[_cooperator.TableName].Columns[pkeyName].ReadOnly = origColumnReadOnlyValue;

                if (_changedRecords.Tables.Contains(_cooperator.TableName))
                {
                    // If the saved results table exists - update or insert the new records...
                    _changedRecords.Tables[_cooperator.TableName].Load(cooperatorSaveResults.Tables[_cooperator.TableName].CreateDataReader(), LoadOption.Upsert);
                    _changedRecords.Tables[_cooperator.TableName].AcceptChanges();

                }
                else
                {
                    // If the saved results table doesn't exist - create it (and include the new records)...
                    _changedRecords.Tables.Add(cooperatorSaveResults.Tables[_cooperator.TableName].Copy());
                    _changedRecords.AcceptChanges();
                }
            }

            // Force a refresh of all data (so that any changes made by middle tier datatriggers
            // is pulled back into this program)...
            BuildCooperatorPage();
            BuildWebCooperatorPage();
            BuildWebOrderRequestPage();

            return errorCount;
        }

        private void ScrubData(DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dc.ExtendedProperties.Contains("is_nullable") &&
                            dc.ExtendedProperties["is_nullable"].ToString().Trim().ToUpper() == "N" &&
                            dr[dc] == DBNull.Value)
                        {
                            if (dc.ExtendedProperties.Contains("default_value") &&
                                !string.IsNullOrEmpty(dc.ExtendedProperties["default_value"].ToString()) &&
                                dc.ExtendedProperties["default_value"].ToString().Trim().ToUpper() != "{DBNULL.VALUE}")
                            {
                                dr[dc] = dc.ExtendedProperties["default_value"].ToString();
                            }
                        }
                    }
                }
            }
        }

        private int SyncSavedResults(DataTable originalTable, DataTable savedResults)
        {
            int errorCount = 0;

            if (savedResults != null && savedResults.PrimaryKey.Length == 1)
            {
                string pKeyCol = savedResults.PrimaryKey[0].ColumnName.Trim().ToUpper();
                savedResults.Columns[pKeyCol].ReadOnly = false;
                foreach (DataRow dr in savedResults.Rows)
                {
                    DataRow originalRow = originalTable.Rows.Find(dr["OriginalPrimaryKeyID"]);

                    switch (dr["SavedAction"].ToString())
                    {
                        case "Insert":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the originalTable row's status for this new row to committed (and update the pkey with the int returned from the server DB)...
                                if (originalRow != null)
                                {
                                    bool origColumnReadOnlyValue = originalRow.Table.Columns[pKeyCol].ReadOnly;
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = false;
                                    originalRow[pKeyCol] = dr["NewPrimaryKeyID"];
                                    originalRow.AcceptChanges();
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = origColumnReadOnlyValue;
                                    originalRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Update":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                originalRow.AcceptChanges();
                                originalRow.ClearErrors();
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Delete":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Set the row's status for this deleted row to committed...
                                if (originalRow != null)
                                {
                                    originalRow.AcceptChanges();
                                    originalRow.ClearErrors();
                                }
                            }
                            else
                            {
                                errorCount++;
                                // Find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in originalTable.Rows)
                                {
                                    if (deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        deletedRow.RejectChanges();
                                        deletedRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            return errorCount;
        }

        private void ux_buttonNewCooperator_Click(object sender, EventArgs e)
        {
            // Create a new cooperator table row...
            DataRow newCooperator = _cooperator.NewRow();
            // Add it to the cooperator table...
            _cooperator.Rows.Add(newCooperator);
            // Navigate to the first editable cell in the DGV's new row...
            int newRowIndex = ux_datagridviewCooperator.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
            int newColIndex = ux_datagridviewCooperator.Columns.GetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
            // First find the DGV row that contains the new table row created in the code above...
            for (int i = 0; i < ux_datagridviewCooperator.Rows.Count; i++)
            {
                if (ux_datagridviewCooperator["cooperator_id", i].Value.Equals(newCooperator["cooperator_id"])) newRowIndex = i;
            }
            // Now iterate through all of the columns until you find the first displayed column in the DGV...
            foreach (DataGridViewColumn dgvc in ux_datagridviewCooperator.Columns)
            {
                if (dgvc.DisplayIndex == 0)
                {
                    newColIndex = dgvc.Index;
                    break;
                }
            }
            // Set the DGV current cell to the first editable cell in the new row...
            ux_datagridviewCooperator.CurrentCell = ux_datagridviewCooperator[newColIndex, newRowIndex];
        }

        private void ux_buttonSave_Click(object sender, EventArgs e)
        {
            int errorCount = 0;
            errorCount = SaveCooperatorData();
            if (errorCount == 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Cooperator Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "CooperatorWizard_ux_buttonSaveMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();
            }
            else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\n  Error Count: {0}", "Cooperator Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "CooperatorWizard_ux_buttonSaveMessage2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }
            // Refresh the formatting of the cells in the DGV on the Cooperator tab page...
            RefreshDGVFormatting(ux_datagridviewCooperator);
            // Refresh the formatting of the cells in the DGV on the Web Cooperator tab page...
            RefreshDGVFormatting(ux_datagridviewWebCooperatorMaster);
            RefreshDGVFormatting(ux_datagridviewWebCooperatorDetail);
        }

        private void ux_buttonSaveAndExit_Click(object sender, EventArgs e)
        {
            int errorCount = 0;
            errorCount = SaveCooperatorData();
            if (errorCount == 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("All data was saved successfully", "Cooperator Wizard Data Save Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "CooperatorWizard_ux_buttonSaveMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();
                this.Close();
            }
            else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\nWould you like to review them now?\n\nClick Yes to review the errors now.\n(Click No to abandon the errors and exit the Cooperator Wizard).\n\n  Error Count: {0}", "Cooperator Wizard Data Save Results", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "CooperatorWizard_ux_buttonSaveMessage3";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = errorCount.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                if (DialogResult.No == ggMessageBox.ShowDialog())
                {
                    this.Close();
                }
            }
            // Refresh the formatting of the cells in the DGV on the Cooperator tab page...
            RefreshDGVFormatting(ux_datagridviewCooperator);
        }

        private void ux_buttonCreateNewCooperator_Click(object sender, EventArgs e)
        {
            int firstNewRowIndex = int.MaxValue;
            foreach (DataGridViewRow dgvr in ux_datagridviewWebCooperatorMaster.SelectedRows)
            {
                // Create a new cooperator table row...
                DataRow newCooperator = _cooperator.NewRow();
                // Populate the row with data from the web_cooperator table's selected row...
                DataRow webCooperator = _webCooperator.Rows.Find(((DataRowView)dgvr.DataBoundItem).Row["web_cooperator_id"]);
                if (webCooperator != null)
                {
                    if (_cooperator.Columns.Contains("last_name") && _webCooperator.Columns.Contains("last_name")) newCooperator["last_name"] = webCooperator["last_name"];
                    if (_cooperator.Columns.Contains("title") && _webCooperator.Columns.Contains("title")) newCooperator["title"] = webCooperator["title"];
                    if (_cooperator.Columns.Contains("first_name") && _webCooperator.Columns.Contains("first_name")) newCooperator["first_name"] = webCooperator["first_name"];
                    if (_cooperator.Columns.Contains("organization") && _webCooperator.Columns.Contains("organization")) newCooperator["organization"] = webCooperator["organization"];
                    if (_cooperator.Columns.Contains("address_line1") && _webCooperator.Columns.Contains("address_line1")) newCooperator["address_line1"] = webCooperator["address_line1"];
                    if (_cooperator.Columns.Contains("address_line2") && _webCooperator.Columns.Contains("address_line2")) newCooperator["address_line2"] = webCooperator["address_line2"];
                    if (_cooperator.Columns.Contains("address_line3") && _webCooperator.Columns.Contains("address_line3")) newCooperator["address_line3"] = webCooperator["address_line3"];
                    if (_cooperator.Columns.Contains("city") && _webCooperator.Columns.Contains("city")) newCooperator["city"] = webCooperator["city"];
                    if (_cooperator.Columns.Contains("postal_index") && _webCooperator.Columns.Contains("postal_index")) newCooperator["postal_index"] = webCooperator["postal_index"];
                    if (_cooperator.Columns.Contains("geography_id") && _webCooperator.Columns.Contains("geography_id")) newCooperator["geography_id"] = webCooperator["geography_id"];
                    if (_cooperator.Columns.Contains("primary_phone") && _webCooperator.Columns.Contains("primary_phone")) newCooperator["primary_phone"] = webCooperator["primary_phone"];
                    if (_cooperator.Columns.Contains("email") && _webCooperator.Columns.Contains("email")) newCooperator["email"] = webCooperator["email"];
                    if (_cooperator.Columns.Contains("category_code") && _webCooperator.Columns.Contains("category_code")) newCooperator["category_code"] = webCooperator["category_code"];
                    if (_cooperator.Columns.Contains("site_id")) newCooperator["site_id"] = _sharedUtils.UserSiteID;
                    if (_cooperator.Columns.Contains("web_cooperator_id") && _webCooperator.Columns.Contains("web_cooperator_id"))
                    {
                        // Check to see if the webCooperator is a real database record (PKEY > 0) or 
                        // if the webCooperator is a virtual record (aka PKEY < 0) because it is a 'SHIP TO' address...
                        if (int.Parse(webCooperator["web_cooperator_id"].ToString()) > 0)
                        {
                            newCooperator["web_cooperator_id"] = webCooperator["web_cooperator_id"];
                        }
                        else
                        {
                            // Working with a 'SHIP TO' address (virtual) so try to find the web_cooperator_id by other means...
                            // First see if the Cooperator Wizard was initiated from the Order Wizard (ie _webOrderRequestRow is not null) 
                            // and if so use the web_order_row to get the web_cooperator_id...
                            if (_webOrderRequestRow != null &&
                                _webOrderRequestRow.Table.Columns.Contains("web_cooperator_id"))
                            {
                                newCooperator["web_cooperator_id"] = _webOrderRequestRow["web_cooperator_id"];
                            }
                            else
                            {
                                // Attempt to find a matching real database record (ie PKEY > 0) that has a matching
                                // last_name, first_name, and organization...
                                DataRow[] webCoopSearch = _webCooperator.Select("web_cooperator_id>0 AND last_name='" + webCooperator["last_name"].ToString() + "' AND first_name='" + webCooperator["first_name"].ToString() + "' AND organization='" + webCooperator["organization"].ToString() + "'");
                                if (webCoopSearch.Length == 1)
                                {
                                    newCooperator["web_cooperator_id"] = webCoopSearch[0]["web_cooperator_id"];
                                }
                            }
                        }
                    }
                }
                // Add it to the cooperator table...
                _cooperator.Rows.Add(newCooperator);
                // Remember the first new row in the cooperator DGV...
                if (firstNewRowIndex == int.MaxValue) firstNewRowIndex = (int)newCooperator["cooperator_id"];
            }

            if (firstNewRowIndex != int.MaxValue)
            {
                // Navigate to the first editable cell in the DGV's new row...
                int newRowIndex = ux_datagridviewCooperator.Rows.GetLastRow(System.Windows.Forms.DataGridViewElementStates.Displayed);
                int newColIndex = 0; //ux_datagridviewCooperator.Columns.gGetFirstColumn(System.Windows.Forms.DataGridViewElementStates.Displayed).Index;
                // First find the DGV row that contains the new table row created in the code above...
                for (int i = 0; i < ux_datagridviewCooperator.Rows.Count; i++)
                {
                    if (ux_datagridviewCooperator["cooperator_id", i].Value.Equals(firstNewRowIndex)) newRowIndex = i;
                }
                // Now iterate through all of the columns until you find the first displayed column in the DGV...
                foreach (DataGridViewColumn dgvc in ux_datagridviewCooperator.Columns)
                {
                    if (dgvc.DisplayIndex == 0)
                    {
                        newColIndex = dgvc.Index;
                        break;
                    }
                }
                // Set the DGV current cell to the first editable cell in the new row...
                ux_datagridviewCooperator.CurrentCell = ux_datagridviewCooperator[newColIndex, newRowIndex];
            }
        }

        private void ux_buttonFindCooperator_Click(object sender, EventArgs e)
        {
            BuildCooperatorPage();
        }

        private void ux_buttonFindWebCooperator_Click(object sender, EventArgs e)
        {
            BuildWebCooperatorPage();
        }

        private void ux_buttonFindWebOrderRequest_Click(object sender, EventArgs e)
        {
            BuildWebOrderRequestPage();
        }

        private void ux_checkboxWebCooperatorFindMatchesBasedOn_CheckedChanged(object sender, EventArgs e)
        {
            if (ux_datagridviewWebCooperatorMaster.DataSource.GetType() == typeof(BindingSource) &&
                ((BindingSource)ux_datagridviewWebCooperatorMaster.DataSource).DataSource.GetType() == typeof(DataTable) &&
                ((BindingSource)ux_datagridviewWebCooperatorMaster.DataSource).DataSource != null &&
                ((DataTable)((BindingSource)ux_datagridviewWebCooperatorMaster.DataSource).DataSource).Rows.Count > 0)
            {
                DataRow dr = ((DataRowView)ux_datagridviewWebCooperatorMaster.CurrentCell.OwningRow.DataBoundItem).Row;
                ApplyMasterDetailBusinessRules(dr);
            }
        }

        private void ux_textboxWebCooperatorFilterText_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tbFilter = (TextBox)sender;
            CheckBox cbFindMatchesBasedOn = new CheckBox();
            switch (tbFilter.Name.Trim().ToUpper())
            {
                case "UX_TEXTBOXWEBLASTNAME":
                    cbFindMatchesBasedOn = ux_checkboxWebCooperatorLastName;
                    break;
                case "UX_TEXTBOXWEBFIRSTNAME":
                    cbFindMatchesBasedOn = ux_checkboxWebCooperatorFirstName;
                    break;
                case "UX_TEXTBOXWEBORGANIZATION":
                    cbFindMatchesBasedOn = ux_checkboxWebCooperatorOrganization;
                    break;
                default:
                    break;
            }
            // Toggle the checked state of the checkbox based on the content
            // of the associated textbox...
            if (string.IsNullOrEmpty(tbFilter.Text) &&
                string.IsNullOrEmpty(e.KeyChar.ToString()))
            {
                cbFindMatchesBasedOn.Checked = false;
            }
            else
            {
                cbFindMatchesBasedOn.Checked = true;
            }
        }

        private void ux_checkboxWebOrderCooperatorFindMatchesBasedOn_CheckedChanged(object sender, EventArgs e)
        {
            BuildWebOrderRequestPage();
        }

        private void ux_radiobuttonPrimaryAddress_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;

            ux_labelWebOrderCooperatorDetails.Text = string.Format(ux_labelWebOrderCooperatorDetails.Tag.ToString(), rb.Text);
            ux_labelWebOrderInstructions.Text = string.Format(ux_labelWebOrderInstructions.Tag.ToString(), rb.Text);

            if (rb.Checked &&
                _webOrderRequestRow != null &&
                _webOrderRequestRow.Table.Columns.Contains("last_name") &&
                _webOrderRequestRow.Table.Columns.Contains("title") &&
                _webOrderRequestRow.Table.Columns.Contains("first_name") &&
                _webOrderRequestRow.Table.Columns.Contains("organization") &&
                _webOrderRequestRow.Table.Columns.Contains("address_line1") &&
                _webOrderRequestRow.Table.Columns.Contains("address_line2") &&
                _webOrderRequestRow.Table.Columns.Contains("address_line3") &&
                _webOrderRequestRow.Table.Columns.Contains("city") &&
                _webOrderRequestRow.Table.Columns.Contains("postal_index") &&
                _webOrderRequestRow.Table.Columns.Contains("geography_id") &&
                _webOrderRequestRow.Table.Columns.Contains("primary_phone") &&
                _webOrderRequestRow.Table.Columns.Contains("email"))
            {
                ux_textboxWebOrderRequestLastName.Text = _webOrderRequestRow["last_name"].ToString();
                ux_textboxWebOrderRequestTitle.Text = _webOrderRequestRow["title"].ToString();
                ux_textboxWebOrderRequestFirstName.Text = _webOrderRequestRow["first_name"].ToString();
                ux_textboxWebOrderRequestOrganization.Text = _webOrderRequestRow["organization"].ToString();
                ux_textboxWebOrderRequestAddressLine1.Text = _webOrderRequestRow["address_line1"].ToString();
                ux_textboxWebOrderRequestAddressLine2.Text = _webOrderRequestRow["address_line2"].ToString();
                ux_textboxWebOrderRequestAddressLine3.Text = _webOrderRequestRow["address_line3"].ToString();
                ux_textboxWebOrderRequestCity.Text = _webOrderRequestRow["city"].ToString();
                ux_textboxWebOrderRequestPostalIndex.Text = _webOrderRequestRow["postal_index"].ToString();
                ux_textboxWebOrderRequestGeography.Text = _sharedUtils.GetLookupDisplayMember("mailing_geography_lookup", _webOrderRequestRow["geography_id"].ToString(), "", _webOrderRequestRow["geography_id"].ToString());
                ux_textboxWebOrderRequestPrimaryPhone.Text = _webOrderRequestRow["primary_phone"].ToString();
                ux_textboxWebOrderRequestEmail.Text = _webOrderRequestRow["email"].ToString();

                ComputeNewMatchScores();
                ux_datagridviewWebOrderRequestCooperator.Sort(ux_datagridviewWebOrderRequestCooperator.Columns["match_score"], ListSortDirection.Descending);
                if (string.IsNullOrEmpty(_finalRecipientCooperatorID))
                {
                    ux_datagridviewWebOrderRequestCooperator.CurrentCell = null;
                    // Make sure the cooperator datagridview is showing the first row (which should be the best match)...
                    if (ux_datagridviewWebOrderRequestCooperator.Rows.Count > 0)
                    {
                        ux_datagridviewWebOrderRequestCooperator.FirstDisplayedScrollingRowIndex = 0;
                    }
                }
                else
                {
                    foreach (DataGridViewRow dgvr in ux_datagridviewWebOrderRequestCooperator.Rows)
                    {
                        if (dgvr.Cells["cooperator_id"].Value.ToString().ToLower() == _finalRecipientCooperatorID.ToLower()) dgvr.Selected = true;
                        ux_datagridviewWebOrderRequestCooperator.CurrentCell = ux_datagridviewWebOrderRequestCooperator.SelectedCells[0];
                    }
                }
                CooperatorAddressVisualCues();
            }
        }

        private void ux_radiobuttonShipToAddress_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;

            ux_labelWebOrderCooperatorDetails.Text = string.Format(ux_labelWebOrderCooperatorDetails.Tag.ToString(), rb.Text);
            ux_labelWebOrderInstructions.Text = string.Format(ux_labelWebOrderInstructions.Tag.ToString(), rb.Text);

            if (rb.Checked &&
                _webOrderRequestRow != null &&
                _webOrderRequestRow.Table.Columns.Contains("last_name") &&
                _webOrderRequestRow.Table.Columns.Contains("title") &&
                _webOrderRequestRow.Table.Columns.Contains("first_name") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_address_line1") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_address_line2") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_address_line3") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_city") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_postal_index") &&
                _webOrderRequestRow.Table.Columns.Contains("ship_to_geography_id") &&
                _webOrderRequestRow.Table.Columns.Contains("primary_phone") &&
                _webOrderRequestRow.Table.Columns.Contains("email"))
            {
                ux_textboxWebOrderRequestLastName.Text = _webOrderRequestRow["last_name"].ToString();
                ux_textboxWebOrderRequestTitle.Text = _webOrderRequestRow["title"].ToString();
                ux_textboxWebOrderRequestFirstName.Text = _webOrderRequestRow["first_name"].ToString();
                ux_textboxWebOrderRequestAddressLine1.Text = _webOrderRequestRow["ship_to_address_line1"].ToString();
                ux_textboxWebOrderRequestAddressLine2.Text = _webOrderRequestRow["ship_to_address_line2"].ToString();
                ux_textboxWebOrderRequestAddressLine3.Text = _webOrderRequestRow["ship_to_address_line3"].ToString();
                ux_textboxWebOrderRequestCity.Text = _webOrderRequestRow["ship_to_city"].ToString();
                ux_textboxWebOrderRequestPostalIndex.Text = _webOrderRequestRow["ship_to_postal_index"].ToString();
                ux_textboxWebOrderRequestGeography.Text = _sharedUtils.GetLookupDisplayMember("mailing_geography_lookup", _webOrderRequestRow["ship_to_geography_id"].ToString(), "", _webOrderRequestRow["geography_id"].ToString());
                ux_textboxWebOrderRequestPrimaryPhone.Text = _webOrderRequestRow["primary_phone"].ToString();
                ux_textboxWebOrderRequestEmail.Text = _webOrderRequestRow["email"].ToString();

                ComputeNewMatchScores();
                ux_datagridviewWebOrderRequestCooperator.Sort(ux_datagridviewWebOrderRequestCooperator.Columns["match_score"], ListSortDirection.Descending);
                if (string.IsNullOrEmpty(_shipToCooperatorID))
                {
                    ux_datagridviewWebOrderRequestCooperator.CurrentCell = null;
                    // Make sure the cooperator datagridview is showing the first row (which should be the best match)...
                    if (ux_datagridviewWebOrderRequestCooperator.Rows.Count > 0)
                    {
                        ux_datagridviewWebOrderRequestCooperator.FirstDisplayedScrollingRowIndex = 0;
                    }
                }
                else
                {
                    foreach (DataGridViewRow dgvr in ux_datagridviewWebOrderRequestCooperator.Rows)
                    {
                        if (dgvr.Cells["cooperator_id"].Value.ToString().ToLower() == _shipToCooperatorID) dgvr.Selected = true;
                        ux_datagridviewWebOrderRequestCooperator.CurrentCell = ux_datagridviewWebOrderRequestCooperator.SelectedCells[0];
                    }
                }
                CooperatorAddressVisualCues();
            }
        }

        private void Ux_datagridviewWebOrderRequestCooperator_SelectionChanged(object sender, EventArgs e)
        {
            CooperatorAddressVisualCues();
        }

        private void CooperatorAddressVisualCues()
        {
            DataGridViewRow SelectedCoopRow = null;

            if (ux_datagridviewWebOrderRequestCooperator.CurrentRow != null)
            {
                SelectedCoopRow = ux_datagridviewWebOrderRequestCooperator.CurrentRow;
            }

            // Retrieve the values from the web order cooperator...
            string webTitle = ux_textboxWebOrderRequestTitle.Text.Trim();
            string webFirstName = ux_textboxWebOrderRequestFirstName.Text.Trim();
            string webLastName = ux_textboxWebOrderRequestLastName.Text.Trim();
            string webOrganization = ux_textboxWebOrderRequestOrganization.Text.Trim();
            string webAddressLine1 = ux_textboxWebOrderRequestAddressLine1.Text.Trim();
            string webAddressLine2 = ux_textboxWebOrderRequestAddressLine2.Text.Trim();
            string webAddressLine3 = ux_textboxWebOrderRequestAddressLine3.Text.Trim();
            string webCity = ux_textboxWebOrderRequestCity.Text.Trim();
            string webGeography = ux_textboxWebOrderRequestGeography.Text.Trim();
            string webPostalIndex = ux_textboxWebOrderRequestPostalIndex.Text.Trim();
            string webPrimaryPhone = ux_textboxWebOrderRequestPrimaryPhone.Text.Trim();
            string webEmail = ux_textboxWebOrderRequestEmail.Text.Trim();

            // Set the values for the selected cooperator to empty...
            string coopTitle = "";
            string coopFirstName = "";
            string coopLastName = "";
            string coopOrganization = "";
            string coopAddressLine1 = "";
            string coopAddressLine2 = "";
            string coopAddressLine3 = "";
            string coopCity = "";
            string coopGeography = "";
            string coopPostalIndex = "";
            string coopPrimaryPhone = "";
            string coopEmail = "";

            // If the user has selected a cooperator in the datagridview populate those values...
            if (SelectedCoopRow != null)
            {
                //Retrieve values from the selected cooperator record...
                coopTitle = SelectedCoopRow.Cells["title"].FormattedValue.ToString().Trim();
                coopFirstName = SelectedCoopRow.Cells["first_name"].FormattedValue.ToString().Trim();
                coopLastName = SelectedCoopRow.Cells["last_name"].FormattedValue.ToString().Trim();
                coopOrganization = SelectedCoopRow.Cells["organization"].FormattedValue.ToString().Trim();
                coopAddressLine1 = SelectedCoopRow.Cells["address_line1"].FormattedValue.ToString().Trim();
                coopAddressLine2 = SelectedCoopRow.Cells["address_line2"].FormattedValue.ToString().Trim();
                coopAddressLine3 = SelectedCoopRow.Cells["address_line3"].FormattedValue.ToString().Trim();
                coopCity = SelectedCoopRow.Cells["city"].FormattedValue.ToString().Trim();
                coopGeography = SelectedCoopRow.Cells["geography_id"].FormattedValue.ToString().Trim();
                coopPostalIndex = SelectedCoopRow.Cells["postal_index"].FormattedValue.ToString().Trim();
                coopPrimaryPhone = SelectedCoopRow.Cells["primary_phone"].FormattedValue.ToString().Trim();
                coopEmail = SelectedCoopRow.Cells["email"].FormattedValue.ToString().Trim();
            }

            // Apply the visual cues to help the user decide which cooperator record is the best choice...
            // Make the comparison strings the same length...
            if (coopTitle.Length > webTitle.Length)
            {
                webTitle = webTitle.PadRight(coopTitle.Length);
            }
            else
            {
                coopTitle = coopTitle.PadRight(webTitle.Length);
            }
            if (coopFirstName.Length > webFirstName.Length)
            {
                webFirstName = webFirstName.PadRight(coopFirstName.Length);
            }
            else
            {
                coopFirstName = coopFirstName.PadRight(webFirstName.Length);
            }
            if (coopLastName.Length > webLastName.Length)
            {
                webLastName = webLastName.PadRight(coopLastName.Length);
            }
            else
            {
                coopLastName = coopLastName.PadRight(webLastName.Length);
            }
            if (coopOrganization.Length > webOrganization.Length)
            {
                webOrganization = webOrganization.PadRight(coopOrganization.Length);
            }
            else
            {
                coopOrganization = coopOrganization.PadRight(webOrganization.Length);
            }
            if (coopAddressLine1.Length > webAddressLine1.Length)
            {
                webAddressLine1 = webAddressLine1.PadRight(coopAddressLine1.Length);
            }
            else
            {
                coopAddressLine1 = coopAddressLine1.PadRight(webAddressLine1.Length);
            }
            if (coopAddressLine2.Length > webAddressLine2.Length)
            {
                webAddressLine2 = webAddressLine2.PadRight(coopAddressLine2.Length);
            }
            else
            {
                coopAddressLine2 = coopAddressLine2.PadRight(webAddressLine2.Length);
            }
            if (coopAddressLine3.Length > webAddressLine3.Length)
            {
                webAddressLine3 = webAddressLine3.PadRight(coopAddressLine3.Length);
            }
            else
            {
                coopAddressLine3 = coopAddressLine3.PadRight(webAddressLine3.Length);
            }
            if (coopCity.Length > webCity.Length)
            {
                webCity = webCity.PadRight(coopCity.Length);
            }
            else
            {
                coopCity = coopCity.PadRight(webCity.Length);
            }
            if (coopGeography.Length > webGeography.Length)
            {
                webGeography = webGeography.PadRight(coopGeography.Length);
            }
            else
            {
                coopGeography = coopGeography.PadRight(webGeography.Length);
            }
            if (coopPostalIndex.Length > webPostalIndex.Length)
            {
                webPostalIndex = webPostalIndex.PadRight(coopPostalIndex.Length);
            }
            else
            {
                coopPostalIndex = coopPostalIndex.PadRight(webPostalIndex.Length);
            }

            ux_richtextboxWebOrderAddress.Clear();
            ux_richtextboxSelectedCooperatorAddress.Clear();
            // Reset the colors in the read-only left panel back to default...
            ux_panelWebOrderRequestSearchCriteria.BackColor = Color.Empty;
            foreach (Control ctrl in ux_panelWebOrderRequestSearchCriteria.Controls)
            {
                if (ctrl.GetType() == typeof(TextBox))
                {
                    ctrl.BackColor = Color.Empty;
                }
            }

            if (!coopTitle.Equals(webTitle))
            {
                ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = Color.Yellow;
                if (ux_datagridviewWebOrderRequestCooperator.CurrentRow != null)
                    if (SelectedCoopRow != null)
                    {
                        ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                        ux_textboxWebOrderRequestTitle.BackColor = Color.Yellow;
                    }
            }
            ux_richtextboxWebOrderAddress.AppendText(webTitle);
            ux_richtextboxSelectedCooperatorAddress.AppendText(coopTitle);
            ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = SystemColors.Control;
            ux_richtextboxWebOrderAddress.AppendText(" ");
            ux_richtextboxSelectedCooperatorAddress.AppendText(" ");

            if (!coopFirstName.Equals(webFirstName))
            {
                ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = Color.Yellow;
                if (SelectedCoopRow != null)
                {
                    ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                    ux_textboxWebOrderRequestFirstName.BackColor = Color.Yellow;
                }
            }
            ux_richtextboxWebOrderAddress.AppendText(webFirstName);
            ux_richtextboxSelectedCooperatorAddress.AppendText(coopFirstName);
            ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = SystemColors.Control;
            ux_richtextboxWebOrderAddress.AppendText(" ");
            ux_richtextboxSelectedCooperatorAddress.AppendText(" ");

            if (!coopLastName.Equals(webLastName))
            {
                ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = Color.Yellow;
                if (SelectedCoopRow != null)
                {
                    ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                    ux_textboxWebOrderRequestLastName.BackColor = Color.Yellow;
                }
            }
            ux_richtextboxWebOrderAddress.AppendText(webLastName);
            ux_richtextboxSelectedCooperatorAddress.AppendText(coopLastName);
            ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = SystemColors.Control;
            ux_richtextboxWebOrderAddress.AppendText(", ");
            ux_richtextboxSelectedCooperatorAddress.AppendText(", ");

            if (!coopOrganization.Equals(webOrganization))
            {
                ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = Color.Yellow;
                if (SelectedCoopRow != null)
                {
                    ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                    ux_textboxWebOrderRequestOrganization.BackColor = Color.Yellow;
                }
            }
            ux_richtextboxWebOrderAddress.AppendText(webOrganization);
            ux_richtextboxSelectedCooperatorAddress.AppendText(coopOrganization);
            ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = SystemColors.Control;
            ux_richtextboxWebOrderAddress.AppendText(", ");
            ux_richtextboxSelectedCooperatorAddress.AppendText(", ");

            if (!coopAddressLine1.Equals(webAddressLine1))
            {
                ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = Color.Yellow;
                if (SelectedCoopRow != null)
                {
                    ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                    ux_textboxWebOrderRequestAddressLine1.BackColor = Color.Yellow;
                }
            }
            ux_richtextboxWebOrderAddress.AppendText(webAddressLine1);
            ux_richtextboxSelectedCooperatorAddress.AppendText(coopAddressLine1);
            ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = SystemColors.Control;
            ux_richtextboxWebOrderAddress.AppendText(", ");
            ux_richtextboxSelectedCooperatorAddress.AppendText(", ");

            if (!coopAddressLine2.Equals(webAddressLine2))
            {
                ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = Color.Yellow;
                if (SelectedCoopRow != null)
                {
                    ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                    ux_textboxWebOrderRequestAddressLine2.BackColor = Color.Yellow;
                }
            }
            ux_richtextboxWebOrderAddress.AppendText(webAddressLine2);
            ux_richtextboxSelectedCooperatorAddress.AppendText(coopAddressLine2);
            ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = SystemColors.Control;
            ux_richtextboxWebOrderAddress.AppendText(", ");
            ux_richtextboxSelectedCooperatorAddress.AppendText(", ");

            if (!coopAddressLine3.Equals(webAddressLine3))
            {
                ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = Color.Yellow;
                if (SelectedCoopRow != null)
                {
                    ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                    ux_textboxWebOrderRequestAddressLine3.BackColor = Color.Yellow;
                }
            }
            ux_richtextboxWebOrderAddress.AppendText(webAddressLine3);
            ux_richtextboxSelectedCooperatorAddress.AppendText(coopAddressLine3);
            ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = SystemColors.Control;
            ux_richtextboxWebOrderAddress.AppendText(", ");
            ux_richtextboxSelectedCooperatorAddress.AppendText(", ");

            if (!coopCity.Equals(webCity))
            {
                ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = Color.Yellow;
                if (SelectedCoopRow != null)
                {
                    ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                    ux_textboxWebOrderRequestCity.BackColor = Color.Yellow;
                }
            }
            ux_richtextboxWebOrderAddress.AppendText(webCity);
            ux_richtextboxSelectedCooperatorAddress.AppendText(coopCity);
            ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = SystemColors.Control;
            ux_richtextboxWebOrderAddress.AppendText(", ");
            ux_richtextboxSelectedCooperatorAddress.AppendText(", ");

            if (!coopGeography.Equals(webGeography))
            {
                ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = Color.Yellow;
                if (SelectedCoopRow != null)
                {
                    ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                    ux_textboxWebOrderRequestGeography.BackColor = Color.Yellow;
                }
            }
            ux_richtextboxWebOrderAddress.AppendText(webGeography);
            ux_richtextboxSelectedCooperatorAddress.AppendText(coopGeography);
            ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = SystemColors.Control;
            ux_richtextboxWebOrderAddress.AppendText(", ");
            ux_richtextboxSelectedCooperatorAddress.AppendText(", ");

            if (!coopPostalIndex.Equals(webPostalIndex))
            {
                ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = Color.Yellow;
                if (SelectedCoopRow != null)
                {
                    ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                    ux_textboxWebOrderRequestPostalIndex.BackColor = Color.Yellow;
                }
            }
            ux_richtextboxWebOrderAddress.AppendText(webPostalIndex);
            ux_richtextboxSelectedCooperatorAddress.AppendText(coopPostalIndex);
            ux_richtextboxSelectedCooperatorAddress.SelectionBackColor = SystemColors.Control;

            if (SelectedCoopRow != null &&
                !coopPrimaryPhone.Equals(webPrimaryPhone))
            {
                ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                ux_textboxWebOrderRequestPrimaryPhone.BackColor = Color.Yellow;
            }

            if (SelectedCoopRow != null &&
                !coopEmail.Equals(webEmail))
            {
                ux_panelWebOrderRequestSearchCriteria.BackColor = Color.SandyBrown;
                ux_textboxWebOrderRequestEmail.BackColor = Color.Yellow;
            }

        }

        private void Ux_datagridviewWebOrderRequestCooperator_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Ux_datagridviewWebOrderRequestCooperator_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (ux_radiobuttonPrimaryAddress.Checked)
            {
                _finalRecipientCooperatorID = ux_datagridviewWebOrderRequestCooperator.CurrentRow.Cells["cooperator_id"].Value.ToString();
                ux_radiobuttonPrimaryAddress.ForeColor = Color.Empty;
            }
            else
            {
                _shipToCooperatorID = ux_datagridviewWebOrderRequestCooperator.CurrentRow.Cells["cooperator_id"].Value.ToString();
                ux_radiobuttonShipToAddress.ForeColor = Color.Empty;
            }
        }

        private void Ux_datagridviewWebOrderRequestCooperator_DoubleClick(object sender, EventArgs e)
        {

        }

        private void Ux_datagridviewWebOrderRequestCooperator_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void ux_buttonWebOrderCreateNewCooperator_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ux_textboxWebLastName.Text))
            {
                ux_tabcontrolMain.SelectTab("ux_tabpageWebCooperator");
                // Populate the text boxes in the left panel of the Web Cooperator tab...
                ux_textboxWebLastName.Text = ux_textboxWebOrderRequestLastName.Text;
                ux_textboxWebFirstName.Text = ux_textboxWebOrderRequestFirstName.Text;
                ux_textboxWebOrganization.Text = ux_textboxWebOrderRequestOrganization.Text;
                ux_textboxWebEMail.Text = "";
                ux_textboxWebPhoneNumber.Text = "";
                if (ux_radiobuttonPrimaryAddress.Checked &&
                    _webOrderRequestRow != null &&
                    _webOrderRequestRow.Table.Columns.Contains("web_cooperator_id") &&
                    !string.IsNullOrEmpty(_webOrderRequestRow["web_cooperator_id"].ToString()))
                {
                    ux_textboxWebCooperatorID.Text = _webOrderRequestRow["web_cooperator_id"].ToString();
                    ux_checkboxWebCooperatorIncludeShippingAdresses.Checked = false;
                }
                else
                {
                    ux_textboxWebCooperatorID.Text = "";
                    ux_checkboxWebCooperatorIncludeShippingAdresses.Checked = true;
                }
                ux_buttonFindWebCooperator.PerformClick();
            }
        }
    }
}
