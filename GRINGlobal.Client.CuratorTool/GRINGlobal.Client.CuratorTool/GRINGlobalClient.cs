﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GRINGlobal.Client.Common;

namespace GRINGlobal.Client.CuratorTool
{
    public partial class GRINGlobalClientCuratorTool : Form
    {
        SharedUtils _sharedUtils;
        Cursor _cursorGG;
        Cursor _cursorREQ;
        Cursor _cursorLUT;
        bool escapeKeyPressed = false;
        string _saveDGVDataDumpFile;
        string _saveListDataDumpFile;
        string _pathSeparator = "|";
        string username;
        string password;
        string passwordClearText;
        string url;
        string _usernameCooperatorID;
        string _currentCooperatorID;
        string site;
        string siteID;
        string languageCode;
        string lastFullPath;
        string lastTabName;
        string commonUserApplicationDataPath;
        string roamingUserApplicationDataPath;
        string userSettingsXMLFilePath;
        string localDBInstance;
        DataSet grinData = new DataSet();
        DataTable userItemList = new DataTable();
        int mouseClickDGVColumnIndex;
        int mouseClickDGVRowIndex;
        FormsData[] localFormsAssemblies;
        BindingSource defaultBindingSource;
        Form dataviewForm;
        char _lastDGVCharPressed;
        private Timer verticalTabExposedTimer = new Timer();
        List<DataGridViewCell> _dgvcFindResults = new List<DataGridViewCell>();
        int _dgvcFindIndex = 0;
        List<TreeNode> _tvnFindResults = new List<TreeNode>();
        int _tvnFindIndex = 0;
        Keys _blockmodeKeyboardMapping = Keys.Control | Keys.Oemtilde;

        public GRINGlobalClientCuratorTool(string[] args)
        {
            InitializeComponent();

            // Parse through the commandline parameters...
            username = GetCommandLineParameter(args, "-user", "");
            passwordClearText = GetCommandLineParameter(args, "-password", "");
            password = SharedUtils.SHA1EncryptionBase64(passwordClearText);
            url = GetCommandLineParameter(args, "-url", "");
            _saveDGVDataDumpFile = GetCommandLineParameter(args, "-savedgvdatadumpfile", null);
            _saveListDataDumpFile = GetCommandLineParameter(args, "-savelistdatadumpfile", null);
        }

        private string GetCommandLineParameter(string[] args, string key, string defaultValue)
        {
            string value = defaultValue;
            char[] delimiters = new char[] { '=' };
            foreach (string keyValuePair in args)
            {
                if (keyValuePair.Contains(key)) value = keyValuePair.Split(delimiters)[1].Trim();
            }
            return value;
        }

        private void GrinGlobalClient_Load(object sender, EventArgs e)
        {
            _usernameCooperatorID = "";// "117534";
            _currentCooperatorID = "";// "117534";
            site = "";// "NC7";
            languageCode = "";
            lastFullPath = "";
            lastTabName = "";
            commonUserApplicationDataPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool";
            roamingUserApplicationDataPath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool";
            userSettingsXMLFilePath = roamingUserApplicationDataPath + @"\UserSettings_v" + System.Reflection.Assembly.GetEntryAssembly().GetName().Version.Build.ToString() + ".xml";

            // Wireup the binding navigator and the Main datagridview...
            // NOTE:(right now the binding source is empty - but later on it will get bound to a data table)...
            defaultBindingSource = new BindingSource();
            defaultBindingSource.DataSource = new DataTable();
            ux_bindingnavigatorMain.BindingSource = defaultBindingSource;
            ux_datagridviewMain.DataSource = defaultBindingSource;

            // Create the middle tier utilities class and connect to the web services...
            _sharedUtils = new SharedUtils(url, username, passwordClearText, false, "GRINGlobalClientCuratorTool");

            // Display the splash page to let the user know that things are happening...
            Splash splash = new Splash();
            splash.StartPosition = FormStartPosition.CenterScreen;
            splash.Show();
            splash.Update();

            if (_sharedUtils.IsConnected)
            {
                try
                {
                    // Load the wizards from the same directory (and all subdirectories) where the Curator Tool was launched...
                    System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory());
                    System.IO.FileInfo[] dllFiles = di.GetFiles("Wizards\\*.dll", System.IO.SearchOption.AllDirectories);
                    if (dllFiles != null && dllFiles.Length > 0)
                    {
                        for (int i = 0; i < dllFiles.Length; i++)
                        {
                            System.Reflection.Assembly newAssembly = System.Reflection.Assembly.LoadFile(dllFiles[i].FullName);
                            foreach (System.Type t in newAssembly.GetTypes())
                            {
                                if (t.GetInterface("IGRINGlobalDataWizard", true) != null)
                                {
                                    System.Reflection.ConstructorInfo constInfo = t.GetConstructor(new Type[] { typeof(string), typeof(SharedUtils) });

                                    if (constInfo != null)
                                    {
                                        string pkeyCollection = ":accessionid=; :inventoryid=; :orderrequestid=; :cooperatorid=; :geographyid=; :taxonomygenusid=; :cropid=";
                                        // Instantiate an object of this type to load...
                                        Form wizardForm = (Form)constInfo.Invoke(new object[] { pkeyCollection, _sharedUtils });
                                        // Get the Form Name and button with this name...
                                        System.Reflection.PropertyInfo propInfo = t.GetProperty("FormName", typeof(string));
                                        string formName = (string)propInfo.GetValue(wizardForm, null);
                                        if (string.IsNullOrEmpty(formName)) formName = t.Name;
                                        ToolStripButton tsbWizard = new ToolStripButton(formName, Icon.ExtractAssociatedIcon(newAssembly.ManifestModule.FullyQualifiedName).ToBitmap(), ux_buttonWizard_Click, "toolStripButton" + newAssembly.ManifestModule.Name);
                                        tsbWizard.Tag = "Wizards\\" + newAssembly.ManifestModule.Name;
                                        toolStrip1.Items.Add(tsbWizard);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception err)
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("Error binding to Wizard Form.\nError Message: {0}", "Wizard Binding Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "GrinGlobalClient_LoadMessage2";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    argsArray[0] = err.Message;
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    ggMessageBox.ShowDialog();
                }

                localDBInstance = _sharedUtils.Url.ToLower().Replace("ldap://", "").Replace("http://", "").Replace("https://", "").Replace("/gringlobal/gui.asmx", "").Replace("/gringlobal_remote_debug/gui.asmx", "").Replace('-', '_').Replace('.', '_').Replace(':', '_');
                localDBInstance = "GRINGlobal_" + localDBInstance;
                username = _sharedUtils.Username;
                password = _sharedUtils.Password;
                passwordClearText = _sharedUtils.Password_ClearText;
                url = _sharedUtils.Url;
                _usernameCooperatorID = _sharedUtils.UserCooperatorID;
                site = _sharedUtils.UserSite;
                siteID = _sharedUtils.UserSiteID;
                languageCode = _sharedUtils.UserLanguageCode.ToString();

                // Get the list of Dataview Forms embedded in assemblies in the CT Forms directiory...
                localFormsAssemblies = _sharedUtils.GetDataviewFormsData();

                // Load the application data...
                LoadApplicationData();

                // Show a logon notification if there is an entry for this in the app_settings table (or AppSettings.txt file)...
                string loginBannerText = _sharedUtils.GetAppSettingValue("GrinGlobalClient_loginBanner");
                if (!string.IsNullOrEmpty(loginBannerText))
                {
                    GGMessageBox loginBannerMessageBox = new GGMessageBox(loginBannerText, "", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button2);
                    loginBannerMessageBox.Name = "GrinGlobalClient_loginBanner";
                    int height = loginBannerMessageBox.Height;
                    int width = loginBannerMessageBox.Width;
                    loginBannerMessageBox.Height = 400;
                    loginBannerMessageBox.Width = 550;
                    //_sharedUtils.UpdateControls(loginBanner.Controls, loginBanner.Name);
                    if (DialogResult.Cancel == loginBannerMessageBox.ShowDialog())
                    {
                        this.Close();
                        return;
                    }
                }

                // Check the status of lookup tables and warn the user if some tables are missing data...
                LookupTableStatusCheck();

                // Wire up the list of valid GG servers to the combobox...
                ux_comboboxActiveWebService.DataSource = new BindingSource(_sharedUtils.WebServiceURLs, null);
                ux_comboboxActiveWebService.DisplayMember = "Key";
                ux_comboboxActiveWebService.ValueMember = "Value";
                ux_comboboxActiveWebService.SelectedValue = _sharedUtils.Url;
                // Indicate to the user which URL is the active GG server...
                ux_statusCenterMessage.TextAlign = ContentAlignment.MiddleCenter;
                if (ux_statusCenterMessage.Tag != null)
                {
                    if (ux_statusCenterMessage.Tag.ToString().Contains("{0}"))
                    {
                        ux_statusCenterMessage.Text = string.Format(ux_statusCenterMessage.Tag.ToString(), _sharedUtils.Url);
                    }
                    else
                    {
                        ux_statusCenterMessage.Text = ux_statusCenterMessage.Tag.ToString() + " (" + _sharedUtils.Url + ")";
                    }
                }
                else
                {
                    ux_statusCenterMessage.Text = ux_statusCenterMessage.Text + " (" + _sharedUtils.Url + ")";
                }

                // Save the list of valid web service urls...
                // But first make sure the roaming profile directory exists...
                if (!System.IO.Directory.Exists(roamingUserApplicationDataPath)) System.IO.Directory.CreateDirectory(roamingUserApplicationDataPath);
                // Now save the list of GRIN-Global servers...
                System.IO.StreamWriter sw = new System.IO.StreamWriter(roamingUserApplicationDataPath + @"\WebServiceURL.txt");
                foreach (KeyValuePair<string, string> kv in ux_comboboxActiveWebService.Items)
                {
                    if (kv.Key != "New...")
                    {
                        sw.WriteLine(kv.Key + "\t" + kv.Value);
                    }
                }
                sw.Close();
                sw.Dispose();

                // Load the cursors for the DGV...
                _cursorGG = _sharedUtils.LoadCursor(@"Images\cursor_GG.cur");
                if (_cursorGG == null) _cursorGG = Cursors.Default;
                _cursorLUT = _sharedUtils.LoadCursor(@"Images\cursor_LUT.cur");
                if (_cursorLUT == null) _cursorLUT = Cursors.Default;
                _cursorREQ = _sharedUtils.LoadCursor(@"Images\cursor_REQ.cur");
                if (_cursorREQ == null) _cursorREQ = Cursors.Default;
                //this.Cursor = _cursorGG;
                //ux_datagridviewMain.Cursor = _cursorGG;
            }
            else
            {
                // Login aborted - disable controls...
                ux_tabcontrolDataviewOptions.Enabled = false;
                ux_tabcontrolCTDataviews.Enabled = false;
                ux_datagridviewMain.Enabled = false;
                ux_comboboxCNO.Enabled = false;
                ux_buttonEditData.Enabled = false;
                // Close the application???
                this.Close();
            }

            // Close the splash page...
            splash.Close();
        }

        private void GrinGlobalClient_Shown(object sender, EventArgs e)
        {
            // Wire up the event handler for the timer that keeps the vertical dataview options tab open...
            verticalTabExposedTimer.Tick += verticalTabExposedTimer_Tick;

            // Not sure why the VS Designer keeps changing the size of the checklistbox control - but it does...
            // so I put this next line of code in here to make sure the control is always the correct size.
            ux_checkedlistboxViewedColumns.Size = new Size(210, 409);

            // Don't wireup this event handler for index changes until after the application is fully loaded...
            ux_comboboxActiveWebService.SelectedIndexChanged += new System.EventHandler(this.ux_comboboxActiveWebService_SelectedIndexChanged);

            // Should be safe to wireup the event handlers for the bindingSource now...
            defaultBindingSource.CurrentChanged += new EventHandler(defaultBindingSource_CurrentChanged);

            // Wire up the event handler for formatting cells in readonly mode...
            ux_datagridviewMain.CellFormatting += new DataGridViewCellFormattingEventHandler(ux_datagridviewMain_ReadOnlyDGVCellFormatting);
        }

        void verticalTabExposedTimer_Tick(object sender, EventArgs e)
        {
            int cursorLocationPtoCX = (ux_splitcontainerViewer.PointToClient(Cursor.Position)).X;
            if (cursorLocationPtoCX < ux_tabcontrolDataviewOptions.Location.X)
            {
                ux_tabcontrolDataviewOptions_Leave(sender, e);
            }
            else if (verticalTabExposedTimer.Interval > 1000)
            {
                verticalTabExposedTimer.Stop();
                verticalTabExposedTimer.Interval = 1000;
                verticalTabExposedTimer.Start();
            }
        }

        private void LoadApplicationData()
        {
            int numGroupListTabs = -1;
            int selectedGroupListTabIndex = -1;
            bool warnWhenLUTablesAreOutdated = true;
            bool rememberRowFilters = true;
            bool promptToClearRowFilter = true;
            int maxRowsReturned = 1000;
            int queryPageSize = 10;
            bool stripSpecialCharactersOnExport = false;
            bool stripSpecialCharactersOnImport = false;
            bool highlightChanges = false;
            bool hideCopyPasteWarnings = false;

            int langCode = 1;
            if (!int.TryParse(languageCode, out langCode))
            {
                langCode = 1;
            }

            // Get language translations for the components and controls in this applicataion...
            if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
            if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);

            // Save the statusbar text for the left, center, and right controls...
            if (string.IsNullOrEmpty(ux_statusLeftMessage.Text)) ux_statusLeftMessage.Text = "";
            if (string.IsNullOrEmpty(ux_statusCenterMessage.Text)) ux_statusCenterMessage.Text = "";
            if (string.IsNullOrEmpty(ux_statusRightMessage.Text)) ux_statusRightMessage.Text = "";
            ux_statusLeftMessage.Tag = ux_statusLeftMessage.Text;
            ux_statusCenterMessage.Tag = ux_statusCenterMessage.Text;
            ux_statusRightMessage.Tag = ux_statusRightMessage.Text;

            // Clear the text for the right status bar message...
            ux_statusRightMessage.Text = "";

            // Temp disable the control (until the standard tables are loaded)...
            ux_tabcontrolDataviewOptions.Enabled = false;
            ux_tabcontrolCTDataviews.Enabled = false;
            ux_datagridviewMain.Enabled = false;
            ux_comboboxCNO.Enabled = false;
            ux_buttonEditData.Enabled = true;

            // Don't wireup this event handler for index changes until after the Cooperators are fully loaded and the app is ready to choose the correct Coop...
            this.ux_comboboxCNO.SelectedIndexChanged -= new System.EventHandler(this.ux_comboboxCNO_SelectedIndexChanged);
            this.ux_comboboxCNO.SelectedIndexChanged -= new System.EventHandler(this.ux_comboboxCNO_SelectedIndexChanged);

            LoadStandardTables();

            int.TryParse(_sharedUtils.GetUserSetting("", "ux_tabcontrolGroupListNavigator", "TabPages.Count", "-1"), out numGroupListTabs);
            int.TryParse(_sharedUtils.GetUserSetting("", "ux_tabcontrolGroupListNavigator", "SelectedIndex", "-1"), out selectedGroupListTabIndex);

            // Get the setting for LU table warnings...
            if (bool.TryParse(_sharedUtils.GetUserSetting("", ux_checkboxWarnWhenLUTablesAreOutdated.Name, "Checked", ""), out warnWhenLUTablesAreOutdated))
            {
                ux_checkboxWarnWhenLUTablesAreOutdated.Checked = warnWhenLUTablesAreOutdated;
            }
            else if (bool.TryParse(_sharedUtils.GetAppSettingValue(ux_checkboxWarnWhenLUTablesAreOutdated.Name), out warnWhenLUTablesAreOutdated))
            {
                ux_checkboxWarnWhenLUTablesAreOutdated.Checked = warnWhenLUTablesAreOutdated;
            }
            else
            {
                ux_checkboxWarnWhenLUTablesAreOutdated.Checked = true;
            }
            // Get the setting for max number of rows to return...
            if (int.TryParse(_sharedUtils.GetUserSetting("", ux_numericupdownMaxRowsReturned.Name, "Value", ""), out maxRowsReturned))
            {
                maxRowsReturned = Math.Max(maxRowsReturned, (int)ux_numericupdownMaxRowsReturned.Minimum);
                ux_numericupdownMaxRowsReturned.Value = maxRowsReturned;
            }
            else if (int.TryParse(_sharedUtils.GetAppSettingValue(ux_numericupdownMaxRowsReturned.Name), out maxRowsReturned))
            {
                maxRowsReturned = Math.Max(maxRowsReturned, (int)ux_numericupdownMaxRowsReturned.Minimum);
                ux_numericupdownMaxRowsReturned.Value = maxRowsReturned;
            }
            else
            {
                ux_numericupdownMaxRowsReturned.Value = ux_numericupdownMaxRowsReturned.Minimum;
            }
            // Get the setting for query page size to use...
            if (int.TryParse(_sharedUtils.GetUserSetting("", ux_numericupdownQueryPageSize.Name, "Value", ""), out queryPageSize))
            {
                queryPageSize = Math.Max(queryPageSize, (int)ux_numericupdownQueryPageSize.Minimum);
                ux_numericupdownQueryPageSize.Value = queryPageSize;
            }
            else if (int.TryParse(_sharedUtils.GetAppSettingValue(ux_numericupdownQueryPageSize.Name), out queryPageSize))
            {
                queryPageSize = Math.Max(queryPageSize, (int)ux_numericupdownQueryPageSize.Minimum);
                ux_numericupdownQueryPageSize.Value = queryPageSize;
            }
            else
            {
                ux_numericupdownQueryPageSize.Value = ux_numericupdownQueryPageSize.Minimum;
            }
            // Get the setting for remembering row filters...
            if (bool.TryParse(_sharedUtils.GetUserSetting("", ux_checkboxRememberRowFilters.Name, "Checked", ""), out rememberRowFilters))
            {
                ux_checkboxRememberRowFilters.Checked = rememberRowFilters;
            }
            else if (bool.TryParse(_sharedUtils.GetAppSettingValue(ux_checkboxRememberRowFilters.Name), out rememberRowFilters))
            {
                ux_checkboxRememberRowFilters.Checked = rememberRowFilters;
            }
            else
            {
                ux_checkboxRememberRowFilters.Checked = true;
            }
            // Get the setting for prompting to clear row filters...
            if (bool.TryParse(_sharedUtils.GetUserSetting("", ux_checkboxPromptToClearRowFilter.Name, "Checked", ""), out promptToClearRowFilter))
            {
                ux_checkboxPromptToClearRowFilter.Checked = promptToClearRowFilter;
            }
            else if (bool.TryParse(_sharedUtils.GetAppSettingValue(ux_checkboxPromptToClearRowFilter.Name), out promptToClearRowFilter))
            {
                ux_checkboxPromptToClearRowFilter.Checked = promptToClearRowFilter;
            }
            else
            {
                ux_checkboxPromptToClearRowFilter.Checked = true;
            }
            // Disable the setting for prompting to clear row filters if remember row filters is unchecked...
            if (ux_checkboxRememberRowFilters.Checked)
            {
                ux_checkboxPromptToClearRowFilter.Enabled = true;
            }
            else
            {
                ux_checkboxPromptToClearRowFilter.Enabled = false;
            }
            // Get the setting for remembering strip special characters on export...
            if (bool.TryParse(_sharedUtils.GetUserSetting("", ux_checkboxStripSpecialCharactersOnExport.Name, "Checked", ""), out stripSpecialCharactersOnExport))
            {
                ux_checkboxStripSpecialCharactersOnExport.Checked = stripSpecialCharactersOnExport;
            }
            else if (bool.TryParse(_sharedUtils.GetAppSettingValue(ux_checkboxStripSpecialCharactersOnExport.Name), out stripSpecialCharactersOnExport))
            {
                ux_checkboxStripSpecialCharactersOnExport.Checked = stripSpecialCharactersOnExport;
            }
            else
            {
                ux_checkboxStripSpecialCharactersOnExport.Checked = false;
            }
            // Get the setting for remembering strip special characters on import...
            if (bool.TryParse(_sharedUtils.GetUserSetting("", ux_checkboxStripSpecialCharactersOnImport.Name, "Checked", ""), out stripSpecialCharactersOnImport))
            {
                ux_checkboxStripSpecialCharactersOnImport.Checked = stripSpecialCharactersOnImport;
            }
            else if (bool.TryParse(_sharedUtils.GetAppSettingValue(ux_checkboxStripSpecialCharactersOnImport.Name), out stripSpecialCharactersOnImport))
            {
                ux_checkboxStripSpecialCharactersOnImport.Checked = stripSpecialCharactersOnImport;
            }
            else
            {
                ux_checkboxStripSpecialCharactersOnImport.Checked = false;
            }

            // Get the setting for remembering Highlight Changes...
            if (bool.TryParse(_sharedUtils.GetUserSetting("", ux_checkboxHighlightChanges.Name, "Checked", ""), out highlightChanges))
            {
                ux_checkboxHighlightChanges.Checked = highlightChanges;
            }
            else if (bool.TryParse(_sharedUtils.GetAppSettingValue(ux_checkboxHighlightChanges.Name), out highlightChanges))
            {
                ux_checkboxHighlightChanges.Checked = highlightChanges;
            }
            else
            {
                ux_checkboxHighlightChanges.Checked = false;
            }
            // Get the setting for remembering Hide Copy/Paste Warnings...
            if (bool.TryParse(_sharedUtils.GetUserSetting("", ux_checkboxHideCopyPasteWarnings.Name, "Checked", ""), out hideCopyPasteWarnings))
            {
                ux_checkboxHideCopyPasteWarnings.Checked = hideCopyPasteWarnings;
            }
            else if (bool.TryParse(_sharedUtils.GetAppSettingValue(ux_checkboxHideCopyPasteWarnings.Name), out hideCopyPasteWarnings))
            {
                ux_checkboxHideCopyPasteWarnings.Checked = hideCopyPasteWarnings;
            }
            else
            {
                ux_checkboxHideCopyPasteWarnings.Checked = false;
            }

            // Get the setting for Block Mode keyboard mapping...
            string blockmodeKeyboardMappingValue = _sharedUtils.GetKeyboardMapping("Block Mode", "B, Control");
            if(!string.IsNullOrEmpty(blockmodeKeyboardMappingValue))
            {
                KeysConverter kc = new KeysConverter();
                _blockmodeKeyboardMapping = (Keys)kc.ConvertFromString(blockmodeKeyboardMappingValue);
            }
            else
            {
                _blockmodeKeyboardMapping = Keys.Control | Keys.Oemtilde;
            }

            // Build the horizontal Dataview tabs TabControl...
            _sharedUtils.BuildDataviewTabControl(ux_tabcontrolCTDataviews);
            // Bind the image list to the Dataview tabs TabControl (for the New Tab tab)...
            ux_tabcontrolCTDataviews.ImageList = BuildTabControlImageList();
            // Bind an image to the NewTab tab...
            if (ux_tabcontrolCTDataviews.TabPages.ContainsKey("ux_tabpageDataviewNewTab")) ux_tabcontrolCTDataviews.TabPages["ux_tabpageDataviewNewTab"].ImageKey = "new_tab";

            // Set the active dataview tab to null (so that no data is retrieved at startup which should speed up loading the CT)
            ux_tabcontrolCTDataviews.SelectedTab = null;

            // Set the image for the Search button...
            ux_buttonSearch.ImageList = BuildTabControlImageList();
            ux_buttonSearch.ImageKey = "search";
            ux_buttonSearch.ImageAlign = ContentAlignment.MiddleLeft;

            // Loading vital lookup tables is done and the dataview tabs are built - enable the controls...
            ux_tabcontrolDataviewOptions.Enabled = true;
            ux_tabcontrolCTDataviews.Enabled = true;
            ux_datagridviewMain.Enabled = true;
            ux_comboboxCNO.Enabled = true;
            ux_buttonEditData.Enabled = true;


            // Load the list navigator combobox with cooperators...
            LoadCooperators(localDBInstance);
            // Deselect the CNO in the cooperator combobox to make sure that when the following SelectedValue=cno will trigger the index changed event...
            ux_comboboxCNO.SelectedValue = -1;
            // We are done getting system data time to wireup this event handler...
            this.ux_comboboxCNO.SelectedIndexChanged += new System.EventHandler(this.ux_comboboxCNO_SelectedIndexChanged);
            // Select the CNO (from login) in the cooperator combobox (this will trigger code to populate the tabs and treeviews too)...
            ux_comboboxCNO.SelectedValue = _usernameCooperatorID;

            // Save the state of the statusbar (to restore later)...
            ux_statusstripMain.DefaultStateSave();
        }

        private void LoadStandardTables()
        {
            if (!_sharedUtils.LocalDatabaseTableExists("code_value_lookup") ||
                !_sharedUtils.LocalDatabaseTableExists("cooperator_lookup"))
            {
                // Looks like the code_value lookup table is not downloaded to the local machine and because it is really needed
                // and we can't count on the user to load it manually, we will automatically load it for them...
                Splash splash = new Splash();
                splash.StartPosition = FormStartPosition.CenterScreen;
                splash.Show();
                splash.Update();
                _sharedUtils.LookupTablesLoadTableFromDatabase("code_value_lookup");
                //_sharedUtils.LookupTablesLoadTableFromDatabase("cooperator_lookup");
                LoadUserSiteCooperatorsLUT();
                splash.Close();
            }
            else
            {
                // The code_value and cooperator LU tables exist so let's update them right now...
                _sharedUtils.LookupTablesUpdateTable("code_value_lookup", false);
                _sharedUtils.LookupTablesUpdateTable("cooperator_lookup", true);
            }
        }

        private void LoadUserSiteCooperatorsLUT()
        {
            //" AND @accession.accession_number_part2 = " + number.ToString();
            string fullCooperatorLUParameterList = ":createddate=" + DateTime.Today.AddYears(1).ToString("s") + "; :modifieddate=" + DateTime.Today.AddYears(1).ToString("s") + "; :startpkey=; :stoppkey=; :valuemember=";
            DataSet dsCoopPKEYS = _sharedUtils.SearchWebService("@cooperator.cooperator_id IN (SELECT cooperator_id FROM sys_user WHERE is_enabled = 'Y')", true, true, "", "cooperator", 0, 0);
            if (dsCoopPKEYS != null &&
                dsCoopPKEYS.Tables.Contains("SearchResult"))
            {
                foreach (DataRow dr in dsCoopPKEYS.Tables["SearchResult"].Rows)
                {
                    fullCooperatorLUParameterList += dr["ID"] + ",";
                }
            }
            DataSet dsInitialCoopLUT = _sharedUtils.GetWebServiceData("cooperator_lookup", fullCooperatorLUParameterList.TrimEnd(','), 0, 0);
            if (dsInitialCoopLUT != null &&
                dsInitialCoopLUT.Tables.Contains("cooperator_lookup"))
            {
                _sharedUtils.LookupTablesSaveDataPageToLocalDB(dsInitialCoopLUT.Tables["cooperator_lookup"]);
            }
        }

        private void LookupTableStatusCheck()
        {
            // Set the statusbar up to show the progress of updating the lookup tables...
            ContentAlignment statusCenterMessageAllignment = ux_statusCenterMessage.TextAlign;
            string statusCenterMessage = ux_statusCenterMessage.Text;
            bool statusRightProgressBarVisible = ux_statusRightProgressBar.Visible;
            ux_statusCenterMessage.TextAlign = ContentAlignment.MiddleRight;
            ux_statusCenterMessage.Text = "One Moment Please - Gathering Data to Update Lookup Tables...";
            ux_statusRightProgressBar.Visible = true;
            ux_statusRightProgressBar.Minimum = 0;
            ux_statusRightProgressBar.Maximum = 100; // tempLookupTables.Tables.Count;
            ux_statusRightProgressBar.Step = 1;
            ux_statusRightProgressBar.Value = 0;
            ux_statusstripMain.Refresh();

            if (!backgroundworkerUpdateLookupTables.IsBusy) backgroundworkerUpdateLookupTables.RunWorkerAsync();
        }

        void ux_DGVCellReport_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;

            DataTable dt = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).Clone();

            // NOTE: because of the way the DGV adds rows to the selectedRows collection
            //       we have to process the rows in the opposite direction they were selected in...
            int rowStart = 0;
            int rowStop = ux_datagridviewMain.SelectedRows.Count;
            int stepValue = 1;
            if (ux_datagridviewMain.SelectedRows.Count > 1 && ux_datagridviewMain.SelectedRows[0].Index > ux_datagridviewMain.SelectedRows[1].Index)
            {
                rowStart = ux_datagridviewMain.SelectedRows.Count - 1;
                rowStop = -1;
                stepValue = -1;
            }

            DataGridViewRow dgvrow = null;
            // Process the rows in the opposite direction they were selected by the user...
            for (int i = rowStart; i != rowStop; i += stepValue)
            {
                dgvrow = ux_datagridviewMain.SelectedRows[i];
                if (!dgvrow.IsNewRow)
                {
                    dt.Rows.Add(((DataRowView)dgvrow.DataBoundItem).Row.ItemArray);
                }
            }

            string fullPathName = tsmi.Tag.ToString();
            _sharedUtils.PrintReport(dt, fullPathName);

            RefreshMainDGVFormatting();
        }

        private void RefreshDGVLookups()
        {
            if (ux_tabcontrolCTDataviews.SelectedTab != null &&
                ux_tabcontrolCTDataviews.SelectedTab.Tag != null &&
                ux_tabcontrolCTDataviews.SelectedTab.Tag.GetType() == typeof(DataviewProperties) &&
                !string.IsNullOrEmpty(((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName))
            {
                string dataGridViewTableName = ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName;
                if (grinData.Tables.Contains(dataGridViewTableName))
                {
                    DataTable dt = grinData.Tables[dataGridViewTableName];
                    List<string> allLookupTablesToUpdate = new List<string>();
                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dc.ExtendedProperties.ContainsKey("gui_hint") &&
                            dc.ExtendedProperties["gui_hint"].ToString().Trim().ToUpper() == "LARGE_SINGLE_SELECT_CONTROL" &&
                            dc.ExtendedProperties.ContainsKey("foreign_key_dataview_name") &&
                            !string.IsNullOrEmpty(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim()))
                        {
                            allLookupTablesToUpdate.Add(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim());
                        }
                    }
                    List<string> distinctLookupTablesToUpdate = allLookupTablesToUpdate.Distinct().ToList();
                    if (distinctLookupTablesToUpdate.Count > 0)
                    {
                        foreach (string lookupTableName in distinctLookupTablesToUpdate)
                        {
                            _sharedUtils.LookupTablesUpdateTable(lookupTableName, false);
                        }
                    }
                }
            }
        }

        private void RefreshMainDGVData()
        {
            string currentFullPath = "";
            string currentTabText = "";
            string dataGridViewTableName = "";
            string paramsIn = "";
            List<KeyValuePair<string, int>> itemList = new List<KeyValuePair<string, int>>();
            bool rebuildDGV = false;

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            if (ux_tabcontrolCTDataviews.SelectedTab == null ||
                ux_tabcontrolCTDataviews.SelectedTab.Tag == null ||
                ux_tabcontrolCTDataviews.SelectedTab.Tag.GetType() != typeof(DataviewProperties) ||
                string.IsNullOrEmpty(((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName))
            {
                // Create a new empty datagridview for the spreadsheet view...
                _sharedUtils.BuildReadOnlyDataGridView(ux_datagridviewMain, new DataTable()); //grinData.Tables[dataGridViewTableName]);
                return;
            }
            else
            {
                if (ux_tabcontrolCTDataviews.SelectedTab.Tag.GetType() == typeof(DataviewProperties))
                {
                    dataGridViewTableName = ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName;
                }
            }

            // Get the currently selected node from the treeview embedded in the tabcontrol...
            // NOTE: if these tab pages start holding more than just the treeview this will need to inspect by name instead of using the index=0 to get to the treeview control
            if (ux_comboboxCNO.SelectedValue != null &&
                _ux_NavigatorTabControl.TabPages.Count > 1 &&
                _ux_NavigatorTabControl.SelectedIndex > -1 &&
                _ux_NavigatorTabControl.SelectedTab.Name != "ux_tabpageGroupListNavigatorNewTab")
            {
                TabPage tp = _ux_NavigatorTabControl.SelectedTab;
                TreeView tv = null;
                TreeNode tn = null;

                // Get the current dataview's parameter list...
                string dvParamList = "";
                DataSet dsParms = _sharedUtils.GetWebServiceData("get_dataview_parameters", ":dataview=" + dataGridViewTableName, 0, 0);
                if (dsParms != null && dsParms.Tables.Contains("get_dataview_parameters"))
                {
                    foreach (DataRow dr in dsParms.Tables["get_dataview_parameters"].Rows)
                    {
                        dvParamList += dr["param_name"].ToString().Trim().ToLower() + "=; ";
                    }
                }

                // Get the current dataview's PKey...
                string dvPKeyName = "";
                if (ux_tabcontrolCTDataviews.SelectedTab.Tag != null)
                {
                    string dataviewName = ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName;
                    // We need to know the pkey for the currently selected dataview tab is so go get zero rows for that dataview 
                    // and inspect the result to learn the pkey name...
                    DataSet dsTemp = GetDGVData(dataviewName, new List<KeyValuePair<string, int>>(), dvParamList, Convert.ToInt32(ux_numericupdownQueryPageSize.Value));
                    if (dsTemp != null &&
                        dsTemp.Tables.Contains(dataviewName) &&
                        dsTemp.Tables[dataviewName].PrimaryKey.Length == 1)
                    {
                        dvPKeyName = dsTemp.Tables[dataviewName].PrimaryKey[0].ColumnName.Trim().ToLower();
                    }
                }

                // Get the current treeview's selected node...
                if (tp != null) tv = (TreeView)tp.Controls[tp.Name + "TreeView"];
                if (tv != null) tn = tv.SelectedNode;

                if (tn != null)
                {
                    // Remember the current tab and node to the global variables (to decide if data needs to be retrieved)...
                    currentFullPath = tn.FullPath;
                    currentTabText = _ux_NavigatorTabControl.SelectedTab.Text;

                    // If the node has changed go get the data - but if we have already retrieved the data for this node don't do it again...
                    if (lastTabName.Trim() != currentTabText || lastFullPath.Trim() != currentFullPath.Trim())
                    {
                        // Build the pkey list that will be passed on to GetDGVData()...
                        itemList.AddRange(BuildPKeyList(tn, dvParamList, dvPKeyName, ux_checkboxIncludeSubFolders.Checked));

                        // Changed the list of IVIDs, ACIDs, and ORNOs so clear all tables in the dataset...
                        grinData.Tables.Clear();
                        // Go get the records for the new list and currently active table view (using the new pkey list)...
                        if (itemList.Count > 0 ||
                            tn.Tag.ToString().Trim().StartsWith("QUERY"))
                        {
                            grinData = GetDGVData(dataGridViewTableName, itemList, dvParamList, Convert.ToInt32(ux_numericupdownQueryPageSize.Value));
                        }

                        // There is new data so rebuild the DGV...
                        rebuildDGV = true;

                        lastFullPath = currentFullPath.Trim();
                        lastTabName = currentTabText.Trim();
                    }
                }

                // If the data for the currently selected dataview is not loaded - get it now...
                if (!grinData.Tables.Contains(dataGridViewTableName))
                {
                    DataSet dsTemp = new DataSet();

                    // Build the pkey list that will be passed on to GetDGVData()...
                    itemList.AddRange(BuildPKeyList(tn, dvParamList, dvPKeyName, ux_checkboxIncludeSubFolders.Checked));

                    // Have not looked at this view for the currently selected list of IVIDs, ACIDs, and ORNOs so go get it...
                    dsTemp = GetDGVData(dataGridViewTableName, itemList, dvParamList, Convert.ToInt32(ux_numericupdownQueryPageSize.Value));
                    if (dsTemp.Tables.Contains(dataGridViewTableName))
                    {
                        grinData.Tables.Add(dsTemp.Tables[dataGridViewTableName].Copy());
                    }
                    else
                    {
                        grinData.Tables.Add(new DataTable(dataGridViewTableName));
                    }

                    // There is new data so rebuild the DGV...
                    rebuildDGV = true;
                }
                else
                {
                    // If the user has clicked on a dataview that already has data...
                    if (ux_datagridviewMain != null &&
                        ux_datagridviewMain.DataSource != null)
                    {
                        //  The current dataview is not the one that is being shown (but it should be so rebuild the DGV)...
                        if ((ux_datagridviewMain.DataSource.GetType() == typeof(BindingSource) &&
                            ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).TableName != dataGridViewTableName) ||
                            ((ux_datagridviewMain.DataSource.GetType() == typeof(DataTable) &&
                            ((DataTable)ux_datagridviewMain.DataSource).TableName != dataGridViewTableName)))
                        {
                            // Rebuild the DGV to show the data for the dataview tab the user just clicked on...
                            rebuildDGV = true;
                        }
                    }
                }

                // Restore the sort order and row filter...
                try
                {
                    grinData.Tables[dataGridViewTableName].DefaultView.Sort = _sharedUtils.GetUserSetting("", dataGridViewTableName, "DefaultView.Sort", "");
                    grinData.Tables[dataGridViewTableName].DefaultView.RowFilter = _sharedUtils.GetUserSetting("", dataGridViewTableName, "DefaultView.RowFilter", "");
                }
                catch (Exception)
                {
                    grinData.Tables[dataGridViewTableName].DefaultView.Sort = "";
                }

                // Do we need to rebuild the DGV???
                if (rebuildDGV)
                {
                    // Reset the datagridview Find Results...
                    _dgvcFindResults.Clear();
                    _dgvcFindIndex = 0;
                    bindingNavigatorFindNext.Enabled = false;
                    bindingNavigatorFindPrev.Enabled = false;

                    // Create a new datagridview for the spreadsheet view...
                    _sharedUtils.BuildReadOnlyDataGridView(ux_datagridviewMain, grinData.Tables[dataGridViewTableName]);

                    // Force a gargabe collection...
                    GC.Collect();
                }

                // If the current row filter is hiding all rows in the datatable alert the user and let them decide what to do...
                DataTable datagridviewTable = (DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource;
                if (datagridviewTable.Rows.Count > 0 &&
                    datagridviewTable.DefaultView.Count == 0)
                {
                    if (ux_checkboxPromptToClearRowFilter.Checked)
                    {
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} retrieved records but your row filter is hiding all of them - would you like to clear the row filter?", "Clear Row Filter", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                        ggMessageBox.Name = "RefreshMainDGVData1";
                        _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                        string[] argsArray = new string[100];
                        argsArray[0] = grinData.Tables[dataGridViewTableName].Rows.Count.ToString();
                        ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                        if (DialogResult.Yes == ggMessageBox.ShowDialog())
                        {
                            // The user wants to clear the row filter...
                            grinData.Tables[dataGridViewTableName].DefaultView.RowFilter = "";
                            datagridviewTable.DefaultView.RowFilter = "";
                            // Remember the cleared row filter...
                            _sharedUtils.SaveUserSetting("", dataGridViewTableName, "DefaultView.RowFilter", "");
                        }
                    }
                }
            }
            else
            {
                // There is no treeview node selected so create an empty dataview table...
                grinData = GetDGVData(dataGridViewTableName, itemList, "", Convert.ToInt32(ux_numericupdownQueryPageSize.Value));
                // Restore the sort order...
                grinData.Tables[dataGridViewTableName].DefaultView.Sort = _sharedUtils.GetUserSetting("", dataGridViewTableName, "DefaultView.Sort", "");
                // Create a new datagridview for the spreadsheet view...
                _sharedUtils.BuildReadOnlyDataGridView(ux_datagridviewMain, grinData.Tables[dataGridViewTableName]);
            }


            // Bind the context menu to the column header and set the DGV header text...
            foreach (DataGridViewColumn dgvc in ux_datagridviewMain.Columns)
            {
                dgvc.SortMode = DataGridViewColumnSortMode.Programmatic;
                dgvc.HeaderCell.ContextMenuStrip = ux_contextmenustripDGVHeader;
                dgvc.ContextMenuStrip = ux_contextmenustripReadOnlyDGVCell;
                dgvc.HeaderText = _sharedUtils.GetFriendlyFieldName(((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).Columns[dgvc.Index], ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).Columns[dgvc.Index].ColumnName);
            }

            // Reset the checkedlistbox of column names (and then re-add them as unchecked)...
            ux_checkedlistboxViewedColumns.Items.Clear();
            foreach (DataColumn dc in ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).Columns)
            {
                ux_checkedlistboxViewedColumns.Items.Add(_sharedUtils.GetFriendlyFieldName(dc, dc.ColumnName), false);
            }

            // Get the user's settings for visible columns...
            string[] columnsVisible = _sharedUtils.GetUserSetting("", dataGridViewTableName, "Columns.Visible", "").Split(' ');

            // Clear the visible columns...
            ux_checkboxSelectAll.CheckState = CheckState.Unchecked;

            // Now go back through the checkedlistbox and make columns visible based on user settings...
            if (columnsVisible.Length == 1 && columnsVisible[0] == "")
            {
                // Check all checkboxes in the checkbox list...
                ux_checkboxSelectAll.CheckState = CheckState.Checked;
            }
            else
            {
                int columnNum = -1;
                foreach (string column in columnsVisible)
                {
                    if (int.TryParse(column, out columnNum) &&
                        columnNum < ux_checkedlistboxViewedColumns.Items.Count) ux_checkedlistboxViewedColumns.SetItemChecked(columnNum, true);
                }
            }

            // Update the statusbar...
            if (ux_statusLeftMessage.Tag.ToString().Contains("{0}") &&
                ux_statusLeftMessage.Tag.ToString().Contains("{1}"))
            {
                ux_statusLeftMessage.Text = string.Format(ux_statusLeftMessage.Tag.ToString(), grinData.Tables[dataGridViewTableName].DefaultView.Count.ToString(), grinData.Tables[dataGridViewTableName].Rows.Count.ToString());
            }
            else if (ux_statusLeftMessage.Tag.ToString().Contains("{0}"))
            {
                ux_statusLeftMessage.Text = string.Format(ux_statusLeftMessage.Tag.ToString(), grinData.Tables[dataGridViewTableName].Rows.Count.ToString());
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void RefreshMainDGVFormatting()
        {
            // If the datagridview has no data source or the selected tab's tag has no dataview string - bail out now...
            if (ux_datagridviewMain.DataSource == null ||
                ux_tabcontrolCTDataviews.SelectedTab == null ||
                ux_tabcontrolCTDataviews.SelectedTab.Tag == null ||
                ux_tabcontrolCTDataviews.SelectedTab.Tag.GetType() != typeof(DataviewProperties) ||
                string.IsNullOrEmpty(((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName))
            {
                // Update the statusbar...
                if (ux_statusLeftMessage.Tag.ToString().Contains("{0}") &&
                    ux_statusLeftMessage.Tag.ToString().Contains("{1}"))
                {
                    ux_statusLeftMessage.Text = string.Format(ux_statusLeftMessage.Tag.ToString(), "0", "0");
                }
                else if (ux_statusLeftMessage.Tag.ToString().Contains("{0}"))
                {
                    ux_statusLeftMessage.Text = string.Format(ux_statusLeftMessage.Tag.ToString(), "0");
                }
                // Indicate to the user which URL is the active GG server...
                ux_statusCenterMessage.TextAlign = ContentAlignment.MiddleCenter;
                if (ux_statusCenterMessage.Tag != null)
                {
                    if (ux_statusCenterMessage.Tag.ToString().Contains("{0}"))
                    {
                        ux_statusCenterMessage.Text = string.Format(ux_statusCenterMessage.Tag.ToString(), _sharedUtils.Url);
                    }
                    else if (!ux_statusCenterMessage.Tag.ToString().Contains(_sharedUtils.Url))
                    {
                        ux_statusCenterMessage.Text = ux_statusCenterMessage.Tag.ToString() + " (" + _sharedUtils.Url + ")";
                    }
                }
                else
                {
                    if (!ux_statusCenterMessage.Text.Contains(_sharedUtils.Url))
                    {
                        ux_statusCenterMessage.Text = ux_statusCenterMessage.Text + " (" + _sharedUtils.Url + ")";
                    }
                }
                return;
            }

            // Highlight the changed grid cells for each datagridview row if the checkbox indicates and the DGV is in edit mode...
            bool useHighlighting = ux_checkboxHighlightChanges.Checked && ux_buttonSaveData.Enabled;
            foreach (DataGridViewRow dgvr in ux_datagridviewMain.Rows)
            {
                RefreshDGVRowFormatting(dgvr, useHighlighting);
            }

            // If in readonly mode arrange the columns as the user settings indicate (columns visible, sorting, widths based on user settings)...
            if (ux_buttonEditData.Enabled)
            {
                string[] columnsVisible = _sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "Columns.Visible", "").Split(' ');
                if (columnsVisible.Length == 1 && columnsVisible[0] == "")
                {
                    // Check all checkboxes in the checkbox list...
                    ux_checkboxSelectAll.CheckState = CheckState.Checked;
                }
                else
                {
                    foreach (DataGridViewColumn dgvc in ux_datagridviewMain.Columns)
                    {
                        if (columnsVisible.Contains(dgvc.Index.ToString()))
                        {
                            dgvc.Visible = true;
                        }
                        else
                        {
                            dgvc.Visible = false;
                        }
                    }
                }

                // Show SortGlyphs for the column headers (this takes two steps)...
                // First reset them all to No Sort...
                foreach (DataGridViewColumn dgvc in ux_datagridviewMain.Columns)
                {
                    dgvc.HeaderCell.SortGlyphDirection = SortOrder.None;
                }
                // Now inspect the sort string from the datatable in use to set the SortGlyphs...
                string strOrder = "";
                string strRowFilter = "";
                if (ux_datagridviewMain.DataSource.GetType() == typeof(BindingSource))
                {
                    strOrder = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.Sort;
                    strRowFilter = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.RowFilter;
                }
                else
                {
                    strOrder = ((DataTable)ux_datagridviewMain.DataSource).DefaultView.Sort;
                    strRowFilter = ((DataTable)ux_datagridviewMain.DataSource).DefaultView.RowFilter;
                }
                char[] chararrDelimiters = { ',' };
                string[] strarrSortCols = strOrder.Split(chararrDelimiters);
                foreach (string strSortCol in strarrSortCols)
                {
                    if (strSortCol.Contains("ASC"))
                    {
                        if (ux_datagridviewMain.Columns.Contains(strSortCol.Replace(" ASC", ""))) ux_datagridviewMain.Columns[strSortCol.Replace(" ASC", "")].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                    }
                    if (strSortCol.Contains("DESC"))
                    {
                        if (ux_datagridviewMain.Columns.Contains(strSortCol.Replace(" DESC", ""))) ux_datagridviewMain.Columns[strSortCol.Replace(" DESC", "")].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                    }
                }

                // If a row filter is applied change the background color for the tabpage displayed...
                if (!string.IsNullOrEmpty(strRowFilter))
                {
                    ux_tabcontrolCTDataviews.SelectedTab.BackColor = Color.Orange;
                    ux_statusLeftMessage.BackColor = Color.Orange;
                }
                else
                {
                    ux_tabcontrolCTDataviews.SelectedTab.BackColor = SystemColors.Control;
                    ux_statusLeftMessage.BackColor = Color.Empty;
                }

                // Now restore the column width and ordering...
                string[] columnWidths = _sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "Columns.Width", "").Split(' ');
                string[] columnOrder = _sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "Columns.DisplayOrder", "").Split(' ');
                int columnNum = -1;
                SortedDictionary<int, int> sdDisplayIndex = new SortedDictionary<int, int>();
                if (columnWidths.Length == columnOrder.Length && columnWidths.Length == ux_datagridviewMain.Columns.Count)
                {
                    for (int i = 0; i < columnOrder.Length; i++)
                    {
                        if (int.TryParse(columnOrder[i], out columnNum)) sdDisplayIndex[columnNum] = i;
                    }

                    for (int i = 0; i < ux_datagridviewMain.Columns.Count; i++)
                    {
                        if (int.TryParse(columnWidths[i], out columnNum)) ux_datagridviewMain.Columns[i].Width = columnNum;// Convert.ToInt32(columnWidths[i]);
                        if (sdDisplayIndex.Keys.Contains(i)) ux_datagridviewMain.Columns[sdDisplayIndex[i]].DisplayIndex = i;
                    }
                }
            }
            else
            {
                string[] columnsVisible = _sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "Columns.Visible", "").Split(' ');
                if (columnsVisible.Length == 1 && columnsVisible[0] == "")
                {
                    // Check all checkboxes in the checkbox list...
                    ux_checkboxSelectAll.CheckState = CheckState.Checked;
                }
                else
                {
                    DataTable dt = (DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource;
                    foreach (DataGridViewColumn dgvc in ux_datagridviewMain.Columns)
                    {
                        if (columnsVisible.Contains(dgvc.Index.ToString()) /*||
                            dt.Columns[dgvc.Name].ExtendedProperties["is_nullable"].ToString() == "N"*/)
                        {
                            dgvc.Visible = true;
                        }
                        else
                        {
                            dgvc.Visible = false;
                        }
                    }
                }
            }

            // Now restore the default cell color and alternating rows default cell color...
            int backgroundColor;
            ux_datagridviewMain.DefaultCellStyle.BackColor = Color.White;
            ux_datagridviewMain.AlternatingRowsDefaultCellStyle.BackColor = Color.White;
            if (int.TryParse(_sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "DefaultCellStyle.BackColor", ""), out backgroundColor))
            {
                ux_datagridviewMain.DefaultCellStyle.BackColor = Color.FromArgb(backgroundColor);
            }
            if (int.TryParse(_sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "AlternatingRowsDefaultCellStyle.BackColor", ""), out backgroundColor))
            {
                ux_datagridviewMain.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(backgroundColor);
            }

            // Update the row count in the status bar...
            if (ux_statusLeftMessage.Tag != null)
            {
                if (ux_statusLeftMessage.Tag.ToString().Contains("{0}") &&
                    ux_statusLeftMessage.Tag.ToString().Contains("{1}"))
                {
                    ux_statusLeftMessage.Text = string.Format(ux_statusLeftMessage.Tag.ToString(), ux_datagridviewMain.Rows.Count.ToString(), grinData.Tables[((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName].Rows.Count.ToString());
                }
                else if (ux_statusLeftMessage.Tag.ToString().Contains("{0}"))
                {
                    ux_statusLeftMessage.Text = string.Format(ux_statusLeftMessage.Tag.ToString(), ux_datagridviewMain.Rows.Count.ToString());
                }
            }
            // Indicate to the user which URL is the active GG server...
            ux_statusCenterMessage.TextAlign = ContentAlignment.MiddleCenter;
            if (ux_statusCenterMessage.Tag != null)
            {
                if (ux_statusCenterMessage.Tag.ToString().Contains("{0}"))
                {
                    ux_statusCenterMessage.Text = string.Format(ux_statusCenterMessage.Tag.ToString(), _sharedUtils.Url);
                }
                else if (!ux_statusCenterMessage.Tag.ToString().Contains(_sharedUtils.Url))
                {
                    ux_statusCenterMessage.Text = ux_statusCenterMessage.Tag.ToString() + " (" + _sharedUtils.Url + ")";
                }
            }
            else
            {
                if (!ux_statusCenterMessage.Text.Contains(_sharedUtils.Url))
                {
                    ux_statusCenterMessage.Text = ux_statusCenterMessage.Text + " (" + _sharedUtils.Url + ")";
                }
            }
        }

        private void RefreshDGVRowFormatting(DataGridViewRow dgvr, bool useHighlighting)
        {
            DataRow dr;

            // Reset the rows cells to the default color scheme...
            foreach (DataGridViewCell dgvc in dgvr.Cells)
            {
                dgvc.Style.BackColor = Color.Empty;
                dgvc.Style.SelectionBackColor = Color.Empty;
            }

            try
            {
                if (dgvr.Index > ((BindingSource)dgvr.DataGridView.DataSource).Count - 1) return;
            }
            catch
            {
                return;
            }

            if (!dgvr.IsNewRow)
            {
                dr = ((DataRowView)dgvr.DataBoundItem).Row;
                if (useHighlighting)
                {
                    // If this is a row that has just been added (or is a "NewRow" that is currently being edited)...
                    if (dr.RowState == DataRowState.Added || dr.RowState == DataRowState.Detached)
                    {
                        foreach (DataGridViewCell dgvc in dgvr.Cells)
                        {
                            // Make the default cell color dark blue...
                            dgvc.Style.BackColor = Color.SteelBlue;
                            dgvc.Style.ForeColor = Color.White;
                            // But if this is a read-only column make it light gray...
                            if (dr.Table.Columns[dgvc.ColumnIndex].ExtendedProperties.Contains("is_readonly") &&
                                dr.Table.Columns[dgvc.ColumnIndex].ExtendedProperties["is_readonly"].ToString() == "Y")
                            {
                                dgvc.Style.BackColor = Color.LightGray;
                                dgvc.Style.ForeColor = Color.Empty;
                            }
                            // Or if there is data in the datarow for this cell (that is different than the default boolean value) make it light blue...
                            else if (dr[dgvc.ColumnIndex] != DBNull.Value)
                            {
                                if (dr.Table.Columns[dgvc.ColumnIndex].ExtendedProperties.Contains("default_value") &&
                                    dr.Table.Columns[dgvc.ColumnIndex].ExtendedProperties["default_value"].ToString() == dr[dgvc.ColumnIndex].ToString() &&
                                    dr.Table.Columns[dgvc.ColumnIndex].ColumnName.StartsWith("is_"))
                                {
                                    dgvc.Style.BackColor = Color.SteelBlue;
                                    dgvc.Style.ForeColor = Color.White;
                                }
                                else
                                {
                                    //dgvc.Style.BackColor = Color.Bisque;
                                    dgvc.Style.BackColor = Color.PowderBlue;
                                    dgvc.Style.ForeColor = Color.Empty;
                                }
                            }
                            // If the cell is in a required field make it violet (but not a bool field because it has a default value of 'N' already populated)...
                            else if (dr.Table.Columns[dgvc.ColumnIndex].ExtendedProperties.Contains("is_nullable") &&
                                dr.Table.Columns[dgvc.ColumnIndex].ExtendedProperties["is_nullable"].ToString() == "N" &&
                                !dr.Table.Columns[dgvc.ColumnIndex].ColumnName.StartsWith("is_"))
                            {
                                dgvc.Style.BackColor = Color.Plum;
                                dgvc.Style.ForeColor = Color.Empty;
                            }
                        }
                    }
                    // If the row has changes make each changed cell yellow...
                    if (dr.RowState == DataRowState.Modified)
                    {
                        foreach (DataGridViewCell dgvc in dgvr.Cells)
                        {
                            // If the cell has been changed make it yellow...
                            if (!dr[dgvc.ColumnIndex, DataRowVersion.Original].Equals(dr[dgvc.ColumnIndex, DataRowVersion.Current]))
                            {
                                dgvc.Style.BackColor = Color.Yellow;
                                dgvc.Style.ForeColor = Color.Empty;
                                dr.SetColumnError(dgvc.ColumnIndex, null);
                            }
                            // Use default background color for this cell...
                            else
                            {
                                dgvc.Style.BackColor = Color.Empty;
                            }
                        }
                    }
                }
                // If the user is doing a 'Find' make the results orange...
                if (!string.IsNullOrEmpty(bindingNavigatorFindText.Text))
                {
                    foreach (DataGridViewCell dgvc in dgvr.Cells)
                    {
                        // If the cell is part of the 'Find' collection make it orange...
                        if (_dgvcFindResults.Contains(dgvc))
                        {
                            dgvc.Style.BackColor = Color.Orange;
                            dgvc.Style.ForeColor = Color.Empty;
                            dr.SetColumnError(dgvc.ColumnIndex, null);
                        }
                    }
                }

            }
            else
            {
                if (dgvr.Cells != null && dgvr.DataGridView.SelectedCells != null && dgvr.DataGridView.SelectedCells.Count > 0 && dgvr.Cells.Contains(dgvr.DataGridView.SelectedCells[0]))
                {
                    foreach (DataGridViewCell dgvc in dgvr.Cells)
                    {
                        if (((DataTable)((BindingSource)dgvr.DataGridView.DataSource).DataSource).Columns[dgvc.ColumnIndex].ExtendedProperties.Contains("is_nullable") &&
                            ((DataTable)((BindingSource)dgvr.DataGridView.DataSource).DataSource).Columns[dgvc.ColumnIndex].ExtendedProperties["is_nullable"].ToString() == "N" &&
                            !((DataTable)((BindingSource)dgvr.DataGridView.DataSource).DataSource).Columns[dgvc.ColumnIndex].ColumnName.StartsWith("is_"))
                        {
                            dgvc.Style.BackColor = Color.Plum;
                            dgvc.Style.ForeColor = Color.Empty;
                        }
                        else
                        {
                            dgvc.Style.BackColor = Color.SandyBrown;
                            dgvc.Style.ForeColor = Color.Empty;
                        }
                    }
                }
            }
        }

        private void RefreshForm()
        {
            // If there is a form still attached to the global variable dataviewForm - dispose of it properly...
            if (dataviewForm != null)
            {
                dataviewForm.Close();
                dataviewForm.Dispose();
                dataviewForm = null;
            }

            // Display Form if user wants one...
            if (ux_tabcontrolCTDataviews.SelectedTab != null &&
                ux_tabcontrolCTDataviews.SelectedTab.Tag != null &&
                ux_tabcontrolCTDataviews.SelectedTab.Tag.GetType() == typeof(DataviewProperties) &&
                !string.IsNullOrEmpty(((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).ViewerStyle))
            {
                DataviewProperties dvp = (DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag;
                System.Reflection.ConstructorInfo constInfo = null;
                // First attempt to find a compatible form for this dataview...
                if (!string.IsNullOrEmpty(dvp.StrongFormName) &&
                    localFormsAssemblies != null &&
                    localFormsAssemblies.Length > 0)
                {
                    foreach (FormsData fd in localFormsAssemblies)
                    {
                        if (!string.IsNullOrEmpty(fd.StrongFormName) &&
                            !string.IsNullOrEmpty(dvp.StrongFormName) &&
                            fd.StrongFormName.Trim().ToUpper() == dvp.StrongFormName.Trim().ToUpper()) constInfo = fd.ConstInfo;
                    }
                }
                // Now show the compatible form as the user wants it...
                switch (((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).ViewerStyle.Trim().ToUpper())
                {
                    case "SPREADSHEET":
                        break;
                    case "FORM":
                        if (constInfo != null &&
                            ux_bindingnavigatorMain.BindingSource != null)
                        {
                            dataviewForm = (Form)constInfo.Invoke(new object[] { ux_bindingnavigatorMain.BindingSource, !ux_buttonEditData.Enabled, _sharedUtils, !ux_buttonEditData.Enabled });
                            dataviewForm.Owner = this;
                            dataviewForm.TopLevel = false;
                            dataviewForm.FormBorderStyle = FormBorderStyle.None;
                            dataviewForm.Anchor = ux_datagridviewMain.Anchor;
                            Point upperLeftCorner = ux_datagridviewMain.Location;
                            upperLeftCorner.Offset(0, 1);
                            dataviewForm.Location = upperLeftCorner;
                            dataviewForm.Width = ux_datagridviewMain.Width;
                            dataviewForm.Height = ux_groupboxDataEditing.Location.Y - dataviewForm.Location.Y;
                            ux_splitcontainerViewer.Panel1.Controls.Add(dataviewForm);
                            dataviewForm.BringToFront();
                            dataviewForm.Show();
                            // Make sure the tab controls are 'on top' of this new form...
                            ux_tabcontrolDataviewOptions.BringToFront();
                        }
                        else
                        {
                            dvp.StrongFormName = "";
                            ux_tabcontrolCTDataviews.SelectedTab.Tag = dvp;
                        }
                        break;
                    case "BOTH":
                        if (constInfo != null &&
                            ux_bindingnavigatorMain.BindingSource != null)
                        {
                            dataviewForm = (Form)constInfo.Invoke(new object[] { ux_bindingnavigatorMain.BindingSource, !ux_buttonEditData.Enabled, _sharedUtils, !ux_buttonEditData.Enabled });
                            if (dvp.AlwaysOnTop.Trim().ToLower() == "true") dataviewForm.Owner = this;
                            dataviewForm.Show();
                        }
                        else
                        {
                            dvp.StrongFormName = "";
                            ux_tabcontrolCTDataviews.SelectedTab.Tag = dvp;
                        }
                        break;
                    default:
                        break;
                }

                // Update the user interface with language translations...
                if (dataviewForm != null)
                {
                    _sharedUtils.UpdateControls(dataviewForm.Controls, this.Name);
                }
            }
        }

        private void GrinGlobalClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            // The user might be closing the form during the middle of edit changes in the datagridview - if so ask the
            // user if they would like to save their data (via the normal Cancel Edit button event)...
            if (ux_datagridviewMain.EditMode != DataGridViewEditMode.EditProgrammatically) ux_buttonCancelEditData.PerformClick();

            // Now check to see if the user has chosen to remain in edit mode (because they clicked the 'No' button on the 
            // Cancel Edit dialog box - if so cancel the FormClose event so that the user can continue editing data...
            if (ux_datagridviewMain.EditMode != DataGridViewEditMode.EditProgrammatically) e.Cancel = true;

            // Update all TabControl settings on the userSettings dataset if a valid cno was obtained during login...
            if (_usernameCooperatorID.Length > 0)
            {
                // Change cursor to the wait cursor...
                Cursor origCursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;

                if (_currentCooperatorID == _usernameCooperatorID)
                {
                    // Save all user gui settings...
                    SetAllUserSettings();
                    _sharedUtils.SaveAllUserSettings();
                    // Save user treeview lists...
                    SaveNavigatorTabControlData(_ux_NavigatorTabControl, int.Parse(_usernameCooperatorID));
                    // Save all LUT dictionary caches...
                    _sharedUtils.LookupTablesSaveALLCaches();
                }

                // Restore cursor to default cursor...
                Cursor.Current = origCursor;
            }
        }

        private void SetAllUserSettings()
        {
            if (_currentCooperatorID == _usernameCooperatorID)
            {
                if (ux_buttonEditData.Enabled)
                {
                    SetGeneralUserSettings();
                    SetDGVMainDataviewUserSettings();
                    SetGroupListNavigatorUserSettings();
                    SetTabControlDataviewUserSettings();
                }
                else
                {
                    SetDGVMainDataviewUserSettings();
                }
            }
        }

        private void SetGeneralUserSettings()
        {
            if (_currentCooperatorID == _usernameCooperatorID)
            {
                _sharedUtils.SaveUserSetting("", ux_checkboxRememberRowFilters.Name, "Checked", ux_checkboxRememberRowFilters.Checked.ToString());
                _sharedUtils.SaveUserSetting("", ux_checkboxPromptToClearRowFilter.Name, "Checked", ux_checkboxPromptToClearRowFilter.Checked.ToString());
                _sharedUtils.SaveUserSetting("", ux_checkboxWarnWhenLUTablesAreOutdated.Name, "Checked", ux_checkboxWarnWhenLUTablesAreOutdated.Checked.ToString());
                _sharedUtils.SaveUserSetting("", ux_numericupdownMaxRowsReturned.Name, "Value", ux_numericupdownMaxRowsReturned.Value.ToString());
                _sharedUtils.SaveUserSetting("", ux_numericupdownQueryPageSize.Name, "Value", ux_numericupdownQueryPageSize.Value.ToString());
                _sharedUtils.SaveUserSetting("", ux_checkboxStripSpecialCharactersOnExport.Name, "Checked", ux_checkboxStripSpecialCharactersOnExport.Checked.ToString());
                _sharedUtils.SaveUserSetting("", ux_checkboxStripSpecialCharactersOnImport.Name, "Checked", ux_checkboxStripSpecialCharactersOnImport.Checked.ToString());
            }
        }

        private void SetDGVMainDataviewUserSettings()
        {
            string columnsWidth = "";
            string columnsVisible = "";
            string columnsDisplayOrder = "";

            if (_currentCooperatorID == _usernameCooperatorID)
            {
                _sharedUtils.SaveUserSetting("", ux_checkboxHighlightChanges.Name, "Checked", ux_checkboxHighlightChanges.Checked.ToString());
                _sharedUtils.SaveUserSetting("", ux_checkboxHideNonErrorRows.Name, "Checked", ux_checkboxHideNonErrorRows.Checked.ToString());
                _sharedUtils.SaveUserSetting("", ux_checkboxHideUnchangedRows.Name, "Checked", ux_checkboxHideUnchangedRows.Checked.ToString());
                _sharedUtils.SaveUserSetting("", ux_checkboxHideCopyPasteWarnings.Name, "Checked", ux_checkboxHideCopyPasteWarnings.Checked.ToString());

                // If the selected tab's tag has no dataview string - bail out now...
                if (ux_tabcontrolCTDataviews.SelectedTab == null ||
                    ux_tabcontrolCTDataviews.SelectedTab.Tag == null ||
                    ux_tabcontrolCTDataviews.SelectedTab.Tag.GetType() != typeof(DataviewProperties) ||
                    string.IsNullOrEmpty(((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName)) return;

                foreach (DataGridViewColumn dgvc in ux_datagridviewMain.Columns)
                {
                    if (dgvc.Name != "TempSortColumn")
                    {
                        columnsWidth += dgvc.Width + " ";
                        if (dgvc.Visible) columnsVisible += dgvc.Index + " ";
                        columnsDisplayOrder += dgvc.DisplayIndex + " ";
                    }
                }

                // Update the TabControl settings to the userSettings dataset...
                // NOTE: this could be saved to SelectedTab.Text instead of SelectedTab.Tag
                //       so that the settings follow the tab name instead of the dataview name,
                //       but this creates problems elsewhere so we won't do it unless forced to
                string dataviewName = ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName;
                _sharedUtils.SaveUserSetting("", dataviewName, "Columns.Width", columnsWidth.Trim());
                _sharedUtils.SaveUserSetting("", dataviewName, "Columns.Visible", columnsVisible.Trim());
                _sharedUtils.SaveUserSetting("", dataviewName, "Columns.DisplayOrder", columnsDisplayOrder.Trim());
                _sharedUtils.SaveUserSetting("", dataviewName, "DefaultCellStyle.BackColor", ux_datagridviewMain.DefaultCellStyle.BackColor.ToArgb().ToString());
                _sharedUtils.SaveUserSetting("", dataviewName, "AlternatingRowsDefaultCellStyle.BackColor", ux_datagridviewMain.AlternatingRowsDefaultCellStyle.BackColor.ToArgb().ToString());
                if (ux_datagridviewMain.DataSource != null)
                {
                    string dgvSort = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.Sort;
                    if (!dgvSort.Contains("TempSortColumn"))
                    {
                        _sharedUtils.SaveUserSetting("", dataviewName, "DefaultView.Sort", dgvSort);
                    }
                    // Remember the rowfilter setting for this DGV...
                    if (ux_checkboxRememberRowFilters.Checked)
                    {
                        string dgvRowFilter = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.RowFilter;
                        _sharedUtils.SaveUserSetting("", dataviewName, "DefaultView.RowFilter", dgvRowFilter);
                    }
                    else
                    {
                        _sharedUtils.SaveUserSetting("", dataviewName, "DefaultView.RowFilter", "");
                    }
                }
            }
        }

        private void SetGroupListNavigatorUserSettings()
        {
            string tabOrder = "";
            TreeView tv;
            if (_currentCooperatorID == _usernameCooperatorID)
            {
                // If the navigator tab control has no data - bail out now...
                if (_ux_NavigatorTabControl == null ||
                _ux_NavigatorTabControl.TabPages == null ||
                _ux_NavigatorTabControl.TabPages.Count < 1) return;

                _sharedUtils.SaveUserSetting("", _ux_NavigatorTabControl.Name, "TabPages.Count", _ux_NavigatorTabControl.TabPages.Count.ToString());
                for (int i = 0; i < _ux_NavigatorTabControl.TabPages.Count; i++)
                {
                    if (_ux_NavigatorTabControl.TabPages[i].Name != "ux_tabpageGroupListNavigatorNewTab")
                    {
                        tabOrder += _ux_NavigatorTabControl.TabPages[i].Text + _pathSeparator;
                        Control[] tabPages = _ux_NavigatorTabControl.Controls.Find(_ux_NavigatorTabControl.TabPages[i].Name, true);
                        foreach (Control tabPage in tabPages)
                        {
                            if (tabPage.GetType() == typeof(TabPage))
                            {
                                foreach (Control ctrl in tabPage.Controls)
                                {
                                    if (ctrl.GetType() == typeof(TreeView))
                                    {
                                        tv = (TreeView)ctrl;
                                        if (_ux_NavigatorTabControl.SelectedIndex == i) _sharedUtils.SaveUserSetting("", _ux_NavigatorTabControl.Name, "SelectedIndex", _ux_NavigatorTabControl.SelectedIndex.ToString());
                                        if (tv != null && tv.SelectedNode != null) _sharedUtils.SaveUserSetting("", tv.Name, "SelectedNode.FullPath", tv.SelectedNode.FullPath);
                                    }
                                }
                            }
                        }
                    }
                }

                _sharedUtils.SaveUserSetting("", _ux_NavigatorTabControl.Name, "TabPages.Order", tabOrder.TrimEnd(_pathSeparator.ToCharArray()));
            }
        }

        private void SetTabControlDataviewUserSettings()
        {
            // If the selected tab's tag has no dataview string - bail out now...
            if (ux_tabcontrolCTDataviews.SelectedTab == null ||
                ux_tabcontrolCTDataviews.SelectedTab.Tag == null ||
                ux_tabcontrolCTDataviews.SelectedTab.Tag.GetType() != typeof(DataviewProperties) ||
                string.IsNullOrEmpty(((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName)) return;
            if (_currentCooperatorID == _usernameCooperatorID)
            {

                int tabPageCount = ux_tabcontrolCTDataviews.TabPages.Count;
                if (ux_tabcontrolCTDataviews.TabPages.ContainsKey("ux_tabpageDataviewNewTab")) tabPageCount -= 1;
                // Check to see if the number of dataview tabs is less than what the user started with and if so delete the extra tabs...
                int oldTabPageCount = -1;
                int.TryParse(_sharedUtils.GetUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages.Count", "-1"), out oldTabPageCount);
                if (oldTabPageCount > tabPageCount)
                {
                    for (int i = tabPageCount; i < oldTabPageCount; i++)
                    {
                        _sharedUtils.DeleteUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].Text"); // Legacy...
                        _sharedUtils.DeleteUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].Tag"); // Legacy...
                        _sharedUtils.DeleteUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].TabName");
                        _sharedUtils.DeleteUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].DataviewName");
                        _sharedUtils.DeleteUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].FormName");
                        _sharedUtils.DeleteUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].ViewerStyle");
                        _sharedUtils.DeleteUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].AlwaysOnTop");
                    }
                }

                _sharedUtils.SaveUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages.Count", tabPageCount.ToString());
                for (int i = 0; i < tabPageCount; i++)
                {
                    if (ux_tabcontrolCTDataviews.TabPages[i].Tag != null &&
                        ux_tabcontrolCTDataviews.TabPages[i].Tag.GetType() == typeof(DataviewProperties) &&
                        !string.IsNullOrEmpty(((DataviewProperties)ux_tabcontrolCTDataviews.TabPages[i].Tag).DataviewName))
                    {
                        _sharedUtils.SaveUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].TabName", ((DataviewProperties)ux_tabcontrolCTDataviews.TabPages[i].Tag).TabName + "");
                        _sharedUtils.SaveUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].DataviewName", ((DataviewProperties)ux_tabcontrolCTDataviews.TabPages[i].Tag).DataviewName + "");
                        _sharedUtils.SaveUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].FormName", ((DataviewProperties)ux_tabcontrolCTDataviews.TabPages[i].Tag).StrongFormName + "");
                        _sharedUtils.SaveUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].ViewerStyle", ((DataviewProperties)ux_tabcontrolCTDataviews.TabPages[i].Tag).ViewerStyle + "");
                        _sharedUtils.SaveUserSetting("", ux_tabcontrolCTDataviews.Name, "TabPages[" + i.ToString() + "].AlwaysOnTop", ((DataviewProperties)ux_tabcontrolCTDataviews.TabPages[i].Tag).AlwaysOnTop + "");
                    }
                }
            }
        }

        public DataSet SaveDGVData(DataSet dataToSave)
        {
            DataSet rollupReturn = new DataSet();
            string centerMessage = ux_statusCenterMessage.Text;
            string rightMessage = ux_statusRightMessage.Text;
            ContentAlignment centerMessageAlignment = ux_statusCenterMessage.TextAlign;

            // Set the statusbar up to show the query progress...
            ux_statusCenterMessage.TextAlign = ContentAlignment.MiddleRight;
            ux_statusCenterMessage.Text = "Saving Data:";
            ux_statusRightProgressBar.Visible = true;
            ux_statusRightProgressBar.Minimum = 0;
            ux_statusRightProgressBar.Maximum = 100;
            ux_statusRightProgressBar.Step = 1;
            ux_statusRightProgressBar.Value = 0;
            ux_statusstripMain.Refresh();

            try
            {
                // Pass the web service method 1000 records at a time (to keep the call smaller than 10MB)...
                DataTable modifiedTable = dataToSave.Tables[0];
                string errorMessage = "";
                DataSet pagedModifiedDataset = new DataSet();
                pagedModifiedDataset.Tables.Add(modifiedTable.Clone());
                int pageSize = (int)ux_numericupdownQueryPageSize.Value;
                pageSize = Math.Min(10000, Math.Max(1, pageSize)); // Clamp the page size between 1 and 10,000
                int pageStart = 0;
                int pageStop = Math.Min(pageSize, modifiedTable.Rows.Count);
                while (pageStart < modifiedTable.Rows.Count)
                {
                    // Clear the table page from the dataset...
                    pagedModifiedDataset.Tables[modifiedTable.TableName].Clear();
                    pagedModifiedDataset.AcceptChanges();

                    // Build a new 'modified data' table page row by row...
                    for (int i = pageStart; i < pageStop; i++)
                    {
                        // Make a copy of the datarow in the paged datatable...
                        DataRow newRow = pagedModifiedDataset.Tables[modifiedTable.TableName].NewRow();
                        switch (modifiedTable.Rows[i].RowState)
                        {
                            case DataRowState.Added:
                                // Populate the new row...
                                newRow.ItemArray = modifiedTable.Rows[i].ItemArray;
                                // Add it to the modified rows table...
                                pagedModifiedDataset.Tables[modifiedTable.TableName].Rows.Add(newRow);
                                break;
                            case DataRowState.Deleted:
                                // 'Undelete the original row (to allow access to the row's field data)...
                                modifiedTable.Rows[i].RejectChanges();
                                // Populate the new row...
                                newRow.ItemArray = modifiedTable.Rows[i].ItemArray;
                                // Add it to the modified rows table...
                                pagedModifiedDataset.Tables[modifiedTable.TableName].Rows.Add(newRow);
                                // Reset the rowstate for the new row...
                                newRow.AcceptChanges();
                                // Set the rowstate to the same as the original source rowstate...
                                newRow.Delete();
                                // 'Re-delete' the original row...
                                modifiedTable.Rows[i].Delete();
                                break;
                            case DataRowState.Detached:
                                break;
                            case DataRowState.Modified:
                                // Populate the new row with the original data from the modified row...
                                foreach (DataColumn dc in modifiedTable.Columns)
                                {
                                    newRow[dc.ColumnName] = modifiedTable.Rows[i][dc.ColumnName, DataRowVersion.Original];
                                }
                                // Add it to the modified rows table...
                                pagedModifiedDataset.Tables[modifiedTable.TableName].Rows.Add(newRow);
                                // Reset the rowstate for the new row...
                                newRow.AcceptChanges();
                                // Now modify the rows column to match the current data in the modified row...
                                foreach (DataColumn dc in modifiedTable.Columns)
                                {
                                    if (!modifiedTable.Rows[i][dc.ColumnName, DataRowVersion.Original].Equals(modifiedTable.Rows[i][dc.ColumnName, DataRowVersion.Current]))
                                    {
                                        newRow[dc.ColumnName] = modifiedTable.Rows[i][dc.ColumnName, DataRowVersion.Current];
                                    }
                                }
                                break;
                            case DataRowState.Unchanged:
                                break;
                            default:
                                break;
                        }
                    }

                    // Call the web method to update the changed/new data...
                    DataSet pagedModifiedDatasetResults = _sharedUtils.SaveWebServiceData(pagedModifiedDataset);

                    // Merge the returned rows into the dataset that will be returned...
                    if (pagedModifiedDatasetResults.Tables.Contains(modifiedTable.TableName) &&
                        pagedModifiedDatasetResults.Tables[modifiedTable.TableName].Rows.Count > 0)
                    {
                        // If the 'rollup' dataset does not have a return table - create it now...
                        if (!rollupReturn.Tables.Contains(modifiedTable.TableName))
                        {
                            rollupReturn.Tables.Add(pagedModifiedDatasetResults.Tables[modifiedTable.TableName].Clone());
                        }
                        // Load the returned rows to the 'rollup' dataset's table...
                        rollupReturn.Tables[modifiedTable.TableName].Load(pagedModifiedDatasetResults.Tables[modifiedTable.TableName].CreateDataReader(), LoadOption.Upsert);
                    }

                    // Roll up the dataset error messages for each page into one message...
                    if (pagedModifiedDatasetResults != null &&
                    pagedModifiedDatasetResults.Tables.Contains("ExceptionTable") &&
                    pagedModifiedDatasetResults.Tables["ExceptionTable"].Rows.Count > 0)
                    {
                        // If the 'rollup' dataset does not have an exceptions table - create it now...
                        if (!rollupReturn.Tables.Contains("ExceptionsTable") &&
                            pagedModifiedDatasetResults.Tables.Contains("ExceptionsTable"))
                        {
                            rollupReturn.Tables.Add(pagedModifiedDatasetResults.Tables["ExceptionsTable"].Clone());
                        }
                        // Append error message to the rollup error message...
                        errorMessage += "\n" + pagedModifiedDatasetResults.Tables["ExceptionTable"].Rows[0]["Message"].ToString();
                    }

                    // Update the paging indexes...
                    pageStart = pageStop;
                    pageStop = Math.Min((pageStart + pageSize), modifiedTable.Rows.Count);

                    // Update the progress bar on the statusbar...
                    ux_statusRightProgressBar.Value = pageStart * 100 / modifiedTable.Rows.Count;
                    ux_statusstripMain.Refresh();

                    // Check to see if we should bail out (at user request)...
                    Application.DoEvents();
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There were errors saving data.\n\nFull error message:\n{0}", "Save Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "SaveDGVDataMessage1";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    argsArray[0] = errorMessage;
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    ggMessageBox.ShowDialog();
                }
            }
            catch
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an unexpected error saving data.", "Save Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "SaveDGVDataMessage2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();
            }

            // If the commandline during application startup had a parameter for _saveDataDumpFile set to a valid filepath 
            // save the data to this file in XML format...
            if (!string.IsNullOrEmpty(_saveDGVDataDumpFile))
            {
                try
                {
                    dataToSave.WriteXml(_saveDGVDataDumpFile, XmlWriteMode.WriteSchema);
                }
                catch (Exception err)
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("Error attempting to save XML dataset to: {0}\n\nError Message:\n{1}", "Save Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "SaveDGVDataMessage3";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    argsArray[0] = _saveDGVDataDumpFile;
                    argsArray[1] = err.Message;
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);

                    ggMessageBox.ShowDialog();
                }
            }

            // Restore the statusbar back to it's original state...
            ux_statusCenterMessage.TextAlign = centerMessageAlignment;
            ux_statusCenterMessage.Text = centerMessage;
            ux_statusRightMessage.Text = rightMessage;
            ux_statusRightProgressBar.Visible = false;
            ux_statusstripMain.Refresh();

            return rollupReturn;
        }

        public DataSet GetDGVData(string dataviewName, List<KeyValuePair<string, int>> itemList, string parameterList, int pageSize)
        {
            DataSet results = new DataSet();

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Reset the escapeKeyPressed flag...
            escapeKeyPressed = false;

            // Remember the original layout of the status bar (before changing it)...
            ContentAlignment centerMessageAlignment = ux_statusCenterMessage.TextAlign;
            string centerMessage = ux_statusCenterMessage.Text;
            string rightMessage = ux_statusRightMessage.Text;
            ux_statusstripMain.DefaultStateSave();

            // Update the controls on the interface (to disable the user from 
            // choosing a new view tab or tree node during the middle of a query)...
            ux_splitcontainerMain.Panel1.Enabled = false;
            ux_splitcontainerMain.Panel2.Enabled = false;
            try
            {
                if (itemList.Count > 0)
                {
                    int nextPage = pageSize;

                    // Set the statusbar up to show the query progress...
                    ux_statusCenterMessage.TextAlign = ContentAlignment.MiddleRight;
                    ux_statusCenterMessage.Text = "Retrieving Data:";
                    ux_statusRightMessage.Text = "   Press the escape key (esc) to cancel query   ";
                    ux_statusRightProgressBar.Visible = true;
                    ux_statusRightProgressBar.Minimum = 0;
                    ux_statusRightProgressBar.Maximum = 100; // groupList.Length;
                    ux_statusRightProgressBar.Step = 1; // pageSize;
                    ux_statusRightProgressBar.Value = 0;
                    ux_statusstripMain.Refresh();

                    // Begin iterating through the collection of PKeys...
                    int pageStart = 0;
                    int pageStop = Math.Min(pageSize, itemList.Count);
                    while (pageStart < itemList.Count && !escapeKeyPressed)
                    {
                        string pkeyCollection = parameterList;
                        for (int i = pageStart; i < pageStop; i++)
                        {
                            if (pkeyCollection.Contains(itemList[i].Key.Trim().ToLower() + "="))
                            {
                                pkeyCollection = pkeyCollection.Replace(itemList[i].Key.Trim().ToLower() + "=", itemList[i].Key.Trim().ToLower() + "=" + itemList[i].Value.ToString() + ",");
                            }
                            else if (pkeyCollection.Contains(itemList[i].Key.Trim().ToLower() + ";"))
                            {
                                pkeyCollection = pkeyCollection.Replace(itemList[i].Key.Trim().ToLower() + ";", itemList[i].Key.Trim().ToLower() + "=" + itemList[i].Value.ToString() + ";");
                            }
                            else
                            {
                                pkeyCollection += itemList[i].Key.Trim().ToLower() + "=" + itemList[i].Value.ToString();
                            }
                        }
                        // Update the paging indexes...
                        pageStart = pageStop;
                        pageStop = Math.Min((pageStart + pageSize), itemList.Count);
                        // Build the param string and get the data...
                        DataSet pagedDataSet = _sharedUtils.GetWebServiceData(dataviewName, pkeyCollection, 0, Convert.ToInt32(ux_numericupdownMaxRowsReturned.Value));

                        // Add the results to the dataset that will be returned...
                        if (pagedDataSet.Tables.Contains(dataviewName) &&
                            results.Tables.Contains(dataviewName))
                        {
                            results.Tables[dataviewName].Merge(pagedDataSet.Tables[dataviewName].Copy());
                        }
                        else if (pagedDataSet.Tables.Contains(dataviewName))
                        {
                            results.Tables.Add(pagedDataSet.Tables[dataviewName].Copy());
                        }
                        else
                        {
                            if (pagedDataSet.Tables.Contains("ExceptionTable") &&
                                pagedDataSet.Tables["ExceptionTable"].Rows.Count > 0)
                            {
                                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an unexpected error retrieving data for {0}.\n\nFull error message:\n{1}", "Get Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                                ggMessageBox.Name = "GetDGVDataMessage1";
                                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                                string[] argsArray = new string[100];
                                argsArray[0] = dataviewName;
                                argsArray[1] = pagedDataSet.Tables["ExceptionTable"].Rows[0]["Message"].ToString();
                                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);

                                ggMessageBox.ShowDialog();
                            }
                        }

                        // Check to see if we have hit the maximum rows returned limit...
                        if (results.Tables[dataviewName].Rows.Count > ux_numericupdownMaxRowsReturned.Value)
                        {
                            // We have hit the max rows limit so set the page start value so that the loop exits normally...
                            pageStart = itemList.Count;
                            for (int i = (int)ux_numericupdownMaxRowsReturned.Value; i < results.Tables[dataviewName].Rows.Count; i++)
                            {
                                results.Tables[dataviewName].Rows[i].Delete();
                            }
                            results.Tables[dataviewName].AcceptChanges();
                        }

                        // Update the progress bar on the statusbar...
                        ux_statusRightProgressBar.Value = pageStart * 100 / itemList.Count; // pageStart;
                        ux_statusstripMain.Refresh();

                        // Check to see if we should bail out (at user request)...
                        Application.DoEvents();
                        Cursor.Current = Cursors.WaitCursor;
                    }
                }
                else
                {
                    // If no primary keys were found return an empty dataset...
                    results = _sharedUtils.GetWebServiceData(dataviewName, "", 0, Convert.ToInt32(ux_numericupdownMaxRowsReturned.Value));

                    if (!results.Tables.Contains(dataviewName) &&
                        results.Tables.Contains("ExceptionTable") &&
                        results.Tables["ExceptionTable"].Rows.Count > 0)
                    {
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an unexpected error retrieving data for {0}.\n\nFull error message:\n{1}", "Get Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                        ggMessageBox.Name = "GetDGVDataMessage2";
                        _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                        string[] argsArray = new string[100];
                        argsArray[0] = dataviewName;
                        argsArray[1] = results.Tables["ExceptionTable"].Rows[0]["Message"].ToString();
                        ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);

                        ggMessageBox.ShowDialog();
                    }
                }

            }
            catch (Exception err)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an unexpected error retrieving data for {0}.\n\nFull error message:\n{1}", "Get Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "GetDGVDataMessage3";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = dataviewName;
                argsArray[1] = err.Message;
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);

                ggMessageBox.ShowDialog();
                if (!results.Tables.Contains(dataviewName))
                {
                    results.Tables.Add(dataviewName);
                }
            }
            finally
            {
                // Restore the statusbar back to it's original state...
                ux_statusCenterMessage.TextAlign = centerMessageAlignment;
                ux_statusCenterMessage.Text = centerMessage;
                ux_statusRightMessage.Text = rightMessage;
                ux_statusRightProgressBar.Visible = false;
                ux_statusstripMain.Refresh();

                // Allow the user to 'move freely about the cabin' again...
                ux_splitcontainerMain.Panel1.Enabled = true;
                ux_splitcontainerMain.Panel2.Enabled = true;

                // Restore cursor to default cursor...
                Cursor.Current = origCursor;
            }

            return results;

        }

        private void LaunchAttachmentWizard(string[] fullPaths, string destinationPKey)
        {
            string attachmentWizardParams = destinationPKey;
            // If the destination node is an accession - convert it to the '**' inventory
            // record before calling the Attachment Wizard...
            if(attachmentWizardParams.Trim().ToLower().Contains(":accessionid"))
            {
                string[] PKeyTokens = attachmentWizardParams.Split('=');
                if (PKeyTokens.Length == 2 &&
                    !string.IsNullOrEmpty(PKeyTokens[1].Trim()))
                {
                    string seQueryString = "@inventory.accession_id IN (" + PKeyTokens[1].Trim() + ") AND @inventory.form_type_code='**'";
                    if (_sharedUtils != null)
                    {
                        // Use the Search Engine to find the inventory IDs...
                        DataSet ds = _sharedUtils.SearchWebService(seQueryString, true, true, null, "inventory", 0, 0);
                        // Build the inventory_id parameter string...
                        attachmentWizardParams = ":inventoryid=";
                        if (ds.Tables.Contains("SearchResult"))
                        {
                            // There should only be one '**' inventory but to be on the safe side 
                            // loop thru all rows in the SE results...
                            foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                            {
                                attachmentWizardParams += dr["ID"].ToString() + ",";
                            }
                            attachmentWizardParams = attachmentWizardParams.TrimEnd(',');
                        }
                    }
                }
            }

            // Show the Attachment Wizard for the PKey(s) found...
            if (!string.IsNullOrEmpty(destinationPKey) &&
                fullPaths != null &&
                fullPaths.Length > 0)
            {
                // Concatenate the file paths into a ',' delimited string...
                string concatFilePaths = ":filepaths=";
                foreach (string fullPath in fullPaths)
                {
                    if (System.IO.File.Exists(fullPath) ||
                        System.IO.Directory.Exists(fullPath) ||
                        Uri.IsWellFormedUriString(fullPath, UriKind.Absolute))
                    {
                        concatFilePaths += fullPath + ", ";
                    }
                }

                // Add the file paths to the PKEY string...
                attachmentWizardParams += "; " + concatFilePaths.TrimEnd(',', ' ');
                string attachmentWizardPath = "Wizards\\AttachmentWizard.dll";
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory());
                System.IO.FileInfo[] dllFiles = di.GetFiles(attachmentWizardPath, System.IO.SearchOption.AllDirectories);
                if (dllFiles != null && dllFiles.Length > 0)
                {
                    for (int i = 0; i < dllFiles.Length; i++)
                    {
                        System.Reflection.Assembly newAssembly = System.Reflection.Assembly.LoadFile(dllFiles[i].FullName);
                        foreach (System.Type t in newAssembly.GetTypes())
                        {
                            if (t.GetInterface("IGRINGlobalDataWizard", true) != null)
                            {
                                System.Reflection.ConstructorInfo constInfo = t.GetConstructor(new Type[] { typeof(string), typeof(SharedUtils) });
                                if (constInfo != null)
                                {
                                    // Instantiate an object of this type to load...
                                    Form wizardForm = (Form)constInfo.Invoke(new object[] { attachmentWizardParams, _sharedUtils });
                                    //wizardForm.Owner = this;
                                    wizardForm.FormClosing += new FormClosingEventHandler(wizard_FormClosing);
                                    wizardForm.Show();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ux_buttonSearch_Click(object sender, EventArgs e)
        {
            // Inspect the parameters to make sure they fit the pattern(s) allowed before invoking the process (to prevent injections)...
            if (SharedUtils.TargetStringFitsPattern(@"^\s*[\S]{1,50}\s*", username, false) &&
                SharedUtils.TargetStringFitsPattern(@"^\s*[\S]{1,255}\s*", passwordClearText, false) &&
                SharedUtils.TargetStringFitsPattern(@"^\s*[\S]{1,255}\s*", _sharedUtils.Url, false))
            {
                // Double quotes need to be escaped in the password before it is passed into the command line that launches Search.exe...
                string commandLineParameters = "-user=" + username + " -password=" + passwordClearText.Replace("\"", "\\\"") + " -url=" + _sharedUtils.Url;
                System.Diagnostics.Process newSearch = System.Diagnostics.Process.Start(@"GRINGlobal-SearchTool.exe", commandLineParameters);
            }
        }

        private void GrinGlobalClient_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                escapeKeyPressed = true;
            }
            else if (e.KeyCode == Keys.F5 && ux_buttonEditData.Enabled)
            {
                // Change cursor to the wait cursor...
                Cursor origCursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;

                // Resetting these two global variables will force a refresh of the DGV data...
                lastFullPath = "";
                lastTabName = "";
                // Save the user settings to local memory cache...
                SetAllUserSettings();
                // Check to see if this dataview uses Lookup Tables and update all of them now...
                RefreshDGVLookups();
                // Refresh the data view...
                RefreshMainDGVData();
                RefreshMainDGVFormatting();
                // Update all of the rest of the Lookup Tables in the background...
                LookupTableStatusCheck();

                // Restore cursor to default cursor...
                Cursor.Current = origCursor;
            }

            // Check to see if the user desired to select cells (Block Mode) instead of default full-row select (this is only done during read-only mode)...
            //if (e.Alt && ux_buttonEditData.Enabled)
            if (ux_buttonEditData.Enabled)
            {
                if (e.KeyData == _blockmodeKeyboardMapping)
                {
                    if (ux_datagridviewMain.SelectionMode != DataGridViewSelectionMode.CellSelect)
                    {
                        ux_datagridviewMain.SelectionMode = DataGridViewSelectionMode.CellSelect;
                        ux_datagridviewMain.GridColor = Color.Red;
                        ux_statusCenterMessage.Text = "Block Mode Selection Enabled (press any key to end)";
                    }
                    else
                    {
                        ux_datagridviewMain.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                        ux_datagridviewMain.GridColor = Color.FromName(System.Drawing.KnownColor.ControlDark.ToString());
                        RefreshMainDGVFormatting();
                    }
                }
                else if (!e.Control)
                {
                    // Ignore keystrokes in the Find textbox...
                    if (bindingNavigatorFindText.Focused) return;
                    ux_datagridviewMain.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                    ux_datagridviewMain.GridColor = Color.FromName(System.Drawing.KnownColor.ControlDark.ToString());
                    RefreshMainDGVFormatting();
                }
            }
        }

        private void backgroundworkerUpdateLookupTables_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (ux_statusRightProgressBar.Visible != true) ux_statusRightProgressBar.Visible = true;
            ux_statusRightProgressBar.Value = e.ProgressPercentage;
            ux_statusCenterMessage.Text = (string)e.UserState;
        }

        private void backgroundworkerUpdateLookupTables_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e != null &&
                e.Result != null &&
                (int)e.Result > 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There are {0} lookup tables with missing data.\nTo maximize performace of this application it is recommended that all lookup tables be downloaded.\n\nWould you like to do this now?", "Missing Lookup Table Data", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "LookupTableStatusCheckMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = ((int)e.Result).ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);

                if (DialogResult.Yes == ggMessageBox.ShowDialog(this))
                {
                    LookupTableLoader ltl = new LookupTableLoader(localDBInstance, _sharedUtils);
                    ltl.StartPosition = FormStartPosition.CenterParent;
                    ltl.Show(this);
                    ltl.Focus();
                }
            }

            // Hide the statusbar progressbar (just in case a data refresh is not going to happen)...
            ux_statusRightProgressBar.Visible = false;
            ux_statusstripMain.Refresh();


            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Refresh the DGV, but first check to make sure the user is not in edit mode...
            if (ux_buttonEditData.Enabled == true)
            {
                RefreshMainDGVData();
                RefreshMainDGVFormatting();
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void backgroundworkerUpdateLookupTables_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int partiallyLoadedTables = 0;
                int i = 0;
                int percentComplete = 0;
                DataTable lookupTableStatus = _sharedUtils.LookupTablesGetSynchronizationStats();
                if (lookupTableStatus != null &&
                    lookupTableStatus.Rows.Count > 0)
                {
                    foreach (DataRow dr in lookupTableStatus.Rows)
                    {
                        if (dr["status"].ToString().Trim().ToUpper() != "COMPLETED" &&
                            dr["status"].ToString().Trim().ToUpper() != "UPDATED")
                        {
                            // The program will not auto-update a partially loaded LUT because there could potentially 
                            // be 10s of thousands of missing records - so increment the partially loaded table count to warn the user later...
                            if (ux_checkboxWarnWhenLUTablesAreOutdated.Checked) partiallyLoadedTables++;
                        }
                        else
                        {
                            // The status of the lookup table is normal so update the table now...
                            percentComplete = 100 * i / lookupTableStatus.Rows.Count;
                            backgroundworkerUpdateLookupTables.ReportProgress(percentComplete, "Updating " + dr["dataview_name"].ToString() + " Lookup Table...");
                            _sharedUtils.LookupTablesUpdateTable(dr["dataview_name"].ToString(), false);
                            i++;
                        }
                    }
                }
                e.Result = partiallyLoadedTables;
            }
            catch
            {
                //MessageBox.Show("An error was encountered while performing the status check for lookup tables.  Stopping status check.", "Lookup Table Status Check Error");
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("An error was encountered in the background thread performing the status check for lookup tables.  Stopping status check.\n\nThe reason the server stopped responding to the Curator Tool background thread is unknown.  Try closing and then restarting the Curator Tool.  If the error persists close the Curator Tool and restart your computer.", "Lookup Table Status Check Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "LookupTableStatusCheckMessage2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();
            }
        }

        private void ux_buttonLookupTableMaintenance_Click(object sender, EventArgs e)
        {
            LookupTableLoader ltl = new LookupTableLoader(localDBInstance, _sharedUtils);
            ltl.StartPosition = FormStartPosition.CenterParent;
            ltl.Show(this);
            //ltl.Activate();
            //ltl.Focus();
        }

        private void ux_comboboxActiveWebService_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_currentCooperatorID == _usernameCooperatorID)
            {
                // Save all user gui settings...
                SetAllUserSettings();
                _sharedUtils.SaveAllUserSettings();
                // Save user treeview lists...
                SaveNavigatorTabControlData(_ux_NavigatorTabControl, int.Parse(_usernameCooperatorID));
                // Save all LUT dictionary caches...
                _sharedUtils.LookupTablesSaveALLCaches();
            }

            // Now try to establish a new web service connection...
            try
            {
                SharedUtils tempSharedUtils = new SharedUtils(ux_comboboxActiveWebService.SelectedValue.ToString(), username, passwordClearText, true, "GRINGlobalClientCuratorTool");

                if (tempSharedUtils.IsConnected)
                {
                    // Set the new global SharedUtils global variable to the newly created tempSharedUtils...
                    _sharedUtils = tempSharedUtils;

                    // Get the user login details for this user...
                    username = _sharedUtils.Username;
                    password = _sharedUtils.Password;
                    passwordClearText = _sharedUtils.Password_ClearText;
                    _usernameCooperatorID = _sharedUtils.UserCooperatorID;
                    //_currentCooperatorID = _sharedUtils.UserCooperatorID;
                    _currentCooperatorID = "-1"; // Make this an imaginary coopID to prevent syncing across databases
                    site = _sharedUtils.UserSite;
                    siteID = _sharedUtils.UserSiteID;
                    languageCode = _sharedUtils.UserLanguageCode.ToString();

                    // Set the global variable for the localDBInstance...
                    localDBInstance = _sharedUtils.Url.ToLower().Replace("ldap://", "").Replace("http://", "").Replace("https://", "").Replace("/gringlobal/gui.asmx", "").Replace("/gringlobal_remote_debug/gui.asmx", "").Replace('-', '_').Replace('.', '_').Replace(':', '_');
                    localDBInstance = "GRINGlobal_" + localDBInstance;
                    // Reload/rebuild the interface...
                    LoadApplicationData();
                    // Check the status of lookup tables and warn the user if some tables are missing data...
                    LookupTableStatusCheck();
                    // Indicate the web service connection...
                    ux_statusCenterMessage.Text = "Connected to: " + _sharedUtils.Url;
                }
                else
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("Unable to connect to {0}\nAborting connection request.", "Connection Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ux_comboboxActiveWebServiceMessage1";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    argsArray[0] = ux_comboboxActiveWebService.Text;
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    ggMessageBox.ShowDialog();
                    ux_comboboxActiveWebService.SelectedValue = _sharedUtils.Url;
                }
            }
            catch
            {
            }
        }

        void defaultBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            //SetAllUserSettings();
            ////userSettings.Save();
            //RefreshMainDGVFormatting();
        }

        private void bindingNavigatorAddNewItem_MouseDown(object sender, MouseEventArgs e)
        {
            // Need to foce the datagridview to commit the last edited cell changes before creating a new record...
            ux_datagridviewMain.EndEdit();
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            RefreshDGVRowFormatting(ux_datagridviewMain.CurrentRow, ux_checkboxHighlightChanges.Checked);
        }

        DataRow drDeletedRow;
        object[] drDeletedRowItemArray;
        private void bindingNavigatorDeleteItem_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridViewRow dgvrDeletedRow;
            // If the red 'X' button on the bindingNavigator has been clicked the current row 
            // in the DataGridView will be automatically deleted - so remember which row is going 
            // to be deleted so that we can restore it if the user choses not to delete selected rows... 
            dgvrDeletedRow = ux_datagridviewMain.CurrentRow;
            if (dgvrDeletedRow != null)
            {
                drDeletedRow = (DataRow)((DataRowView)dgvrDeletedRow.DataBoundItem).Row;
                drDeletedRowItemArray = drDeletedRow.ItemArray;
            }
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            // By the time red 'X' button click event message gets to this handler the bindingNavigator.CurrentRow has 
            // already been deleted (but none of the other selected rows are deleted by the default event handler) - so
            // we need to ask if the user really wants to do this and if so delete all selected rows otherwise restore the 
            // record deleted by the event handler of the built-in button control...
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("WARNING!!!  You are about to permanently delete {0} records from the central database!\n\nAre you sure you want to do this?", "Record Delete Confirmation", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button2);
            ggMessageBox.Name = "UserInterfaceUtils_ProcessDGVEditShortcutKeysMessage1";
            if (_sharedUtils != null && _sharedUtils.IsConnected) _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = (ux_datagridviewMain.SelectedRows.Count + 1).ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (DialogResult.OK == ggMessageBox.ShowDialog())
            {
                // The user really does want to delete the records - so delete all remaining selected rows...
                foreach (DataGridViewRow dgvr in ux_datagridviewMain.SelectedRows)
                {
                    dgvr.DataGridView.Rows.Remove(dgvr);
                }
            }
            else
            {
                // The user does not want to delete the records but since the current row has already been delete we 
                // need to restore it now by rejecting the changes...
                if (drDeletedRow != null &&
                    drDeletedRow.RowState == DataRowState.Deleted)
                {
                    drDeletedRow.RejectChanges();
                }
                else if (drDeletedRow.RowState == DataRowState.Detached)
                {
                    drDeletedRow.Table.Rows.Add(drDeletedRowItemArray);
                }
            }

            // This will unselect any rows that might have been multi-selected in the DGV prior to pressing the 
            // red 'X' button on the Navigation Bar...
            ux_datagridviewMain.ClearSelection();
            RefreshMainDGVFormatting();
        }

        private void ux_buttonSaveUserSettingsNow_Click(object sender, EventArgs e)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            if (_currentCooperatorID == _usernameCooperatorID)
            {
                // Save all user gui settings...
                SetAllUserSettings();
                _sharedUtils.SaveAllUserSettings();
                // Save user treeview lists...
                SaveNavigatorTabControlData(_ux_NavigatorTabControl, int.Parse(_usernameCooperatorID));

                // Reload the treeview lists...
                // 1) get the treeview lists for the currently selected cooperator...
                DataTable userListItems = GetUserItemList((int)ux_comboboxCNO.SelectedValue);
                // 2) rebuild the Navigator Tab Control with the new user item list...
                _ux_NavigatorTabControl = BuildTabControl(userListItems);
                // With the Navigator TabControl built we can place it in the left pane...
                ux_panelNavigator.Controls.Clear();
                ux_panelNavigator.Controls.Add(_ux_NavigatorTabControl);
                _ux_NavigatorTabControl.Size = ux_panelNavigator.Size;
                _ux_NavigatorTabControl.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;

                // Save all LUT dictionary caches...
                _sharedUtils.LookupTablesSaveALLCaches();
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void ux_buttonWizard_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            string wizardDLLPath = "";

            if (sender.GetType() == typeof(ToolStripButton) &&
                ((ToolStripButton)sender).Tag != null) wizardDLLPath = ((ToolStripButton)sender).Tag.ToString();

            if (!string.IsNullOrEmpty(wizardDLLPath))
            {
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory());
                System.IO.FileInfo[] dllFiles = di.GetFiles(wizardDLLPath, System.IO.SearchOption.AllDirectories);
                if (dllFiles != null && dllFiles.Length > 0)
                {
                    for (int i = 0; i < dllFiles.Length; i++)
                    {
                        System.Reflection.Assembly newAssembly = System.Reflection.Assembly.LoadFile(dllFiles[i].FullName);
                        foreach (System.Type t in newAssembly.GetTypes())
                        {
                            if (t.GetInterface("IGRINGlobalDataWizard", true) != null)
                            {
                                System.Reflection.ConstructorInfo constInfo = t.GetConstructor(new Type[] { typeof(string), typeof(SharedUtils) });
                                if (constInfo != null)
                                {
                                    DataTable dt = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource);
                                    string currentDGVPKeys = "";
                                    if (dt.PrimaryKey != null &&
                                        dt.PrimaryKey.Length == 1 &&
                                        !string.IsNullOrEmpty(dt.PrimaryKey[0].ColumnName))
                                    {
                                        // Create the string of pkeys to be passed on to the wizard...
                                        foreach (DataGridViewRow dgvr in ux_datagridviewMain.SelectedRows)
                                        {
                                            currentDGVPKeys += dgvr.Cells[dt.PrimaryKey[0].ColumnName.Trim().ToLower()].Value.ToString() + ",";
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(currentDGVPKeys))
                                    {
                                        currentDGVPKeys = ":" + dt.PrimaryKey[0].ColumnName.Trim().ToLower().Replace("_", "") + "=" + currentDGVPKeys.TrimEnd(',');
                                    }
                                    // Instantiate an object of this type to load...
                                    Form wizardForm = (Form)constInfo.Invoke(new object[] { currentDGVPKeys, _sharedUtils });
                                    //wizardForm.Owner = this;
                                    wizardForm.FormClosing += new FormClosingEventHandler(wizard_FormClosing);
                                    wizardForm.Show();

                                }
                            }
                        }
                    }
                }
            }

            Cursor.Current = Cursors.Default;
        }

        void wizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            Form wizard = (Form)sender;
            if (e.Cancel)
            {
                wizard.Activate();
            }
            else
            {
                System.Reflection.PropertyInfo pi = wizard.GetType().GetProperty("ChangedRecords");
                if (pi != null &&
                    pi.PropertyType.Name.ToUpper() == "DATATABLE")
                {
                    DataTable dt = (DataTable)pi.GetValue(wizard, null);
                    SyncSavedRecordsWithTreeViewAndDGV(dt);

                    // The next four lines force the RefreshDGVData method to retrieve a fresh copy of the data...
                    lastFullPath = "";
                    lastTabName = "";
                    SetAllUserSettings();
                    // Refresh the data view...
                    RefreshMainDGVData();
                    RefreshMainDGVFormatting();
                }
            }
        }

        private int SyncSavedRecordsWithTreeViewAndDGV(DataTable savedRecords)
        {
            int errorCount = 0;
            TreeNode tempTreeNodeParent = new TreeNode();

            if (savedRecords != null && savedRecords.PrimaryKey.Length == 1)
            {
                // Make an empty copy of the user's item list...
                //DataTable newGETLISTSItems = userItemList.Clone();
                string pKeyCol = savedRecords.PrimaryKey[0].ColumnName.Trim().ToUpper();
                savedRecords.Columns[pKeyCol].ReadOnly = false;
                foreach (DataRow dr in savedRecords.Rows)
                {
                    DataRow originalRow = null;
                    if (ux_datagridviewMain != null &&
                        ux_datagridviewMain.DataSource != null &&
                        ux_datagridviewMain.DataSource.GetType() == typeof(BindingSource) &&
                        ((BindingSource)ux_datagridviewMain.DataSource).DataSource != null &&
                        ((BindingSource)ux_datagridviewMain.DataSource).DataSource.GetType() == typeof(DataTable) &&
                        ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).Columns.Count > 0)
                    {
                        originalRow = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).Rows.Find(dr["OriginalPrimaryKeyID"]);
                    }

                    switch (dr["SavedAction"].ToString())
                    {
                        case "Insert":
                            // "NewPrimaryKeyID"
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Create a new new TreeNode...
                                TreeNode newNode = new TreeNode();

                                string newPKeyValue = dr["NewPrimaryKeyID"].ToString();
                                string dataviewPKeyName = dr.Table.PrimaryKey[0].ColumnName.Trim();
                                string tablePKeyName = dr.Table.PrimaryKey[0].ExtendedProperties["table_field_name"].ToString().Trim();
                                string tableName = dr.Table.PrimaryKey[0].ExtendedProperties["table_name"].ToString().Trim();
                                string properties = dataviewPKeyName.Trim().ToUpper();
                                properties += ";:" + tablePKeyName.Replace("_", "") + "=" + newPKeyValue;
                                properties += ";@" + tableName + "." + tablePKeyName + "=" + newPKeyValue;
                                newNode.Name = properties.Split(';')[1].Trim();
                                newNode.Text = newNode.Name;
                                newNode.Tag = properties;
                                newNode.ImageKey = "inactive_" + tablePKeyName.ToUpper();
                                newNode.SelectedImageKey = "active_" + tablePKeyName.ToUpper();

                                newNode.ToolTipText = "";
                                // Now check to see if virtual nodes are wanted for this new node...
                                if (!string.IsNullOrEmpty(_sharedUtils.GetAppSettingValue(properties.Split(';')[0].Trim().ToUpper() + "_VIRTUAL_NODE_DATAVIEW")))
                                {
                                    TreeNode dummyNode = new TreeNode();
                                    string virtualQuery = _sharedUtils.GetAppSettingValue(properties.Split(';')[0].Trim().ToUpper() + "_VIRTUAL_NODE_DATAVIEW") + ", ";
                                    virtualQuery += properties.Split(';')[1].Trim().ToLower();

                                    dummyNode.Text = "!!DUMMYNODE!!";
                                    dummyNode.Name = "!!DUMMYNODE!!"; ;
                                    dummyNode.Tag = virtualQuery;
                                    //dummyNode.ImageKey = "inactive_" + virtualQuery;
                                    //dummyNode.SelectedImageKey = "active_" + virtualQuery;
                                    newNode.Nodes.Add(dummyNode);
                                }

                                // Add the node to the collection of nodes for new records...
                                tempTreeNodeParent.Nodes.Add(newNode);

                                // Set the datagridview row's status for this new row to committed (and update the pkey with the int returned from the server DB)...
                                if (originalRow != null)
                                {
                                    bool origColumnReadOnlyValue = originalRow.Table.Columns[pKeyCol].ReadOnly;
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = false;
                                    originalRow[pKeyCol] = dr["NewPrimaryKeyID"];
                                    originalRow.AcceptChanges();
                                    originalRow.Table.Columns[pKeyCol].ReadOnly = origColumnReadOnlyValue;
                                }
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Update":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                dr.ClearErrors();
                                // Set the row's status for this deleted row to committed...
                                if (originalRow != null)
                                {
                                    originalRow.AcceptChanges();
                                }
                            }
                            else
                            {
                                errorCount++;
                                if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                            break;
                        case "Delete":
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                // Find the treeview nodes with this pkey and pkey_type...
                                foreach (TabPage tp in _ux_NavigatorTabControl.TabPages)
                                {
                                    foreach (Control ctrl in tp.Controls)
                                    {
                                        if (ctrl.GetType() == typeof(TreeView))
                                        {
                                            TreeView tv = (TreeView)ctrl;
                                            TreeNode[] deletedNodes = tv.Nodes.Find(":" + pKeyCol.Trim().ToLower().Replace("_", "") + "=" + dr[pKeyCol].ToString(), true);
                                            foreach (TreeNode tn in deletedNodes)
                                            {
                                                // Delete the entries in the users item list records by adding
                                                // the app_user_item_list pkey(s) associated with this node to the collection of records
                                                // that should be deleted from the server when the CT exits...
                                                _deletedTreeNodes.AddRange(AddToDeletedTreeNodeList(tn));
                                                // Delete the node from the tree view...
                                                tn.Remove();
                                            }
                                        }
                                    }
                                }
                                // Set the row's status for this deleted row to committed...
                                if (originalRow != null)
                                {
                                    originalRow.AcceptChanges();
                                }
                            }
                            else
                            {
                                errorCount++;
                                // Find the deleted row (NOTE: datatable.rows.find() method does not work on deleted rows)...
                                foreach (DataRow deletedRow in ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).Rows)
                                {
                                    if (deletedRow[0, DataRowVersion.Original].Equals(dr["OriginalPrimaryKeyID"]))
                                    {
                                        deletedRow.RejectChanges();
                                        deletedRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            // Ask the user if they would like to add the newly saved items to their treeview's selected node...
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have successfully added {0} new records to the database.\nWould you like links to these new records added to your current list folder?", "Add new item links", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "SyncUserItemListMessage1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = tempTreeNodeParent.Nodes.Count.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);

            if (tempTreeNodeParent.Nodes.Count > 0 &&
                DialogResult.OK == ggMessageBox.ShowDialog())
            {
                // Add the new nodes to the active node for the selected tab's treeview...
                foreach (Control ctrl in _ux_NavigatorTabControl.SelectedTab.Controls)
                {
                    if (ctrl.GetType() == typeof(TreeView))
                    {
                        TreeView tv = (TreeView)ctrl;
                        TreeNode parentFolder = null;

                        // Find a suitable folder to contain the new nodes...
                        if (tv != null && isFolder(tv.SelectedNode))
                        {
                            parentFolder = tv.SelectedNode;
                        }
                        else if (tv != null &&
                                tv.SelectedNode.Parent != null &&
                                isFolder(tv.SelectedNode.Parent))
                        {
                            parentFolder = tv.SelectedNode.Parent;
                        }
                        else if (tv != null &&
                                tv.SelectedNode.Parent != null &&
                                tv.SelectedNode.Parent.Parent != null)
                        {
                            parentFolder = tv.SelectedNode.Parent.Parent;
                        }

                        // Hopefully we have found a suitable folder to contain the new nodes
                        // and if so add them to that folder now and then refresh the folder's formatting...
                        if (parentFolder != null)
                        {
                            // Add the nodes to the folder...
                            foreach (TreeNode tnNewNode in tempTreeNodeParent.Nodes)
                            {
                                parentFolder.Nodes.Add(tnNewNode);
                            }
                            // Refresh the folder formatting to give the new nodes their proper titles...
                            RefreshTreeviewNodeFormatting(parentFolder);
                        }
                    }
                }
            }
            return errorCount;
        }

        private void ux_menuitemFileExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButtonSearch_Click(object sender, EventArgs e)
        {
            ux_buttonSearch_Click(sender, e);
        }

        private void ux_contextmenustripDataview_Opening(object sender, CancelEventArgs e)
        {
            if (ux_tabcontrolCTDataviews.TabPages.Count < 3)
            {
                ux_dataviewmenuCloseTab.Enabled = false;
            }
            else
            {
                ux_dataviewmenuCloseTab.Enabled = true;
            }
        }

        private void ux_checkboxShowAllCooperators_CheckedChanged(object sender, EventArgs e)
        {
            DataTable cooperatorTable = (DataTable)ux_comboboxCNO.DataSource;
            DataRowView drvCurrentCooperator = (DataRowView)(ux_comboboxCNO.SelectedItem);
            if (ux_checkboxShowAllCooperators.Checked)
            {
                cooperatorTable.DefaultView.RowFilter = "";
                ux_comboboxCNO.SelectedItem = drvCurrentCooperator;
            }
            else
            {
                if (cooperatorTable.Columns.Contains("site")) cooperatorTable.DefaultView.RowFilter = "site = '" + site + "'";
                if (drvCurrentCooperator["site"].ToString() == site)
                {
                    ux_comboboxCNO.SelectedItem = drvCurrentCooperator; ;
                }
                else
                {
                    //ux_comboboxCNO.SelectedIndex = 0;
                    ux_comboboxCNO.SelectedValue = _usernameCooperatorID;
                }
            }
        }

        private void ux_checkboxIncludeSubFolders_CheckedChanged(object sender, EventArgs e)
        {
            // Reset the last processed treeview node (to force a full data refresh)...
            lastFullPath = "";
            lastTabName = "";
            // So that this call will be refreshed properly...
            SetAllUserSettings();
            // Refresh the data view...
            RefreshMainDGVData();
            RefreshMainDGVFormatting();
        }

        private void ux_checkboxHotSyncTreeview_CheckedChanged(object sender, EventArgs e)
        {
            if (ux_checkboxHotSyncTreeview.Checked)
            {
                DataGridViewCell dgvc = ux_datagridviewMain.CurrentCell;
                ux_datagridviewMain.CurrentCell = null;
                ux_datagridviewMain.CurrentCell = dgvc;
            }
            else
            {
                TabPage tp = _ux_NavigatorTabControl.SelectedTab;
                foreach (Control ctrl in tp.Controls)
                {
                    if (ctrl.GetType() == typeof(TreeView))
                    {
                        // Reset each node to the default font for the control...
                        ResetTreeviewNodeFormatting(((TreeView)ctrl).Nodes);
                    }
                }
            }
        }

        private void ux_menuitemChangePassword_Click(object sender, EventArgs e)
        {
            ChangePassword cp = new ChangePassword(_sharedUtils.Url, _sharedUtils.Username, "", _sharedUtils);
            cp.StartPosition = FormStartPosition.CenterParent;
            if (DialogResult.OK == cp.ShowDialog(this))
            {
                _sharedUtils = new SharedUtils(_sharedUtils.Url, _sharedUtils.Username, cp.Password, true, "GRINGlobalClientCuratorTool");
            }
        }

        void tsmiLanguage_Click(object sender, EventArgs e)
        {
            int newLangID = 1;
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            if (int.TryParse(tsmi.Tag.ToString(), out newLangID))
            {
                _sharedUtils.ChangeLanguage(newLangID);
                languageCode = newLangID.ToString();
                _sharedUtils.UserLanguageCode = newLangID;
                // Get language translations for the components and controls in this applicataion...
                if (this.components != null && this.components.Components != null) _sharedUtils.UpdateComponents(this.components.Components, this.Name);
                if (this.Controls != null) _sharedUtils.UpdateControls(this.Controls, this.Name);
                // Save the statusbar text for the left, center, and right controls...
                ux_statusLeftMessage.Tag = ux_statusLeftMessage.Text;
                ux_statusCenterMessage.Tag = ux_statusCenterMessage.Text;
                ux_statusRightMessage.Tag = ux_statusRightMessage.Text;
                ux_statusRightMessage.Text = "";
                // Refresh the DGV data to get new column headings...
                ux_buttonRefreshData.PerformClick();
                // Warn the user that they should reload their LU tables...
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have successfully changed your language to {0}.\n\nWarning: Your lookup tables are language specific so you should reload them very soon.\n\nWould you like to do this now?", "Reload Lookup Tables", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "tsmiLanguage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = tsmi.Text;
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);

                if (DialogResult.Yes == ggMessageBox.ShowDialog())
                {
                    // Reload LU tables now...
                    LookupTableLoader ltl = new LookupTableLoader(localDBInstance, _sharedUtils);
                    ltl.StartPosition = FormStartPosition.CenterParent;
                    ltl.Show();
                }
            }
        }

        private void ux_menuitemFile_DropDownOpening(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            DataSet ds = new DataSet();
            ds = _sharedUtils.GetWebServiceData("get_sys_language", "", 0, 0);
            // Clear the drop down list of languages...
            ux_menuitemLanguage.DropDownItems.Clear();
            // Rebuild the list of supported languages (English is the default required language)...
            if (ds.Tables.Contains("get_sys_language") &&
                ds.Tables["get_sys_language"].Rows.Count > 0)
            {
                if (ds.Tables["get_sys_language"].Columns.Contains("is_enabled")) ds.Tables["get_sys_language"].DefaultView.RowFilter = "is_enabled='Y'";
                if (ds.Tables["get_sys_language"].DefaultView.Count > 0 &&
                    ds.Tables["get_sys_language"].Columns.Contains("TITLE") &&
                    ds.Tables["get_sys_language"].Columns.Contains("sys_lang_id"))
                {
                    foreach (DataRowView drv in ds.Tables["get_sys_language"].DefaultView)
                    {
                        ToolStripMenuItem tsmi = new ToolStripMenuItem(drv["TITLE"].ToString(), null, tsmiLanguage_Click);
                        tsmi.Tag = drv["sys_lang_id"];
                        if (_sharedUtils.UserLanguageCode.ToString() == drv["sys_lang_id"].ToString()) tsmi.Checked = true;
                        ux_menuitemLanguage.DropDownItems.Add(tsmi);
                    }
                }
            }
            else
            {
                ToolStripMenuItem tsmi = new ToolStripMenuItem("English", null, tsmiLanguage_Click);
                tsmi.Tag = 1;
                ux_menuitemLanguage.DropDownItems.Add(tsmi);
            }

            Cursor.Current = Cursors.Default;
        }

        private void ux_menuitemReportsManage_Click(object sender, EventArgs e)
        {
            ReportManager reportManagerForm = new ReportManager(_sharedUtils);
            reportManagerForm.Show();
        }

        private void ux_menuitemHelpKeyboardMapping_Click(object sender, EventArgs e)
        {
            KeyboardMapper keyboardMapperForm = new KeyboardMapper(_sharedUtils);
            keyboardMapperForm.ShowDialog();

            // Now that the user has returned from editing the keyboard mappings we should update the setting for Block Mode keyboard mapping...
            string blockmodeKeyboardMappingValue = _sharedUtils.GetKeyboardMapping("Block Mode", "Oemtilde, Control");
            if (!string.IsNullOrEmpty(blockmodeKeyboardMappingValue))
            {
                KeysConverter kc = new KeysConverter();
                _blockmodeKeyboardMapping = (Keys)kc.ConvertFromString(blockmodeKeyboardMappingValue);
            }
            else
            {
                _blockmodeKeyboardMapping = Keys.Control | Keys.Oemtilde;
            }
        }

        private void ux_menuitemHelpAbout_Click(object sender, EventArgs e)
        {
            GGMessageBox ggMessageBox = new GGMessageBox("GRIN-Global Curator Tool v{0}\n\n* * * * *\n\nThis software was created by USDA/ARS, with Bioversity International coordinating testing and feedback from the international genebank community.  Development was supported financially by USDA/ARS and by a major grant from the Global Crop Diversity Trust.  This statement by USDA does not imply approval of these enterprises to the exclusion of others which might also be suitable.\n\nUSDA grants to each Recipient of this software non-exclusive, royalty free, world-wide, permission to use, copy, modify, publish, distribute, perform publicly and display publicly this software.  Notice of this permission as well as the other paragraphs in this notice shall be included in all copies or modifications of this software.\n\nThis software application has not been tested or otherwise examined for suitability for implementation on, or compatibility with, any other computer systems.  USDA does not warrant, either explicitly or implicitly, that this software program will not cause damage to the user’s computer or computer operating system, nor does USDA warrant, either explicitly or implicitly, the effectiveness of the software application.\n\nThe English text above shall take precedence in the event of any inconsistencies between the English text and any translation of this notice.\n\n* * * * *", "GRIN-Global Curator Tool v{0}", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "ux_menuitemHelpAboutMessageBox1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
            ggMessageBox.Caption = string.Format(ggMessageBox.Caption, argsArray);
            argsArray = new string[100];
            argsArray[0] = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            DialogResult returnJunk = ggMessageBox.ShowDialog();
        }

        private void ux_menuitemToolsMakeDBAccessible_Click(object sender, EventArgs e)
//        private void ux_menuitemHelpMakeDBAccessible_Click(object sender, EventArgs e)
        {
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("This action will add all users on this computer to the local SQL Server database.\n\nDo you wish to continue with this action?", "Make Local Database Accessible", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
            ggMessageBox.Name = "ux_menuitemHelpMakeDBAccessibleMessageBox1";
            _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            argsArray[0] = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
            ggMessageBox.Caption = string.Format(ggMessageBox.Caption, argsArray);
            argsArray = new string[100];
            argsArray[0] = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (DialogResult.Yes == ggMessageBox.ShowDialog())
            {
                // Execute SQL to add the BUILTIN\Users account to the sysadmins role on the local database...
                // The SQL looks something like this: EXEC master..sp_addsrvrolemember @loginame = N'BUILTIN\Users', @rolename = N'sysadmin'
                if (_sharedUtils.LocalDatabaseMakeAccessibleToAllUsers())
                {
                    ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("Local SQL Server database is now accessible to all users of this computer.", "Make Local Database Accessible Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ux_menuitemHelpMakeDBAccessibleMessageBox2";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    argsArray = new string[100];
                    argsArray[0] = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
                    ggMessageBox.Caption = string.Format(ggMessageBox.Caption, argsArray);
                    ggMessageBox.ShowDialog();
                }
                else
                {
                    ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("Your attempt to make the local SQL Server database accessible to all users failed.\n\nNOTE: You must be an administrator on the local SQL Server database to perform this action.\n\nRefer to installation manual to perform this task manually if the problem persists.", "Make Local Database Accessible Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ux_menuitemHelpMakeDBAccessibleMessageBox3";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    argsArray = new string[100];
                    argsArray[0] = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
                    ggMessageBox.Caption = string.Format(ggMessageBox.Caption, argsArray);
                    ggMessageBox.ShowDialog();
                }
            }
        }

        private void ux_buttonRefreshData_Click(object sender, EventArgs e)
        {
            if (ux_buttonEditData.Enabled) // This is true when the CT is not in edit mode
            {
                // Change cursor to the wait cursor...
                Cursor origCursor = Cursor.Current;
                Cursor.Current = Cursors.WaitCursor;

                // Temp disable the RefreshData button...
                ux_buttonRefreshData.Enabled = false;

                // Resetting these two global variables will force a refresh of the DGV data...
                lastFullPath = "";
                lastTabName = "";

                // Save the user settings to local memory cache...
                SetAllUserSettings();

                // Check to see if this dataview uses Lookup Tables and update all of them now...
                RefreshDGVLookups();

                // Refresh the data view...
                RefreshMainDGVData();
                RefreshMainDGVFormatting();

                // Update all of the rest of the Lookup Tables in the background...
                LookupTableStatusCheck();

                // Re-enable the RefreshData button...
                ux_buttonRefreshData.Enabled = true;

                // Restore cursor to default cursor...
                Cursor.Current = origCursor;
            }
        }

        private void ux_menuitemToolsResetTreeviewLists_Click(object sender, EventArgs e)
        {
            DataSet UserListItems = _sharedUtils.GetWebServiceData("get_lists", ":cooperatorid=" + _usernameCooperatorID, 0, 0);
            DataSet modifiedData = new DataSet();
            DataSet saveErrors = new DataSet();
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("WARNING!!!  You are about to permanently delete all list objects (TreeView tabs and nodes) for your user account!\n\nAre you sure you want to do this?", "List Delete Confirmation", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button2);
            ggMessageBox.Name = "ux_menuitemToolsResetTreeviewListsMessageBox1";
            if (_sharedUtils != null && _sharedUtils.IsConnected) _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (DialogResult.OK == ggMessageBox.ShowDialog())
            {
                if (UserListItems.Tables.Contains("get_lists") &&
                _usernameCooperatorID == _currentCooperatorID)
                {
                    // Iterate through each record in the users item list and mark it for deletion...
                    foreach (DataRow dr in UserListItems.Tables["get_lists"].Rows)
                    {
                        dr.Delete();
                    }

                    // Get just the rows that have changed and put them in to a new dataset...
                    if (UserListItems.Tables["get_lists"].GetChanges() != null)
                    {
                        modifiedData.Tables.Add(UserListItems.Tables["get_lists"].GetChanges());
                    }
                    // Now save the changes back to the database...
                    saveErrors = SaveDGVData(modifiedData);

                    // Reset the user preference for visible tabs to nothing...
                    _sharedUtils.SaveAllUserSettings();

                    // Force a rebuild of the navigator tabs...
                    int currentCNOValue = (int)ux_comboboxCNO.SelectedValue;
                    ux_comboboxCNO.SelectedValue = -1;
                    ux_comboboxCNO.SelectedValue = currentCNOValue;
                }
            }
        }

        private void ux_menuitemToolsResetUserSettings_Click(object sender, EventArgs e)
        {
            GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("WARNING!!!  You are about to permanently delete all user settings for your user account!\n\nAre you sure you want to do this?", "List Delete Confirmation", MessageBoxButtons.OKCancel, MessageBoxDefaultButton.Button2);
            ggMessageBox.Name = "ux_menuitemToolsResetUserSettingsMessageBox1";
            if (_sharedUtils != null && _sharedUtils.IsConnected) _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
            string[] argsArray = new string[100];
            ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
            if (DialogResult.OK == ggMessageBox.ShowDialog())
            {
                // Reset all user preferences for the login account...
                if (_currentCooperatorID == _usernameCooperatorID)
                {
                    _sharedUtils.DeleteAllUserSettings();
                    _sharedUtils.SaveAllUserSettings();
                    LoadApplicationData();
                }
            }
        }

        private void ux_menuitemReportsCreateSampleData_Click(object sender, EventArgs e)
//        private void ux_menuitemToolsCreateLocalReportTables_Click(object sender, EventArgs e)
        {
            LocalDatabase ldb = new LocalDatabase("GRINGlobal_report_sample_data");
            ldb.CreateTable((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource, false, true);
        }

        private void ux_checkboxRememberRowFilters_CheckedChanged(object sender, EventArgs e)
        {
            if (ux_checkboxRememberRowFilters.Checked)
            {
                ux_checkboxPromptToClearRowFilter.Enabled = true;
            }
            else
            {
                ux_checkboxPromptToClearRowFilter.Enabled = false;
            }
        }

        private void bindingNavigatorFindDropDown_ButtonClick(object sender, EventArgs e)
        {
            if (bindingNavigatorFindAdvancedFindToolStripMenuItem.Checked)
            {
                bindingNavigatorFindAdvancedFindToolStripMenuItem.PerformClick();
            }
            else if(bindingNavigatorFindInColumnToolStripMenuItem.Checked)
            {
                bindingNavigatorFindInColumnToolStripMenuItem.PerformClick();
            }
            else // default to bindingNavigatorFindInDataviewToolStripMenuItem.Checked = true
            {
                bindingNavigatorFindInDataviewToolStripMenuItem.PerformClick();
            }
        }

        private void bindingNavigatorFindInColumnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Default Find is to search for matching text in the current column of the datagridview - but the
            // user has just chosen a new 'mode' for searches - update the checked menu item to reflect this...
            _dgvcFindResults.Clear();
            foreach (ToolStripMenuItem tsmiFindMode in bindingNavigatorFindButton.DropDownItems)
            {
                tsmiFindMode.Checked = false;
            }
            ((ToolStripMenuItem)sender).Checked = true;

            if (!string.IsNullOrEmpty(bindingNavigatorFindText.Text) &&
                ux_datagridviewMain.CurrentCell != null)
            {
                _dgvcFindResults = datagridviewColumnFind(ux_datagridviewMain.Columns[ux_datagridviewMain.CurrentCell.ColumnIndex].Name, bindingNavigatorFindText.Text);
            }
            // Clear all formatting and redraw the DGV...
            RefreshMainDGVFormatting();
            // Highlight the first found cell...
            _dgvcFindIndex = 0;
            if (_dgvcFindResults != null && _dgvcFindResults.Count > 0)
            {
                ux_datagridviewMain.CurrentCell = _dgvcFindResults[_dgvcFindIndex];
                bindingNavigatorFindNext.Enabled = true;
                bindingNavigatorFindPrev.Enabled = true;
            }
            else
            {
                bindingNavigatorFindNext.Enabled = false;
                bindingNavigatorFindPrev.Enabled = false;
            }
        }

        private void bindingNavigatorFindInDataviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Default Find is to search for matching text in the current column of the datagridview - but the
            // user has just chosen a new 'mode' for searches - update the checked menu item to reflect this...
            _dgvcFindResults.Clear();
            foreach (ToolStripMenuItem tsmiFindMode in bindingNavigatorFindButton.DropDownItems)
            {
                tsmiFindMode.Checked = false;
            }
            ((ToolStripMenuItem)sender).Checked = true;

            _dgvcFindResults.Clear();
            if (!string.IsNullOrEmpty(bindingNavigatorFindText.Text))
            {
                foreach (DataGridViewColumn dgvColumn in ux_datagridviewMain.Columns)
                {
                    _dgvcFindResults.AddRange(datagridviewColumnFind(dgvColumn.Name, bindingNavigatorFindText.Text));
                }
            }
            // Clear all formatting and redraw the DGV...
            RefreshMainDGVFormatting();
            // Highlight the first found cell...
            _dgvcFindIndex = 0;
            if (_dgvcFindResults != null && _dgvcFindResults.Count > 0)
            {
                ux_datagridviewMain.CurrentCell = _dgvcFindResults[_dgvcFindIndex];
                bindingNavigatorFindNext.Enabled = true;
                bindingNavigatorFindPrev.Enabled = true;
            }
            else
            {
                bindingNavigatorFindNext.Enabled = false;
                bindingNavigatorFindPrev.Enabled = false;
            }
        }

        private void bindingNavigatorFindText_TextChanged(object sender, EventArgs e)
        {
            // The search criteria has changed so clear the search results...
            if (_dgvcFindResults.Count > 0 ||
                string.IsNullOrEmpty(bindingNavigatorFindText.Text))
            {
                _dgvcFindResults.Clear();
                _dgvcFindIndex = 0;
                RefreshMainDGVFormatting();
            }

            // Set the visual cues for the find controls...
            if (string.IsNullOrEmpty(bindingNavigatorFindText.Text))
            {
                //bindingNavigatorFindButton.Enabled = false;
                bindingNavigatorFindNext.Enabled = false;
                bindingNavigatorFindPrev.Enabled = false;
            }
            else
            {
                bindingNavigatorFindButton.Enabled = true;
                bindingNavigatorFindNext.Enabled = false;
                bindingNavigatorFindPrev.Enabled = false;
            }
        }

        private void bindingNavigatorFindNext_Click(object sender, EventArgs e)
        {
            if (_dgvcFindResults.Count > 0)
            {
                if (_dgvcFindIndex < _dgvcFindResults.Count - 1)
                {
                    _dgvcFindIndex++;
                }
                else
                {
                    _dgvcFindIndex = 0;
                }
                // Set the found cell only if the column is visible...
                if (_dgvcFindResults[_dgvcFindIndex].Visible &&
                    ux_datagridviewMain.Rows.Contains(_dgvcFindResults[_dgvcFindIndex].OwningRow)) ux_datagridviewMain.CurrentCell = _dgvcFindResults[_dgvcFindIndex];
            }
        }

        private void bindingNavigatorFindPrev_Click(object sender, EventArgs e)
        {
            if (_dgvcFindResults.Count > 0)
            {
                if (_dgvcFindIndex > 0)
                {
                    _dgvcFindIndex--;
                }
                else
                {
                    _dgvcFindIndex = _dgvcFindResults.Count - 1;
                }
                // Set the found cell only if the column is visible...
                if (_dgvcFindResults[_dgvcFindIndex].Visible &&
                    ux_datagridviewMain.Rows.Contains(_dgvcFindResults[_dgvcFindIndex].OwningRow)) ux_datagridviewMain.CurrentCell = _dgvcFindResults[_dgvcFindIndex];
            }
        }

        private List<DataGridViewCell> datagridviewColumnFind(string columnName, string findText)
        {
            List<DataGridViewCell> dgvcFindResults = new List<DataGridViewCell>();
            string regexFindPattern = "^" + findText.Replace("%", ".*") + "$";
            foreach (DataGridViewRow dgvr in ux_datagridviewMain.Rows)
            {
                string dgvcText = dgvr.Cells[columnName].Value.ToString();
                if (SharedUtils.TargetStringFitsPattern(regexFindPattern, dgvcText, true)) dgvcFindResults.Add(dgvr.Cells[columnName]);
            }

            return dgvcFindResults;
        }

        private void bindingNavigatorFindAdvancedFindToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // For CIP advanced find dialogbox control (or wizard?)...
        }

        private void ClearTreeNodeBackColor(TreeNodeCollection tnc)
        {
            foreach (TreeNode tn in tnc)
            {
                tn.BackColor = Color.Empty;
                ClearTreeNodeBackColor(tn.Nodes);
            }
        }

        private void ResetTreeNodeFindResults()
        {
            // Reset the finding collection and index...
            _tvnFindIndex = 0;
            _tvnFindResults.Clear();
            // Disable the navigation buttons...
            ux_buttonTreeviewFind.Enabled = true;
            ux_buttonTreeviewFindNext.Enabled = false;
            ux_buttonTreeviewFindPrev.Enabled = false;
            // Clear the backcolor for all treeview nodes in each navigator tab...
            foreach (TabPage tp in _ux_NavigatorTabControl.TabPages)
            {
                if (tp.Name != "ux_tabpageGroupListNavigatorNewTab") ClearTreeNodeBackColor(((TreeView)tp.Controls[0]).Nodes);
            }
        }

        private void ux_textboxTreeviewFindText_TextChanged(object sender, EventArgs e)
        {
            ResetTreeNodeFindResults();
        }

        private void ux_buttonTreeviewFind_Click(object sender, EventArgs e)
        {
            ResetTreeNodeFindResults();
            if (_ux_NavigatorTabControl != null)
            {
                // Find all of the tree nodes that have matching text in all of the treeviews...
                foreach (TabPage tp in _ux_NavigatorTabControl.TabPages)
                {
                    if (tp.Name != "ux_tabpageGroupListNavigatorNewTab")
                    {
                        TreeView tv = (TreeView)tp.Controls[0];

                        // Find the treeview nodes that have matching text...
                        _tvnFindResults.AddRange(FindTreeViewNodeText(tv.Nodes, ux_textboxTreeviewFindText.Text));
                    }
                }

                // Change the background color of the tree nodes that matched the search text...
                foreach (TreeNode tn in _tvnFindResults)
                {
                    tn.BackColor = Color.Orange;
                }

                // Goto the first node in the find collection and make it visible...
                _tvnFindIndex = 0;
                if (_tvnFindResults.Count > 0)
                {
                    // Enable the navigation buttons...
                    ux_buttonTreeviewFind.Enabled = false;
                    ux_buttonTreeviewFindNext.Enabled = true;
                    ux_buttonTreeviewFindPrev.Enabled = true;

                    foreach (TabPage tp in _ux_NavigatorTabControl.TabPages)
                    {
                        if (tp.Name != "ux_tabpageGroupListNavigatorNewTab")
                        {
                            TreeView tv = (TreeView)tp.Controls[0];
                            TreeNode[] firstFoundNodes = tv.Nodes.Find(_tvnFindResults[_tvnFindIndex].Name, true);
                            if (firstFoundNodes.Length > 0)
                            {
                                _ux_NavigatorTabControl.SelectTab(tp);
                                firstFoundNodes[0].EnsureVisible();
                                firstFoundNodes[0].BackColor = Color.Gold;
                                //tv.SelectedNode = firstFoundNodes[0];
                                //tv.SelectedNode.EnsureVisible();
                                break;
                            }
                        }
                    }
                }
                else
                {
                    // Reset the collection of found nodes and the index to the current found node...
                    _tvnFindIndex = 0;
                    _tvnFindResults.Clear();
                    // Disable the navigation buttons...
                    ux_buttonTreeviewFind.Enabled = true;
                    ux_buttonTreeviewFindNext.Enabled = false;
                    ux_buttonTreeviewFindPrev.Enabled = false;
                    // Clear the backcolor for all treeview nodes in each navigator tab...
                    foreach (TabPage tp in _ux_NavigatorTabControl.TabPages)
                    {
                        if (tp.Name != "ux_tabpageGroupListNavigatorNewTab") ClearTreeNodeBackColor(((TreeView)tp.Controls[0]).Nodes);
                    }

                    // None of the treeview nodes match the search text - so now we should see if 
                    // there is a match for accession, inventory, order, or cooperator friendly names...
                    if (ux_datagridviewMain.DataSource.GetType() == typeof(BindingSource) &&
                        ((BindingSource)ux_datagridviewMain.DataSource).DataSource.GetType() == typeof(DataTable) &&
                        ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).PrimaryKey.Length > 0)
                    {
                        // Ask the user if they would like to continue their search on the remote database (resolving to
                        // the current PKEY on the active datagridview)...
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There are no matches for '{0}' in your Navigator Pane Treeviews.\n\nWould you like to continue searching for '{0}' on the server (all matches will be resolved to '{1}')", "Navigator Pane Treeview Find", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                        ggMessageBox.Name = "ux_buttonTreeviewFind";
                        _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                        string[] argsArray = new string[100];
                        argsArray[0] = ux_textboxTreeviewFindText.Text;
                        argsArray[1] = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).PrimaryKey[0].ColumnName;
                        ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);

                        if (DialogResult.Yes == ggMessageBox.ShowDialog())
                        {
                            DataTable dt = (DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource;
                            string ResolveTo = dt.PrimaryKey[0].ColumnName.Replace("_id", "").Trim();
                            DataSet ds = _sharedUtils.SearchWebService(ux_textboxTreeviewFindText.Text, true, true, "", ResolveTo, 0, 1000);
                            if (ds != null &&
                                ds.Tables.Contains("SearchResult") &&
                                ds.Tables["SearchResult"].Rows.Count > 0)
                            {
                                TabPage tpFindResults = new TabPage();
                                if (!_ux_NavigatorTabControl.TabPages.ContainsKey("Find Results"))
                                {
                                    string uniqueTabText = EnsureUniqueTabText(_ux_NavigatorTabControl, "Find Results");
                                    tpFindResults = BuildTabPageAndTreeView(uniqueTabText, null);
                                }
                                else
                                {
                                    tpFindResults = _ux_NavigatorTabControl.TabPages["Find Results"];
                                    // Sanity check to make sure the user did not delete all of the treeview folders in the 'Find Results' tab page...
                                    if (tpFindResults.Controls.Count > 0 &&
                                        tpFindResults.Controls[0].GetType() == typeof(TreeView))
                                    {
                                        // There is a root folder - next let's make sure there is
                                        // at least on sub-folder to store found objects...
                                        TreeNode tnRootNode = ((TreeView)tpFindResults.Controls[0]).Nodes[0];
                                        if (tnRootNode.Nodes.Count < 1)
                                        {
                                            // No sub-folders so make one now...
                                            TreeNode tnNewListNode = new TreeNode();
                                            tnNewListNode.Name = tnRootNode.FullPath;
                                            tnNewListNode.Text = "New List";
                                            tnNewListNode.Tag = "FOLDER; DYNAMIC_FOLDER_SEARCH_CRITERIA=; ";
                                            tnNewListNode.ImageKey = "inactive_folder";
                                            tnNewListNode.SelectedImageKey = "active_folder";
                                            tnNewListNode.ToolTipText = "";
                                            tnRootNode.Nodes.Add(tnNewListNode);
                                        }
                                    }
                                }

                                // Iterate thru the search results and add a new treenode for each PKey found...
                                foreach (DataRow dr in ds.Tables["SearchResult"].Rows)
                                {
                                    TreeNode newNode = new TreeNode();

                                    string dataviewPKeyName = dt.PrimaryKey[0].ColumnName;
                                    string tablePKeyName = dt.PrimaryKey[0].ExtendedProperties["table_field_name"].ToString().Trim();
                                    string tableName = dt.PrimaryKey[0].ExtendedProperties["table_name"].ToString().Trim();
                                    string properties = dataviewPKeyName.Trim().ToUpper();
                                    properties += ";:" + tablePKeyName.Replace("_", "") + "=" + dr["ID"].ToString();
                                    properties += ";@" + tableName + "." + tablePKeyName + "=" + dr["ID"].ToString();
                                    newNode.Name = properties.Split(';')[1].Trim();
                                    newNode.Text = newNode.Name;
                                    newNode.Tag = properties;
                                    newNode.ImageKey = "inactive_" + tablePKeyName.ToUpper();
                                    newNode.SelectedImageKey = "active_" + tablePKeyName.ToUpper();

                                    // Now check to see if virtual nodes are wanted for this new node...
                                    if (!string.IsNullOrEmpty(_sharedUtils.GetAppSettingValue(tablePKeyName + "_VIRTUAL_NODE_DATAVIEW")))
                                    {
                                        TreeNode dummyNode = new TreeNode();
                                        string virtualQuery = _sharedUtils.GetAppSettingValue(tablePKeyName + "_VIRTUAL_NODE_DATAVIEW") + ", ";
                                        virtualQuery += properties.Split(';')[1].Trim().ToLower();

                                        dummyNode.Text = "!!DUMMYNODE!!";
                                        dummyNode.Name = "!!DUMMYNODE!!"; ;
                                        dummyNode.Tag = virtualQuery;
                                        //dummyNode.ImageKey = "inactive_" + virtualQuery;
                                        //dummyNode.SelectedImageKey = "active_" + virtualQuery;
                                        newNode.Nodes.Add(dummyNode);
                                    }

                                    // If this is a new tab page - add it to the tab control...
                                    ((TreeView)tpFindResults.Controls[0]).Nodes[0].Nodes[0].Nodes.Add(newNode);
                                }

                                // Insert the tab page as the left-most tab the navigator tab control (but only if it was not there)...
                                if (!_ux_NavigatorTabControl.TabPages.ContainsKey("Find Results"))
                                {
                                    _ux_NavigatorTabControl.TabPages.Insert(0, tpFindResults);
                                }

                                // Create the new tool strip menu item and bind it to the click event handler...
                                ToolStripMenuItem tsmiNew = new ToolStripMenuItem(tpFindResults.Text, null, ux_tab_tsmi_NavigatorShowTabsItem_Click, tpFindResults.Text);
                                tsmiNew.Checked = true;
                                ((ToolStripMenuItem)_ux_NavigatorTabControl.ContextMenuStrip.Items["ux_tab_cms_NavigatorShowTab"]).DropDownItems.Add(tsmiNew);
                                // Add this new tabpage to the tabcontrol's dictionary list (for hide/show functionality)...
                                if (_ux_NavigatorTabControl != null &&
                                    _ux_NavigatorTabControl.Tag != null &&
                                    _ux_NavigatorTabControl.Tag.GetType() == typeof(Dictionary<string, TabPage>) &&
                                    !((Dictionary<string, TabPage>)_ux_NavigatorTabControl.Tag).ContainsKey(tpFindResults.Name))
                                {
                                    ((Dictionary<string, TabPage>)_ux_NavigatorTabControl.Tag).Add(tpFindResults.Name, tpFindResults);
                                }

                                // Give the new nodes their proper titles...
                                RefreshTreeviewNodeFormatting(((TreeView)tpFindResults.Controls[0]).Nodes[0].Nodes[0]);

                                // Refresh the datagridview...
                                _ux_NavigatorTabControl.SelectedTab = tpFindResults;
                                ((TreeView)tpFindResults.Controls[0]).SelectedNode = null;
                                ((TreeView)tpFindResults.Controls[0]).SelectedNode = ((TreeView)tpFindResults.Controls[0]).Nodes[0].Nodes[0];
                                ((TreeView)tpFindResults.Controls[0]).SelectedNode.EnsureVisible();
                            }
                        }
                    }
                }
            }
        }

        private List<TreeNode> FindTreeViewNodeText(TreeNodeCollection nodes, string findText)
        {
            List<TreeNode> returnTNList = new List<TreeNode>();
            string regexFindPattern = "^" + findText.Replace("%", ".*") + "$";

            foreach (TreeNode tn in nodes)
            {
                if (isFolder(tn))
                {
                    // Check to see if the folder name matches the findText and add it if it does...
                    if (SharedUtils.TargetStringFitsPattern(regexFindPattern, tn.Text, true))
                    {
                        returnTNList.Add(tn);
                    }
                    // Now recursively go thru all objects inside the folder...
                    returnTNList.AddRange(FindTreeViewNodeText(tn.Nodes, regexFindPattern));
                }
                else
                {
                    // Add the node if the node text matches the findText...
                    if (SharedUtils.TargetStringFitsPattern(regexFindPattern, tn.Text, true))
                    {
                        returnTNList.Add(tn);
                    }
                }
            }

            return returnTNList;
        }

        private void ux_buttonTreeviewFindNext_Click(object sender, EventArgs e)
        {
            // Change the background color of the tree nodes that matched the search text...
            foreach (TreeNode tn in _tvnFindResults)
            {
                tn.BackColor = Color.Orange;
            }
            // Calculate which node will get focus...
            if (_tvnFindIndex < _tvnFindResults.Count - 1)
            {
                _tvnFindIndex++;
            }
            else
            {
                _tvnFindIndex = 0;
            }

            // If there are no find results bail out now...
            if (_tvnFindResults == null || _tvnFindResults.Count == 0) return;

            // Change focus to the new node...
            TreeView tv = _tvnFindResults[_tvnFindIndex].TreeView;
            TabPage tp = (TabPage)tv.Parent;
            if (tv != null &&
                tp != null)
            {
                TreeNode[] foundNodes = tv.Nodes.Find(_tvnFindResults[_tvnFindIndex].Name, true);
                if (foundNodes.Length > 0)
                {
                    _ux_NavigatorTabControl.SelectTab(tp);
                    foundNodes[0].EnsureVisible();
                    foundNodes[0].BackColor = Color.Gold;
                }
            }
        }

        private void ux_buttonTreeviewFindPrev_Click(object sender, EventArgs e)
        {
            // Change the background color of the tree nodes that matched the search text...
            foreach (TreeNode tn in _tvnFindResults)
            {
                tn.BackColor = Color.Orange;
            }
            // Calculate which node will get focus...
            if (_tvnFindIndex > 0)
            {
                _tvnFindIndex--;
            }
            else
            {
                _tvnFindIndex = _tvnFindResults.Count - 1;
            }

            // If there are no find results bail out now...
            if (_tvnFindResults == null || _tvnFindResults.Count == 0) return;

            // Change focus to the new node...
            TreeView tv = _tvnFindResults[_tvnFindIndex].TreeView;
            TabPage tp = (TabPage)tv.Parent;
            if (tv != null &&
                tp != null)
            {
                TreeNode[] foundNodes = tv.Nodes.Find(_tvnFindResults[_tvnFindIndex].Name, true);
                if (foundNodes.Length > 0)
                {
                    _ux_NavigatorTabControl.SelectTab(tp);
                    foundNodes[0].EnsureVisible();
                    foundNodes[0].BackColor = Color.Gold;
                }
            }
        }
    }

    public static class StatusStripExtensions
    {
        private static ContentAlignment _ux_statusLeftMessage_TextAlign = ContentAlignment.MiddleLeft;
        private static ContentAlignment _ux_statusCenterMessage_TextAlign = ContentAlignment.MiddleLeft;
        private static ContentAlignment _ux_statusRightMessage_TextAlign = ContentAlignment.MiddleLeft;
        private static string _ux_statusLeftMessage_Text = "";
        private static string _ux_statusCenterMessage_Text = "";
        private static string _ux_statusRightMessage_Text = "";
        private static bool _ux_statusLeftProgressBar_Visible = false;
        private static int _ux_statusLeftProgressBar_Minimum = 0;
        private static int _ux_statusLeftProgressBar_Maximum = 100;
        private static bool _ux_statusRightProgressBar_Visible = false;
        private static int _ux_statusRightProgressBar_Minimum = 0;
        private static int _ux_statusRightProgressBar_Maximum = 100;

        public static void DefaultStateRestore(this System.Windows.Forms.StatusStrip ss)
        {
            ss.Items[0].TextAlign = _ux_statusLeftMessage_TextAlign;
            ss.Items[0].Text = _ux_statusLeftMessage_Text;
            ss.Items[1].Visible = _ux_statusLeftProgressBar_Visible;
            ((ToolStripProgressBar)ss.Items[1]).Minimum = _ux_statusLeftProgressBar_Minimum;
            ((ToolStripProgressBar)ss.Items[1]).Maximum = _ux_statusLeftProgressBar_Maximum;
            ss.Items[2].TextAlign = _ux_statusCenterMessage_TextAlign;
            ss.Items[2].Text = _ux_statusCenterMessage_Text;
            ss.Items[3].Visible = _ux_statusRightProgressBar_Visible;
            ((ToolStripProgressBar)ss.Items[3]).Minimum = _ux_statusRightProgressBar_Minimum;
            ((ToolStripProgressBar)ss.Items[3]).Maximum = _ux_statusRightProgressBar_Maximum;
            ss.Items[4].TextAlign = _ux_statusRightMessage_TextAlign;
            ss.Items[4].Text = _ux_statusRightMessage_Text;
        }

        public static void DefaultStateSave(this System.Windows.Forms.StatusStrip ss)
        {
            _ux_statusLeftMessage_TextAlign = ss.Items[0].TextAlign;
            _ux_statusLeftMessage_Text = ss.Items[0].Text;
            _ux_statusLeftProgressBar_Visible = ss.Items[1].Visible;
            _ux_statusLeftProgressBar_Minimum = ((ToolStripProgressBar)ss.Items[1]).Minimum;
            _ux_statusLeftProgressBar_Maximum = ((ToolStripProgressBar)ss.Items[1]).Maximum;
            _ux_statusCenterMessage_TextAlign = ss.Items[2].TextAlign;
            _ux_statusCenterMessage_Text = ss.Items[2].Text;
            _ux_statusRightProgressBar_Visible = ss.Items[3].Visible;
            _ux_statusRightProgressBar_Minimum = ((ToolStripProgressBar)ss.Items[3]).Minimum;
            _ux_statusRightProgressBar_Maximum = ((ToolStripProgressBar)ss.Items[3]).Maximum;
            _ux_statusRightMessage_TextAlign = ss.Items[4].TextAlign;
            _ux_statusRightMessage_Text = ss.Items[4].Text;
        }
    }

}
