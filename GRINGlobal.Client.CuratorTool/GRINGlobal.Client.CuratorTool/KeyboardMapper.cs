﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GRINGlobal.Client.Common;

namespace GRINGlobal.Client
{
    public partial class KeyboardMapper : Form
    {
        SharedUtils _sharedUtils;
        DataTable _origKeyboardMappings = new DataTable();
        DataTable _currentKeyboardMappings = new DataTable();

        public DataTable KeyboardMappings
        {
            get
            {
                return _currentKeyboardMappings;
            }
        }

        public KeyboardMapper(SharedUtils sharedUtils)
        {
            InitializeComponent();
            _sharedUtils = sharedUtils;
            _origKeyboardMappings = _sharedUtils.KeyboardMappings.Copy();
            _currentKeyboardMappings = _sharedUtils.KeyboardMappings.Copy();
        }

        private void KeyboardMapper_Load(object sender, EventArgs e)
        {
            foreach(DataRow dr in _origKeyboardMappings.Rows)
            {
                ux_comboboxFunction.Items.Add(dr["function_name"].ToString());
            }

            if (ux_comboboxFunction.Items.Count > 0)
            {
                ux_comboboxFunction.SelectedIndex = -1;
                ux_comboboxFunction.SelectedIndex = 0;
            }
        }

        private void ux_comboboxFunction_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow[] drs = _currentKeyboardMappings.Select("function_name = '" + ux_comboboxFunction.Text + "'");
            if(drs != null && drs.Length > 0)
            {
                ux_labelKeyboardShortcut.Text = drs[0]["key_data"].ToString();
            }
        }

        private void ProcessKeyDownEvents(KeyEventArgs e)
        {
            DataRow[] drs = _currentKeyboardMappings.Select("function_name = '" + ux_comboboxFunction.Text + "'");
            if (drs != null && drs.Length > 0)
            {
                ux_labelKeyboardShortcut.Text = e.KeyData.ToString();
                drs[0]["key_code"] = e.KeyCode.ToString();
                drs[0]["key_data"] = e.KeyData.ToString();
                drs[0]["key_value"] = e.KeyValue.ToString();
                drs[0]["modifiers"] = e.Modifiers.ToString();
            }
        }
        private void KeyboardMapper_KeyDown(object sender, KeyEventArgs e)
        {
            ProcessKeyDownEvents(e);
            this.Text = "Form - KeyDown";
        }

        private void ux_buttonReset_Click(object sender, EventArgs e)
        {
            DataRow[] origDRS = _origKeyboardMappings.Select("function_name = '" + ux_comboboxFunction.Text + "'");
            DataRow[] currentDRS = _currentKeyboardMappings.Select("function_name = '" + ux_comboboxFunction.Text + "'");
            if (origDRS != null && origDRS.Length > 0 &&
                currentDRS != null && currentDRS.Length > 0)
            {
                ux_labelKeyboardShortcut.Text = origDRS[0]["key_data"].ToString();
                currentDRS[0]["key_code"] = origDRS[0]["key_code"].ToString();
                currentDRS[0]["key_data"] = origDRS[0]["key_data"].ToString();
                currentDRS[0]["key_value"] = origDRS[0]["key_value"].ToString();
                currentDRS[0]["modifiers"] = origDRS[0]["modifiers"].ToString();
            }

        }

        private void ux_buttonSave_Click(object sender, EventArgs e)
        {
            _sharedUtils.KeyboardMappings = _currentKeyboardMappings.Copy();
            this.Close();
        }

        private void ux_buttonCancel_Click(object sender, EventArgs e)
        {
            // Cancel edits - so revert the current records to the original records...
            _currentKeyboardMappings = _origKeyboardMappings.Copy();
            this.Close();
        }

        private void KeyboardMapper_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            DataRow[] drs = _currentKeyboardMappings.Select("function_name = '" + ux_comboboxFunction.Text + "'");
            if (drs != null && drs.Length > 0)
            {
                ux_labelKeyboardShortcut.Text = e.KeyData.ToString();
                drs[0]["key_code"] = e.KeyCode.ToString();
                drs[0]["key_data"] = e.KeyData.ToString();
                drs[0]["key_value"] = e.KeyValue.ToString();
                drs[0]["modifiers"] = e.Modifiers.ToString();
            }
            this.Text = "Form - PreviewKeyDown";
        }
    }
}
