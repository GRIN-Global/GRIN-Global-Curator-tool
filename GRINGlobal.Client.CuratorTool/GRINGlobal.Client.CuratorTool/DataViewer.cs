﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using GRINGlobal.Client.Common;
using System.Collections.Generic;

namespace GRINGlobal.Client.CuratorTool
{
    public partial class GRINGlobalClientCuratorTool
    {
        DataGridViewSelectedCellCollection selectedCells;

        #region TabControl for Dataview logic...

        private void ux_dataviewmenuNewTab_Click(object sender, EventArgs e)
        {
            int indexOfNewTab = ux_tabcontrolCTDataviews.SelectedIndex;
            // Add the tab to the tabcontrol...
            _sharedUtils.ux_tabcontrolCreateNewTab(ux_tabcontrolCTDataviews, indexOfNewTab);
        }

        private void ux_dataviewmenuTabProperties_Click(object sender, EventArgs e)
        {
            int indexOfCurrentTab = ux_tabcontrolCTDataviews.SelectedIndex;

            _sharedUtils.ux_tabcontrolShowProperties(ux_tabcontrolCTDataviews, indexOfCurrentTab);

            SetAllUserSettings();
            // Refresh the data view...
            RefreshMainDGVData();
            RefreshMainDGVFormatting();
            RefreshForm();
        }

        private void ux_dataviewmenuCloseTab_Click(object sender, EventArgs e)
        {
            int indexOfTabToDelete = ux_tabcontrolCTDataviews.SelectedIndex;
            _sharedUtils.ux_tabcontrolRemoveTab(ux_tabcontrolCTDataviews, indexOfTabToDelete);

            // Save the changes to tabcontrol in the usersettings...
            SetAllUserSettings();
        }

        private void ux_dataviewmenuCloseOtherTabs_Click(object sender, EventArgs e)
        {

            int indexOfTabToKeep = ux_tabcontrolCTDataviews.SelectedIndex;
            TabPage tabPageToKeep = ux_tabcontrolCTDataviews.SelectedTab;
            ux_tabcontrolCTDataviews.TabPages.Remove(tabPageToKeep);
            ux_tabcontrolCTDataviews.TabPages.Insert(0, tabPageToKeep);
            ux_tabcontrolCTDataviews.SelectedTab = tabPageToKeep;
            while (ux_tabcontrolCTDataviews.TabPages.Count > 2)
            {
                ux_tabcontrolCTDataviews.TabPages.RemoveAt(1);
            }
        }

        private void ux_dataviewmenuCloseTabsToTheRight_Click(object sender, EventArgs e)
        {
            int indexOfTabToKeep = ux_tabcontrolCTDataviews.SelectedIndex;
            while (ux_tabcontrolCTDataviews.TabPages.Count > indexOfTabToKeep + 2)
            {
                ux_tabcontrolCTDataviews.TabPages.RemoveAt(indexOfTabToKeep + 1);
            }
        }

        private void ux_tabcontrolDataview_Deselecting(object sender, TabControlCancelEventArgs e)
        {
            // Update all TabControl settings on the userSettings dataset...
            SetDGVMainDataviewUserSettings();
        }

        private void ux_tabcontrolDataview_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ux_tabcontrolCTDataviews.SelectedIndex > -1)
            {
                if (ux_tabcontrolCTDataviews.SelectedTab.Name == "ux_tabpageDataviewNewTab")
                {
                    int indexOfNewTab = ux_tabcontrolCTDataviews.SelectedIndex;
                    // Add the tab to the tabcontrol...
                    _sharedUtils.ux_tabcontrolCreateNewTab(ux_tabcontrolCTDataviews, indexOfNewTab);
                }
            }

            // If there is a form still attached to the global variable dataviewForm - dispose of it properly...
            if (dataviewForm != null)
            {
                dataviewForm.Close();
                dataviewForm.Dispose();
                dataviewForm = null;
            }

            // Refresh the data view...
            RefreshMainDGVData();
            RefreshMainDGVFormatting();
            // Display a form if the tab property indicates...
            RefreshForm();
       }
 
        private void ux_tabcontrolDataview_MouseDown(object sender, MouseEventArgs e)
        {
            _sharedUtils.ux_tabcontrolMouseDownEvent(ux_tabcontrolCTDataviews, e);
        }

        private void ux_tabcontrolDataview_DragOver(object sender, DragEventArgs e)
        {
            _sharedUtils.ux_tabcontrolDragOverEvent(ux_tabcontrolCTDataviews, e);
        }

        private void ux_tabcontrolDataview_DragDrop(object sender, DragEventArgs e)
        {
            _sharedUtils.ux_tabcontrolDragDropEvent(ux_tabcontrolCTDataviews, e);
        }

        #endregion

        #region Vertical TabControl for Dataview Options logic...

        private void ux_tabcontrolDataDataviewOptions_DrawItem(object sender, DrawItemEventArgs e)
        {
            Rectangle clipRectangle = ux_tabcontrolDataviewOptions.GetTabRect(e.Index);
            float offsetX = (float)(clipRectangle.Left + ((float)clipRectangle.Width / 2.0));
            float offsetY = (float)(clipRectangle.Top + ((float)clipRectangle.Height / 2.0));
            Font tabFont;
            Brush tabBrush;
            if (ux_tabcontrolDataviewOptions.SelectedIndex == e.Index)
            {
                tabFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Underline, GraphicsUnit.Point);
                tabBrush = Brushes.Black;
                e.Graphics.FillRectangle(Brushes.White, clipRectangle);
                e.Graphics.FillRectangle(Brushes.Orange, clipRectangle.X, clipRectangle.Y, 3, clipRectangle.Height);
            }
            else
            {
                tabFont = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point);
                tabBrush = Brushes.Black;
                e.Graphics.FillRectangle(Brushes.WhiteSmoke, clipRectangle);
            }

            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            sf.FormatFlags = StringFormatFlags.DirectionVertical;

            e.Graphics.ResetTransform();
            e.Graphics.TranslateTransform(-offsetX, -offsetY, System.Drawing.Drawing2D.MatrixOrder.Append);
            e.Graphics.RotateTransform(180f, System.Drawing.Drawing2D.MatrixOrder.Append);
            e.Graphics.TranslateTransform(offsetX, offsetY, System.Drawing.Drawing2D.MatrixOrder.Append);
            e.Graphics.DrawString(ux_tabcontrolDataviewOptions.TabPages[e.Index].Text,
                //new Font("Microsoft Sans Serif", 8f, FontStyle.Regular, GraphicsUnit.Point), 
                //new System.Drawing.SolidBrush(e.ForeColor),
                                    tabFont,
                                    tabBrush,
                                    ux_tabcontrolDataviewOptions.GetTabRect(e.Index),
                                    new StringFormat(sf));

        }

        private void ux_tabcontrolDataviewOptions_Enter(object sender, EventArgs e)
        {
            if (ux_tabcontrolDataviewOptions.Left > ux_datagridviewMain.Right)
            {
                ux_tabcontrolDataviewOptions.Left -= ux_tabcontrolDataviewOptions.TabPages[0].Width;
            }
            verticalTabExposedTimer.Stop();
            verticalTabExposedTimer.Interval = 1000;
            verticalTabExposedTimer.Start();
        }

        private void ux_tabcontrolDataviewOptions_Leave(object sender, EventArgs e)
        {
            if (ux_tabcontrolDataviewOptions.Tag == null &&
                ux_tabcontrolDataviewOptions.Left < ux_datagridviewMain.Right)
            {
                ux_tabcontrolDataviewOptions.Left += ux_tabcontrolDataviewOptions.TabPages[0].Width;
                verticalTabExposedTimer.Stop();
                // Remember any of the settings that might have been changed in the vertical tab view...
                SetAllUserSettings();
                // If the vertical tab still has focus yield focus to the main DGV...
                //if (ux_tabcontrolDataviewOptions.Focused) ux_datagridviewMain.Focus();
                ux_datagridviewMain.Select();
            }
        }

        private void ux_checkboxHighlightChanges_CheckedChanged(object sender, EventArgs e)
        {
            // Save the setting right now so that all DGV logic will do the right thing....
            _sharedUtils.SaveUserSetting("", ux_checkboxHighlightChanges.Name, "Checked", ux_checkboxHighlightChanges.Checked.ToString());
            RefreshMainDGVFormatting();
            ux_datagridviewMain.Focus();
        }

        private void ux_checkboxHideNonErrorRows_CheckedChanged(object sender, EventArgs e)
        {
            // Hide the non-error rows if the checkbox indicates...
            if (ux_checkboxHideNonErrorRows.Checked && ux_buttonSaveData.Enabled)
            {
                ux_datagridviewMain.CurrentCell = null; // You can't hide a row that the CurrentCell property is pointing at
                DataTable dt = (DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource;
                string errorList = dt.PrimaryKey[0].ColumnName + " IN (";
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr.HasErrors)
                    {
                        errorList += dr[dt.PrimaryKey[0].ColumnName].ToString() + ",";
                    }
                }
                errorList = errorList.TrimEnd(new char[] { ',' }) + ")";
                errorList = errorList.Replace(" IN ()", " IN (null)");
                dt.DefaultView.RowFilter = errorList;
            }
            else
            {
                ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.RowFilter = "";
            }
            RefreshMainDGVFormatting();
            ux_datagridviewMain.Focus();
        }

        private void ux_checkboxHideUnchangedRows_CheckedChanged(object sender, EventArgs e)
        {
            // Hide the unchanged rows if the checkbox indicates...
            if (ux_checkboxHideUnchangedRows.Checked && ux_buttonSaveData.Enabled)
            {
                ux_datagridviewMain.CurrentCell = null; // You can't hide a row that the CurrentCell property is pointing at
                ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.RowStateFilter = DataViewRowState.ModifiedCurrent;
            }
            else
            {
                ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.RowStateFilter = DataViewRowState.CurrentRows;
            }
            RefreshMainDGVFormatting();
            ux_datagridviewMain.Focus();
        }

        private void ux_checkboxHideCopyPasteWarnings_CheckedChanged(object sender, EventArgs e)
        {
            // Save the setting right now so that all DGV logic will do the right thing....
            _sharedUtils.SaveUserSetting("", ux_checkboxHideCopyPasteWarnings.Name, "Checked", ux_checkboxHideCopyPasteWarnings.Checked.ToString());
        }

        private void ux_buttonDefaultCellColor_Click(object sender, EventArgs e)
        {
            if (ux_tabcontrolDataviewOptions.Tag == null)
            {
                ux_tabcontrolDataviewOptions.Tag = "DefaultCellColor";
            }
            ux_colordialog.Color = ux_datagridviewMain.DefaultCellStyle.BackColor;
            DialogResult diagResult = ux_colordialog.ShowDialog();
            ux_datagridviewMain.DefaultCellStyle.BackColor = ux_colordialog.Color;
            if (ux_tabcontrolDataviewOptions.Tag == "DefaultCellColor")
            {
                if (verticalTabExposedTimer.Interval < 5000)
                {
                    verticalTabExposedTimer.Stop();
                    verticalTabExposedTimer.Interval = 5000;
                    verticalTabExposedTimer.Start();
                }
                ux_tabcontrolDataviewOptions.Tag = null;
            }
        }

        private void ux_buttonAlternatingRowColor_Click(object sender, EventArgs e)
        {
            if (ux_tabcontrolDataviewOptions.Tag == null)
            {
                ux_tabcontrolDataviewOptions.Tag = "AlternatingRowColor";
            }
            ux_colordialog.Color = ux_datagridviewMain.AlternatingRowsDefaultCellStyle.BackColor;
            DialogResult diagResult = ux_colordialog.ShowDialog();
            ux_datagridviewMain.AlternatingRowsDefaultCellStyle.BackColor = ux_colordialog.Color;
            if (ux_tabcontrolDataviewOptions.Tag == "AlternatingRowColor")
            {
                if (verticalTabExposedTimer.Interval < 5000)
                {
                    verticalTabExposedTimer.Stop();
                    verticalTabExposedTimer.Interval = 5000;
                    verticalTabExposedTimer.Start();
                }
                ux_tabcontrolDataviewOptions.Tag = null;
            }
        }

        #endregion

        #region CheckedListBox for selecting Viewed Columns in the DataGridView logic...

        private void ux_checkedlistboxViewedColumns_Leave(object sender, EventArgs e)
        {
            // Remember all user settings...
            SetDGVMainDataviewUserSettings();
        }

        private void ux_checkedlistboxViewedColumns_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            // This method uses a foreach loop because the 'friendly name' text in the checkbox list is not the datatable field name
            // so finding the correct column in the dgv would be difficult (because we would need to inspect each dgv column Caption 
            // to get the 'friendly name' text to match to the checkbox
            int clbItem = -1;
            foreach (DataGridViewColumn dgvc in ux_datagridviewMain.Columns)
            {
                clbItem = ux_checkedlistboxViewedColumns.FindStringExact(dgvc.HeaderText);
                if (clbItem > -1) dgvc.Visible = ux_checkedlistboxViewedColumns.GetItemChecked(clbItem);
                // This next line of code is necessary because the checkbox that has been changed
                // has not had it's new value assigned yet, so you must inspect the e.NewValue state
                if (clbItem == e.Index) dgvc.Visible = (e.NewValue == CheckState.Checked);
            }
            // Now we need to iterate through each item in the checklistbox collection to determine 
            // if all checkboxes are uniformly checked or unchecked so that we can set the 
            // checkedstate of the 'Select/Deselect All' checkbox...
            bool allAreChecked = true;
            bool allAreUnchecked = true;
            for (int i = 0; i < ux_checkedlistboxViewedColumns.Items.Count; i++)
            {
                bool checkedState = ux_checkedlistboxViewedColumns.GetItemChecked(i);
                if(i == e.Index) checkedState = (e.NewValue == CheckState.Checked);

                if (checkedState)
                {
                    allAreUnchecked = false;
                }
                else
                {
                    allAreChecked = false;
                }
            }
            if (allAreChecked) ux_checkboxSelectAll.CheckState = CheckState.Checked;
            else if (allAreUnchecked) ux_checkboxSelectAll.CheckState = CheckState.Unchecked;
            else ux_checkboxSelectAll.CheckState = CheckState.Indeterminate;
        }

        private void ux_checkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (ux_checkboxSelectAll.CheckState == CheckState.Checked)
            {
                for (int i = 0; i < ux_checkedlistboxViewedColumns.Items.Count; i++) ux_checkedlistboxViewedColumns.SetItemChecked(i, true);
            }
            else if (ux_checkboxSelectAll.CheckState == CheckState.Unchecked)
            {
                for (int i = 0; i < ux_checkedlistboxViewedColumns.Items.Count; i++) ux_checkedlistboxViewedColumns.SetItemChecked(i, false);
            }
        }

        #endregion

        #region Edit, Save, and Cancel buttons and edit-specific checkboxes logic...

        private void ux_buttonEditData_Click(object sender, EventArgs e)
        {
            // Remember the first visible column to restore later...
            int firstDisplayedCellRowIndex = 0;
            int firstDisplayedCellColumnIndex = 0;
            if (ux_datagridviewMain.FirstDisplayedCell != null)
            {
                firstDisplayedCellRowIndex = ux_datagridviewMain.FirstDisplayedCell.RowIndex;
                firstDisplayedCellColumnIndex = ux_datagridviewMain.FirstDisplayedCell.ColumnIndex;
            }
            int currentRowIndex = 0;
            int currentColumnIndex = 0;
            if (ux_datagridviewMain.CurrentCell != null)
            {
                currentRowIndex = ux_datagridviewMain.CurrentCell.RowIndex;
                currentColumnIndex = ux_datagridviewMain.CurrentCell.ColumnIndex;
            }

            // Remember all user settings...
            SetAllUserSettings();

            // Update the controls on the interface...
            ux_groupboxEditMode.Visible = true;
            ux_checkboxHighlightChanges.Enabled = true;
            ux_checkboxHideNonErrorRows.Enabled = true;
            ux_checkboxHideUnchangedRows.Enabled = true;
            ux_checkboxHideCopyPasteWarnings.Enabled = true;
            ux_buttonSaveData.Enabled = true;
            ux_buttonEditData.Enabled = false;
            ux_buttonCancelEditData.Enabled = true;
            ux_splitcontainerMain.Panel1.Enabled = false;
            ux_tabcontrolCTDataviews.Enabled = false;

            // Disable saving user settings when in edit mode...
            ux_buttonSaveUserSettingsNow.Enabled = false;

            // Bail if there is no data to process...
            if (ux_tabcontrolCTDataviews.SelectedTab == null ||
                ux_tabcontrolCTDataviews.SelectedTab.Tag == null ||
                ux_tabcontrolCTDataviews.SelectedTab.Tag.GetType() != typeof(DataviewProperties) ||
                string.IsNullOrEmpty(((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName))
            {
                return;
            }

            // Create a filtered and sorted copy of the data to be edited...
            // First get the filtered and sorted rows in the readonly datatable copied to a new temp table...
            DataTable filteredSortedTable = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.ToTable();
            // Create a new empty table that will have just the rows in the filtered and sorted rows in the temp table...
            DataTable editTable = grinData.Tables[((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName].Clone();
            // Add each row in the readonly view to the edit table...
            System.Collections.Generic.List<object> pKeyVals = new System.Collections.Generic.List<object>();
            foreach (DataRow dr in filteredSortedTable.Rows)
            {
                pKeyVals.Clear();
                foreach (DataColumn pKeyCol in editTable.PrimaryKey)
                {
                    pKeyVals.Add(dr[pKeyCol.ColumnName]);
                }
                DataRow newRow = editTable.NewRow();

                if (pKeyVals.Count > 0 && grinData.Tables[((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName].Rows.Contains(pKeyVals.ToArray()))
                {
                    newRow.ItemArray = grinData.Tables[((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName].Rows.Find(pKeyVals.ToArray()).ItemArray;
                    editTable.Rows.Add(newRow);
                }
            }

            editTable.AcceptChanges();

            // Drop the original datatable and add the new one...
            grinData.Tables.Remove(((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName);
            grinData.Tables.Add(editTable);

            // Create the editable DGV...
            _sharedUtils.BuildEditDataGridView(ux_datagridviewMain, grinData.Tables[((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName]);
            
            // Refresh formatting on the new DGV...
            RefreshMainDGVFormatting();

            // Restore the column widths to match the Read-Only settings (this is done only once during the building
            // of the Edit DGV - it is not performed in the normal 'RefreshMainDGVFormatting' method...
            string[] columnWidths = _sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "Columns.Width", "").Split(' ');
            string[] columnOrder = _sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "Columns.DisplayOrder", "").Split(' ');
            int columnNum = -1;
            SortedDictionary<int, int> sdDisplayIndex = new SortedDictionary<int, int>();
            if (columnWidths.Length == columnOrder.Length && columnWidths.Length == ux_datagridviewMain.Columns.Count)
            {
                for (int i = 0; i < columnOrder.Length; i++)
                {
                    if (int.TryParse(columnOrder[i], out columnNum)) sdDisplayIndex[columnNum] = i;
                }

                for (int i = 0; i < ux_datagridviewMain.Columns.Count; i++)
                {
                    if (int.TryParse(columnWidths[i], out columnNum)) ux_datagridviewMain.Columns[i].Width = columnNum;// Convert.ToInt32(columnWidths[i]);
                    if (sdDisplayIndex.Keys.Contains(i)) ux_datagridviewMain.Columns[sdDisplayIndex[i]].DisplayIndex = i;
                }
            }

            // Prepare the grid view for handling cell edits...
            ux_datagridviewMain.Enabled = true;
            ux_datagridviewMain.Focus();
            ux_datagridviewMain.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
            ux_datagridviewMain.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2;
            //ux_datagridviewMain.AllowUserToAddRows = true;
            ux_datagridviewMain.AllowUserToDeleteRows = true;
            ux_datagridviewMain.AllowDrop = true;

            // With SelectionMode updated it is now safe to restore the DGV to show the leftmost visible column (this was the leftmost visible column in Read-Only mode)... 
            if (ux_datagridviewMain.Rows.Count > 0)
            {
                if (firstDisplayedCellRowIndex < 0) firstDisplayedCellRowIndex = 0;
                if (firstDisplayedCellRowIndex >= ux_datagridviewMain.Rows.Count) firstDisplayedCellRowIndex = ux_datagridviewMain.Rows.Count - 1;
                if (firstDisplayedCellColumnIndex < 0) firstDisplayedCellColumnIndex = 0;
                if (firstDisplayedCellColumnIndex >= ux_datagridviewMain.Columns.Count) firstDisplayedCellColumnIndex = ux_datagridviewMain.Columns.Count - 1;
                if (ux_datagridviewMain[firstDisplayedCellColumnIndex, firstDisplayedCellRowIndex].Visible)
                {
                    ux_datagridviewMain.FirstDisplayedCell = ux_datagridviewMain[firstDisplayedCellColumnIndex, firstDisplayedCellRowIndex];
                }
                if (currentRowIndex < 0) currentRowIndex = 0;
                if (currentRowIndex >= ux_datagridviewMain.Rows.Count) currentRowIndex = ux_datagridviewMain.Rows.Count - 1;
                if (currentColumnIndex < 0) currentColumnIndex = 0;
                if (currentColumnIndex >= ux_datagridviewMain.Columns.Count) currentColumnIndex = ux_datagridviewMain.Columns.Count - 1;
                if (ux_datagridviewMain[currentColumnIndex, currentRowIndex].Visible)
                {
                    ux_datagridviewMain.CurrentCell = ux_datagridviewMain[currentColumnIndex, currentRowIndex];
                }
            }

            // Set the context menu for the columns...
            foreach (DataGridViewColumn dgvc in ux_datagridviewMain.Columns)
            {
                dgvc.ContextMenuStrip = ux_contextmenustripEditDGVCell;
            }

            // Un-wire the event handler for formatting cells in readonly mode...
            ux_datagridviewMain.CellFormatting -= new DataGridViewCellFormattingEventHandler(ux_datagridviewMain_ReadOnlyDGVCellFormatting);

            // Wire up the event handler for formatting cells in edit mode (populating the FK lookup values)...
            ux_datagridviewMain.CellFormatting += new DataGridViewCellFormattingEventHandler(ux_datagridviewMain_EditDGVCellFormatting);
            ux_datagridviewMain.CellParsing += new DataGridViewCellParsingEventHandler(ux_datagridviewMain_EditDGVCellParsing);
            ux_datagridviewMain.DataError += new DataGridViewDataErrorEventHandler(dataGridView_DataError);
            
            // If a Form is being shown, enable its controls...
            if (dataviewForm != null)
            {
                System.Reflection.PropertyInfo pi = dataviewForm.GetType().GetProperty("EditMode");
                if(pi != null) pi.SetValue(dataviewForm, true, null);
            }
        }

        private void ux_buttonSaveData_Click(object sender, EventArgs e)
        {
            DataSet modifiedRecords = new DataSet();
            DataSet errorRecords;
            int errorCount = 0;

            // Remember the changes the user has made to the controls for the
            // DGV during edit mode...
            // 1) Remember the state of the AllowUserToAddRows property...
            bool origAllowUsersToAddRows = ux_datagridviewMain.AllowUserToAddRows;
            // 2) Remember the state of the hide non-error rows checkbox...
            bool hideNonErrorRows = ux_checkboxHideNonErrorRows.Checked;
            // 3) Remove row filter for row errors...
            ux_checkboxHideNonErrorRows.Checked = false;

            foreach (DataRowView drv in ((BindingSource)ux_datagridviewMain.DataSource).List)
            {
                if (drv.IsEdit ||
                    drv.Row.RowState == DataRowState.Added ||
                    drv.Row.RowState == DataRowState.Deleted ||
                    drv.Row.RowState == DataRowState.Detached ||
                    drv.Row.RowState == DataRowState.Modified)
                {
                    drv.EndEdit();
                    drv.Row.ClearErrors();
                }
            }
            // Get just the rows that have changed and put them in to a new dataset...
            if (((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).GetChanges() != null)
            {
                modifiedRecords.Tables.Add(((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).GetChanges());
                ScrubData(modifiedRecords);
            }

            if (modifiedRecords.Tables.Contains(((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).TableName) &&
                modifiedRecords.Tables[((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).TableName].GetErrors().Length > 0)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The data being saved has errors that should be reviewed.\n\nWould you like to review them now?\n\nClick Yes to abort saving the data and review the errors now.\n(Click No to continue saving the data with errors).", "Data Errors Need Review", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ux_buttonSaveDataMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                if (DialogResult.Yes == ggMessageBox.ShowDialog()) return;
            }

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Temp. supress the AllowUserToAddRows property to allow processing of the save results...
            ux_datagridviewMain.AllowUserToAddRows = false;

            // If there are changes in this datagridview - save them now...
            if (modifiedRecords.Tables.Contains(((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).TableName))
            {
                // Save the data...
                errorRecords = SaveDGVData(modifiedRecords);

                // Update/refresh the treeview items with changes that were just saved...
                errorCount = SyncSavedRecordsWithTreeViewAndDGV(errorRecords.Tables[((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).TableName]);
            }

            if (errorCount == 0)
            {
                // Restore the grid view for handling row selections...
                ux_datagridviewMain.Enabled = true;
                ux_datagridviewMain.Focus();
                ux_datagridviewMain.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                ux_datagridviewMain.EditMode = DataGridViewEditMode.EditProgrammatically;
                ux_datagridviewMain.AllowUserToAddRows = false;
                ux_datagridviewMain.AllowUserToDeleteRows = false;
                ux_datagridviewMain.AllowDrop = false;

                // Update the controls on the interface...
                ux_groupboxEditMode.Visible = false;
                ux_checkboxHighlightChanges.Enabled = false;
                ux_checkboxHideNonErrorRows.Checked = false;
                ux_checkboxHideNonErrorRows.Enabled = false;
                ux_checkboxHideUnchangedRows.Checked = false;
                ux_checkboxHideUnchangedRows.Enabled = false;
                ux_checkboxHideCopyPasteWarnings.Enabled = false;
                ux_buttonSaveData.Enabled = false;
                ux_buttonEditData.Enabled = true;
                ux_buttonCancelEditData.Enabled = false;
                ux_splitcontainerMain.Panel1.Enabled = true;
                ux_tabcontrolCTDataviews.Enabled = true;

                // Enable saving user settings when in read-only mode...
                ux_buttonSaveUserSettingsNow.Enabled = true;

                // Un-wire the event handler for formatting cells in edit mode (populating the FK lookup values)...
                ux_datagridviewMain.CellFormatting -= new DataGridViewCellFormattingEventHandler(ux_datagridviewMain_EditDGVCellFormatting);
                ux_datagridviewMain.CellParsing -= new DataGridViewCellParsingEventHandler(ux_datagridviewMain_EditDGVCellParsing);

                // Wire up the event handler for formatting cells in readonly mode...
                ux_datagridviewMain.CellFormatting += new DataGridViewCellFormattingEventHandler(ux_datagridviewMain_ReadOnlyDGVCellFormatting);

                // Remember the first visible column to restore later...
                int firstDisplayedCellRowIndex = 0;
                int firstDisplayedCellColumnIndex = 0;
                if (ux_datagridviewMain.FirstDisplayedCell != null)
                {
                    firstDisplayedCellRowIndex = ux_datagridviewMain.FirstDisplayedCell.RowIndex;
                    firstDisplayedCellColumnIndex = ux_datagridviewMain.FirstDisplayedCell.ColumnIndex;
                }
                int currentRowIndex = 0;
                int currentColumnIndex = 0;
                if (ux_datagridviewMain.CurrentCell != null)
                {
                    currentRowIndex = ux_datagridviewMain.CurrentCell.RowIndex;
                    currentColumnIndex = ux_datagridviewMain.CurrentCell.ColumnIndex;
                }

                // Now refresh the data and a new copy of the table should be retrieved...
                // Resetting these two global variables will force a refresh of the DGV data...
                lastFullPath = "";
                lastTabName = "";
                RefreshMainDGVData();
                // Is the next line needed anymore???
                RefreshMainDGVFormatting();

                // With SelectionMode updated it is now safe to restore the DGV to show the leftmost visible column (this was the leftmost visible column in Read-Only mode)... 
                if (ux_datagridviewMain.Rows.Count > 0)
                {
                    if (firstDisplayedCellRowIndex < 0) firstDisplayedCellRowIndex = 0;
                    if (firstDisplayedCellRowIndex >= ux_datagridviewMain.Rows.Count) firstDisplayedCellRowIndex = ux_datagridviewMain.Rows.Count - 1;
                    if (firstDisplayedCellColumnIndex < 0) firstDisplayedCellColumnIndex = 0;
                    if (firstDisplayedCellColumnIndex >= ux_datagridviewMain.Columns.Count) firstDisplayedCellColumnIndex = ux_datagridviewMain.Columns.Count - 1;
                    if (ux_datagridviewMain[firstDisplayedCellColumnIndex, firstDisplayedCellRowIndex].Visible)
                    {
                        ux_datagridviewMain.FirstDisplayedCell = ux_datagridviewMain[firstDisplayedCellColumnIndex, firstDisplayedCellRowIndex];
                    }
                    if (currentRowIndex < 0) currentRowIndex = 0;
                    if (currentRowIndex >= ux_datagridviewMain.Rows.Count) currentRowIndex = ux_datagridviewMain.Rows.Count - 1;
                    if (currentColumnIndex < 0) currentColumnIndex = 0;
                    if (currentColumnIndex >= ux_datagridviewMain.Columns.Count) currentColumnIndex = ux_datagridviewMain.Columns.Count - 1;
                    if (ux_datagridviewMain[currentColumnIndex, currentRowIndex].Visible)
                    {
                        ux_datagridviewMain.CurrentCell = ux_datagridviewMain[currentColumnIndex, currentRowIndex];
                    }
                }

                // Moved DataError event handler to here to handle missing codes in edited rows during the RefreshMainDGVData() method call (this was done on 8/5/2014)...
                // Un-wire the event handler for processing data errors for cells in edit mode...
                ux_datagridviewMain.DataError -= new DataGridViewDataErrorEventHandler(dataGridView_DataError);

                // If a Form is being shown, disable its controls since we are done editing...
                if (dataviewForm != null)
                {
                    System.Reflection.PropertyInfo pi = dataviewForm.GetType().GetProperty("EditMode");
                    if (pi != null) pi.SetValue(dataviewForm, false, null);
                }
            }
            else
            {
                if (errorCount == 1)
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an error encountered during the Save operation.", "Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ux_buttonSaveDataMessage2";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    ggMessageBox.ShowDialog();
                }
                else
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There were {0} errors encountered during the Save operation.", "Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.Name = "ux_buttonSaveDataMessage3";
                    _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                    string[] argsArray = new string[100];
                    argsArray[0] = errorCount.ToString();
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                    ggMessageBox.ShowDialog();
                }

                // Restore the state of the AllowUserToAddRows property...
                ux_datagridviewMain.AllowUserToAddRows = origAllowUsersToAddRows;
                // Restore row filter for row errors...
                ux_checkboxHideNonErrorRows.Checked = hideNonErrorRows;
                // If you are here there were errors in some of the changed rows so
                // update the datagridview with the results of the attempted save...
                RefreshMainDGVFormatting();
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }


        private void ScrubData(DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dc.ExtendedProperties.Contains("is_nullable") &&
                            dc.ExtendedProperties["is_nullable"].ToString().Trim().ToUpper() == "N" &&
                            dr.RowState != DataRowState.Deleted &&
                            dr[dc] == DBNull.Value)
                        {
                            if (dc.ExtendedProperties.Contains("default_value") &&
                                !string.IsNullOrEmpty(dc.ExtendedProperties["default_value"].ToString()) &&
                                dc.ExtendedProperties["default_value"].ToString().Trim().ToUpper() != "{DBNULL.VALUE}")
                            {
                                dr[dc] = dc.ExtendedProperties["default_value"].ToString();
                            }
                        }
                    }
                }
            }
        }

        private void ux_buttonCancelEditData_Click(object sender, EventArgs e)
        {
            int intRowEdits = 0;
            bool bCancel = true;

            if (((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).GetChanges() != null)
            {
                intRowEdits = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).GetChanges().Rows.Count;
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have {0} unsaved row change(s), are you sure you want to cancel?", "Cancel Edits", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ux_buttonCancelEditDataMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = intRowEdits.ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                if (DialogResult.No == ggMessageBox.ShowDialog())
                {
                    bCancel = false;
                }
            }
            if (bCancel)
            {
                // Restore the grid view for handling row selections...
                ux_datagridviewMain.Enabled = true;
                ux_datagridviewMain.Focus();
                ux_datagridviewMain.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                ux_datagridviewMain.EditMode = DataGridViewEditMode.EditProgrammatically;
                ux_datagridviewMain.AllowUserToAddRows = false;
                ux_datagridviewMain.AllowUserToDeleteRows = false;
                ux_datagridviewMain.AllowDrop = false;

                // Update the controls on the interface...
                ux_groupboxEditMode.Visible = false;
                ux_checkboxHighlightChanges.Enabled = false;
                ux_checkboxHideNonErrorRows.Checked = false;
                ux_checkboxHideNonErrorRows.Enabled = false;
                ux_checkboxHideUnchangedRows.Checked = false;
                ux_checkboxHideUnchangedRows.Enabled = false;
                ux_checkboxHideCopyPasteWarnings.Enabled = false;
                ux_buttonSaveData.Enabled = false;
                ux_buttonEditData.Enabled = true;
                ux_buttonCancelEditData.Enabled = false;
                ux_splitcontainerMain.Panel1.Enabled = true;
                ux_tabcontrolCTDataviews.Enabled = true;

                // Enable saving user settings when in read-only mode...
                ux_buttonSaveUserSettingsNow.Enabled = true;

                // Now refresh the data and a new copy of the table should be retrieved...
                // Resetting these two global variables will force a refresh of the DGV data...
                lastFullPath = "";
                lastTabName = "";

                // Remember the first visible column to restore later...
                int firstDisplayedCellRowIndex = 0;
                int firstDisplayedCellColumnIndex = 0;
                if (ux_datagridviewMain.FirstDisplayedCell != null)
                {
                    firstDisplayedCellRowIndex = ux_datagridviewMain.FirstDisplayedCell.RowIndex;
                    firstDisplayedCellColumnIndex = ux_datagridviewMain.FirstDisplayedCell.ColumnIndex;
                }
                int currentRowIndex = 0;
                int currentColumnIndex = 0;
                if (ux_datagridviewMain.CurrentCell != null)
                {
                    currentRowIndex = ux_datagridviewMain.CurrentCell.RowIndex;
                    currentColumnIndex = ux_datagridviewMain.CurrentCell.ColumnIndex;
                }

                // Update the formatting for the datagridview cells (to remove highlighting of cells)
                RefreshMainDGVData();
                RefreshMainDGVFormatting();

                // With SelectionMode updated it is now safe to restore the DGV to show the leftmost visible column (this was the leftmost visible column in Read-Only mode)... 
                if (ux_datagridviewMain.Rows.Count > 0)
                {
                    if (firstDisplayedCellRowIndex < 0) firstDisplayedCellRowIndex = 0;
                    if (firstDisplayedCellRowIndex >= ux_datagridviewMain.Rows.Count) firstDisplayedCellRowIndex = ux_datagridviewMain.Rows.Count - 1;
                    if (firstDisplayedCellColumnIndex < 0) firstDisplayedCellColumnIndex = 0;
                    if (firstDisplayedCellColumnIndex >= ux_datagridviewMain.Columns.Count) firstDisplayedCellColumnIndex = ux_datagridviewMain.Columns.Count - 1;
                    if (ux_datagridviewMain[firstDisplayedCellColumnIndex, firstDisplayedCellRowIndex].Visible)
                    {
                        ux_datagridviewMain.FirstDisplayedCell = ux_datagridviewMain[firstDisplayedCellColumnIndex, firstDisplayedCellRowIndex];
                    }
                    if (currentRowIndex < 0) currentRowIndex = 0;
                    if (currentRowIndex >= ux_datagridviewMain.Rows.Count) currentRowIndex = ux_datagridviewMain.Rows.Count - 1;
                    if (currentColumnIndex < 0) currentColumnIndex = 0;
                    if (currentColumnIndex >= ux_datagridviewMain.Columns.Count) currentColumnIndex = ux_datagridviewMain.Columns.Count - 1;
                    if (ux_datagridviewMain[currentColumnIndex, currentRowIndex].Visible)
                    {
                        ux_datagridviewMain.CurrentCell = ux_datagridviewMain[currentColumnIndex, currentRowIndex];
                    }
                }

                // If a Form is being shown, enable its controls...
                if (dataviewForm != null)
                {
                    System.Reflection.PropertyInfo pi = dataviewForm.GetType().GetProperty("EditMode");
                    if (pi != null) pi.SetValue(dataviewForm, false, null);
                }

                // Un-wire the event handler for formatting cells in edit mode (populating the FK lookup values)...
                ux_datagridviewMain.CellFormatting -= new DataGridViewCellFormattingEventHandler(ux_datagridviewMain_EditDGVCellFormatting);
                ux_datagridviewMain.CellParsing -= new DataGridViewCellParsingEventHandler(ux_datagridviewMain_EditDGVCellParsing);
                ux_datagridviewMain.DataError -= new DataGridViewDataErrorEventHandler(dataGridView_DataError);

                // Wire up the event handler for formatting cells in readonly mode...
                ux_datagridviewMain.CellFormatting += new DataGridViewCellFormattingEventHandler(ux_datagridviewMain_ReadOnlyDGVCellFormatting);
            }
        }

        #endregion

        #region DataGridView logic...

        private void ux_datagridviewMain_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else if (dgv.DataSource.GetType() == typeof(DataTable))
            {
                dv = ((DataTable)(dgv.DataSource)).DefaultView;
            }

            // Reset the last char pressed global variable...
            _lastDGVCharPressed = (char)0;

            //  If we are: 
            //  1) in edit mode, 
            //  2) the current cell is parked on a cell that is a FK lookup 
            //  3) and the Alt and Ctrl keys are not down
            // Then remember the keypress so that it can be passed into the Lookup Picker dialog...
            if (ux_buttonEditData.Enabled == false &&
                dv != null &&
                dgv.CurrentCell != null &&
                dgv.CurrentCell.ColumnIndex > -1 &&
                dgv.CurrentCell.RowIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[dgv.CurrentCell.ColumnIndex];
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    dgv.CurrentCell.RowIndex < dv.Count &&
                    dv[dgv.CurrentCell.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (!e.Alt && !e.Control)
                    {
                        KeysConverter kc = new KeysConverter();
                        string lastChar = kc.ConvertToString(e.KeyValue).Replace("NumPad", "");
                        if (lastChar.Length == 1)
                        {
                            if (e.Shift)
                            {
                                _lastDGVCharPressed = lastChar.ToUpper()[0];
                            }
                            else
                            {
                                _lastDGVCharPressed = lastChar.ToLower()[0];
                            }
                        }
                    }
                }
            }
        }

        private void ux_datagridviewMain_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            // Keyboard shortcuts for edit mode...
            if (!ux_buttonEditData.Enabled)
            {
                if (_sharedUtils.ProcessDGVEditShortcutKeys(dgv, e, _usernameCooperatorID))
                {
                    RefreshMainDGVFormatting();
                    // Commented out the RefreshForm() call to prevent the Form from popping up in front of the CT...
                    //RefreshForm();
                }
            }
            else
            {
                if (_sharedUtils.ProcessDGVReadOnlyShortcutKeys(dgv, e, _usernameCooperatorID))
                {
                    // Save the user settings to local memory cache...
                    SetAllUserSettings();
                    RefreshMainDGVFormatting();
                    // The user might have been in cell-select (block) select mode - so reset it to the default row-select mode now...
                    ux_datagridviewMain.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                    // Reset the DGV colors to default (gets rid of red grid color indicating block mode selection)...
                    ux_datagridviewMain.GridColor = Color.FromName(System.Drawing.KnownColor.ControlDark.ToString());
                    e.Handled = true;
                }
            }
        }

        private void ux_datagridviewMain_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void ux_datagridviewMain_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (dgv.Rows[e.RowIndex].IsNewRow && dgv.CurrentCell.GetType() != typeof(DataGridViewCheckBoxCell))
            {
                DataTable dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;
                dgv.DataSource = null;
                dgv.CancelEdit();
                e.Cancel = true;
                // NOTE: If this is the DGV NewRow, then it is not bound to a row in the DataTable, so the following code is a HACK to force a new row into the bound DataTable...
                // BEGIN HACK...
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
                dgv.DataSource = dt;
                dgv.CurrentCell = dgv[e.ColumnIndex, e.RowIndex];
                //dt.Rows.Remove(dr);
                // END HACK...
            }
        }

        private void ux_datagridviewMain_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataRow dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
            if (!dgv.CurrentRow.IsNewRow)
            {
                if (dr.GetColumnsInError().Contains(dr.Table.Columns[e.ColumnIndex]) && dr[e.ColumnIndex] != DBNull.Value) dr.SetColumnError(e.ColumnIndex, null);
            }
            dgv.EndEdit();
            dr.EndEdit();
            RefreshDGVRowFormatting(dgv.CurrentRow, ux_checkboxHighlightChanges.Checked);
        }

        private void ux_datagridviewMain_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataTable dt = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;
            }
            else
            {
                dt = (DataTable)dgv.DataSource;
            }
            string columnName = dgv.CurrentCell.OwningColumn.Name;
            DataColumn dc = dt.Columns[columnName];
            DataRow dr;

            if (_sharedUtils.LookupTablesIsValidFKField(dc))
            {
                string luTableName = dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim();
                dr = ((DataRowView)dgv.CurrentRow.DataBoundItem).Row;
                string suggestedFilter = dgv.CurrentCell.EditedFormattedValue.ToString();
                if (_lastDGVCharPressed > 0) suggestedFilter = _lastDGVCharPressed.ToString();
                GRINGlobal.Client.Common.LookupTablePicker ltp = new GRINGlobal.Client.Common.LookupTablePicker(_sharedUtils, columnName, dr, suggestedFilter);
                _lastDGVCharPressed = (char)0;
                ltp.StartPosition = FormStartPosition.CenterParent;
                if (DialogResult.OK == ltp.ShowDialog())
                {
                    if (dr != null)
                    {
                        if (ltp.NewKey != null && dr[dgv.CurrentCell.ColumnIndex].ToString().Trim() != ltp.NewKey.Trim())
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = ltp.NewKey.Trim();
                            dgv.CurrentCell.Value = ltp.NewValue.Trim();
                        }
                        else if (ltp.NewKey == null)
                        {
                            dr[dgv.CurrentCell.ColumnIndex] = DBNull.Value;
                            dgv.CurrentCell.Value = "";
                        }
                        dr.SetColumnError(dgv.CurrentCell.ColumnIndex, null);
                    }
                }
                dgv.EndEdit();
            }
        }

        void dataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            // Display a warning message to the user that invalid data is entered for integer and datetime fields (but not FK lookups)...
            if ((dgv[e.ColumnIndex, e.RowIndex].ValueType == typeof(int) ||
                dgv[e.ColumnIndex, e.RowIndex].ValueType == typeof(DateTime)) &&
                !string.IsNullOrEmpty(dgv[e.ColumnIndex, e.RowIndex].OwningColumn.DataPropertyName))
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox(e.Exception.Message, "Cancel Edits", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "dataGridView_DataErrorMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                ggMessageBox.ShowDialog();
            }
        }

        void ux_datagridviewMain_EditDGVCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            // Check to see if this cell is in a column that needs a FK lookup...
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }
            if (dv != null && 
                e.ColumnIndex > -1 && 
                e.ColumnIndex < dv.Table.Columns.Count  &&
                e.RowIndex > -1 &&
                e.RowIndex < dv.Count)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                if (_sharedUtils.LookupTablesIsValidFKField(dc) &&
                    e.RowIndex < dv.Count &&
                    dv[e.RowIndex].Row.RowState != DataRowState.Deleted)
                {
                    if (dv[e.RowIndex][e.ColumnIndex] != DBNull.Value)
                    {
                        e.Value = _sharedUtils.GetLookupDisplayMember(dc.ExtendedProperties["foreign_key_dataview_name"].ToString().Trim(), dv[e.RowIndex][e.ColumnIndex].ToString().Trim(), "", dv[e.RowIndex][e.ColumnIndex].ToString().Trim());
                    }
                    dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    e.FormattingApplied = true;
                }
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        e.Value = ((DateTime)dv[e.RowIndex][e.ColumnIndex]).ToString(dateFormat);
                        e.FormattingApplied = true;
                    }
                }
                else if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    (dc.DataType == typeof(int) ||
                    dc.DataType == typeof(Int16) ||
                    dc.DataType == typeof(Int32) ||
                    dc.DataType == typeof(Int64)))
                {
                    int junk;
                    if (!int.TryParse(e.Value.ToString(), out junk))
                    {
                        dgv[e.ColumnIndex, e.RowIndex].ErrorText = dv[e.RowIndex].Row.GetColumnError(dc);
                    }
                }

                if (dc.ReadOnly)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }
                else if (dv.Table.Columns[e.ColumnIndex].ExtendedProperties.Contains("is_nullable") &&
                    dv.Table.Columns[e.ColumnIndex].ExtendedProperties["is_nullable"].ToString() == "N" &&
                    !dv.Table.Columns[e.ColumnIndex].ColumnName.StartsWith("is_") &&
                    dv[e.RowIndex][e.ColumnIndex] == DBNull.Value)
                {
                    e.CellStyle.BackColor = Color.Plum;
                }
            }
        }

        void ux_datagridviewMain_EditDGVCellParsing(object sender, DataGridViewCellParsingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            DataView dv = null;
            if (dgv.DataSource.GetType() == typeof(BindingSource))
            {
                dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            }
            else
            {
                dv = ((DataTable)dgv.DataSource).DefaultView;
            }
            if (dv != null && e.ColumnIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                if (e.Value != null &&
                    e.Value != DBNull.Value &&
                    dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormat = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        DateTime formattedDate;
                        if (DateTime.TryParseExact(e.Value.ToString(), dateFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out formattedDate) ||
                            DateTime.TryParse(e.Value.ToString(), out formattedDate))
                        {
                            e.Value = formattedDate;
                            e.ParsingApplied = true;
                        }
                    }
                }
            }
        }

        void ux_datagridviewMain_ReadOnlyDGVCellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            DataView dv = ((DataTable)((BindingSource)dgv.DataSource).DataSource).DefaultView;
            if (dv != null && e.ColumnIndex > -1)
            {
                DataColumn dc = dv.Table.Columns[e.ColumnIndex];
                // Format the date fields if a format code is available...
                if (dc.DataType == typeof(DateTime))
                {
                    if (dgv.Columns.Contains(dc.ColumnName + "_code"))
                    {
                        string dateFormatDisplayMember = dv[e.RowIndex][dc.ColumnName + "_code"].ToString().Trim();
                        string dateFormatValueMember = "";
                        if (!string.IsNullOrEmpty(dateFormatDisplayMember)) dateFormatValueMember = _sharedUtils.GetLookupValueMember(dv[e.RowIndex].Row, "code_value_lookup", dateFormatDisplayMember, dv.Table.Columns[dc.ColumnName + "_code"].ExtendedProperties["group_name"].ToString(), "");
                        if (e.Value != null &&
                            e.Value != DBNull.Value)
                        {
                            e.Value = ((DateTime)dv[e.RowIndex][e.ColumnIndex]).ToString(dateFormatValueMember);
                            e.FormattingApplied = true;
                        }
                    }
                }

                // Show read-only columns as grayed out...
                if (dc.ReadOnly)
                {
                    e.CellStyle.BackColor = Color.LightGray;
                }

                // Show the current cell in the read-only DGV (aka the cell with focus in the full-row selected)...
                if (dgv.CurrentCell != null &&
                   e.ColumnIndex == dgv.CurrentCell.ColumnIndex &&
                   e.RowIndex == dgv.CurrentCell.RowIndex)
                {
                    if (_dgvcFindResults.Count > 0  &&
                        _dgvcFindResults.Contains(dgv.CurrentCell))
                    {
                        e.CellStyle.SelectionBackColor = Color.Gold;
                        e.CellStyle.SelectionForeColor = Color.Black;
                    }
                    else
                    {
                        e.CellStyle.SelectionBackColor = Color.LightSkyBlue;
                        e.CellStyle.SelectionForeColor = Color.Black;
                    }
                }
            }
        }

        private void ux_datagridviewMain_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (ux_checkboxHotSyncTreeview.Checked)
            {
                DataGridView dgv = (DataGridView)sender;

                // If this is the new row - format the cells to give the user visual cues about what is a required field...
                if (dgv.Rows[e.RowIndex].IsNewRow)
                {
                    RefreshDGVRowFormatting(dgv.Rows[e.RowIndex], ux_checkboxHighlightChanges.Checked);
                }

                if (!dgv.Rows[e.RowIndex].IsNewRow &&
                    _ux_NavigatorTabControl.SelectedTab != null &&
                    _ux_NavigatorTabControl.SelectedTab.Name != "ux_tabpageGroupListNavigatorNewTab")
                {
                    DataTable dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;
                    TreeView tv = (TreeView)_ux_NavigatorTabControl.SelectedTab.Controls[0];
                    TreeNode[] linkedNodes;
                    DataRowView drv = ((DataRowView)dgv[e.ColumnIndex, e.RowIndex].OwningRow.DataBoundItem);

                    // Reset each node to the default font for the control...
                    ResetTreeviewNodeFormatting(tv.Nodes);

                    // Get a 'raw' copy of the active row (so we have easy access to the FKeys)...
                    if (grinData.Tables.Contains(dt.TableName) && grinData.Tables[dt.TableName].Rows.Contains(drv[dt.PrimaryKey[0].ColumnName]))
                    {
                        DataRow rawDR = grinData.Tables[dt.TableName].NewRow();
                        rawDR.ItemArray = grinData.Tables[dt.TableName].Rows.Find(drv[dt.PrimaryKey[0].ColumnName]).ItemArray;
                        // Iterate through each column in the datatable to see if any columns point at items in the treeview...
                        foreach (DataColumn dc in rawDR.Table.Columns)
                        {
                            if (dc.ExtendedProperties["is_primary_key"].ToString().Trim().ToUpper() == "Y")
                            {
                                linkedNodes = tv.SelectedNode.Nodes.Find(":" + dc.ColumnName.Replace("_", "") + "=" + rawDR[dc.ColumnName].ToString(), true);
                                foreach (TreeNode tn in linkedNodes)
                                {
                                    // Get the font used by the TreeView because the NodeFont property is set to the default null...
                                    Font boldFont = new Font(tv.Font, FontStyle.Bold);
                                    // Set the font for each node...
                                    tn.NodeFont = boldFont;
                                    // KLUDGE: Set the node text to itself to force a recalc of the clipping rectangle (otherwise some
                                    // of the text could get clipped because the new font takes more space than the original font for the same text)...
                                    tn.Text = tn.Text;
                                }
                            }
                            else if (dc.ExtendedProperties["is_foreign_key"].ToString().Trim().ToUpper() == "Y")
                            {
                                linkedNodes = tv.SelectedNode.Nodes.Find(":" + dc.ColumnName.Replace("_", "") + "=" + rawDR[dc.ColumnName].ToString(), true);
                                foreach (TreeNode tn in linkedNodes)
                                {
                                    // Get the font used by the TreeView because the NodeFont property is set to the default null...
                                    Font italicFont = new Font(tv.Font, FontStyle.Italic | FontStyle.Underline);
                                    // Set the font for each node...
                                    tn.NodeFont = italicFont;
                                    // KLUDGE: Set the node text to itself to force a recalc of the clipping rectangle (otherwise some
                                    // of the text could get clipped because the new font takes more space than the original font for the same text)...
                                    tn.Text = tn.Text;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ux_datagridviewMain_RowLeave(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            // If this is the new row reset the format of the cells to give the user visual cues that they are no longer editing the new row...
            if (dgv.Rows[e.RowIndex].IsNewRow)
            {
                // Reset the rows cells to the default color scheme...
                foreach (DataGridViewCell dgvc in dgv.Rows[e.RowIndex].Cells)
                {
                    dgvc.Style.BackColor = Color.Empty;
                    dgvc.Style.SelectionBackColor = Color.Empty;
                }
            }
        }

        private void ux_datagridviewMain_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
        }
        
        private void ux_datagridviewMain_Leave(object sender, EventArgs e)
        {
            // Remember all user settings...
            if (ux_buttonEditData.Enabled)
            {
                SetAllUserSettings();
            }
        }

        private void ux_datagridviewMain_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            // Change the cursor to provide a visual cue about what kind of cell the mouse is hovering over...
            if (ux_checkboxEnableCursorVisualCues.Checked)
            {
                if (!ux_buttonEditData.Enabled && (e.RowIndex > -1) && (e.ColumnIndex > -1))
                {
                    DataTable dt = null;
                    if (ux_datagridviewMain.DataSource.GetType() == typeof(BindingSource) &&
                        ((BindingSource)ux_datagridviewMain.DataSource).DataSource.GetType() == typeof(DataTable))
                    {
                        dt = (DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource;
                    }
                    else if (ux_datagridviewMain.DataSource.GetType() == typeof(DataTable))
                    {
                        dt = (DataTable)ux_datagridviewMain.DataSource;
                    }
                    if (dt != null &&
                        !dt.Columns[e.ColumnIndex].ReadOnly &&
                        dt.Columns[e.ColumnIndex].ExtendedProperties.Contains("gui_hint") &&
                        dt.Columns[e.ColumnIndex].ExtendedProperties["gui_hint"].ToString() == "LARGE_SINGLE_SELECT_CONTROL")
                    {
                        ux_datagridviewMain.Cursor = _cursorLUT;
                    }
                    else
                    {
                        ux_datagridviewMain.Cursor = _cursorGG;
                    }
                }
                else if (ux_datagridviewMain.Cursor == _cursorLUT && (e.RowIndex > -1) && (e.ColumnIndex > -1))
                {
                    ux_datagridviewMain.Cursor = _cursorGG;
                }
            }
            else
            {
                ux_datagridviewMain.Cursor = Cursors.Default;
            }
        }

        private void ux_datagridviewMain_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {            
            DataObject doDragDropData;

            if ((e.RowIndex > -1) && (e.ColumnIndex > -1) && (e.Button == MouseButtons.Left) &&
                ((ux_datagridviewMain.SelectionMode == DataGridViewSelectionMode.FullRowSelect && ux_datagridviewMain.SelectedRows.Contains(ux_datagridviewMain.Rows[e.RowIndex]) && ux_datagridviewMain.SelectedRows.Count > 0) ||
                 (ux_datagridviewMain.SelectionMode == DataGridViewSelectionMode.RowHeaderSelect && ux_datagridviewMain.SelectedRows.Contains(ux_datagridviewMain.Rows[e.RowIndex]) && ux_datagridviewMain.SelectedRows.Count > 0) ||
                 (ux_datagridviewMain.SelectionMode == DataGridViewSelectionMode.CellSelect && ux_datagridviewMain.SelectedCells.Contains(ux_datagridviewMain[e.ColumnIndex, e.RowIndex]) && ux_datagridviewMain.SelectedCells.Count > 0) ||
                 (ux_datagridviewMain.SelectionMode == DataGridViewSelectionMode.RowHeaderSelect && ux_datagridviewMain.SelectedCells.Contains(ux_datagridviewMain[e.ColumnIndex, e.RowIndex]) && ux_datagridviewMain.SelectedCells.Count > 1)))
            {
                // Get the string of selected rows extracted from the GridView...
                doDragDropData = BuildDragAndDropDGVData(ux_datagridviewMain);
                ux_datagridviewMain.DoDragDrop(doDragDropData, DragDropEffects.Copy);
            }
        }

        private void ux_datagridviewMain_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
        }

        private void ux_datagridviewMain_DragOver(object sender, DragEventArgs e)
        {
            // Okay we are in the middle of a Drag and Drop operation and the mouse is in 
            // the DGV control so lets handle this event...

            // This code will change the cursor icon to give the user feedback about whether or not
            // the drag-drop operation is allowed...
            //

            // Get the DGV object...
            DataGridView dgv = (DataGridView)sender;

            // Convert the mouse coordinates from screen to client...
            Point ptClientCoord = dgv.PointToClient(new Point(e.X, e.Y));

            // Is this a string being dragged to a node...
            if (e.Data.GetDataPresent("System.String") && !dgv.ReadOnly)
            {
                e.Effect = DragDropEffects.Copy;
            }
            else if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ux_datagridviewMain_DragDrop(object sender, DragEventArgs e)
        {
            // The drag-drop event is coming to a close process this event to handle the dropping of
            // data into the treeview...

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Get the DGV object...
            DataGridView dgv = (DataGridView)sender;

            // Is this an allowed drop???
            if (e.Effect != DragDropEffects.None)
            {
                // Is this a collection of dataset rows being dragged to the DGV...
                if (e.Data.GetDataPresent("System.String"))
                {
                    char[] rowDelimiters = new char[] { '\r', '\n' };
                    char[] columnDelimiters = new char[] { '\t' };
                    string rawText = (string)e.Data.GetData("System.String");
                    DataTable dt = (DataTable)((BindingSource)dgv.DataSource).DataSource;
                    int badRows = 0;
                    int missingRows = 0;
                    bool importSuccess = false;
                    importSuccess = _sharedUtils.ImportTextToDataTableUsingKeys(rawText, dt, rowDelimiters, columnDelimiters, out badRows, out missingRows);
                    if (!importSuccess)
                    {
                        Point clientCoord = ux_datagridviewMain.PointToClient(new Point(e.X, e.Y));
                        System.Windows.Forms.DataGridView.HitTestInfo hti = ux_datagridviewMain.HitTest(clientCoord.X, clientCoord.Y);
                        ux_datagridviewMain.CurrentCell = ux_datagridviewMain[hti.ColumnIndex, hti.RowIndex];
                        importSuccess = _sharedUtils.ImportTextToDataTableUsingBlockStyle(rawText, dgv, rowDelimiters, columnDelimiters, out badRows, out missingRows);
                    }

                    RefreshMainDGVFormatting();

                }
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        public DataObject BuildDragAndDropDGVData(DataGridView dgv)
        {
            DataObject doReturn = new DataObject();

            // Write the rows out in text format first...
            string copyString = _sharedUtils.BuildClipboardString(dgv, ux_buttonEditData.Enabled, (System.Windows.Forms.Control.ModifierKeys & Keys.Control) != Keys.Control, ux_checkboxStripSpecialCharactersOnExport.Checked);

            // Now write it out as a collection of data table rows (for tree view drag and drop).
            DataSet dsData = new DataSet();
            dsData.Tables.Add(((DataTable)((BindingSource)dgv.DataSource).DataSource).Clone());
            foreach (DataGridViewRow dgvr in dgv.SelectedRows)
            {
                if (!dgvr.IsNewRow) dsData.Tables[0].Rows.Add(((DataRowView)dgvr.DataBoundItem).Row.ItemArray);
            }

            // Set the data types into the data object and return...
            doReturn.SetData(typeof(string), copyString);
            doReturn.SetData(typeof(DataSet), dsData);

            return doReturn;
        }

        #endregion

        #region DGV cell context menu logic...

        private void ux_datagridviewMain_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            // Set the global variables so that later processing will know where to apply the command from the context menu...
            mouseClickDGVColumnIndex = e.ColumnIndex;
            mouseClickDGVRowIndex = e.RowIndex;

            // Change the color of the cell background so that the user
            // knows what cell the context menu applies to...
            if (e.RowIndex > -1 && e.ColumnIndex > -1)
            {
                ux_datagridviewMain.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Red;
                ux_datagridviewMain.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionBackColor = Color.Red;
            }

            // Since the event handler is called for both column header cells and DGV cells we need to proccess them separately...
            if (e.RowIndex > -1)
            {
                // DGV cell processing...
                // Clear the reports tool strip menu items and get a fresh list from the Reports directory...
                ux_dgvcellmenuReports.DropDownItems.Clear();

                // There are lots of ways to get the Reports directory location (the cleanest is to find the CT directory and go from there)...
                // So... Here are some of the better ways to find the CT's EXE path:
                //      System.Environment.GetFolderPath(System.Reflection.Assembly.GetExecutingAssembly().Location)
                //      System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
                //      System.Windows.Forms.Application.StartupPath
                //      System.IO.Directory.GetCurrentDirectory()  <-- this one changes if the user saves an export of a report from CR Viewer

                //System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.Windows.Forms.Application.StartupPath + "\\Reports");
                System.Collections.Generic.Dictionary<string, string> reportsMap = _sharedUtils.GetReportsMapping();
                string dataviewName = ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName.Trim().ToUpper();
                //foreach (System.IO.FileInfo fi in di.GetFiles("*.*", System.IO.SearchOption.AllDirectories))
                //{
                //    if (reportsMap.ContainsKey(fi.Name.Trim().ToUpper()) &&
                //        reportsMap[fi.Name.Trim().ToUpper()].Contains(" " + dataviewName + " "))
                //    {
                //        ToolStripItem tsi = ux_dgvcellmenuReports.DropDownItems.Add(fi.Name, null, ux_DGVCellReport_Click);
                //        tsi.Tag = fi.FullName;
                //    }
                //}
                foreach(string key in reportsMap.Keys)
                {
                    if(reportsMap[key].Contains(" " + dataviewName + " ") &&
                        System.IO.File.Exists(key))
                    {
                                ToolStripItem tsi = ux_dgvcellmenuReports.DropDownItems.Add(System.IO.Path.GetFileName(key), null, ux_DGVCellReport_Click);
                                tsi.Tag = key;
                    }
                }
                if (ux_dgvcellmenuReports.DropDownItems.Count > 0)
                {
                    ux_dgvcellmenuReports.Enabled = true;
                }
                else
                {
                    ux_dgvcellmenuReports.Enabled = false;
                }
                // Enable/Disable the Change Ownership menu selection based on if the user owns all of the rows...
                bool userOwnsAllRows = false;
                string cooperator = _usernameCooperatorID;
                // If the DGV is in read-only mode use the display value - not the cooperator_id for comparison...
                if (ux_buttonEditData.Enabled)
                {
                    cooperator = _sharedUtils.GetLookupDisplayMember("cooperator_lookup", _usernameCooperatorID, "", _usernameCooperatorID);
                }
                // Now iterate through the selected rows to make sure the current user owns all of them...
                if (ux_datagridviewMain.Columns.Contains("owned_by"))
                {
                    // Assume the user owns all the rows until proven otherwise...
                    userOwnsAllRows = true;
                    foreach (DataGridViewRow dgvr in ux_datagridviewMain.SelectedRows)
                    {
                        if (((DataRowView)dgvr.DataBoundItem).Row["owned_by"].ToString() != cooperator) userOwnsAllRows = false;
                    }
                }
                // If the user owns all of the selected rows enable the change owner menu item..
                ux_dgvcellmenuChangeOwner.Enabled = userOwnsAllRows;
                // If the user is an admin or site manager - enable the change owner menu item...
                string[] userGroups = _sharedUtils.UserGroups.ToUpper().Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                if (userGroups.Contains("ADMINS")) ux_dgvcellmenuChangeOwner.Enabled = true;
                if (userGroups.Contains("MANAGE_SITE_" + _sharedUtils.UserSite.ToUpper())) ux_dgvcellmenuChangeOwner.Enabled = true;
            }
            else
            {
                // Column header processing...
                return;
            }
        }

        private void ux_dgvcellmenuSecurityWizard_Click(object sender, EventArgs e)
        {
            // Get the PKey name and the table name for the currently active datagridview...
            string pkeyColumnName = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).PrimaryKey[0].ColumnName;
            string tableName = "";
            if (((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).PrimaryKey[0].ExtendedProperties.Contains("table_name") &&
                !string.IsNullOrEmpty(((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).PrimaryKey[0].ExtendedProperties["table_name"].ToString()))
            {
                tableName = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).PrimaryKey[0].ExtendedProperties["table_name"].ToString();
            }
            else
            {
                tableName = pkeyColumnName.Remove(pkeyColumnName.LastIndexOf("_id"));
            }

            // Gather up the PKeys in the datagridview currently selected by the user...
            string pkeyCollection = "";
            foreach (DataGridViewRow dgvr in ux_datagridviewMain.SelectedRows)
            {
                pkeyCollection += dgvr.Cells[pkeyColumnName].Value + ",";
            }

            // Launch the Security Wizard...
            SecurityWizard newSecurityWizardDialog = new SecurityWizard(tableName, pkeyColumnName, pkeyCollection.TrimEnd(','), _sharedUtils);
            if (newSecurityWizardDialog.ShowDialog(this) == DialogResult.OK)
            {
            }
            else
            {
            }

            // Refresh the main DGV's formatting...
            RefreshMainDGVFormatting();
        }

        private void ux_dgvcellmenuChangeOwner_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).Clone());
            foreach(DataGridViewRow dgvr in ux_datagridviewMain.SelectedRows)
            {
                ds.Tables[0].LoadDataRow(((DataRowView)dgvr.DataBoundItem).Row.ItemArray, LoadOption.Upsert);
            }
            GRINGlobal.Client.Common.ChangeOwnership newChangeOwnershipDialog = new GRINGlobal.Client.Common.ChangeOwnership(ds, _sharedUtils);
            if (newChangeOwnershipDialog.ShowDialog(this) == DialogResult.OK)
            {
                ux_buttonRefreshData.PerformClick();
            }
            else
            {
            }
        }

        private void ux_contextmenustripDGVCell_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
        }

        private void ux_dgvcellmenuShowOnlyRowsWithThisDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string DataGridViewFilter = "";
            string ColName = "Error!";
            string CellValue = "Error!";

            if ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource != null)
            {
                DataGridViewFilter = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.RowFilter;

                if (mouseClickDGVColumnIndex >= 0 && mouseClickDGVRowIndex >= 0)
                {
                    // Get the column name for the bound datatable (NOTE: convert the dbnulls to '' using the isnull(,) function so that they are filtered properly)...
                    ColName = "convert(isnull(" + ux_datagridviewMain.Columns[mouseClickDGVColumnIndex].Name + ", ''), 'System.String')";
                    CellValue = "'" + ux_datagridviewMain[mouseClickDGVColumnIndex, mouseClickDGVRowIndex].Value.ToString().Replace("'", "''") + "'";
                    if (DataGridViewFilter.Length > 0)
                    {
                        DataGridViewFilter += " AND " + ColName + " = " + CellValue;
                    }
                    else
                    {
                        DataGridViewFilter = ColName + " = " + CellValue;
                    }
                }
                // Set the row filter for the table currently viewed in the DGV (this is a copy of the grinData original)...
                ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.RowFilter = DataGridViewFilter;
                // Save the users settings (which may or may not include this filter)...
                SetAllUserSettings();
                // Refresh the formatting for the datagridview to show the sort glyphs...
                RefreshMainDGVFormatting();
            }
        }

        private void ux_dgvcellmenuHideRowsWithThisDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string DataGridViewFilter = "";
            string ColName = "Error!";
            string CellValue = "Error!";

            if ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource != null)
            {
                DataGridViewFilter = ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.RowFilter;
                if (mouseClickDGVColumnIndex >= 0 && mouseClickDGVRowIndex >= 0)
                {
                    // Get the column name for the bound datatable (NOTE: convert the dbnulls to '' using the isnull(,) function so that they are filtered properly)...
                    ColName = "convert(isnull(" + ux_datagridviewMain.Columns[mouseClickDGVColumnIndex].Name + ", ''), 'System.String')";
                    CellValue = "'" + ux_datagridviewMain[mouseClickDGVColumnIndex, mouseClickDGVRowIndex].Value.ToString().Replace("'", "''") + "'";
                    if (DataGridViewFilter.Length > 0)
                    {
                        DataGridViewFilter += " AND " + ColName + " <> " + CellValue;
                    }
                    else
                    {
                        DataGridViewFilter = ColName + " <> " + CellValue;
                    }
                }
                // Set the row filter for the table currently viewed in the DGV (this is a copy of the grinData original)...
                ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.RowFilter = DataGridViewFilter;
                // Save the users settings (which may or may not include this filter)...
                SetAllUserSettings();
                // Refresh the formatting for the datagridview to show the sort glyphs...
                RefreshMainDGVFormatting();
            }
        }

        private void ux_dgvcellmenuResetRowFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.RowFilter = "";
            SetAllUserSettings();
            // Refresh the formatting for the datagridview to show the sort glyphs...
            RefreshMainDGVFormatting();
        }

        private void sortAscendingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fieldName = "";
            string sortOrder = _sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "DefaultView.Sort", "");

            fieldName = ux_datagridviewMain.Columns[mouseClickDGVColumnIndex].Name;
            if (sortOrder.Contains(fieldName + " DESC"))
            {
                sortOrder = sortOrder.Replace(fieldName + " DESC", fieldName + " ASC");
            }
            else if (!sortOrder.Contains(fieldName + " ASC"))
            {
                if (sortOrder.Length > 0)
                {
                    sortOrder += "," + fieldName + " ASC";
                }
                else
                {
                    sortOrder += fieldName + " ASC";
                }
            }
            sortOrder = sortOrder.Replace(",,", ",");

            // Save the new sort order to userSettings...
            ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.Sort = sortOrder;
            SetAllUserSettings();
            RefreshMainDGVFormatting();
        }

        private void sortDescendingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fieldName = "";
            string sortOrder = _sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "DefaultView.Sort", "");

            fieldName = ux_datagridviewMain.Columns[mouseClickDGVColumnIndex].Name;
            if (sortOrder.Contains(fieldName + " ASC"))
            {
                sortOrder = sortOrder.Replace(fieldName + " ASC", fieldName + " DESC");
            }
            else if (!sortOrder.Contains(fieldName + " DESC"))
            {
                if (sortOrder.Length > 0)
                {
                    sortOrder += "," + fieldName + " DESC";
                }
                else
                {
                    sortOrder += fieldName + " DESC";
                }
            }
            sortOrder = sortOrder.Replace(",,", ",");

            // Save the new sort order to userSettings...
            ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.Sort = sortOrder;
            SetAllUserSettings();
            RefreshMainDGVFormatting();
        }

        private void noSortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fieldName = "";
            string sortOrder = _sharedUtils.GetUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "DefaultView.Sort", "");

            fieldName = ux_datagridviewMain.Columns[mouseClickDGVColumnIndex].Name;
            if (sortOrder.Contains(fieldName + " DESC"))
            {
                sortOrder = sortOrder.Replace(fieldName + " DESC", "");
            }
            else if (sortOrder.Contains(fieldName + " ASC"))
            {
                sortOrder = sortOrder.Replace(fieldName + " ASC", "");
            }
            // Remove all commas at the start and end of the string...
            sortOrder = sortOrder.TrimStart(',').TrimEnd(',');
            // Remove any double commas from the string...
            sortOrder = sortOrder.Replace(",,", ",");

            // Save the new sort order to userSettings...
            ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.Sort = sortOrder;
            SetAllUserSettings();
            RefreshMainDGVFormatting();
        }

        private void resetAllSortingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string sortOrder = "";
            // Save the new sort order to userSettings...
            _sharedUtils.SaveUserSetting("", ((DataviewProperties)ux_tabcontrolCTDataviews.SelectedTab.Tag).DataviewName, "DefaultView.Sort", sortOrder);
            ((DataTable)((BindingSource)ux_datagridviewMain.DataSource).DataSource).DefaultView.Sort = sortOrder;
            SetAllUserSettings();
            RefreshMainDGVFormatting();
        }

        private void ux_dgvcellmenuEditCopy_Click(object sender, EventArgs e)
        {
            if (!ux_datagridviewMain.SelectedCells.Contains(ux_datagridviewMain[mouseClickDGVColumnIndex, mouseClickDGVRowIndex]))
            {
                ux_datagridviewMain.CurrentCell = ux_datagridviewMain[mouseClickDGVColumnIndex, mouseClickDGVRowIndex];
            }
            KeyEventArgs sendKeys = new KeyEventArgs(Keys.C | Keys.Control);
            ux_datagridviewMain_KeyDown(ux_datagridviewMain, sendKeys);
        }

        private void ux_dgvcellmenuEditPaste_Click(object sender, EventArgs e)
        {
            if (!ux_datagridviewMain.SelectedCells.Contains(ux_datagridviewMain[mouseClickDGVColumnIndex, mouseClickDGVRowIndex]))
            {
                ux_datagridviewMain.CurrentCell = ux_datagridviewMain[mouseClickDGVColumnIndex, mouseClickDGVRowIndex];
            }
            KeyEventArgs sendKeys = new KeyEventArgs(Keys.V | Keys.Control);
            ux_datagridviewMain_KeyDown(ux_datagridviewMain, sendKeys);
        }

        private void ux_dgvcellmenuEditDelete_Click(object sender, EventArgs e)
        {
            if (!ux_datagridviewMain.SelectedCells.Contains(ux_datagridviewMain[mouseClickDGVColumnIndex, mouseClickDGVRowIndex]))
            {
                ux_datagridviewMain.CurrentCell = ux_datagridviewMain[mouseClickDGVColumnIndex, mouseClickDGVRowIndex];
            }
            KeyEventArgs sendKeys = new KeyEventArgs(Keys.Delete);
            ux_datagridviewMain_KeyDown(ux_datagridviewMain, sendKeys);
        }

        #endregion
    }
}