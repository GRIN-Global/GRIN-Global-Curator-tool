﻿
namespace GRINGlobal.Client
{
    partial class KeyboardMapper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_comboboxFunction = new System.Windows.Forms.ComboBox();
            this.ux_labelFunction = new System.Windows.Forms.Label();
            this.ux_labelKeyboardShortcut = new System.Windows.Forms.Label();
            this.ux_buttonSave = new System.Windows.Forms.Button();
            this.ux_buttonCancel = new System.Windows.Forms.Button();
            this.ux_buttonReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ux_comboboxFunction
            // 
            this.ux_comboboxFunction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_comboboxFunction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ux_comboboxFunction.FormattingEnabled = true;
            this.ux_comboboxFunction.Location = new System.Drawing.Point(12, 29);
            this.ux_comboboxFunction.Name = "ux_comboboxFunction";
            this.ux_comboboxFunction.Size = new System.Drawing.Size(325, 21);
            this.ux_comboboxFunction.TabIndex = 0;
            this.ux_comboboxFunction.SelectedIndexChanged += new System.EventHandler(this.ux_comboboxFunction_SelectedIndexChanged);
            // 
            // ux_labelFunction
            // 
            this.ux_labelFunction.AutoSize = true;
            this.ux_labelFunction.Location = new System.Drawing.Point(13, 13);
            this.ux_labelFunction.Name = "ux_labelFunction";
            this.ux_labelFunction.Size = new System.Drawing.Size(51, 13);
            this.ux_labelFunction.TabIndex = 1;
            this.ux_labelFunction.Text = "Function:";
            // 
            // ux_labelKeyboardShortcut
            // 
            this.ux_labelKeyboardShortcut.AutoSize = true;
            this.ux_labelKeyboardShortcut.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ux_labelKeyboardShortcut.Location = new System.Drawing.Point(16, 57);
            this.ux_labelKeyboardShortcut.Name = "ux_labelKeyboardShortcut";
            this.ux_labelKeyboardShortcut.Size = new System.Drawing.Size(0, 24);
            this.ux_labelKeyboardShortcut.TabIndex = 2;
            // 
            // ux_buttonSave
            // 
            this.ux_buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSave.Location = new System.Drawing.Point(200, 87);
            this.ux_buttonSave.Name = "ux_buttonSave";
            this.ux_buttonSave.Size = new System.Drawing.Size(96, 23);
            this.ux_buttonSave.TabIndex = 3;
            this.ux_buttonSave.Text = "Save and Exit";
            this.ux_buttonSave.UseVisualStyleBackColor = true;
            this.ux_buttonSave.Click += new System.EventHandler(this.ux_buttonSave_Click);
            // 
            // ux_buttonCancel
            // 
            this.ux_buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCancel.Location = new System.Drawing.Point(302, 87);
            this.ux_buttonCancel.Name = "ux_buttonCancel";
            this.ux_buttonCancel.Size = new System.Drawing.Size(96, 23);
            this.ux_buttonCancel.TabIndex = 4;
            this.ux_buttonCancel.Text = "Cancel";
            this.ux_buttonCancel.UseVisualStyleBackColor = true;
            this.ux_buttonCancel.Click += new System.EventHandler(this.ux_buttonCancel_Click);
            // 
            // ux_buttonReset
            // 
            this.ux_buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonReset.Location = new System.Drawing.Point(343, 27);
            this.ux_buttonReset.Name = "ux_buttonReset";
            this.ux_buttonReset.Size = new System.Drawing.Size(55, 23);
            this.ux_buttonReset.TabIndex = 5;
            this.ux_buttonReset.Text = "Reset";
            this.ux_buttonReset.UseVisualStyleBackColor = true;
            this.ux_buttonReset.Click += new System.EventHandler(this.ux_buttonReset_Click);
            // 
            // KeyboardMapper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 121);
            this.Controls.Add(this.ux_buttonReset);
            this.Controls.Add(this.ux_buttonCancel);
            this.Controls.Add(this.ux_buttonSave);
            this.Controls.Add(this.ux_labelKeyboardShortcut);
            this.Controls.Add(this.ux_labelFunction);
            this.Controls.Add(this.ux_comboboxFunction);
            this.KeyPreview = true;
            this.Name = "KeyboardMapper";
            this.Text = "Keyboard Mapper";
            this.Load += new System.EventHandler(this.KeyboardMapper_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyboardMapper_KeyDown);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.KeyboardMapper_PreviewKeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ux_comboboxFunction;
        private System.Windows.Forms.Label ux_labelFunction;
        private System.Windows.Forms.Label ux_labelKeyboardShortcut;
        private System.Windows.Forms.Button ux_buttonSave;
        private System.Windows.Forms.Button ux_buttonCancel;
        private System.Windows.Forms.Button ux_buttonReset;
    }
}