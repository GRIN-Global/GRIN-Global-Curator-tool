﻿
namespace GRINGlobal.Client.Common
{
    partial class SQLServerReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_reportviewerMain = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // ux_reportviewerMain
            // 
            this.ux_reportviewerMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_reportviewerMain.Location = new System.Drawing.Point(1, 1);
            this.ux_reportviewerMain.Name = "ux_reportviewerMain";
            this.ux_reportviewerMain.ServerReport.BearerToken = null;
            this.ux_reportviewerMain.Size = new System.Drawing.Size(411, 365);
            this.ux_reportviewerMain.TabIndex = 0;
            // 
            // SQLServerReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 367);
            this.Controls.Add(this.ux_reportviewerMain);
            this.Name = "SQLServerReportForm";
            this.Text = "SQLServerReportForm";
            this.Load += new System.EventHandler(this.SQLServerReportForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer ux_reportviewerMain;
    }
}