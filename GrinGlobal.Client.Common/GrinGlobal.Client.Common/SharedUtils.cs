﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace GRINGlobal.Client.Common
{
    public struct DataviewProperties
    {
        public string TabName;
        public string DataviewName;
        public string StrongFormName;
        public string ViewerStyle;
        public string AlwaysOnTop;
    }

    public struct FormsData
    {
        public string FormName;
        public string PreferredDataviewName;
        public System.Reflection.ConstructorInfo ConstInfo;
        public string StrongFormName;
    }

    public class SharedUtils
    {
        private WebServices _webServices;
        private LookupTables _lookupTables;
        private LocalDatabase _localDatabase;
        private UserSettings _userSettings;
        private AppSettings _appSettings;
        private UserInterfaceUtils _userInterfaceUtils;
        private BarcodeServer _barcodeServer;
        private DataTable _keyboardMappings;
        private bool _isConnected = false;
        private Dictionary<string, string> _webServiceURLs;
        private string _userCooperatorID = "";
        private int _userLanguageCode = 0;
        private string _userSite = "";
        private string _userSiteID = "";
        private string _userGroups = "";
        private string _appName = "GRINGlobalClientCuratorTool";

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true, CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        private static extern IntPtr LoadCursorFromFile(string path);

        public bool IsConnected
        {
            get
            {
                return _isConnected;
            }
        }

        public string Username
        {
            get
            {
                return _webServices.Username;
            }
        }

        public string Password
        {
            get
            {
                return _webServices.Password;
            }
        }

        public string Password_ClearText
        {
            get
            {
                return _webServices.Password_ClearText;
            }
        }

        public string UserCooperatorID
        {
            get
            {
                return _userCooperatorID;
            }
        }

        public int UserLanguageCode
        {
            get
            {
                return _userLanguageCode;
            }
            set
            {
                _userLanguageCode = value;
                _appSettings = new AppSettings(_webServices, _userLanguageCode, _appName);
            }
        }

        public string UserSite
        {
            get
            {
                return _userSite;
            }
        }

        public string UserSiteID
        {
            get
            {
                return _userSiteID;
            }
        }

        public string UserGroups
        {
            get
            {
                return _userGroups;
            }
        }

        public string Url
        {
            get
            {
                return _webServices.Url;
            }
        }

        public Dictionary<string, string> WebServiceURLs
        {
            get
            {
                return _webServiceURLs;
            }
        }

        public int LookupTablesLoadingPageSize
        {
            get
            {
                return _lookupTables.PageSize;
            }
            set
            {
                _lookupTables.PageSize = value;
            }
        }

        public DataTable KeyboardMappings
        {
            get
            {
                return _keyboardMappings;
            }
            set
            {
                _keyboardMappings = value;

                // Save the keyboard mapping changes to the user's roaming profile directory...
                string roamingKeyboardMappingFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\KeyboardMapping.xml";
                // Save the current keyboard mappings to the XML database...
                _keyboardMappings.WriteXml(roamingKeyboardMappingFilePath, XmlWriteMode.WriteSchema);
            }
        }

        public SharedUtils(string webServiceURL, string webServiceUsername, string webServicePassword, bool hideServerList, string appName)
        {
            _isConnected = TestCredentials(webServiceURL, webServiceUsername, webServicePassword);
            _appName = appName;
            if (string.IsNullOrEmpty(appName)) _appName = "GRINGlobalClientCuratorTool";

            // If the credentials supplied by the parameters to this method are not valid - present the login dialog...
            if (!_isConnected)
            {
                Login loginDialog = new Login(webServiceUsername, webServicePassword, webServiceURL, null, hideServerList);
                loginDialog.StartPosition = FormStartPosition.CenterScreen;
                loginDialog.ShowDialog();
                if (DialogResult.OK == loginDialog.DialogResult)
                {
                    _isConnected = true;
                    webServiceURL = loginDialog.SelectedWebServiceURL;
                    webServiceUsername = loginDialog.UserName;
                    webServicePassword = loginDialog.Password_ClearText;
                    _userCooperatorID = loginDialog.UserCooperatorID;
                    int langCode = 1;
                    if (int.TryParse(loginDialog.UserLanguageCode, out langCode))
                    {
                        _userLanguageCode = langCode;
                    }
                    else
                    {
                        _userLanguageCode = 1;
                    }
                    _userSite = loginDialog.UserSite;
                    _userSiteID = loginDialog.UserSiteID;
                    _userGroups = loginDialog.UserGroups;
                    _webServiceURLs = loginDialog.WebServiceURLs;
                }
            }

            if (_isConnected)
            {
                // Create the WebServices class...
                _webServices = new WebServices(webServiceURL, webServiceUsername, SHA1EncryptionBase64(webServicePassword), webServicePassword, _userSite);
                
                // Create the UserSettings class...
                _userSettings = new UserSettings(_webServices, _userCooperatorID, _appName);
                DataSet userData = _webServices.ValidateLogin();
                if (userData != null &&
                    userData.Tables.Contains("validate_login") &&
                    userData.Tables["validate_login"].Rows.Count > 0)
                {
                    int sys_lang_id = 1;
                    if (int.TryParse(userData.Tables["validate_login"].Rows[0]["sys_lang_id"].ToString(), out sys_lang_id))
                    {
                        _userLanguageCode = sys_lang_id;
                    }
                    else
                    {
                        _userLanguageCode = 1;
                    }
                }
                
                // Create the App Settings Class...
                _appSettings = new AppSettings(_webServices, _userLanguageCode, _appName);

                // Create the UserInterfaceUtils helper class...
                _userInterfaceUtils = new UserInterfaceUtils(_webServices);
            }

            // Build the local DB name from the web service URL...
            string localDBInstance = webServiceURL.ToLower().Replace("ldap://", "").Replace("http://", "").Replace("https://", "").Replace("/gringlobal/gui.asmx", "").Replace("/gringlobal_remote_debug/gui.asmx", "").Replace('-', '_').Replace('.', '_').Replace(':', '_');
            localDBInstance = "GRINGlobal_" + localDBInstance;

            // Test to see if the user has access to the ProgramData\GRIN-Global\Curator Tool\{current database connection} folder used to store LUT cache/dictionary files...
//if (_isConnected) _isConnected = TestProgramDataFolderAccess(localDBInstance);

            // Create the LocalDatabase class...
            if (_isConnected) _localDatabase = new LocalDatabase(localDBInstance);

            // Test to see if the user can connect to the local SQL Server database...
            if (_isConnected) _isConnected = TestLocalDatabaseAccess(_localDatabase);

            // Test to see if the user has elevated permissions to the local SQL Server database...
            if (_isConnected) _isConnected = TestLUTAccess(localDBInstance);

            // Connect to the localDatabase and create the lookup table class...
            if (_isConnected)
            {
                bool optimizeLUTForSpeed = false;
                bool.TryParse(_userSettings["", "ux_checkboxOptimizeLUTForSpeed", "Checked"], out optimizeLUTForSpeed);
                _lookupTables = new LookupTables(_webServices, _localDatabase, optimizeLUTForSpeed);
            }

            // Create the local BarcodeServer class...
            _barcodeServer = new BarcodeServer();

            // Load the KeyboardMappings datatable...
            _keyboardMappings = LoadKeyboardMapping();
        }

        private bool TestCredentials(string webServiceURL, string webServiceUsername, string webServicePassword)
        {
            bool validLoginCredentials = false;
            
            // Bail out now if the URL or username are empty strings...
            if (string.IsNullOrEmpty(webServiceURL) || string.IsNullOrEmpty(webServiceUsername)) return validLoginCredentials;

            try
            {
                _webServices = new WebServices(webServiceURL, webServiceUsername, SHA1EncryptionBase64(webServicePassword), webServicePassword, _userSite);
                DataSet userData = _webServices.ValidateLogin();
                if (userData != null &&
                    userData.Tables.Contains("validate_login") &&
                    userData.Tables["validate_login"].Rows.Count > 0)
                {
                    // Successful login...
                    validLoginCredentials = true;
                    if (userData.Tables["validate_login"].Columns.Contains("cooperator_id")) _userCooperatorID = userData.Tables["validate_login"].Rows[0]["cooperator_id"].ToString();
                    if (userData.Tables["validate_login"].Columns.Contains("site_code")) _userSite = userData.Tables["validate_login"].Rows[0]["site_code"].ToString();
                    if (userData.Tables["validate_login"].Columns.Contains("site")) _userSite = userData.Tables["validate_login"].Rows[0]["site"].ToString();
                    if (userData.Tables["validate_login"].Columns.Contains("site_id")) _userSiteID = userData.Tables["validate_login"].Rows[0]["site_id"].ToString();
                    string langString = "0";
                    if (userData.Tables["validate_login"].Columns.Contains("sec_lang_id")) langString = userData.Tables["validate_login"].Rows[0]["sec_lang_id"].ToString();
                    if (userData.Tables["validate_login"].Columns.Contains("sys_lang_id")) langString = userData.Tables["validate_login"].Rows[0]["sys_lang_id"].ToString();
                    int langInt = 0;
                    if (int.TryParse(langString, out langInt)) _userLanguageCode = langInt;
                }
            }
            catch
            {
                // Unsuccessful login...
                validLoginCredentials = false;
            }

            return validLoginCredentials;
        }

        private bool TestProgramDataFolderAccess(string localDBInstance)
        {
            bool programDataFolderAccess = false;
            string destinationFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool\" + localDBInstance;

            try
            {
                // Presume the user has read/write permissions unless proven otherwise in the test below...
                programDataFolderAccess = true;
                // Make sure the destination folder exists - and if not create it...
                if (Directory.Exists(destinationFilePath))
                {
                    // Try reading the list of files in the GG ProgramData folder...
                    DirectoryInfo di = new DirectoryInfo(destinationFilePath);
                    FileInfo[] fi = di.GetFiles();
                    // Try to open the first file in the folder for write access...
                    if(fi.Length > 0)
                    {
                        string filePath = fi[0].FullName;
                        FileStream fsTestWritePermissions = File.Open(filePath, FileMode.Append, FileAccess.Write);
                        fsTestWritePermissions.Close();
                        fsTestWritePermissions.Dispose();
                    }
                }
            }
            catch (System.Exception e)
            {
                programDataFolderAccess = false;
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("User access to the folder containing the Lookup Table Cache files has been denied!\n\nMake sure the 'Users' group has been granted read and write permission to this parent folder:\n\"" + System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool\" + "\"\n\nFull error message:\n{0}", "Lookup Table Cache Access Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "TestProgramDataFolderAccess";
                if (_appSettings != null) this.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = e.Message;
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }

            return programDataFolderAccess;
        }

        private bool TestLocalDatabaseAccess(LocalDatabase localDatabase)
        {
            bool localDatabaseAccess = false;

            if (!string.IsNullOrEmpty(localDatabase.ConnectionString))
            {
                localDatabaseAccess = true;
            }
            else
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("Unable to contact the local lookup table database, is SQL Server database engine running on your PC?\n\nMake sure the SQL Server service is running (this can be checked in the Windows Task Manager).\n\nFull error message:\n{0}", "SQL Server Database Engine Not Running Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "TestLocalDatabaseAccess";
                if (_appSettings != null) this.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = "";
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }

            return localDatabaseAccess;
        }

        private bool TestLUTAccess(string localDBInstance)
        {
            bool localLUTAccess = false;

            try
            {
                if (this._localDatabase != null &&
                    !string.IsNullOrEmpty(this._localDatabase.ConnectionString))
                {
                    DataTable dt = new DataTable("__TestCreateTable__");
                    dt.Columns.Add("col1", typeof(int));
                    this._localDatabase.CreateTable(dt, true, true);
                    this._localDatabase.Remove(dt.TableName);
                    localLUTAccess = true;
                }
            }
            catch (System.Exception e)
            {
                localLUTAccess = false;
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("User access to the local SQL Server (Lookup Tables) database has been denied!\n\nMake sure the System Administrator who installed the Curator Tool has run the 'Make database available to all users' command from the Tools menu.\n\nFull error message:\n{0}", "Lookup Table Cache Access Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "TestLUTAccess";
                if (_appSettings != null) this.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = e.Message;
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }
            return localLUTAccess;
        }

        public DataSet ChangeLanguage(int newLanguage)
        {
            return _webServices.ChangeLanguage(newLanguage);
        }

        public DataSet ChangeOwnership(DataSet ownedDataset, string newCNO, bool includeChildren)
        {
            int cno = -1;
            if (!int.TryParse(newCNO, out cno))
            {
                // The supplied cooperator id is invalid...
                DataSet userData = _webServices.ValidateLogin();
                if (userData != null &&
                    userData.Tables.Contains("validate_login") &&
                    userData.Tables["validate_login"].Rows.Count > 0)
                {
                    // Successful login - so get the users cno...
                    if (userData.Tables["validate_login"].Columns.Contains("cooperator_id")) cno = (int)userData.Tables["validate_login"].Rows[0]["cooperator_id"];
                }
                else
                {
                    // Hummmm...  Things are looking real bad - set the cno to an -1 (there should never be a cno = -1, so this should error out on the server)
                    cno = -1;
                }
            }
            return _webServices.ChangeOwnership(ownedDataset, cno, includeChildren);
        }
        
        public string GetLookupDisplayMember(string lookupTable, string valueMember, string groupName, string defaultDisplayMember)
        {
            string displayMember = "";

            if (lookupTable.ToLower().Trim() != "code_value_lookup")
            {
                int pkey = -1;
                if (int.TryParse(valueMember, out pkey))
                {
                    displayMember = _lookupTables.GetPKeyDisplayMember(lookupTable, pkey, defaultDisplayMember);
                }
                else
                {
                    displayMember = defaultDisplayMember;
                }
            }
            else
            {
                displayMember = _lookupTables.GetCodeValueDisplayMember(groupName, valueMember, defaultDisplayMember);
            }
            return displayMember;
        }

        public string GetLookupValueMember(DataRow dr, string lookupTable, string displayMember, string groupName, string defaultValueMember)
        {
            string valueMember = "";
            if (lookupTable.ToLower().Trim() != "code_value_lookup")
            {
                int pkey;
                if (int.TryParse(defaultValueMember, out pkey))
                {
                    valueMember = _lookupTables.GetPKeyValueMember(dr, lookupTable, displayMember, pkey).ToString();
                }
                else
                {
                    valueMember = _lookupTables.GetPKeyValueMember(dr, lookupTable, displayMember, -1).ToString();
                    if (valueMember == "-1") valueMember = defaultValueMember;
                }
            }
            else
            {
                valueMember = _lookupTables.GetCodeValueValueMember(displayMember, groupName, defaultValueMember);
            }
            return valueMember;
        }

        public DataTable GetLocalData(string SQLSelect, string SQLparms)
        {
            DataTable dt;
            string[] parms = SQLparms.Split(';');
            dt = _localDatabase.GetData(SQLSelect, parms);
            return dt;
        }

        public bool SaveLocalData(DataTable DataTable)
        {
            return _localDatabase.SaveData(DataTable);
        }

        public DataSet GetWebServiceData(string dataviewName, string delimitedParameterList, int offset, int limit)
        {
            DataSet results = new DataSet();
            DataSet dataViewParams = new DataSet();
            if (IsConnected)
            {
                results = _webServices.GetData(dataviewName.ToLower(), delimitedParameterList, offset, limit);
            }      
            return results;
        }

        public DataSet SaveWebServiceData(DataSet modifiedDataSet)
        {
            DataSet ds = new DataSet();
            if (IsConnected)
            {
                ds = _webServices.SaveData(modifiedDataSet);
            }
            return ds;
        }

        public DataSet SearchWebService(string query, bool ignoreCase, bool andTermsTogether, string indexList, string resolverName, int offset, int limit)
        {
            DataSet ds;
            ds = _webServices.Search(query, ignoreCase, andTermsTogether, indexList, resolverName, offset, limit, "parseonly=false; passthru=NonIndexedOrComparison; languageid=0; lookupcodedvalues=true; ignorecache=false");
            return ds;
        }

        public DataSet SearchWebService(string query, bool ignoreCase, bool andTermsTogether, string indexList, string resolverName, int offset, int limit, string searchOptions)
        {
            DataSet ds;
            ds = _webServices.Search(query, ignoreCase, andTermsTogether, indexList, resolverName, offset, limit, searchOptions);
            return ds;
        }

        public DataSet GetFileInfo(string groupName, string versionName, bool onlyAvailable, bool onlyLatest)
        {
            DataSet results = new DataSet();
            if (IsConnected)
            {
                results = _webServices.GetFileInfo(groupName, versionName, onlyAvailable, onlyLatest);
            }
            return results;
        }

        public string GetKeyboardMapping(string functionName, string defaultReturnValue)
        {
            string returnValue = defaultReturnValue;

            DataRow[] drs = _keyboardMappings.Select("function_name = '" + functionName + "'");

            if (drs != null &&
                drs.Length > 0)
            {
                returnValue = drs[0]["key_data"].ToString(); 
            }
            else 
            {
                returnValue = defaultReturnValue;
            }
            return returnValue;
        }

        public string GetUserSetting(string form, string resource, string key, string defaultReturnValue)
        {
            string returnValue = _userSettings[form, resource, key];
            if (string.IsNullOrEmpty(returnValue)) returnValue = defaultReturnValue;
            return returnValue;
        }

        public void DeleteUserSetting(string form, string resource, string key)
        {
            _userSettings.Delete(form, resource, key);
        }

        public void SaveUserSetting(string form, string resource, string key, string value)
        {
            _userSettings[form,  resource, key] = value;
        }

        public void SaveAllUserSettings()
        {
            _userSettings.Save();
        }

        public void LoadAllUserSettings()
        {
            _userSettings.Load();
        }

        public void DeleteAllUserSettings()
        {
            _userSettings.DeleteAll();
        }

        public Cursor LoadCursor(string filePath)
        {
            Cursor interopCursor;
            try
            {
                interopCursor = new Cursor(LoadCursorFromFile(filePath));
            }
            catch
            {
                interopCursor = null;
            }
            return interopCursor;
        }

        private DataTable LoadKeyboardMapping()
        {
            DataTable keyboardMapping = new DataTable("keyboard_mapping");
            string roamingKeyboardMappingFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\KeyboardMapping.xml";
            string commonKeyboardMappingFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool\KeyboardMapping.xml";

            // Look for (and process) a KeyboardMapping.xml file to allow custom application settings entries...
            // But first make sure the roaming profile directory exists...
            if (!System.IO.Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\"))
            {
                // Create the roaming profile directory...
                DirectoryInfo roamingDI = System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\");
                // Populate the roaming profile directory with the files from the CommonApplicationData directory...
                DirectoryInfo commonDI = new DirectoryInfo(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool\");
                foreach (FileInfo fi in commonDI.GetFiles())
                {
                    fi.CopyTo(roamingDI.FullName + @"\" + fi.Name, true);
                }
            }

            // Now check the user's personal roaming profile directory again for a copy of the KeyboardMapping.xml file...
            if (System.IO.File.Exists(roamingKeyboardMappingFilePath))
            {
                // Found a personal user's copy of the KeyboardMapping.xml file - so use it...
                keyboardMapping.ReadXml(roamingKeyboardMappingFilePath);
            }
            else
            {
                // No KeyboardMapping.xml file exists - so create a new datatable and save it to the roaming profile path...
                // First create the columns...
                keyboardMapping.Columns.Add("function_name", typeof(string));
                keyboardMapping.Columns.Add("key_code", typeof(string));
                keyboardMapping.Columns.Add("key_data", typeof(string));
                keyboardMapping.Columns.Add("key_value", typeof(string));
                keyboardMapping.Columns.Add("modifiers", typeof(string));

                // Populate the datatable with the default shortcut key for switching into 'Block Mode' in the main dgv...
                Keys BlockMode = Keys.Oemtilde | Keys.Control;
                KeyEventArgs kk = new KeyEventArgs(BlockMode);
                DataRow newKeyboardMappingRow = keyboardMapping.NewRow();
                newKeyboardMappingRow["function_name"] = "Block Mode";
                newKeyboardMappingRow["key_code"] = kk.KeyCode.ToString();
                newKeyboardMappingRow["key_data"] = kk.KeyData.ToString();
                newKeyboardMappingRow["key_value"] = kk.KeyValue.ToString();
                newKeyboardMappingRow["modifiers"] = kk.Modifiers.ToString();
                keyboardMapping.Rows.Add(newKeyboardMappingRow);

                // Write the new datatable out to the user's roaming profile directory...
                keyboardMapping.WriteXml(roamingKeyboardMappingFilePath, XmlWriteMode.WriteSchema);
            }

            return keyboardMapping;
        }

        public string SaveAttachment(string destinationFilePath, byte[] imageBytes, bool createThumbnail, bool overWriteIfExists)
        {
            return _webServices.SaveAttachment(destinationFilePath, imageBytes, createThumbnail, overWriteIfExists);
        }

        public byte[] GetAttachment(string filePath)
        {
            return _webServices.GetAttachment(filePath);
        }

        public bool DeleteAttachment(string filePath)
        {
            return _webServices.DeleteAttachment(filePath);
        }

        public string GetAppSettingValue(string name)
        {
            return _appSettings.GetAppSettingValue(name);
        }

        public void UpdateComponents(System.ComponentModel.ComponentCollection compCollection, string formName)
        {
            _appSettings.UpdateComponents(compCollection, formName);
        }

        public void UpdateControls(System.Windows.Forms.Control.ControlCollection ctrlCollection, string formName)
        {
            _appSettings.UpdateControls(ctrlCollection, formName);
        }

        public bool LocalDatabaseTableExists(string tableName)
        {
            return _localDatabase.TableExists(tableName);
        }

        public void LocalDatabaseCreateTable(DataTable newTable)
        {
            _localDatabase.CreateTable(newTable, false, true);
        }

        public bool LocalDatabaseMakeAccessibleToAllUsers()
        {
            return _localDatabase.MakeAccessibleToAllUsers();
        }

        public DataTable LookupTablesGetMatchingRows(string dataviewName, string displayMemberLikeFilter, int maxReturnRows)
        {
            return _lookupTables.LookupTableGetMatchingRows(dataviewName, displayMemberLikeFilter, maxReturnRows);
        }

        public void LookupTablesUpdateTable(object objDataviewName, bool runAsBackgroundThread)
        {
            if (runAsBackgroundThread)
            {
                new System.Threading.Thread(_lookupTables.UpdateTable).Start(objDataviewName);
            }
            else
            {
                _lookupTables.UpdateTable(objDataviewName);
            }
        }

        public void LookupTablesLoadTableFromDatabase(object objDataviewName)
        {
            _lookupTables.LoadTableFromDatabase(objDataviewName);
        }

        public DataTable LookupTablesGetSynchronizationStats()
        {
            return _lookupTables.GetSynchronizationStats();
        }

        public bool LookupTablesIsUpdated(string tableName)
        {
            return _lookupTables.IsUpdated(tableName);
        }

        public void LookupTablesClearLookupTable(object objDataviewName)
        {
            _lookupTables.ClearLocalLookupTable(objDataviewName);
        }

        public bool LookupTablesIsValidFKField(DataColumn dc)
        {
            return _lookupTables.IsValidFKField(dc);
        }

        public bool LookupTablesIsValidCodeValueField(DataColumn dc)
        {
            return _lookupTables.IsValidCodeValueField(dc);
        }

        public void LookupTablesSaveALLCaches()
        {
            _lookupTables.SaveAllLUTDictionaries();
        }

        public void LookupTablesSaveDataPageToLocalDB(DataTable DataTable)
        {
            _lookupTables.SaveDataPageToLocalDB(DataTable);
        }

        public void BuildReadOnlyDataGridView(DataGridView dataGridView, DataTable dataTable)
        {
            _userInterfaceUtils.buildReadOnlyDataGridView(dataGridView, dataTable, _lookupTables);
        }

        public void BuildEditDataGridView(DataGridView dataGridView, DataTable dataTable)
        {
            _userInterfaceUtils.buildEditDataGridView(dataGridView, dataTable, _lookupTables, _userCooperatorID);
        }

        public void BindComboboxToCodeValue(ComboBox comboBox, DataColumn dc)
        {
            if (dc.ExtendedProperties.Contains("group_name"))
            {
                string groupName = dc.ExtendedProperties["group_name"].ToString();
                DataTable dt = _lookupTables.GetCodeValueDataTableByGroupName(groupName);
                if (dc.ExtendedProperties.Contains("is_nullable") && dc.ExtendedProperties["is_nullable"].ToString() == "Y")
                {
                    DataRow dr = dt.NewRow();
                    dr["display_member"] = "[Null]";
                    dr["value_member"] = DBNull.Value;
                    dt.Rows.InsertAt(dr, 0);
                    dt.AcceptChanges();
                }

                comboBox.DisplayMember = "display_member";
                comboBox.ValueMember = "value_member";
                comboBox.DataSource = dt;
                //comboBox.DefaultCellStyle.DataSourceNullValue = DBNull.Value;
                //comboBox.DefaultCellStyle.NullValue = "[Null]";
            }
        }

        public bool ProcessDGVEditShortcutKeys(DataGridView dgv, KeyEventArgs e, string cno)
        {
            return _userInterfaceUtils.ProcessDGVEditShortcutKeys(dgv, e, cno, _lookupTables, _appSettings, _userSettings);
        }

        public bool ProcessDGVReadOnlyShortcutKeys(DataGridView dgv, KeyEventArgs e, string cno)
        {
            return _userInterfaceUtils.ProcessDGVReadOnlyShortcutKeys(dgv, e, cno, _lookupTables, _appSettings, _userSettings);
        }

        public string BuildClipboardString(DataGridView dgv, bool dgvIsReadOnly, bool useFriendlyColumnNames, bool stripSpecialCharacters)
        {
            return _userInterfaceUtils.buildClipboardString(dgv, dgvIsReadOnly, useFriendlyColumnNames, stripSpecialCharacters);
        }

        public bool ImportTextToDataTableUsingKeys(string rawImportText, DataTable destinationTable, char[] rowDelimiters, char[] columnDelimiters, out int badRows, out int missingRows)
        {
            return _userInterfaceUtils.ImportTextToDataTableUsingKeys(rawImportText, destinationTable, rowDelimiters, columnDelimiters, out badRows, out missingRows, _lookupTables, _appSettings, _userSettings);
        }

        public bool ImportTextToDataTableUsingBlockStyle(string rawImportText, DataGridView dataGridView, char[] rowDelimiters, char[] columnDelimiters, out int badRows, out int missingRows)
        {
            return _userInterfaceUtils.ImportTextToDataTableUsingBlockStyle(rawImportText, dataGridView, rowDelimiters, columnDelimiters, out badRows, out missingRows, _lookupTables, _appSettings, _userSettings);
        }

        public string GetFriendlyFieldName(DataColumn dc, string defaultName)
        {
            return _userInterfaceUtils.GetFriendlyFieldName(dc, defaultName);
        }

        public void ux_tabcontrolCreateNewTab(TabControl ux_tabcontrolDataview, int indexOfNewTab)
        {
            _userInterfaceUtils.ux_tabcontrolCreateNewTab(ux_tabcontrolDataview, indexOfNewTab);
        }

        public void ux_tabcontrolAddTab(TabControl ux_tabcontrolDataview, string text, DataviewProperties tag, int position)
        {
            _userInterfaceUtils.ux_tabcontrolAddTab(ux_tabcontrolDataview, text, tag, position);
        }

        public void ux_tabcontrolShowProperties(TabControl ux_tabcontrolDataview, int indexOfCurrentTab)
        {
            _userInterfaceUtils.ux_tabcontrolShowProperties(ux_tabcontrolDataview, indexOfCurrentTab);
        }

        public void ux_tabcontrolRemoveTab(TabControl ux_tabcontrolDataview, int indexOfTabToDelete)
        {
            _userInterfaceUtils.ux_tabcontrolRemoveTab(ux_tabcontrolDataview, indexOfTabToDelete);
        }

        public void ux_tabcontrolMouseDownEvent(TabControl ux_tabcontrolDataview, MouseEventArgs e)
        {
            _userInterfaceUtils.ux_tabcontrolMouseDownEvent(ux_tabcontrolDataview, e);
        }

        public void ux_tabcontrolDragOverEvent(TabControl ux_tabcontrolDataview, DragEventArgs e)
        {
            _userInterfaceUtils.ux_tabcontrolDragOverEvent(ux_tabcontrolDataview, e);
        }

        public void ux_tabcontrolDragDropEvent(TabControl ux_tabcontrolDataview, DragEventArgs e)
        {
            _userInterfaceUtils.ux_tabcontrolDragDropEvent(ux_tabcontrolDataview, e);
        }

        public void BuildDataviewTabControl(TabControl ux_tabcontrolDataview)
        {
            _userInterfaceUtils.buildDataviewTabControl(ux_tabcontrolDataview, this);
        }
             
        public static string SHA1EncryptionBase64(string clearText)
        {
            string encryptedText = "";
            // Example encryption:  "administrator" = "s6ypLHk+4OmxqbCl9fwETgUUDfM="

            // First create a SHA1 class to do the encryption...
            System.Security.Cryptography.SHA1 sha1Hash = new System.Security.Cryptography.SHA1Managed();
            // This next line does three things:
            // 1) Encode the clearText into an array of UTF8 bytes 
            // 2) Pass the byte array to the SHA1 class (to compute the SHA1 hash)
            // 3) Convert the SHA1 encrypted bytes array to a Base 64 string (so that it can pass through to the webservice as standard text)...
            encryptedText = Convert.ToBase64String(sha1Hash.ComputeHash(System.Text.UTF8Encoding.UTF8.GetBytes(clearText)));

            return encryptedText;
        }

        public static bool TargetStringFitsPattern(string regexPattern, string targetString, bool ignoreCase)
        {
            bool patternFound = false;
            System.Text.RegularExpressions.RegexOptions searchOptions = System.Text.RegularExpressions.RegexOptions.None; //new System.Text.RegularExpressions.RegexOptions();
            if(ignoreCase) searchOptions = System.Text.RegularExpressions.RegexOptions.IgnoreCase;

            if (targetString.Length > 0 &&
                regexPattern.Length > 0)
            {
                patternFound = System.Text.RegularExpressions.Regex.IsMatch(targetString, regexPattern, searchOptions);
            }

            return patternFound;
        }

        public FormsData[] GetDataviewFormsData()
        {
            FormsData[] localAssembliesFormsData = new FormsData[0];

            try
            {
                // Load the static forms from the same directory (and all subdirectories) where the Curator Tool was launched...
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Directory.GetCurrentDirectory());
                System.IO.FileInfo[] dllFiles = null;
                if (System.IO.Directory.Exists(System.IO.Directory.GetCurrentDirectory() + "\\Forms")) dllFiles = di.GetFiles("Forms\\*.dll", System.IO.SearchOption.AllDirectories);
                if (dllFiles != null && dllFiles.Length > 0)
                {
                    localAssembliesFormsData = new FormsData[dllFiles.Length];
                    for (int i = 0; i < dllFiles.Length; i++)
                    {
                        System.Reflection.Assembly newAssembly = System.Reflection.Assembly.LoadFile(dllFiles[i].FullName);
                        foreach (System.Type t in newAssembly.GetTypes())
                        {
                            if (t.GetInterface("IGRINGlobalDataForm", true) != null)
                            {
                                System.Reflection.ConstructorInfo constInfo = t.GetConstructor(new Type[] { typeof(BindingSource), typeof(bool), typeof(SharedUtils), typeof(bool) });
                                if (constInfo != null)
                                {
                                    // Instantiate an object of this type to inspect...
                                    Form f = (Form)constInfo.Invoke(new object[] { new BindingSource(), false, this, false });
                                    // Get the Form Name and save it to the array...
                                    System.Reflection.PropertyInfo propInfo = t.GetProperty("FormName", typeof(string));
                                    string formName = (string)propInfo.GetValue(f, null);
                                    if (string.IsNullOrEmpty(formName)) formName = t.Name;
                                    localAssembliesFormsData[i].FormName = formName;
                                    // Get the preferred dataview name and save it to the array...
                                    propInfo = t.GetProperty("PreferredDataview", typeof(string));
                                    string preferredDataview = (string)propInfo.GetValue(f, null);
                                    if (string.IsNullOrEmpty(preferredDataview)) preferredDataview = "";
                                    localAssembliesFormsData[i].PreferredDataviewName = preferredDataview;
                                    // Save the constructor info object to the array...
                                    localAssembliesFormsData[i].ConstInfo = constInfo;
                                    localAssembliesFormsData[i].StrongFormName = constInfo.Module.FullyQualifiedName + " : " + formName + " : " + preferredDataview;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("Error binding to dataview Form.\nError Message: {0}", "Form Binding Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "GrinGlobalClient_LoadMessage1";
                this.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = err.Message;
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }

            return localAssembliesFormsData;
        }

        public Dictionary<string, string> GetReportsMapping(bool ignoreComments = true)
        {
            string usersReportsMappingFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\ReportsMapping.txt";
            string allUsersReportsMappingFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool\ReportsMapping.txt";
            string reportsMappingFilePath = "";

            Dictionary<string, string> reportsMapping = new Dictionary<string, string>();

            // Look for (and process) a ReportsMapping.txt file to allow custom application settings entries...
            // But first make sure the roaming profile directory exists...
            if (!System.IO.Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports"))
            {
                // Create the Reports directory...
                DirectoryInfo roamingReportsDI = System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports");
                // Populate the roaming Reports directory with the reports from the CommonApplicationData directory...
                DirectoryInfo commonReportsDI = new DirectoryInfo(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\GRIN-Global\Curator Tool\Reports");
                foreach(FileInfo fi in commonReportsDI.GetFiles())
                {
                    fi.CopyTo(roamingReportsDI.FullName + @"\" + fi.Name, true);
                }
            }

            // Now check the user's personal roaming profile for a copy of the ReportsMapping.txt file...
            if (System.IO.File.Exists(usersReportsMappingFilePath))
            {
                // Found a personal user's copy of the ReportsMapping.txt file - so use it...
                reportsMappingFilePath = usersReportsMappingFilePath;
            }
            else if (System.IO.File.Exists(allUsersReportsMappingFilePath))
            {
                // Couldn't find a copy of the ReportsMapping.txt file so copy the master into the user's personal roaming profile AppData directory...
                System.IO.File.Copy(allUsersReportsMappingFilePath, usersReportsMappingFilePath);
                reportsMappingFilePath = usersReportsMappingFilePath;
            }


            // Now check to see if the version of the ReportsMapping.txt file in the user's personal roaming profile AppData directory needs to be updated...
            if (System.IO.File.Exists(usersReportsMappingFilePath) &&
                System.IO.File.Exists(allUsersReportsMappingFilePath) &&
                ReportsMappingFileNeedsUpdating(usersReportsMappingFilePath, allUsersReportsMappingFilePath))
            {
                // Make sure the Backup directory exists in the roaming profile directory...
                if (!System.IO.Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Backup"))
                {
                    System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Backup");
                }
                // Create backup file name...
                string usersReportsMappingBackupFilePath = usersReportsMappingFilePath.Replace(@"\ReportsMapping.txt", @"\Backup\ReportsMapping_" + DateTime.Today.ToString("yyyy-MM-dd") + ".txt");
                // Copy the original ReportsMapping.txt file to the Backup directory and append today's date to the filename (but don't overwrite backup file if already there)...
                if (!System.IO.File.Exists(usersReportsMappingBackupFilePath))
                {
                    System.IO.File.Copy(usersReportsMappingFilePath, usersReportsMappingBackupFilePath);
                }
                // Copy the new AppSettings.txt file into the user's roaming profile directory...
                System.IO.File.Copy(allUsersReportsMappingFilePath, usersReportsMappingFilePath, true);
            }

            // Now check to see if there are report files in the allUser's Reports directory that should be copied over to the user's Reports directory...
            if (System.IO.File.Exists(usersReportsMappingFilePath) &&
                System.IO.File.Exists(allUsersReportsMappingFilePath))
            {
                string[] allUsersReports = Directory.GetFiles(allUsersReportsMappingFilePath.Replace("ReportsMapping.txt", "Reports"), "*.*", SearchOption.TopDirectoryOnly);
                string[] usersReports = Directory.GetFiles(usersReportsMappingFilePath.Replace("ReportsMapping.txt", "Reports"), "*.*", SearchOption.TopDirectoryOnly);
                foreach (string allUserReport in allUsersReports)
                {
                    bool matchNotFound = true;
                    foreach (string userReport in usersReports)
                    {
                        if(System.IO.Path.GetFileName(userReport).ToLower().Equals(System.IO.Path.GetFileName(allUserReport).ToLower()))
                        {
                            matchNotFound = false;
                            break;
                        }
                    }
                    // If no matching filename was found copy the allUser's report file to the user's Reports directory...
                    if(matchNotFound)
                    {
                        System.IO.File.Copy(allUserReport, usersReportsMappingFilePath.Replace("ReportsMapping.txt", @"Reports\" + System.IO.Path.GetFileName(allUserReport)), true);
                    }
                }
            }


            // Process the ReportsMapping.txt file...
            if (!string.IsNullOrEmpty(reportsMappingFilePath))
            {
//string defaultReportsPath = (System.Windows.Forms.Application.StartupPath + @"\Reports\").ToUpper().Trim();
string defaultReportsPath = (System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports\").ToUpper().Trim();
                System.IO.StreamReader sr = new System.IO.StreamReader(reportsMappingFilePath);
                while (!sr.EndOfStream)
                {
                    string reportMap = sr.ReadLine().Trim().ToUpper();
                    if (!string.IsNullOrEmpty(reportMap.Trim()) &&
                        !(reportMap.Trim().StartsWith("#") && ignoreComments))
                    {
                        string[] keyValuePair = reportMap.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                        if (keyValuePair != null)
                        {
                            // If the key does not include the full path (original/legacy format) - provide a full path to the default 'Reports' folder for the CT...
                            string testForPath = System.IO.Path.GetDirectoryName(keyValuePair[0].ToUpper().Trim());
                            if(string.IsNullOrEmpty(testForPath))
                            {
                                // This key does not include the path to the file so provide the default path now...
                                keyValuePair[0] = System.IO.Path.Combine(defaultReportsPath, keyValuePair[0].ToUpper().Trim());
                            }
                            // Force the key to uppercase and trim...
                            keyValuePair[0] = keyValuePair[0].ToUpper().Trim();
                            if (keyValuePair.Length >= 2)
                            {
                                // Working off the assumption that the first '=' sign found is the delimiter for the key/value pair so concat all fields after the first '=' sign...
                                for (int i = 2; i < keyValuePair.Length; i++)
                                {
                                    keyValuePair[1] += "=" + keyValuePair[i];
                                }
                                // Now that the value in the key/value pair has been knitted back together (ie. includes any other '=' sign characters the
                                // Value can be properly formatted using the vertical pipe as a delimiter...
                                string[] valueTokens = keyValuePair[1].Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                                if (valueTokens.Length > 1)
                                {
                                    keyValuePair[1] = "";
                                    foreach (string token in valueTokens)
                                    {
                                        keyValuePair[1] += " " + token.Trim() + " |";
                                    }
                                    keyValuePair[1] = keyValuePair[1].TrimEnd('|');
                                }
                                else if(valueTokens.Length == 1)
                                {
                                    keyValuePair[1] = " " + valueTokens[0].Trim() + " ";
                                }
                            }
                            else if (keyValuePair.Length == 1)
                            {
                                keyValuePair = new string[] { keyValuePair[0], ""}; 
                                //break;
                            }
                            // Update or Add the new Report Mapping...
                            if (!reportsMapping.ContainsKey(keyValuePair[0].Trim()))
                            {
                                // This setting does not exist so Add it...
                                reportsMapping.Add(keyValuePair[0].Trim(), keyValuePair[1]);
                            }
                            else
                            {
                                // This setting is already in the dictionary so update it's value...
                                reportsMapping[keyValuePair[0].Trim()] += "|" + keyValuePair[1];
                            }
                        }
                    }
                }
                sr.Close();
                sr.Dispose();
            }
            return reportsMapping;
        }

        private bool ReportsMappingFileNeedsUpdating(string currentReportsMappingFile, string newReportsMappingFile)
        {
            bool needsUpdating = false;

            string currentVersionFile = System.IO.File.ReadAllText(currentReportsMappingFile);
            string currentVersion = "0.0.0.0";
            // Iterate through each line to find a version number for the file...
            StringReader sr = new StringReader(currentVersionFile);
            while (sr.Peek() != -1)
            {
                string ln = sr.ReadLine();
                if (ln.Contains("ReportsMappingFileVersion"))
                {
                    string[] lnTokens = ln.Split('=');
                    if (lnTokens.Length == 2)
                    {
                        string[] versionTokens = lnTokens[1].Split(' ');
                        currentVersion = versionTokens[0].Trim();
                        break;
                    }
                }
            }

            string newVersionFile = System.IO.File.ReadAllText(newReportsMappingFile);
            string newVersion = "0.0.0.0";
            // Iterate through each line to find a version number for the file...
            sr = new StringReader(newVersionFile);
            while (sr.Peek() != -1)
            {
                string ln = sr.ReadLine();
                if (ln.Contains("ReportsMappingFileVersion"))
                {
                    string[] lnTokens = ln.Split('=');
                    if (lnTokens.Length == 2)
                    {
                        string[] versionTokens = lnTokens[1].Split(' ');
                        newVersion = versionTokens[0].Trim();
                        break;
                    }
                }
            }

            // Test to see if the newVersion comes before (-1), equals (0), or comes after (1) currentVersion...
            if (newVersion.CompareTo(currentVersion) > 0)
            {
                needsUpdating = true;
            }

            return needsUpdating;
        }

        public void PrintReport(DataTable dataTable, string reportFullPath)
        {
            // Make sure the barcode server is properly shut down...
            StopBarcodeServer();
            // Now start the barcode server with the specified URI...
            StartBarcodeServer("http://localhost:53186/");

            FileInfo fi = new FileInfo(reportFullPath);
            if (fi.Exists)
            {
                if (fi.Extension.ToUpper() == ".RPT")
                {
                    CrystalReportForm crystalReport = new CrystalReportForm(dataTable, reportFullPath);
                    crystalReport.StartPosition = FormStartPosition.CenterParent;
                    UpdateControls(crystalReport.Controls, crystalReport.Name);
                    crystalReport.ShowDialog();
                }
                else if (fi.Extension.ToUpper() == ".RDL" || fi.Extension.ToUpper() == ".RDLC")
                {
                    SQLServerReportForm sqlServerReport = new SQLServerReportForm(dataTable, reportFullPath);
                    sqlServerReport.StartPosition = FormStartPosition.CenterParent;
                    UpdateControls(sqlServerReport.Controls, sqlServerReport.Name);
                    sqlServerReport.ShowDialog();
                }
            }

            // Printing is over so shutdown the barcode server...
            StopBarcodeServer();
        }

        private void StartBarcodeServer(string barcodeServerURL)
        {
            _barcodeServer.Start(barcodeServerURL);
        }

        private void StopBarcodeServer()
        {
            _barcodeServer.Stop();
        }
    }
}
