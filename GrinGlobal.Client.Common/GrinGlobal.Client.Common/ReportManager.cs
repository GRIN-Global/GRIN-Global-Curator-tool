﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GRINGlobal.Client.Common
{
    public partial class ReportManager : Form
    {
        SharedUtils _sharedUtils;
        DataTable _reportsMapping = new DataTable("ReportsMapping");
        DataTable _dataviews = new DataTable("Dataviews");
        Dictionary<string, string> _currentReportsMapping;
        bool _useDefaultServer = true;
        bool _useDefaultServerCredentials = true;
        string _reportWebServerURL = "";
        string _reportWebServerUsername = "";
        string _reportWebServerPassword = "";

        public ReportManager(SharedUtils sharedUtils)
        {
            InitializeComponent();

            _sharedUtils = sharedUtils;

            // Load Report Manager user options...
            bool useDefaultServer = false;
            if (bool.TryParse(_sharedUtils.GetUserSetting(this.Name, "_useDefaultServer", "Value", _useDefaultServer.ToString()), out useDefaultServer))
            {
                _useDefaultServer = useDefaultServer;
            }
            if (_useDefaultServer)
            {
                _reportWebServerURL = _sharedUtils.Url;
            }
            else
            {
                _reportWebServerURL = _sharedUtils.GetUserSetting(this.Name, "_reportWebServerURL", "Value", _reportWebServerURL);
            }
            bool useDefaultServerCredentials = false;
            if (bool.TryParse(_sharedUtils.GetUserSetting(this.Name, "_useDefaultServerCredentials", "Value", _useDefaultServer.ToString()), out useDefaultServerCredentials))
            {
                _useDefaultServerCredentials = useDefaultServerCredentials;
            }
            if (_useDefaultServerCredentials)
            {
                _reportWebServerUsername = _sharedUtils.Username;
                _reportWebServerPassword = _sharedUtils.Password_ClearText;
            }
            else
            {
                _reportWebServerUsername = _sharedUtils.GetUserSetting(this.Name, "_reportWebServerUsername", "Value", _reportWebServerUsername);
                _reportWebServerPassword = _sharedUtils.GetUserSetting(this.Name, "_reportWebServerPassword", "Value", _reportWebServerPassword);
            }


            // Create table fields for _reportsMapping datatable...
            _reportsMapping.Columns.Add("name", typeof(string));
            _reportsMapping.Columns.Add("is_enabled", typeof(bool));
            _reportsMapping.Columns.Add("full_path", typeof(string));
            _reportsMapping.Columns.Add("dataviews", typeof(string));
            _reportsMapping.Columns.Add("required_fields", typeof(string));

            //  Create table fields for the _dataviews datatable...
            _dataviews.Columns.Add("name", typeof(string));
            _dataviews.Columns.Add("is_active", typeof(bool));
            _dataviews.Columns.Add("field_list", typeof(string));
            _dataviews.Columns.Add("score", typeof(int));

            // Load the reports_mapping dictionary...
            _currentReportsMapping = _sharedUtils.GetReportsMapping(false);
        }

        private void ReportManager_Load(object sender, EventArgs e)
        {
            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            LoadDataviews();
            LoadDefaultReports(_reportsMapping);

            // Upgrade _currentReportsMapping to make sure there is an entry in the dictionary for every 
            // report found in the loading methods just executed and that the key is using the full path
            // instead of the legacy filename only...
            foreach (DataRow dr in _reportsMapping.Rows)
            {
                if (!_currentReportsMapping.ContainsKey(dr["full_path"].ToString().Trim().ToUpper()))
                {
                    // Full path was not found - see if a legacy filename only entry can be found...
                    if (_currentReportsMapping.ContainsKey(dr["name"].ToString().Trim().ToUpper()))
                    {
                        // Add a new entry to the reports mapping file that uses the full path for the key and 
                        // the current value for the legacy filename 
                        _currentReportsMapping.Add(dr["full_path"].ToString().Trim().ToUpper(), _currentReportsMapping[dr["name"].ToString().Trim().ToUpper()]);
                    }
                    else
                    {
                        // No match was found for this report - create a new reports mapping entry with the full path and no
                        // associated dataviews...
                        _currentReportsMapping.Add(dr["full_path"].ToString().Trim().ToUpper(), "");
                    }
                }
            }

            // No changes have been made so disable the Save button...
            ux_buttonSave.Enabled = false;

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }

        private void ReportManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ux_buttonSave.Enabled)
            {
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("You have unsaved changes, are you sure you want to cancel your edits and close this window?", "Cancel Edits and Close", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ReportManager_FormClosingMessage1";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                // Show the warning dialog message if there are unsaved edits...
                if (DialogResult.No == ggMessageBox.ShowDialog())
                {
                    e.Cancel = true;
                }
            }

            // Save these setting to the server...
            _sharedUtils.SaveUserSetting(this.Name, "_useDefaultServer", "Value", _useDefaultServer.ToString());
            _sharedUtils.SaveUserSetting(this.Name, "_useDefaultServerCredentials", "Value", _useDefaultServerCredentials.ToString());
            _sharedUtils.SaveUserSetting(this.Name, "_reportWebServerURL", "Value", _reportWebServerURL);
            _sharedUtils.SaveUserSetting(this.Name, "_reportWebServerUsername", "Value", _reportWebServerUsername);
            _sharedUtils.SaveUserSetting(this.Name, "_reportWebServerPassword", "Value", _reportWebServerPassword);
        }

        private void LoadDataviews()
        {
            // Get the collection of dataview names from the remote server...
            DataSet dsDataviewList = _sharedUtils.GetWebServiceData("get_dataview_list", "", 0, 0);
            DataSet dsDataviewFieldTitles = _sharedUtils.GetWebServiceData("all_dataview_field_titles", "", 0, 0);
            if (dsDataviewList.Tables.Contains("get_dataview_list") &&
                dsDataviewFieldTitles.Tables.Contains("all_dataview_field_titles"))
            {
                foreach (DataRow drDataview in dsDataviewList.Tables["get_dataview_list"].Rows)
                {
                    DataRow newDataviewDR = _dataviews.NewRow();
                    newDataviewDR["name"] = drDataview["dataview_name"].ToString().Trim().ToUpper();
                    string dataviewFieldList = " ";
                    dsDataviewFieldTitles.Tables["all_dataview_field_titles"].DefaultView.RowFilter = "dataview_name='" + drDataview["dataview_name"].ToString().Trim().ToUpper() + "'";
                    foreach (DataRowView drvDataviewField in dsDataviewFieldTitles.Tables["all_dataview_field_titles"].DefaultView)
                    {
                        dataviewFieldList += drvDataviewField["field_name"].ToString().Trim().ToUpper() + " ";
                    }
                    newDataviewDR["field_list"] = dataviewFieldList;
                    newDataviewDR["score"] = 0;
                    _dataviews.Rows.Add(newDataviewDR);
                }
            }

            // Set the sorting order for the dataview listbox...
            _dataviews.DefaultView.Sort = "score DESC, name ASC";

            // Apply the _dataviews datatable row filter based on the state of the 'Show All' checkbox...
            if (!ux_checkboxShowAll.Checked)
            {
                _dataviews.DefaultView.RowFilter = "score > 50";
            }
            else
            {
                _dataviews.DefaultView.RowFilter = "";
            }

            // Populate the dataviews checked list box...
            ux_checkedlistboxDataviews.Items.Clear();
            foreach (DataRowView drv in _dataviews.DefaultView)
            {
                ux_checkedlistboxDataviews.Items.Add(drv["name"] + " (" + drv["score"].ToString() + "%)", false);
            }
        }

        private void LoadDefaultReports(DataTable reportsMapping)
        {
            System.Collections.Generic.Dictionary<string, string> reportsMap = _sharedUtils.GetReportsMapping(false);
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports\");

            // Clear the reportsMapping datatable rows...
            reportsMapping.Rows.Clear();
            // Clear the checkedlistbox control items...
            ux_checkedlistboxDefaultReports.Items.Clear();

            foreach (System.IO.FileInfo fi in di.GetFiles("*.*", System.IO.SearchOption.AllDirectories))
            {
                if (reportsMap.ContainsKey(fi.FullName.Trim().ToUpper()))
                {
                    DataRow newRow = reportsMapping.NewRow();
                    newRow["name"] = fi.Name;
                    newRow["is_enabled"] = true;
                    newRow["full_path"] = fi.FullName;
                    newRow["dataviews"] = reportsMap[fi.FullName.Trim().ToUpper()];
                    newRow["required_fields"] = GetReportFields(fi.FullName.Trim().ToUpper());
                    reportsMapping.Rows.Add(newRow);
                }
                else if (reportsMap.ContainsKey("#" + fi.FullName.Trim().ToUpper()))
                {
                    DataRow newRow = reportsMapping.NewRow();
                    newRow["name"] = fi.Name;
                    newRow["is_enabled"] = false;
                    newRow["full_path"] = fi.FullName;
                    newRow["dataviews"] = reportsMap["#" + fi.FullName.Trim().ToUpper()];
                    newRow["required_fields"] = GetReportFields(fi.FullName.Trim().ToUpper());
                    reportsMapping.Rows.Add(newRow);
                }
                else if (reportsMap.ContainsKey(fi.Name.Trim().ToUpper()))
                {
                    DataRow newRow = reportsMapping.NewRow();
                    newRow["name"] = fi.Name;
                    newRow["is_enabled"] = true;
                    newRow["full_path"] = fi.FullName;
                    newRow["dataviews"] = reportsMap[fi.Name.Trim().ToUpper()];
                    newRow["required_fields"] = GetReportFields(fi.FullName.Trim().ToUpper());
                    reportsMapping.Rows.Add(newRow);
                }
                else if (reportsMap.ContainsKey("#" + fi.Name.Trim().ToUpper()))
                {
                    DataRow newRow = reportsMapping.NewRow();
                    newRow["name"] = fi.Name;
                    newRow["is_enabled"] = true;
                    newRow["full_path"] = fi.FullName;
                    newRow["dataviews"] = reportsMap["#" + fi.Name.Trim().ToUpper()];
                    newRow["required_fields"] = GetReportFields(fi.FullName.Trim().ToUpper());
                    reportsMapping.Rows.Add(newRow);
                }
                else
                {
                    DataRow newRow = reportsMapping.NewRow();
                    newRow["name"] = fi.Name;
                    newRow["is_enabled"] = false;
                    newRow["full_path"] = fi.FullName;
                    newRow["dataviews"] = "";
                    newRow["required_fields"] = GetReportFields(fi.FullName.Trim().ToUpper());
                    reportsMapping.Rows.Add(newRow);
                }
            }

            // Populate the items in the default reports checkedlistbox control...
            foreach (DataRow dr in reportsMapping.Rows)
            {
                ux_checkedlistboxDefaultReports.Items.Add(dr["name"].ToString(), isReportEnabled(dr["name"].ToString()) || isReportEnabled(dr["full_path"].ToString()));
            }

            // Select the first report in the checkedlistbox control...
            if (ux_checkedlistboxDefaultReports.Items.Count > 0) ux_checkedlistboxDefaultReports.SelectedIndex = 0;
        }

        private string GetReportFields(string FilePath)
        {
            string requiredFields = " ";
            CrystalDecisions.CrystalReports.Engine.ReportDocument reportDocument = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            if (FilePath.ToUpper().Contains(".RPT"))
            {
                try
                {
                    reportDocument.Load(FilePath);
                    for (int i = 0; i < reportDocument.Database.Tables[0].Fields.Count; i++)
                    {
                        //string fieldName = reportDocument.Database.Tables[0].Fields[i].Name;
                        //string fieldKind = reportDocument.Database.Tables[0].Fields[i].Kind.ToString();
                        //string fieldFormulaName = reportDocument.Database.Tables[0].Fields[i].FormulaName;
                        //string fieldNumberOfBytes = reportDocument.Database.Tables[0].Fields[i].NumberOfBytes.ToString();
                        //string fieldTableName = reportDocument.Database.Tables[0].Fields[i].TableName;
                        //string fieldUseCount = reportDocument.Database.Tables[0].Fields[i].UseCount.ToString();
                        //string fieldValueType = reportDocument.Database.Tables[0].Fields[i].ValueType.ToString();
                        requiredFields += reportDocument.Database.Tables[0].Fields[i].Name + " ";
                    }
                }
                catch (Exception)
                {
                }
            }
            else if(FilePath.ToUpper().Contains(".RDLC"))
            {
                try
                {
                    using (var fs = System.IO.File.OpenText(FilePath))
                    {
                        System.Xml.XmlReaderSettings settings = new System.Xml.XmlReaderSettings();
                        settings.IgnoreWhitespace = true;
                        using (System.Xml.XmlReader reader = System.Xml.XmlReader.Create(fs, settings))
                        {
                            while (reader.Read())
                            {
                                if (reader.Name.ToUpper() == "DATAFIELD" &&
                                    reader.NodeType == System.Xml.XmlNodeType.Element)
                                {
                                    reader.Read();
                                    if (reader.NodeType == System.Xml.XmlNodeType.Text) requiredFields += reader.Value + " "; 
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                }
            }

            reportDocument.Close();
            reportDocument.Dispose();

            return requiredFields;
        }

        private bool isReportEnabled(string reportName)
        {
            bool isEnabled = false;
            if (_currentReportsMapping.ContainsKey(reportName.ToUpper()))
            {
                isEnabled = true;
            }

            return isEnabled;
        }

        private void ux_checkedlistboxDefaultReports_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox clb = (CheckedListBox)sender;
            if (clb != null && clb.SelectedItem != null)
            {
                if (e.NewValue == CheckState.Checked)
                {
                    ux_groupboxDataviews.Enabled = true;
                }
                else
                {
                    ux_groupboxDataviews.Enabled = false;
                }

                string reportName = reportName = clb.SelectedItem.ToString().Trim().ToUpper();
                // Update the enabled status for the selected report...
                foreach (DataRow dr in _reportsMapping.Select("name='" + reportName.Trim() + "'"))
                {
                    dr["is_enabled"] = ux_groupboxDataviews.Enabled.ToString();
                }
            }

            // Enable the Save button...
            ux_buttonSave.Enabled = true;
        }

        private void ux_checkboxShowAll_CheckedChanged(object sender, EventArgs e)
        {
            // Apply the _dataviews datatable row filter based on the state of the 'Show All' checkbox...
            if (!ux_checkboxShowAll.Checked)
            {
                _dataviews.DefaultView.RowFilter = "score > 50";
            }
            else
            {
                _dataviews.DefaultView.RowFilter = "";
            }

            // Refresh dataview listbox based on currently selected report...
            RefreshDataviewListbox(ux_checkedlistboxDefaultReports.SelectedItem.ToString().Trim().ToUpper());
        }

        private void ux_checkedlistboxDefaultReports_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckedListBox clbReport = (CheckedListBox)sender;

            // Enable/disable the dataview groupbox based on the checked state of the currently selected report...
            if (clbReport.SelectedIndex > -1 && clbReport.GetItemChecked(clbReport.SelectedIndex))
            {
                ux_groupboxDataviews.Enabled = true;
            }
            else
            {
                ux_groupboxDataviews.Enabled = false;
            }

            RefreshDataviewListbox(clbReport.SelectedItem.ToString().Trim().ToUpper());
        }

        private void RefreshDataviewListbox(string reportName)
        {
            string[] requiredReportFields = null;
            string[] associatedDataviews = null;

            // Step 1 - recalculate the dataview scores based on the currently selected report...
            if (!string.IsNullOrEmpty(reportName))
            {
                // Get the list of required fields for the selected report...
                foreach (DataRow dr in _reportsMapping.Select("name='" + reportName.Trim() + "'"))
                {
                    requiredReportFields = dr["required_fields"].ToString().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                }

                // Calculate the required report field 'score' for each dataview...
                foreach (DataRow drDataview in _dataviews.Rows)
                {
                    float fieldMatches = 0;
                    foreach (string fieldName in requiredReportFields)
                    {
                        if (drDataview["field_list"].ToString().Contains(" " + fieldName.Trim().ToUpper() + " ")) fieldMatches++;
                    }
                    if (requiredReportFields.Length > 0)
                    {
                        drDataview["score"] = Math.Round(100.0 * (fieldMatches / requiredReportFields.Length));
                    }
                    else
                    {
                        drDataview["score"] = 0;
                    }
                }
            }

            // Populate the dataviews checked list box...
            ux_checkedlistboxDataviews.Items.Clear();
            foreach (DataRowView drv in _dataviews.DefaultView)
            {
                ux_checkedlistboxDataviews.Items.Add(drv["name"].ToString().Trim().ToUpper() + " (" + drv["score"].ToString() + "%)", false);
            }

            // Get the list of dataviews currently associated with the selected report...
            foreach (DataRow dr in _reportsMapping.Select("name='" + reportName.Trim() + "'"))
            {
                associatedDataviews = dr["dataviews"].ToString().Split('|');
            }

            int itemIndex = -1;
            // Set the checkbox for all dataviews associated with the current report...
            if (associatedDataviews != null && associatedDataviews.Length > 0)
            {
                foreach (string dataviewName in associatedDataviews)
                {
                    //itemIndex = ux_checkedlistboxDataviews.FindStringExact(dataviewName.Trim().ToUpper());
                    itemIndex = ux_checkedlistboxDataviews.FindString(dataviewName.Trim().ToUpper() + " ");
                    if (itemIndex > -1)
                    {
                        ux_checkedlistboxDataviews.SetItemChecked(itemIndex, true);
                    }
                }
            }
        }

        private void ux_checkedlistboxDataviews_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            string associatedDataviews = "";
            string reportName = "";
            string dataviewName = "";
            if (ux_checkedlistboxDefaultReports.SelectedItem != null &&
                ux_checkedlistboxDataviews.SelectedItem != null)
            {
                reportName = ux_checkedlistboxDefaultReports.SelectedItem.ToString().Trim().ToUpper();
                string[] dataviewNameParse = ux_checkedlistboxDataviews.SelectedItem.ToString().Trim().ToUpper().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                dataviewName = dataviewNameParse[0].Trim().ToUpper();

                DataRow[] drs = _reportsMapping.Select("name='" + reportName.Trim() + "'");
                if (drs.Length > 0) associatedDataviews = drs[0]["dataviews"].ToString();
                if (e.NewValue == CheckState.Checked)
                {
                    if (!associatedDataviews.ToUpper().Contains(" " + dataviewName + " "))
                    {
                        if (string.IsNullOrEmpty(associatedDataviews.Trim()))
                        {
                            associatedDataviews = " " + dataviewName + " ";
                        }
                        else
                        {
                            associatedDataviews = " " + associatedDataviews.Trim().ToUpper() + " | " + dataviewName + " ";
                        }
                    }
                }
                else
                {
                    if (associatedDataviews.ToUpper().Contains(dataviewName))
                    {
                        string[] dataviews = associatedDataviews.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        string newAssociatedDataviews = "";
                        foreach (string dataview in dataviews)
                        {
                            if (!dataview.Trim().ToUpper().Contains(dataviewName)) newAssociatedDataviews += dataview.Trim().ToUpper() + " | ";
                        }
                        associatedDataviews = " " + newAssociatedDataviews.TrimEnd(' ', '|') + " ";
                    }
                }

                // Set the list of dataviews associated with the selected report for every matching datarow...
                foreach (DataRow dr in drs)
                {
                    dr["dataviews"] = associatedDataviews;
                }

                // Enable the Save button...
                ux_buttonSave.Enabled = true;
            }
        }

        private void ux_buttonSave_Click(object sender, EventArgs e)
        {
            string usersReportsMappingFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\ReportsMapping.txt";
            System.IO.TextWriter tw = System.IO.File.CreateText(usersReportsMappingFilePath);

            // Write the version number for this ReportsMapping.txt file as a comment in the first line of the file...
            string versionNumber = System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
            tw.WriteLine("# ReportsMappingFileVersion=" + versionNumber.ToString());
            tw.WriteLine("#");
            // Now iterate thru all of the records in the report mapping table and write a new line for each RDLC report and/or Crystal Report...
            foreach (DataRow dr in _reportsMapping.Rows)
            {
                if (string.IsNullOrEmpty(dr["dataviews"].ToString()) || !(bool)dr["is_enabled"]) tw.Write("#");
                tw.WriteLine(dr["full_path"].ToString() + " = " + dr["dataviews"].ToString().Trim().ToLower().Replace(" | ", "; "));
            }
            tw.Close();

            // Disable the Save button...
            ux_buttonSave.Enabled = false;
            // Close the window...
            this.Close();
        }

        private void ux_buttonCancel_Click(object sender, EventArgs e)
        {
            // Close the window...
            this.Close();
        }

        private void ux_buttonCheckForDefaultReportUpdates_Click(object sender, EventArgs e)
        {
            WebServices ReportWebServices = new WebServices(_reportWebServerURL, _reportWebServerUsername, SharedUtils.SHA1EncryptionBase64(_reportWebServerPassword), _reportWebServerPassword, "");
            DataSet validatedLogin = ReportWebServices.ValidateLogin();
            if (validatedLogin != null &&
                validatedLogin.Tables.Contains("validate_login") &&
                validatedLogin.Tables["validate_login"].Rows.Count > 0)
            {
                // Sucessfully logged into the reports server - proceed with report updates...
                DataSet ds = ReportWebServices.GetFileInfo(null, null, false, false);
                string dss = ReportWebServices.GetVersion();
                if (ds != null &&
                    ds.Tables.Count > 0 &&
                    ds.Tables.Contains("file_info") &&
                    ds.Tables["file_info"].Columns.Contains("file_name") &&
                    ds.Tables["file_info"].Columns.Contains("virtual_file_path"))
                {
                    try
                    {
                        byte[] reportBytes;
                        string destinationFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports\";
                        if (!System.IO.Directory.Exists(destinationFilePath)) System.IO.Directory.CreateDirectory(destinationFilePath);
                        DataTable dt = ds.Tables["file_info"];
                        dt.DefaultView.RowFilter = "file_name like '%.rpt' OR file_name like '%.rdl' OR file_name like '%.rdlc'";
                        foreach (DataRowView drv in dt.DefaultView)
                        {
                            reportBytes = ReportWebServices.GetAttachment(drv["virtual_file_path"].ToString().Trim());
                            System.IO.File.WriteAllBytes(destinationFilePath + drv["file_name"].ToString().Trim(), reportBytes);
                        }

                        // Let the user know how many reports were updated to the latest version... 
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("{0} reports were updated successfully", "Report Manager Update Results", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                        ggMessageBox.Name = "ux_buttonCheckForDefaultReportUpdatesMessage1";
                        _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                        string[] argsArray = new string[100];
                        argsArray[0] = dt.DefaultView.Count.ToString();
                        ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                        ggMessageBox.ShowDialog();
                    }
                    catch
                    {
                        // ignore the error and continue...
                    }

                    //// Enable the Save button...
                    //ux_buttonSave.Enabled = true;
                }
            }
            else if (validatedLogin != null &&
                validatedLogin.Tables.Contains("ExceptionTable") &&
                validatedLogin.Tables["ExceptionTable"].Rows.Count > 0)

            {
                // Let the user know there were problems logging into the reports server... 
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There were errors connecting to the server distributing Curator Tool Crystal Reports.\n\nFull error message from the server:\n{0}", "Report Manager Connection Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ux_buttonCheckForDefaultReportUpdatesMessage2";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = validatedLogin.Tables["ExceptionTable"].Rows[0]["Message"].ToString();
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }
            else
            {
                // Let the user know there were problems logging into the reports server... 
                GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("The server distributing Curator Tool Crystal Reports did not respond.\n\nNo message was returned from server:\n{0}", "Report Manager Connection Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                ggMessageBox.Name = "ux_buttonCheckForDefaultReportUpdatesMessage3";
                _sharedUtils.UpdateControls(ggMessageBox.Controls, ggMessageBox.Name);
                string[] argsArray = new string[100];
                argsArray[0] = "";
                ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, argsArray);
                ggMessageBox.ShowDialog();
            }
        }

        private void ux_buttonOptions_Click(object sender, EventArgs e)
        {
            ReportManagerOptions reportManagerOptionsDialog = new ReportManagerOptions();
            reportManagerOptionsDialog.UseDefaultURL = _useDefaultServer;
            reportManagerOptionsDialog.UseDefaultCredentials = _useDefaultServerCredentials;
            reportManagerOptionsDialog.CustomServerURL = _reportWebServerURL;
            reportManagerOptionsDialog.CustomUsername = _reportWebServerUsername;
            reportManagerOptionsDialog.CustomPassword = _reportWebServerPassword;
            if (DialogResult.OK == reportManagerOptionsDialog.ShowDialog())
            {
                _useDefaultServer = reportManagerOptionsDialog.UseDefaultURL;
                _useDefaultServerCredentials = reportManagerOptionsDialog.UseDefaultCredentials;
                // URL to get updated reports...
                if (_useDefaultServer)
                {
                    _reportWebServerURL = _sharedUtils.Url;
                }
                else
                {
                    _reportWebServerURL = reportManagerOptionsDialog.CustomServerURL;
                }
                // Username/password to use when connecting to the URL for updated reports...
                if (_useDefaultServerCredentials)
                {
                    _reportWebServerUsername = _sharedUtils.Username;
                    _reportWebServerPassword = _sharedUtils.Password_ClearText;
                }
                else
                {
                    _reportWebServerUsername = reportManagerOptionsDialog.CustomUsername;
                    _reportWebServerPassword = reportManagerOptionsDialog.CustomPassword;
                }
            }
        }

        private void ux_checkedlistboxDefaultReports_DragEnter(object sender, DragEventArgs e)
        {
            // Okay we are in the middle of a Drag and Drop operation and the mouse has entered 
            // the listbox control so lets handle this event...

            // Is this an attachment being dragged in to the Attachment Wizard treeview...
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] fullPaths = (string[])e.Data.GetData(DataFormats.FileDrop);
                // Process each filepath in the collection to make sure only supported reports are being
                // dragged into the listbox control...
                string allowedReportExtensions = " .RPT .RDL .RDLC ";
                bool allReportsSupported = true;
                foreach (string fullPath in fullPaths)
                {
                    if (!allowedReportExtensions.Contains(" " + System.IO.Path.GetExtension(fullPath.ToUpper()) + " ")) allReportsSupported = false;
                }
                if (allReportsSupported)
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ux_checkedlistboxDefaultReports_DragDrop(object sender, DragEventArgs e)
        {
            // The drag-drop event is coming to a close - process this event to handle the dropping of
            // data into the listbox control...

            // Change cursor to the wait cursor...
            Cursor origCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // Process the report files being dropped into the listbox control...
            if (e.Data.GetDataPresent("FileDrop") && e.Effect != DragDropEffects.None)
            {
                bool fileSaveErrorsEncountered = false;
                // Get the list of report file paths...
                string[] fullPaths = (string[])e.Data.GetData(DataFormats.FileDrop);
                // Get the default report location folder and ensure it exists...
                string destinationFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\GRIN-Global\Curator Tool\Reports\";
                if (!System.IO.Directory.Exists(destinationFilePath)) System.IO.Directory.CreateDirectory(destinationFilePath);

                // Process each filepath in the collection...
                foreach (string fullPath in fullPaths)
                {
                    try
                    {
                        System.IO.FileInfo fi = new System.IO.FileInfo(fullPath);
                        System.IO.File.Copy(fullPath, destinationFilePath + fi.Name, true);
                    }
                    catch (Exception err)
                    {
                        // Something went wrong with the file copy so let the user
                        // know there were problems... 
                        fileSaveErrorsEncountered = true;
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox1 = new GRINGlobal.Client.Common.GGMessageBox("There were errors saving the report file '{0}' to the Reports folder.\n\nFull error message returned:\n{1}", "Report Manager Report File Save Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                        ggMessageBox1.Name = "ux_checkedlistboxDefaultReports_DragDropMessage1";
                        _sharedUtils.UpdateControls(ggMessageBox1.Controls, ggMessageBox1.Name);
                        string[] argsArray1 = new string[100];
                        argsArray1[0] = fullPath;
                        argsArray1[1] = err.Message;
                        ggMessageBox1.MessageText = string.Format(ggMessageBox1.MessageText, argsArray1);
                        ggMessageBox1.ShowDialog();
                    }
                }

                // Report the file drag/drop status to the user...
                if (!fileSaveErrorsEncountered)
                {
                    // Refresh the list of reports...
                    LoadDefaultReports(_reportsMapping);
                    // Rebuild the listbox control using the updated _reportsMapping datatable...

                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox2 = new GRINGlobal.Client.Common.GGMessageBox("Successfully saved {0} report file(s) to the Reports folder.", "Report Manager Report File Save", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox2.Name = "ux_checkedlistboxDefaultReports_DragDropMessage2";
                    _sharedUtils.UpdateControls(ggMessageBox2.Controls, ggMessageBox2.Name);
                    string[] argsArray2 = new string[100];
                    argsArray2[0] = fullPaths.Length.ToString();
                    ggMessageBox2.MessageText = string.Format(ggMessageBox2.MessageText, argsArray2);
                    ggMessageBox2.ShowDialog();
                }
            }

            // Restore cursor to default cursor...
            Cursor.Current = origCursor;
        }
    }
}
