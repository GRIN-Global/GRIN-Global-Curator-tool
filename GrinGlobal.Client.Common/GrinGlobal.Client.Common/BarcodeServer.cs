﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;
using ZXing;

namespace GRINGlobal.Client.Common
{
    class BarcodeServer
    {
        HttpListener BarcodeServerListener = new HttpListener();
        bool keepBarcodeServerAlive = false;

        public void Start(string barcodeServerURI)
        {
            keepBarcodeServerAlive = true;
            StartBarcodeServer(barcodeServerURI);
        }

        public void Stop()
        {
            keepBarcodeServerAlive = false;
            BarcodeServerListener.Stop();
            //BarcodeServerListener.Close();
        }

        private async void StartBarcodeServer(string barcodeServerURI)
        {
            if (!BarcodeServerListener.IsListening)
            {
                if (!BarcodeServerListener.Prefixes.Contains(barcodeServerURI)) BarcodeServerListener.Prefixes.Add(barcodeServerURI);

                BarcodeServerListener.Start();
            }

            while (keepBarcodeServerAlive)
            {
                try
                {
                    HttpListenerContext context = await BarcodeServerListener.GetContextAsync();
                    if (keepBarcodeServerAlive)
                    {
                        ProcessBarcodeRequest(context);
                    }
                    else
                    {
                        Stop();
                    }
                }
                catch
                {
                    // Suppress error...
                }
            }
        }
        private static void ProcessBarcodeRequest(HttpListenerContext context)
        {
            if (context.Request.HttpMethod.ToUpper() == "GET")
            {
                BarcodeWriter barcodeWriter = new BarcodeWriter();
                string d = context.Request.QueryString.Get("d");
                // The 'd' data parameter is required...
                if (!string.IsNullOrEmpty(d))
                {
                    string st = context.Request.QueryString.Get("st");
                    if (string.IsNullOrEmpty(st)) st = "T";
                    bool showText = true;
                    if (st.ToUpper() != "T") showText = false;
                    string x = context.Request.QueryString.Get("x");
                    if (string.IsNullOrEmpty(x)) x = "1";
                    int widthInPixels = Convert.ToInt32(double.Parse(x) * 96.0 / 2.54);
                    string y = context.Request.QueryString.Get("y");
                    if (string.IsNullOrEmpty(y)) y = "1";
                    int heightInPixels = Convert.ToInt32(double.Parse(y) * 96.0 / 2.54);
                    string i = context.Request.QueryString.Get("i");
                    if (string.IsNullOrEmpty(i)) i = "JPEG";
                    ImageFormat imageFormat = ImageFormat.Jpeg;
                    if (i.ToUpper().Equals("JPG")) imageFormat = ImageFormat.Jpeg;
                    else if (i.ToUpper().Equals("JPEG")) imageFormat = ImageFormat.Jpeg;
                    else if (i.ToUpper().Equals("GIF")) imageFormat = ImageFormat.Gif;
                    else if (i.ToUpper().Equals("1BMP")) imageFormat = ImageFormat.Bmp;
                    else if (i.ToUpper().Equals("BMP")) imageFormat = ImageFormat.Bmp;
                    else if (i.ToUpper().Equals("PNG")) imageFormat = ImageFormat.Png;
                    else imageFormat = ImageFormat.Jpeg;
                    switch (context.Request.Url.AbsolutePath.ToLower())
                    {
                        case "/aztec":
                            barcodeWriter.Format = BarcodeFormat.AZTEC;
                            ZXing.Aztec.AztecEncodingOptions aztecEncodingOptions = new ZXing.Aztec.AztecEncodingOptions();
                            aztecEncodingOptions.PureBarcode = !showText;
                            aztecEncodingOptions.Height = heightInPixels;
                            aztecEncodingOptions.Width = widthInPixels;
                            barcodeWriter.Options = aztecEncodingOptions;
                            break;
                        case "/datamatrix":
                            barcodeWriter.Format = BarcodeFormat.DATA_MATRIX;
                            ZXing.Datamatrix.DatamatrixEncodingOptions datamatrixEncodingOptions = new ZXing.Datamatrix.DatamatrixEncodingOptions();
                            //datamatrixEncodingOptions.SymbolShape = ZXing.Datamatrix.Encoder.SymbolShapeHint.FORCE_NONE;
                            datamatrixEncodingOptions.PureBarcode = !showText;
                            datamatrixEncodingOptions.Height = heightInPixels;
                            datamatrixEncodingOptions.Width = widthInPixels;
                            barcodeWriter.Options = datamatrixEncodingOptions;
                            break;
                        case "/linear":
                            barcodeWriter.Format = BarcodeFormat.CODE_128;
                            ZXing.OneD.Code128EncodingOptions code128EncodingOptions = new ZXing.OneD.Code128EncodingOptions();
                            code128EncodingOptions.PureBarcode = !showText;
                            code128EncodingOptions.Height = heightInPixels;
                            code128EncodingOptions.Width = widthInPixels;
                            barcodeWriter.Options = code128EncodingOptions;
                            break;
                        case "/qrcode":
                            barcodeWriter.Format = BarcodeFormat.QR_CODE;
                            ZXing.QrCode.QrCodeEncodingOptions qrCodeEncodingOptions = new ZXing.QrCode.QrCodeEncodingOptions();
                            qrCodeEncodingOptions.PureBarcode = !showText;
                            qrCodeEncodingOptions.Height = heightInPixels;
                            qrCodeEncodingOptions.Width = widthInPixels;
                            barcodeWriter.Options = qrCodeEncodingOptions;
                            break;
                        default:
                            break;
                    }
                    Bitmap barcodeBitmap = barcodeWriter.Write(d);
                    MemoryStream barcodeStream = new MemoryStream();
                    barcodeBitmap.Save(barcodeStream, imageFormat);
                    context.Response.ContentLength64 = barcodeStream.Length;
                    context.Response.ContentType = "image/" + imageFormat.ToString().ToUpper();
                    context.Response.OutputStream.Write(barcodeStream.ToArray(), 0, (int)barcodeStream.Length);
                }
            }
        }
    }
}
