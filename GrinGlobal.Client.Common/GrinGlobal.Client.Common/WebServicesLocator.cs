﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GRINGlobal.Client.Common
{
    public partial class WebServicesLocator : Form
    {
        Dictionary<string, string> _origWebServiceURLs;
        DataTable _webServiceURLs;
        BindingSource _webServiceURLsBindingSource;

        public WebServicesLocator(Dictionary<string, string> WebServiceURLs)
        {
            InitializeComponent();
            _origWebServiceURLs = WebServiceURLs;
            _webServiceURLs = new DataTable();
            _webServiceURLs.Columns.Add("display_member", typeof(string));
            _webServiceURLs.Columns.Add("value_member", typeof(string));
            _webServiceURLs.Columns.Add("is_ssl_connection", typeof(bool));
            _webServiceURLs.Columns.Add("is_ldap_connection", typeof(bool));
            foreach (string key in WebServiceURLs.Keys)
            {
                DataRow newURL = _webServiceURLs.NewRow();
                if (WebServiceURLs[key].ToString().Contains("ldap://"))
                {
                    newURL["value_member"] = WebServiceURLs[key].ToString().Replace("ldap://", "").Replace("/GRINGlobal/GUI.asmx", "").Trim();
                    newURL["is_ssl_connection"] = true;
                    newURL["is_ldap_connection"] = true;
                }
                else if (WebServiceURLs[key].ToString().Contains("https://"))
                {
                    newURL["value_member"] = WebServiceURLs[key].ToString().Replace("https://", "").Replace("/GRINGlobal/GUI.asmx", "").Trim();
                    newURL["is_ssl_connection"] = true;
                    newURL["is_ldap_connection"] = false;
                }
                else
                {
                    newURL["value_member"] = WebServiceURLs[key].ToString().Replace("http://", "").Replace("/GRINGlobal/GUI.asmx", "").Trim();
                    newURL["is_ssl_connection"] = false;
                    newURL["is_ldap_connection"] = false;
                }
                newURL["display_member"] = key.ToString();
                _webServiceURLs.Rows.Add(newURL);
            }
            ux_listboxWebserviceURLNames.DisplayMember = "display_member";
            ux_listboxWebserviceURLNames.ValueMember = "value_member";
            _webServiceURLsBindingSource = new BindingSource(_webServiceURLs, null);
            ux_listboxWebserviceURLNames.DataSource = _webServiceURLsBindingSource;
            ux_textboxListName.DataBindings.Add("Text", _webServiceURLsBindingSource, "display_member");
            ux_textboxServerName.DataBindings.Add("Text", _webServiceURLsBindingSource, "value_member");
            ux_checkboxUseSSL.DataBindings.Add("Checked", _webServiceURLsBindingSource, "is_ssl_connection");
            ux_checkboxUseLDAP.DataBindings.Add("Checked", _webServiceURLsBindingSource, "is_ldap_connection");
        }

        private void ux_buttonOK_Click(object sender, EventArgs e)
        {
            _origWebServiceURLs.Clear();
            foreach (DataRow dr in _webServiceURLs.Rows)
            {
                if ((bool)dr["is_ldap_connection"] && (bool)dr["is_ssl_connection"])
                {
                    _origWebServiceURLs.Add(dr["display_member"].ToString(), "ldap://" + dr["value_member"].ToString() + "/GRINGlobal/GUI.asmx");
                }
                else if ((bool)dr["is_ssl_connection"])
                {
                    _origWebServiceURLs.Add(dr["display_member"].ToString(), "https://" + dr["value_member"].ToString() + "/GRINGlobal/GUI.asmx");
                }
                else
                {
                    _origWebServiceURLs.Add(dr["display_member"].ToString(), "http://" + dr["value_member"].ToString() + "/GRINGlobal/GUI.asmx");
                }
            }
        }

        private void ux_buttonCancel_Click(object sender, EventArgs e)
        {
        }

        private void ux_buttonTestServer_Click(object sender, EventArgs e)
        {
            // Test the address for the GRIN-Global web services...
            try
            {
                string serverAddress = "";
                if (ux_checkboxUseLDAP.Checked || ux_checkboxUseSSL.Checked)
                {
                    serverAddress = "https://" + ux_textboxServerName.Text.Trim() + "/GRINGlobal/GUI.asmx";
                }
                else
                {
                    serverAddress = "http://" + ux_textboxServerName.Text.Trim() + "/GRINGlobal/GUI.asmx";
                }

                // Check to make sure the URL is valid...
                Uri uriResult;
                if(Uri.TryCreate(serverAddress, UriKind.Absolute, out uriResult))
                {
                    if(uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps)
                    {
                        // Test the URL to make sure it is reachable right now...
                        System.Net.WebClient wc = new System.Net.WebClient();
                        var junk = wc.DownloadData(serverAddress);
                        // If we made it past the DownloadData() method the URL must be up and running - so test if it is a GG website...
                        WebServices ws = new WebServices(serverAddress, "", "", "", "");
                        string wsVersion = ws.GetVersion();
                        if (wsVersion.ToLower().StartsWith("gringlobal.web"))
                        {
                            MessageBox.Show("Successfully connected to server running '" + wsVersion + "'");
                        }
                        else
                        {
                            MessageBox.Show("'" + ux_textboxServerName.Text.Trim() + "' is not a valid GRIN-Global server.\n\r\n\r" + "Server response: '" + wsVersion + "'");
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Server '" + ux_textboxServerName.Text.Trim() + "' is not responding.");
            }
        }

        private void ux_buttonAddNewListItem_Click(object sender, EventArgs e)
        {
            string serverAddress = "localhost";
            DataRow newURL = _webServiceURLs.NewRow();
            newURL["value_member"] = serverAddress;
            newURL["display_member"] = ux_groupboxListName.Text.Trim();
            newURL["is_ssl_connection"] = false;
            newURL["is_ldap_connection"] = false;
            _webServiceURLs.Rows.Add(newURL);
            ux_listboxWebserviceURLNames.SelectedIndex = ux_listboxWebserviceURLNames.Items.Count - 1;
            ux_textboxListName.Focus();
            ux_textboxListName.SelectAll();
        }

        private void ux_buttonDeleteListItem_Click(object sender, EventArgs e)
        {
            _webServiceURLs.Rows[_webServiceURLsBindingSource.Position].Delete();
        }

        private void ux_buttonMoveUp_Click(object sender, EventArgs e)
        {
            if (_webServiceURLsBindingSource.Position > 0)
            {
                int currentIndex = _webServiceURLsBindingSource.Position;
                string tempDisplayMember = _webServiceURLs.Rows[currentIndex]["display_member"].ToString();
                string tempValueMember = _webServiceURLs.Rows[currentIndex]["value_member"].ToString();
                bool tempIsSSLConnection = (bool)_webServiceURLs.Rows[currentIndex]["is_ssl_connection"];
                bool tempIsLDAPConnection = (bool)_webServiceURLs.Rows[currentIndex]["is_ldap_connection"];
                _webServiceURLs.Rows[currentIndex]["display_member"] = _webServiceURLs.Rows[currentIndex - 1]["display_member"];
                _webServiceURLs.Rows[currentIndex]["value_member"] = _webServiceURLs.Rows[currentIndex - 1]["value_member"];
                _webServiceURLs.Rows[currentIndex]["is_ssl_connection"] = _webServiceURLs.Rows[currentIndex - 1]["is_ssl_connection"];
                _webServiceURLs.Rows[currentIndex]["is_ldap_connection"] = _webServiceURLs.Rows[currentIndex - 1]["is_ldap_connection"];
                _webServiceURLs.Rows[currentIndex - 1]["display_member"] = tempDisplayMember;
                _webServiceURLs.Rows[currentIndex - 1]["value_member"] = tempValueMember;
                _webServiceURLs.Rows[currentIndex - 1]["is_ssl_connection"] = tempIsSSLConnection;
                _webServiceURLs.Rows[currentIndex - 1]["is_ldap_connection"] = tempIsLDAPConnection;
                _webServiceURLsBindingSource.MovePrevious();
            }
        }

        private void ux_buttonMoveDown_Click(object sender, EventArgs e)
        {
            if (_webServiceURLsBindingSource.Position < (_webServiceURLsBindingSource.Count - 1))
            {
                int currentIndex = _webServiceURLsBindingSource.Position;
                string tempDisplayMember = _webServiceURLs.Rows[currentIndex]["display_member"].ToString();
                string tempValueMember = _webServiceURLs.Rows[currentIndex]["value_member"].ToString();
                bool tempIsSSLConnection = (bool)_webServiceURLs.Rows[currentIndex]["is_ssl_connection"];
                bool tempIsLDAPConnection = (bool)_webServiceURLs.Rows[currentIndex]["is_ldap_connection"];
                _webServiceURLs.Rows[currentIndex]["display_member"] = _webServiceURLs.Rows[currentIndex + 1]["display_member"];
                _webServiceURLs.Rows[currentIndex]["value_member"] = _webServiceURLs.Rows[currentIndex + 1]["value_member"];
                _webServiceURLs.Rows[currentIndex]["is_ssl_connection"] = _webServiceURLs.Rows[currentIndex + 1]["is_ssl_connection"];
                _webServiceURLs.Rows[currentIndex]["is_ldap_connection"] = _webServiceURLs.Rows[currentIndex + 1]["is_ldap_connection"];
                _webServiceURLs.Rows[currentIndex + 1]["display_member"] = tempDisplayMember;
                _webServiceURLs.Rows[currentIndex + 1]["value_member"] = tempValueMember;
                _webServiceURLs.Rows[currentIndex + 1]["is_ssl_connection"] = tempIsSSLConnection;
                _webServiceURLs.Rows[currentIndex + 1]["is_ldap_connection"] = tempIsLDAPConnection;
                _webServiceURLsBindingSource.MoveNext();
            }
        }

        private void ux_textboxListName_TextChanged(object sender, EventArgs e)
        {
            ((DataRowView)_webServiceURLsBindingSource.Current)["display_member"] = ux_textboxListName.Text;
        }

        private void ux_checkboxUseSSL_CheckedChanged(object sender, EventArgs e)
        {
            if (ux_checkboxUseSSL.Checked)
            {
                ux_checkboxUseLDAP.Enabled = true;
                ux_checkboxUseLDAP.Checked = (bool)_webServiceURLs.Rows[_webServiceURLsBindingSource.Position]["is_ldap_connection"];
            }
            else
            {
                ux_checkboxUseLDAP.Enabled = false;
                ux_checkboxUseLDAP.Checked = false;
            }
        }

        private void ux_checkboxUseLDAP_CheckedChanged(object sender, EventArgs e)
        {
            if (ux_checkboxUseLDAP.Checked)
            {
                ux_checkboxUseSSL.Enabled = false;
            }
            else
            {
                ux_checkboxUseSSL.Enabled = true;
            }
        }
    }
}
