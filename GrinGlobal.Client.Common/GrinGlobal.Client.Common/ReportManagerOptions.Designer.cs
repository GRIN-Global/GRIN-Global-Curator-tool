﻿
namespace GRINGlobal.Client.Common
{
    partial class ReportManagerOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_groupboxServer = new System.Windows.Forms.GroupBox();
            this.ux_labelCustomServerURL = new System.Windows.Forms.Label();
            this.ux_textboxCustomServerURL = new System.Windows.Forms.TextBox();
            this.ux_radiobuttonCustomServer = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonDefaultServer = new System.Windows.Forms.RadioButton();
            this.ux_groupboxLoginCredentials = new System.Windows.Forms.GroupBox();
            this.ux_labelCustomPassword = new System.Windows.Forms.Label();
            this.ux_textboxCustomPassword = new System.Windows.Forms.TextBox();
            this.ux_labelCustomUsername = new System.Windows.Forms.Label();
            this.ux_textboxCustomUsername = new System.Windows.Forms.TextBox();
            this.ux_radiobuttonCustomLogin = new System.Windows.Forms.RadioButton();
            this.ux_radiobuttonDefaultLogin = new System.Windows.Forms.RadioButton();
            this.ux_buttonSave = new System.Windows.Forms.Button();
            this.ux_buttonCancel = new System.Windows.Forms.Button();
            this.ux_groupboxServer.SuspendLayout();
            this.ux_groupboxLoginCredentials.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_groupboxServer
            // 
            this.ux_groupboxServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxServer.Controls.Add(this.ux_labelCustomServerURL);
            this.ux_groupboxServer.Controls.Add(this.ux_textboxCustomServerURL);
            this.ux_groupboxServer.Controls.Add(this.ux_radiobuttonCustomServer);
            this.ux_groupboxServer.Controls.Add(this.ux_radiobuttonDefaultServer);
            this.ux_groupboxServer.Location = new System.Drawing.Point(13, 13);
            this.ux_groupboxServer.Name = "ux_groupboxServer";
            this.ux_groupboxServer.Size = new System.Drawing.Size(404, 100);
            this.ux_groupboxServer.TabIndex = 0;
            this.ux_groupboxServer.TabStop = false;
            this.ux_groupboxServer.Text = "Server";
            // 
            // ux_labelCustomServerURL
            // 
            this.ux_labelCustomServerURL.AutoSize = true;
            this.ux_labelCustomServerURL.Location = new System.Drawing.Point(30, 70);
            this.ux_labelCustomServerURL.Name = "ux_labelCustomServerURL";
            this.ux_labelCustomServerURL.Size = new System.Drawing.Size(32, 13);
            this.ux_labelCustomServerURL.TabIndex = 3;
            this.ux_labelCustomServerURL.Text = "URL:";
            // 
            // ux_textboxCustomServerURL
            // 
            this.ux_textboxCustomServerURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxCustomServerURL.Enabled = false;
            this.ux_textboxCustomServerURL.Location = new System.Drawing.Point(70, 67);
            this.ux_textboxCustomServerURL.Name = "ux_textboxCustomServerURL";
            this.ux_textboxCustomServerURL.Size = new System.Drawing.Size(328, 20);
            this.ux_textboxCustomServerURL.TabIndex = 2;
            this.ux_textboxCustomServerURL.TextChanged += new System.EventHandler(this.ux_textboxCustomServerURL_TextChanged);
            // 
            // ux_radiobuttonCustomServer
            // 
            this.ux_radiobuttonCustomServer.AutoSize = true;
            this.ux_radiobuttonCustomServer.Location = new System.Drawing.Point(7, 43);
            this.ux_radiobuttonCustomServer.Name = "ux_radiobuttonCustomServer";
            this.ux_radiobuttonCustomServer.Size = new System.Drawing.Size(60, 17);
            this.ux_radiobuttonCustomServer.TabIndex = 1;
            this.ux_radiobuttonCustomServer.Text = "Custom";
            this.ux_radiobuttonCustomServer.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCustomServer.CheckedChanged += new System.EventHandler(this.ux_radiobuttonCustomServer_CheckedChanged);
            // 
            // ux_radiobuttonDefaultServer
            // 
            this.ux_radiobuttonDefaultServer.AutoSize = true;
            this.ux_radiobuttonDefaultServer.Checked = true;
            this.ux_radiobuttonDefaultServer.Location = new System.Drawing.Point(7, 20);
            this.ux_radiobuttonDefaultServer.Name = "ux_radiobuttonDefaultServer";
            this.ux_radiobuttonDefaultServer.Size = new System.Drawing.Size(277, 17);
            this.ux_radiobuttonDefaultServer.TabIndex = 0;
            this.ux_radiobuttonDefaultServer.TabStop = true;
            this.ux_radiobuttonDefaultServer.Text = "Default (use currently connected GRIN-Global server)";
            this.ux_radiobuttonDefaultServer.UseVisualStyleBackColor = true;
            this.ux_radiobuttonDefaultServer.CheckedChanged += new System.EventHandler(this.ux_radiobuttonDefaultServer_CheckedChanged);
            // 
            // ux_groupboxLoginCredentials
            // 
            this.ux_groupboxLoginCredentials.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxLoginCredentials.Controls.Add(this.ux_labelCustomPassword);
            this.ux_groupboxLoginCredentials.Controls.Add(this.ux_textboxCustomPassword);
            this.ux_groupboxLoginCredentials.Controls.Add(this.ux_labelCustomUsername);
            this.ux_groupboxLoginCredentials.Controls.Add(this.ux_textboxCustomUsername);
            this.ux_groupboxLoginCredentials.Controls.Add(this.ux_radiobuttonCustomLogin);
            this.ux_groupboxLoginCredentials.Controls.Add(this.ux_radiobuttonDefaultLogin);
            this.ux_groupboxLoginCredentials.Location = new System.Drawing.Point(13, 120);
            this.ux_groupboxLoginCredentials.Name = "ux_groupboxLoginCredentials";
            this.ux_groupboxLoginCredentials.Size = new System.Drawing.Size(404, 119);
            this.ux_groupboxLoginCredentials.TabIndex = 1;
            this.ux_groupboxLoginCredentials.TabStop = false;
            this.ux_groupboxLoginCredentials.Text = "Login Credentials";
            // 
            // ux_labelCustomPassword
            // 
            this.ux_labelCustomPassword.AutoSize = true;
            this.ux_labelCustomPassword.Location = new System.Drawing.Point(6, 93);
            this.ux_labelCustomPassword.Name = "ux_labelCustomPassword";
            this.ux_labelCustomPassword.Size = new System.Drawing.Size(56, 13);
            this.ux_labelCustomPassword.TabIndex = 9;
            this.ux_labelCustomPassword.Text = "Password:";
            // 
            // ux_textboxCustomPassword
            // 
            this.ux_textboxCustomPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxCustomPassword.Enabled = false;
            this.ux_textboxCustomPassword.Location = new System.Drawing.Point(70, 90);
            this.ux_textboxCustomPassword.Name = "ux_textboxCustomPassword";
            this.ux_textboxCustomPassword.PasswordChar = '*';
            this.ux_textboxCustomPassword.Size = new System.Drawing.Size(328, 20);
            this.ux_textboxCustomPassword.TabIndex = 8;
            this.ux_textboxCustomPassword.TextChanged += new System.EventHandler(this.ux_textboxCustomPassword_TextChanged);
            // 
            // ux_labelCustomUsername
            // 
            this.ux_labelCustomUsername.AutoSize = true;
            this.ux_labelCustomUsername.Location = new System.Drawing.Point(6, 67);
            this.ux_labelCustomUsername.Name = "ux_labelCustomUsername";
            this.ux_labelCustomUsername.Size = new System.Drawing.Size(58, 13);
            this.ux_labelCustomUsername.TabIndex = 7;
            this.ux_labelCustomUsername.Text = "Username:";
            // 
            // ux_textboxCustomUsername
            // 
            this.ux_textboxCustomUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_textboxCustomUsername.Enabled = false;
            this.ux_textboxCustomUsername.Location = new System.Drawing.Point(70, 64);
            this.ux_textboxCustomUsername.Name = "ux_textboxCustomUsername";
            this.ux_textboxCustomUsername.Size = new System.Drawing.Size(328, 20);
            this.ux_textboxCustomUsername.TabIndex = 6;
            this.ux_textboxCustomUsername.TextChanged += new System.EventHandler(this.ux_textboxCustomUsername_TextChanged);
            // 
            // ux_radiobuttonCustomLogin
            // 
            this.ux_radiobuttonCustomLogin.AutoSize = true;
            this.ux_radiobuttonCustomLogin.Location = new System.Drawing.Point(7, 40);
            this.ux_radiobuttonCustomLogin.Name = "ux_radiobuttonCustomLogin";
            this.ux_radiobuttonCustomLogin.Size = new System.Drawing.Size(60, 17);
            this.ux_radiobuttonCustomLogin.TabIndex = 5;
            this.ux_radiobuttonCustomLogin.Text = "Custom";
            this.ux_radiobuttonCustomLogin.UseVisualStyleBackColor = true;
            this.ux_radiobuttonCustomLogin.CheckedChanged += new System.EventHandler(this.ux_radiobuttonCustomLogin_CheckedChanged);
            // 
            // ux_radiobuttonDefaultLogin
            // 
            this.ux_radiobuttonDefaultLogin.AutoSize = true;
            this.ux_radiobuttonDefaultLogin.Checked = true;
            this.ux_radiobuttonDefaultLogin.Location = new System.Drawing.Point(7, 17);
            this.ux_radiobuttonDefaultLogin.Name = "ux_radiobuttonDefaultLogin";
            this.ux_radiobuttonDefaultLogin.Size = new System.Drawing.Size(283, 17);
            this.ux_radiobuttonDefaultLogin.TabIndex = 4;
            this.ux_radiobuttonDefaultLogin.TabStop = true;
            this.ux_radiobuttonDefaultLogin.Text = "Default (use current GRIN-Global username/password)";
            this.ux_radiobuttonDefaultLogin.UseVisualStyleBackColor = true;
            this.ux_radiobuttonDefaultLogin.CheckedChanged += new System.EventHandler(this.ux_radiobuttonDefaultLogin_CheckedChanged);
            // 
            // ux_buttonSave
            // 
            this.ux_buttonSave.Location = new System.Drawing.Point(261, 245);
            this.ux_buttonSave.Name = "ux_buttonSave";
            this.ux_buttonSave.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonSave.TabIndex = 2;
            this.ux_buttonSave.Text = "Save";
            this.ux_buttonSave.UseVisualStyleBackColor = true;
            this.ux_buttonSave.Click += new System.EventHandler(this.ux_buttonSave_Click);
            // 
            // ux_buttonCancel
            // 
            this.ux_buttonCancel.Location = new System.Drawing.Point(342, 245);
            this.ux_buttonCancel.Name = "ux_buttonCancel";
            this.ux_buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonCancel.TabIndex = 3;
            this.ux_buttonCancel.Text = "Cancel";
            this.ux_buttonCancel.UseVisualStyleBackColor = true;
            this.ux_buttonCancel.Click += new System.EventHandler(this.ux_buttonCancel_Click);
            // 
            // ReportManagerOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 277);
            this.Controls.Add(this.ux_buttonCancel);
            this.Controls.Add(this.ux_buttonSave);
            this.Controls.Add(this.ux_groupboxLoginCredentials);
            this.Controls.Add(this.ux_groupboxServer);
            this.Name = "ReportManagerOptions";
            this.Text = "ReportManagerOptions";
            this.ux_groupboxServer.ResumeLayout(false);
            this.ux_groupboxServer.PerformLayout();
            this.ux_groupboxLoginCredentials.ResumeLayout(false);
            this.ux_groupboxLoginCredentials.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox ux_groupboxServer;
        private System.Windows.Forms.Label ux_labelCustomServerURL;
        private System.Windows.Forms.TextBox ux_textboxCustomServerURL;
        private System.Windows.Forms.RadioButton ux_radiobuttonCustomServer;
        private System.Windows.Forms.RadioButton ux_radiobuttonDefaultServer;
        private System.Windows.Forms.GroupBox ux_groupboxLoginCredentials;
        private System.Windows.Forms.Label ux_labelCustomPassword;
        private System.Windows.Forms.TextBox ux_textboxCustomPassword;
        private System.Windows.Forms.Label ux_labelCustomUsername;
        private System.Windows.Forms.TextBox ux_textboxCustomUsername;
        private System.Windows.Forms.RadioButton ux_radiobuttonCustomLogin;
        private System.Windows.Forms.RadioButton ux_radiobuttonDefaultLogin;
        private System.Windows.Forms.Button ux_buttonSave;
        private System.Windows.Forms.Button ux_buttonCancel;
    }
}