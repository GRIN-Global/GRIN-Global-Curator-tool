﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Reporting.WinForms;

namespace GRINGlobal.Client.Common
{
    public partial class SQLServerReportForm : Form
    {
        private DataTable _reportSourceTable;
        private string _sqlServerReportFilePath;

        public SQLServerReportForm(DataTable reportData, string sqlServerReportFilePath)
        {
            InitializeComponent();

            // Make a copy of the data to be provided to the SQL Server Report...
            _reportSourceTable = reportData.Copy();
            _sqlServerReportFilePath = sqlServerReportFilePath;
        }

        private void SQLServerReportForm_Load(object sender, EventArgs e)
        {
            /*
            ux_reportviewerMain.Reset();

            string reportDataSetName = GetDataSetName(_sqlServerReportFilePath);
            ux_reportviewerMain.LocalReport.ReportPath = _sqlServerReportFilePath;
            ReportDataSource rds = new ReportDataSource(reportDataSetName, _reportSourceTable);
            ux_reportviewerMain.LocalReport.DataSources.Clear();
            ux_reportviewerMain.LocalReport.DataSources.Add(rds);
            this.ux_reportviewerMain.RefreshReport();
            */

            ux_reportviewerMain.Reset();
            ux_reportviewerMain.LocalReport.DataSources.Clear();
            ux_reportviewerMain.LocalReport.EnableExternalImages = true;
            ux_reportviewerMain.LocalReport.EnableHyperlinks = true;
            ux_reportviewerMain.LocalReport.ReportPath = _sqlServerReportFilePath;
            // Try getting the dataset name using the built-in method from the local report class (in the viewer)...
            IEnumerable<string> datasetNames = ux_reportviewerMain.LocalReport.GetDataSourceNames();
            string reportDataSetName = "DataSet1";
            if (datasetNames.Count() == 1)
            {
                // If there is only one dataset in the report all is good - use the dataset name...
                reportDataSetName = datasetNames.ElementAt(0);
            }
            else
            {
                // If there is zero or more than one dataset name in the report use a 'brute force' method to find the one needed...
                reportDataSetName = GetDataSetName(_sqlServerReportFilePath);
            }
            ReportDataSource rds = new ReportDataSource(reportDataSetName, _reportSourceTable);
            ux_reportviewerMain.LocalReport.DataSources.Add(rds);
            ux_reportviewerMain.RefreshReport();
            // The next line fixes a behavior in the viewer that didn't properly render any pages after the first page (in a multi page print job)...
            ux_reportviewerMain.SetDisplayMode(DisplayMode.PrintLayout);
        }

        private string GetDataSetName(string reportFullPath)
        {
            string returnDataSetName = "";

            using (var fs = File.OpenText(reportFullPath))
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreWhitespace = true;
                using (XmlReader reader = XmlReader.Create(fs, settings))
                {
                    while (reader.Read())
                    {
                        if (reader.Name.ToUpper() == "DATASETNAME" &&
                            reader.NodeType == XmlNodeType.Element)
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text) returnDataSetName = reader.Value;
                        }
                    }
                }
            }

            return returnDataSetName;
        }
    }
}
