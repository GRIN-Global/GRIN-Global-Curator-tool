﻿
namespace GRINGlobal.Client.Common
{
    partial class ReportManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ux_groupboxReports = new System.Windows.Forms.GroupBox();
            this.ux_buttonOptions = new System.Windows.Forms.Button();
            this.ux_buttonCheckForDefaultReportUpdates = new System.Windows.Forms.Button();
            this.ux_checkedlistboxDefaultReports = new System.Windows.Forms.CheckedListBox();
            this.ux_labelDefaultReports = new System.Windows.Forms.Label();
            this.ux_checkedlistboxDataviews = new System.Windows.Forms.CheckedListBox();
            this.ux_groupboxDataviews = new System.Windows.Forms.GroupBox();
            this.ux_checkboxShowAll = new System.Windows.Forms.CheckBox();
            this.ux_buttonSave = new System.Windows.Forms.Button();
            this.ux_buttonCancel = new System.Windows.Forms.Button();
            this.ux_groupboxReports.SuspendLayout();
            this.ux_groupboxDataviews.SuspendLayout();
            this.SuspendLayout();
            // 
            // ux_groupboxReports
            // 
            this.ux_groupboxReports.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ux_groupboxReports.Controls.Add(this.ux_buttonOptions);
            this.ux_groupboxReports.Controls.Add(this.ux_buttonCheckForDefaultReportUpdates);
            this.ux_groupboxReports.Controls.Add(this.ux_checkedlistboxDefaultReports);
            this.ux_groupboxReports.Controls.Add(this.ux_labelDefaultReports);
            this.ux_groupboxReports.Location = new System.Drawing.Point(12, 12);
            this.ux_groupboxReports.Name = "ux_groupboxReports";
            this.ux_groupboxReports.Size = new System.Drawing.Size(318, 426);
            this.ux_groupboxReports.TabIndex = 1;
            this.ux_groupboxReports.TabStop = false;
            this.ux_groupboxReports.Text = "Reports:";
            // 
            // ux_buttonOptions
            // 
            this.ux_buttonOptions.Image = global::GRINGlobal.Client.Common.Properties.Resources.Settings;
            this.ux_buttonOptions.Location = new System.Drawing.Point(284, 13);
            this.ux_buttonOptions.Name = "ux_buttonOptions";
            this.ux_buttonOptions.Size = new System.Drawing.Size(28, 23);
            this.ux_buttonOptions.TabIndex = 6;
            this.ux_buttonOptions.UseVisualStyleBackColor = true;
            this.ux_buttonOptions.Click += new System.EventHandler(this.ux_buttonOptions_Click);
            // 
            // ux_buttonCheckForDefaultReportUpdates
            // 
            this.ux_buttonCheckForDefaultReportUpdates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCheckForDefaultReportUpdates.Location = new System.Drawing.Point(172, 13);
            this.ux_buttonCheckForDefaultReportUpdates.Name = "ux_buttonCheckForDefaultReportUpdates";
            this.ux_buttonCheckForDefaultReportUpdates.Size = new System.Drawing.Size(106, 23);
            this.ux_buttonCheckForDefaultReportUpdates.TabIndex = 5;
            this.ux_buttonCheckForDefaultReportUpdates.Text = "Update All...";
            this.ux_buttonCheckForDefaultReportUpdates.UseVisualStyleBackColor = true;
            this.ux_buttonCheckForDefaultReportUpdates.Click += new System.EventHandler(this.ux_buttonCheckForDefaultReportUpdates_Click);
            // 
            // ux_checkedlistboxDefaultReports
            // 
            this.ux_checkedlistboxDefaultReports.AllowDrop = true;
            this.ux_checkedlistboxDefaultReports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_checkedlistboxDefaultReports.FormattingEnabled = true;
            this.ux_checkedlistboxDefaultReports.Location = new System.Drawing.Point(6, 42);
            this.ux_checkedlistboxDefaultReports.Name = "ux_checkedlistboxDefaultReports";
            this.ux_checkedlistboxDefaultReports.Size = new System.Drawing.Size(306, 379);
            this.ux_checkedlistboxDefaultReports.TabIndex = 4;
            this.ux_checkedlistboxDefaultReports.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ux_checkedlistboxDefaultReports_ItemCheck);
            this.ux_checkedlistboxDefaultReports.SelectedIndexChanged += new System.EventHandler(this.ux_checkedlistboxDefaultReports_SelectedIndexChanged);
            this.ux_checkedlistboxDefaultReports.DragDrop += new System.Windows.Forms.DragEventHandler(this.ux_checkedlistboxDefaultReports_DragDrop);
            this.ux_checkedlistboxDefaultReports.DragEnter += new System.Windows.Forms.DragEventHandler(this.ux_checkedlistboxDefaultReports_DragEnter);
            // 
            // ux_labelDefaultReports
            // 
            this.ux_labelDefaultReports.AutoSize = true;
            this.ux_labelDefaultReports.Location = new System.Drawing.Point(6, 19);
            this.ux_labelDefaultReports.Name = "ux_labelDefaultReports";
            this.ux_labelDefaultReports.Size = new System.Drawing.Size(108, 13);
            this.ux_labelDefaultReports.TabIndex = 1;
            this.ux_labelDefaultReports.Text = "Curator Tool Reports:";
            // 
            // ux_checkedlistboxDataviews
            // 
            this.ux_checkedlistboxDataviews.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_checkedlistboxDataviews.CheckOnClick = true;
            this.ux_checkedlistboxDataviews.FormattingEnabled = true;
            this.ux_checkedlistboxDataviews.IntegralHeight = false;
            this.ux_checkedlistboxDataviews.Location = new System.Drawing.Point(6, 41);
            this.ux_checkedlistboxDataviews.Name = "ux_checkedlistboxDataviews";
            this.ux_checkedlistboxDataviews.Size = new System.Drawing.Size(324, 378);
            this.ux_checkedlistboxDataviews.TabIndex = 2;
            this.ux_checkedlistboxDataviews.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ux_checkedlistboxDataviews_ItemCheck);
            // 
            // ux_groupboxDataviews
            // 
            this.ux_groupboxDataviews.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_groupboxDataviews.Controls.Add(this.ux_checkboxShowAll);
            this.ux_groupboxDataviews.Controls.Add(this.ux_checkedlistboxDataviews);
            this.ux_groupboxDataviews.Location = new System.Drawing.Point(337, 13);
            this.ux_groupboxDataviews.Name = "ux_groupboxDataviews";
            this.ux_groupboxDataviews.Size = new System.Drawing.Size(336, 425);
            this.ux_groupboxDataviews.TabIndex = 3;
            this.ux_groupboxDataviews.TabStop = false;
            this.ux_groupboxDataviews.Text = "Dataviews:";
            // 
            // ux_checkboxShowAll
            // 
            this.ux_checkboxShowAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_checkboxShowAll.AutoSize = true;
            this.ux_checkboxShowAll.Location = new System.Drawing.Point(263, 18);
            this.ux_checkboxShowAll.Name = "ux_checkboxShowAll";
            this.ux_checkboxShowAll.Size = new System.Drawing.Size(67, 17);
            this.ux_checkboxShowAll.TabIndex = 3;
            this.ux_checkboxShowAll.Text = "Show All";
            this.ux_checkboxShowAll.UseVisualStyleBackColor = true;
            this.ux_checkboxShowAll.CheckedChanged += new System.EventHandler(this.ux_checkboxShowAll_CheckedChanged);
            // 
            // ux_buttonSave
            // 
            this.ux_buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonSave.Enabled = false;
            this.ux_buttonSave.Location = new System.Drawing.Point(520, 452);
            this.ux_buttonSave.Name = "ux_buttonSave";
            this.ux_buttonSave.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonSave.TabIndex = 4;
            this.ux_buttonSave.Text = "Save";
            this.ux_buttonSave.UseVisualStyleBackColor = true;
            this.ux_buttonSave.Click += new System.EventHandler(this.ux_buttonSave_Click);
            // 
            // ux_buttonCancel
            // 
            this.ux_buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ux_buttonCancel.Location = new System.Drawing.Point(601, 452);
            this.ux_buttonCancel.Name = "ux_buttonCancel";
            this.ux_buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.ux_buttonCancel.TabIndex = 5;
            this.ux_buttonCancel.Text = "Cancel";
            this.ux_buttonCancel.UseVisualStyleBackColor = true;
            this.ux_buttonCancel.Click += new System.EventHandler(this.ux_buttonCancel_Click);
            // 
            // ReportManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 487);
            this.Controls.Add(this.ux_buttonCancel);
            this.Controls.Add(this.ux_buttonSave);
            this.Controls.Add(this.ux_groupboxDataviews);
            this.Controls.Add(this.ux_groupboxReports);
            this.Name = "ReportManager";
            this.Text = "Report Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReportManager_FormClosing);
            this.Load += new System.EventHandler(this.ReportManager_Load);
            this.ux_groupboxReports.ResumeLayout(false);
            this.ux_groupboxReports.PerformLayout();
            this.ux_groupboxDataviews.ResumeLayout(false);
            this.ux_groupboxDataviews.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox ux_groupboxReports;
        private System.Windows.Forms.CheckedListBox ux_checkedlistboxDataviews;
        private System.Windows.Forms.GroupBox ux_groupboxDataviews;
        private System.Windows.Forms.CheckBox ux_checkboxShowAll;
        private System.Windows.Forms.Button ux_buttonSave;
        private System.Windows.Forms.Button ux_buttonCancel;
        private System.Windows.Forms.CheckedListBox ux_checkedlistboxDefaultReports;
        private System.Windows.Forms.Label ux_labelDefaultReports;
        private System.Windows.Forms.Button ux_buttonCheckForDefaultReportUpdates;
        private System.Windows.Forms.Button ux_buttonOptions;
    }
}