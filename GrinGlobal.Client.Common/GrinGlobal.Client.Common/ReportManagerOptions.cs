﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GRINGlobal.Client.Common
{
    public partial class ReportManagerOptions : Form
    {
        bool _useDefaultURL = true;
        bool _useDefaultCredentials = true;
        string _customServerURL = "";
        string _customUsername = "";
        string _customPassword = "";

        public ReportManagerOptions()
        {
            InitializeComponent();
        }

        public bool UseDefaultURL
        {
            get
            {
                return _useDefaultURL;
            }
            set
            {
                _useDefaultURL = value;
                if (_useDefaultURL)
                {
                    ux_radiobuttonDefaultServer.Checked = true;
                    ux_radiobuttonCustomServer.Checked = false;
                }
                else
                {
                    ux_radiobuttonDefaultServer.Checked = false;
                    ux_radiobuttonCustomServer.Checked = true;
                }
            }
        }

        public bool UseDefaultCredentials
        {
            get
            {
                return _useDefaultCredentials;
            }
            set
            {
                _useDefaultCredentials = value;
                if(_useDefaultCredentials)
                {
                    ux_radiobuttonDefaultLogin.Checked = true;
                    ux_radiobuttonCustomLogin.Checked = false;
                }
                else
                {
                    ux_radiobuttonDefaultLogin.Checked = false;
                    ux_radiobuttonCustomLogin.Checked = true;
                }
            }
        }

        public string CustomServerURL
        {
            get
            {
                return _customServerURL;
            }
            set
            {
                _customServerURL = value;
                ux_textboxCustomServerURL.Text = _customServerURL;
            }
        }

        public string CustomUsername
        {
            get
            {
                return _customUsername;
            }
            set
            {
                _customUsername = value;
                ux_textboxCustomUsername.Text = _customUsername;
            }
        }

        public string CustomPassword
        {
            get
            {
                return _customPassword;
            }
            set
            {
                _customPassword = value;
                ux_textboxCustomPassword.Text = _customPassword;
            }
        }

        private void ux_radiobuttonDefaultServer_CheckedChanged(object sender, EventArgs e)
        {
            ux_textboxCustomServerURL.Enabled = false;

            // Enable the Save button if the user has changed the report server URL...
            RefreshControls();
        }

        private void ux_radiobuttonCustomServer_CheckedChanged(object sender, EventArgs e)
        {
            ux_textboxCustomServerURL.Enabled = true;
            if (ux_textboxCustomServerURL.Text.Trim().Length < 1) ux_textboxCustomServerURL.Text = "https://arsiaamp3GGRpts.agron.iastate.edu/gringlobal/GUI.asmx";

            // Enable the Save button if the user has changed the report server URL...
            RefreshControls();
        }

        private void ux_radiobuttonDefaultLogin_CheckedChanged(object sender, EventArgs e)
        {
            ux_textboxCustomUsername.Enabled = false;
            ux_textboxCustomPassword.Enabled = false;

            // Enable the Save button if the user has changed the login credentials...
            RefreshControls();
        }

        private void ux_radiobuttonCustomLogin_CheckedChanged(object sender, EventArgs e)
        {
            ux_textboxCustomUsername.Enabled = true;
            ux_textboxCustomPassword.Enabled = true;

            // Enable the Save button if the user has changed the login credentials...
            RefreshControls();
        }

        private void ux_textboxCustomServerURL_TextChanged(object sender, EventArgs e)
        {
            // Enable the Save button if the user has changed the report server URL...
            RefreshControls();
        }

        private void ux_textboxCustomUsername_TextChanged(object sender, EventArgs e)
        {
            // Enable the Save button if the user has changed the login credentials...
            RefreshControls();
        }

        private void ux_textboxCustomPassword_TextChanged(object sender, EventArgs e)
        {
            // Enable the Save button if the user has changed the login credentials...
            RefreshControls();
        }

        private void ux_buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ux_buttonSave_Click(object sender, EventArgs e)
        {
            _useDefaultURL = ux_radiobuttonDefaultServer.Checked;
            _useDefaultCredentials = ux_radiobuttonDefaultLogin.Checked;
            _customServerURL = ux_textboxCustomServerURL.Text.Trim();
            _customUsername = ux_textboxCustomUsername.Text.Trim();
            _customPassword = ux_textboxCustomPassword.Text.Trim();

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void RefreshControls()
        {
            if(ux_radiobuttonDefaultServer.Checked != _useDefaultURL ||
                ux_radiobuttonDefaultLogin.Checked != _useDefaultCredentials ||
                ux_textboxCustomServerURL.Text != _customServerURL ||
                ux_textboxCustomUsername.Text != _customUsername ||
                ux_textboxCustomPassword.Text != _customPassword)
            {
                ux_buttonSave.Enabled = true;
            }
            else
            {
                ux_buttonSave.Enabled = false;
            }
        }
    }
}
